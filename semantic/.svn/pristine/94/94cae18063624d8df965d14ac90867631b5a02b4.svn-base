from g3.inference import nodeSearch
from g3.cost_functions.cost_function_semantic_mapping import CostFnSemanticMapping
from assert_utils import sorta_eq
import spatial_features_cxx as sf
from esdcs import esdcIo
from esdcs.groundings import Path, PhysicalObject, Prism, Place
from scipy import transpose as tp
import numpy as na
from esdcs.extractor import stanfordParserExtractor
from g3.esdcs_to_ggg import gggs_from_esdc_group
from semantic_mapping.semantic_mapping_state import node_to_physical_object
import math
from stopwords import stopwords
from g3.graph import GGG
from g3.evidences import Evidences
from esdcs.context import Context

class FigureText:
    def __init__(self, esdc):
        self.figure_esdc = esdc
        self.figure_text = esdc.text
        self.keywords = [k for k in self.figure_text.lower().split(" ") 
                         if not k in stopwords]
    def __str__(self):
        return self.figure_text

class SluInferenceResult:
    def __init__(self, figure_map_node, landmark_map_node, 
                 path_factor, path_cost, landmark_factor, landmark_cost, ggg):

        self.figure_map_node = figure_map_node
        self.landmark_map_node = landmark_map_node


        self.ggg = ggg
        self.path_factor = path_factor
        self.path_cost = path_cost
        self.landmark_factor = landmark_factor
        self.landmark_cost = landmark_cost
        self.cost = self.path_cost + self.landmark_cost
        self.probability = math.exp(-self.cost)

class SluApi:
    def __init__(self):
        model_fname = "../externals/slu/data/directions/direction_training/annotation/models/crf_discrete_sr_mapping_1.5.pck"
        #model_fname = "../externals/slu/data/directions/direction_training/annotation/models/crf_continuous_sr_1.5.pck"
        self.cost_function = CostFnSemanticMapping.from_mallet(model_fname, guiMode=True)
        self.task_planner = nodeSearch.BeamSearch(self.cost_function)


        self.distance_threshold = 30



    def score_particle(self, input_ggg, particle, path_node, 
                       landmark_node, path_factor, landmark_factor):
        """
        Score a particular particle by iterating through all nodes.
        """
        results = []
        self.cost_function.nodes = [(n, node_to_physical_object(n))
                                     for n in particle.nodelist]

        for i, (figure_map_node, pobj) in enumerate(self.cost_function.nodes):
            if figure_map_node.is_supernode == 0:
                continue
            for j, (landmark_map_node, landmark_pobj) in enumerate(self.cost_function.nodes):
                if landmark_map_node.is_supernode == 0:
                    continue

                if figure_map_node == landmark_map_node:
                    continue
                here_map_node = particle.get_current_position_node()                    

                if sf.math2d_dist(here_map_node.xy, figure_map_node.xy) > self.distance_threshold:
                    continue

                if sf.math2d_dist(here_map_node.xy, landmark_map_node.xy) > self.distance_threshold:
                    continue
                    

                # make a path
                #here_xy = particle.get_current_position_xy()
                #path = Path([0, 1], tp([(here_xy + (0, 0)),
                 #                       (figure_map_node.xy + (0, 0))]))

                #path_points = self.pquery.getPathPython(particle.id, -1, figure_map_node.id)

                #path_points = self.pquery.getPathPython(particle.id, landmark_map_node.id, figure_map_node.id)
                #path_points = [p + (0, 0) for p in path_points]
                path_points = particle.get_path_pts(here_map_node.id, figure_map_node.id)
                path_points = [p + (0, 0) for p in path_points]

                if len(path_points) == 0:
                    continue
                path = Path.from_xyztheta(na.arange(len(path_points)) * 1000, tp(path_points))
                place = node_to_physical_object(figure_map_node)
                x, y = figure_map_node.xy
                place = Place(Prism.from_points_xy([(x-0.1, x+0.1, x+0.1, x-0.1), (y-0.1, y-0.1, y+0.1, y+0.1)], place.prism.zStart, place.prism.zEnd))

                #if not path.hash_string.startswith("5d08dce") or not landmark_pobj.hash_string.startswith("ef449517a"):
                #    continue


                ggg = GGG.from_ggg_and_evidence(input_ggg, 
                                                Evidences.copy(input_ggg.evidences))
                ggg.null_costs()
                print "path node type", path_node.type
                if path_node.is_path:
                    ggg.set_evidence_for_node(path_node, [path])
                else:
                    ggg.set_evidence_for_node(path_node, [place])
                ggg.set_evidence_for_node(landmark_node, [landmark_pobj])
                agent = node_to_physical_object(here_map_node)
                ggg.context = Context.from_groundings([landmark_pobj, agent], agent_id=agent.id)

                self.task_planner.cost(ggg, [])

                path_cost = ggg.cost_for_factor(path_factor)

                landmark_cost = ggg.cost_for_factor(landmark_factor)
                results.append(SluInferenceResult(figure_map_node, 
                                                  landmark_map_node,
                                                  path_factor,
                                                  path_cost, 
                                                  landmark_factor, 
                                                  landmark_cost, ggg))
            #if i > 2:
            #    break
        results.sort(key=lambda x: x.cost)
        return results
                            

    def score_particles(self, text, particles):
        v = 1
        """
        For each particle, return a list of nodes that match the text.
        """
        f, landmark_phrase = text.split("is")
        landmark_words = landmark_phrase.split(" ")
        for i, w in enumerate(landmark_words):
            if w == "the":
                break
        sr = " ".join(landmark_words[0:i]).strip()
        landmark = " ".join(landmark_words[i:])

        if sr in ("down", "toward", "through"):
            sdc_type = "PATH"
        else:
            sdc_type = "PLACE"
        esdcs = esdcIo.parse("""
- '%s.'
- - EVENT:
      f: %s
      r:  is
      l:
         %s:
            r: %s
            l: %s
""" % (text, f, sdc_type, sr, landmark))
        print "sr", sr, "type", sdc_type        
        #extractor = stanfordParserExtractor.Extractor()
        #esdcs = extractor.extractEsdcs(text)
        #assert esdcs.entireText == text, (esdcs.entireText, text)

        if(v):
            print [str(e) for e in esdcs]

        figure_esdc = esdcs[0].f[0]
        path_esdc = esdcs[0].l[0]
        path_landmark_esdc = path_esdc.l[0]
        
        if(v):
            print "object", figure_esdc
            print "path", path_esdc
            print "landmark", path_landmark_esdc

        ggg = gggs_from_esdc_group([path_esdc])[0]

        path_node = ggg.top_event_node
        landmark_node = ggg.node_for_esdc(path_landmark_esdc)
        if(v):
            print path_landmark_esdc
        path_factor = ggg.esdc_to_factor(path_esdc)
        landmark_factor = ggg.esdc_to_factor(path_landmark_esdc)
        if(v):
            print "landmark", landmark_factor
        for phi_node in ggg.phi_nodes:
            ggg.set_evidence_for_node(phi_node, True)
        if(v):
            print "text", text
        results = []
        for p in particles: 
            results.append((p, self.score_particle(ggg, p, path_node, 
                                                   landmark_node, 
                                                   path_factor, 
                                                   landmark_factor)))
        return FigureText(figure_esdc), results
