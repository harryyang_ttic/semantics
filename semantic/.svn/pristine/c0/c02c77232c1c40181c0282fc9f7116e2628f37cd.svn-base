# Particle class for topological graph SLAM
import spatial_features_cxx as sf
# Import pygraph
from pygraph.classes.graph import graph
from pygraph.classes.digraph import digraph
from pygraph.algorithms.searching import breadth_first_search
from pygraph.readwrite.dot import write
from pygraph.algorithms.filters import find
from pygraph.algorithms.minmax import shortest_path
import spatial_features_cxx as sf

class Particle():
   def __init__(self, particle_id = 0, num = 0, weight = 1.0, par = None):
      if par == None:
         self.id = particle_id
         self.num_nodes = num
         self.nodelist = []
         self.nodelist_dict = {}
         self.edges = []
         self.weight = weight
         self.next_edge_id = 0
         self.region_map = {}
      else:
         self.id = particle_id
         self.num_nodes = par.num_nodes
         self.nodelist = par.nodelist[:]
         self.edges = par.edges[:]
         self.weight = 0
         self.region_map = {}
         if len(self.edges) > 0:
            self.next_edge_id = self.edges[len(self.edges)-1].edge_id+1
         else:
            self.next_edge_id = 0

      self.language_node_id = -1;
      self.language_node = None
      self.last_checked_id = -1
      self.pygraph = graph()
      self.spanning_tree = None
      self.dist_from_start = None  

   def get_pygraph_path_nodes(self, start_node, end_node):
      """
      Return the shortest path between two nodes as a list of xy points.
      """

      path_nodes = []
      path_nodes.append(end_node)
      last_node = end_node
      if(self.spanning_tree != None):
         while(self.spanning_tree[last_node] != start_node and self.spanning_tree[last_node] != None):
            path_nodes.append(self.spanning_tree[last_node])
            #print "Path Node " , self.spanning_tree[last_node].id
            last_node = self.spanning_tree[last_node]

      path_nodes.reverse()
      return path_nodes
      
   def get_pygraph_path_pts(self, start_node, end_node):
      """
      Return the shortest path between two nodes as a list of xy points.
      """

      path_nodes = []
      path_nodes.append(end_node)
      last_node = end_node
      if(self.spanning_tree != None):
         while(self.spanning_tree[last_node] != start_node and self.spanning_tree[last_node] != None):
            path_nodes.append(self.spanning_tree[last_node])
            #print "Path Node " , self.spanning_tree[last_node].id
            last_node = self.spanning_tree[last_node]

      path_nodes.reverse()

      #print "Path From " , start_node.id , " => " , end_node.id 

      #for i in path_nodes:
      #   print "\t", i.id
      
      #print "Path between Node : " , start_node_id, " and Node : " , end_node_id
      #for i in path: 
      #   print "\t Node : " , i.id
      return [n.xy for n in path_nodes]

   def build_shortest_distance(self, start_node):
      self.spanning_tree, self.dist_from_start = shortest_path(self.pygraph, start_node)

   def update_graph(self):
      #add nodes 
      #and edges 
      nodes = []
      for i in self.nodelist:
         nodes.append(i) #.id)
      self.pygraph.add_nodes(nodes)

      edges = []
      for j in self.edges:
         #do we add weights??
         nd1 = self.nodelist_dict[j.node1]
         nd2 = self.nodelist_dict[j.node2]
         opposite_pair = [nd2,nd1] 
         if(opposite_pair not in edges):
            edges.append([nd1, nd2])


      #connect close nodes 
      for i in self.nodelist:
         for j in self.nodelist:
            if(sf.math2d_dist(i.xy, j.xy) < 1.0):
               actual_pair = [i,j]
               opposite_pair = [j,i] 
               
               if((opposite_pair not in edges) and (actual_pair not in edges)):
                  edges.append([i,j])

      for ed in edges:
         self.pygraph.add_edge(ed)

   def update_language_node(self):
      if(self.nodelist_dict.has_key(self.language_node_id)):
         self.language_node = self.nodelist_dict[self.language_node_id]
         print "Found language node" 
      else:
         self.language_node = self.nodelist[self.num_nodes-1]


   def children_from_topological_map(self, node):
      children = []
      for edge in self.edges:
         if edge.node1 == node.id:
            children.append(self.node_for_id(edge.node2))
         if edge.node2 == node.id:
            children.append(self.node_for_id(edge.node1))
      return children

   def make_node_id_to_child_ids_shortest_path(self):
      node_id_to_child_ids = dict()
      
      for node in self.nodelist:
         child_ids = set()
         for edge in self.edges:
            if edge.node1 == node.id:
               child_ids.add(edge.node2)
            if edge.node2 == node.id:
               child_ids.add(edge.node1)
         for other_node in self.nodelist:
            if other_node != node:
               if sf.math2d_dist(other_node.xy, node.xy) < 4:
                  child_ids.add(other_node.id)

         node_id_to_child_ids[node.id] = child_ids
      return node_id_to_child_ids

         
   def add_edge(self, e):
      e.edge_id = self.next_edge_id
      self.next_edge_id += 1
      self.edges.append(e)
      
   def get_current_position_node(self):
      if(self.language_node == None):
         self.update_language_node()
      return self.language_node #self.nodelist[self.num_nodes-1]
   def get_current_position_xy(self):
      n = self.get_current_position_node()
      return (n.xy[0], n.xy[1])
   
   def node_for_id(self, node_id):
      for n in self.nodelist:
         if n.id == node_id:
            return n
      raise ValueError("No node for id: " + `node_id`)

   def get_path_nodes(self, start_node_id, end_node_id):
      """
      Return the shortest path between two nodes as a list of nodes. 
      """
      start_node = self.node_for_id(start_node_id)
      end_node = self.node_for_id(end_node_id)

      #node_id_to_parent_id = self.breadth_first_search(start_node, end_node)
      node_id_to_parent_id = self.a_star_search(start_node, end_node)
      path = []
      current_node = end_node

      while True:
         path.append(current_node)
         if current_node.id == start_node.id:
            break
         else:
            current_node_id = node_id_to_parent_id[current_node.id]
            current_node = self.node_for_id(current_node_id)

      return path

   def get_path_pts(self, start_node_id, end_node_id):
      """
      Return the shortest path between two nodes as a list of xy points.
      """
      path = self.get_path_nodes(start_node_id, end_node_id)
      
      #print "Path between Node : " , start_node_id, " and Node : " , end_node_id
      #for i in path: 
      #   print "\t Node : " , i.id
      return [n.xy for n in path]

   @property
   def node_id_to_child_ids(self):
      self.node_id_to_child_ids = self.make_node_id_to_child_ids_shortest_path()      
      return self.node_id_to_child_ids

   def breadth_first_search(self, start_node, end_node):
      active_set = [start_node]

      node_id_to_parent_id = dict()
      visited_node_ids = set([start_node])
     

      while len(active_set) != 0:
         new_active_set = []
         for node in active_set:
            visited_node_ids.add(node.id)
            #for child in self.children(node):
            for child_id in self.node_id_to_child_ids[node.id]:
               child = self.node_for_id(child_id)
               if not child.id in visited_node_ids:
                  new_active_set.append(child)
                  node_id_to_parent_id[child.id] = node.id
                  if child.id == end_node.id:
                     return node_id_to_parent_id
         active_set = new_active_set
      return node_id_to_parent_id


   def a_star_search(self, start_node, end_node):
      active_set = [(start_node.id)]

      node_id_to_parent_id = dict()
      visited_node_ids = set([start_node])
      g_score = dict()
      f_score = dict()
      g_score[start_node.id] = 0
      f_score[start_node.id] = g_score[start_node.id] + sf.math2d_dist(start_node.xy, end_node.xy)

      while len(active_set) != 0:
         current_node_id = active_set.pop(0)
         current_node = self.node_for_id(current_node_id)
         if current_node.id == end_node.id:
            return node_id_to_parent_id
         visited_node_ids.add(current_node.id)
         
         for child_id in self.node_id_to_child_ids[current_node.id]:
            child = self.node_for_id(child_id)
            if not child.id in visited_node_ids:
               tentative_g_score = g_score[current_node.id] + sf.math2d_dist(current_node.xy, child.xy)
               if child.id not in active_set or tentative_g_score < g_score[child.id]:
                  if child.id not in active_set:
                     active_set.append(child.id)
                     node_id_to_parent_id[child.id] = current_node.id
                     g_score[child.id] = tentative_g_score
                     f_score[child.id] = g_score[child.id] + sf.math2d_dist(child.xy, end_node.xy)
      return node_id_to_parent_id
