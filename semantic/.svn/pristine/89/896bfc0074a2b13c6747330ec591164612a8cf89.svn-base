cmake_minimum_required(VERSION 2.6.0)

# pull in the pods macros. See cmake/pods.cmake for documentation
set(POD_NAME python_scripts)
include(cmake/pods.cmake)

# require python
find_package(PythonInterp REQUIRED)

# install all python files in the python/ subdirectory
pods_install_python_packages(python)

# install a script "hello-python" that runs the hello.main python module
# This script gets installed to ${CMAKE_INSTALL_PREFIX}/bin/hello-python
# and automatically sets the correct python path.
pods_install_python_script(get-raw-odometry log_analysis.get_raw_odometry)
pods_install_python_script(covariance-testing log_analysis.covariance_testing)
pods_install_python_script(get-cylinder-detections-in-body log_analysis.get_cylinder_detections_in_body)
pods_install_python_script(ekf-online-slam ekf.ekf_online_slam)
pods_install_python_script(display-topo-graph topological_graph.make_topo_graph)
pods_install_python_script(run-topo-slam topo_slam.topo_graph_slam)
pods_install_python_script(sum-of-squares-error log_analysis.sum_of_squares_error)
pods_install_python_script(sum-of-squares-error-max-particle log_analysis.sum_of_squares_max_particle)
pods_install_python_script(nees-test log_analysis.nees_test)
pods_install_python_script(language-observation language_observation.language_observation)
pods_install_python_script(publish-language-observations language_observation.publish_language_observations)
pods_install_python_script(slu-eval slu_language_evaluation.slu_language_evaluation)
pods_install_python_script(slu-region-eval slu_region_language_evaluation.slu_region_language_evaluation)
pods_install_python_script(particle-analysis particle_analysis.particle_analysis)
pods_install_python_script(plot-weights particle_analysis.plot_particle_weights)
pods_install_python_script(er-annotation2lcm utils.annotation2lcm)
pods_install_python_script(er-rss-graph-comparison rss_experiments.graph_comparison)
pods_install_python_script(er-lcm-to-python-graph rss_experiments.lcm_to_python_graph)
pods_install_python_script(er-ask-questions ask_question_synth.ask_questions)
pods_install_python_script(er-plot-simple-sr simple_sr_plot.plot_simple_sr)
pods_install_python_script(er-plot-performance performance_logger.perform_logger)
pods_install_python_script(er-plot-performance-log performance_logger.analyze_log)