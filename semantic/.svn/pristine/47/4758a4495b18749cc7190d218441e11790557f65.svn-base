#
# Latex Makefile adapted from a collection of several other Makefiles,
# most notably that of Matti Airas et al., which is available at
#
#  http://www.acoustics.hut.fi/u/mairas/UltimateLatexMakefile
#
#-------------------------------------------------------------
#   History:
#   Date		Who			What
#   -------------	---------	----------------------
#   07/26/05		mrw			Created



##### Variables #############
LATEX	= latex
BIBTEX	= bibtex
MAKEINDEX = makeindex
#XDVI	= xdvi -gamma 4
XDVI	= xdvi
XPDF    = xpdf
DVIPS	= dvips
PSPDF   = ps2pdf
DVIPDF  = dvipdft
L2H	= latex2html
GH	= gv
GS = gs

RERUN = "(There were undefined references|Rerun to get (cross-references|the bars) right)"
RERUNBIB = "No file.*\.bbl|Citation.*undefined"
MAKEIDX = "^[^%]*\\makeindex"
MPRINT = "^[^%]*print"
USETHUMBS = "^[^%]*thumbpdf"

DATE=$(shell date +%Y-%m-%d)

COPY = if test -r $(<:%.tex=%.toc); then cp $(<:%.tex=%.toc) $(<:%.tex=%.toc.bak); fi 
RM = rm -f
OUTDATED = echo "EPS-file is out-of-date!" && false


SRC	:= $(shell egrep -l '^[^%]*\\begin\{document\}' *.tex)
#SRC	:= concept.tex
DVIF = $(SRC:%.tex=%.dvi)
PSF	= $(SRC:%.tex=%.ps)
TRG = $(SRC:%.tex=%.pdf)
PDFLATEXTRG = $(SRC:%.tex=%.pdf)


# Sources
#SRC:=$(wildcard *.tex)


# ATTENTION!
# File-extensions to delete recursive from here
EXTENSION=aux bbl blg glg idx ind ilg lof log lol lot lox out toc 




# Function definitions
define run-latex
	$(COPY);$(LATEX) $<
	egrep $(MAKEIDX) $< && ($(MAKEINDEX) $(<:%.tex=%);$(COPY);$(LATEX) $<) >/dev/null; true
	egrep -c $(RERUNBIB) $(<:%.tex=%.log) && ($(BIBTEX) $(<:%.tex=%);$(COPY);$(LATEX) $<) ; true
	egrep $(RERUN) $(<:%.tex=%.log) && ($(COPY);$(LATEX) $<) ; true
	egrep $(RERUN) $(<:%.tex=%.log) && ($(COPY);$(LATEX) $<) ; true
	if cmp -s $(<:%.tex=%.toc) $(<:%.tex=%.toc.bak); then true ;else $(LATEX) $< ; fi
	$(RM) $(<:%.tex=%.toc.bak)
	# Display relevant warnings
	egrep -i "(Reference|Citation).*undefined" $(<:%.tex=%.log) ; true
endef

# pdflatex
define run-pdflatex
	@$(run-latex)
endef


define get_dependencies
	deps=`perl -ne '($$_)=/^[^%]*\\\(?:include|input)\{(.*?)\}/;@_=split /,/;foreach $$t (@_) {print "$$t.tex "}' $<`
endef

define getbibs
	bibs=`perl -ne '($$_)=/^[^%]*\\\bibliography\{(.*?)\}/;@_=split /,/;foreach $$b (@_) {print "$$b.bib "}' $< $$deps`
endef

define geteps
	epses=`perl -ne '@foo=/^[^%]*\\\(includegraphics|psfig)(\[.*?\])?\{(.*?)\}/g;if (defined($$foo[2])) { if ($$foo[2] =~ /.eps$$/) { print "$$foo[2] "; } else { print "$$foo[2].eps "; }}' $< $$deps`
endef

define manconf
	mandeps=`if test -r $(basename $@).cnf ; then cat $(basename $@).cnf |tr -d '\n\r' ; fi`
endef



all 	: $(TRG)

.PHONY	: all show clean ps pdf showps veryclean

clean	:
	-rm -f $(TRG) $(PSF) $(DVIF) $(TRG:%.pdf=%.aux) $(TRG:%.pdf=%.bbl) $(TRG:%.pdf=%.blg) $(TRG:%.pdf=%.log) $(TRG:%.pdf=%.out) $(TRG:%.pdf=%.idx) $(TRG:%.pdf=%.ilg) $(TRG:%.pdf=%.ind) $(TRG:%.pdf=%.toc) $(TRG:%.pdf=%.d)


veryclean	: clean
	  -rm -f *.log *.aux *.dvi *.bbl *.blg *.ilg *.toc *.lof *.lot *.idx *.ind *.ps  *~

# This is a rule to generate a file of prerequisites for a given .tex file
%.d	: %.tex
	$(get_dependencies) ; echo $$deps ; \
	$(getbibs) ; echo $$bibs ; \
	$(geteps) ; echo $$epses ; \
	$(manconf) ; echo  $$mandeps  ;\
	echo "$*.dvi $@ : $< $$deps $$bibs $$epses $$mandeps" > $@ 
	include $(SRC:.tex=.d) # this was on a line by itself

# $(DEP) $(EPSPICS) $(BIBFILE)
$(DVIF)	: %.dvi : %.tex
	  @$(run-latex)

# The following two rules go from dvi-->ps-->pdf by explicitely producing the ps
$(PSF)	: %.ps : %.dvi
	 @$(DVIPS) -Ppdf -GO -q -j -f $< -o $@

$(TRG)  : %.pdf : %.ps
	@$(PSPDF) -dPDFSETTINGS=/prepress -dEmbedAllFonts=true -dSubsetFonts=true $< $@


# The following generates the PDF by piping the output of DVIPS to gs. This is much like
# calling ps2pdf, which is essentially a script that calls gs.

# To use pdflatex, comment the two lines above and uncomment the lines below
#$(TRG) : LATEX = pdflatex

#$(TRG)  : %.pdf : %.tex
#	@$(run-pdflatex)


show	: $(TRG)
	@for i in $(TRG) ; do $(XPDF) $$i & done

showps	: $(PSF)
	  @for i in $(PSF) ; do $(GH) $$i & done

ps	: $(PSF) 

dvi	: $(DVIF) 

pdflatex : LATEX = pdflatex

pdflatex: $(SRC)
	@$(run-latex) $(SRC)
	@$(run-latex) $(SRC)
	@$(run-latex) $(SRC)

# TODO: This probably needs fixing
html	: @$(DEP) $(EPSPICS)
	  @$(L2H) $(SRC)


