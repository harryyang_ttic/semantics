#include <yaml-cpp/yaml.h>
#include "annotation.hpp"
#include <fstream>
#include <iostream>
#include "feature_extractor.hpp"
#include "dataset.hpp"
#include <boost/archive/polymorphic_text_oarchive.hpp>

#include <shark/Data/Csv.h> //importing the file
#include <shark/Algorithms/Trainers/RFTrainer.h> //the random forest trainer
#include <shark/ObjectiveFunctions/Loss/ZeroOneLoss.h> //zero one loss for evaluation
#include <getopt.h>

using namespace sr_dataset;
using namespace std;
using namespace shark;

RealVector get_data_vector(gsl_vector *v){
    RealVector vec(v->size);

    size_t n_cols = v->size;
    for(size_t i=0; i < n_cols; i++){

        double value = gsl_vector_get (v, i);
        vec[i] = value;
    }
    return vec;
}

struct state_t {
    lcm_t *lcm; 
    GMainLoop *mainloop; 
    vector<rf_result> results; 
    slu_annotation_list_t *msg; 
    vector<datapoint> trained_datapoints; 
    vector<datapoint> test_datapoints; 

    state_t(){
        msg = NULL;
    };

    void update_dataset(){
        if(msg != NULL){
            slu_annotation_list_t_destroy(msg);
        }
            
        msg = (slu_annotation_list_t *) calloc(1,sizeof(slu_annotation_list_t));
        msg->count = (int) (trained_datapoints.size() + test_datapoints.size());
        msg->annotations = (slu_annotation_t *) calloc(msg->count, sizeof(slu_annotation_t));
        for(int i=0; i < trained_datapoints.size(); i++){
            trained_datapoints[i].annotation.to_lcm(&msg->annotations[i]);
        }

        for(int i=0; i < test_datapoints.size(); i++){
            int k = trained_datapoints.size() + i;
            test_datapoints[i].annotation.to_lcm(&msg->annotations[k]);
        }
    }

    void publish_dataset(lcm_t *lcm){
        slu_annotation_list_t_publish(lcm, "SLU_ANNOTATIONS", msg);
    }
};

static void on_request (const lcm_recv_buf_t *rbuf, const char *channel,
                                 const slu_annotation_request_t *msg, void *user)
{
    state_t *s = (state_t *) user;

    s->publish_dataset(s->lcm);
}

void usage(char *name){
    fprintf(stdout, "Usage : %s\n", name);
}

int main(int argc, char **argv)
{

    const char *optstring = "d:o:vhc:";
    char c;
    
    bool have_path = false;
    string path;
    bool verbose = false;
    string output_path("model/");
    int max_number = 1000000000;
    struct option long_opts[] = { { "help", no_argument, 0, 'h' },
                                  {  "data", required_argument, 0, 'd' },
                                  {  "output_folder", required_argument, 0, 'o' },
                                  {  "load_count", required_argument, 0, 'n' },
                                  { "verbose", no_argument, 0, 'v' },
                                  { 0, 0, 0, 0 } };

    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
        switch (c) {
        case 'd':
            have_path = true;
            path = string(optarg);
            break;
        case 'c':
            max_number = atoi(optarg);
            break;
        case 'o':
            output_path = string(optarg);
            break;
        case 'v':
            verbose = true;
            break;
        case 'h':
        default:
            usage(argv[0]);
            return 1;
        }
    }

    if(!have_path){
        usage(argv[0]);
        return -1;
    }

    state_t state;
        
    const gsl_rng_type * T = gsl_rng_default;
    gsl_rng *rng = gsl_rng_alloc (T);
    
    struct timeval tv;
    unsigned int seed;
    gettimeofday(&tv,0);
    seed = tv.tv_sec + tv.tv_usec;

    //seed doesnt seem to work
    gsl_rng_set (rng, seed);
    
    YAML::Node config = YAML::LoadFile(path);//"config.yaml");
    
    cout << "Number of examples : " << fmin(max_number, config.size()) << endl;
    
    vector<string> orig_names = spatial_features::path_feature_names();

    cout << "Done loading" << endl;

    int size = 100;
    
    int no_examples = fmin(max_number, config.size());
    
    map<string, dataset> data_corpus; 
    map<string, dataset>::iterator it;    

    //vector<Annotation> corpus;
    map<string, annotation_corpus> corpus;
    map<string, annotation_corpus>::iterator it_c;

    for (int i=0; i < no_examples; i++){
        
        YAML::Node nd = config[i];

        try{
            Annotation annotation(nd);
            if(i % size==0){
                fprintf(stdout, "Processing example : %d/%d\n", i, no_examples);
            }
            if(verbose){
                annotation.print();
            }

            gsl_matrix *temp_path = annotation.get_path_matrix();
            gsl_matrix *temp_bbox = annotation.get_landmark_matrix();

            if(!temp_path || !temp_bbox){
                fprintf(stderr, "Error - null gsl mat\n");
                continue;
            }

            if(verbose){
                spatial_features::print_gsl_matrix(temp_path);
                spatial_features::print_gsl_matrix(temp_bbox);
            }

            gsl_vector *vec = spatial_features::path_feature_values(temp_path , temp_bbox, true);


            ///Values are clamped also in SLU 
        
            //spatial_features::print_features(orig_names, vec);
            vector<string> new_names;
            //spatial_features::rectify_path_features(orig_names, vec, new_names, true, 10);

            /* With 10 clamping 
              across [45] - 1 / 1
              along [45] - 1 / 0.733333
              around [45] - 1 / 0.933333
              away from [45] - 1 / 1
              down [45] - 1 / 1
              out [45] - 1 / 1
              past [45] - 1 / 1
              through [45] - 1 / 0.666667
              to [43] - 1 / 1
              towards [45] - 1 / 1
              until [45] - 1 / 1
             */

            /* With no clamping
               across [45] - 1 / 0.866667
               along [45] - 1 / 0.8
               around [45] - 1 / 1
               away from [45] - 1 / 0.866667
               down [45] - 1 / 0.933333
               out [45] - 1 / 1
               past [45] - 1 / 1
               through [45] - 1 / 0.8
               to [43] - 1 / 1
               towards [45] - 1 / 0.933333
               until [45] - 1 / 1
            */

            spatial_features::rectify_path_features(orig_names, vec, new_names);
            if(verbose){
                spatial_features::print_features(new_names, vec);
            }

            annotation.add_feature_vector(new_names, vec);
            //we need to split this to the different SR's
            RealVector v = get_data_vector(vec);
            unsigned int l = annotation.get_relation_label();

            string relation = annotation.command.get_relation();
        
            it = data_corpus.find(relation); 
        
            if(it != data_corpus.end()){
                it->second.add_data(annotation, v, l);
            }
            else{
                cout << "Found new relation : " << relation << endl;
                dataset ds(relation, rng);
                ds.add_data(annotation,v,l);
                data_corpus.insert(make_pair(relation, ds));
            }

            gsl_matrix_free(temp_path);
            gsl_matrix_free(temp_bbox);
            gsl_vector_free(vec);
        
            //corpus.push_back(annotation);
            it_c = corpus.find(relation);
            
            if(it_c != corpus.end()){
                it_c->second.add(annotation);
            }
            else{
                annotation_corpus cp(relation);
                cp.add(annotation);
                corpus.insert(make_pair(relation, cp));
            }
        }
        catch(AnnotationException e){
            //e.print();
            cout << ".";
        }        
    }

    for(it = data_corpus.begin(); it != data_corpus.end(); it++){
        it->second.print();
    }

    cout << "\n\n\n";   

    double train_ratio = 3.0 / 4.0;

    for(it = data_corpus.begin(); it != data_corpus.end(); it++){

        cout << "\n==============================================="  << endl;

        it->second.print();

        if(it->second.get_positive_count() == 0){
            cout << "No positive examples - continuing" << endl;
            cout << "==============================================="  << endl;
            continue;
        }
        
        //it->second.update_balanced_dataset();
        ClassificationDataset data = it->second.get_classification_data();     

        if(data.numberOfElements() == 0){
            continue;
        }

        it->second.add_to_datasets(train_ratio, state.trained_datapoints, state.test_datapoints);

        //train with 3/4 of the dataset
        int train_count = data.numberOfElements() * train_ratio; 
        
        ClassificationDataset dataTest = splitAtElement(data, train_count);

        cout << "Training set - number of data points: " << data.numberOfElements()
             << " number of classes: " << numberOfClasses(data)
             << " input dimension: " << inputDimension(data) << endl;

        cout << "Test set - number of data points: " << dataTest.numberOfElements()
             << " number of classes: " << numberOfClasses(dataTest)
             << " input dimension: " << inputDimension(dataTest) << endl;

        //Generate a random forest
        RFTrainer trainer;
        RFClassifier model;
        trainer.train(model, data);
        cout<< "Done training\n";
        // evaluate Random Forest classifier
        ZeroOneLoss<unsigned int, RealVector> loss;
        Data<RealVector> prediction = model(data.inputs());

        double tr_acc = 1. - loss.eval(data.labels(), prediction);
        cout << "Random Forest on training set accuracy: " << 1. - loss.eval(data.labels(), prediction) << endl;

        prediction = model(dataTest.inputs());
        double te_acc = 1. - loss.eval(dataTest.labels(), prediction);
        cout << "Random Forest on test set accuracy:     " << 1. - loss.eval(dataTest.labels(), prediction) << endl;
        
        cout << "\n==============================================="  << endl;

        state.results.push_back(rf_result(it->second.relation, data.numberOfElements(), tr_acc, te_acc));

        //how to save the model 
        //http://image.diku.dk/shark/sphinx_pages/build/html/rest_sources/tutorials/concepts/misc/serialization.html
        char name[2048]; 
        sprintf(name, "%s/%s_rf.model", output_path.c_str(), it->first.c_str());
        
        ofstream ofs(name);
        boost::archive::polymorphic_text_oarchive oa(ofs);
        model.write(oa);
        ofs.close();
    }

    fprintf(stderr, "Trained Exampled : %d - Test Examples : %d\n", (int) state.trained_datapoints.size(), (int) state.test_datapoints.size());

    for(int i=0; i < state.results.size(); i++){
        state.results[i].print();
    }

    state.update_dataset();

    state.lcm =  bot_lcm_get_global(NULL);
    state.mainloop = g_main_loop_new( NULL, FALSE );
    
    slu_annotation_request_t_subscribe(state.lcm, "SLU_ANNOTATION_REQUEST", on_request, &state);
    bot_glib_mainloop_attach_lcm (state.lcm);
    bot_signal_pipe_glib_quit_on_kill(state.mainloop);
    g_main_loop_run(state.mainloop);
    bot_glib_mainloop_detach_lcm(state.lcm);
        
    return 0;
}
