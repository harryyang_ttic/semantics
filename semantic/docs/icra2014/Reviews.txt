Reviewer 1 of ICRA 2014 submission 1697

Comments to the author
======================

The paper describes a method for performing joint inference
over a spatial, topological and semantic environment
representation. The goal is to jointly represent and reason
over robot sensory data and natural-language human
annotations, with the aim of processing complex annotations
such as "the conference room is at the end of the
corridor".

I find it somewhat difficult to assess this paper.
On a theoretical level, I feel the paper builds a very tall
tower of inference machinery on a very narrow base of
sensory features. The inference task attempted is extremely
ambitious: jointly reasoning about metric uncertainty, 
topological uncertainty (which is hyper-exponential), and
label grounding. Meanwhile, the sensory underpinnings are
rudimentary (e.g. Simple and arbitrary hand-picked formulas
in
Eq. 7, Eq. 10, Eq. 12, multiple hand-picked constants
elsewhere).
It is not clear that the complex inference is achieving
anything sensible.

These misgivings are compounded by an inadequate results
section. It is hard to say with any confidence if the
method is actually working. The
results presented are mainly qualitative, and almost
impossible to interpret since no ground truth is provided.
(For example, in Figures 1 and 5, it is
impossible to tell if the semantic labels are correct).
Figure 8 is presented as evidence that the success of
grounding complex language, although the reader cannot draw
any such conclusion because it is not clear what impact the
lingustic information played in producing the labels in the
figure. A before-and-after
view showing the label state before and after the
linguistic information was provided (and indicating where
the robot was when the
annotation was made) would seem to be necessary to provide
more support for the claims.

Overall, I understand that this is ongoing work and some of
the sensory functions are essentially a placeholder.
However, with such an ambitious inference task, there must
be real concern that the approach is intractable. I would
like to see a lot more evidence presented to convince the
reader on this front.


Reviewer 3 of ICRA 2014 submission 1697

Comments to the author
======================

The authors propose a semantic mapping algorithm that
learns human-centric environment models from natural
language speech and scene interpretation. This paper relies
and extends the work already published in Proc. Robotics:
Science and Systems (RSS), 2013 : "Learning
Spatial-Semantic Representations from Natural Language
Descriptions and Scene Classification" with the same
authors. Although this work is correctly referred in the
bibliography, some parts of the two papers are quite
similar (i.e the section III "Semantic Graph
Representation" and the section III of the RSS paper
"Semantic Graph Representation"). The paper extends the
earlier approach to learn richer and more meaningful
semantic models of the environments. It allows to
incorporate scene classification using the robot's onboard
sensors (laser range finder and cameras) to estimate region
types. In my opinion, the new added material is sufficient
to justify this new submission.  With regard to standard
approaches for building semantic representation, this work
overpasses some limitations: capability to maintain
multiple hypothesis, bottom-up and top-down consistency
from metric/topological to semantic maps and capability to
maintain/merge sensor-based classification and label
observations from the user.

The experiments presented in the section "Results" are
convincing and	of high quality. It will be interesting to
have an idea of the computational cost for dealing with a
large and complex environment. In my opinion, the paper
should be accepted for the conference. I have just one
condition: the content of the paper is very dense and it
should be accepted only in its long version of 8 pages. A
shorter version, reduced to 6 pages, will be damageable in
term of understanding and readability. 

In its current version, the paper is well written and self
contents. 

Reviewer 4 of ICRA 2014 submission 1697

Comments to the author
======================

This paper presents a highly integrated approach to
building semantic maps which include distributions over
metric, topological and semantic maps in parallel. Building
on prior work this paper removes the need for a fixed
interval topological segmentation and instead maintains a
distribution across node assigments to regions. The work is
evaluated in comparison to the authors' previous work and
is shown to produce more compact and accurate topological
maps. THe work is very impressive and well explained,
although the evaluation is quite sparse on the language
grounding/influence parts (which are also included as
contributions here).

Related work is considered, but there is more interesting,
related work from Pronobis and colleagues that also reasons
about exploration and search, and therefore maintains
distributions over possibile connectivities, e.g. 

Hanheide, Marc, et al. "Exploiting probabilistic knowledge
under uncertain sensing for efficient robot behaviour."
Proceedings of the Twenty-Second international joint
conference on Artificial Intelligence-Volume Volume Three.
AAAI Press, 2011.

Aydemir, A.; Pronobis, A.; Gobelbecker, M.; Jensfelt, P.,
"Active Visual Object Search in Unknown Environments Using
Uncertain Semantics," Robotics, IEEE Transactions on ,
vol.29, no.4, pp.986,1002, Aug. 2013
doi: 10.1109/TRO.2013.2256686

... and also work that reasons -- non-probabilistically --
over node to region assignments

Hawes, N.; Hanheide, M.; Hargreaves, J.; Page, B.; Zender,
H.; Jensfelt, P., "Home alone: Autonomous extension and
correction of spatial representations," Robotics and
Automation (ICRA), 2011 IEEE International Conference on ,
vol., no., pp.3907,3914, 9-13 May 2011

However, the presented work goes beyond much of this in
interesting ways, so the omission is not critical.

The paper is well written and structured. The only typo I
noticed was  that "scanmatching" should probably be
hyphenated.

