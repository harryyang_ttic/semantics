\section{Related Work}
%

The field is at the point where robots need to reason over
human-centric models of space due in large part to the extensive
progress that has been made in solving the Simultaneous Localization
and Mapping (SLAM) problem. Not only have contributions to SLAM
allowed robots to operate robustly in unstructured environments like
our homes, but many existing approaches to semantic mapping are built
upon SLAM algorithms. Beginning with the seminal work
of~\citet{smith86}, SLAM is predominantly concerned with building
either globally metric or, to a lesser extent, topological maps for
the purpose of navigation. Our algorithm differs in that we represent
the map as a hierarchy that jointly models the metric, topological,
and semantic properties of the environment. The latter two layers are
particularly useful for human-centric mapping as the semantic map
models properties useful in grounding natural language
commands~\citep{tellex11}, while the topology is consistent with the
representation that humans use to model space~\citep{lynch60}. We use
the topological layer in our semantic graph to induce a pose graph in
the same fashion as many of the state-of-the-art SLAM
algorithms~\cite{konolige04, eustice05,olson07}. Given this topology,
we represent the distribution over the metric map as a Gaussian and,
like information filter-based SLAM algorithms~\citep{thrun04,
  eustice05, walter07, kaess08}, parametrize the distribution in the
canonical form for computational efficiency.

Unlike the SLAM problem, semantic mapping~\citep{kuipers00, zender08,
  kollar09, hemachandra11, pronobis12} is primarily interested in
learning higher-level properties of the robot's environment. These
properties include spatial attributes (like metric mapping) as well as
concepts such each room's type (e.g., ``hallway,'' or ``kitchen''),
their colloquial names (e.g., ``Carrie's office''), or the objects
that they contain. This information is useful for navigation, but also
facilitates human-robot interaction, including more efficient command
and control mechanisms.

Early work in semantic mapping includes the Spatial Semantic Hierarchy
(SSH) proposed by \citet{kuipers00} that represents a robot's spatial
knowledge as a coupled hierarchy. At the lowest level, the local
environment is modeled as a collection of control laws, each
expressing the relationship between sensory input and motor output,
that facilitate localization and generating local geometric
maps. Above the control level is the causal level, which provides a
discrete model of the actions that transition between each of the
control laws. The topological level represents the environment as a
collection of regions, places, and paths that abstract states and
actions from the causal level. While the topology serves as the
primary global map of the environment, the local geometric maps from
the control level can be merged via the topology to formulate a global
metric map.

\citet{kuipers04} describe an extension to the SSH that employs a
hybrid metric and topological representation to better represent
environments at both small and large scales. The Hybrid SSH treats the
environment as a collection of interconnected locations, each being
small in scale. The method employs metric maps to model the local
geometry of distinct regions from which they use local paths to induce
a symbolic global topology that describes the large-scale
environment. By decoupling the map in this manner, this approach more
efficiently models ambiguities in large-scale loop closure with
multiple compact topologies, without requiring that the set of local
metric maps be registered consistently in a single global reference
frame. This is a distinct benefit over submap approaches to
SLAM~\citep{leonard03,bosse04} that similarly employ local metric maps
but also seek to ensure that these submaps are consistent in a global
reference frame. The authors have shown~\citep{modayil04, beeson10}
that the representation allows uncertainties to be handled more
effectively by factoring them into individual components that capture
local metrical, global topological, and globally metrical
uncertainties. Our algorithm also consists of a hybrid metric and
topological representation and factors the joint distribution into
separate metrical and topological terms, employing different
hypotheses over the topological map to represent the distribution over
the space of loop closures. However, we maintain a globally metric map
of the environment with respect to a single frame of reference, which
can make our algorithm sensitive to global inconsistencies within
large environments. Another difference lies in our definition of
regions, which we segment based upon distance traveled. The Hybrid
SSH, in contrast, defines regions based upon their local geometric
structure (e.g., separated by doorways). Unlike our approach, however,
the Hybrid SSH does not model the semantic labels or colloquial names
associated with different regions of the environment.


More recent efforts similarly take a hierarchical approach to
representing semantic and spatial properties of a robot's
environment. Many existing solutions~\citep{galindo05, mozos07,
  meger08, vasudevan08, krieg05, zender08, hemachandra11, pronobis12}
build on the effectiveness of SLAM by augmenting a low-level metric
map with layers that encode the topological and semantic properties of
the environment. Typically, an off-the-shelf SLAM implementation is
used to build the metric layer. One level up in the hierarchy is the
topological map, taking the form of a graph, where vertices denote
different \emph{places} in the environment and edges model their
connectivity. Layered above the topology is the semantic map that
represents abstract properties associated with each place, such as
their type or the objects that they contain.

One distinction among existing approaches to semantic mapping, and
topological mapping in general, is the means by which the environment
is segmented into different places. \citet{thrun98}, for example, rely
on a user to push a button each time the robot transitions to a new
region. A straightforward automatic strategy is to segment regions
based upon distance, placing vertices at a fixed spacing as the robot
travels in the environment~\citep{zender08}. This is the approach that
we take in this paper. An alternative is to use heuristics such as
door detections to separate regions~\citep{pronobis12}, which yields
segmentations that can be more semantically meaningful within indoor
environments. Meanwhile, others have found success partitioning the
environment by clustering observations of local
metric~\citep{brunskill07} and semantic~\citep{mozos07} properties. Of
particular relevance to this work, \citet{ranganathan11} employ
multiple methods to define regions, including manual segmentation at
the location of gateways (e.g., doorways, junctions) and automatic
segmentation based upon changes in visual
appearance~\citep{ranganathan09}.

The properties contained within the semantic map are most often
inferred from the robot's sensor data (e.g., LIDAR scans and camera
images), using scene classifiers~\citep{pronobis10} and object
detectors~\citep{torralba03, kollar09}. For example, \citet{mozos07}
use a combination of boosted laser range features and image-based
object detections to classify the robot's surround as it navigates and
show how this can be used to induce a topology for the
environment. Similarly, \citet{meger08} layer a visual attention
system and image-based object recognition on top of a SLAM occupancy
grid map to build semantic maps that encode the locations of objects
of interest within the environment. \citet{vasudevan08} describe a
probabilistic framework that uses clustered object detections to learn
conceptual models of space that express their hierarchical structure
(e.g., that an ``office'' may include a ``workspace'' and ``meeting
area'') and the objects that they contain. They argue that this model
is amenable to a hierarchical metric-topological-semantic SLAM
framework, though they leave that for future work.

These solutions rely upon scene classifiers and object detectors to
infer the properties that make up the semantic map. The effectiveness
of these approaches is a function of the richness of the training
data. As such, they perform best when the environments have similar
appearance and regular geometry, and when the objects are drawn from a
common set. Even in structured settings, it is not uncommon for the
regions to be irregular and for the objects to be difficult to
recognize, either because they are out of context or are singletons
(Fig.~\ref{fig:question_mark}). Furthermore, scene classification
doesn't provide a means to infer the specific labels that humans use
for a location, such as ``Carrie's office'' or the ``Kiva conference
room.''

Our algorithm, on the other hand, is capable of learning the class and
colloquial name for different spaces in the environment from a human's
description. Recognizing the efficiency of human supervision during
learning, several researchers have proposed methods that incorporate
user-provided spoken cues into the semantic map~\citep{zender08,
  hemachandra11, pronobis12}. Of particular relevance,
\citet{zender08} use labels assigned by people to identify objects in
the robot's surround. They combine these labels with a LIDAR-based
place classifier to learn semantic maps for office environments that
encode the relationship between room categories and the objects that
they contain. Similarly, \citet{pronobis12} describe a multi-modal
probabilistic framework capable of incorporating semantic information
from a wide variety of modalities. These include a user's speech,
which is modeled just like any other sensor. The method seeks to learn
richer, more descriptive environment models by fusing semantic cues
from object detections, place appearance and geometry, as well as
human input into a single model.

Our algorithm differs from the existing state-of-the-art in semantic
mapping in three fundamental ways. First, our framework employs a
learned model of free-form utterances to reason over expressions that
are less constrained than those handled by other methods. To be
precise, we currently assume that these descriptions involve labels
for and spatial relations between one or two locations, though the
structure of these expressions is only limited by rules of grammar and
the amount of training data. Second, by using scene classification,
existing methods can only infer semantic properties of areas that are
within the field-of-view of the robot's sensors. Similarly, previous
efforts to incorporate user-provided labels assume that the object is
within view or that the user is referencing the robot's current
location. In contrast, our method reasons over more expressive
descriptions that enable robots to learn concepts like labels and
spatial relations for distant areas as well as regions of the
environment that the robot has not yet visited.  Third, existing
methods allow updates to the metric layer to influence the topological
and semantic maps, but don't use information at the semantic layer
improve the rest of the hierarchy. By maintaining a joint distribution
over the metric, topological, and semantic properties of the
environment, our framework uses updates to any one layer to improve
the other layers in the hierarchy. For example, we show how the
semantic map can be used to recognize loop closures, a fundamental
problem in SLAM, and thereby add edges to the topology that, in turn,
correct errors in the metric map.

We note that semantic observations are not the only source of
information that is useful for place recognition. Many mapping
algorithms build local laser scan patches for each region and
correlate these patches to identify loop
closures~\citep{gutmann99}. However, these techniques are prone to
perceptual aliasing when the local geometry is not distinctive, such
as in the case of hallways. More recent methods consider a region's
visual appearance as a more discriminative means of performing place
recognition~\citep{se05, cummins08, ranganathan11}. Of particular
note, \citet{cummins08} learn a generative model of region appearance
using a bag-of-words representation that expresses the commonality of
certain features. By effectively modeling this perceptual ambiguity,
the authors are able to reject invalid loop closures despite
significant aliasing, while correctly recognizing valid loop
closures. This and related approaches in effect choose the maximum
likelihood loop closure, relying on the assumption that the place
model is sufficiently descriptive that the resulting distribution over
the space of loop closures is peaked around the true
correspondence. Our approach differs in that it uses semantic
information to maintain a distribution over the space of loop closures
rather than only that which is most likely.

This work incorporates user-provided labels and spatial relations by
interpreting the free-form descriptions in the context of the semantic
graph.  As such, it is important to note existing efforts to solve
what \citet{harnad90} refers to as the symbol grounding problem, the
problem of mapping linguistic elements to their corresponding
manifestation in the external world. In the robotics domain, the
grounding problem has primarily been addressed in the context of
following route directions and other natural language commands. One
class of solutions~\citep{skubic04,macmahon06,
  dzifcak09,chen11,matuszek10,matuszek12a} considers the problem as
one of parsing free-form commands into their formal language
equivalent, which a planner takes as input. Other
approaches~\citep{kollar10,tellex11,tellex12} function by mapping
free-form utterances into their corresponding object and action
referents in the robot's world model. With the exception of
\citet{macmahon06} and \citet{matuszek12a}, existing methods assume
the map is known a priori. We take the latter approach to grounding
spatial language by inferring the locations that the user is referring
to in the map that is learned online. However, we allow these
locations to be unknown to the robot when the user provides the
descriptions.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "ijrr2013"
%%% End: 
