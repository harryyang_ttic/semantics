\section{The Semantic Graph}

%
This section presents our approach to maintaining a distribution over
semantic graphs, our environment representation that consists jointly
of metric, topological, and semantic maps.

The metric map models information contained in the robot's low-level
sensor readings. The topological map models the connectivity between
regions that can be inferred from navigation as well as natural
language descriptions. The semantic map represents categories that the
user conveys.

\subsection{Semantic Graphs}
%
We model the environment as a set of \emph{places}, regions in the
environment a fixed distance apart\footnote{We use 5\,m spacing for
  the results presented in this paper.} that the robot has visited. We
represent each place by its pose $x_i$ in a global reference frame and
a label $l_i$ (e.g., ``gym,'' ``hallway'').  More formally, we
represent the environment by the \mbox{tuple $\{G_t, X_t, L_t\}$} that
constitutes the semantic graph. The graph \mbox{$G_t=(V_t,E_t)$} denotes the
environment topology with a vertex \mbox{$V_t = \{v_1, v_2, \ldots,
  v_t\}$} for each place that the robot has visited, and undirected
edges $E_t$ that signify observed relations between vertices, based on
metric or semantic information.  The vector \mbox{$X_t = [x_1, x_2,
  \ldots, x_t]$} encodes the pose associated with each vertex. The set
\mbox{$L_t = \{l_1, l_2, \ldots, l_t\}$} includes the semantic label
$l_i$ associated with each vertex. The semantic graph
(Fig.~\ref{fig:semantic_graph_example}) grows as the robot moves
through the environment. Our method adds a new vertex $v_{t+1}$ to the
topology after the robot travels a specified distance, and augments
the vector of poses and collection of labels with the corresponding
pose $x_{t+1}$ and labels $l_{t+1}$, respectively. This model
resembles the pose graph representation commonly employed by SLAM
solutions~\cite{kaess08}.

Our goal is to induce a distribution over the semantic graph,
including the locations, topology, and semantic labels given
information about an environment obtained from a robot's range
sensors, odometry readings, and the user's descriptions of the
environment.
%
%
\begin{figure}
    \centering
    \includegraphics[width=0.9\linewidth]{./graphics/semantic_graph_pictorial}
    \caption{An example of a semantic graph.} \label{fig:semantic_graph_example}
\end{figure}
%
%

\subsection{Distribution Over Semantic Graphs}
%
We estimate a joint distribution over the topology $G_t$, the vector
of locations $X_t$, and the set of labels $L_t$. Formally, we maintain
this distribution over semantic graphs $\{G_t, X_t, L_t\}$ at time $t$
conditioned upon the history of metric exteroceptive sensor data $z^t
= \{z_1, z_2, \ldots, z_t\}$, odometry $u^t = \{u_1, u_2, \ldots,
u_t\}$, and natural language descriptions $\lambda^t = \{\lambda_1,
\lambda_2, \ldots, \lambda_t\}$:
%
%
\begin{equation} \label{eqn:distribution_w_labels}
    p(G_t, X_t, L_t \vert z^t, u^t, \lambda^t).
\end{equation}
%
%
%
%
\begin{table}[!t]
    \renewcommand{\tabularxcolumn}[1]{m{#1}}
    \renewcommand{\arraystretch}{1.3}
    \centering
    \caption{Semantic Graph Notation} \label{tab:notation2}
    \begin{tabularx}{1.0\columnwidth}{cX}
        \toprule
        Symbol & Description\\
        \midrule
        $G_t = (V_t, E_t)$ & Graph representation of the topology at
        time $t$ that consists of a set of vertices $V_t=\{v_1, v_2,
        \ldots, v_t\}$ connected by
        undirected edges $E_t$.\\
        \midrule
        % %% Prob can be described better 
        $L_t$ & Set of labels $l_{t,i}$ associated with each place.\\
        \midrule
        % % \hline
        $l_{t,j}^{(i)}$ & Label distribution for node $j$ in particle $i$.\\
        \midrule
        $\lambda_t$ & Parsed natural language description of the
        environment at time $t$.\\
        \midrule
        $X_t$ & Vector of landmark poses $[x_1,..., x_t]$\\
        \midrule
        $z^t$ & Set of sensor readings made up to time $t$ by sensors onboard
        the robot.\\
        \midrule
        $u^t$ & Set of odometry readings up to time $t$.\\
        \bottomrule
    \end{tabularx}
\end{table}
%%
%%

Each language variable $\lambda_i$ denotes a (possibly null)
utterance, such as ``This is the kitchen,'' or ``The gym is down the
hall.''  Table~\ref{tab:notation2} outlines our notation.  We factor
the joint posterior into a distribution over the graphs and a
conditional distribution over the node poses and labels,
%
%
\begin{multline} \label{eqn:factored_posterior}
     p(G_t, X_t, L_t \vert z^t, u^t, \lambda^t) = p(L_t \vert
     X_t, G_t, z^t, u^t, \lambda^t) \\
     \times p(X_t \vert G_t, z^t, u^t, \lambda^t) \times
       p(G_t \vert z^t, u^t, \lambda^t).
\end{multline}
%
%
The left-most expression in this factorization explicitly models the
dependence of the labels on the topology and the location of each
region. The middle term encodes the conditional distribution over the
metric map given the topology and, in this way, mimics pose graph
formulations to SLAM, given the loop closure (i.e., the topology). The
right-most expression denotes the distribution over the graph
conditioned upon the sensor history and language.

The space of possible graphs for a particular environment is spanned
by the allocation of edges between nodes. The number of edges,
however, can be exponential in the number of nodes. Hence, maintaining
the full distribution over graphs is intractable for all but trivially
small environments. To overcome this complexity, we assume as in
\citet{ranganathan11} that the distribution over graphs is dominated
by a small subset of topologies while the likelihood associated with
the majority of topologies is nearly zero. In general, this assumption
holds when the environment structure (e.g., indoor, man-made) or the
robot motion (e.g., exploration) limits
connectivity~\citep{ranganathan11}.  In addition, conditioning the
graph on the descriptions further increases the peakedness of
the distribution, thereby increasing the validity of this assumption,
because it decreases the probability of edges when the labels and
semantic relations are inconsistent with the language.

The assumption that the distribution is concentrated around a limited
set of topologies suggests the use of particle-based methods to
represent the posterior over graphs, $p(G_t \vert z^t, u^t,
\lambda^t)$. Inspired by the derivation of \citet{ranganathan11} for
topological SLAM, we employ Rao-Blackwellization to model the factored
formulation~\eqref{eqn:factored_posterior}, whereby we accompany the
sample-based distribution over graphs with analytic representations
for the conditional posteriors over the node locations and
labels. Specifically, we represent the posterior over the node poses
$p(X_t \vert G_t, z^t, u^t, \lambda^t)$ by a Gaussian, which we
parametrize in the canonical form. We maintain a Dirichlet
distribution that models the posterior distribution over the set of
node labels $p(L_t \vert X_t, G_t, z^t, u^t, \lambda^t)$.

We represent the joint distribution over the topology, node locations,
and labels as a set of particles
%
%
\begin{equation}
    \mathcal{P}_t = \{P_t^{(1)}, P_t^{(2)}, \ldots, P_t^{(n)}\}.
\end{equation}
%
%
Each particle $P_t^{(i)} \in \mathcal{P}_t$ consists of the set
%
%
\begin{equation}
     P_t^{(i)} = \left\{ G_t^{(i)}, X_t^{(i)}, L_t^{(i)}, w_t^{(i)} \right\},
\end{equation}
%
%
where $G_t^{(i)}$ denotes a sample from the space of graphs;
$X_t^{(i)}$ is the analytic distribution over locations; $L_t^{(i)}$
is the analytic distribution over labels; and $w_t^{(i)}$ is the
weight of particle $i$.



%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "ijrr2013"
%%% End: 
