\section{Introduction}
\footnotetext[1]{The first two authors contributed equally to this
  paper.}\addtocounter{footnote}{1}

Until recently, robots that operated outside the laboratory were
limited to controlled, prepared environments that explicitly prevent
interaction with humans. There is an increasing demand, however, for
robots that operate not as machines used in isolation, but as
co-inhabitants that assist people in a range of different
activities. If robots are to work effectively as our teammates, they
must become able to efficiently and flexibly interpret and carry out
our requests.
%
Recognizing this need, there has been increased focus on enabling
robots to interpret natural language commands~\citep{matuszek10,
  tellex11, dzifcak09, bugmann04, chen11}. This capability would, for
example, enable a first responder to direct a micro-aerial vehicle by
speaking ``fly up the stairs, proceed down the hall, and inspect the
second room on the right past the kitchen.'' A fundamental challenge
is to correctly associate linguistic elements from the command to a
robot's understanding of the external world.  We can alleviate this
challenge by developing robots that formulate knowledge
representations that model the higher-level semantic properties of
their environment.

%
%
\begin{figure}[!t]
    \centering
    \includegraphics[width=1.0\columnwidth]{./graphics/narrated_tour_wheelchair}
    \caption{A user gives a tour to a robotic wheelchair designed to
        assist residents in a long-term care facility.} \label{fig:tour_wheelchair}
\end{figure}
%
%
Semantic mapping~\citep{kuipers00, zender08, pronobis12} addresses
this need by providing robots with human-centric models of their
environment. Approaches often take as input low-level sensor (e.g.,
LIDAR, images) and odometry streams and infer metric, topological, and
semantic properties of the environment. Existing algorithms populate
the semantic map with scene attributes (e.g., room type) that can be
inferred from image- and LIDAR-based classifiers. However, general
purpose classifiers are unable to identify many useful properties of
an environment, such as the colloquial names associated with each
region. For example, it would be difficult to recognize and infer the
meaning of the question mark (Fig.~\ref{fig:question_mark}) that
indicates the location of the information desk at MIT's Stata Center,
which people frequently use as a reference point. Furthermore, the
dependence on onboard sensor streams prevents the algorithms from
reasoning about parts of the world that are outside the field-of-view
of these sensors. This has implications on the efficiency with which
robot's can learn human-centric representations of their environment.
%
%
\begin{figure}[!t]
    \centering
    \includegraphics[width=0.6\linewidth]{./graphics/stata_information_desk}
    \caption{General-purpose sensor-based classifiers would find it difficult
        to recognize the question mark that indicates the location of
        an information desk.} \label{fig:question_mark}
\end{figure}
%
%

We describe an approach first presented by the
authors~\citep{walter13} that enables robots to efficiently learn
human-centric models of the environment from a narrated, guided tour
(Fig.~\ref{fig:tour_wheelchair}) by fusing knowledge inferred from
natural language descriptions with conventional low-level sensor
data. Our method allows people to convey meaningful concepts,
including semantic labels and relations for both local and distant
regions of the environment, simply by speaking to the robot. The
advantage is that the robot can learn concepts that people are
arguably better-able to convey from its opportunistic interaction with
humans. The challenge lies in effectively combining these noisy,
disparate sources of information. A user's descriptions convey
concepts (e.g., ``the second room on the right'') that are ambiguous
with regard to their metric associations: they may refer to the region
that the robot currently occupies, to more distant parts of the
environment, or even to aspects of the environment that the robot will
never observe. In contrast, the sensors that robots commonly employ
for mapping, such as cameras and LIDARs, yield metric observations
arising only from the robot's immediate surroundings.

To handle ambiguity, we propose a representation referred to as the
\emph{semantic graph} that jointly combines metric, topological, and
semantic models of the environment. The metric layer takes the form of
a vector of poses for each region in the environment together with the
resulting occupancy-grid map that captures the perceived
structure. The topological layer consists of a graph in which nodes
correspond to reachable regions of the environment, and edges denote
pairwise spatial relations. The semantic layer contains the labels
with which people refer to regions. This knowledge representation is
well-suited to fusing concepts from a user's descriptions with the
robot's metric observations of its surroundings.

We estimate a joint distribution over the semantic, topological and
metric maps, conditioned on the language and the metric observations
from the robot's proprioceptive and exteroceptive sensors.  The space
of semantic graphs, however, increases combinatorially with the size
of the environment. We efficiently maintain the distribution using a
Rao-Blackwellized particle filter~\citep{doucet00} to track a factored
form of the joint distribution over semantic graphs. Specifically, we
approximate the marginal over the space of topologies with a set of
particles, and analytically model conditional distributions over
metric and semantic maps as Gaussian and Dirichlet, respectively.  The
algorithm updates these distributions iteratively over time using
descriptions and sensor measurements as they arrive.  We model the
likelihood of natural language utterances with the Generalized
Grounding Graph $(\textrm{G}^3)$ framework~\citep{tellex11}. Given a
description, the $\textrm{G}^3$ model induces a learned distribution
over semantic labels for the nodes in the semantic graph that we then
use to update the Dirichlet distribution. The algorithm uses the
resulting semantic distribution to propose modifications to the graph,
allowing semantic information to influence the metric and topological
layers.

This paper builds on our earlier work~\citep{walter13}, which presents
the initial semantic graph framework. We better place the
contributions of our method in the context of the current
state-of-the-art in semantic mapping and provide a more detailed
description of our estimation framework, including the means by which
we interpret natural language descriptions. Additionally, we describe
a new capability whereby the method reasons over and learns from
\emph{anticipatory} descriptions that refer to regions in the
environment not currently in the map. The user can then describe
locations that the robot may or may not have previously visited,
enabling the robot to more efficiently learn semantic maps of the
environment.

We evaluate our algorithm through six ``guided tour'' experiments that
take place within mixed indoor-outdoor environments. We show that, by
maintaining a joint distribution over the metric, topological, and
semantic maps, the algorithm learns models of the environment that are
richer and more accurate than can be achieved with existing
language-based semantic mapping algorithms. We analyze the
effectiveness with which the algorithm integrates semantic knowledge
from natural language descriptions and demonstrate the utility of the
learned maps for navigation.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "ijrr2013"
%%% End: 
