\section{Conclusion}
%
We have described an algorithm that estimates
metrically accurate semantic maps from a user's natural language
descriptions. The novelty lies in learning the joint distribution over
the metric, topological, and semantic properties of the environment,
which enables the method to fuse the robot's sensor stream with
knowledge inferred from the descriptions. We have presented results
from several experimental evaluations that demonstrate the algorithm's
ability to infer accurate metric, topological, and semantic
maps. However, there are several limitations to our current approach.

A known issue with sample-based methods such as ours is the problem
of particle depletion~\cite{doucet00} whereby a majority of samples
evolve to support regions of the distribution with negligible
likelihood. This results in a poor approximation to the target
distribution and can cause the filter to diverge. Resampling the
particles based upon a measure of the variance in their weights, as we
do, reduces the likelihood of particle depletion. In practice, we have
not found depletion to occur, as suggested by the results. We
partially attribute this to using the distribution over the semantic
map as part of the proposal, which reduces the
frequency of erroneous samples. Nonetheless, particle depletion may
occur and can be mitigated by adding additional particles
to hypothesize new topologies in the event that the distribution
appears to misrepresent the target distribution, for example, as
suggested by the particle weights~\citep{fearnhead03}.

Our method is capable of inferring semantic information only from the
user's descriptions. This means that the algorithm can only model a
region's label if it was specifically referenced by the user. Further,
it precludes the method from incorporating allocentric descriptions
for which the user never labels the landmark. For example, the
algorithm can not learn from the description ``The gym is down the
hall'' unless the user identifies the location of the hallway. Our
recent work~\citep{hemachandra14} alleviates this requirement by
also using geometric- and appearance-based scene classifiers
to infer semantic information from LIDAR and vision.

An additional limitation of the algorithm is that it treats a region's
colloquial name (e.g., ``Carrie's office'') and its type (``office'')
jointly as being labels. Thus, any subsequent reference to a labeled
region is required to use the label that was originally given. We have
recently extended our representation to model the type-name hierarchy
for each region, where we infer the type from the aforementioned scene
classifiers~\citep{hemachandra14}. 

The algorithm partions the environment by instantiating regions at a
fixed distance apart as the robot travels. This results in regions
that are not semantically meaningful, with multiple regions being used
to model the same area. Consequently, the algorithm may ground
language to the wrong node in the topology, either by inferring an
incorrect landmark or by diffusing labels across multiple nodes. Our
latest work~\citep{hemachandra14} employs spectral clustering to
segment the environment into regions based upon the consistency of
their local LIDAR scans, yielding regions that are more meaningful.

The information that we are currently able to infer from a user's
descriptions is limited to a region's colloquial name and its relation
to another region in the environment. It does not support a user's
ability to convey general properties of the environment, such as ``You can find
computers in offices,'' or ``nurses' stations tend to be
located near elevator lobbies.''

Our current framework was designed to learn a semantic map of an
environment from an initial tour, with the idea that this map can then
be used for localization, navigation, and grounding natural language
commands during long term operation. As with most pose graph
approaches to SLAM, our algorithm may add regions that duplicate the
same part of the environment, building maps that grow with time
rather than space. This is particularly undesirable when the robot
operates for extended periods of time within the same environment. We
have recently updated our representation so as to
reuse existing nodes in the graph,
updating their metric, topological, and semantic properties, rather
than adding new redundant nodes~\citep{hemachandra14}. 

In summary, we described an approach to learning human-centric maps of
an environment from user-provided natural language descriptions. The
novelty lies in fusing high-level information conveyed by a user's
speech with low-level observations from traditional sensors. By
jointly estimating the environment's metric, topological, and semantic
structure, we demonstrated that the algorithm yields accurate
representations of its environment.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "ijrr2013"
%%% End: 
