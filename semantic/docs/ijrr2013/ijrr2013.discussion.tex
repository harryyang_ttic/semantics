\section{Discussion}

\subsection{Learning from Allocentric, Anticipatory Language} \label{sec:grounding_discussion}

A contribution of our work is the use of natural language descriptions
to produce consistent semantic maps from spatial relations and labels
inferred from language. The advantage of this capability is that it
allows robots to more efficiently acquire human-centric maps of their
environment. The challenge to learning from these expressions is that
their groundings are ambiguous---the user may refer to regions that
may be distant from the robot and outside the field-of-view of its
sensors. Additionally, it may be that the descriptions are
anticipatory, when the robot has yet to visit the figure that the user
is describing or the landmark that they are referencing.
%
%
\begin{figure*}[!tb]
    \centering
    \subfigure[Anticipatory Description
    Given]{\includegraphics[width=0.325\linewidth]{./graphics/language_complex/stata_36_38_sixth_floor/before_new}\label{fig:stata_third_floor_before}}\hfil
    \subfigure[Landmark is Added]{\includegraphics[width=0.325\linewidth]{./graphics/language_complex/stata_36_38_sixth_floor/landmark_new}\label{fig:stata_third_floor_landmark}}\hfil
    \subfigure[Figure is Grounded]{\includegraphics[width=0.325\linewidth]{./graphics/language_complex/stata_36_38_sixth_floor/figure_new}\label{fig:stata_third_floor_figure}}\hfil
    \caption{A depiction of the process of learning from an
      anticipatory description. \subref{fig:stata_third_floor_before}
      The user describes the ``lobby'' as being ``down the hallway,''
      yet the hallway has not been labeled and there is no node for
      the elevator lobby in the
      topology. \subref{fig:stata_third_floor_landmark} The user
      labels the current region as the ``hallway,'' providing the
      landmark location. \subref{fig:stata_third_floor_figure} Once
      nodes are added that are consistent with the description, the
      algorithm updates the labels. The green box indicates the actual
      location of the
      lobby.} \label{fig:stata_third_floor_before_after}
\end{figure*}
%
%
Figure~\ref{fig:stata_third_floor_before_after} depicts the process of
learning from an anticipatory description as part of the Stata Center
lab tour
(Fig.~\ref{fig:stata_third_floor}). Figure~\ref{fig:stata_third_floor_before}
shows the robot traversing a hallway when the user states that ``The
elevator lobby is down the hallway.'' At this point, the semantic
graph includes several nodes with a high likelihood of having the
label ``hallway.'' However, the robot has yet to visit the specific
hallway that the person is using as the landmark and, as a result, the
semantic graph does not include nodes for this region. The graph also
lacks nodes for the region that the user refers to as the ``elevator
lobby.'' The algorithm attempts to ground the description using the
language model as described in Section~\ref{sec:update_language},
which yields a likelihood for each pair of nodes as being the landmark
and the figure. 
% This calculation (the term inside the summation in
% Equation.~\ref{eqn:landmark_likelihood}) includes the likelihood of
% the label used in the description (``hallway'') under the hypothesized
% landmark's label distribution and the likelihood that the node poses
% are consistent with the spatial relation (``down''). We evaluate the
% latter by first solving for the shortest path through the known map
% that starts at the robot's pose at the time of the utterance and
% passes through the landmark region to the figure. We treat this path
% as the grounding $\phi_i$ for the spatial relation and evaluate its
% likelihood according to the discriminative language
% model~\eqref{eqn:g3_factors}.

This algorithm performs this grounding process for each particle, and
updates those for which the likelihood of the top pair is sufficiently
high ($0.2$). In this example, the likelihood of the candidate
groundings for most of the particles is low and the algorithm
postpones language integration. As the tour proceeds
(Fig.~\ref{fig:stata_third_floor_landmark}), the guide labels the
robot's position as being the ``hallway,'' which updates the label
distribution for the adjacent node. The algorithm again attempts to
ground the language, this time using the newly added hallway nodes as
the landmark. However, paths that start at the pose from which the
description was first given and pass through the landmark to other
nodes do not resemble the learned model for the ``down''
relation. After the robot and user continue and more nodes are added
to the topology (Fig.~\ref{fig:stata_third_floor_figure}), the
framework again attempts to ground the description, this time
returning highly-confident estimates for the locations of the landmark
and the figure, per the induced path. However, not all of the inferred
locations are correct, which is consistent with what we see with other
allocentric expressions. In this case, the system assigns ``elevator
lobby'' labels to nodes that preceded the hallway as well as several
nodes beyond the true location of the lobby (green box). We attribute
this to the difficulty in dealing with frame-of-reference when
grounding language as well as to using features for the ``down''
relation that attempt to accommodate a wide range of scales (i.e. the
length of hallways differs significantly across the environments that
we consider).

In an effort to better understand the accuracy with which the
algorithm learns from environment descriptions, we consider regions
whose semantic properties were inferred from allocentric
utterances. Figure~\ref{fig:32_36_38_insets} presents close-up views
of the regions that were labeled as part of the multi-building tour
(Fig.~\ref{fig:32_36_38_graph}). The portion of the semantic graph
shown in Figure~\ref{fig:32_36_38_inset_1_2} results from two
descriptions, ``The lobby is down the hallway'' and ``The elevator
lobby is down the hallway,'' which were uttered at the locations
indicated by the numbers ``1'' and ``2,'' respectively. The former
utterance was anticipatory as the robot had not yet visited the lobby
area when the description was given. Nonetheless, the framework
successfully labels that region of the environment when the robot
later visits it, without any aliasing effects. However, grounding the
second utterance results in high likelihoods associated with some
nodes that are not actually in the elevator lobby, causing the label
to ``bleed'' into other areas. We attribute this to the ambiguity that
results from not reasoning over frame-of-reference without which the
nodes are consistent with being ``down'' the hallway. The performance
improves for the anticipatory utterance in
Figure~\ref{fig:32_36_38_inset_4} where the algorithm waits to infer
the location of the lab until it is visited. We see similar effects
for the descriptions in Figure~\ref{fig:32_36_38_inset_3_5} where the
system correctly infers the location of another elevator lobby but
attributes the ``office'' label to nodes that are actually in a
hallway. This results from a simple set of features that encode the
``near'' relation based upon distance. Additionally, our algorithm
uses a fixed separation to define regions and does not reason over
their geometry (e.g., the shape of hallways is typically distinct from
that of offices.)  Meanwhile,
Figure~\ref{fig:stata_1_long_tour_inset_2} depicts the semantic
information inferred for the utterance ``The lobby is through the
entrance'' from the large indoor/outdoor tour where we see that the
algorithm correctly grounds the location of the lobby without any
aliasing.

\subsection{Navigation}

A consequence of maintaining a joint distribution over each layer of 
the semantic graph is that the framework is able to use knowledge of
the semantic properties of the environment to update the topology and
metric map. This improves the accuracy of the resulting semantic graph
and, in turn, facilitates navigation. To better understand the effects
on navigation efficiency, we consider the task of finding the optimal path
between two nodes in the topology, as if the robot were asked to use
the semantic graph to navigate from its current location to a named
region in the environment.

We examine the semantic graphs that we learned with and without
language-based constraints for the two indoor/outdoor scenarios, the
autonomous tour, and the Killian Court dataset. For each, we randomly
picked 1000 pairs of start and goal nodes in the graph and used a
graph search algorithm to find the shortest path through the topology,
with equal cost for each edge in the graph. The same node pairs were
used for each of the semantic graphs for a given
environment. Table~\ref{tab:navigation} compares the average optimal
path length through the graphs that result from our method and the
baseline, which does not infer constraints from the descriptions.  The
graphs that we estimate when language influences only the semantic
layer give rise to optimal paths that are noticeably longer than the
paths reflected in the graphs that we learn by jointly estimating the
semantic graph. This difference stems from the fact that our
representation provides semantic-based edges that allow the planner to
identify shortcuts in the topology that are otherwise not suggested by
the baseline map, which mimics the current state-of-the-art in
language-augmented semantic mapping.
%
%
\begin{table}[!tb]
    % \renewcommand{\tabularxcolumn}[1]{m{#1}}
    \renewcommand{\arraystretch}{1.3}
    \centering
    \caption{Average Length of the Optimal Path} \label{tab:navigation}
    \begin{tabular}{@{}rrr@{}}\toprule
        Experiment & Baseline & SG  \\
        \midrule
        Small Indoor/Outdoor & 41.59\,m & 23.50\,m\\
        Large Indoor/Outdoor & 68.14\,m  & 35.52\,m\\
        Autonomous &   43.49\,m  & 25.70\,m\\
        Killian Court & 63.08\,m  & 40.76\,m\\
        \bottomrule
    \end{tabular}
\end{table}

\subsection{Possibility of aliasing with language}
%
%
\begin{figure*}[!tb]
    \centering
    \subfigure[Incorrect loop closure added]{\includegraphics[width=0.8\linewidth]{./graphics/language_aliasing/wrong_particle}\label{fig:wrong_particle}}\\
    \subfigure[No incorrect loop closures]{\includegraphics[width=0.8\linewidth]{./graphics/language_aliasing/correct_particle}\label{fig:correct_particle}}
    \caption{A demonstration of the effects of perceptual aliasing for
      the three building tour (Fig.~\ref{fig:32_36_38_graph}) in which
      \subref{fig:wrong_particle} the algorithm accepts an invalid
      edge between different regions that have similar
      geometry for one particle. However, the majority of the particles did not propose
      erroneous edges and the weight of this map soon decreases to
      $1/10^{th}$ of that of the correct particle and is removed upon
      resampling.} \label{fig:language_aliasing}
\end{figure*}
%
%

When proposing edges to the topology based upon the label
distributions, we perform exhaustive scan-matching to check the
validity of each proposed loop closure. While this helps to filter out
the large majority of erroneous edges, the matching may yield false
positives in regions that are perceptually aliased
(Fig.~\ref{fig:language_aliasing}). However, since the hypothesis
space of potential language edges is large, the likelihood that all
particles sample invalid edges is low, confining such occurrences to a
small subset of particles. Empirically, we have found that the weight
of these particles is quickly reduced as their metric maps are
inconsistent with subsequent sensor measurements. These particles then
tend to be removed during resampling.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "ijrr2013"
%%% End: 
