\section{Building Semantic Maps with Language}

Algorithm~\ref{alg:algorithm} outlines the process by which we
recursively update the distribution over semantic
graphs~\eqref{eqn:factored_posterior} to reflect the latest robot
motion, metric sensor data, and utterances.  In the first step, we
propagate each sample $G_{t-1}^{(i)}$, which represents the posterior
$p(G_{t-1} \vert z^{t-1}, u^{t-1}, \lambda^{t-1})$ at time $t-1$, by
adding a node for the robot's new pose (connected by an edge to the
previous node) and proposing additional loop-closure edges according
to the current metric and label distributions. This results in a
sample-based estimate for the prior at time $t$, $p(G_t \vert z^{t-1},
u^t, \lambda^t)$. Next, we update the Gaussian distribution over the
node poses by incorporating the constraints induced by the new
loop-closure edges. We then proceed to update the Dirichlet
distributions based upon the structure of the graph and parsed
language $\lambda_t$, if available. Finally, we update the weight
$w_t^{(i)}$ according to the likelihood of new metric measurements
$z_t$ and resample if needed. We repeat these steps for each particle,
yielding the particle set representation $\mathcal{P}_t$ of the new
posterior distribution at time $t$, $p(G_t, X_t, L_t \vert z^t, u^t,
\lambda^t)$.  The following sections explain each step in detail.
%
%
\begin{algorithm}[t]
    \DontPrintSemicolon
    \KwIn{$P_{t-1} =\left\{P_{t-1}^{(i)}\right\}$, and $\left(u_t, z_t,\lambda_t\right)$, 
        where $P_{t-1}^{(i)} = \left\{ G_{t-1}^{(i)}, X_{t-1}^{(i)}, L_{t-1}^{(i)}, w_{t-1}^{(i)} \right\}$}
    \BlankLine
    \KwOut{$P_t = \left\{P_t^{(i)}\right\}$}
    %
    \BlankLine
    \For{ $i = 1$ to $n$} {
        % This is a hack to prevent the enumerated list from extending
        % beyond the width of the column. It will likely need to be
        % changed if the column changes.
        \begin{enumerate}[rightmargin=\dimexpr\linewidth-7.25cm-\leftmargin\relax]
        \item Propagate the graph sample $G_{t-1}^{(i)}$ using the
            proposal distribution $p(G_t \lvert G_{t-1}^{(i)},z^{t-1},
            u^t, \lambda^t)$, using odometry $u_t$ and current
            distributions over labels $L_{t-1}^{(i)}$ and poses $X_{t-1}^{(i)}$.\;
            % Employ proposal distribution $p(G_t \lvert G_{t-1}^{(i)},
            % z^{t-1}, u^t, \lambda^t)$ to propagate the graph sample
            % $G_{t-1}^{(i)}$ according to odometry $u_t$ and current
            % distributions over labels $L_{t-1}^{(i)}$ and poses $X_{t-1}^{(i)}$.\;
        \item Update the Gaussian distribution over the node poses
            $X_t^{(i)}$ according to the constraints induced \\ by the
            newly-added graph edges.\;
        \item Update the Dirichlet distribution over the current and
            adjacent nodes $L_t^{(i)}$ according to the language %parsed language
            $\lambda_t$.\;
        \item Compute the new particle weight $w_t^{(i)}$ based upon the
            previous weight $w_{t-1}^{(i)}$ and the metric data $z_t$.\; 
        \end{enumerate}
    }
    \BlankLine
     Normalize weights and resample if needed.
%
\caption{Semantic Mapping Algorithm} 
\label{alg:algorithm}
\end{algorithm}
%
%

\subsection{Graph Augmentation using the Proposal Distribution}

Given the posterior distribution over the semantic graph at time
$t-1$, we first compute the prior distribution over the graph
$G_t$. We do so by sampling from a proposal distribution that
is the predictive prior of the current graph given the previous graph
and sensor data, and the recent odometry and language:
%
%
\begin{equation} \label{eqn:proposal2}
    p(G_t \vert G_{t-1}, z^{t-1}, u^t, \lambda^t)
\end{equation}
%
%
We formulate the proposal distribution by first augmenting the graph
to reflect the robot's motion. Specifically, we add a node $v_t$ to
the graph that corresponds to the robot's current pose with an edge to
the previous node $v_{t-1}$ that represents the temporal constraint
between the two poses. We denote this intermediate graph as
$G_t^-$. Similarly, we add the new pose as predicted by the robot's
motion model to the vector of poses $X_t^-$ and the node's label to
the label vector $L_t^-$ according to the process described in
Subsection~\ref{sec:update_language}.\footnote{The label update
  explains the presence of the latest language $\lambda_t$.}

We formulate the proposal distribution~\eqref{eqn:proposal2} in terms
of the likelihood of adding edges between nodes in this modified graph
$G_t^-$. The system considers two forms of additional edges: first,
those suggested by the spatial distribution of nodes and second, by
the semantic distribution for each node.

\subsubsection{Spatial Distribution-based Constraints} \label{sec:spatial_constraints}
%
We first propose connections between the robot's current node $v_t$
and others in the graph based upon their metric location. We do so by
sampling from a distance-based proposal distribution biased towards
nodes that are spatially close. Doing so requires marginalizing over
the distances $d_t$ between node pairs, as shown in
equation~\eqref{eqn:distance_proposal}, where we omit the history of
language observations $\lambda^t$, metric measurements $z^{t-1}$, and
odometry $u^t$ for
brevity. Equation~\eqref{eqn:distance_proposal_conditional} reflects
the assumption that additional edges expressing constraints involving
the current node $e_{tj} \notin E^-$ are conditionally
independent. Equation~\eqref{eqn:distance_proposal_marginal}
approximates the marginal in terms of the distance between the two
nodes associated with the additional edge.
%
%
\begin{subequations} 
    \label{eqn:distance_proposal}
    \begin{align}
        p_a(G_t \vert &G^-_t, z^{t-1}, u^t, \lambda^t)%\notag\\    
        = \prod_{j:e_{tj} \notin E^-}  p(G_t^{tj} \vert G_t^-)
        \label{eqn:distance_proposal_conditional}\\
        &= \prod_{j:e_{tj} \notin E^-} \int_{X^-_t} p(G_t^{tj}  \vert X_t^-,
        G_t^-) p(X_t^- \vert G_t^-)\\        
        &\approx \prod_{j:e_{tj} \notin E^-} \int_{d_{tj}} p(G_t^{tj} \vert
        d_{tj}, G_t^-) p(d_{tj} \vert G_t^-),
        \label{eqn:distance_proposal_marginal}
    \end{align}
\end{subequations} 
%
%

The conditional distribution $p(G_t^{tj}\vert d_{tj}, G_t^-, z^{t-1},
u^t, \lambda^t)$ expresses the likelihood of adding an edge between nodes $v_t$
and $v_j$ based upon their spatial location. We represent the
distribution for a particular edge between vertices $v_i$ and $v_j$ a
distance $d_{ij} = \lvert x_i - x_j \rvert_2$ apart as
%
%
\begin{equation}
    p(G_t^{ij} \vert d_{ij}, G_t^-, z^{t-1}, u^t, \lambda^t) \propto \frac{1}{1+\gamma
d_{ij}^2},
\end{equation}
%
%
where $\gamma$ specifies distance bias. For the evaluations in this
paper, we use $\gamma = 0.2$. We approximate the distance prior
$p(d_{tj} \vert G_t^-, z^{t-1}, u^t, \lambda^t)$ with a folded Gaussian
distribution,
%
% 
\begin{multline}
    p (d_{ij}; \mu, \sigma) = \frac{1}{\sigma\sqrt{2
            \pi}}\exp\left(-\frac{(-d_{ij}-\mu)^2}{2\sigma^2}\right) 
    \\+ 
    \frac{1}{\sigma\sqrt{2 \pi}}\exp\left(-\frac{(d_{ij}-\mu)^2}{2\sigma^2}\right)   
\end{multline}
%
%
where $\mu$ is the the mean and $\sigma$ is the standard deviation,
approximated based upon a linearized model for the distance between
the normally distributed positions $x_i$ and $x_j$. The
probability is 0 for $d_{ij} < 0$.

The algorithm samples from the proposal
distribution~\eqref{eqn:distance_proposal} to identify candidate
edges. Before adding these to the graph, we use laser scans to build
local maps around each node and compare the maps associated with the
two nodes using scan-matching
(Fig.~\ref{fig:edge_proposal_distance}). This matching allows the
method to reject most invalid edges, however it may still yield false
positives for areas with ambiguous local geometry. In order to reduce
the effects of this perceptual aliasing, we evaluate the likelihood of
the scan-matched estimates of the inter-region transformations under
our distribution over the metric map. The algorithm retains edges
according to their Mahalanobis distance and adds edges deemed to be
valid along with their estimated transformations.
%
%
\begin{figure}[!tb]
    \centering
    \subfigure[Rejected Edges]{\includegraphics[width=0.49\linewidth]{./graphics/edge_proposals_failure_1} \label{fig:edge_proposal_distance_rejected}}\hfil%
    \subfigure[Rejected and Accepted Edges]{\includegraphics[width=0.49\linewidth]{./graphics/edge_proposals_success} \label{fig:edge_proposal_distance_accepted}}
    \caption{In the proposal step, the algorithm hypothesizes the
      addition of new edges in the graph based upon the estimated
      distance between nodes. Candidate edges are
      \subref{fig:edge_proposal_distance_rejected} rejected (black) or
      \subref{fig:edge_proposal_distance_accepted} accepted (red)
      based upon scan-matching.} \label{fig:edge_proposal_distance}
\end{figure}
%
%

\subsubsection{Semantic Map-based Constraints}
%
A fundamental contribution of our method is the ability for the
semantic map to influence the metric and topological maps. This
capability results from the use of the label distributions to perform
place recognition. The algorithm identifies loop closures by sampling
from a proposal distribution that expresses the semantic similarity
between nodes. In similar fashion to the spatial distance-based
proposal, computing the proposal requires marginalizing over the space
of labels:
%
%
\begin{subequations} \label{eqn:semantic_edge_proposal}
    \begin{align} 
        p_s(&G_t \vert G^-_t, z^{t-1}, u^t, \lambda^t)% \nonumber \\    
        = \prod_{j:e_{tj} \notin E^-} p(G_t^{tj} \vert G_t^-,
        \lambda_t)\\
        &= \prod_{j:e_{tj} \notin E^-} \sum_{L_t^-} p(G_t^{tj} \vert
        L_t^-, G_t^-, \lambda_t)p(L_t^- \vert G_t^-)\\
        &\approx \prod_{j:e_{tj} \notin E^-} \sum_{l_t^-,l_j^-}
        p(G_t^{tj} \vert l_t^-,l_j^-, G_t^-) p(l_t^-,l_j^- \vert
        G_t^-), \label{eqn:semantic_edge_proposal_c}
    \end{align}
\end{subequations}
%
%
where we have omitted the metric, odometry, and language inputs for
clarity.  The first line follows from the assumption that additional
edges that express constraints to the current node $e_{tj} \notin E^-$
are conditionally independent. The second line represents the
marginalization over the space of labels, while the last line results
from the assumption that the semantic edge likelihoods depend only on
the labels for the vertex pair. We model the likelihood of edges
between two nodes as non-zero for the same label
%
%
\begin{figure}[!t] 
  \centering
  \includegraphics[width=\linewidth]{./graphics/language_edges_example}%new_images/same_label_rejections_new}
  \caption{The algorithm proposes new graph edges between node pairs
      based on their label distributions, which are depicted as pie
      charts for nodes whose distribution is not uniform. It rejects invalid
      edges that result from ambiguous labels (black) and adds the
      edge (green) that denotes a valid loop closure.}
  \label{fig:rejected_lang_proposals}
\end{figure} 
%
%
\begin{equation} \label{semantic_edge_prob}
    p(G_t^{tj} \vert l_t,l_j) = \begin{cases}\theta_{l_t} &\mbox{if } l_t=l_j \\
        0 & \mbox{if } l_t \neq l_j \end{cases} 
\end{equation} 
%
%
where $\theta_{l_t}$ denotes the label-dependent likelihood that edges
exist between nodes with the same label. In practice, we assume a
uniform saliency prior for each
label. Equation~\eqref{eqn:semantic_edge_proposal_c} then measures the
cosine similarity between the label distributions.

We sample from the proposal
distribution~\eqref{eqn:semantic_edge_proposal} to hypothesize new
semantic map-based edges. As with distance-based edges, we validate
proposed edges by building local maps for each region and performing
scan-matching between these maps. In practice, we additionally
introduce a bias that penalizes matches between frequently occurring
regions like hallways. Figure~\ref{fig:rejected_lang_proposals} shows
several different edges sampled from the proposal distribution at one
stage of a tour.\footnote{Throughout the paper, we only visualize the
  semantic distribution for nodes whose distribution is not uniform.}
Here, the algorithm identifies candidate loop closures between
different ``entrances'' in the environment and accepts those (shown in
green) whose local laser scans are consistent. Note that some
particles may add invalid edges (e.g., due to perceptual or semantic aliasing),
but their weights will decrease as subsequent measurements become
inconsistent with the hypothesis.


\subsection{Updating the Metric Map Based on New Edges}
%
The proposal step results in the addition, to each particle, of a new
node at the current robot pose, along with an edge representing its
temporal relationship to the previous node. The proposal step also
hypothesizes additional loop-closure edges. Next, the algorithm
incorporates these relative pose constraints into the Gaussian
representation for the marginal distribution over the map
%
%
\begin{equation}
    p(X_t \vert G_t, z^t, u^t, \lambda^t) = \mathcal{N}^{-1}(X_t;
    \Sigma_t^{-1}, \eta_t),
\end{equation}
%
%
where $\Sigma_t^{-1}$ and $\eta_t$ are the information (inverse
covariance) matrix and information vector that parametrize the
canonical form of the Gaussian. We utilize the iSAM
algorithm~\citep{kaess08} to update the canonical form by iteratively
solving for the QR factorization of the information matrix. We omit
the details of the algorithm for lack of space and refer the reader to
\citet{kaess08} for more
information. Figure~\ref{fig:metric_map_w_occupancy} shows the
resulting metric poses and their uncertainties.
%
%
\begin{figure}[!ht]
   \centering
   \includegraphics[width=\linewidth]{./graphics/metric_covariance}
   \caption{The mean position and $1\sigma$ uncertainty ellipse for each node,
       along with the resulting occupancy grid map.}\label{fig:metric_map_w_occupancy}
\end{figure}
%
%
\subsection{Updating the Semantic Map Based on Natural Language} \label{sec:update_language}
%

Next, the algorithm updates the distribution over the current set of
labels $L_t = \{l_{t,1}, l_{t,2}, \ldots, l_{t,t}\}$ associated with
each particle. This update reflects information regarding labels and
spatial relations that spoken descriptions convey, as well as semantic
concepts that are suggested by the addition of edges to the graph. In
maintaining the label distribution, we make the assumption that the
node labels are conditionally independent given the topology and node
poses
%
%
\begin{equation}
    p(L_t \vert X_t, G_t, z^t, u^t, \lambda^t) = \prod_{i=1}^{t} p(l_{t,i}
    \vert X_t, G_t, z^t, u^t, \lambda^t).
\end{equation}
%
%
This assumption ignores dependencies between labels associated with
nearby nodes, but simplifies the form for the distribution over labels
associated with a single node.  We model each node's label
distribution as a Dirichlet distribution of the form
%
%
\begin{align}
    p(l_{t,i} | \lambda_1 \dots \lambda_t) &= Dir(l_{t,i}; \alpha_1
    \dots \alpha_K)\nonumber \\
    &= \frac{\Gamma(\sum_1^K\alpha_i)}{\Gamma(\alpha_1) \times \ldots \times
        \Gamma(\alpha_K)} \prod _ {k=1}^K l_{t,i,k}^{\alpha_k - 1},
\end{align}
%
%
where $l_{t,i,k}$ for $k \in \{1,\ldots,K\}$ is the $k^\textrm{th}$
label associated with node $i$ at time $t$.
We initialize the parameters $\alpha_1 \ldots \alpha_K$ to $0.2$,
which results in a prior that is uniform over the different
labels. Given subsequent language input, this favors
distributions that are peaked around a single label.
%
%
\begin{figure}[!tb]
    \centering
    \includegraphics[width=0.8\linewidth]{./graphics/gym_example_map}
    \caption{The user utters the description ``The gym is down the
        hall'' when the robot is at the location indicated by the
        triangle.} \label{fig:gym_example_map}
\end{figure}
%
%

We consider user-provided expressions that use spatial relations to
describe one or two locations in the environment. The first form are
\emph{egocentric} utterances (e.g., ``This is the gym'') that assign
labels to the robot's current location.  A contribution of our work is
the ability to incorporate information from \emph{allocentric} spatial
language that express spatial relations and labels that are associated
with non-local, potentially distant regions in the environment. By
interpreting these expressions, such as ``The kitchen is through the
cafeteria,'' our framework enables robots to learn rich semantic maps
of their environment more efficiently.

Learning from allocentric expressions is challenging because their
groundings are ambiguous---the places to which the user refers are
often not obvious. Consider the scenario outlined in
Figure~\ref{fig:gym_example_map}. The semantic map includes an area
that has a high likelihood of being a ``lobby'' and a second believed
to be a ``hallway.'' As the robot (triangle) continues to explore the
environment, the user utters the description ``The gym is down the
hall.'' Descriptions like these are often ambiguous. For example,
there may be multiple ``hall'' regions in the map or it may be that
the robot has yet to visit the region that the user is referring to,
or if it has, it is not aware of its label. Similarly, several
regions in the map are candidates for being the ``gym,'' but the user
may also be identifying a region that is not yet in the map.

In order to understand an expression like ``The gym is down the
hall,'' the system must first ground the landmark phrase ``the hall''
to a specific entity in the environment.  It must then infer an entity
in the environment that corresponds to the word ``the gym.''  One can
no longer assume that the user is referring to the current location as
``the gym'' (the \emph{figure}\footnote{In spatial linguistic theory,
  this is often referred to as the \emph{trajector}.}) or that the
location of the ``hall'' (the \emph{landmark}) is known (e.g., there
are likely many ``halls'' in the environment). We use the label
distribution to reason over the possible nodes that denote the
landmark. In doing so, we make the additional assumption that the
landmark exists in the graph and normalize the likelihoods for
candidate ``hall'' nodes. We later relax this assumption as we
describe shortly. We account for the uncertainty in the figure by
formulating a distribution over the nodes in the topology that
expresses their likelihood of being the figure.  Formally, we model
the likelihood that each node $v_i$ is the figure by marginalizing
over the space of candidate landmarks
%
%
\begin{equation} \label{eqn:landmark_likelihood}
    p(\phi_{v_i}^f = {\tt T}) = \sum\limits_{v_j}
    p(\phi_{v_i}^f = {\tt T} \vert \phi_{v_j}^l = {\tt T})\, p
(\phi_{v_j}^l = {\tt T}),
\end{equation}
%
%
where $\phi_{v_i}^l$ and $\phi_{v_i}^f$ are binary-valued random
variables that indicate that node $v_i$ is the landmark and figure,
respectively. The landmark likelihood $p(\phi_{v_j}^l = {\tt T})$
follows from the normalized label distributions, as described
above. We arrive at the conditional distribution $p(\phi_{v_i}^f =
{\tt T} \vert \phi_{v_j}^l = {\tt T})$ using the \G3 framework to
infer groundings for the different parts of the description. In the
case of this example, the framework induces a probability distribution
over nodes whose location is consistent with being ``down the hall''
from each of the conditioned landmark nodes, based upon the robot's
pose at the time the user offers the communication. In this manner, we
make the assumption that the person is describing the environment in
the robot's frame of reference. The grounding
likelihood~\eqref{eqn:landmark_likelihood} simplifies with egocentric
language as the figure is implicitly the robot's current location.

\subsubsection{Grounding Natural Language Descriptions with \G3}\label{sec:grounding_language}

Before proceeding, we briefly describe the \G3 algorithm, which was
initially proposed by \citet{tellex11}. Given natural language text
$\Lambda$, \G3 provides a distribution over the space of possible
mappings between each word in the parsed description and the
corresponding groundings in the external model. This distribution
takes the general form
%
%
\begin{equation} \label{eqn:g3_general}
    p(\Phi \vert \Gamma, \Lambda, M),
\end{equation}
%
%
where $\Gamma = \{\gamma_1, \ldots, \gamma_n\}$ denotes the set of
possible groundings and $M$ represents the robot's world model, which
includes the robot's pose and a map of the environment. The
correspondence variable $\Phi$ contains boolean-valued variables
$\phi$ for each linguistic element $\lambda \in \Lambda$ and grounding
$\gamma \in \Gamma$, such that $\phi = {\tt True}$ iff $\gamma$
corresponds to $\lambda$. In our application, the groundings are the
locations of the nodes in the semantic graph the paths between nodes
according to the metric map.

Taking advantage of the compositional, hierarchical structure of
natural language~\citep{jackendoff85}, \G3 parses the utterance into a
set of Spatial Description Clauses (SDCs). Each SDC is assigned a type
(event, object, place, or path) and consists of landmark
$\lambda_i^l$, figure $\lambda_i^f$, and relation $\lambda_i^r$
phrases. For the purposes of this work, we parse descriptions into
place and path SDCs using a learned grammar that includes possible
labels and spatial relations. \G3 then factors the
distribution~\eqref{eqn:g3_general} into individual terms, one for
each linguistic element
%
%
\begin{equation} \label{eqn:g3_factored}
    p(\Phi \vert \Gamma, \Lambda, M) = \prod_i p(\phi_i \vert
    \lambda_i, \Gamma, M).
\end{equation}
%
%
\begin{figure}[!tb]
    \centering
    \includegraphics[width=0.8\linewidth]{./graphics/gym_example_factor_graph}
    \caption{The factor graph model for the utterance ``The gym is
        down the hall'' that is used by the \G3 algorithm.} \label{fig:gym_example_factor_graph}
\end{figure}
%
%
This factored distribution is represented as a graphical model using a
factor graph, such as the one shown in
Figure~\ref{fig:gym_example_factor_graph} for the ``the gym is down
the hall'' utterance. The \G3 algorithm uses a log-linear model for
each of the factors
%
%
\begin{equation} \label{eqn:g3_factors}
    p(\phi_i \vert \lambda_i, \Gamma, M) \propto \exp \left(
        \sum\limits_j \mu_j s_j(\phi_i, \lambda_i, \Gamma, M) \right),
\end{equation}
%
%
where $\mu_j$ are weights and $s_j$ are features that encode the
relationship between the linguistic element $\lambda_i$ and the
groundings $\Gamma$. For example, we use a feature that relates the
length of the path through the map from the landmark grounding
$\gamma_i^l$ and figure grounding $\gamma_i^f$ when the relation
$\lambda_i^r$ is ``down from''
%
%
\begin{equation} \label{eqn:g3_feature}
    s (\gamma_i^l, \gamma_i^f, \lambda_i^r) \triangleq \lvert
    x_{\gamma_i^l} - x_{\gamma_i^f} \rvert \wedge (\textrm{``down
        from''} \in \lambda_i^r).
\end{equation}
%
Similarly, features for other relations express the consistency of the
path between pairs of nodes with the uttered relation. The set of
relations for which we have trained feature weights include
``through,'' ``down from,'' ``near,'' and ``away from.''  Additional
features include the likelihood of the landmark label $\lambda_i^l$
under the multinomial associated with the node's $\gamma_i^l$ label
distribution.
            
The \G3 model learns the weights $\mu_k$ associated with each feature
by training on a corpus of SDCs from natural language descriptions and
the known groundings $\Gamma$ and correspondences $\Phi$. In
particular, we train our \G3 model using a route directions
corpus~\citep{kollar10} that includes a set spoken directions through
an office building and positive and negative examples of paths through
the environment.

Given a particular spoken description, we use \G3 to infer groundings
for the different parts of the utterance. In the case of the current
example, the framework uses the multinomial distributions over labels
to find a node corresponding to the ``hall'' landmark and induces a
probability distribution over ``gyms'' based on the nodes that are
``down the hall'' from the identified landmark nodes. We ground
relational utterances $\lambda_i^r$ by considering the shortest path
that travels from the robot's pose at the time of the description
through the pair of landmark $\gamma_i^l$ and figure $\gamma_i^f$ node
groundings. We use the A$^*$ algorithm~\citep{russell03} to solve for
the shortest path through the semantic graph topology. We then use
features over these paths~\eqref{eqn:g3_feature} to evaluate their
consistency with the uttered relation (e.g., ``down from,'' ``near,''
and ``through'').  The likelihood of this path is calculated for each
possible figure and landmark pair. We marginalize out the landmarks to
arrive at the likelihood of the figure region having the described
label
%
%
\begin{equation}
    p(f_j) = \sum_{l_i}{p(\phi_j| f_j,l_i, p_j) p(l_i)},
\end{equation}
%
%
where $f_j$ is the figure being evaluated, $p_j$ is the path from the
robot's location at the time of the description to the figure,
$\phi_j$ is the corresponding likelihood of the grounding, and $l_i$
is a corresponding landmark.

For both types of expressions, the algorithm updates the semantic
distribution according to the rule
%
%
\begin{equation}
    \begin{split}
    p(&l_{t,i} | \lambda_t = (k, i), l_{t-1,i}) = \\
    &\tfrac{\Gamma(\sum_1^K\alpha_i^{t-1} + \Delta \alpha)}
    {\Gamma(\alpha_1^{t-1}) \times \ldots \times \Gamma(\alpha_k^{t-1}
        + \Delta \alpha) \times \ldots \times \Gamma(\alpha_K)}  \prod _ {k=1}^K l_{t,i,k}^{\alpha_k - 1},
\end{split}
\end{equation}
%
%
where $\Delta \alpha$ is the likelihood of the figure grounding.  In
the case of egocentric language, when the robot's position is
implicitly the figure, we set this likelihood to \mbox{$\Delta
  \alpha=1$} for the current node in the graph. When the descriptions
are ambiguous, we set $\Delta \alpha$ to the landmark likelihood
computed via Equation~\ref{eqn:landmark_likelihood}.

An advantage of having a probabilistic model over the space of
groundings is that it provides a means of recognizing when there is
not enough information contained in the semantic graph to ground the
language. This allows us to recognize many of the situations in which
the user describes areas that either the robot hasn't yet visited or
they reference landmarks whose labels were never added to the map. For
example, it's not uncommon for the user to mention regions that are
within sight but they have yet to reach (e.g., the user may say ``The
lab is across the lobby,'' but the robot has never been to the region
being referred to as ``the lab.''). We refer to descriptions of this
form as \emph{anticipatory}.

We identify instances of anticipatory descriptions by using our
distributions over the landmark and figure locations to evaluate the
likelihood that the landmark matches a labeled region in the graph and
that there are one or mode candidate figure regions consistent with
the language. When the method is sufficiently confident in the ability
to ground the language (we use a threshold of $0.2$), we update the
label distributions as described above. However, when the grounding
likelihoods suggest an anticipatory description, the algorithm adds
the expression along with its timestamp to a per-particle queue of
anticipatory descriptions. As the robot proceeds through the
environment and new nodes and semantic information are added to the
map, the algorithm continues to evaluate the grounding
likelihood~\eqref{eqn:landmark_likelihood} for the queued
descriptions. Specifically, we consider candidate pairs of landmark
and figure nodes and determine the landmark's likelihood according to
its label distribution. We express the figure likelihood as the
probability of the landmark-to-figure path under the learned language
model~\eqref{eqn:g3_factored}, where we consider the shortest path
that runs from the robot's pose at the time of the description,
through the landmark region, and on to the figure node. The logic is
that the description is most useful when the robot has visited the
regions to which the user refers and, thereby, the map has regions
whose labels and inter-region paths that are consistent with the
expression. The algorithm performs this process separately for each
particle, which may result in some particles incorporating the
description sooner than others.

In addition to input language, we also update the label distribution
for a node when the proposal step adds an edge to another node in the
graph. These edges may correspond to temporal constraints that exist
between consecutive nodes, or they may denote loop closures based upon
the spatial distance between nodes that we infer from the metric
map. Upon adding an edge to a node for which we have previously
incorporated a direct language observation, we propagate the observed
label to the newly connected node using a value of $\Delta \alpha =
0.5$.

\subsection{Updating the Particle Weights}

Having proposed a new set of graphs $\{G_t^{(i)}\}$ and updated the
analytic distributions over the metric and semantic maps for each
particle, we update their weights. The update follows from the ratio
between the target distribution over the graph and the proposal
distribution, and can be shown to be
%
%
\begin{subequations}
    \begin{align}
        w_t^{(i)} &= \frac{\textrm{Target distribution}}{\textrm{Proposal
                distribution}} \\
        &= \frac{p(G_t^{(i)} \vert z^{t}, u^{t}, \lambda^{t})}{p(G_t^{(i)}
            \vert G_{t-1}^{(i)}, z^{t-1}, u^t, \lambda^t)} 
        w_{t-1}^{(i)}\\
        &=\frac{p(z_t \vert G_t^{(i)},
            z^{t-1}, u^t, \lambda^t)}{p(z_t \vert z^{t-1})} 
        \cdot p(G_{t-1}^{(i)} \vert z^{t-1}, u^t, \lambda^t) \label{eqn:reweight_three}\\
        &\propto p(z_t \vert G_t^{(i)}, z^{t-1}, u^t, \lambda^t)
        \cdot p(G_{t-1}^{(i)} \vert z^{t-1}, u^t, \lambda^t)\\
        \tilde{w}_t^{(i)}&=p(z_t \vert G_t^{(i)}, z^{t-1}, u^t, \lambda^t) \cdot 
        w_{t-1}^{(i)}, \label{eqn:reweight_computation}
    \end{align}
\end{subequations}
%
%\begin{equation}
%        \tilde{w}_t^{(i)}=p(z_t \vert  G_t^{(i)}, z^{t-1}, u^t, \lambda^t) \cdot 
%        w_{t-1}^{(i)},
%\end{equation}
%
%
where $w_{t-1}^{(i)}$ is the weight of particle $i$ at time $t-1$ and
$\tilde{w}_t^{(i)}$ denotes the unnormalized weight at time $t$. We evaluate the
measurement likelihood (e.g., of LIDAR) by marginalizing over the node poses
%
%
\begin{multline}
    p(z_t \lvert G_t^{(i)}, z^{t-1}, u^t, \lambda^t) = \int_{X_t}
    p(z_t \lvert X_t^{(i)}, G_t^{(i)}, z^{t-1}, u^t, \lambda^t) \\
    \times p(X_t^{(i)} \lvert G_t^{(i)}, z^{t-1}, u^t, \lambda^t)
    dX_t,
\end{multline}
%
%
which allows us to utilize the conditional measurement model. In the
experiments presented next, we model the measurement as an observed
transformation between poses, which we compute via scan-matching. We
model this distribution (first term in the integral) as Gaussian,
which we have empirically found to be accurate.

After calculating and normalizing the new importance weights, we
periodically perform resampling based upon the effective number of
particles, as proposed by~\citet{liu96},
%
%
\begin{equation}
    N_{eff} = \frac{1}{\sum\limits_{i=0}^n{w_i^2}}.
\end{equation}
%
%
When the effective number of particles $N_{eff}$ falls below the
threshold $N/2$, where $N$ is the number of particles, we resample
using the algorithm described by~\citet{doucet00}.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "ijrr2013"
%%% End: 
