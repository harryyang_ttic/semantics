\section{Results}
%
We evaluate our algorithm through six experiments that involve a human
giving a robotic wheelchair
(Fig.~\ref{fig:tour_wheelchair})~\cite{hemachandra11} a narrated tour
of several buildings and courtyards on the MIT campus. The robot was
equipped with forward- and rearward-facing LIDARs, wheel encoders, and
an IMU. Speech was recorded using a wireless microphone worn by the
user.  In the first two experiments, the robot was manually driven
while the user interjected textual descriptions of the environment. In
the third experiment, the robot autonomously followed the human who
provided spoken descriptions.  Speech recognition was performed
manually.

\subsection{Indoor/Outdoor: Small Tour}
%
%
\begin{figure*}[!ht]
  \centering
  \subfigure[No language constraints]{%
      \includegraphics[width=0.65\linewidth]{./graphics/language_none/stata_1_short_tour/graph_and_occ}
      \label{fig:small_tour_no_language}}\\
  %
  \subfigure[Egocentric language]{%
      \includegraphics[width=0.65\linewidth]{./graphics/language_simple/stata_1_short_tour/graph_and_occ}
      \label{fig:small_tour_simple_language}}\\
  % 
  \subfigure[Allocentric language]{%
      \includegraphics[width=0.65\linewidth]{./graphics/language_complex/stata_1_short_tour/graph_and_occ}
      \label{fig:small_tour_complex_language}}
  \caption{Maximum likelihood semantic graphs for the small
      tour. In contrast to \subref{fig:small_tour_no_language} the
      baseline algorithm, our method incorporates key loop closures based upon \subref{fig:small_tour_simple_language}~egocentric and
      \subref{fig:small_tour_complex_language}~allocentric descriptions
      that result in metric, topological, and semantic maps that are
      noticeably more accurate. The dashed line denotes the
      approximate ground truth trajectory. The inset presents a view
      of the semantic and topological maps near the gym region.} \label{fig:small_tour}
\end{figure*}
%
%
The first experiment (Fig.~\ref{fig:small_tour}) took place on the
first floor of the Stata Center at MIT, which includes lecture halls,
elevator lobbies, a gym, and a cafeteria, as well as the adjacent
courtyard. Starting at one of the elevator lobbies, the user proceeded
to visit the gym, exited the building and, after navigating the
courtyard, returned to the gym and finished at the elevator lobby. The
user provided textual descriptions of the environment, twice each for
the elevator lobby and gym regions. We compare the performance of our
method based upon different forms of language input against a baseline
algorithm that emulates the current state-of-the-art in
language-augmented semantic mapping. In all cases, the algorithms were
run with 10 particles to approximate the distribution over the space
of topologies.  The final topology contained $137$
nodes.

\subsubsection{No Language Constraints}
%
We consider a baseline approach that directly labels nodes based upon
egocentric language, but does not propose edges based upon label
distributions. It does, however, propose loop closures based upon the
distribution over the metric map
(Section~\ref{sec:spatial_constraints}). The baseline emulates typical
solutions by augmenting a state-of-the-art iSAM metric map with a
semantic layer without allowing semantic information to influence
lower layers.

Figure~\ref{fig:small_tour_no_language} presents the resulting metric,
topological, and semantic maps that constitute the semantic graph for
the highest-weighted particle. The accumulation of odometry drift
results in significant errors in the estimate for the robot's pose
when revisiting the gym and elevator lobby. Without reasoning over the
semantic map, the algorithm is unable to detect loop closures. This
results in significant errors in the metric map as well as the
semantic map, which hallucinates two separate elevator lobbies
(purple) and gyms (orange).

\subsubsection{Egocentric Language}

We evaluate our algorithm when the user provides descriptions in the
form of egocentric language, in which case there is no ambiguity in
the landmark and figure that are implicitly the robot's current
location.

Figure~\ref{fig:small_tour_simple_language} presents the semantic
graph corresponding to the highest-weighted particle that our
algorithm estimates. By considering the semantic map when proposing
loop closures, the algorithm recognizes that the second region that
the user labeled as the gym is the same place that was labeled earlier
in the tour. At the time of receiving the second label, drift in the
odometry led to significant error in the gym's location much like the
baseline result (Fig.~\ref{fig:small_tour_no_language}).  The
algorithm immediately corrects this error in the semantic graph by
using the label distribution to propose loop closures at the gym and
elevator lobby, which would otherwise require searching a
combinatorially large space. The resulting maximum likelihood map is
topologically and semantically consistent throughout and metrically
consistent for most of the environment. The exception is the
courtyard, where only odometry measurements were available, causing
drift in the pose estimate. Attesting to the model's validity, the
ground truth topology receives 92.7\% of the probability mass and,
furthermore, the top four particles are each consistent with the
ground truth.

\subsubsection{Allocentric Language} 
%
Next, we consider the algorithm's performance when the figure and
landmark regions that the user's descriptions reference can no longer
be assumed to be the robot's current position. Specifically, we
replaced the initial labeling of the gym with an indirect reference of
the form ``The gym is down the hallway,'' with the hallway labeled
through egocentric language. The language inputs are otherwise
identical to those employed for the egocentric language scenario and
the baseline evaluation.
%
%
\begin{figure*}[!tp]
    \centering
    \subfigure[Ground Truth]{%
      \includegraphics[width=0.625\linewidth]{./graphics/stata_1_long_tour_ground_truth_topology}
      \label{fig:large_tour_ground_truth}}\\
    \subfigure[No language constraints]{%
      \includegraphics[width=0.625\linewidth]{./graphics/language_none/stata_1_long_tour/graph_and_occ}
      \label{fig:large_tour_no_language}}\\
    \subfigure[Allocentric language]{\includegraphics[width=0.625\linewidth]{./graphics/language_complex/stata_1_long_tour/graph_and_occ}
      \label{fig:large_tour_complex_language}}
    \caption{Maximum likelihood semantic graphs for
      \subref{fig:large_tour_ground_truth} the large tour
      experiment. \subref{fig:large_tour_no_language}~The result of
      the baseline algorithm with letter pairs that indicate map
      components that correspond to the same environment region.
      \subref{fig:large_tour_complex_language}~The result produced
      by our method based upon allocentric language descriptions, with an indication of the loop closures
      recognized based upon the semantic map.} \label{fig:large_tour}
\end{figure*}
%
%
\begin{figure}[!h] 
    \centering
    \subfigure[Egocentric language]%
    {\includegraphics[width=0.4\linewidth]{./graphics/gym_simple_language_with_legend}%
        \label{fig:label_distribution_simple_language}}\hfil
    \subfigure[Allocentric language]%
    {\includegraphics[width=0.4\linewidth]{./graphics/gym_complex_language}%
        \label{fig:label_distribution_complex_language}}
    \caption{Pie charts that compare the semantic map label distributions that
        result from 
        \subref{fig:label_distribution_simple_language}~the egocentric language
        description ``This is the gym'' with that of
        \subref{fig:label_distribution_complex_language}~the allocentric
        language description ``The gym is down the hall.'' 
    }\label{fig:label_dist_comparison}
\end{figure}
%
%
\begin{figure}[!th]
    \centering
    \subfigure[Lobby]{ \includegraphics[width=0.475\linewidth]{./graphics/language_complex/stata_1_long_tour/inset_1}\label{fig:stata_1_long_tour_inset_2}}\hfill
     \subfigure[Cafeteria]{ \includegraphics[width=0.475\linewidth]{./graphics/language_complex/stata_1_long_tour/inset_2}}
     \caption{Inset views of the (a) lobby and
       (b) cafeteria portions of the semantic graph for the large tour
       experiment (Fig.~\ref{fig:large_tour_complex_language}).} \label{fig:large_tour_complex_language_insets}
\end{figure}
%
%

The algorithm incorporates allocentric language into the semantic map
using the \G3 framework as described in
Section~\ref{sec:update_language} to infer the nodes in the graph that
constitute the figure (i.e., the ``gym'') and the landmark (i.e., the
``hallway''). This grounding attributes a non-zero likelihood to all
nodes that exhibit the relation of being ``down'' from the nodes
identified as being the ``hallway.''
Figure~\ref{fig:label_dist_comparison} compares the label
distributions that result from this grounding with those from
egocentric language. The algorithm attributes the ``gym'' label to
multiple nodes in the semantic graph as a result of the ambiguity in
the figure's location as well as the \G3 model, which yields high
likelihoods for several paths as being ``down from'' the landmark
nodes. When the user later labels the region after returning from the
courtyard, the algorithm proposes a loop closure despite significant
drift in the estimate for the robot's pose. As with the egocentric
language scenario, this results in a semantic graph for the
environment that is accurate topologically, semantically, and
metrically (Fig.~\ref{fig:small_tour_complex_language}).
%
%
\begin{figure}[!tb]
    \centering
    \includegraphics[width=0.95\linewidth]{./graphics/language_complex/stata_1_autonomous_tour/graph_and_occ}
    \caption{Maximum likelihood map for
        the autonomous tour.}\label{fig:autonomous_tour}
\end{figure}

\subsection{Indoor/Outdoor: Large Tour}

The second experiment (Fig.~\ref{fig:large_tour}) considers an
extended tour of MIT's Stata Center as well as two neighboring
buildings and their shared courtyard. In order to evaluate the
algorithm's ability to deal with ambiguity in the labels, the robot
visited several places with the same semantic attributes (e.g.,
elevator lobbies, entrances, and cafeterias) and visited some places
more than once (e.g., one cafeteria and the amphitheater). We
accompanied the tour with 20 descriptions of the environment that took
the form of both egocentric and allocentric language.

As with the smaller tour, we compare our method against the baseline
semantic mapping algorithm. Figure~\ref{fig:large_tour_no_language}
presents the baseline estimate for the environment's semantic
graph. Without incorporating allocentric language or allowing semantic
information to influence the topological and metric layers, the
resulting semantic graph exhibits significant errors in the metric
map, an incorrect topology, and aliasing of the labeled places that
the robot revisited. In contrast,
Figure~\ref{fig:large_tour_complex_language} demonstrates that, by
using semantic information to propose constraints in the topology, our
algorithm yields correct topological and semantic maps, and metric
maps with notably less
error. Figure~\ref{fig:large_tour_complex_language_insets} presents
the inset views for the lobby and second cafeteria portion of the map
that were labeled with allocentric descriptions. The resulting model
assigns 93.5\% of the probability mass to the ground truth topology,
with each of the top five particles being consistent with ground
truth.

The results highlight the ability of our method to tolerate
ambiguities in the labels assigned to different regions of the
environment. This is a direct consequence of the use of semantic
information, which allows the algorithm to significantly reduce the
number of candidate loop closures that is otherwise combinatorial in
the size of the map. This enables the particle filter to efficiently
model the distribution over graphs. While some particles may propose
invalid loop closures due to ambiguity in the labels, the algorithm is
able to recover with a manageable number of particles. In this
experiment, the algorithm employed 10 particles to approximate the
distribution over topologies. The final topology contained $213$
nodes.

For utterances with allocentric language, our algorithm was able to
generate reasonable groundings for the figure and landmark
locations. However, due to the simplistic way in which we define
regions, groundings for ``the lobby'' were not entirely accurate due
to the sensitivity to the local metric structure of the environment
when grounding paths that go ``through the entrance.'' We discuss this
in more detail in Section~\ref{sec:grounding_discussion}.

\subsection{Indoor/Outdoor: Autonomous Tour}
%
%
\begin{figure}[!tb]
    \centering
    \includegraphics[width=1.0\linewidth]{./graphics/language_complex/stata_third_floor/stata_third_floor}
    \caption{Maximum likelihood semantic graph inferred from the
        narrated tour of the Stata Center
        lab.} \label{fig:stata_third_floor}
\end{figure}
%
\begin{figure}[!t]
    \centering
    \includegraphics[width=0.875\linewidth]{./graphics/language_complex/stata_36_38_sixth_floor/32_36_38_semantic_graph}
    \caption{Maximum likelihood semantic graph for the MIT 32-36-38 tour. The allocentric descriptions are shown with
        numbers indicating their
        order.} \label{fig:32_36_38_graph}
\end{figure}
%
%
In the third experiment, the robot autonomously followed a user during
a narrated tour along a route similar to that of the first
experiment~\cite{Semantic_mapping_video}. Using a headset microphone,
the user provided spoken descriptions of the environment that included
ambiguous references to regions with the same label (e.g., elevator
lobbies, entrances). The utterances included both egocentric and
allocentric descriptions of the environment. The speech was recorded
as it was uttered in synchronization with the LIDAR and odometry
data. The audio was later manually transcribed into text that was
inserted alongside the sensor observations according to the time that
the audio was initially recorded. In this manner, the algorithm
handled the text, LIDAR, and odometry data as they were received,
emulating a scenario in which a speech recognizer was used to parse
the user's utterances during the tour.

The algorithm operated in this fashion using 10 particles to
approximate the distribution over the space of topologies. The final
topology contained $135$ nodes. Figure~\ref{fig:autonomous_tour}
presents the maximum likelihood semantic graph that our algorithm
estimates. By incorporating information that the descriptions convey,
the algorithm recognizes key loop closures that result in accurate
semantic maps. The resulting model assigns 82.9\% of the probability
mass to the ground truth topology, with each of the top nine particles
being consistent with ground truth.

\subsection{Stata Center Lab Tour}

We consider an additional experiment in which the robot was driven
throughout different labs on the third floor of MIT's Stata
Center. The narrated tour involved both egocentric and allocentric
descriptions of the environment, the latter of which were anticipatory
in nature with the user referencing locations in the environment that
the robot had not yet visited. Figure~\ref{fig:stata_third_floor}
presents the maximum likelihood semantic map that our framework
learned from the narrated tour using a total of 10 particles.  The
final topology contained $71$ nodes. The system correctly grounds each
of the allocentric descriptions despite the ambiguity that exists in
the landmark and figure locations, as we discuss in more detail
shortly.

\subsection{MIT 32-36-38 Tour}

In order to verify the validity of the algorithm in different
environments, we consider an extended tour of three connected
buildings on the MIT campus (buildings 32, 36, and 38). The robotic
wheelchair was manually driven throughout the office-like environment,
visiting offices, elevator lobbies, conference rooms, and lab spaces
whose appearance and structure varied between each building. Text was
added at several points throughout the tour to emulate recognized
natural language descriptions. We provided both egocentric and
allocentric utterances, including several instances of anticipatory
descriptions when the robot had not yet visited the referenced portions
of the environment (both the figure and the referent). We ran our
framework with 10 particles to model the distribution over
topologies. The final topology contained $148$ nodes.
Figure~\ref{fig:32_36_38_graph} denotes the maximum likelihood
semantic graph that resulted from our algorithm. The text indicates
the allocentric descriptions that were given to the system in the
numbered order.
%
%
\begin{figure}[!t]
    \centering%
    \vspace{-0.5em}
    \subfigure[]{\includegraphics[width=0.48\linewidth]{./graphics/language_complex/stata_36_38_sixth_floor/32_36_38_inset_1_2}\label{fig:32_36_38_inset_1_2}}\hfil%
    \subfigure[]{\includegraphics[width=0.48\linewidth]{./graphics/language_complex/stata_36_38_sixth_floor/32_36_38_inset_7}\label{fig:32_36_38_inset_7}}\\
    \subfigure[]{\includegraphics[width=0.48\linewidth]{./graphics/language_complex/stata_36_38_sixth_floor/32_36_38_inset_3_5}\label{fig:32_36_38_inset_3_5}}\hfil    
    \subfigure[]{\includegraphics[width=0.48\linewidth]{./graphics/language_complex/stata_36_38_sixth_floor/32_36_38_inset_4}\label{fig:32_36_38_inset_4}}%
    \caption{Inset views for the MIT 32-36-38 tour
      (Fig.~\ref{fig:32_36_38_graph}) that demonstrate the way in
      which the algorithm learns from allocentric descriptions
      \subref{fig:32_36_38_inset_1_2} ``The lobby is down the
      hallway'' (anticipatory, location 1) and ``The elevator lobby is
      down the hallway'' (location 2), \subref{fig:32_36_38_inset_7}
      ``The lobby is down the hallway'' (anticipatory),
      \subref{fig:32_36_38_inset_3_5} ``The office is near the
      hallway'' (anticipatory, location 3) and ``The elevator lobby is
      down the hallway'' (location 5), and
      \subref{fig:32_36_38_inset_4} ``The lab is down the hallway''
      (anticipatory). The dashed boxes denote the ground-truth
      boundaries for the regions.}
        \label{fig:32_36_38_insets}
\end{figure}
%
%

\subsection{Killian Court Tour}

The final experiment considers a tour of Killian Court, a set
of interconnected buildings on MIT's campus, which has served as a
benchmark environment for previous mapping algorithms. We consider
this environment in an effort to see how the algorithm performs when
tasked with mapping larger spaces that involve significant geometric
and semantic aliasing. Specifically, this part of the MIT campus
consists primarily of several long hallways with nearly identical
structure, including the so-called ``infinite corridor'' that serves
as one of the main hallways at MIT.
%
%
\begin{figure*}[!t]
    \centering
    \includegraphics[width=0.81\linewidth]{./graphics/language_complex/killian_court/killian_court}
    \caption{The maximum likelihood semantic map that results from a
        tour of MIT's Killian Court.}\label{fig:killian_court}
\end{figure*}
%
%

Starting in the north-east corner (Fig.~\ref{fig:killian_court}), we
gave the robot a tour along the infinite corridor that spans from left
to right in the Figure. After entering one of the main lobbies
(upper-left), we proceeded through buildings 5 and 3 and then exited
into the courtyard. We took a U-shaped path outside, entered building
4, and then traveled through buildings 6, 6C, and 14 before returning
to the start.  We provided both egocentric and allocentric language
descriptions at different points during the tour to
assign labels to and spatial relations between different
regions. These descriptions took the form of text that was interjected
in synchronization with the LIDAR and odometry streams as the data was
post-processed.

The algorithm learned a distribution over semantic maps from the
stream of descriptions, odometry, and LIDAR data, using 10 particles
to hypothesize the different topologies. The final topology contained
$276$ nodes. Figure~\ref{fig:killian_court} shows the resulting
maximum likelihood semantic graph overlaid on an approximately aligned
map of the MIT campus. Qualitatively, the map is metrically,
topologically, and semantically accurate with the exception of the map
of building 14 where a glass hallway between buildings 2 and 14 forced
the algorithm to use odometry for the inter-pose constraints. As with
the previous evaluations, we ran our framework without language-based
constraints to emulate the current state-of-the-art in
language-augmented semantic mapping. While we omit the figure for
space, we note that the resulting map is significantly warped.
%
%
\subsection{Computational Requirements}
%
%
\begin{table}[!t]
    \renewcommand{\arraystretch}{1.3}
    \centering
    \caption{Average Delay in Adding Node} \label{tab:delay}
    \begin{tabular}{@{}rrr@{}}\toprule
        Dataset & Average & Standard \\ 
                & Delay (s) & Deviation (s)\\ 
        \hline
        MIT 32-36-38       & 0.532 & 3.138 \\
        Killian Court Tour        & 0.682 & 2.726 \\
        Indoor/Outdoor Large Tour & 2.186 & 4.670 \\        
        \hline
    \end{tabular}
\end{table}
%
%

We analyze the computational cost of the algorithm by considering the
delay between when a node is first proposed (i.e., based on distance
traveled) and the time at which it is added to the map. This measure
reflects the overall time required of the algorithm, since it will not
add nodes until it has finished incorporating the most recent
description and proposed loop closures.  We consider the delay for the
three longest datasets, namely the indoor/outdoor large tour, the MIT
32-36-38 tour and the Killian Court tour.  Table~\ref{tab:delay}
summarizes the performance for each of these datasets.  Note that the
implementation has not been optimized to run in real-time, and each
particle is currently processed sequentially (i.e., particle updates
are not parallelized).  The variance in the delays is due to periods
of increased computation that correspond to instances when language
annotations are processed. This delay is dominated by two components
of the algorithm. The first is the time required to ground allocentric
descriptions using the $G^3$ framework for all particles. The second
is the time taken to scan-match the semantic-based loop closures that
are subsequently proposed between nodes with updated label
distributions. Allocentric language grounding requires computational
effort that is linear in the number of unique particles. Similarly,
the scan-match verification is linear in the number of nodes that are
updated with new label information, which is independent of the
size of the map. The computational requirements for verification are
dominated by a scan-match procedure that is exhaustive in its search
due to the potentially large error in the prior pose-to-pose
transform.
%
%
\begin{table*}[!t]
    \renewcommand{\arraystretch}{1.3}
    \centering
    \caption{Semantic Map Accuracy} \label{tab:semantic}
    \begin{tabular}{@{}rrrrrrrrr@{}}\toprule
         & \multicolumn{2}{c}{Indoor/Outdoor} &
         \multicolumn{2}{c}{Killian Court} & \multicolumn{1}{c}{MIT 32-36-38} & \multicolumn{1}{c}{Stata Center}\\
         & \multicolumn{2}{c}{Large Tour} & \multicolumn{2}{c}{Tour} & \multicolumn{1}{c}{Tour} & \multicolumn{1}{c}{Tour}\\
         \toprule
        Type & Baseline & SG & Baseline & SG & SG & SG\\
        \hline
        Cafeteria 		& 20\%	& 36\% 	& 23\% 	& 45\% 	& -	& -		\\
	Entrance 		& 43\%	& 46\% 	& 12\% 	& 47\% 	& - 	& -		\\
        Elevator Lobby 		& 46\%	& 46\% 	& 49\% 	& 49\% 	& 34\% 	& 40\%	\\
        Hallway 		& 8\% 	& 8\% 	& 18\% 	& 19\% 	& 36\% 	& 30\%	\\
        Lobby 			& 8\% 	& 13\% 	& 34\% 	& 47\% 	& 29\% 	& 21\%	\\
        Lab 			& - 	& - 	& 0\% 	& 47\% 	& 42\% 	& 37\%	\\
        Amphitheater 		& 25\%	& 53\% 	& - 	& - 	& - 	& -		\\
        Courtyard 		& 12\%	& 47\% 	& - 	& - 	& - 	& -		\\
        Office 			& - 	& - 	& - 	& - 	& 53\% 	& 56\%	\\        
        Conference Room 	& - 	& - 	& - 	& -	 & 51\%	& 56\%	\\
        Gym 			& 33\% 	& 48\% 	& - 	& - 	& - 	& -	\\
	\hline
    \end{tabular}
\end{table*}
%
%

\subsection{Semantic Accuracy}

Table~\ref{tab:semantic} outlines the accuracy of the resulting
semantic maps for four datasets, where we calculate the accuracy as
follows. First, we identify the regions for which language contributed
to their label distributions. We compute the ground truth label for
each of these regions and compute the cosine similarity between the
ground truth multinomial (assumed to have a likelihood of 1.0 for the
true label) and that of the label distribution.

For the indoor/outdoor large tour and the Killian Court tour, we also
compared the results for the maps that did not propose language
edges. Since large segments of these maps were metrically and
topologically inaccurate, we assigned a minimum score for regions that
were significantly inaccurate. In effect, this corresponds to
assigning these regions a uniform multinomial over labels. As can be
seen for the first two datasets, the use of our approach improves the
semantic accuracy of a number of regions. This improvement stems both
from the metric and topological accuracy of the learned maps as well
as the algorithm's ability to integrate allocentric language. In the
MIT 32-36-38 and the Stata Center tours, we also achieve reasonable
accuracy for most categories. We do note that in case of allocentric
language, some expressions can be ambiguous, either due to the
presence of multiple potential landmarks or due to the ambiguity in
the expression. For example, given the description ``The lobby is down
the hallway,'' there may be multiple regions whose location is
consistent with being ``down'' the hallway, of which only one is the
lobby. In these situations, each of these regions will receive high
likelihood of being the figure and the label distributions for each
will be updated accordingly. Additionally, we find that the accuracy
of the semantic maps is sensitive to our choice for region
decomposition. For example, hallways score fairly low under our
fixed-size segmentation, which can significantly underestimate their
spatial extent. We see these issues as inherent to our definition of
regions that would be diminished with a more sophisticated
segmentation strategy that takes into account local
appearance~\citep{brunskill07, pronobis12, ranganathan09,
  hemachandra14} and semantic~\citep{mozos07} properties of the
environment.


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "ijrr2013"
%%% End: 
