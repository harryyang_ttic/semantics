\documentclass{article}
\begin{document}

\section{Topological Graph SLAM Without Language}

We want to keep track of the posterior distribution on graphs.
Specifically, nodes of the graph are generated automatically after
travelling a certain distance based on raw odometry.  The information
stored in the graph is in the form of edges; in this case, two nodes
being connected to each other implies to straight-line traversability
by the robot.  As the space of all such graphs is combinatorial, we
cannot analytically represent the distribution over graphs.  Instead,
we use a Rao-Blackwellized Particle Filter to represent the graph of
connectivity via particle samples and the distribution of node
locations for a certain connectivity graph analytically as a Gaussian.

There are several limiting factors with this approach.  First, the use
of a global metric map internal to each particle means that for large
areas, the computations very quickly become intractable.  Similarly,
the fact that new nodes are instantiated every fixed distance means
that the number of landmarks to consider will be linear in the amount
of time that has passed.  Additionally, if the robot retraces its
steps frequently and traverses nearby areas, then the number of edges
that must be stored in the graph of the average particle will be
quadratic in the number of locations.

Starting with the posterior distribution on graphs and using Bayes'
Rule to obtain the prior distribution and measurement likelihood, the
result is:
$$p(G_t | z^t, u^t)  \propto p(G_t | z^{t-1}, u^t) \cdot p(z_t | G_t, z^{t-1}, u^t) $$

Here, $p(G_t | z^{t-1}, u^t)$ is the prior distribution on graphs and
$ p(z_t | G_t, z^{t-1}, u^t)$ is the measurement likelihood (ie, the
updating multiplicative factor for the weighting of the particles).
Note that we include the odometry measurement $u_t$ in the prior
distribution.  This ensures that computational resources are used
efficiently as more proposed particles will come from high probability
areas of the distribution than if the measurement $u_t$ were only used
to reweight particles at the end.

We can further factorize the prior distribution as:
$$ p(G_t | z^{t-1}, u^t) \propto p(G_t | G_{t-1}, z^{t-1}, u^{t-1}) \cdot p(G_{t-1} | z^{t-1}, u^{t-1}) \cdot p(u_t | G_t, z^{t-1}, u^{t-1})$$

Here, $p(G_{t-1} | z^{t-1}, u^{t-1})$ is the posterior from the
previous cycle.  The proposal distribution is $p(G_t | G_{t-1},
z^{t-1}, u^{t-1}) \cdot p(u_t | G_t, z^{t-1}, u^{t-1}) $. Note that,
as mentioned above, the measurement $u_t$ is used in the determination
of the proposal distribution.

\subsection{Proposal Distribution}

The proposal distribution considers the variety of ways in which
adding a new node can affect the graph.  Specifically, some set of new
edges will be added, and the proposal distribution is a distribution
over these possibilities.  We are not considering nodes to ever be
considered the same (unlike in Ranganathan and Dellaert's OPTM
algorithm); instead, we are considering the possible edges.
% We can use a Dirichlet distribution (instead of a Dirichlet process)
% to model the prior because we do not need to consider the
% possibility of a sample outside our previous model (in Ranganathan
% and Dellaert, they had to consider the possibility of a node being a
% new node and one of the previously seen nodes).
A sample from the proposal distribution will be a binary vector of
length $N-1$ where $N$ is the number of nodes (counting the newest
node).

One question to be considered is whether new information allows two
old nodes to connect.  It may make the most sense for this to be an
option for language constraints but not for odometry or lidar
constraints.

When creating this prior distribution, we want it to have several
properties.  First, it must propose connections from the newly added
node to some subset of the previous nodes.  Secondly, it should be
more likely to be connected to nearby nodes.  Thirdly, as the run
continues, we expect to have more connections because the robot has
been more places and is more likely to be near places it previously
was.

When adding edges purely due to odometry, this constraint adds nothing
to the internal SLAM map--the internal map is what gave rise to the
added edge to begin with.  When adding edges purely based on lidar,
the scan matching rotation and translation provides a constraint that
can be used for an update step on the map within each particle.  When
edges are eventually added via language, potentially between two old
nodes rather than the current node and old nodes, this will add an
edge and a `soft' constraint (with high variance) in underlying SLAM
map.

Odometry does not take into account the underlying structure of the
world.  Using distance to add edges means that, accurately, far away
points will likely not be connected and close points will be more
likely to be connected.  However, without outside information, this
would result in the likely graphs being relatively dense, with dense
bands along the robot's travels and overlaps if it goes near its old
path even if separated by an impassible barrier.  This is why the
lidar data and the uncertainty in the scan matching will be used to
weight the particles--untraversable connections in graphs will cause
the graph to be weighted less strongly.

Initially, however, we do not use lidar measurements or language
measurements and use only odometry.  As odometry connections do not
add any additional constraints to the SLAM map, the underlying global
map will be the same for all particles, though the overlaid
connectivity graph will be different.

An initial proposal for the proposal distribution is:

\begin{displaymath}
  v[i] = \left\{
    \begin{array}{lr}
      1 :& \textrm{with probability} \; \; \frac{1}{k*d(G_i, G_n)+1} \\
      0 :& \textrm{otherwise}
    \end{array}
  \right.
\end{displaymath} 

Here, $v[i]$ is the $i$th element of the vector of edges from the
$n$th node (ie, $v[i]$ is a $1$ if the $i$th node connects to the
$n$th node).  $k$ is a constant for weighting.  $d(G_i, G-n)$ denotes
the distance between the $i$th node $G_i$ and the $n$th node $G_n$.
This proposal distribution is a simple model with the following
desired properties:

\begin{itemize}
\item If the distance is 0, a connection will be made with probability
  1.
\item If the distance is $\infty$, a connection will be made with
  probability 0.
\end{itemize}

It doesn't fall off very quickly, though, which may have the side
effect of creating more edges than is optimal.  Potentially, a half
Gaussian probability distribution or a $\chi^2$ distribution may be
better (half a Gaussian because distances are nonnegative).  This is
something that can be considered later.

This proposal distribution takes into account both $p(G_t | G_{t-1},
z^{t-1}, u^{t-1}) $ and $ p(u_t | G_t, z^{t-1}, u^{t-1})$: the second
because the proposal distribution uses the current location of this
node and the first because the distances are taken with respect to
each other node in the graph.  A more involved model might also take
into account the connectivity of other nodes and place a higher
probability on the connection of the new node to an old node if the
old node is highly connected.



\subsection{Weighting}

Particles weights will be adjusted based on the measurement likelihood
function $p(z_t | G_t, z^{t-1}, u^t)$.  The weight of each particle at
time $t$, $w_t$, will be proportional to
$$ w_t \propto p(z_t | G_t, z^{t-1}, u^t) \cdot w_{t-1}$$
where $w_{t-1}$ is the weight of the particle at the previous
timestep.  The probability $p(z_t | G_t, z^{t-1}, u^t)$ will be based
on the combined likelihoods of the different scan matches between the
current node being added and the previous nodes it is postulated to be
adjacent to.





\end{document} 
