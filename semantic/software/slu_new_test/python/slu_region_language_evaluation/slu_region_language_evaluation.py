# topological graph slam
from particle import Particle
from dcrf3.train_rf import CostFnRF
from region_label_distribution import LabelDistribution
from PyQt4.QtCore import SIGNAL, QSocketNotifier, QObject, QTimer
from region_node import Node
from region_edge import Edge
import region_slu_api
#from semantic_mapping.semantic_mapping_state import particle_to_context
import spatial_features_cxx as sf

from numpy import log
#from numpy.linalg import 
import scipy
#from scipy import *
#from scipy.integrate import*
import sys
import random
import traceback
import time
import math 
from lcm import LCM
from slam.slu_result_list_t import slu_result_list_t
from slam.slu_landmark_result_t import slu_landmark_result_t
from slam.slu_grounding_result_list_t import slu_grounding_result_list_t
from slam.slu_grounding_result_t import slu_grounding_result_t
from slam.slu_figure_result_t import slu_figure_result_t
from slam.slu_result_t import slu_result_t
from slam.region_slu_querry_t import region_slu_querry_t
from slam.graph_edge_t import graph_edge_t
from slam.node_label_list_t import node_label_list_t
from slam.graph_particle_list_t import graph_particle_list_t
from slam.node_probability_t import node_probability_t
from slam.dirichlet_update_t import dirichlet_update_t
from slam.language_label_t import language_label_t
from slam.language_edge_t import language_edge_t
from slam.slu_parse_language_querry_t import slu_parse_language_querry_t
from region import Region
## New querry infrastructure 
from slam.complex_language_collection_list_t import complex_language_collection_list_t
from slam.complex_language_result_list_t import complex_language_result_list_t
from slam.complex_language_result_t import complex_language_result_t
from slam.complex_language_path_result_list_t import complex_language_path_result_list_t
from slam.complex_language_path_result_t import complex_language_path_result_t

from operator import itemgetter, attrgetter

import basewindow 
from g3.cost_functions.gui import crfFeatureWeights


class lcm_listener():
   def __init__(self):
      #self.slu_api = slu_api.SluApi()
      self._running = True
      self.lc = LCM()
      ## We are given a complex language as well as a corresponding particle list 
      ## This needs to be converted to the python particle definition and then 
      ## Sent over to SLU and then scored 
      ## The resulting scores need to be sent back to the isam module - which 
      ## updates the Dir distributions of the nodes and then proposes new language edges 

      #This sends the whole particle list for G3 to process
      self.lang_sub = self.lc.subscribe("ISAM_REGION_SLU_LANGUAGE", self.slu_region_handler)
      #This will send all the potential figures and landmarks for each particle 
      self.querry_sub = self.lc.subscribe("SLU_COMPLEX_LANGUAGE_QUERRY", self.slu_region_querry_handler)
      
      self.language_parse = self.lc.subscribe("ISAM_REGION_SLU_LANGUAGE_PARSE", self.slu_parse_language_handler)

      self.received_slam_message = 0
      self.time = 0
      self.num_particles = 0
      self.particles = []
      self.similarity_threshold = .2
      self.labelsToAdd = []
      self.keyword_lookup = {} #this mapping gets reset whenever a new slu request comes 
      ### How to make this mapping not be static ??
      #self.keyword_lookup = {'courtyard' : 0, 'gym' : 1, 'hallway' : 2, 'amphitheater' : 3, 'cafeteria' : 4, 'elevator lobby' : 5, 'entrance': 6, 'lobby':7}
      self.slu = region_slu_api.SluApi()
      ### what is this used for?? 
      self.saliency = [.8, .9, .1, .95, .5, .4, .7]
      self.featureBrowser = crfFeatureWeights.MainWindow()
      self.featureBrowser.show()
      print "made slu"


      prob_of_label_given_region_label = {}

      synonyms = [['classroom', 'lecturehall'], ['boardroom', 'conferenceroom', 'meetingroom'], 
                  ['elevator'], ['elevatorlobby'], ['reception'] ,['lounge', 'lobby'], ['hall', 'hallway', 'passageway', 'corridor', 'walkway'], ['office', 'cubicle', 'workspace', 'lab', 'computerlab']] 

      synonym_set = []
      label_set = []
      for i in synonyms:
         synonym_set.append(set(i))
         for j in i:
            label_set.append(j)


      print label_set
      print len(label_set)

      #lets build the label likelihood table 

      self.prob_of_label_given_region_label = {}

      ##We need to set this to false when we are running the icra stuff - and true for 
      ## RSS stuff to work - ideally this should work based on signal from slam

      self.use_full_dist = True

      if(True):
         for label1 in label_set:
            conditional_prob_dict = {}

            for label2 in label_set:
               #if synonym - add high value 
               if(self.is_synonym(synonym_set, label1, label2)):
                  conditional_prob_dict[label2] = 0.9
               else:
                  conditional_prob_dict[label2] = 0.1
            self.prob_of_label_given_region_label[label1] =  conditional_prob_dict    
      else:
         for label1 in label_set:
            conditional_prob_dict = {}

            for label2 in label_set:
               #if synonym - add high value 
               if (label2 == label1):
                  conditional_prob_dict[label2] = 1.0
               else:
                  conditional_prob_dict[label2] = 0.0
            self.prob_of_label_given_region_label[label1] =  conditional_prob_dict   

      self.print_conditional_probabilities()

   def is_synonym(self, synonym_set, label1, label2):
      for synonym in synonym_set:
         inter1 = synonym.intersection(set([label1]))
         inter2 = synonym.intersection(set([label2]))
         if(len(inter1) > 0 and len(inter2)):
            return True
         
      return False

   def print_conditional_probabilities(self):
      print "========== Conditional Probabilities for Labels ==========" 
      for key in self.prob_of_label_given_region_label.keys():
         print "Label : ", key , " Given Label " 
         cond_prob = self.prob_of_label_given_region_label[key]
         for i in cond_prob.keys():
            prob = cond_prob[i]
            print "\t ", i, " : " , prob

   def lcm_filenos(self):
      return [self.lc.fileno(),
              #self.slu_api.pquery.lcm_fileno,
              ]
              
   def activated(self, i):
      print "Activate Called"
      self.lc.handle()
      print "Activate Returned"

   def run(self):
      print "Started LCM listener"
      
      while self._running:
         try:
            self.lc.handle()
            
         except KeyboardInterrupt:
            print "\n============== Interrupt =================\n"
            self._running = False
            pass
   
   def update_keymap(self, particle_list):
      self.keyword_lookup = {}
      label_dist = {}

      print particle_list.label_info
      print particle_list.label_info.count 
      print particle_list.label_info.id
      for i in range(particle_list.label_info.count): #particle_list.label_info.id):
         label_dist[particle_list.label_info.id[i]] = particle_list.label_info.names[i]
         self.keyword_lookup[particle_list.label_info.names[i]] = particle_list.label_info.id[i]

      print label_dist
      print self.keyword_lookup

   def decode_region_particle_msg(self, particle_list, landmark_label, label_from_region, uttrance_node_id, last_checked_id):

      print "=============== Decoding Particle Msg ===============" 
      self.num_particles = particle_list.no_particles
      new_particles = []
      self.keyword_lookup = {}
      label_dist = {}

      print particle_list.label_info
      print particle_list.label_info.count 
      print particle_list.label_info.id
      for i in range(particle_list.label_info.count): #particle_list.label_info.id):
         label_dist[particle_list.label_info.id[i]] = particle_list.label_info.names[i]
         self.keyword_lookup[particle_list.label_info.names[i]] = particle_list.label_info.id[i]

      print label_dist
      print self.keyword_lookup
      

      #build this from the particle list message 
      for i in range(particle_list.no_particles):
         particle = Particle()
         particle.language_node_id = uttrance_node_id
         particle.last_checked_id = last_checked_id
         par = particle_list.particle_list[i]
         particle.id = par.id
         particle.num_nodes = par.no_nodes
         particle.weight = par.weight
         particle.nodelist = []
         particle.edges = []
         #print "------------------------------------------\nParticle Id: ", par.id
         #for j in range(par.no_nodes):

         for k in range(par.no_regions):
             region = par.region_list[k]
   
             for j in range(region.count):                 

                 node = region.nodes[j]#par.node_list[j]
                 nodeinst = Node()
                 nodeinst.id = node.id
                 particle.region_map[node.id] = region.id
                 nodeinst.xy = node.xy
                 nodeinst.heading = node.heading
                 nodeinst.pofz = node.pofz
                 nodeinst.is_supernode = node.is_supernode
                 nodeinst.cov = node.cov
                 nodeinst.parent_supernode = node.parent_supernode
                    ## Bounding box
                 bb = []
                 for i in range(node.no_points):
                     bb.append( (node.x_coords[i], node.y_coords[i]) )
                 nodeinst.bounding_box = bb
                 
                 #each node in the region is going to get the same label distribution as the region 
                 #This should be adjusted 
                 nodeinst.label_dist = LabelDistribution(label_dist, region.region_label_dist, landmark_label, self.prob_of_label_given_region_label, self.use_full_dist)

                 particle.nodelist.append(nodeinst)
                 #The dict is prob not useful now - but we will keep it around for now 
                 particle.nodelist_dict[nodeinst.id] = nodeinst
         
         particle.update_language_node()

         for j in range(par.no_edges):
             ## We should prob get rid of the bad edges 
             edge = par.edge_list[j]
            #Add only sucessful ones
             if(edge.status == graph_edge_t.STATUS_SUCCESS):
                 edgeinst = Edge()
                 edgeinst.node1 = edge.node_id_1
                 edgeinst.node2 = edge.node_id_2
                 edgeinst.edge_id = edge.id
                 edgeinst.status = edge.status
                 edgeinst.type = edge.type               
               #This seems to want to keep around if an edge is new - i dont think we will need that now 
                 edgeinst.new = True
               
                 edgeinst.transformation = edge.transformation
                 edgeinst.cov = edge.cov
                 particle.edges.append(edgeinst)

         ## Doublecheck with stephanie that we dont need to add logical constraints between previous and current 
         ## supernodes

         particle.update_graph()
         new_particles.append(particle)
      return new_particles

   def decode_region_particle_msg_working_old(self, particle_list):

      print "=============== Decoding Particle Msg ===============" 
      self.num_particles = particle_list.no_particles
      new_particles = []
      self.keyword_lookup = {}
      label_dist = {}

      print particle_list.label_info
      print particle_list.label_info.count 
      print particle_list.label_info.id
      for i in range(particle_list.label_info.count): #particle_list.label_info.id):
         label_dist[particle_list.label_info.id[i]] = particle_list.label_info.names[i]
         self.keyword_lookup[particle_list.label_info.names[i]] = particle_list.label_info.id[i]

      print label_dist
      print self.keyword_lookup
      

      #build this from the particle list message 

      
      for i in range(particle_list.no_particles):
         particle = Particle()
         par = particle_list.particle_list[i]
         particle.id = par.id
         particle.num_nodes = par.no_nodes
         particle.weight = par.weight
         particle.nodelist = []
         particle.edges = []
         #print "------------------------------------------\nParticle Id: ", par.id
         #for j in range(par.no_nodes):
         
         

         for k in range(par.no_regions):
             region = par.region_list[k]
   
             for j in range(region.count):                 

                 node = region.nodes[j]#par.node_list[j]
                 nodeinst = Node()
                 nodeinst.id = node.id
                 particle.region_map[node.id] = region.id
                 nodeinst.xy = node.xy
                 nodeinst.heading = node.heading
                 nodeinst.pofz = node.pofz
                 nodeinst.is_supernode = node.is_supernode
                 nodeinst.cov = node.cov
                 nodeinst.parent_supernode = node.parent_supernode
                    ## Bounding box
                 bb = []
                 for i in range(node.no_points):
                     bb.append( (node.x_coords[i], node.y_coords[i]) )
                 nodeinst.bounding_box = bb
                 
                 #### We need to change this one now 
                 #ld = region.region_label_dist
                 
                 #each node in the region is going to get the same label distribution as the region 
                 nodeinst.label_dist = LabelDistribution(label_dist, region.region_label_dist)
                 #nodeinst.label_dist.num_labels = ld.count
                 #we should check if it has a label distribution 
                 #nodeinst.label_dist.total_obs = 0 #ld.total_obs
                 
                 #print "Label distribution size : ", len(nodeinst.label_dist.observation_frequency)
                 #print "Msg Label Size : " , ld.num_labels

                 #for now we are using the region's label distribution 
                 #we will only test one node per region - based on which one is set as the supernode (which is the mean node for the region) 
                 #for k in range(ld.count):
                 #    nodeinst.label_dist.distribution[ld.classes[k].type] = ld.classes[k].probability

                 particle.nodelist.append(nodeinst)
                 #The dict is prob not useful now - but we will keep it around for now 
                 particle.nodelist_dict[nodeinst.id] = nodeinst
            

         for j in range(par.no_edges):
             ## We should prob get rid of the bad edges 
             edge = par.edge_list[j]
            #Add only sucessful ones
             if(edge.status == graph_edge_t.STATUS_SUCCESS):
                 edgeinst = Edge()
                 edgeinst.node1 = edge.node_id_1
                 edgeinst.node2 = edge.node_id_2
                 edgeinst.edge_id = edge.id
                 edgeinst.status = edge.status
                 edgeinst.type = edge.type               
               #This seems to want to keep around if an edge is new - i dont think we will need that now 
                 edgeinst.new = True
               
                 edgeinst.transformation = edge.transformation
                 edgeinst.cov = edge.cov
                 particle.edges.append(edgeinst)
         ## Doublecheck with stephanie that we dont need to add logical constraints between previous and current 
         ## supernodes
         new_particles.append(particle)
      return new_particles

   def send_extensive_slu_update(self, result, spatial_relation, figure_label, landmark_label, language_event=-1):
      msg = slu_grounding_result_list_t() 
      print "No of results : ", len(result.keys())
      msg.no_particles = len(result.keys())

      msg.lang_id = language_event

      print "Language Event ID : " , language_event
      msg.figure = figure_label
      msg.landmark = landmark_label
      msg.spatial_relation = spatial_relation

      msg.results = []

      for p_id in result.keys():
         part_res = result[p_id]['labels']
         particle = result[p_id]['particle']
         p_result = slu_grounding_result_t()
         p_result.particle_id = p_id
         
         print "Particle ID : " + str(p_id)
         #there should only be one 
         for l in part_res.keys():

            print "Label ID : " , l 
            node_prob = part_res[l]
            #These are the node prob pairs for each label
            p_result.no_figures = len(node_prob.keys())
            
            p_result.result = []
                       
            for nd_key in node_prob.keys():
               print "Node ID : " + str(nd_key)  + " Region ID :" , particle.region_map[nd_key],  " Prob : " + str(node_prob[nd_key]['total_prob']) #/ full_prob)
               fig_t = slu_figure_result_t()
               fig_t.figure_id = nd_key# node_prob[nd_key][0]#nd_key
               fig_t.prob = node_prob[nd_key]['total_prob']#node_prob[nd_key] #/ full_prob
               
               fig_t.no_nodes = len(node_prob[nd_key]['path'])
               fig_t.path_node_ids = []

               for pth_pt in node_prob[nd_key]['path']:
                  fig_t.path_node_ids.append(pth_pt.id)

               fig_t.no_landmarks = len(node_prob[nd_key]['landmark_list'])

               fig_t.landmark_results = []

               for land_mk in node_prob[nd_key]['landmark_list']:
                  lm_res = slu_landmark_result_t()
                  lm_res.landmark_id = land_mk['node'].id
                  lm_res.path_probability = land_mk['path_prob']
                  lm_res.grnd_probability = land_mk['ground_prob']
                  lm_res.landmark_probability = land_mk['landmark_prob']
                  fig_t.landmark_results.append(lm_res)
                  
                  
               p_result.result.append(fig_t)


         msg.results.append(p_result)

      print "Size of results : ", len(msg.results)
      print "Message Formed" 
      self.lc.publish("SLU_LANG_EXTENSIVE_RESULT",msg.encode())

   def send_slu_update(self, result, language_event=-1):
      msg = slu_result_list_t() 
      msg.utime = 0 
      print "No of results : ", len(result.keys())
      msg.size = len(result.keys())

      msg.language_event = language_event

      print "Language Event ID : " , msg.language_event
      msg.results = []

      for p_id in result.keys():
         part_res = result[p_id]['labels']
         particle = result[p_id]['particle']
         p_result = slu_result_t()
         p_result.particle_id = p_id
         p_result.no_labels = len(part_res.keys())
         p_result.labels = []
         print "Particle ID : " + str(p_id)
         for l in part_res.keys():
            print "Label ID : " , l 
            node_prob = part_res[l]
            #These are the node prob pairs for each label
            nd_label_list = node_label_list_t()
            nd_label_list.label = l
            
            nd_label_list.nodes = []
                       
            for nd_key in node_prob.keys():
               print "Node ID : " + str(nd_key)  + " Region ID :" , particle.region_map[nd_key],  " Prob : " + str(node_prob[nd_key]['total_prob']) #/ full_prob)
               nd_t = node_probability_t()
               nd_t.id = nd_key# node_prob[nd_key][0]#nd_key
               nd_t.prob = node_prob[nd_key]['total_prob']#node_prob[nd_key] #/ full_prob
               nd_label_list.nodes.append(nd_t)

            nd_label_list.num = len(node_prob.keys())

            p_result.labels.append(nd_label_list)

         msg.results.append(p_result)

      print "Size of results : ", len(msg.results)
      print "Message Formed" 
      self.lc.publish("SLU_LANG_RESULT",msg.encode())

      ############ Also send the extensive results message 
      

   def send_slu_update_depricated(self, result):
      msg = slu_result_list_t() 
      msg.utime = 0 
      msg.size = len(result.keys())
      msg.results = []
      for p_id in result.keys():
         part_res = result[p_id]['labels']
         particle = result[p_id]['particle']
         p_result = slu_result_t()
         p_result.particle_id = p_id
         p_result.no_labels = len(part_res.keys())
         p_result.labels = []
         print "Particle ID : " + str(p_id)
         for l in part_res.keys():
            print "Label ID : " , l 
            nd_list = part_res[l]
            #These are the node prob pairs for each label
            nd_label_list = node_label_list_t()
            nd_label_list.label = l
            
            nd_label_list.nodes = []

            ##Sum up and normalize and send

            ### We should probably send the full grounding results - so we can see which ones give the highest grounding likelihoods

            node_prob = {}
            full_prob = 0
            for nd in nd_list:
               if(node_prob.has_key(nd[0])):
                  node_prob[nd[0]] = [nd[0], node_prob[nd[0]][1] + nd[1]]
               else:
                  node_prob[nd[0]] = [nd[0], nd[1]]
               full_prob += nd[1]
                  

            print node_prob.keys()

            print node_prob

            #for nd in nd_list:
            #   #print "Node ID : " + str(nd[0])  + " Prob : " + str(nd[1])
            #   nd_t = node_probability_t()
            #   nd_t.id = nd[0]
            #   nd_t.prob = nd[1]
            #   nd_label_list.nodes.append(nd_t)
            
            for nd_key in node_prob.keys():
               print "Node ID : " + str(nd_key)  + "Region ID :" , particle.region_map[nd_key],  " Prob : " + str(node_prob[nd_key][1]) #/ full_prob)
               nd_t = node_probability_t()
               nd_t.id = node_prob[nd_key][0]#nd_key
               nd_t.prob = node_prob[nd_key][1]#node_prob[nd_key] #/ full_prob
               nd_label_list.nodes.append(nd_t)

            nd_label_list.num = len(node_prob.keys())

            p_result.labels.append(nd_label_list)

         msg.results.append(p_result)

      print "Message Formed" 
      self.lc.publish("SLU_LANG_RESULT",msg.encode())

   def send_slu_update_old(self, result):
      msg = slu_result_list_t() 
      msg.utime = 0 
      msg.size = len(result.keys())
      msg.results = []
      for p_id in result.keys():
         part_res = result[p_id]
         p_result = slu_result_t()
         p_result.particle_id = p_id
         p_result.no_labels = len(part_res.keys())
         p_result.labels = []
         for l in part_res.keys():
            nd_list = part_res[l]
            #These are the node prob pairs for each label
            nd_label_list = node_label_list_t()
            nd_label_list.label = l
            nd_label_list.num = len(nd_list)
            nd_label_list.nodes = []

            ##Sum up and normalize and send
            
            for nd in nd_list:
               print "Node ID : " + str(nd[0])  + " Prob : " + str(nd[1])
               nd_t = node_probability_t()
               nd_t.id = nd[0]
               nd_t.prob = nd[1]
               nd_label_list.nodes.append(nd_t)
            p_result.labels.append(nd_label_list)

         msg.results.append(p_result)

      print "Message Formed" 
      self.lc.publish("SLU_LANG_RESULT",msg.encode())

   def slu_parse_language_handler(self, channel, data):
      msg = language_label_t.decode(data)

      label = msg.update
      
      print "Received SLU request"

      sr, fig , landmark = self.slu.parse_language(label)

      result = slu_parse_language_querry_t()
      result.utime = msg.utime
      result.utterance = label
      result.landmark = landmark
      result.figure = fig
      result.sr = sr

      print "Language : " , result.utterance
      print "Landmark : " , result.landmark
      print "Figure : " , result.figure
      print "Spartial Relation : " , result.sr
      

      self.lc.publish("SLU_LANG_PARSE_RESULT",result.encode())

   def slu_region_querry_handler(self, channel, data):
      
      time_start = time.time()
      
      msg = complex_language_collection_list_t.decode(data)

      time_decode = time.time()
      
      verbose = False#True
 
      ## Arrange for each Complex language event (for all particles) 
      ## This will prevent us from having to rebuild a ggg object multiple times for 
      ## the same language
      complex_language = {}

      particle_region_map = {}

      for particle in msg.collection:

         #mapping from node id to region id 
         region_node_map = {}
         particle_region_map[particle.particle_id] = region_node_map

         node_poses = {}
         for nd in particle.poses:
            node_poses[nd.id] = {'xy': nd.xy, 'theta': nd.t}

         landmark_regions = {}
         for region in particle.landmarks:
            #we should probably use a class 
            landmark_regions[region.node_id] = Region(region)
            region_node_map[region.node_id] = region.id
            #print "region " , region.id

         for lang_event in particle.language:
            event = {}
            if(complex_language.has_key(lang_event.event_id)):
               ## Add this particle to the event 
               event = complex_language[lang_event.event_id]
            else:
               complex_language[lang_event.event_id] = event
               event['utterence'] = lang_event.utterence               
               event['particle'] = {}
               
            print "Adding Complex Langugage " , lang_event.event_id , " , " , lang_event.utterence   
               
            ## Add the information for the particle to this event 
            paths = []
            landmarks = []
            
            utterence_node = Region()            
            utterence_node.set_region_from_node(lang_event.utterence_node, node_poses[lang_event.utterence_node])
            event['particle'][particle.particle_id] = {'landmarks': landmarks, 'paths': paths, 'utterence_node': utterence_node}
            
            for lm in lang_event.valid_landmarks:
               print "Landmark Region : " , lm
               landmarks.append(landmark_regions[lm])

            for path in lang_event.path:
               print "Adding Path For Node : " , path.node_id , " Length : " , path.count
               f_path = {}
               paths.append(f_path)
               f_path['id'] = path.node_id
               f_path['path'] = []
               f_path['figure'] = landmark_regions[path.node_id]
               f_path['landmarks'] = []

               for lm in path.landmark_ids:
                  f_path['landmarks'].append(lm)

               for nd in path.nodes:
                  f_path['path'].append(node_poses[nd]['xy'])
      
      if(verbose):            
         for event_id, event in complex_language.iteritems():
            print "Complex Language Event : " , event['utterence']
            for id, particle in event['particle'].iteritems():
               print "Particle : " , id

               print "Landmarks ---" 
               for lm in particle['landmarks']:
                  print "\t Region : ", lm.id

               print "Figures ---" 
               for path in particle['paths']:
                  print "\t Figure Node : ",  path['id']
                  #for pt in path['path']:
                  #   print "\t\t", pt[0], " , " , pt[1] 
            
      
      time_to_convert = time.time()

      ### Now lets ground these 

      grounding_result = {}

      #should we convert these for each particle for all events?? - same as sent?? 

      for event_id, event in complex_language.iteritems():
         print "Complex Language Event : " , event['utterence']
         
         event_result = self.slu.evaluate_language_event(event)

         for p_id, p_result in event_result.iteritems():
            particle_result = {}
            if grounding_result.has_key(p_id):
               particle_result = grounding_result[p_id]
            else:
               grounding_result[p_id] = particle_result

            particle_result[event_id]= {'event': event,'result': p_result}

      print "\n\n=================================================================================\n\n"

      time_to_ground = time.time()

      print "Time to Decode message : ", time_decode - time_start
      
      print "Time to Convert : " , time_to_convert - time_decode

      print "Time to Ground : " , time_to_ground - time_to_convert 


      result_msg = complex_language_result_list_t()
      result_msg.utime = msg.utime
      result_msg.no_particles = len(grounding_result)
      result_msg.results = []
      

      for p_id, particle_result in grounding_result.iteritems():
         print "Particle ID : ", p_id 
         
         region_node_map = particle_region_map[p_id]
         
         p_result_msg = complex_language_result_t()
         p_result_msg.particle_id = p_id
         p_result_msg.count = len(particle_result)
         p_result_msg.language = []

         result_msg.results.append(p_result_msg)
         
         for event_id, event_result in particle_result.iteritems():
            print "\tEvent ID : " , event_id, " =>", event_result['event']['utterence']

            e_result = event_result['result']
            
            event_msg = complex_language_path_result_list_t()
            event_msg.event_id = event_id
            event_msg.no_paths = len(e_result)
            event_msg.results = []

            p_result_msg.language.append(event_msg)

            for figure_id, path_result in e_result.iteritems():

               path_result_msg =complex_language_path_result_t()
               
               path_result_msg.node_id = figure_id
               path_result_msg.region_id = region_node_map[figure_id]
               path_result_msg.no_landmarks = len(path_result)
               path_result_msg.landmark_results = []

               event_msg.results.append(path_result_msg)
               
               print "\t\tFigure : ", figure_id 

               sorted_landmarks = []
               #Lets sort these 
               for landmark_id, g_result in path_result.iteritems():
                  sorted_landmarks.append((landmark_id, g_result['path_prob']))
                  
               sorted_landmarks = sorted(sorted_landmarks, key=itemgetter(1))

               sorted_landmarks.reverse()
                
               for landmark in sorted_landmarks:
                  l_result_msg = slu_landmark_result_t()
                  #this is the region id for the landmark 
                  l_result_msg.landmark_id = region_node_map[landmark[0]]
                  l_result_msg.path_probability = landmark[1]
                  path_result_msg.landmark_results.append(l_result_msg)
                  
                  print "\t\t\t Landmark : " , landmark[0] , " => Prob " , landmark[1]
      
      ### Convert the result and send it to Semantic Mapping module 

      print "\n\n=================================================================================\n\n"
            
      self.lc.publish("SLU_COMPLEX_LANGUAGE_RESULT", result_msg.encode())


   def slu_region_handler(self, channel, data):
      msg = region_slu_querry_t.decode(data)
      language_event = msg.language_event
      label = msg.utterance
      particle_msg = msg.plist
      
      self.update_keymap(particle_msg)

      print "Received SLU request"

      sr, fig , landmark = self.slu.parse_language(label)

      fig_split = fig.split(" ")

      fig_only = []
      for w in fig_split: 
         if w != "the":
            fig_only.append(w)

      fig = " ".join(fig_only).strip()

      landmark_split = landmark.split(" ")

      landmark_only = []
      for w in landmark_split: 
         if w != "the":
            landmark_only.append(w)

      landmark = " ".join(landmark_only).strip()   

      print "Spatial Relation : " , sr

      print "Figure : " , fig #the label we are trying to ground 
      
      print "Landmark : " , landmark #the label we are trying to ground 

      print "Uttrance Node ID : " , msg.uttrance_node_id

      #Check if the landmark is in the keywords 
      landmark_ind = -1
      figure_ind = -1
      landmark_keyword = None
      figure_keyword = None
      label_names = self.keyword_lookup.keys()
      for lb in label_names:
         if(landmark == lb): #.find(lb) >= 0):
            landmark_ind = self.keyword_lookup[lb]
            landmark_keyword = lb
            print "Key found : " , lb , " Landmark " , landmark , " Index : " , landmark_ind 

         if(fig == lb): #.find(lb) >= 0):
            figure_ind = self.keyword_lookup[lb]
            figure_keyword = lb
            print "Key found : " , lb , " Figure " , fig , " Index : " , figure_ind 
            
      print "Landmark Ind : ", landmark_ind
      print "Figure Ind   : ", figure_ind

      if(figure_ind < 0 or landmark_ind < 0):
         print "Error Landmark or figure not found in labels : Returning"
         return 

      #label_from_region = {'hallway' : 1.0, 'corridor': 1.0, 'walkway': 0.9, 'elevatorlobby':0.1, 'classroom': 0.1 ....}  
      label_from_region = {}
      particle_list = self.decode_region_particle_msg(particle_msg, landmark_keyword, label_from_region, msg.uttrance_node_id, msg.last_checked_node_id)

      if(sr == 'near'):
         landmark_distance_threshold = 15#35 #ignore things too far away 
         
         figure_distance_threshold = 35 

         near_mean_dist = 10

         ## Do something different - e.g. a sigmoid function around a fixed radius 
         print "Spatial Relation is \"Near\" - Handling differently"

         result_list = {}
         for particle in particle_list: 
            """
            Score a particular particle by iterating through all nodes.
            """
            valid_supernodes = []
            for n in particle.nodelist:
               if n.is_supernode:                             
                  if len(n.bounding_box) == 0:
                     print "Skipping supernode - has no valid bounding box"
                     continue
                  valid_supernodes.append(n)
                  
        #the paths being evaluated is the same independant of the figure 
        #this can be calculated at the start

            here_map_node = particle.get_current_position_node()    

            particle_result = {}

            for figure_node in valid_supernodes: 
               if figure_node.id <= particle.last_checked_id:
                    continue

               landmark_total_prob = 0
               total_grounding_prob = 0
               for landmark_node in valid_supernodes: 
                  if figure_node == landmark_node:
                    continue

                  #skip invalid landmark nodes 
                  if(landmark_node.label_dist.is_uniform()):
                     continue

                  if(landmark_node.label_dist.getprobability_index(landmark_ind) < 0.2):
                    continue

                  if sf.math2d_dist(here_map_node.xy, figure_node.xy) > figure_distance_threshold:
                     continue

                  if sf.math2d_dist(here_map_node.xy, landmark_node.xy) > landmark_distance_threshold:
                     continue            
                  
                  print "Figure Node : " , figure_node.id , " Region : " , figure_node.parent_supernode , " Label : " ,  figure_node.get_main_label()
                  print "Landmark Node : " , landmark_node.id , " Region : " , landmark_node.parent_supernode, " Label : " ,  landmark_node.get_main_label()
                  
                  dx = near_mean_dist - sf.math2d_dist(figure_node.xy, landmark_node.xy)

                  ### Log scale 
                  path_prob = 1/ (1+ math.exp(-dx))

                  landmark_prob = landmark_node.label_dist.getprobability_index(landmark_ind)
                  
                  landmark_total_prob += landmark_prob

                  total_grounding_prob += landmark_prob * path_prob

                  print "Path Prob " , path_prob
                  print "Landmark Prob " , landmark_prob

                  grounding_result = {'node': landmark_node, 'path_prob': path_prob, 'landmark_prob': landmark_prob, 'ground_prob': path_prob * landmark_prob}

                  if(particle_result.has_key(figure_node.id)):
                     particle_result[figure_node.id]['landmark_list'].append(grounding_result)
                  else:
                     particle_result[figure_node.id] = {'landmark_list': [], 'total_prob': 0,  'path': []}
                     particle_result[figure_node.id]['landmark_list'].append(grounding_result)
                     
               if(particle_result.has_key(figure_node.id)):
                  particle_result[figure_node.id]['total_prob'] = total_grounding_prob / landmark_total_prob
                  print "Figure " , figure_node.id, " => Total Grounding Prob : " , total_grounding_prob / landmark_total_prob
                  for i in particle_result[figure_node.id]['landmark_list']:
                     i['ground_prob'] = i['ground_prob'] / landmark_total_prob
                     print "\tLandmark : ", i['node'].id, " -> Grounding Prob : " ,  i['ground_prob'] , " - Path : ", i['path_prob']
                     
               if(len(particle_result.keys()) > 0): 
                  result_list[particle.id] = {'particle': particle, 'labels': {figure_ind:particle_result}}
                  print "Done computing near"

         self.send_slu_update(result_list, language_event)

      else:
      #this should ideally be built from the lcm message sent over 
         #we want to populate the node's label distribution with likelyhood of generating the landmark 
         #label given the region's label distribution 
         
         ## Call SLU 
         figure_text, results = self.slu.score_particles(label, landmark_ind, particle_list)

         print "Done Processing"
         print "Figure Text", figure_text

         ### Done - lets package this up and send it off to iSAM

         ### Add this back in for drawing the results 
         #starting_context = particle_to_context(results[0][0])

         result_to_send = {}

         print "--------------------------------------------"

         '''figure_grounding_map = {}

         for particle, result_list in results:
            figure_grounding_map[particle.id] = {}
            for i in result_list:
               if(figure_grounding_map[particle.id].has_key(i.figure_map_node.id)):
                  figure_grounding_map[particle.id][i.figure_map_node.id].append(i)
               else:
                  figure_grounding_map[particle.id][i.figure_map_node.id] = [i]'''

         ##The message should tell us which language to ignore 

         figure_grounding_result = {}

         for particle, result_list in results:         
            for k in figure_text.keywords: 
               if self.keyword_lookup.has_key(k):
                  print "Label " , k , " Label Ind : " , self.keyword_lookup[k]  

            p_result = {'particle': particle, 'labels': {}}

            here_map_node = particle.get_current_position_node() 

            figure_groundings = {}

            label_id = -1
            if(self.keyword_lookup.has_key(figure_text.keywords[0])):
               label_id = self.keyword_lookup[k]  

            if (label_id == -1):
               print "Label index not found - exiting"
               continue

            for i in result_list:
               if(figure_groundings.has_key(i.figure_map_node.id)):
                  figure_groundings[i.figure_map_node.id].append(i)
               else:
                  figure_groundings[i.figure_map_node.id] = [i]


            figure_probabilities = {}
            ##Do the grounding prob calculation here 
            for fig in figure_groundings.keys():
               total_landmark_likelihood = 0  

               parent_supernode = figure_groundings[fig][0].figure_map_node.parent_supernode

               path = particle.get_pygraph_path_nodes(here_map_node, figure_groundings[fig][0].figure_map_node)

               print "\tFigure Node ", fig , " Region : " , parent_supernode
               for grnd in figure_groundings[fig]:
                  total_landmark_likelihood += math.exp(-grnd.landmark_cost)

               total_grounding_likelihood = 0

               landmark_list = []

               if(total_landmark_likelihood == 0):
                  total_grounding_likelihood = 0
               else:
                  for grnd in figure_groundings[fig]:
                     landmark_prob = math.exp(-grnd.landmark_cost) / total_landmark_likelihood
                     grounding_prob = math.exp(-grnd.path_cost) * landmark_prob
                     lamdmark = {'node': grnd.landmark_map_node, 'ground_prob': grounding_prob, 'landmark_prob': landmark_prob, 'path_prob': math.exp(-grnd.path_cost)}
                     landmark_list.append(lamdmark)
                     total_grounding_likelihood += grounding_prob
                     print "\t Landmark : ", grnd.landmark_map_node.parent_supernode , " : ", grounding_prob, " - Path : " , math.exp(-grnd.path_cost)               
               print "\tGrounding Likelihood : ", total_grounding_likelihood

               figure_probabilities[fig] = {'total_prob': total_grounding_likelihood, 'path': path, 'landmark_list': landmark_list}

            ## add the results for the particles 
            p_result['labels'][label_id] = figure_probabilities

            figure_grounding_result[particle.id] = p_result

         self.send_slu_update(figure_grounding_result, language_event)

         #self.send_extensive_slu_update(figure_grounding_result, sr, fig , landmark, language_event)

         '''grounding_results = []
         for part in figure_grounding_map.keys():
            print "Particle : " , part 
            figure_result = {}
            figure_map = figure_grounding_map[part]
            for fig in figure_map.keys():

               total_landmark_likelihood = 0   

               parent_supernode = figure_map[fig][0].figure_map_node.parent_supernode

               print "\tFigure Node ", fig , " Region : " , parent_supernode
               for grnd in figure_map[fig]:
                  total_landmark_likelihood += math.exp(-grnd.landmark_cost)

               total_grounding_likelihood = 0
               for grnd in figure_map[fig]:
                  grounding_prob = math.exp(-grnd.path_cost) * math.exp(-grnd.landmark_cost) / total_landmark_likelihood
                  total_grounding_likelihood += grounding_prob
                  print "\tt Landmark : ", grnd.landmark_map_node.parent_supernode , " : ", grounding_prob               
               print "\tGrounding Likelihood : ", total_grounding_likelihood

               figure_result[fig] = total_grounding_likelihood
            grounding_results[part] = figure_result'''

         '''for particle, result_list in results:
            print "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\nPARTICLE\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            print particle

            result_for_particle = {'particle': particle , 'labels': {}}
            for k in figure_text.keywords: 
               if self.keyword_lookup.has_key(k):
                  print "Label " , k , " Label Ind : " , self.keyword_lookup[k]  
                  l = self.keyword_lookup[k]  
                  #this is the result for each particle 
                  nodes_to_update = []
                  for result in result_list:
                     ## result.figure_map_node  # this is the node for which the prob is returned (not the id - the node) 
                     ### wonder what the figure text is ?? - can it be more than one label
                     ## result.probability is the liklihood 
                     #print "Node ID: " + str(result.figure_map_node.id) + " => Prob " + str(result.probability)
                     res = [result.figure_map_node.id, result.probability]
                     nodes_to_update.append(res)

                  result_for_particle['labels'][l] = nodes_to_update
               else:
                  print "Error - No label index found for " , k
            result_to_send[particle.id] = result_for_particle'''


         #self.send_slu_update(result_to_send)
         ## These tend to cause segfault in Sachi's machine
         print "Calling show"
         #featureBrowser = crfFeatureWeights.MainWindow()
         #featureBrowser.show()
         #if(len(results) > 0):
         #   particle, result_list = results[0]
         #   if(len(result_list) > 0):
         #      self.featureBrowser.load(starting_context, self.slu.cost_function, result_list[0].path_factor)

         print "Done"
         #import cPickle
         #cPickle.dump((self.particles, channel, data), open("label_handler_args.pck", "w"))

         #self.app.exec_() 

def main():
   import signal
   import os
   
   #app = basewindow.makeApp()
   #slu = slu_api.SluApi()
   
   def exitAll():
        os.killpg(os.getpgrp(), signal.SIGINT)

   def handler(sig, arg):
        os.kill(os.getpid(), signal.SIGTERM)
   signal.signal(signal.SIGINT, handler)
      

        
   app = basewindow.makeApp()

   lcm = lcm_listener()

   for fileno in lcm.lcm_filenos():
      socket_notifier = QSocketNotifier(fileno, QSocketNotifier.Read)
      app.connect(socket_notifier,
                  SIGNAL("activated(int)"),
                  lcm.activated)

   
   #app.connect(socket_notifier,
   #            SIGNAL("activated(int)"),
   #            lcm.activated)

   #timer = QTimer()
   #timer.setInterval(100)
   #timer.setSingleShot(False)
   #app.connect(timer, SIGNAL('timeout()'),print_status)
                          #glWidget.spin)
   #timer.start()

   #featureBrowser = crfFeatureWeights.MainWindow()
   #featureBrowser.show()


   #while bg_thread.is_alive():
      #bg_thread.join(1)
   print "Calling App.exec"
   app.exec_()
      
if __name__ == "__main__":
   main()

