# Edge class for topological graph SLAM

class Edge():
   def __init__(self, node1 = 0, node2 =0, _id = -1, status = 0, type_edge=1):
      #flag = 0: initialized
      #flag = 1: successful edge
      #flag = 2: failed edge
      #type = 0: unknown
      #type = 1: odometry
      #type = 2: scanmatch
      #type = 4: language
      self.node1 = node1
      self.node2 = node2
      self.edge_id = _id
      self.status = status
      self.type = type_edge
      self.transformation = []
      self.cov = []
      self.new = False
