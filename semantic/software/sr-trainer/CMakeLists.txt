cmake_minimum_required(VERSION 2.6.0)

# pull in the pods macros. See cmake/pods.cmake for documentation
set(POD_NAME sr-trainer)
include(cmake/pods.cmake)

include(cmake/lcmtypes.cmake)
lcmtypes_build()

lcmtypes_build(C_AGGREGATE_HEADER sr_trainer.h)

include_directories(${LCMTYPES_INCLUDE_DIRS})

add_subdirectory(src)
add_subdirectory(src/test)
add_subdirectory(src/renderer)
