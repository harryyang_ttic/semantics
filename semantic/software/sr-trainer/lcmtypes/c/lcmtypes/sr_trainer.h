#ifndef __lcmtypes_sr_trainer_h__
#define __lcmtypes_sr_trainer_h__

#include "slu_path_t.h"
#include "slu_annotation_request_t.h"
#include "slu_annotation_list_t.h"
#include "slu_annotation_t.h"
#include "slu_pose_t.h"
#include "slu_point_t.h"
#include "slu_object_t.h"

#endif
