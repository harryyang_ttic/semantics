#ifndef FEATURE_EXTRACTOR_H
#define FEATURE_EXTRACTOR_H 

#include <slu_spatial_features/spatial_feature_extractor.h>

#ifdef __cplusplus
extern "C" {
#endif

    //sf.flu_polygon_names();
    namespace spatial_features{

        vector<string> object_feature_names(vector<string> figure);

        gsl_vector* object_feature_values(vector<string> figure, 
                                          gsl_matrix* gnd1_xy, 
                                          bool perform_scaling);
    
        //sf.flu_polygon 

        //sfe_extract_f_path_l_polygon

        vector<string> path_feature_names();

        //sfe_f_path_l_polygon_names

        gsl_vector* path_feature_values(gsl_matrix* fig1_xyth, 
                                        gsl_matrix* gnd1_xy, 
                                        bool normalize);    
        //sfe_extract_f_path_l_polygon
        int rectify_path_features(const vector<string> names, gsl_vector *v, vector<string> &new_names, bool clamp=false, double threshold = 100000);

        void print_features(const vector<string> &names, const gsl_vector *v);

        void print_gsl_matrix(gsl_matrix *m);
    }

    


#ifdef __cplusplus
}
#endif

#endif
