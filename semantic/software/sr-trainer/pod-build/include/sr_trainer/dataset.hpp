#ifndef SR_DATASET_H_
#define SR_DATASET_H_

#include <gsl/gsl_randist.h>
#include "annotation.hpp"

#include <shark/Data/Csv.h> //importing the file
#include <shark/Algorithms/Trainers/RFTrainer.h> //the random forest trainer
#include <shark/ObjectiveFunctions/Loss/ZeroOneLoss.h> //zero one loss for evaluation

using namespace std;
using namespace shark;

namespace sr_dataset{

    struct annotation_corpus{
        string name;
        vector<Annotation> datapoints;
        slu_annotation_list_t *msg;
        bool added;

        annotation_corpus(string _name):name(_name), msg(NULL), added(false) {}

        void add(Annotation dp){
            datapoints.push_back(dp);
            added = true;
        }

        void print(){
            cout << "Relation : " << name << " => " << datapoints.size() << endl;
        }

        void update_dataset(){
            if(!added)
                return;

            if(msg != NULL){
                slu_annotation_list_t_destroy(msg);
            }

            msg = (slu_annotation_list_t *) calloc(1,sizeof(slu_annotation_list_t));
            msg->count = (int) datapoints.size();
            msg->annotations = (slu_annotation_t *) calloc(msg->count, sizeof(slu_annotation_t));
            for(int i=0; i < datapoints.size(); i++){
                datapoints[i].to_lcm(&msg->annotations[i]);
            }
            added = false;
        }

        void publish_dataset(lcm_t *lcm){
            update_dataset();
            slu_annotation_list_t_publish(lcm, "SLU_ANNOTATIONS", msg);
        }
    };


    struct rf_result {
        string relation;
        int dataset_size;
        double training_accuracy;
        double test_accuracy;

        rf_result(string _relation, int _size, double tr_a, double te_a): relation(_relation), dataset_size(_size),
                                                                          training_accuracy(tr_a), test_accuracy(te_a) {}

        void print(){
            cout << relation << " [" << dataset_size << "] - " << training_accuracy << " / " << test_accuracy << endl;
        }
    };

    struct datapoint{
        RealVector features;
        unsigned int label;
        Annotation annotation;
        datapoint(Annotation _annotation, RealVector _features, unsigned int _label): features(_features), label(_label), annotation(_annotation) {}
    };

    struct dataset{
        string relation;
        //std::vector<unsigned int> positive_labels;

        vector<datapoint> p_corpus;
        vector<datapoint> n_corpus;

        std::vector<unsigned int> labels;
        std::vector<RealVector> points;
        vector<datapoint> sorted_full_dataset;

        ClassificationDataset data;
        bool c_data_updated;
        gsl_rng *rng;

        dataset(string _relation, gsl_rng *_rng):relation(_relation), c_data_updated(false), rng(_rng) {}

        void add_data(Annotation annot, RealVector data, unsigned int l){
            /*points.push_back(data);
              labels.push_back(l);*/
            if(l){
                p_corpus.push_back(datapoint(annot, data, l));
            }
            else{
                n_corpus.push_back(datapoint(annot, data, l));
            }

            c_data_updated = false;
        }

        double get_true_ratio(){
            int total_count = get_total_count();
            int positive_count = get_positive_count();

            if(total_count)
                return positive_count / (double) total_count;
        }

        int get_positive_count(){
            return p_corpus.size();
        }

        int get_total_count(){
            return (p_corpus.size() + n_corpus.size());
        }

        void create_classification_data(){
            data = createLabeledDataFromRange(points, labels);
            c_data_updated = true;
        }

        vector<datapoint> sample_count(int count, vector<datapoint> &orig){

            if(orig.size() <= count){
                return orig;
            }
            else{

                vector<datapoint> samples;

                fprintf(stderr, "Number of total examples : %d -> Sampled down to : %d\n", (int) orig.size(), count);
                int size = orig.size();
                int a[count], b[size];

                for (int i = 0; i < size; i++){
                    b[i] = i;
                }

                gsl_ran_choose (rng, a, count, b, size, sizeof (int));

                for(int i=0; i < count; i++){
                    samples.push_back(orig[a[i]]);
                }

                return samples;
            }
        }

        vector<datapoint> sort_samples(vector<datapoint> &orig){
            vector<datapoint> samples;

            int size = orig.size();

            int a[size];

            for (int i = 0; i < size; i++){
                a[i] = i;
            }

            gsl_ran_shuffle (rng, a, size, sizeof (int));

            for(int i=0; i < size; i++){
                samples.push_back(orig[a[i]]);
            }

            return samples;
        }


        void update_balanced_dataset(){
            int num_positive_examples = get_positive_count();
            if(num_positive_examples < n_corpus.size()){
                vector<datapoint> p_samples = sort_samples(p_corpus);
                vector<datapoint> n_samples = sample_count(num_positive_examples, n_corpus);
                cout << "Sampled Dataset size\n" << n_samples.size() << endl;

                vector<datapoint> full_dataset;

                for(int i=0; i < p_samples.size(); i++){
                    full_dataset.push_back(p_samples[i]);
                }

                for(int i=0; i < n_samples.size(); i++){
                    full_dataset.push_back(n_samples[i]);
                }

                sorted_full_dataset.clear();
                sorted_full_dataset = sort_samples(full_dataset);
                //we should sort the other one
                labels.clear();
                points.clear();

                fprintf(stdout, "Balanced dataset size : %d\n", (int) sorted_full_dataset.size());

                for(int i=0; i< sorted_full_dataset.size(); i++){
                    points.push_back(sorted_full_dataset[i].features);
                    labels.push_back(sorted_full_dataset[i].label);
                }
            }
        }

        void add_to_datasets(double train_ratio, vector<datapoint> &trained_datapoints, vector<datapoint> &test_datapoints){
            int train_count = sorted_full_dataset.size() * train_ratio;

            for(int i=0; i < train_count; i++){
                trained_datapoints.push_back(sorted_full_dataset[i]);
            }
            for(int i=train_count; i < sorted_full_dataset.size(); i++){
                test_datapoints.push_back(sorted_full_dataset[i]);
            }
        }

        ClassificationDataset get_full_data(){
            std::vector<unsigned int> f_labels;
            std::vector<RealVector> f_points;

            vector<datapoint> full_dataset;
            for(int i=0; i < p_corpus.size(); i++){
                full_dataset.push_back(p_corpus[i]);
            }

            for(int i=0; i < n_corpus.size(); i++){
                full_dataset.push_back(n_corpus[i]);
            }

            for(int i=0; i< full_dataset.size(); i++){
                f_points.push_back(full_dataset[i].features);
                f_labels.push_back(full_dataset[i].label);
            }

            ClassificationDataset full_data = createLabeledDataFromRange(f_points, f_labels);
            return full_data;
        }

        ClassificationDataset get_full_positive_data(){
            std::vector<unsigned int> f_labels;
            std::vector<RealVector> f_points;

            vector<datapoint> full_dataset;
            for(int i=0; i < p_corpus.size(); i++){
                full_dataset.push_back(p_corpus[i]);
            }

            for(int i=0; i< full_dataset.size(); i++){
                f_points.push_back(full_dataset[i].features);
                f_labels.push_back(full_dataset[i].label);
            }

            ClassificationDataset full_data = createLabeledDataFromRange(f_points, f_labels);
            return full_data;
        }

        vector<Annotation> get_full_positive_annotations(){
            vector<Annotation> result;
            for(int i=0; i < p_corpus.size(); i++){
                result.push_back(p_corpus[i].annotation);
            }
            return result;
        }

        vector<Annotation> get_full_negative_annotations(){
            vector<Annotation> result;
            for(int i=0; i < n_corpus.size(); i++){
                result.push_back(n_corpus[i].annotation);
            }
            return result;
        }

        vector<Annotation> get_full_annotations(){
            vector<Annotation> result;
            for(int i=0; i < p_corpus.size(); i++){
                result.push_back(p_corpus[i].annotation);
            }

            for(int i=0; i < n_corpus.size(); i++){
                result.push_back(n_corpus[i].annotation);
            }
            return result;
        }


        ClassificationDataset get_full_negative_data(){
            std::vector<unsigned int> f_labels;
            std::vector<RealVector> f_points;

            vector<datapoint> full_dataset;

            for(int i=0; i < n_corpus.size(); i++){
                full_dataset.push_back(n_corpus[i]);
            }

            for(int i=0; i< full_dataset.size(); i++){
                f_points.push_back(full_dataset[i].features);
                f_labels.push_back(full_dataset[i].label);
            }

            ClassificationDataset full_data = createLabeledDataFromRange(f_points, f_labels);
            return full_data;
        }

        ClassificationDataset get_classification_data(){
            update_balanced_dataset();
            //if(!c_data_updated)
            create_classification_data();

            return data;
        }

        void print(){
            cout << "Relation : " << relation << " => " << get_positive_count() << " / " << get_total_count() << endl;
        }
    };
}
#endif
