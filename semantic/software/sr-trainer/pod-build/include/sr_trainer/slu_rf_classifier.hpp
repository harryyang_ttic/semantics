#ifndef SLU_RF_CLASSIFIER_
#define SLU_RF_CLASSIFIER_

#include "feature_extractor.hpp"
#include "dataset.hpp"
#include <boost/archive/polymorphic_text_iarchive.hpp>
#include <bot_param/param_client.h>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>

using namespace sr_dataset;
using namespace std;
using namespace shark;

class SLUClassifier{
  
public:

    SLUClassifier(char *path, bool verbose=false);
    
    void fill_spatial_relations();

    vector<double> get_likelihood(string sr, vector<Annotation> &annotation); 

    vector<double> get_likelihood(string sr, vector<SLUDataPoint> &data, bool publish=false, bool verb=false);//, lcm_t *lcm=NULL); 

    vector<double> get_hack_likelihood(string sr, vector<SLUDataPoint> &data);
    
    static RealVector get_data_vector(gsl_vector *v);
    
    void publish_dataset(string sr, vector<SLUDataPoint> &datapoints);

    void publish_dataset_result(string sr, vector<SLUDataPoint> &datapoints, vector<double> prob);

    Command parse_language(string command);    

    void print_bot_trans(const BotTrans &bt) const;

private:
    double get_sigmoid_value(double val, double mean, double threshold, double sigma);
    double get_sigmoid_value_angle(double val, double mean, double threshold, double sigma);
    map<string, RFClassifier> model_map; 
    map<string, RFClassifier>::iterator it_model;
    
    vector<string> spatial_relations; //should this be loaded before hand?? 
    
    lcm_t *lcm;
    bool verbose; 
};

#endif
