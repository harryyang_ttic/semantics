#ifndef __lcmtypes_sr_trainer_hpp__
#define __lcmtypes_sr_trainer_hpp__

#include "slu/annotation_list_t.hpp"
#include "slu/object_t.hpp"
#include "slu/pose_t.hpp"
#include "slu/annotation_request_t.hpp"
#include "slu/path_t.hpp"
#include "slu/point_t.hpp"
#include "slu/annotation_t.hpp"

#endif
