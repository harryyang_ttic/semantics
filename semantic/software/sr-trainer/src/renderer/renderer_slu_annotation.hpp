#ifndef SLU_ANNOTATION_RENDERER_H_
#define SLU_ANNOTATION_RENDERER_H_

#ifdef __cplusplus
extern "C" {
#endif
void setup_renderer_slu_annotation (BotViewer *viewer, int priority, BotParam * param);
#ifdef __cplusplus
}
#endif
#endif
