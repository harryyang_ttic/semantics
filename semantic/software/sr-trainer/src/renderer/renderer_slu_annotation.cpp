#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <glib.h>
#include <string.h>
#include <assert.h>
#include <gdk/gdkkeysyms.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

#include <bot_vis/bot_vis.h>

#include <bot_core/bot_core.h>
#include <bot_vis/viewer.h>
#include <bot_vis/gtk_util.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>
#include <bot_core/math_util.h>
#include <geom_utils/geometry.h>

#include <lcmtypes/sr_trainer.h>
#include <vector>
#include <string>
#include <map>
#include <iostream>
using namespace std;

#define PARAM_REQUEST_ANNOTATIONS "Request Annotation"
#define PARAM_SLU_INDEX "Annotation index"
#define PARAM_DRAW_ALL "Draw all annotations"
#define PARAM_SELECT_POSITIVE "(+) Annotations"
#define PARAM_SELECT_NEGATIVE "(-) Annotations"
#define PARAM_SET_FOCUS "Focus view"
#define PARAM_DRAW_TEXT "Draw Text"
#define RENDERER_NAME "SLU Annotation Renderer"
#define PARAM_SR_NAME "Spatial Relation"
#define PARAM_CENTER_ANNOTATION "Center Annotations"
#define PARAM_CLEAR_ANNOTATION "Clear Annotations"


struct sr_annotation_list{
    string relation;
    vector<slu_annotation_t *> pos_annot;
    vector<slu_annotation_t *> neg_annot;
    vector<slu_annotation_t *> all_annot;

    sr_annotation_list(string _relation):relation(_relation) {}

    void add_annotation(slu_annotation_t *annotation){
        if(annotation->path_true){
            pos_annot.push_back(annotation);
            all_annot.push_back(annotation);
        }
        else{
            neg_annot.push_back(annotation);
            all_annot.push_back(annotation);
        } 
    }

    void clear(){
        for(int i=0; i < all_annot.size(); i++){
            slu_annotation_t_destroy(all_annot[i]);
        }
        all_annot.clear();
        pos_annot.clear();
        neg_annot.clear();
    }
    
    void print(){
        cout << "Relation : " << relation << " Size : " << all_annot.size() << endl; 
    }
};

struct RendererSLUAnnotation {
    BotRenderer renderer;
    BotEventHandler ehandler;
    
    lcm_t *lcm;
    BotParam *param;

    slu_annotation_list_t *a_msg;

    BotViewer         *viewer;
    BotGtkParamWidget *pw;   

    vector<string> spatial_relations;

    int *sr_id;
    char **sr_description; 

    int draw_ind; 
    
    bool draw_text;
    bool draw_all;
    bool draw_positive; 
    bool draw_negative; 
    bool center_annotation; 

    string active_sr;

    map<string, sr_annotation_list> annotation_map; 

    double footprint[8];

    double fp_length;
    double fp_width;

    double front_middle[2];

    sr_annotation_list *active_list; 

    RendererSLUAnnotation(){
        active_list = NULL; 
        active_sr = string("across");
        spatial_relations.push_back("across");
        spatial_relations.push_back("along");
        spatial_relations.push_back("around");
        spatial_relations.push_back("away from");
        spatial_relations.push_back("down");
        spatial_relations.push_back("out");
        spatial_relations.push_back("past");
        spatial_relations.push_back("straight");
        spatial_relations.push_back("through");
        spatial_relations.push_back("to");
        spatial_relations.push_back("towards");
        spatial_relations.push_back("turnLeft");
        spatial_relations.push_back("turnRight");
        spatial_relations.push_back("until");
        spatial_relations.push_back("near");
        spatial_relations.push_back("behind");

        sr_id = (int *) calloc(spatial_relations.size(), sizeof(int));
        sr_description = (char **) calloc(spatial_relations.size(), sizeof(char *));
        for(int i=0; i < spatial_relations.size(); i++){
            sr_id[i] = i;
            sr_description[i] = strdup(spatial_relations[i].c_str());
        }
        draw_ind = -1;
        a_msg = NULL;
        draw_all = false;
        draw_text = true;
        draw_positive = true;
        draw_negative = true;
        center_annotation = true;
    }
};

static void send_sr_request(RendererSLUAnnotation *self, char *name){
    slu_annotation_request_t msg; 
    msg.utime = bot_timestamp_now();
    msg.relation = name;
    msg.type = SLU_ANNOTATION_REQUEST_T_TYPE_ALL;
    slu_annotation_request_t_publish(self->lcm, "SLU_ANNOTATION_REQUEST", &msg);
}

static void draw_g_triangle(RendererSLUAnnotation *self, double pos[3], double theta, double color_ratio, double scale = 1.0){
    glColor3fv(bot_color_util_jet(color_ratio));

    glPushMatrix();
    glTranslated(pos[0], pos[1], pos[2]);
    glRotatef(theta * 180 / M_PI, 0, 0, 1);
    glScalef (scale, scale, scale);
    glPushAttrib (GL_ENABLE_BIT | GL_DEPTH_BUFFER_BIT);
    glDisable (GL_DEPTH_TEST);
    glLineWidth(2);
    glEnable(GL_POLYGON_OFFSET_FILL);
    glPolygonOffset (1, 1);

    glBegin(GL_TRIANGLES);
    glVertex3f( self->front_middle[0], self->front_middle[1], 0);
    glVertex3f( self->footprint[6], self->footprint[7], 0); 
    glVertex3f(  self->footprint[4], self->footprint[5], 0);
    glEnd();
    glPopAttrib ();
    glPopMatrix();
}

static void set_active_relation(RendererSLUAnnotation *self){
    int sr_ind = bot_gtk_param_widget_get_enum(self->pw, PARAM_SR_NAME);
    self->active_sr = self->spatial_relations[sr_ind]; 
    
    map<string, sr_annotation_list>::iterator it;
    it = self->annotation_map.find(self->active_sr);
    if(it != self->annotation_map.end()){
        self->active_list = &it->second;
    }
    else{
        self->active_list = NULL;
    }
}

static void draw_g_circle(double pos[3], double color_ratio, double resolution_radians, double radius){
    double theta = 0;
        
    int count = fmax(2,ceil(2 *M_PI / resolution_radians));
    double delta =  2* M_PI/count;
    glColor3fv(bot_color_util_jet(color_ratio));

    glBegin(GL_TRIANGLE_FAN);
    glVertex3d(pos[0], pos[1], pos[2]);
    glVertex3d(pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2]);
    for(int k=0; k< count; k++){
        theta += delta;
        glVertex3d( pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2]);
    }
    glEnd();
}

static void update_g_params(RendererSLUAnnotation *self, BotGtkParamWidget *pw){
    self->draw_ind = bot_gtk_param_widget_get_int(self->pw, PARAM_SLU_INDEX);
    self->draw_all = bot_gtk_param_widget_get_bool(self->pw, PARAM_DRAW_ALL);
    self->draw_positive = bot_gtk_param_widget_get_bool(self->pw, PARAM_SELECT_POSITIVE);
    self->draw_negative = bot_gtk_param_widget_get_bool(self->pw, PARAM_SELECT_NEGATIVE);
    self->draw_text = bot_gtk_param_widget_get_bool(self->pw, PARAM_DRAW_TEXT);
    self->center_annotation = bot_gtk_param_widget_get_bool(self->pw, PARAM_CENTER_ANNOTATION);    
}

static void update_sliders(RendererSLUAnnotation *self){
    if(!self->active_list){
        bot_gtk_param_widget_set_enabled (self->pw, PARAM_SLU_INDEX, 0);
        return;
    }
    if(self->draw_positive && self->draw_negative){
        if(self->active_list->all_annot.size()){
            bot_gtk_param_widget_modify_int(self->pw, PARAM_SLU_INDEX, 0, self->active_list->all_annot.size() - 1, 1, 0);
            bot_gtk_param_widget_set_enabled (self->pw, PARAM_SLU_INDEX, 1);
        }
        else{
            bot_gtk_param_widget_set_enabled (self->pw, PARAM_SLU_INDEX, 0);
        }
    }
    else if(self->draw_positive){
        if(self->active_list->pos_annot.size()){
            bot_gtk_param_widget_modify_int(self->pw, PARAM_SLU_INDEX, 0, self->active_list->pos_annot.size() - 1, 1, 0);            
            bot_gtk_param_widget_set_enabled (self->pw, PARAM_SLU_INDEX, 1);
        }        
        else{
            bot_gtk_param_widget_set_enabled (self->pw, PARAM_SLU_INDEX, 0);
        }
    }
    else if(self->draw_negative){
        if(self->active_list->neg_annot.size()){
            bot_gtk_param_widget_modify_int(self->pw, PARAM_SLU_INDEX, 0, self->active_list->neg_annot.size() - 1, 1, 0);
            bot_gtk_param_widget_set_enabled (self->pw, PARAM_SLU_INDEX, 1);
        }
        else{
            bot_gtk_param_widget_set_enabled (self->pw, PARAM_SLU_INDEX, 0);
        }
    }
}

static void on_annotation (const lcm_recv_buf_t *rbuf, const char *channel,
                           const slu_annotation_list_t *msg, void *user)
{
    RendererSLUAnnotation *self = (RendererSLUAnnotation *)user;
    g_assert(self);

    if(bot_gtk_param_widget_get_bool(self->pw, PARAM_CLEAR_ANNOTATION)){        
        map<string, sr_annotation_list>::iterator it;
        for(it = self->annotation_map.begin(); it != self->annotation_map.end(); it++){
            it->second.clear();
        }
        //self->annotation_map.clear();        
    }

    if(self->a_msg){        
        self->active_list = NULL;
        slu_annotation_list_t_destroy(self->a_msg);
    }

    self->a_msg = slu_annotation_list_t_copy(msg);

    fprintf(stderr, "Number of examples : %d\n", self->a_msg->count);

    if(self->a_msg->count > 0){
        map<string, sr_annotation_list>::iterator it;

        for(int i=0; i < self->a_msg->count; i++){
            string rel(self->a_msg->annotations[i].relation);
            
            it = self->annotation_map.find(rel);
            if(it != self->annotation_map.end()){
                it->second.add_annotation(slu_annotation_t_copy(&self->a_msg->annotations[i]));
                fprintf(stderr, "Adding annotation : %s\n", rel.c_str());
                it->second.print();
            }
            else{
                sr_annotation_list annot_list(rel);
                annot_list.add_annotation(slu_annotation_t_copy(&self->a_msg->annotations[i]));
                self->annotation_map.insert(make_pair(rel, annot_list));
                fprintf(stderr, "Adding annotation : %s\n", rel.c_str());
                annot_list.print();
            }
        }

        set_active_relation(self);

        update_sliders(self);
        
        update_g_params(self, self->pw);
    }
    bot_viewer_request_redraw (self->viewer);
}

static void
renderer_slu_annotation_destroy (BotRenderer *renderer)
{
    fprintf(stderr, "Called destroy\n");

    if (!renderer)
        return;

    RendererSLUAnnotation *self = (RendererSLUAnnotation *) renderer->user;

    if (!self)
        return;
    
    free (self);
}

/*static void set_focus(RendererSLUAnnotation *self){
    if(self->draw_all)
        return;
    if(self->a_msg && self->draw_ind > 0 && self->draw_ind < self->a_msg->count){
        slu_annotation_t *annot = &self->a_msg->annotations[self->draw_ind];
        slu_path_t *path = &annot->path;
        
        if(path->count > 1 && self->focus){
            BotViewHandler *vhandler = self->viewer->view_handler;

            double eye[3];
            double lookat[3];
            double up[3];

            BotTrans last;
            last.trans_vec[0] = path->points[0].pt.xyz[0];
            last.trans_vec[1] = path->points[0].pt.xyz[1];
            last.trans_vec[2] = path->points[0].pt.xyz[2];

            bot_roll_pitch_yaw_to_quat(path->points[0].rpy, last.rot_quat);
            
            vhandler->get_eye_look(vhandler, eye, lookat, up);
            double diff[3];
            bot_vector_subtract_3d(eye, lookat, diff);
            
            bot_vector_add_3d(last.trans_vec, diff, eye);
            
            vhandler->set_look_at(vhandler, eye, last.trans_vec, up);
        }
    }
    }*/

static void draw_slu_annotation(RendererSLUAnnotation *self, slu_annotation_t *annot, bool draw_positive=false){

    slu_path_t *path = &annot->path;
    slu_object_t *lm = &annot->landmark; 

    if(draw_positive && !annot->path_true)
        return;

    if(path->count == 0){
        return;
    }

    //draw the path 
    glLineWidth(5.0);
    if(annot->path_true){
        glColor3fv(bot_color_util_green);
    }
    else{
        glColor3fv(bot_color_util_red);
    }
    
    double xy[2] = {0,0};

    if(self->center_annotation){
        xy[0] = path->points[0].pt.xyz[0];
        xy[1] = path->points[0].pt.xyz[1];
    }

    if(path->count > 1){        
        glBegin (GL_LINES);
        for(int i=1; i < path->count; i++){
            glVertex3d (path->points[i-1].pt.xyz[0] - xy[0], path->points[i-1].pt.xyz[1] - xy[1], path->points[i-1].pt.xyz[2]);
            glVertex3d (path->points[i].pt.xyz[0] - xy[0], path->points[i].pt.xyz[1] - xy[1], path->points[i].pt.xyz[2]);
        }    
        glEnd();
    }

    for(int i=0; i < path->count; i++){
        double xyz[3] = {path->points[i].pt.xyz[0] - xy[0], path->points[i].pt.xyz[1] - xy[1], path->points[i].pt.xyz[2]}; 
        draw_g_triangle(self, xyz, path->points[i].rpy[2], 0.0);
    }

    glColor3fv(bot_color_util_blue);
    if(lm->count > 1){
        /*glBegin (GL_LINE_LOOP);
        for(int i=1; i < lm->count; i++){
            glVertex3d (lm->upper_points[i-1].xyz[0], lm->upper_points[i-1].xyz[1], lm->upper_points[i-1].xyz[2]);
            glVertex3d (lm->upper_points[i].xyz[0], lm->upper_points[i].xyz[1], lm->upper_points[i].xyz[2]);
        }    
        glEnd();*/

        glBegin (GL_LINE_LOOP);
        for(int i=1; i < lm->count; i++){
            glVertex3d (lm->lower_points[i-1].xyz[0] - xy[0], lm->lower_points[i-1].xyz[1] - xy[1], lm->lower_points[i-1].xyz[2]);
            glVertex3d (lm->lower_points[i].xyz[0] - xy[0], lm->lower_points[i].xyz[1] - xy[1], lm->lower_points[i].xyz[2]);
        }    
        glEnd();
    }    
    
    if(self->draw_text){
        double textpos[3] = {path->points[0].pt.xyz[0] - xy[0] + 5.0, path->points[0].pt.xyz[1] - xy[1] + 5.0, path->points[0].pt.xyz[2] + 0.1};
        glColor3f(1.0,1.0,1.0);
        char text[100];
        if(annot->relation){
            sprintf(text, "%s : %.2f", annot->relation, annot->prob);
        }
        else{
            sprintf(text, "Prob : %.2f", annot->prob);
        }
        bot_gl_draw_text(textpos, GLUT_BITMAP_HELVETICA_12, text,
                         BOT_GL_DRAW_TEXT_DROP_SHADOW);
    }
}

static void 
renderer_slu_annotation_draw (BotViewer *viewer, BotRenderer *renderer)
{
    RendererSLUAnnotation *self = (RendererSLUAnnotation*)renderer->user;
    g_assert(self);

    if(self->active_list){
        glPushAttrib (GL_DEPTH_BUFFER_BIT | GL_POINT_BIT | GL_CURRENT_BIT);
        glEnable (GL_DEPTH_TEST);
        glEnable(GL_BLEND);
        
        glEnable( GL_POINT_SMOOTH );
        glEnable(GL_LINE_SMOOTH);
        glEnable(GL_MULTISAMPLE);

        glEnable( GL_BLEND );
        glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
        glPointSize( 8.0 );
        
        if(self->draw_all){
            if(self->draw_positive && self->draw_negative){
                for(int i=0; i < self->active_list->all_annot.size(); i++){
                    draw_slu_annotation(self, self->active_list->all_annot[i]);
                } 
                /*for(int i=0; i < self->a_msg->count; i++){
                    draw_slu_annotation(self, &self->a_msg->annotations[i]);
                    }*/           
            }            
            else if(self->draw_positive){
                for(int i=0; i < self->active_list->pos_annot.size(); i++){
                    draw_slu_annotation(self, self->active_list->pos_annot[i]);
                }  
            }
            else if(self->draw_negative){
                for(int i=0; i < self->active_list->neg_annot.size(); i++){
                    draw_slu_annotation(self, self->active_list->neg_annot[i]);
                }  
            }            
        }
        else if(self->draw_positive){
            if(self->draw_ind >= 0 && self->draw_ind < self->active_list->pos_annot.size()){
                //draw annotation
                draw_slu_annotation(self, self->active_list->pos_annot[self->draw_ind]);
            }
        }
        else if(self->draw_negative){
            if(self->draw_ind >= 0 && self->draw_ind < self->active_list->neg_annot.size()){
                //draw annotation
                draw_slu_annotation(self, self->active_list->neg_annot[self->draw_ind]);
            }
        }
        glPopAttrib();
    }
}

static int mouse_press (BotViewer *viewer, BotEventHandler *ehandler,
                        const double ray_start[3], const double ray_dir[3], 
                        const GdkEventButton *event)
{
    RendererSLUAnnotation *self = (RendererSLUAnnotation*) ehandler->user;

    double xy[2];
    int consumed = 0;

    /*geom_ray_z_plane_intersect_3d(POINT3D(ray_start), POINT3D(ray_dir), 
                                  0, POINT2D(xy));

                                  bot_viewer_request_redraw(viewer);*/

    return consumed;
}

static void
on_param_widget_changed (BotGtkParamWidget *pw, const char *name, void *user)
{
    RendererSLUAnnotation *self = (RendererSLUAnnotation *) user;
    
    update_g_params(self, pw);
    if(!strcmp(name, PARAM_SR_NAME)){
        set_active_relation(self);
        update_sliders(self);
    }

    if(!strcmp(name, PARAM_SELECT_POSITIVE) || !strcmp(name, PARAM_SELECT_NEGATIVE)){
        update_sliders(self);
    }

    if(!strcmp(name, PARAM_CLEAR_ANNOTATION) && bot_gtk_param_widget_get_bool(self->pw, PARAM_CLEAR_ANNOTATION)){  
        map<string, sr_annotation_list>::iterator it;
        for(it = self->annotation_map.begin(); it != self->annotation_map.end(); it++){
            it->second.clear();
        }
    }

    //set_focus(self);

    if (!strcmp(name, PARAM_REQUEST_ANNOTATIONS)) {
        int sr_ind = bot_gtk_param_widget_get_enum(self->pw, PARAM_SR_NAME);

        if(sr_ind < self->spatial_relations.size()){
            fprintf(stderr, "Spatial Relation : %s\n", self->sr_description[sr_ind]);
        }
        send_sr_request(self, self->sr_description[sr_ind]);
    }

    bot_viewer_request_redraw (self->viewer);
}
      
static void
on_load_preferences (BotViewer *viewer, GKeyFile *keyfile, void *user_data)
{
    RendererSLUAnnotation *self = (RendererSLUAnnotation *) user_data;
    bot_gtk_param_widget_load_from_key_file (self->pw, keyfile, self->renderer.name);
}

static void
on_save_preferences (BotViewer *viewer, GKeyFile *keyfile, void *user_data)
{
    RendererSLUAnnotation *self = (RendererSLUAnnotation *) user_data;
    bot_gtk_param_widget_save_to_key_file (self->pw, keyfile, self->renderer.name);
}

static RendererSLUAnnotation *
renderer_slu_annotation_new (BotViewer *viewer, int priority, BotParam * param)
{    
    RendererSLUAnnotation *self = new RendererSLUAnnotation();//(RendererSLUAnnotation*) calloc (1, sizeof (*self));

    self->viewer = viewer;

    BotRenderer *renderer = &self->renderer;
    renderer->draw = renderer_slu_annotation_draw;
    
    //this doesn't get called on exit - as per the viewer 
    renderer->destroy = renderer_slu_annotation_destroy;
    renderer->widget = bot_gtk_param_widget_new();
    renderer->name = RENDERER_NAME;
    renderer->user = self;
    renderer->enabled = 1;
    
    self->lcm = bot_lcm_get_global (NULL);
    if (!self->lcm) {
        fprintf (stderr,"Error: setup_renderer_laser() failed to get global lcm object\n");
        renderer_slu_annotation_destroy (renderer);
        return NULL;
    }

    self->param = param;
    if (!self->param) {
        fprintf (stderr,"Error: setup_renderer_laser() failed to get BotParam instance\n");
        renderer_slu_annotation_destroy (renderer);
        return NULL;
    }

    double *fp = self->footprint;

    bot_param_get_double_array_or_fail (self->param, "calibration.vehicle_bounds.front_left",
                                        fp, 2);
    bot_param_get_double_array_or_fail (self->param, "calibration.vehicle_bounds.front_right",
                                        fp+2, 2);
    bot_param_get_double_array_or_fail (self->param, "calibration.vehicle_bounds.rear_right",
                                        fp+4, 2);
    bot_param_get_double_array_or_fail (self->param, "calibration.vehicle_bounds.rear_left",
                                        fp+6, 2);

    self->fp_length = self->footprint[0] - self->footprint[4];
    self->fp_width = fabs (self->footprint[1] - self->footprint[3]);

    self->front_middle[0] = (self->footprint[0] + self->footprint[2])/2;
    self->front_middle[1] = (self->footprint[1] + self->footprint[3])/2;

    self->pw = BOT_GTK_PARAM_WIDGET (renderer->widget);

    gtk_widget_show_all (renderer->widget);
    g_signal_connect (G_OBJECT (self->pw), "changed", 
                      G_CALLBACK (on_param_widget_changed), self);
    g_signal_connect (G_OBJECT (viewer), "load-preferences", 
                      G_CALLBACK (on_load_preferences), self);
    g_signal_connect (G_OBJECT (viewer), "save-preferences",
                      G_CALLBACK (on_save_preferences), self);

    bot_gtk_param_widget_add_enumv(self->pw, PARAM_SR_NAME, BOT_GTK_PARAM_WIDGET_DEFAULTS, 0, self->spatial_relations.size(), (const char **) self->sr_description, self->sr_id);

    bot_gtk_param_widget_add_buttons(self->pw, PARAM_REQUEST_ANNOTATIONS, NULL);

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0,
                                       PARAM_DRAW_ALL, 0, NULL);
    
    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) BOT_GTK_PARAM_WIDGET_CHECKBOX,
                                       PARAM_SELECT_POSITIVE, 1, NULL);
    
    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) BOT_GTK_PARAM_WIDGET_CHECKBOX,
                                       PARAM_SELECT_NEGATIVE, 1, NULL);

    /*bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0,
                                       PARAM_SET_FOCUS, 1, NULL);*/

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0,
                                       PARAM_DRAW_TEXT, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0,
                                       PARAM_CENTER_ANNOTATION, 1, NULL);

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0,
                                       PARAM_CLEAR_ANNOTATION, 1, NULL);

    slu_annotation_list_t_subscribe(self->lcm, "SLU_ANNOTATIONS", on_annotation, self);

    bot_gtk_param_widget_add_int(self->pw, PARAM_SLU_INDEX, BOT_GTK_PARAM_WIDGET_SLIDER, 0, 1, 1, 0);
    bot_gtk_param_widget_set_enabled (self->pw, PARAM_SLU_INDEX, 0);

    
    //the mouse handler is causing segfaults 
    self->ehandler.name = (char*)RENDERER_NAME;
    self->ehandler.enabled = 0;
    self->ehandler.mouse_press = mouse_press;
    self->ehandler.user = self;
    
    bot_viewer_add_event_handler(viewer, &self->ehandler, priority);

    return self;
}

extern "C" void
setup_renderer_slu_annotation (BotViewer *viewer, int priority, BotParam * param)
{
    RendererSLUAnnotation *self = renderer_slu_annotation_new (viewer, priority, param);
    bot_viewer_add_renderer (viewer, &self->renderer, priority);
}


