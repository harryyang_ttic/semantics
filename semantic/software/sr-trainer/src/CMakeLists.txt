cmake_minimum_required (VERSION 2.6)

add_definitions(-fPIC)
#add_definitions(-fpermissive -c -pipe -Wall -W -DQT_SHARED -DQT_NO_DEBUG -DQT_THREAD_SUPPORT)

# Create a shared library libhello.so with a single source file
add_library(sr-trainer SHARED
  feature_extractor.cpp
)

# make the header public
# install it to include/hello
pods_install_headers(feature_extractor.hpp DESTINATION sr_trainer)

# make the library public
pods_install_libraries(sr-trainer)

# uncomment these lines to link against another library via pkg-config
set(REQUIRED_PACKAGES slu-spatial-features)

pods_use_pkg_config_packages(sr-trainer ${REQUIRED_PACKAGES})

# create a pkg-config file for the library, to make it easier for other
# software to use.
pods_install_pkg_config_file(sr-trainer
    CFLAGS
    LIBS -lsr-trainer
    REQUIRES ${REQUIRED_PACKAGES}
    VERSION 0.0.1)

# Create a shared library libhello.so with a single source file
add_library(sr-annotation SHARED #STATIC ## yaml doesn't seem to like shared - complains about -fPIC
  annotation.cpp 
  object.cpp
  context.cpp
  command.cpp
  sludatapoint.cpp
  path.cpp
)

# make the header public
# install it to include/hello
pods_install_headers(annotation.hpp dataset.hpp DESTINATION sr_trainer)

# make the library public
pods_install_libraries(sr-annotation)

# uncomment these lines to link against another library via pkg-config
set(REQUIRED_PACKAGES sr-trainer yaml-cpp bot2-core lcmtypes_sr-trainer)

pods_use_pkg_config_packages(sr-annotation ${REQUIRED_PACKAGES})

target_link_libraries(sr-annotation
  -lgsl -lgslcblas 
)

# create a pkg-config file for the library, to make it easier for other
# software to use.
pods_install_pkg_config_file(sr-annotation
    CFLAGS
    LIBS -lsr-annotation
    REQUIRES ${REQUIRED_PACKAGES}
    VERSION 0.0.1)

add_executable(training-data-publisher
  training_data_loader.cpp
)

pods_use_pkg_config_packages(training-data-publisher
    yaml-cpp 
    bot2-core 
    lcmtypes_sr-trainer
    sr-trainer
    sr-annotation
)

target_link_libraries(training-data-publisher
  -lshark -lboost_serialization -lgsl -lgslcblas -lm
)

pods_install_executables(training-data-publisher)


add_executable(rf-trainer
  rf_trainer.cpp
  #annotation.cpp object.cpp context.cpp command.cpp path.cpp
)

pods_use_pkg_config_packages(rf-trainer
    yaml-cpp 
    bot2-core 
    lcmtypes_sr-trainer
    sr-trainer
    sr-annotation
)

target_link_libraries(rf-trainer
  -lshark -lboost_serialization -lgsl -lgslcblas -lm #-lgsl -lgslcblas
)

pods_install_executables(rf-trainer)

add_executable(rf-load-classifier
  rf_load_classifier.cpp
)

target_link_libraries(rf-load-classifier
  -lshark -lboost_serialization -lgsl -lgslcblas -lm
)

pods_use_pkg_config_packages(rf-load-classifier
  yaml-cpp 
  bot2-core 
  lcmtypes_sr-trainer
  sr-trainer
  sr-annotation
)

pods_install_executables(rf-load-classifier)

add_library(slu-rf-classifier SHARED
  slu_rf_classifier.cpp
)

target_link_libraries(slu-rf-classifier
    -lshark
    -lboost_serialization
    -lgsl -lgslcblas
)

pods_install_headers(slu_rf_classifier.hpp DESTINATION sr_trainer)

# make the library public
pods_install_libraries(slu-rf-classifier)

set(REQUIRED_PACKAGES sr-trainer
  sr-annotation
  #bot2-param-client
)

pods_use_pkg_config_packages(slu-rf-classifier ${REQUIRED_PACKAGES})

# create a pkg-config file for the library, to make it easier for other
# software to use.
pods_install_pkg_config_file(slu-rf-classifier
    CFLAGS
    LIBS -lslu-rf-classifier
    REQUIRES ${REQUIRED_PACKAGES}
    VERSION 0.0.1)

