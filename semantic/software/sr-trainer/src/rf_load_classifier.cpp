#include "feature_extractor.hpp"
#include "dataset.hpp"
#include <boost/archive/polymorphic_text_iarchive.hpp>

using namespace sr_dataset;
using namespace std;
using namespace shark;

RealVector get_data_vector(gsl_vector *v){
    RealVector vec(v->size);

    size_t n_cols = v->size;
    for(size_t i=0; i < n_cols; i++){

        double value = gsl_vector_get (v, i);
        vec[i] = value;
    }
    return vec;
}

int main(int argc, char **argv)
{
    if(argc < 2)
        return -1;

    vector<string> spatial_relations; 

    spatial_relations.push_back("across");
    spatial_relations.push_back("along");
    spatial_relations.push_back("around");
    spatial_relations.push_back("away from");
    spatial_relations.push_back("down");
    spatial_relations.push_back("out");
    spatial_relations.push_back("past");
    spatial_relations.push_back("straight");
    spatial_relations.push_back("through");
    spatial_relations.push_back("to");
    spatial_relations.push_back("towards");
    spatial_relations.push_back("turnLeft");
    spatial_relations.push_back("turnRight");
    spatial_relations.push_back("until");
    
    map<string, RFClassifier> model_map; 
    map<string, RFClassifier>::iterator it_model;

    fprintf(stderr, "Model Folder : %s\n", argv[1]);
    bool verbose = false; 

    char model_path[2048];
    for(int i=0; i < spatial_relations.size(); i++){
        RFClassifier model;
        
        sprintf(model_path, "%s/%s_rf.model", argv[1], spatial_relations[i].c_str());

        try{
            ifstream ifs(model_path);
            
            boost::archive::polymorphic_text_iarchive ia(ifs);
            
            model.read(ia);
            ifs.close();
            model_map.insert(make_pair(spatial_relations[i], model));
        }
        catch(boost::archive::archive_exception e){
            cout << "Model not found for relation : " << spatial_relations[i] << endl;
        }
    }

    map<string, dataset> data_corpus; 
    map<string, dataset>::iterator it;    

    //vector<Annotation> corpus;
    map<string, annotation_corpus> corpus;
    map<string, annotation_corpus>::iterator it_c;

    vector<string> orig_names = spatial_features::path_feature_names();

    const gsl_rng_type * T = gsl_rng_default;
    gsl_rng *rng = gsl_rng_alloc (T);
    
    struct timeval tv;
    unsigned int seed;
    gettimeofday(&tv,0);
    seed = tv.tv_sec + tv.tv_usec;

    //seed doesnt seem to work
    gsl_rng_set (rng, seed);

    if(argc > 2){
        int size = 100;
        fprintf(stderr, "Loading test file\n");
        string path = string(argv[2]);
        YAML::Node config = YAML::LoadFile(path);
        
        for (int i=0; i < config.size(); i++){
        
            YAML::Node nd = config[i];

            try{
                Annotation annotation(nd);
                if(i % size==0){
                    fprintf(stdout, "Processing example : %d/%d\n", i, (int) config.size());
                }
                
                //annotation.print();
                if(verbose){
                    annotation.print();
                }

                gsl_matrix *temp_path = annotation.get_path_matrix();
                gsl_matrix *temp_bbox = annotation.get_landmark_matrix();

                if(!temp_path || !temp_bbox){
                    fprintf(stderr, "Error - null gsl mat\n");
                    continue;
                }

                if(verbose){
                    spatial_features::print_gsl_matrix(temp_path);
                    spatial_features::print_gsl_matrix(temp_bbox);
                }

                gsl_vector *vec = spatial_features::path_feature_values(temp_path , temp_bbox, true);

                vector<string> new_names;
 
                spatial_features::rectify_path_features(orig_names, vec, new_names);
                if(verbose){
                    spatial_features::print_features(new_names, vec);
                }

                annotation.add_feature_vector(new_names, vec);
                //we need to split this to the different SR's
                RealVector v = get_data_vector(vec);
                unsigned int l = annotation.get_relation_label();

                string relation = annotation.command.get_relation();

                it = data_corpus.find(relation); 
        
                if(it != data_corpus.end()){
                    it->second.add_data(annotation, v, l);
                }
                else{
                    cout << "Found new relation : " << relation << endl;
                    dataset ds(relation, rng);
                    ds.add_data(annotation,v,l);
                    data_corpus.insert(make_pair(relation, ds));
                }

                gsl_matrix_free(temp_path);
                gsl_matrix_free(temp_bbox);
                gsl_vector_free(vec);
        
                //corpus.push_back(annotation);
                it_c = corpus.find(relation);
            
                if(it_c != corpus.end()){
                    it_c->second.add(annotation);
                }
                else{
                    annotation_corpus cp(relation);
                    cp.add(annotation);
                    corpus.insert(make_pair(relation, cp));
                }
                
            }
            catch(AnnotationException e){
                //e.print();
                cout << ".";
            }  
        }

        for(it = data_corpus.begin(); it != data_corpus.end(); it++){
            
            it_model = model_map.find(it->first);
            
            if(it_model == model_map.end()){
                cout << "Model not found for relation : " << it->first << endl;
                continue;
            }
            
            cout << "\n==============================================="  << endl;

            cout << "Data corpus Relation : " << it->first << endl;
            
            ClassificationDataset p_data = it->second.get_full_positive_data();
            ClassificationDataset n_data = it->second.get_full_negative_data();

            ZeroOneLoss<unsigned int, RealVector> loss;
            Data<RealVector> prediction = it_model->second(p_data.inputs());

            typedef Data<RealVector>::element_range Elements;
            
            Elements elements = prediction.elements();
            if(1){
                for(Elements::iterator pos = elements.begin(); pos != elements.end(); ++pos){
                    //std::cout<<*pos<<std::endl;
                    RealVector rv = *pos;
                    if(rv[1] < 0.5){
                        fprintf(stdout, "Positive Prob : %.2f\n", rv[1]);
                    }
                }
            }
            
            double p_acc = 1. - loss.eval(p_data.labels(), prediction);
            cout << "Random Forest on Positive dataset accuracy: " << 1. - loss.eval(p_data.labels(), prediction) << endl;
            
            prediction = it_model->second(n_data.inputs());

            Elements elements_1 = prediction.elements();
            for(Elements::iterator pos = elements_1.begin(); pos != elements_1.end(); ++pos){
                //std::cout<<*pos<<std::endl;
                RealVector rv = *pos;
                if(rv[0] < 0.5){
                    fprintf(stdout, "Negative Prob : %.2f\n", rv[0]);
                }
            }

            double n_acc = 1. - loss.eval(n_data.labels(), prediction);
            cout << "Random Forest on Negative dataset accuracy:     " << 1. - loss.eval(n_data.labels(), prediction) << endl;
            
            cout << "\n==============================================="  << endl;
        }
    }
}
