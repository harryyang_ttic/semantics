#include "feature_extractor.hpp"

vector<string> spatial_features::object_feature_names(vector<string> figure){
    
    return flu_polygon_names(figure);
}

gsl_vector* spatial_features::object_feature_values(vector<string> figure, 
                                   gsl_matrix* gnd1_xy, 
                                   bool perform_scaling){
    return flu_polygon(figure, gnd1_xy, perform_scaling);
}


vector<string> spatial_features::path_feature_names(){
    return sfe_f_path_l_polygon_names();
}


//sfe_extract_f_path_l_polygon
gsl_vector* spatial_features::path_feature_values(gsl_matrix* fig1_xyth, 
                                                  gsl_matrix* gnd1_xy, 
                                                  bool normalize){
    return sfe_extract_f_path_l_polygon(fig1_xyth, gnd1_xy, normalize);
}

//remove the nan features and add binary features 
int spatial_features::rectify_path_features(const vector<string> names, gsl_vector *v, vector<string> &new_names, bool clamp, double threshold){
    if(names.size() != v->size){
        fprintf(stderr, "Error - feature sizes different\n");
        return -1;
    }

    size_t n_cols = v->size;    

    char new_name[2048]; 
    for(size_t i=0; i < n_cols; i++){
        double value = gsl_vector_get (v, i);
        if(isnan(value)){
            //fprintf(stdout, "%s \t - Nan\n", names[i].c_str());
            sprintf(new_name, "%s_is_nan", names[i].c_str());
            new_names.push_back(string(new_name));
            gsl_vector_set(v, i, 1.0);
        }
        else{            
            new_names.push_back(names[i]);
            if(clamp){
                if(value < -threshold){
                    gsl_vector_set(v, i, -threshold);
                }
                else if(value > threshold){
                    gsl_vector_set(v, i, threshold);
                }
            }
        }
    }

    return 0; 

}

void spatial_features::print_features(const vector<string> &names, const gsl_vector *v){
    fprintf(stderr, "Printing GSL Vector\n");
    if(names.size() != v->size){
        fprintf(stderr, "Error - feature sizes different\n");
        exit(-1);
    }
    size_t n_cols = v->size;
    for(size_t i=0; i < n_cols; i++){

        double value = gsl_vector_get (v, i);
        if(isnan(value)){
            fprintf(stdout, "%s \t - Nan\n", names[i].c_str());
        }
        else{
            fprintf(stdout, "%s \t - %4.3f\n", names[i].c_str(), gsl_vector_get (v, i));
        }
    }
    fprintf(stdout, "\n");
}

void spatial_features::print_gsl_matrix(gsl_matrix *m){
    size_t n_rows = m->size1;
    size_t n_columns = m->size2;

    for(size_t i=0; i < n_rows; i++){
        for(size_t j=0; j < n_columns; j++){
            double value = gsl_matrix_get(m, i,j);
            fprintf(stdout, "%4.3f\t", value);// << "\t";
        }
        fprintf(stdout, "\n");
    }
}
