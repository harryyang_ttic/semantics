#include <sr_trainer/slu_rf_classifier.hpp>
#include <path_util/path_util.h>
#include <bot_core/bot_core.h> 
#include <bot_param/param_client.h>
#include <occ_map/PixelMap.hpp>
#include <vector>
#include <map>

using namespace sr_dataset;
using namespace occ_map;

typedef pair<int, int> intPair;

int main(int argc, char **argv){
    //Load the SLU classifiers 
    
    if(argc <=1){
        fprintf(stderr, "Please enter spatial relation type");
        return -1; 
    }

    string relation(argv[1]);

    lcm_t *lcm = lcm_create(NULL);
    if (!lcm) {
        fprintf(stderr, "ERROR: lcm_create() failed\n");
        return 1;
    }   
    BotParam *param = bot_param_new_from_server(lcm, 1);
    const char *data_path = getDataPath();
    char key[200];

    sprintf(key,"natural_language_understanding.model_path"); //
    SLUClassifier *slu_classifier; 
    
    char *slu_path = NULL;
    
    char full_slu_path[2000]; 
    if (0 != bot_param_get_str(param, key, &slu_path)){
        fprintf(stderr, "SLU Path not defined: %s\n", key);
        slu_classifier = NULL;
        return -1;
    }
    else{
        sprintf(full_slu_path, "%s/%s", data_path, slu_path);
        slu_classifier = new SLUClassifier(full_slu_path);
        free(slu_path);
        if(!slu_classifier){
            fprintf(stderr, "Error - no classifier created\n");
            return -1;
        }
        fprintf(stderr, "SLU Path : %s\n", full_slu_path);
        fprintf(stderr, "Loaded SLU classifier\n");
    }    

    // Allocate the pixel map for the local occupancy grid
    double xy0[2] = {-15.0, -15.0};
    double xy1[2] = {15.0, 15.0};
    double res = 0.2;
    
    //passed to the particles - used for calculating pofz - check if its better to use a local copy
    FloatPixelMap *local_px_map = new FloatPixelMap(xy0, xy1, res, 0, true, true);
    
    int ixy0[2] = {0, 0};
    int ixy1[2] = {(xy1[0] - xy0[0]) / res, (xy1[1] - xy0[1]) / res};

    double xyzt_r[4] = {0,0,0,0};
    Pose bot_pose(0, xyzt_r);
    

    vector<intPair> positions;
    vector<SLUDataPoint> datapoints;

    fprintf(stdout, "Testing locations\n");

    fprintf(stderr, "Bounds : %d,%d - %d,%d\n", ixy0[0], ixy1[0], ixy0[1], ixy1[1]);

    for(int i= ixy0[0]; i < ixy1[0]; i++){
        for(int j= ixy0[1]; j < ixy1[1]; j++){
            //get 
            vector<Pose> np_path;// = getPosesFromNodes(path); 
            np_path.push_back(bot_pose);
            
            int ixy[2] = {i,j};
            double xyzt[4] = {0, 0, 0, 0};
            local_px_map->tableToWorld(ixy, xyzt);
            
            Pose pt(1, xyzt);
            np_path.push_back(pt);
            
            double lm_z[2] = {0, 2.0};
            vector<Point> lm_points; 
            SLUDataPoint dp(np_path, lm_points, lm_z);
            datapoints.push_back(dp);
            positions.push_back(make_pair(i,j));
        }
    }

    vector<double> results = slu_classifier->get_likelihood(relation, datapoints); 
    
    if(results.size() != positions.size()){
        fprintf(stderr, "Results not returned for entire dataset : %d (%d)\n", (int) results.size(), (int) positions.size());
        return -1;
    }

    for(int i=0; i < positions.size(); i++){
        int ixy[2] = {positions[i].first, positions[i].second};
        local_px_map->updateValue(ixy, results[i]);
    }

    const occ_map_pixel_map_t *map_msg = local_px_map->get_pixel_map_t(bot_timestamp_now());
    occ_map_pixel_map_t_publish(lcm, "SR_RESULT_MAP", map_msg);
}
