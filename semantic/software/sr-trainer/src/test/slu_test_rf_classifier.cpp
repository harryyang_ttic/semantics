#include <sr_trainer/slu_rf_classifier.hpp>

int main(int argc, char **argv){

    if(argc >=2){
        char *path = argv[1]; 
        SLUClassifier classifier(path);

        //string command("the gym is down the hallway");
        Command cmd = classifier.parse_language("the gym is down the hallway");  
        cout << cmd;

        Command cmd1 = classifier.parse_language("the elevator lobby is through the entrance");    
        cout << cmd1;

        Command cmd2 = classifier.parse_language("the lab is to my right");      
        cout << cmd2;

        Command cmd3 = classifier.parse_language("the lab is behind us");      
        cout << cmd3;
        
        if(argc > 2){

            map<string, dataset> data_corpus; 
            map<string, dataset>::iterator it;    

            map<string, annotation_corpus> corpus;
            map<string, annotation_corpus>::iterator it_c;

            vector<string> orig_names = spatial_features::path_feature_names();

            const gsl_rng_type * T = gsl_rng_default;
            gsl_rng *rng = gsl_rng_alloc (T);
    
            struct timeval tv;
            unsigned int seed;
            gettimeofday(&tv,0);
            seed = tv.tv_sec + tv.tv_usec;

            //seed doesnt seem to work
            gsl_rng_set (rng, seed);

            int size = 100;
            fprintf(stderr, "Loading test file\n");
            string path = string(argv[2]);
            YAML::Node config = YAML::LoadFile(path);
        
            for (int i=0; i < config.size(); i++){
        
                YAML::Node nd = config[i];

                try{
                    Annotation annotation(nd);
                    if(i % size==0){
                        fprintf(stdout, "Processing example : %d/%d\n", i, (int) config.size());
                    }
                
                    gsl_matrix *temp_path = annotation.get_path_matrix();
                    gsl_matrix *temp_bbox = annotation.get_landmark_matrix();

                    if(!temp_path || !temp_bbox){
                        fprintf(stderr, "Error - null gsl mat\n");
                        continue;
                    }

                    gsl_vector *vec = spatial_features::path_feature_values(temp_path , temp_bbox, true);

                    vector<string> new_names;
 
                    spatial_features::rectify_path_features(orig_names, vec, new_names);

                    annotation.add_feature_vector(new_names, vec);
                    //we need to split this to the different SR's
                    RealVector v = SLUClassifier::get_data_vector(vec);
                    unsigned int l = annotation.get_relation_label();

                    string relation = annotation.command.get_relation();

                    it = data_corpus.find(relation); 
        
                    if(it != data_corpus.end()){
                        it->second.add_data(annotation, v, l);
                    }
                    else{
                        cout << "Found new relation : " << relation << endl;
                        dataset ds(relation, rng);
                        ds.add_data(annotation,v,l);
                        data_corpus.insert(make_pair(relation, ds));
                    }

                    gsl_matrix_free(temp_path);
                    gsl_matrix_free(temp_bbox);
                    gsl_vector_free(vec);
        
                    //corpus.push_back(annotation);
                    it_c = corpus.find(relation);
            
                    if(it_c != corpus.end()){
                        it_c->second.add(annotation);
                    }
                    else{
                        annotation_corpus cp(relation);
                        cp.add(annotation);
                        corpus.insert(make_pair(relation, cp));
                    }                
                }
                catch(AnnotationException e){
                    cout << ".";
                }  
            }

            for(it = data_corpus.begin(); it != data_corpus.end(); it++){
                vector<Annotation> p_annot = it->second.get_full_positive_annotations();
                vector<Annotation> n_annot = it->second.get_full_negative_annotations();
                
                vector<double> p_r = classifier.get_likelihood(it->first, p_annot);
                vector<double> n_r = classifier.get_likelihood(it->first, n_annot);
            }
        }
    }    
}
