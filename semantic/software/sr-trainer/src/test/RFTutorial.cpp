//===========================================================================
/*!
 * 
 *
 * \brief       Linear Regression Tutorial Sample Code
 * 
 * This file is part of the "Random Forest" tutorial.
 * It requires some toy sample data that comes with the library.
 * 
 * 
 *
 * \author      K. N. Hansen
 * \date        2012
 *
 *
 * \par Copyright 1995-2014 Shark Development Team
 * 
 * <BR><HR>
 * This file is part of Shark.
 * <http://image.diku.dk/shark/>
 * 
 * Shark is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Shark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Shark.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
//===========================================================================

#include <shark/Data/Csv.h> //importing the file
#include <shark/Algorithms/Trainers/RFTrainer.h> //the random forest trainer
#include <shark/ObjectiveFunctions/Loss/ZeroOneLoss.h> //zero one loss for evaluation

#include <iostream> 

using namespace std; 
using namespace shark;


/*void csvStringToData(LabeledData<RealVector, unsigned int> &dataset,
  std::string const& contents,
  LabelPosition lp,
  char separator,
  char comment,
  std::size_t maximumBatchSize
  ){
  std::vector<CsvPoint> rows = import_csv_reader_points(contents, lp, separator,comment);
  if(rows.empty()){//empty file leads to empty data object.
  dataset = LabeledData<RealVector, unsigned int>();
  return;
  }

  //check labels for conformity
  bool binaryLabels = false;
  int minPositiveLabel = std::numeric_limits<int>::max();
  {

  int maxPositiveLabel = -1;
  for(std::size_t i = 0; i != rows.size(); ++i){
  int label = rows[i].first;
  if(label < -1)
  throw SHARKEXCEPTION("negative labels are only allowed for classes -1/1");
  else if(label == -1)
  binaryLabels = true;
  else if(label < minPositiveLabel)
  minPositiveLabel = label;
  else if(label > maxPositiveLabel)
  maxPositiveLabel = label;
  }
  if(binaryLabels && (minPositiveLabel == 0||  maxPositiveLabel > 1))
  throw SHARKEXCEPTION("negative labels are only allowed for classes -1/1");
  }

  //copy rows of the file into the dataset
  std::size_t dimensions = rows[0].second.size();
  std::vector<std::size_t> batchSizes = shark::detail::optimalBatchSizes(rows.size(),maximumBatchSize);
  dataset = LabeledData<RealVector, unsigned int>(batchSizes.size());
  std::size_t currentRow = 0;
  for(std::size_t b = 0; b != batchSizes.size(); ++b) {
  RealMatrix& inputs = dataset.batch(b).input;
  UIntVector& labels = dataset.batch(b).label;
  inputs.resize(batchSizes[b],dimensions);
  labels.resize(batchSizes[b]);
  //copy the rows into the batch
  for(std::size_t i = 0; i != batchSizes[b]; ++i,++currentRow){
  if(rows[currentRow].second.size() != dimensions)
  throw SHARKEXCEPTION("vectors are required to have same size");

  for(std::size_t j = 0; j != dimensions; ++j){
  inputs(i,j) = rows[currentRow].second[j];
  }
  int rawLabel = rows[currentRow].first;
  labels[i] = binaryLabels? 1 + (rawLabel-1)/2 : rawLabel -minPositiveLabel;
  }
  }
  SIZE_CHECK(currentRow == rows.size());
  }*/

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>

int main(int argc, char **argv) {

    //*****************LOAD AND PREPARE DATA***********************//
    //Read Sample data set C.csv
    
    char *path = argv[1];      
    
    ClassificationDataset data;
    import_csv(data, path, LAST_COLUMN, ' ');
    
    cout << "No : " << data.elements().size() << endl;

    ClassificationDataset::element_range elements = data.elements();

    //Vector is defined in /software/semantic/software/externals/libshark/Shark/include/shark/LinAlg/BLAS/vector.hpp 

    /*RealVector vec(100);
    for(int i =0; i < vec.size(); i++){
        vec[i] = i * 2; 
    }

    for(int i =0; i < vec.size(); i++){
        cout << i << " : " << vec[i] << endl;
    }
    
    return -1;*/

    for(int i=0; i < elements.size(); i++){
        ClassificationDataset::element_type tp = elements[i];
        //cout << tp.input <<endl;
        RealVector rvec = tp.input; 
        cout << "Datapoint : " << i << " - " << rvec.size() << endl;;
        for(int k=0; k < rvec.size(); k++){
            cout << "\t" << k << " : " << rvec[k] << endl; 
        }
    }

    for(int i=0; i < data.numberOfElements(); i++){
        //cout << data.numberOfElements() << endl;
        //cout << data.element(i) << endl;
    }

    //Split the dataset into a training and a test dataset

    cout << "Number of examples : " << data.numberOfElements() << endl; 

    //train with 3/4 of the dataset
    int train_count = data.numberOfElements() / 4.0 * 3.0;

    ClassificationDataset dataTest = splitAtElement(data, train_count);//311);

    cout << "Training set - number of data points: " << data.numberOfElements()
         << " number of classes: " << numberOfClasses(data)
         << " input dimension: " << inputDimension(data) << endl;

    cout << "Test set - number of data points: " << dataTest.numberOfElements()
         << " number of classes: " << numberOfClasses(dataTest)
         << " input dimension: " << inputDimension(dataTest) << endl;

    //Generate a random forest
    RFTrainer trainer;
    RFClassifier model;
    trainer.train(model, data);
    cout<< "Done training\n";
    // evaluate Random Forest classifier
    ZeroOneLoss<unsigned int, RealVector> loss;
    Data<RealVector> prediction = model(data.inputs());
    cout << "Random Forest on training set accuracy: " << 1. - loss.eval(data.labels(), prediction) << endl;

    prediction = model(dataTest.inputs());
    cout << "Random Forest on test set accuracy:     " << 1. - loss.eval(dataTest.labels(), prediction) << endl;
}
