#include "annotation.hpp"

using namespace std;

Command::Command(){
    relation = string("unknown");
    landmark = string("unknown");
    command = string("unknown");
    figure = string("unknown");
}

Command::Command(string _command, string _relation, string _landmark, string _figure):command(_command), relation(_relation),
                                                                                      landmark(_landmark), figure(_figure){
}

Command::Command(YAML::Node _command){
    //cout << "Command size : " << _command.size() << endl;
    //cout << "Command : " << _command[0] << endl;

    command = _command[0].as<string>();

    //cout << "-------------------------------\nCommand Parse : " << _command[1] << "\n-------------------------------" << endl;

    YAML::Node parse = _command[1];
    if(parse.size() > 1){
        cout << "More than one parse" << endl;
        exit(-1);
    }


    //for(int i=0; i < parse.size(); i++){
    YAML::Node _p = parse[0];
    //cout << i << " : " << " Size : " <<  _p.size() << endl;
    //cout << _p << endl;

    try{
        if(_p["PATH"]){
            //cout << "Relation : " << _p["PATH"]["r"].as<string>() << endl;
            //cout << "Landmark : " << _p["PATH"]["l"].as<string>() << endl;
            relation = _p["PATH"]["r"].as<string>();
            landmark = _p["PATH"]["l"].as<string>();
        }
        else{
            relation = string("error");
            landmark = string("error");
            fprintf(stderr, "No path\n");
            //exit(-1);
            throw AnnotationException("No command parse", _p);
        }
    }
    catch(YAML::TypedBadConversion<std::string> e){
        //cout << _p << endl;
        //cout << "YAML Error : " << endl;
        throw AnnotationException("command parse error", _p);
    }
}

string Command::get_relation(){
    return relation;
}

void Command::print(std::ostream &os) const{
    os << "Command : " << command << endl;
    os << "\tRelation : " << relation << endl;
    os << "\tLandmark : " << landmark << endl;
    os << "\tFigure : " << figure << endl;
}

std::ostream& operator<<(std::ostream& os, const Command& obj)
{
    obj.print(os);
    return os;
}


