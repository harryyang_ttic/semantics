#include "annotation.hpp"

using namespace std;

Annotation::Annotation(YAML::Node annotation){
    try{
        assignment_id = annotation["assignmentId"].as<string>();

        YAML::Node y_groundings = annotation["objectGroundings"];

        //cout << "Landmark Grounding => Size :" << y_groundings.size() << endl;

        bool path_found = false;
        bool landmark_found = false;

        for(int i=0; i < y_groundings.size(); i++){
            YAML::Node _gnd = y_groundings[i][0];

            if(!_gnd[0].as<string>().compare("Path")){
                path_grounding = Path(_gnd[1]);
                //path_grounding.print();
                path_found = true;
            }
            else if(!_gnd[0].as<string>().compare("PhysicalObject")){
                landmark_grounding = Object(_gnd[1]);
                //landmark_grounding.print();
                landmark_found = true;
            }
        }

        if(!landmark_found || !path_found){
            fprintf(stderr, "Error - Landmark Grounding or Path Grounding not found\n");
            exit(-1);
        }

        //landmark_grounding = Object(y_groundings["PhysicalObject"]);

        YAML::Node y_agent = annotation["agent"];

        agent = Object(y_agent);

        command = Command(annotation["command"]);

        grounding = Grounding(annotation["groundingIsCorrect"]);
    }
    catch(YAML::TypedBadConversion<std::string> e){
        throw AnnotationException("command parse error", annotation);
    }
    catch(AnnotationException e){
        throw e;
    }
}

void Annotation::to_lcm(slu_annotation_t *msg) const{
    msg->relation = strdup(command.relation.c_str());
    msg->landmark_name = strdup(command.landmark.c_str());
    msg->command = strdup(command.command.c_str());

    msg->path_true = grounding.relation_grounding;
    msg->landmark_true = grounding.landmark_grounding;

    path_grounding.to_lcm(&msg->path);
    landmark_grounding.to_lcm(&msg->landmark);
}

bool Annotation::get_relation_label(){
    return grounding.get_relation_grounding();
}

bool Annotation::get_landmark_label(){
    return grounding.get_landmark_grounding();
}

void Annotation::print(){
    cout << "-----------------------------------------------------------" << endl;
    cout << "Assignment ID : "  << assignment_id << endl;
    command.print();
    cout << "======= Path Object =========\n" << endl;
    path_grounding.print();
    cout << "========== Physical Object ========" << endl;
    landmark_grounding.print();
    grounding.print();
     cout << "========== Agent ========" << endl;
    agent.print();
    cout << "-----------------------------------------------------------" << endl;
}

int Annotation::add_feature_vector(vector<string> names, gsl_vector *v){
    if(names.size() != v->size){
        return -1;
    }

    feature_map.clear();

    size_t n_cols = v->size;
    for(size_t i=0; i < n_cols; i++){
        double value = gsl_vector_get (v, i);
        Feature f(names[i], value);
        feature_map.push_back(f);
    }

    return 0;
}

gsl_matrix *Annotation::get_path_matrix(){
    return path_grounding.get_gsl_matrix();
}

gsl_matrix *Annotation::get_landmark_matrix(){
    return landmark_grounding.get_bbox_gsl_matrix();
}

