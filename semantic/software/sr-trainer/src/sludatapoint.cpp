#include "annotation.hpp"

using namespace std;

SLUDataPoint::SLUDataPoint(){
    valid = false;
}

SLUDataPoint::SLUDataPoint(vector<Pose> path, vector<Point> landmark_bb, double lm_height[2], vector<string> tags){
    path_grounding = Path(path);
    landmark_grounding = Object(landmark_bb, lm_height, tags);
    valid = true;
}

SLUDataPoint::SLUDataPoint(vector<Pose> path, vector<Point> landmark_bb, double lm_height[2]){
    vector<string> tags;
    path_grounding = Path(path);
    landmark_grounding = Object(landmark_bb, lm_height, tags);
    valid = true;
}

bool SLUDataPoint::isValid(){
    return valid;
}

gsl_matrix *SLUDataPoint::get_path_matrix(){
    return path_grounding.get_gsl_matrix();
}

gsl_matrix *SLUDataPoint::get_landmark_matrix(){
    return landmark_grounding.get_bbox_gsl_matrix();
}

int SLUDataPoint::add_feature_vector(vector<string> names, gsl_vector *v){
    if(names.size() != v->size){
        return -1;
    }

    feature_map.clear();

    size_t n_cols = v->size;
    for(size_t i=0; i < n_cols; i++){
        double value = gsl_vector_get (v, i);
        Feature f(names[i], value);
        feature_map.push_back(f);
    }
    return 0;
}

Path &SLUDataPoint::get_path(){
    return path_grounding;
}

void SLUDataPoint::print(){
    cout << "-----------------------------------------------------------" << endl;
    command.print();
    cout << "======= Path Object =========\n" << endl;
    path_grounding.print();
    cout << "========== Physical Object ========" << endl;
    landmark_grounding.print();
    cout << "-----------------------------------------------------------" << endl;
}

void SLUDataPoint::to_lcm(string sr, slu_annotation_t *msg) const {
    msg->relation = strdup(sr.c_str());
    msg->landmark_name = strdup(command.landmark.c_str());
    msg->command = strdup(command.command.c_str());

    msg->path_true = 1;
    msg->landmark_true = 1;

    path_grounding.to_lcm(&msg->path);
    landmark_grounding.to_lcm(&msg->landmark);

    msg->prob = 0;
}


