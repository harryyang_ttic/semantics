#include <yaml-cpp/yaml.h>
#include "annotation.hpp"
#include <fstream>
#include <iostream>
#include "feature_extractor.hpp"
#include "dataset.hpp"

using namespace sr_dataset;
using namespace std; 
using namespace shark;

struct state_t {
    lcm_t *lcm; 
    GMainLoop *mainloop; 
    map<string, annotation_corpus> corpus;
};

static void on_request (const lcm_recv_buf_t *rbuf, const char *channel,
                                 const slu_annotation_request_t *msg, void *user)
{
    state_t *s = (state_t *) user;

    string relation(msg->relation);
    
    cout << "Request received for relation : " << relation << endl; 

    map<string, annotation_corpus>::iterator it_c;
    it_c = s->corpus.find(relation); 
    
    if(it_c != s->corpus.end()){
        it_c->second.publish_dataset(s->lcm);        
    }
    else{
        cout << "Relation not found in dataset" << endl;
    }
}

int main(int argc, char **argv)
{
    if(argc < 2)
        return -1;

    state_t state; 

    int max_number = 10000000;
    if(argc >= 3){
        max_number = atoi(argv[2]);
    }
        
    const gsl_rng_type * T = gsl_rng_default;
    gsl_rng *rng = gsl_rng_alloc (T);
    
    struct timeval tv;
    unsigned int seed;
    gettimeofday(&tv,0);
    seed = tv.tv_sec + tv.tv_usec;

    //seed doesnt seem to work
    gsl_rng_set (rng, seed);
    
    string path = string(argv[1]);
    
    YAML::Node config = YAML::LoadFile(path);
    
    cout << "Number of examples : " << fmin(max_number, config.size()) << endl;

    vector<string> orig_names = spatial_features::path_feature_names();

    bool verbose = false;

    cout << "Done loading" << endl;

    int size = 100;
    
    int no_examples = fmin(max_number, config.size());

    //map<string, annotation_corpus> corpus;
    map<string, annotation_corpus>::iterator it_c;
        
    for (int i=0; i < no_examples; i++){
        
        YAML::Node nd = config[i];

        try{
            Annotation annotation(nd);
            if(i % size==0){
                fprintf(stdout, "Processing example : %d/%d\n", i, no_examples);
            }
            if(verbose){
                annotation.print();
            }

            gsl_matrix *temp_path = annotation.get_path_matrix();
            gsl_matrix *temp_bbox = annotation.get_landmark_matrix();

            if(!temp_path || !temp_bbox){
                fprintf(stderr, "Error - null gsl mat\n");
                continue;
            }

            if(verbose){
                spatial_features::print_gsl_matrix(temp_path);
                spatial_features::print_gsl_matrix(temp_bbox);
            }

            gsl_vector *vec = spatial_features::path_feature_values(temp_path , temp_bbox, true);

            vector<string> new_names;

            spatial_features::rectify_path_features(orig_names, vec, new_names);
            if(verbose){
                spatial_features::print_features(new_names, vec);
            }

            string relation = annotation.command.get_relation();                   
        
            annotation.add_feature_vector(new_names, vec);

            gsl_matrix_free(temp_path);
            gsl_matrix_free(temp_bbox);
            gsl_vector_free(vec);
            
            it_c = state.corpus.find(relation);
            
            if(it_c != state.corpus.end()){
                it_c->second.add(annotation);
            }
            else{
                annotation_corpus cp(relation);
                cp.add(annotation);
                state.corpus.insert(make_pair(relation, cp));
            }
        }
        catch(AnnotationException e){
            //cout << "Parsing error" << endl;
            cout << ".";
        }        
    }

    state.lcm =  bot_lcm_get_global(NULL);

    for(it_c = state.corpus.begin(); it_c != state.corpus.end(); it_c++){
        it_c->second.print();
        it_c->second.update_dataset();
        //it_c->second.publish_dataset(lcm);        
    }

    state.mainloop = g_main_loop_new( NULL, FALSE );
    
    slu_annotation_request_t_subscribe(state.lcm, "SLU_ANNOTATION_REQUEST", on_request, &state);
    bot_glib_mainloop_attach_lcm (state.lcm);
    bot_signal_pipe_glib_quit_on_kill(state.mainloop);
    g_main_loop_run(state.mainloop);
    bot_glib_mainloop_detach_lcm(state.lcm);
    //have an lcm listener 
        
    return 0;
}
