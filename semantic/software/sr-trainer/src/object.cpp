#include "annotation.hpp"

using namespace std;

Object::Object(){

}

Object::Object(vector<Point> &landmark_points, double lm_height[2], vector<string> names){
    tags = names;
    prism = Prism(landmark_points, lm_height);
}

Object::Object(YAML::Node object){
    //YAML::Node _prism = agent["prism"];
    prism = Prism(object["prism"]);
    path = Path(object["path"]);

    YAML::Node _tags = object["tag"];

    for(int i =0; i <_tags.size(); i++){
        string _t = _tags[i].as<string>();
        tags.push_back(_t);
    }
    //cout << "Prism : \n" << _prism << endl;
}


void Object::print(){
    cout << "Object : \n" <<endl; //<< agent << endl;
    cout << "  Tags : ";
    for(int i=0; i < tags.size(); i++){
        cout << tags[i] << endl;
    }
    cout << endl;
    prism.print();

    path.print(true);
}

gsl_matrix *Object::get_bbox_gsl_matrix(){
    return prism.get_gsl_matrix();
}

void Object::to_lcm(slu_object_t *msg) const{

    msg->tag_count = (int) tags.size();
    msg->tags = (char **) calloc(msg->tag_count, sizeof(char *));

    for(int i=0; i < tags.size(); i++){
        msg->tags[i] = (char *) strdup(tags[i].c_str());
    }

    prism.to_lcm(msg);
}
