#include "slu_rf_classifier.hpp"

SLUClassifier::SLUClassifier(char *path, bool _verbose):verbose(_verbose){
    lcm = bot_lcm_get_global(NULL);
    fill_spatial_relations();
    
    fprintf(stderr, "Model Folder : %s\n", path);

    char model_path[2048];
    for(int i=0; i < spatial_relations.size(); i++){
        RFClassifier model;
        
        sprintf(model_path, "%s/%s_rf.model", path, spatial_relations[i].c_str());

        try{
            ifstream ifs(model_path);
            
            boost::archive::polymorphic_text_iarchive ia(ifs);
            
            model.read(ia);
            ifs.close();
            model_map.insert(make_pair(spatial_relations[i], model));
            cout << "(+) Model found for relation : " << spatial_relations[i] << endl;
        }
        catch(boost::archive::archive_exception e){
            cout << "(-) Model not found for relation : " << spatial_relations[i] << endl;
        }
    }
}

void SLUClassifier::fill_spatial_relations(){
    spatial_relations.push_back("across");
    spatial_relations.push_back("along");
    spatial_relations.push_back("around");
    spatial_relations.push_back("awayfrom");
    spatial_relations.push_back("down");
    spatial_relations.push_back("out");
    spatial_relations.push_back("at");
    spatial_relations.push_back("past");
    spatial_relations.push_back("straight");
    spatial_relations.push_back("through");
    spatial_relations.push_back("to");
    spatial_relations.push_back("towards");
    spatial_relations.push_back("turnLeft");
    spatial_relations.push_back("turnRight");
    spatial_relations.push_back("left");
    spatial_relations.push_back("right");
    spatial_relations.push_back("until");
    spatial_relations.push_back("infrontof"); //what about this 
    spatial_relations.push_back("behind");    
    spatial_relations.push_back("near");    
}

vector<double> SLUClassifier::get_likelihood(string sr, vector<Annotation> &annotations){
    map<string, RFClassifier>::iterator it_model;
    vector<double> results;
    it_model = model_map.find(sr);
    if(it_model == model_map.end()){
        cout << "Model not found for relation : " << sr << endl;
        return results; 
    } 
    
    cout << "Testing for relation : " << sr << endl;

    std::vector<RealVector> points;
    vector<string> orig_names = spatial_features::path_feature_names();
    for(int i=0; i < annotations.size(); i++){
        gsl_matrix *temp_path = annotations[i].get_path_matrix();
        gsl_matrix *temp_bbox = annotations[i].get_landmark_matrix();

        if(!temp_path || !temp_bbox){
            fprintf(stderr, "Error - null gsl mat\n");
            return results;
        }

        if(verbose){
            spatial_features::print_gsl_matrix(temp_path);
            spatial_features::print_gsl_matrix(temp_bbox);
        }
        
        gsl_vector *vec = spatial_features::path_feature_values(temp_path , temp_bbox, true);
        
        vector<string> new_names;
        
        spatial_features::rectify_path_features(orig_names, vec, new_names);
        if(verbose){
            spatial_features::print_features(new_names, vec);
        }
        
        RealVector v = get_data_vector(vec);
        points.push_back(v);
        //unsigned int l = annotation.get_relation_label();

        gsl_matrix_free(temp_path);
        gsl_matrix_free(temp_bbox);
        gsl_vector_free(vec);
    }
    Data<RealVector> data = createDataFromRange(points);

    //cout << data.
    Data<RealVector> prediction = it_model->second(data);    
    
    typedef Data<RealVector>::element_range Elements;
    
    Elements elements = prediction.elements();
    for(Elements::iterator pos = elements.begin(); pos != elements.end(); ++pos){
        RealVector rv = *pos;
        //cout << rv << endl;
        //fprintf(stdout, "[0] : %.2f - [1] : %.2f\n", rv[0], rv[1]);
        fprintf(stdout, "\tLikelihood of True : %f\n", rv[1]);
        //i'm assuming that this the likelihood of the label being 1 
        results.push_back(rv[1]); 
        //std::cout<<*pos<<std::endl;
        //cout << *pos[1] << endl; 
    }
    return results;
}

Command SLUClassifier::parse_language(string command){
    using namespace boost::algorithm;

    //split works on characters (not strings)
    std::vector<std::string> tokens;
    split(tokens, command, is_any_of(" "));

    cout << "Full Command : " << command << endl; 

    string figure_full; 
    vector<string> valid_tokens; 
    bool b_found = false;
    
    if(tokens.size() > 1){
        for(int i=0; i < tokens.size(); i++){
            if(!tokens[i].compare("is")){
                b_found = true;
                continue;
            }
            if(!b_found){
                figure_full.append(tokens[i]);
                figure_full.append(" ");
            }
            else{
                if(tokens[i].size() > 0){
                    valid_tokens.push_back(tokens[i]);
                }
            }
        }
    }   

    string relation_full; 
    string landmark_full;
    bool found = false;    

    for(int i=0; i < valid_tokens.size(); i++){
        if(!valid_tokens[i].compare("the")){
            found = true;                
        }
        if(!found){
            relation_full.append(valid_tokens[i]);
            relation_full.append(" ");
        }
        else{
            landmark_full.append(valid_tokens[i]);
            landmark_full.append(" ");
        }
    }
    
    string landmark; 
    string figure;
    string relation_match;

    std::vector<std::string> tokens_l;
    split(tokens_l, landmark_full, is_any_of(" "));
    for(int i=0; i < tokens_l.size(); i++){
        if(tokens_l[i].compare("the")){
            landmark.append(tokens_l[i]);
            landmark.append(" ");
        }
    }    

    std::vector<std::string> tokens_f;
    split(tokens_f, figure_full, is_any_of(" "));
    for(int i=0; i < tokens_f.size(); i++){
        if(tokens_f[i].compare("the")){
            figure.append(tokens_f[i]);
            figure.append(" ");
        }
    }

    std::vector<std::string> tokens_r;
    split(tokens_r, relation_full, is_any_of(" "));
    for(int i=0; i < tokens_r.size(); i++){
        if(tokens_r[i].compare("the")){
            relation_match.append(tokens_r[i]);
            relation_match.append(" ");
        }
    }

    //match to the fixed set of relations we have 
    trim(relation_match);
    string relation;
    
    std::vector<std::string> tokens_relation;
    split(tokens_relation, relation_match, is_any_of(" "));

    int found_c = 0;
    for(int i=0; i < spatial_relations.size(); i++){
        for(int j=0; j < tokens_relation.size(); j++){
            if(!spatial_relations[i].compare(tokens_relation[j])){
                relation = spatial_relations[i];
                found_c++;
            }
        }        
    }
    if(found_c > 1){
        fprintf(stderr, "Multiple relations found\n");
    }

    trim(landmark);    
    trim(figure);

    Command cmd(command, relation, landmark, figure);
    return cmd;
}

RealVector SLUClassifier::get_data_vector(gsl_vector *v){
    RealVector vec(v->size);

    size_t n_cols = v->size;
    for(size_t i=0; i < n_cols; i++){

        double value = gsl_vector_get (v, i);
        vec[i] = value;
    }
    return vec;
}

void SLUClassifier::publish_dataset(string sr, vector<SLUDataPoint> &datapoints){
    slu_annotation_list_t *msg; 
    msg = (slu_annotation_list_t *) calloc(1,sizeof(slu_annotation_list_t));
    msg->count = (int) datapoints.size();
    msg->annotations = (slu_annotation_t *) calloc(msg->count, sizeof(slu_annotation_t));
    for(int i=0; i < datapoints.size(); i++){
        datapoints[i].to_lcm(sr, &msg->annotations[i]);
        msg->annotations[i].relation = strdup(sr.c_str());
    }

    slu_annotation_list_t_publish(lcm, "SLU_ANNOTATIONS", msg);
    slu_annotation_list_t_destroy(msg);
}

void SLUClassifier::publish_dataset_result(string sr, vector<SLUDataPoint> &datapoints, vector<double> prob){
    slu_annotation_list_t *msg; 
    msg = (slu_annotation_list_t *) calloc(1,sizeof(slu_annotation_list_t));
    msg->count = (int) datapoints.size();
    msg->annotations = (slu_annotation_t *) calloc(msg->count, sizeof(slu_annotation_t));
    
    bool valid = false;
    if(prob.size() == datapoints.size()){
        valid = true;
    }
    
    for(int i=0; i < datapoints.size(); i++){
        datapoints[i].to_lcm(sr, &msg->annotations[i]);
        if(valid){
            msg->annotations[i].prob = prob[i];
        }
    }

    slu_annotation_list_t_publish(lcm, "SLU_ANNOTATIONS", msg);
    slu_annotation_list_t_destroy(msg);
}

void SLUClassifier::print_bot_trans(const BotTrans &bt) const{
    double rpy[3]; 
    bot_quat_to_roll_pitch_yaw(bt.rot_quat, rpy);
    fprintf(stderr, "%.2f, %.2f, %.2f\n", bt.trans_vec[0], bt.trans_vec[1], bot_to_degrees(rpy[2]));
}

/*
  vector<double> SLUClassifier::get_hack_likelihood(string sr, vector<SLUDataPoint> &datapoints){
    vector<double> results;
    //cout << "Hack eval called : " << sr << endl; 
    for(int i=0; i < datapoints.size(); i++){
        Path pth = datapoints[i].get_path(); //path_grounding;
        if(pth.poses.size() > 1){
            int end = pth.poses.size() -1;
            //pth.poses[0].print(true); //current(heard) location
            //pth.poses[end].print(true); //figure location 

            BotTrans agent_to_local = pth.poses[0].get_bot_trans();
            BotTrans figure_to_local = pth.poses[end].get_bot_trans();
            
            BotTrans local_to_agent = agent_to_local;
            bot_trans_invert(&local_to_agent);
            BotTrans figure_to_agent; 
            bot_trans_apply_trans_to(&local_to_agent, &figure_to_local, &figure_to_agent);

            //print_bot_trans(figure_to_agent);

            double rpy[3]; 
            bot_quat_to_roll_pitch_yaw(figure_to_agent.rot_quat, rpy);
            //fprintf(stdout, "Figure to agent : %f,%f,%f\n", figure_to_agent.trans_vec[0], figure_to_agent.trans_vec[1], bot_to_degrees(rpy[2]));

            double dist = hypot(figure_to_agent.trans_vec[0], figure_to_agent.trans_vec[1]);
            double theta = atan2(figure_to_agent.trans_vec[1], figure_to_agent.trans_vec[0]);
            
            //fprintf(stdout, "Distance : %.3f Orientation : %.2f\n", dist, bot_to_degrees(theta));

            if(!sr.compare("near")){
                double near_threshold = 10.0;
                double dx = near_threshold - dist;
                double prob = 1/ (1+ exp(-dx));
                //fprintf(stdout, "Probability : %.2f\n", prob);
                results.push_back(prob);
            }
            else if(!sr.compare("behind")){
                double sigma = 2.0;
                //for now a simple hack - put a center on the left (0,10) and see 
                BotTrans behind_agent; 
                behind_agent.trans_vec[0] = -10;
                behind_agent.trans_vec[1] = 0;
                behind_agent.trans_vec[2] = 0;

                double rpy[3] = {0, 0, 0};
                
                bot_roll_pitch_yaw_to_quat(rpy, behind_agent.rot_quat);

                BotTrans figure_to_behind; 
                BotTrans agent_to_behind = behind_agent; 
                bot_trans_invert(&agent_to_behind);
                
                bot_trans_apply_trans_to(&agent_to_behind, &figure_to_agent, &figure_to_behind);

                double threshold = 5.0;
                double dist_from = hypot(figure_to_behind.trans_vec[0], figure_to_behind.trans_vec[1]);

                //fprintf(stdout, "Distance from behind : %f\n", dist_from_behind);
                double dx = (threshold - dist_from)/sigma;

                double prob = 1/ (1+ exp(-dx));
                
                results.push_back(prob);
            }
            else if(!sr.compare("infrontof")){
                double sigma = 2.0;
                //for now a simple hack - put a center on the left (0,10) and see 
                BotTrans behind_agent; 
                behind_agent.trans_vec[0] = 10;
                behind_agent.trans_vec[1] = 0;
                behind_agent.trans_vec[2] = 0;

                double rpy[3] = {0, 0, 0};
                
                bot_roll_pitch_yaw_to_quat(rpy, behind_agent.rot_quat);

                BotTrans figure_to_behind; 
                BotTrans agent_to_behind = behind_agent; 
                bot_trans_invert(&agent_to_behind);
                
                bot_trans_apply_trans_to(&agent_to_behind, &figure_to_agent, &figure_to_behind);

                double threshold = 5.0;
                double dist_from = hypot(figure_to_behind.trans_vec[0], figure_to_behind.trans_vec[1]);

                //fprintf(stdout, "Distance from behind : %f\n", dist_from_behind);
                double dx = (threshold - dist_from)/sigma;

                double prob = 1/ (1+ exp(-dx));
                
                results.push_back(prob);
            }
            else if(!sr.compare("left")){
                double sigma = 2.0;
                //for now a simple hack - put a center on the left (0,10) and see 
                BotTrans behind_agent; 
                behind_agent.trans_vec[0] = 0;
                behind_agent.trans_vec[1] = 10;
                behind_agent.trans_vec[2] = 0;

                double rpy[3] = {0, 0, 0};
                
                bot_roll_pitch_yaw_to_quat(rpy, behind_agent.rot_quat);

                BotTrans figure_to_behind; 
                BotTrans agent_to_behind = behind_agent; 
                bot_trans_invert(&agent_to_behind);
                
                bot_trans_apply_trans_to(&agent_to_behind, &figure_to_agent, &figure_to_behind);

                double threshold = 5.0;
                double dist_from = hypot(figure_to_behind.trans_vec[0], figure_to_behind.trans_vec[1]);

                //fprintf(stdout, "Distance from behind : %f\n", dist_from_behind);
                double dx = (threshold - dist_from)/sigma;

                double prob = 1/ (1+ exp(-dx));
                
                results.push_back(prob);
            }
            else if(!sr.compare("right")){
                double sigma = 2.0;
                //for now a simple hack - put a center on the left (0,10) and see 
                BotTrans behind_agent; 
                behind_agent.trans_vec[0] = 0;
                behind_agent.trans_vec[1] = -10;
                behind_agent.trans_vec[2] = 0;

                double rpy[3] = {0, 0, 0};
                
                bot_roll_pitch_yaw_to_quat(rpy, behind_agent.rot_quat);

                BotTrans figure_to_behind; 
                BotTrans agent_to_behind = behind_agent; 
                bot_trans_invert(&agent_to_behind);
                
                bot_trans_apply_trans_to(&agent_to_behind, &figure_to_agent, &figure_to_behind);

                double threshold = 5.0;
                double dist_from = hypot(figure_to_behind.trans_vec[0], figure_to_behind.trans_vec[1]);

                //fprintf(stdout, "Distance from behind : %f\n", dist_from_behind);
                double dx = (threshold - dist_from)/sigma;

                double prob = 1/ (1+ exp(-dx));
                
                results.push_back(prob);
            }            
        }        
    }
    return results;
}
*/

double SLUClassifier::get_sigmoid_value(double val, double mean, double threshold, double sigma){
    double delta = fabs(val - mean);

    double dev = (threshold - delta) / sigma;

    double max_prob = 1 / (1 + exp(-threshold /sigma));

    return 1/ (1+ exp(-dev)) / max_prob;
}

double SLUClassifier::get_sigmoid_value_angle(double val, double mean, double threshold, double sigma){
    double delta = fabs(bot_mod2pi(val - mean));

    double dev = bot_mod2pi((threshold - delta)) / sigma;

    double max_prob = 1 / (1 + exp(-threshold /sigma));

    return 1/ (1+ exp(-dev)) / max_prob;
}


vector<double> SLUClassifier::get_hack_likelihood(string sr, vector<SLUDataPoint> &datapoints){
    vector<double> results;
    //cout << "Hack eval called : " << sr << endl; 
    for(int i=0; i < datapoints.size(); i++){
        Path pth = datapoints[i].get_path(); //path_grounding;
        if(pth.poses.size() > 1){
            int end = pth.poses.size() -1;
            //pth.poses[0].print(true); //current(heard) location
            //pth.poses[end].print(true); //figure location 

            BotTrans agent_to_local = pth.poses[0].get_bot_trans();
            BotTrans figure_to_local = pth.poses[end].get_bot_trans();

            /*cout << "Agent to local" << endl;
            print_bot_trans(agent_to_local);
            cout << "Figure to local" << endl;
            print_bot_trans(figure_to_local);*/
            
            BotTrans local_to_agent = agent_to_local;
            bot_trans_invert(&local_to_agent);
            BotTrans figure_to_agent; 
            bot_trans_apply_trans_to(&local_to_agent, &figure_to_local, &figure_to_agent);

            //print_bot_trans(figure_to_agent);

            double rpy[3]; 
            bot_quat_to_roll_pitch_yaw(figure_to_agent.rot_quat, rpy);
            //fprintf(stdout, "Figure to agent : %f,%f,%f\n", figure_to_agent.trans_vec[0], figure_to_agent.trans_vec[1], bot_to_degrees(rpy[2]));

            double dist = hypot(figure_to_agent.trans_vec[0], figure_to_agent.trans_vec[1]);
            double theta = atan2(figure_to_agent.trans_vec[1], figure_to_agent.trans_vec[0]);
            
            //fprintf(stdout, "Distance : %.3f Orientation : %.2f\n", dist, bot_to_degrees(theta));

            double angle_threshold = 20; //20
            double angle_sigma = 20; //40

            double dist_sigma = 10.0; //2.0
            double dist_threshold = 10.0; // 3.0

            if(!sr.compare("near")){
                double dist_mean = 7.0;
                double prob = 0;

                dist_threshold = 3.0;
                dist_sigma = 3.0;
                if(dist < dist_mean){
                    prob = 1;
                }
                else{
                    prob = get_sigmoid_value(dist, dist_mean, dist_threshold, dist_sigma);
                }                
                //double near_threshold = 10.0;
                //double dx = near_threshold - dist;
                //double prob = 1/ (1+ exp(-dx));

                results.push_back(prob);
            }
            else if(!sr.compare("at")){
                double dist_mean = 2.0;
                double dist_sigma = 2.0;
                double prob = 0;

                dist_threshold = 1.0;

                if(dist < dist_mean){
                    prob = 1;
                }
                else{
                    prob = get_sigmoid_value(dist, dist_mean, dist_threshold, dist_sigma);
                }                
                //double near_threshold = 10.0;
                //double dx = near_threshold - dist;
                //double prob = 1/ (1+ exp(-dx));

                results.push_back(prob);
            }            
            else if(!sr.compare("awayfrom")){
                double dist_mean = 5.0;
                double prob = 0;
                if(dist < dist_mean){
                    prob = 1;
                }
                else{
                    prob = get_sigmoid_value(dist, dist_mean, dist_threshold, dist_sigma);
                }                
                //double near_threshold = 10.0;
                //double dx = near_threshold - dist;
                //double prob = 1/ (1+ exp(-dx));

                results.push_back(1 - prob);
            }
            else if(!sr.compare("behind")){
                double dist_mean = 5.0; //mean distance 

                double prob_x = get_sigmoid_value(dist, dist_mean, dist_threshold, dist_sigma);

                double t_mean = bot_to_radians(180);
                double t_threshold = bot_to_radians(angle_threshold);
                double t_sigma = bot_to_radians(angle_sigma);
        
                double prob_t = get_sigmoid_value_angle(theta, t_mean, t_threshold, t_sigma);
        
                double prob = prob_x * prob_t; 

                
                results.push_back(prob);
            }
            else if(!sr.compare("infrontof")){

                double dist_mean = 5.0; //mean distance 

                double prob_x = get_sigmoid_value(dist, dist_mean, dist_threshold, dist_sigma);

                double t_mean = bot_to_radians(0);
                double t_threshold = bot_to_radians(angle_threshold);
                double t_sigma = bot_to_radians(angle_sigma);

                double prob_t = get_sigmoid_value_angle(theta, t_mean, t_threshold, t_sigma);

                double prob = prob_x * prob_t; 
                
                results.push_back(prob);
            }
            else if(!sr.compare("left")){                
                double dist_mean = 5.0; //mean distance 

                double prob_x = get_sigmoid_value(dist, dist_mean, dist_threshold, dist_sigma);
        
                double t_mean = bot_to_radians(90);
                double t_threshold = bot_to_radians(angle_threshold);
                double t_sigma = bot_to_radians(angle_sigma);

                double prob_t = get_sigmoid_value_angle(theta, t_mean, t_threshold, t_sigma);
        
                double prob = prob_x * prob_t; 

                results.push_back(prob);
            }
            else if(!sr.compare("right")){
                double dist_mean = 5.0; //mean distance 

                double prob_x = get_sigmoid_value(dist, dist_mean, dist_threshold, dist_sigma);
        
                double t_mean = bot_to_radians(-90);
                double t_threshold = bot_to_radians(angle_threshold);
                double t_sigma = bot_to_radians(angle_sigma);
        
                double prob_t = get_sigmoid_value_angle(theta, t_mean, t_threshold, t_sigma);

                double prob = prob_x * prob_t; 
        
                results.push_back(prob);
            }            
        }
        else{
            if(!sr.compare("near")){
                double prob = 1.0;
                results.push_back(prob);
            }
            else if(!sr.compare("at")){
                double prob = 1.0; 
                results.push_back(prob);
            }            
            else if(!sr.compare("awayfrom")){
                double prob = 0.0;
                results.push_back(1 - prob);
            }
        }
    }
    return results;
}



vector<double> SLUClassifier::get_likelihood(string sr, vector<SLUDataPoint> &datapoints, bool publish, bool verb){
    map<string, RFClassifier>::iterator it_model;
    vector<double> results;

    it_model = model_map.find(sr);
    if(it_model == model_map.end()){
        if(verb)
            cout << "Model not found for relation : " << sr << endl;
        return get_hack_likelihood(sr, datapoints);
    } 

    std::vector<RealVector> points;
    vector<string> orig_names = spatial_features::path_feature_names();

    int64_t s_utime_f = bot_timestamp_now();

    for(int i=0; i < datapoints.size(); i++){
        gsl_matrix *temp_path = datapoints[i].get_path_matrix();
        gsl_matrix *temp_bbox = datapoints[i].get_landmark_matrix();

        if(!temp_path || !temp_bbox){
            fprintf(stderr, "Error - null gsl mat\n");
            return results;
        }
        
        if(verbose){
            spatial_features::print_gsl_matrix(temp_path);
            spatial_features::print_gsl_matrix(temp_bbox);
        }
        
        gsl_vector *vec = spatial_features::path_feature_values(temp_path , temp_bbox, true);
        
        vector<string> new_names;
        
        spatial_features::rectify_path_features(orig_names, vec, new_names);
        if(verbose){
            spatial_features::print_features(new_names, vec);
        }
        
        RealVector v = get_data_vector(vec);
        points.push_back(v);
        //unsigned int l = annotation.get_relation_label();

        gsl_matrix_free(temp_path);
        gsl_matrix_free(temp_bbox);
        gsl_vector_free(vec);
    }

    int64_t e_utime_f = bot_timestamp_now();
    
    if(verb)
        fprintf(stdout, "Time to calculate features : %f\n", (e_utime_f - s_utime_f) / 1.0e6);

    Data<RealVector> data = createDataFromRange(points);

    //cout << data.
    Data<RealVector> prediction = it_model->second(data);    
    
    int64_t e_utime_p = bot_timestamp_now();
    if(verb)
        fprintf(stdout, "Time to calculate likelihoods : %f\n", (e_utime_p - e_utime_f) / 1.0e6);

    typedef Data<RealVector>::element_range Elements;
    
    Elements elements = prediction.elements();
    for(Elements::iterator pos = elements.begin(); pos != elements.end(); ++pos){
        RealVector rv = *pos;
        if(verb)
            fprintf(stdout, "[0] : %.2f - [1] : %.2f\n", rv[0], rv[1]);
        results.push_back(rv[1]); 
    }

    if(publish)
        publish_dataset_result(sr, datapoints, results);
    
    return results;
}
