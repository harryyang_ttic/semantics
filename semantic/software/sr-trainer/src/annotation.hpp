#ifndef SLU_ANNOTATION_H
#define SLU_ANNOTATION_H

#include <string.h>
#include <yaml-cpp/yaml.h>
#include <bot_core/bot_core.h>
#include <lcmtypes/sr_trainer.h>
#include <iostream>

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>

struct Feature{
  std::string name;
  double value;

  Feature(std::string _name, double _value):name(_name), value(_value) {};
};

class AnnotationException{
 public:
  std::string err;
  YAML::Node nd;
  AnnotationException(std::string _err, YAML::Node _nd):err(_err), nd(_nd) {};

  void print(){
    std::cout << "Error : " << err << std::endl;
    std::cout << nd << std::endl;
  }
};

class Point{
 public:
  double xyz[3];

  Point(){
    memset(xyz, 0, 3 * sizeof(double));
  };
  Point(double _xyz[3]){
    memcpy(xyz, _xyz, 3 * sizeof(double));
  }

  void print(){
    fprintf(stdout, "Pose : %.3f,%.3f,%.3f", xyz[0], xyz[1], xyz[2]);
  }
};

class Orientation{
 public:
  double quat[4];

  Orientation(){
    memset(quat, 0, 4 * sizeof(double));
  }

  Orientation(double yaw){
    double rpy[3] = {0,0, yaw};
    bot_roll_pitch_yaw_to_quat(rpy, quat);
  }

  Orientation(double _quat[4]){
    memcpy(quat, _quat, 4 * sizeof(double));
  }

  void get_rpy(double rpy[3]) const{
    bot_quat_to_roll_pitch_yaw(quat, rpy);
  }

  double get_yaw(){
    double rpy[3];
    bot_quat_to_roll_pitch_yaw(quat, rpy);
    return rpy[2];
  }

  void print(){
    double rpy[3];
    get_rpy(rpy);
    fprintf(stdout, " RPY : %.3f, %.3f, %.3f\n", rpy[0], rpy[1], rpy[2]);
  }

};

class Pose{
 public:
  int timestamp;
  Point position;
  Orientation orientation;
  //double quat[4];

  Pose(int _timestamp, double xyzt[4]):timestamp(_timestamp){
    position = Point(xyzt);
    orientation = Orientation(xyzt[3]);
  };

  Pose(int _timestamp, YAML::Node _point, bool yaw=true):timestamp(_timestamp){

    if(yaw){
      if(_point.size() != 4){
        fprintf(stderr, "Points of wrong size\n");
        exit(-1);
      }

      double xyzt[4];
      int i = 0;
      for(YAML::iterator p_it=_point.begin();p_it!=_point.end();++p_it, i++){
        YAML::Node _val = *p_it;
        xyzt[i] = _val.as<double>();
      }
      position = Point(xyzt);
      orientation = Orientation(xyzt[3]);
    }
    else{ //coming as pos and quaternion
      if(_point.size() != 7){
        fprintf(stderr, "Points of wrong size\n");
        exit(-1);
      }
      double xyz[3];
      double quat[4];
      int i = 0;
      for(int i=0; i < 3; i++){
        YAML::Node _val = _point[i];
        xyz[i] = _val.as<double>();
      }
      for(int i=3; i < 7; i++){
        YAML::Node _val = _point[i];
        quat[i-3] = _val.as<double>();
      }
      position = Point(xyz);
      orientation = Orientation(quat);
    }
  }

  void to_lcm(slu_pose_t *msg) const{
    msg->timestamp = timestamp;
    msg->pt.xyz[0] = position.xyz[0];
    msg->pt.xyz[1] = position.xyz[1];
    msg->pt.xyz[2] = position.xyz[2];

    orientation.get_rpy(msg->rpy);
  }

  void print(bool p_orientation=false){
    fprintf(stdout , "\t[%d]\t", timestamp);
    position.print();
    if(p_orientation){
      orientation.print();
    }
    else{
      fprintf(stdout, "\n");
    }
  }

  BotTrans get_bot_trans(){
    BotTrans tf;
    tf.trans_vec[0] = position.xyz[0];
    tf.trans_vec[1] = position.xyz[1];
    tf.trans_vec[2] = position.xyz[2];

    memcpy(tf.rot_quat, orientation.quat, 4 * sizeof(double));
    return tf;
  }

};

class Path{
 public:
  Path();
  Path(std::vector<Pose> path);

  Path(YAML::Node context);
  void print(bool orientation=false);
  gsl_matrix *get_gsl_matrix();

  void to_lcm(slu_path_t *msg) const;
  std::vector<Pose> poses;
};

class Prism{
 public:
  Prism() {}

  Prism(std::vector<Point> &points, double z_height[2]){
    for(int i=0; i < points.size(); i++){
      double xyz[3] = {points[i].xyz[0], points[i].xyz[1], z_height[0]};
      Point lp(xyz);
      xyz[2] = z_height[1];
      Point up(xyz);
      upper_points.push_back(up);
      lower_points.push_back(lp);
    }
  }

  Prism(YAML::Node _prism) {
    YAML::Node _z_start = _prism["zStart"];
    YAML::Node _z_end = _prism["zEnd"];
    YAML::Node _points = _prism["points"];

    double z_start = _z_start.as<double>();
    double z_end = _z_end.as<double>();

    for(int i=0; i < _points.size(); i++){
      YAML::Node _pt = _points[i];
      double xyz_u[3] = {_pt[0].as<double>(), _pt[1].as<double>(), z_end};
      double xyz_l[3] = {_pt[0].as<double>(), _pt[1].as<double>(), z_start};

      Point pt_u(xyz_u);
      Point pt_l(xyz_l);
      upper_points.push_back(pt_u);
      lower_points.push_back(pt_l);
    }
  }

  void print(){
    fprintf(stdout, "  Prism :\n");
    for(int i=0; i < upper_points.size(); i++){
      fprintf(stdout, "\t[U] ");
      upper_points[i].print();
      fprintf(stdout, " = \t[L] ");
      lower_points[i].print();
      fprintf(stdout, "\n");
    }
  }

  gsl_matrix *get_gsl_matrix(){
    //return NULL;
    size_t no_rows = 2;
    size_t no_columns = upper_points.size();
    gsl_matrix *object_mat = gsl_matrix_alloc (no_rows, no_columns);

    for(int i=0; i < upper_points.size(); i++){
      gsl_matrix_set(object_mat, 0, i, upper_points[i].xyz[0]);
      gsl_matrix_set(object_mat, 1, i, upper_points[i].xyz[1]);
    }
    return object_mat;
  }

  void to_lcm(slu_object_t *msg) const{
    msg->count = (int) upper_points.size();

    msg->upper_points = (slu_point_t *) calloc(msg->count, sizeof(slu_point_t));
    msg->lower_points = (slu_point_t *) calloc(msg->count, sizeof(slu_point_t));

    for(int i=0; i < upper_points.size(); i++){
      msg->upper_points[i].xyz[0] = upper_points[i].xyz[0];
      msg->upper_points[i].xyz[1] = upper_points[i].xyz[1];
      msg->upper_points[i].xyz[2] = upper_points[i].xyz[2];

      msg->lower_points[i].xyz[0] = lower_points[i].xyz[0];
      msg->lower_points[i].xyz[1] = lower_points[i].xyz[1];
      msg->lower_points[i].xyz[2] = lower_points[i].xyz[2];
    }
  }

  std::vector<Point> upper_points;
  std::vector<Point> lower_points;
};

class Object{
 public:
  Object();
  Object(YAML::Node object);
  Object(std::vector<Point> &landmark_points, double lm_height[2], std::vector<std::string> names);

  void print();

  Prism prism;
  Path path;
  std::vector<std::string> tags;

  void to_lcm(slu_object_t *msg) const;

  gsl_matrix *get_bbox_gsl_matrix();
};

class Command{
 public:
  Command();
  Command(std::string _command, std::string _relation, std::string _landmark, std::string _figure);
  Command(YAML::Node command);
  void print(std::ostream &os=std::cout) const;

  std::string get_relation();
  friend std::ostream& operator<<(std::ostream& os, const Command& obj);

  std::string command;
  std::string relation;
  std::string landmark;
  std::string figure;
};

class Grounding{
 public:
  bool landmark_grounding;
  bool relation_grounding;

  Grounding():landmark_grounding(false), relation_grounding(false)
  {};

  Grounding(bool _landmark_grounding, bool _relation_grounding): landmark_grounding(_landmark_grounding), relation_grounding(_relation_grounding){};

  Grounding(YAML::Node grounding):landmark_grounding(false), relation_grounding(false){
    if(grounding.size() < 2){
      std::cout << "Not enough groundings\n";
    }
    std::string r_gnd = grounding[0].as<std::string>();
    std::string l_gnd = grounding[1].as<std::string>();

    //probably should lower case it

    if(!r_gnd.compare("false")){
      relation_grounding = false;
    }
    else if(!r_gnd.compare("true")){
      relation_grounding = true;
    }
    else{
      fprintf(stderr, "Incorrect grounding label\n");
    }
    if(!l_gnd.compare("false")){
      landmark_grounding = false;
    }
    else if(!l_gnd.compare("true")){
      landmark_grounding = true;
    }
    else{
      fprintf(stderr, "Incorrect grounding label\n");
      exit(-1);
    }
  }

  void print(){
    if(relation_grounding){
      std::cout << "Relation Grounding : True \t";
    }
    else{
      std::cout << "Relation Grounding : False\t";
    }

    if(landmark_grounding){
      std::cout << "Landmark Grounding : True\n";
    }
    else{
      std::cout << "Landmark Grounding : False\n";
    }
  }

  bool get_landmark_grounding(){
    return landmark_grounding;
  }

  bool get_relation_grounding(){
    return relation_grounding;
  }
};

class Context{
 public:
  Context();
  Context(YAML::Node context);
};

//this is what we will use to querry (which has less stuff) - possibly not the agent as well
class SLUDataPoint{
 public:
  SLUDataPoint();
  SLUDataPoint(std::vector<Pose> path, std::vector<Point> landmark_bb, double lm_height[2]);
  SLUDataPoint(std::vector<Pose> path, std::vector<Point> landmark_point, double lm_height[2], std::vector<std::string> tags);
  Path &get_path();

  gsl_matrix *get_path_matrix();
  gsl_matrix *get_landmark_matrix();

  double prob;

  void print();

  int add_feature_vector(std::vector<std::string> names, gsl_vector *v);

  void to_lcm(std::string sr, slu_annotation_t *msg) const;

  bool isValid();

 private:
  bool valid;
  Path path_grounding;
  Object landmark_grounding;
  Command command;
  //Object agent;
  std::vector<Feature> feature_map;

};

class Annotation{
 public:
  Annotation(YAML::Node annotation);

  void to_lcm(slu_annotation_t *msg) const;

  void print();

  //we need to get the gsl matrix features from this

  gsl_matrix *get_path_matrix();
  gsl_matrix *get_landmark_matrix();

  int add_feature_vector(std::vector<std::string> name, gsl_vector *v);

  bool get_relation_label();

  bool get_landmark_label();

  std::vector<Feature> feature_map;
  std::string assignment_id;
  int id;
  std::string annotation_text;

  Object agent;
  Command command;

  Path path_grounding;
  Object landmark_grounding;

  Grounding grounding; //ground truth phi's
};

#endif
