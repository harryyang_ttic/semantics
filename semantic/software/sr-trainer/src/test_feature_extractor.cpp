#include "feature_extractor.hpp"
#include <iostream>

using namespace spatial_features;
using namespace std; 

int main(int argc, char **argv){

    vector<string> landmark;
    landmark.push_back(string("landmark"));
    
    vector<string> obj_f_names = object_feature_names(landmark);

    cout << " Object Features " << endl;

    for(int i=0; i < obj_f_names.size(); i++){
        cout << obj_f_names[i] << endl;
    }
    
    vector<string> pf_names = path_feature_names();
    
    cout << " Path Features " << endl;
    for(int i=0; i < pf_names.size(); i++){
        cout << pf_names[i] << endl;
    }    

    return 0;
}
