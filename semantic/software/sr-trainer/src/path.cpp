#include "annotation.hpp"

using namespace std;

Path::Path(){}

Path::Path(YAML::Node y_path){
    //cout << "Path : " << y_path << endl;

    bool yaw = false;
    YAML::Node path_list;
    if(y_path["points_xyztheta"]){
        path_list = y_path["points_xyztheta"];
        yaw = true;
    }
    else if(y_path["points_xyzquat"]){
        path_list = y_path["points_xyztheta"];
    }

    YAML::Node ts = y_path["timestamps"];

    if(ts.size() != path_list.size()){
        fprintf(stderr, "Timestamp size is not the same as the point size\n");
        return;
    }

    if(path_list.IsSequence()){
        for(int i=0; i < path_list.size(); i++){
            YAML::Node _point = path_list[i];
            int timestamp = ts[i].as<int>();

            Pose pose(timestamp, _point, yaw);
            //pose.print(true);
            poses.push_back(pose);
        }
    }
}

Path::Path(vector<Pose> path){
    poses = path;
}

void Path::print(bool orientation){
    cout << "  Path:" << endl;
    for(int i=0; i < poses.size(); i++){
        poses[i].print(orientation);
    }
}

gsl_matrix *Path::get_gsl_matrix(){
    size_t no_rows = 3;
    size_t no_columns = poses.size();
    gsl_matrix *path_mat = gsl_matrix_alloc (no_rows, no_columns);

    for(int i=0; i < poses.size(); i++){
        gsl_matrix_set(path_mat, 0, i, poses[i].position.xyz[0]);
        gsl_matrix_set(path_mat, 1, i, poses[i].position.xyz[1]);
        gsl_matrix_set(path_mat, 2, i, poses[i].orientation.get_yaw());
    }

    return path_mat;
}

void Path::to_lcm(slu_path_t *msg) const{
    msg->count = (int) poses.size();

    msg->points = (slu_pose_t *)calloc(msg->count, sizeof(slu_pose_t));

    for(int i=0; i < poses.size(); i++){
        poses[i].to_lcm(&msg->points[i]);
    }
}
