# topological graph slam

from particle import Particle
from node import Node
from edge import Edge

from numpy import *
from numpy.linalg import *
import scipy
from scipy import *
from scipy.integrate import*
import time
import sys
import threading
import random
import math
from optparse import OptionParser

from lcm import LCM
from lcm import EventLog
from erlcm.raw_odometry_msg_t import raw_odometry_msg_t
from slam.graph_particle_list_t import graph_particle_list_t
from slam.graph_particle_t import graph_particle_t
from slam.graph_node_t import graph_node_t
from slam.graph_node_list_t import graph_node_list_t
from slam.graph_edge_t import graph_edge_t
from slam.language_edge_t import language_edge_t
from slam.graph_node_t import graph_node_t
from slam.init_node_t import init_node_t
from bot_core.pose_t import pose_t
from common.quaternion import Quaternion

STATUS_INITIALIZED = 0
STATUS_SUCCESS = 1
STATUS_FAILED = 2
STATUS_PROCESSED = 3

TYPE_UNKNOWN = 0
TYPE_ODOM = 1
TYPE_SM = 2
TYPE_LANGUAGE = 3
TYPE_PREV_TO_CURRENT = 4

PARTICLE_STATUS_INIT = 0
PARTICLE_STATUS_HAVE_LOC = 1
PARTICLE_STATUS_PROCESSED = 2

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

    def disable(self):
        self.HEADER = ''
        self.OKBLUE = ''
        self.OKGREEN = ''
        self.WARNING = ''
        self.FAIL = ''
        self.ENDC = ''


class lcm_listener():
   def __init__(self, mode, numparticles, keep_edges, consec_scanmatch, resample, resample_always,printf):
      self._running = True
      self.lc = LCM()
      self.particle_sub = self.lc.subscribe("PARTICLE_RESULT", self.particle_handler)
      self.edge_sub = self.lc.subscribe("LANGUAGE_EDGE", self.edge_handler)
      self.node_sub = self.lc.subscribe("SLAM_PARTICLE_NODE_LIST", self.node_handler)
      self.received_slam_message = 0
      self.time = 0
      self.mode = mode
      self.const = 1.0 #sachi - tried with 2.0 also 
      self.constraint_dist_threshold = 8.0 #threshold beyond which nodes are not connected
      self.graph_node_distance_threshold = 4 #in mode 2 nodes closer than 4 will not be connected
      self.keepedges = keep_edges
      self.num_particles = numparticles
      self.particles = []
      self.next_particle_id = self.num_particles
      self.next_edge_id = 0
      self.consec_sm = TYPE_ODOM
      self.resample_var = resample
      self.resample_always = resample_always
      if consec_scanmatch: # if true, connect consecutive nodes via scanmatch. default - odometry
         self.consec_sm = TYPE_SM
      for i in range(self.num_particles):
         self.particles.append(Particle(particle_id = i, weight = math.log(1.0/(self.num_particles*1.0))))
      if printf:
         self.file = open("rayleigh.txt", 'w')
      self.printf = printf
      self.bad_edges = 0
      self.good_edges = 0
      
      
      
      
      
   def run(self):
      print "Started LCM listener"
      
      while self._running:
         try:
            self.lc.handle()
         except KeyboardInterrupt:
            print "\n============== Interrupt =================\n"
            self._running = False
            pass
            
            
            
      
            
   def edge_handler(self, channel, data):
      print "\nReceived language edge message"
      lang_edge = language_edge_t.decode(data)
      e = Edge(node1=lang_edge.node_id_1, node2=lang_edge.node_id_2, flag=STATUS_INITIALIZED, type_edge=TYPE_LANGUAGE)
      for i in self.particles:
         if i.id == lang_edge.particle_id:
            i.add_edge(e)
            
      e.transformation = [0,0,0]
      e.cov= [.5,0,0, 0,.5,0, 0,0,1]
      
      self.post_message()
   
   
   
   
   def node_handler(self, channel, data):
      print "\n=================================================\nReceived node init message"

      graph_node_list = graph_node_list_t.decode(data)
      self.time = graph_node_list.utime#pose.utime
      for i in graph_node_list.nodes:
         self.create_new_node(i)
         #print "Received node: ", i.id
      self.post_message()
 

 
 
  
   def create_new_node(self,node):
      print "Adding new node"
      for i in range(self.num_particles):
         self.particles[i].num_nodes += 1
         n = Node(node_id=node.id, xy=[node.xy[0],node.xy[1]], utime=node.utime, is_supernode=node.is_supernode)
         self.particles[i].nodelist.append(n)
         if self.particles[i].num_nodes > 1:
            e = Edge(node1=self.particles[i].nodelist[self.particles[i].num_nodes-1].id, node2=self.particles[i].nodelist[self.particles[i].num_nodes-2].id, flag = STATUS_INITIALIZED, type_edge = self.consec_sm)
            self.particles[i].add_edge(e)
   
   
   
   
   
   def particle_handler(self, channel, data):
      self.good_edges = 0
      self.received_slam_message = True
      print "received input particle"
      adjust = []
      particle_list = graph_particle_list_t.decode(data)
      self.time = particle_list.utime
      status = particle_list.status
      for i in range(particle_list.no_particles):
         par = particle_list.particle_list[i]
         #print "------------------------------------------\nParticle Id: ", par.id
         for j in range(par.no_nodes):
            node = par.node_list[j]
            self.particles[i].nodelist[j].xy = node.xy
            self.particles[i].nodelist[j].heading = node.heading
            self.particles[i].nodelist[j].pofz = node.pofz
            self.particles[i].nodelist[j].is_supernode = node.is_supernode
            self.particles[i].nodelist[j].cov = node.cov
            if self.particles[i].nodelist[j].have_loc == False:
               self.particles[i].nodelist[j].have_loc = True
         adjust.append([])
         for j in range(par.no_edges):
            edge = par.edge_list[j]
            for k in self.particles[i].edges:
               if k.edge_id == edge.id:
                  k.flag = edge.status
                  if k.flag == STATUS_SUCCESS and not edge.type == TYPE_PREV_TO_CURRENT:
                     adjust[i].append(edge.pofz)
                     k.flag = STATUS_PROCESSED
                  if edge.status == STATUS_FAILED and not self.keepedges:
                     self.particles[i].edges.remove(k)
                     self.bad_edges += 1
                  elif edge.status == STATUS_SUCCESS:
                     self.good_edges += 1
                  if self.printf:
                     self.file.write("Z " + str(self.bad_edges) + " " + str(self.good_edges))
         #if par.weight_multiplier < 0.707106781186 or par.weight_multiplier > .707106781188:
         #   print bcolors.WARNING + str(par.weight_multiplier) + bcolors.ENDC         
         #else:
         #   print "wm: ", par.weight_multiplier
         self.particles[i].nodelist[self.particles[i].num_nodes-1].pofz = par.weight_multiplier
      self.update_weights(adjust)
      if self.add_edges(status):
         self.post_message(status=status)
      
      if self.resample_var:
         if self.calculate_N_eff() < self.num_particles/2 or self.resample_always:
            self.resample()
            #print "RESAMPLED - N_EFF: ", self.calculate_N_eff()
         #else:
            #print "NO RESAMPLE - N_EFF: ", self.calculate_N_eff()
      
      
      
      
      
   def calculate_N_eff(self):
      neff = 0
      for i in range(self.num_particles):
         neff += math.exp(self.particles[i].weight)**2
      return 1.0/neff
   




   def add_edges(self, status):
      changed = False
      #for each particle
      for i in range(self.num_particles):
         for node in self.particles[i].nodelist:
            #if node.processed and not node.edges_added:
            if status == 1 and node.have_loc and not node.processed: #ie, we have the locations and haven't processed yet
               if node.is_supernode:
                  if self.mode == 1 and i == 0:
                     pass
                  else:
                     self.create_edges(self.particles[i],node)
                     changed = True
               node.processed = True
      return changed




   def create_edges(self,particle,node):
      if(particle.num_nodes == 1):
         #print "Only one node - no edges created"
         return
      
      for i in particle.nodelist:
         if(i.id >= node.id or node.processed == True or i.is_supernode == 0):
            continue
         
         #print "Calculating distance from: ", node.id, " to: ", i.id
         #print "processed: ", node.processed
         #print "edges_added: ", node.edges_added 
         (d,sigma) = self.calc_distance_between_nodes(node,i)
         #print "Distance to ", i.id , " : ",  d, "; sigma : ", sigma
         #p = self.calc_probability_gaussian(d,sigma)
         p = self.calc_probability_rice(d,sigma)
         r = random.random() #/ 100 #HACK - divided by 5 to make the code make more edges
         #print "Threshold : " , p , " Rand : ", r
         string = "A " + str(d) + " " + str(sigma) + " " + str(p) + " " + str(r) + "\n"
         if self.printf:
            self.file.write(string)
         if r < p:
            #print "PROPOSING EDGE"
            print "Distance to ", i.id , " from ", node.id, " : ",  d, "; sigma : ", sigma
            print "Threshold : " , p , " Rand : ", r
            e = Edge(node1=node.id, node2=i.id, flag=STATUS_INITIALIZED, type_edge=TYPE_SM)
            particle.add_edge(e)
      #if mode == 1 => Only current to previous nodes are connected 
      #if mode == 2 => close nodes other than the current to previous are not conencted 






   def calc_distance_between_nodes(self, node1, node2):
      x1 = node1.xy[0]
      y1 = node1.xy[1]
      x2 = node2.xy[0]
      y2 = node2.xy[1]
      
      dist = math.sqrt((x1-x2)**2 + (y1-y2)**2)
      #return (dist,0)

      cov1 = []
      cov1.append([])
      cov1.append([])
      cov1[0].append(node1.cov[0])
      cov1[0].append(node1.cov[1])
      cov1[1].append(node1.cov[3])
      cov1[1].append(node1.cov[4])
      cov2 = []
      cov2.append([])
      cov2.append([])
      cov2[0].append(node2.cov[0])
      cov2[0].append(node2.cov[1])
      cov2[1].append(node2.cov[3])
      cov2[1].append(node2.cov[4])
      
      
      g = lambda x1,y1,x2,y2: .5*((x1-x2)**2+(y1-y2)**2)**-.5
      
      sq = g(x1,y1,x2,y2)
      
      H = []
      H.append(sq*2*(x1-x2))
      H.append(sq*2*(y1-y2))
      H.append(sq*-2*(x1-x2))
      H.append(sq*-2*(y1-y2))
      
      H = array(H)
      
      
      S = []
      t = [cov1[0][0],cov1[0][1],0,0]
      S.append(t)
      t = [cov1[1][0],cov1[1][1],0,0]
      S.append(t)
      t = [0,0, cov2[0][0],cov2[0][1]]
      S.append(t)
      t = [0,0, cov2[1][0],cov2[1][1]]
      S.append(t)
      
      S = array(S)
      
      #print "H: ", H, "\nS: ", S
      
      sigma = dot(H,dot(S,H.T))
      
      
      #print "distance: ", dist
      return (dist, sigma)
      
      
      
   def make_gauss(self, mu,sigma):
       k = 1 / (sigma * math.sqrt(2*math.pi))
       s = -1.0 / (2 * sigma * sigma)
       def f(x):
           return k * math.exp(s * (x - mu)*(x - mu))
       return f
       
       
       
       
       
   def calc_probability_rayleigh(self, d, sigma):
      if sigma == 0:
         print "ERROR! Sigma is zero!"
         sigma = .001
      par = d / ( math.sqrt( math.pi / 2 ) )
      f = lambda x: ( x / par**2 ) * math.exp( -x**2 / ( 2 * par**2 ) )
      g = lambda x:  1.0 / (self.const*x**2 + 1)
      h = lambda x: g(x)*f(x)
      (val,error) = quad(h, 0, self.constraint_dist_threshold)
      return val





   def calc_probability_rice(self, d, sigma):
      if sigma == 0:
         print "ERROR! Sigma is zero!"
         sigma = .001
      f = lambda x: ( x / (sigma**2 * self.const*x**2 +1) ) * math.exp( -(x**2+d**2) / ( 2 * sigma**2 ) ) * i0(x*d / sigma**2)
      g = lambda x:  1.0 / (self.const*x**2 + 1)
      h = lambda x: g(x)*f(x)
      (val,error) = quad(f, 1, self.constraint_dist_threshold, limit=500, epsabs=100, epsrel=100)
      print "integral value: ", val
      return val





   def calc_probability_gaussian(self, d, sigma):
      if sigma == 0:
         print "ERROR! Sigma is zero!"
         sigma = .001
      f = self.make_gauss(d,sigma)
      g = lambda x:  1.0 / (self.const*x**2 + 1)
      h = lambda x: g(x)*f(x)
      (val,error) = quad(h, 0, self.constraint_dist_threshold)
      return val

         
         
         
         
         
   def update_weights(self, adjust_list):
      prob_sum = 0
      
      adjust = []
      
      for i in range(self.num_particles):
         adjust.append(1)
         for j in adjust_list[i]:
            adjust[i] *= j
      for i in range(self.num_particles):
         if(adjust[i] <= 0):
            print "ERROR: particle weight multiplier is less than 0"
            self.particles[i].weight = 0
         else:
            self.particles[i].weight = self.particles[i].weight + math.log(adjust[i])
            prob_sum += math.exp(self.particles[i].weight)
         #print prob_sum
      if prob_sum > 1:
         print "ERROR: sum of probabilities greater than 1"
      elif prob_sum == 0:
         print "ERROR: sum of probabilities is 0"
      else:
         for i in self.particles:
            #print "weight before: ", i.weight
            i.weight = i.weight - math.log(prob_sum)
            #print "weight after: ", i.weight
            #print "i, weight:", i, ",", math.exp(i.weight)
         
      
      
   
         
   def post_message(self, status=None):
      msg = graph_particle_list_t()
      msg.no_particles = self.num_particles
      msg.utime = time.time() * 10**6
      if status == PARTICLE_STATUS_HAVE_LOC:
         msg.status = PARTICLE_STATUS_PROCESSED
      elif status == None:
         msg.status == PARTICLE_STATUS_INIT
      else:
         msg.status = status
      parlist = []
      for i in range(self.num_particles):
         #export lcm message. Channel: PARTICLE_OUT; Message type: slam_graph_particle_t
         par = graph_particle_t()
         par.id = self.particles[i].id
         par.no_nodes = self.particles[i].num_nodes
         nodelist = []
         for j in self.particles[i].nodelist:
            newnode = graph_node_t()
            newnode.id = j.id
            newnode.utime = j.utime
            newnode.xy = j.xy
            newnode.heading = j.heading
            newnode.pofz = j.pofz
            newnode.is_supernode = j.is_supernode
            nodelist.append(newnode)
         par.node_list = nodelist
         par.no_edges = len(self.particles[i].edges)
         edgelist = []
         for j in self.particles[i].edges:
            newedge = graph_edge_t()
            newedge.node_id_1 = j.node1
            newedge.node_id_2 = j.node2
            newedge.status = j.flag
            newedge.id = j.edge_id
            newedge.type = j.type_edge
            if newedge.type == 3:
               newedge.transformation = j.transformation
               newedge.cov = j.cov
            else:
               newedge.transformation = [0,0,0]
               newedge.cov = [0,0,0, 0,0,0, 0,0,0]
            edgelist.append(newedge)
         par.edge_list = edgelist
         par.weight = self.particles[i].weight
         par.weight_multiplier = 0
         parlist.append(par)
      msg.particle_list = parlist
      self.lc.publish("PARTICLE_OUT",msg.encode())
      print "particle out message sent"
   
   
   
   
   
   def resample(self):
      a = []
      for i in range(self.num_particles):
         a.append([])
         if i == 0:
            a[i].append(math.exp(self.particles[i].weight))
         else:
            a[i].append(math.exp(self.particles[i].weight)+a[i-1][0])
         a[i].append(0)
      for i in range(self.num_particles):
         t = random.random()
         par = len(a)-1
         for j in range(len(a)):
            if t > a[j][0]:
               par = j-1
         a[par][1] += 1
      
      new_particles = []
      
      for i in range(self.num_particles):
         n = a[i][1]
         if n > 0:
            new_particles.append(self.particles[i])
            print "old id:", new_particles[len(new_particles)-1].id
            for j in range(n-1):
               new_particles.append(Particle(par = self.particles[i], particle_id = self.next_particle_id))
               self.next_particle_id += 1
               print "new id: ", new_particles[len(new_particles)-1].id
      
      for i in range(self.num_particles):
         new_particles[i].weight = math.log(1.0 / self.num_particles)
      
      self.particles = new_particles
      self.post_message()
         
      
      
      
      
if __name__ == "__main__":
   parser = OptionParser()
   parser.add_option("-l", dest = "mode", action="store_true", 
                     help="Special mode - different actions on each particle", default=False)
   parser.add_option("-p", dest = "numparticles", action="store", type="int",
                     help="Number of particles",  default = 10)
   parser.add_option("-e", dest = "keep_edges", action="store_true", help="Keep all edges - do not discard bad scan matches", 
                     default = False)
   parser.add_option("--sm", dest = "consec_scanmatch", action="store_true",
                     help="get constraints between consecutive nodes via scanmatch, not odometry", default = False)
   parser.add_option("--nr", dest = "resample", action="store_false", help="will keep the filter from resampling", default = True)
   parser.add_option("-r", dest = "resample_always", action="store_true", help="will make the filter resample at every step", default = False)
   parser.add_option("-f", dest = "printf", action="store_true", help="prints out edge values to a file", default = False)
   (options, args) = parser.parse_args()
   mode = False
   numparticles = 10
   keep_edges = False
   consec_scanmatch = False
   resample = True
   resample_always = False
   printf = False
   
   if(options.numparticles != None):
      numparticles = options.numparticles
   
   if(options.mode != None):
      mode = options.mode
      
   if(options.keep_edges != None):
      keep_edges = options.keep_edges
      
   if(options.consec_scanmatch !=None):
      consec_scanmatch = options.consec_scanmatch
   
   if(options.resample != None):
      resample = options.resample
      
   if(options.resample_always != None):
      resample_always = options.resample_always
      
   if(options.printf != None):
      printf = options.printf
      
   #print "options: ", options 
   #print "mode", mode
   
   background = lcm_listener(mode, numparticles, keep_edges, consec_scanmatch,resample,resample_always, printf)
   bg_thread = threading.Thread( target=background.run )
   bg_thread.start()
      
   
