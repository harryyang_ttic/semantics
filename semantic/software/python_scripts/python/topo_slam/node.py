# Node class for topological graph SLAM

class Node():
   def __init__(self, node_id = 0, utime = 0, xy = None, heading = 0, flag = 0, is_supernode = 0):
      self.id = node_id
      self.utime = utime
      if xy == None:
         self.xy = [0,0]
      else:
         self.xy = xy
      self.heading = heading
      self.flag = flag
      self.no_constraints = 0;
      self.pofz = 0
      self.is_supernode = is_supernode;
      self.cov = []
      self.have_loc = False
      self.processed = False
