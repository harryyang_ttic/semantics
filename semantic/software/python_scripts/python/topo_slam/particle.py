# Particle class for topological graph SLAM

class Particle():
   def __init__(self, particle_id = 0, num = 0, weight = 1.0, par = None):
      if par == None:
         self.id = particle_id
         self.num_nodes = num
         self.nodelist = []
         self.edges = []
         self.weight = weight
         self.next_edge_id = 0
      else:
         self.id = particle_id
         self.num_nodes = par.num_nodes
         self.nodelist = par.nodelist[:]
         self.edges = par.edges[:]
         self.weight = 0
         if len(self.edges)>0:
            self.next_edge_id = self.edges[len(self.edges)-1].edge_id+1
         else:
            self.next_edge_id = 0
         
   def add_edge(self, e):
      e.edge_id = self.next_edge_id
      self.next_edge_id += 1
      self.edges.append(e)
