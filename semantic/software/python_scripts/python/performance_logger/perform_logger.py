##    * functions
##          o output_as_speech( text )
import threading
import time
from optparse import OptionParser
import pygtk
import gtk
import lcm
import functools
from slam.performance_t import performance_t
import matplotlib.pyplot as plt
from Tkinter import *

root=Tk()

class lcm_listener(threading.Thread):
    def __init__(self, callback):
        threading.Thread.__init__(self)
        self.callback = callback
        self.lc = lcm.LCM()
        self.subscription_performance = self.lc.subscribe("SLAM_PROGRESS", self.performance_handler)
        self.perform = []
        self.t = []
        self.delta1 = []
        self._running = True        
        self.first_utime = -1;

    def performance_handler(self, channel, data):           
        msg = performance_t.decode(data)
        if(self.first_utime < 0): 
            self.first_utime = msg.last_sensor_utime
        delta_1 = (msg.last_node_utime - msg.last_node_in_graph_utime) / 1.0e6
        delta_2 = (msg.last_sensor_utime - msg.last_node_in_graph_utime) / 1.0e6
        result = {'utime':msg.last_sensor_utime, 'delta1':delta_1, 'delta2':delta_2}
        delta_t = (msg.last_sensor_utime - self.first_utime) / 1.0e6
        self.t.append(delta_t)
        self.delta1.append(delta_1)
        print msg.last_sensor_utime , ",", msg.last_node_utime, ",", msg.last_node_in_graph_utime

        self.perform.append(result)
        
        #plt.plot(self.t, self.delata1)
        #plt.show()
        t = threading.Thread( target=functools.partial(self.callback , [self.t, self.delta1]))
        t.start()
        

    def run(self):
        print "Started LCM Listener"
        try:
            while self._running:
                self.lc.handle()
        except KeyboardInterrupt:
            pass

class plot_graph:
    def __init__( self):
        self.data_x = []
        self.data_y = []
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111)
        #plt.show()
        plt.ion()
    def plot_data(self, data):
        #print "Updated"
        self.data_x = data[0]
        self.data_y = data[1]
        #y = data[1]
        #plt.plot(x,y)
        #plt.show()

    def print_data(self):
        #line1, = ax.plot(x, y, 'r-')
        #print "Plot utime : " , self.data_x
        #print "Plot Delta : " , self.data_y
        plt.clf()
        #print "."
        plt.plot(self.data_x, self.data_y)
        #line1 = self.ax.plot(self.data_x, self.data_y)
        #plt.show()
        #self.fig.canvas.draw()
        root.after(200, self.print_data)
        time.sleep(1e-6)

if __name__ == "__main__":    
    parser = OptionParser()
    parser.add_option("-p", "--path", dest="path",action="store",
                     help="File Path")
    
    (options, args) = parser.parse_args()
    
    plotter = plot_graph()
    background = lcm_listener(plotter.plot_data)
    background.start()

    #Add simple gui here if you want 
    
    #gtk.gdk.threads_init()
    #gtk.main()
    root.after(200, plotter.print_data)
    root.mainloop()
