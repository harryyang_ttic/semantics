import numpy as na


class LabelDistribution():
   """
   Label distribution class for language_observation script
   """

   def __init__(self, label_map, region_label_dist, landmark_label, cond_prob, use_full_dist=True):
      self.label_map = label_map

      self.labels = []
      
      keys = label_map.keys()
      keys.sort()

      if(use_full_dist):
         for i in keys: 
            #print "Key : " , i, " Label : " , self.label_map[i]
            label = self.label_map[i]
            self.labels.append(label)#self.label_map[i]]'''

         #print "Labels (ordered)"
         #print self.labels

         self.distribution = {}

         self.num_labels = len(self.labels)
         self.total_obs = 0

         self.observation_frequency = na.zeros(len(self.labels))
         
         for i in region_label_dist.classes:
            l_id = i.type
            #print "ID : " , l_id , "  => Label : " , self.label_map[l_id], "Prob " , i.probability
            self.observation_frequency[i.type] = i.probability

            self.total_obs += i.probability

      else: 
         self.labels.append(landmark_label)
         self.labels.append('other')

         print "Labels (ordered)"
         print self.labels

         self.distribution = {}

         self.num_labels = len(self.labels)
         self.total_obs = 0

         self.observation_frequency = na.zeros(len(self.labels))

         #defining this here for now 
         for i in region_label_dist.classes:
            l_id = i.type
            print "ID : " , l_id , "  => Label : " , self.label_map[l_id], "Prob " , i.probability

            #we now calculate the probability of generating the landmark label given the label distribution 
            #and the conditional label distribution
            p_of_label_given_ld = cond_prob[landmark_label][self.label_map[l_id]]
            prob_of_label = p_of_label_given_ld * i.probability
            prob_of_not_label = (1 - p_of_label_given_ld) * i.probability
            self.observation_frequency[0] += prob_of_label
            self.observation_frequency[1] += prob_of_not_label

            #print "Prob of LD label (", self.label_map[l_id] , ") : " , i.probability, " Prob of generating Label (", landmark_label , "): " , prob_of_label , "( " , p_of_label_given_ld , "*",  i.probability , ")"
            #print "\tProb Not Label : " , prob_of_not_label

            self.total_obs += (prob_of_label + prob_of_not_label)
            #self.observation_frequency[l_id] = i.probability
            #self.total_obs += i.probability

         print "Prob of Label     : " , self.observation_frequency[0]
         print "Prob of Not Label : " , self.observation_frequency[1]

      self.count_labels = []
      for i in range(self.num_labels):
         self.count_labels.append(0)


   def __init__old(self, label_map, region_label_dist):
      self.label_map = label_map

      self.labels = []
      
      keys = label_map.keys()
      keys.sort()

      for i in keys: 
         print "Key : " , i, " Label : " , self.label_map[i]
         label = self.label_map[i]
         self.labels.append(label)#self.label_map[i]]

      print "Labels (ordered)"
      print self.labels

      self.distribution = {}

      self.num_labels = len(self.labels)
      self.total_obs = 0
      
      self.observation_frequency = na.zeros(len(self.labels))
      
      #defining this here for now 
      for i in region_label_dist.classes:
         l_id = i.type
         print "ID : " , l_id , "  => Label : " , self.label_map[l_id], "Prob " , i.probability
         self.observation_frequency[l_id] = i.probability
         self.total_obs += i.probability

      self.count_labels = []
      for i in range(self.num_labels):
         self.count_labels.append(0)

   def get_main_label(self):
      m = 0
      #now this can have multiple elements - and then can break if they have the same likelihoods 
      for i in range(self.num_labels):
         if self.observation_frequency[i] > self.observation_frequency[m]:
            m = i
      return m

   def is_uniform(self):
      """
      True if all labels are equally likely; false otherwise
      """
      if na.any(na.array(self.observation_frequency) - self.observation_frequency[0]):
         return False
      else:
         return True

   def best_label(self):
      if self.is_uniform():
         return ""
      else:
         return self.labels[na.argmax(self.observation_frequency)]
      
   def add(self, l, amount=1):
      self.observation_frequency[l]+=amount
      self.total_obs += amount
      if amount == 1:
         self.count_labels[l]+=1
   
   def getlabels(self):
      rtrn = []
      for i in range(len(self.count_labels)):
         for j in range(self.count_labels[i]):
            rtrn.append(i)
      #print "count labels: ", self.count_labels
      #print "rtrn: ", rtrn
      return rtrn
      
   def printdata(self):
      print "Total observations: ", self.total_obs
      s = "obs dist: "
      for i in range(self.num_labels):
         s += str(self.observation_frequency[i]) + " "
      print s
      
   def getprobability(self, s):
      """
      Returns the probability for multinomial key 's', or 0 if s is unknown.
      """
      for i in range(len(self.labels)):
         if self.labels[i] == s:
            return self.observation_frequency[i] /self.total_obs
      return 0

   def getprobability_index(self, i):
      """
      Returns the probability for multinomial key 's', or 0 if s is unknown.
      """
      if(i >= 0 and i < len(self.observation_frequency)):
         return self.observation_frequency[i] /self.total_obs      
      else:
         return 0
   
   def getdictionary(self):
      """
      Returns a dictionary of string: probability for the multinomial
      """
      d = dict()
      for i in self.labels:
         d[i] = self.getprobability(i);
      return d
