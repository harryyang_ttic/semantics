from g3.inference import nodeSearch
from g3.cost_functions.cost_function_semantic_mapping import CostFnSemanticMapping
from assert_utils import sorta_eq
import spatial_features_cxx as sf
from esdcs import esdcIo
from esdcs.groundings import Path, PhysicalObject, Prism, Place
from scipy import transpose as tp
import numpy as na
from esdcs.extractor import stanfordParserExtractor
from g3.esdcs_to_ggg import gggs_from_esdc_group
from semantic_mapping.semantic_mapping_state import node_to_physical_object
import math
from stopwords import stopwords
from g3.graph import GGG
from g3.evidences import Evidences
from esdcs.context import Context

class FigureText:
    def __init__(self, esdc):
        self.figure_esdc = esdc
        self.figure_text = esdc.text
        self.keywords = [k for k in self.figure_text.lower().split(" ") 
                         if not k in stopwords]
    def __str__(self):
        return self.figure_text

class SluInferenceResult:
    def __init__(self, figure_map_node, landmark_map_node, 
                 path_factor, path_cost, landmark_factor, landmark_cost, ggg):

        self.figure_map_node = figure_map_node
        self.landmark_map_node = landmark_map_node

        self.ggg = ggg
        self.path_factor = path_factor
        self.path_cost = path_cost
        self.landmark_factor = landmark_factor
        self.landmark_cost = landmark_cost
        self.cost = self.path_cost + self.landmark_cost
        self.probability = math.exp(-self.cost)

class SluApi:
    def __init__(self):
        model_fname = "../externals/slu/data/directions/direction_training/annotation/models/crf_discrete_sr_1.5.pck"
        #model_fname = "../externals/slu/data/directions/direction_training/annotation/models/crf_continuous_sr_1.5.pck"
        self.cost_function = CostFnSemanticMapping.from_mallet(model_fname, guiMode=True)
        self.task_planner = nodeSearch.BeamSearch(self.cost_function)

        self.landmark_distance_threshold = 15#35 #ignore things too far away 
        
        self.figure_distance_threshold = 35 #ignore things too far away 

    ##Lets do a hack for near 
    def score_particle(self, landmark_label_ind, input_ggg, particle, path_node, 
                       landmark_node, path_factor, landmark_factor):
        """
        Score a particular particle by iterating through all nodes.
        """
        results = []

        #for n in particle.nodelist:
        #   print len(n.bounding_box)

        ## We should make a list of only the supernodes - this is wasting computation creating cost function nodes 
        
        
        valid_supernodes = []
        for n in particle.nodelist:
            if n.is_supernode:                             
                if len(n.bounding_box) == 0:
                    print "Skipping supernode - has no valid bounding box"
                    continue
                valid_supernodes.append(n)
        
        #the paths being evaluated is the same independant of the figure 
        #this can be calculated at the start

        here_map_node = particle.get_current_position_node()    

        particle.build_shortest_distance(here_map_node)

        path_cache = {}

        for n in valid_supernodes:
            path_cache[n] = particle.get_pygraph_path_pts(here_map_node, n)
        
        self.cost_function.nodes = [(n, node_to_physical_object(n))
                                    for n in valid_supernodes] #particle.nodelist]

        
        #goes through all the pairings 
        for i, (figure_map_node, pobj) in enumerate(self.cost_function.nodes):

            print "-------------------- Figure Node ", figure_map_node.id , "[", figure_map_node.parent_supernode ,"] ----------------" 

            if figure_map_node.id <= particle.last_checked_id:
                #print "Figure node check in previous request - skipping"
                continue
            
            for j, (landmark_map_node, landmark_pobj) in enumerate(self.cost_function.nodes):
                if figure_map_node == landmark_map_node:
                    continue

                #skip invalid landmark nodes 
                if(landmark_map_node.label_dist.is_uniform()):
                    #print "\tRegion ", landmark_map_node.parent_supernode, " doesn't have any label info - skipping as landmark"
                    continue

                if(landmark_map_node.label_dist.getprobability_index(landmark_label_ind) < 0.2):
                    #print "\tRegion ", landmark_map_node.parent_supernode, " doesn't have enough likelihood of being a landmark - skipping"
                    continue

                if sf.math2d_dist(here_map_node.xy, figure_map_node.xy) > self.figure_distance_threshold:
                    continue

                if sf.math2d_dist(here_map_node.xy, landmark_map_node.xy) > self.landmark_distance_threshold:
                    continue                
                
                #maybe we should skip if the landmark doesn't have enough label probability - not worth evaluating ??

                ### we should ignore landmarks that don't have labels 

                print "Figure Node : " , figure_map_node.id , " Region : " , figure_map_node.parent_supernode , " Label : " ,  figure_map_node.get_main_label()
                print "Landmark Node : " , landmark_map_node.id , " Region : " , landmark_map_node.parent_supernode, " Label : " ,  landmark_map_node.get_main_label()

                # make a path
                #here_xy = particle.get_current_position_xy()
                #path = Path([0, 1], tp([(here_xy + (0, 0)),
                 #                       (figure_map_node.xy + (0, 0))]))

                #path_points = self.pquery.getPathPython(particle.id, -1, figure_map_node.id)

                #path_points = self.pquery.getPathPython(particle.id, landmark_map_node.id, figure_map_node.id)
                #path_points = [p + (0, 0) for p in path_points]
                #This path is between the current position and the figure node 
                
                #path_points = particle.get_pygraph_path_pts(here_map_node, figure_map_node)
                #particle.get_path_pts(here_map_node.id, figure_map_node.id)

                figure_path_points = path_cache[figure_map_node]

                path_points = [p + (0, 0) for p in figure_path_points]

                if len(path_points) == 0:
                    continue

                #we should also continue if the path points are too long ?? 

                #assuming that the nodes are 1 meter apart 
                #or we can calculate the distance 
                print "Path length : " , len(path_points)

                path = Path.from_xyztheta(na.arange(len(path_points)) * 1000, tp(path_points))
                place = node_to_physical_object(figure_map_node)

                ##Location of the figure
                x, y = figure_map_node.xy
                place = Place(Prism.from_points_xy([(x-0.1, x+0.1, x+0.1, x-0.1), (y-0.1, y-0.1, y+0.1, y+0.1)], place.prism.zStart, place.prism.zEnd))

                #if not path.hash_string.startswith("5d08dce") or not landmark_pobj.hash_string.startswith("ef449517a"):
                #    continue

                ggg = GGG.from_ggg_and_evidence(input_ggg, 
                                                Evidences.copy(input_ggg.evidences))
                ggg.null_costs()
                #print "path node type", path_node.type
                if path_node.is_path:
                    ggg.set_evidence_for_node(path_node, [path])
                else:
                    ggg.set_evidence_for_node(path_node, [place])

                ggg.set_evidence_for_node(landmark_node, [landmark_pobj])
                agent = node_to_physical_object(here_map_node)
                ggg.context = Context.from_groundings([landmark_pobj, agent], agent_id=agent.id)

                self.task_planner.cost(ggg, [])

                #the following are the contributers to the probability of the groundings 
                path_cost = ggg.cost_for_factor(path_factor)

                ### This is wrong - this should be normalized by all valid landmarks 
                landmark_cost = ggg.cost_for_factor(landmark_factor)

                print "Path Prob " , math.exp(-path_cost)
                print "Landmark Prob " , math.exp(-landmark_cost)

                results.append(SluInferenceResult(figure_map_node, 
                                                  landmark_map_node,
                                                  path_factor,
                                                  path_cost, 
                                                  landmark_factor, 
                                                  landmark_cost, ggg))
            #if i > 2:
            #    break
        results.sort(key=lambda x: x.cost)

        #print "Minimum Spanning Tree" 
        #print particle.spanning_tree

        return results
                   

    def parse_language(self, text):
        print "Language" , text

        v = 1

        f, landmark_phrase = text.split("is")
        landmark_words = landmark_phrase.split(" ")
        for i, w in enumerate(landmark_words):
            if w == "the":
                break
        sr = " ".join(landmark_words[0:i]).strip()
        landmark = " ".join(landmark_words[i:])

        if sr in ("down", "toward", "through"):
            sdc_type = "PATH"
        else:
            sdc_type = "PLACE"
        esdcs = esdcIo.parse("""
- '%s.'
- - EVENT:
      f: %s
      r:  is
      l:
         %s:
            r: %s
            l: %s
""" % (text, f, sdc_type, sr, landmark))
        print "sr", sr, "type", sdc_type        
        #extractor = stanfordParserExtractor.Extractor()
        #esdcs = extractor.extractEsdcs(text)
        #assert esdcs.entireText == text, (esdcs.entireText, text)

        if(v):
            print [str(e) for e in esdcs]

        return sr, f , landmark

    def score_particles(self, text, landmark_ind, particles):
        
        v = 1
        """
        For each particle, return a list of nodes that match the text.
        """
        f, landmark_phrase = text.split("is")
        landmark_words = landmark_phrase.split(" ")
        for i, w in enumerate(landmark_words):
            if w == "the":
                break
        sr = " ".join(landmark_words[0:i]).strip()
        landmark = " ".join(landmark_words[i:])

        if sr in ("down", "toward", "through"):
            sdc_type = "PATH"
        else:
            sdc_type = "PLACE"
        esdcs = esdcIo.parse("""
- '%s.'
- - EVENT:
      f: %s
      r:  is
      l:
         %s:
            r: %s
            l: %s
""" % (text, f, sdc_type, sr, landmark))
        print "sr", sr, "type", sdc_type        
        #extractor = stanfordParserExtractor.Extractor()
        #esdcs = extractor.extractEsdcs(text)
        #assert esdcs.entireText == text, (esdcs.entireText, text)

        if(v):
            print [str(e) for e in esdcs]

        figure_esdc = esdcs[0].f[0]
        path_esdc = esdcs[0].l[0]
        path_landmark_esdc = path_esdc.l[0]
        
        if(True):
            print "object", figure_esdc
            print "path", path_esdc
            print "landmark", path_landmark_esdc

        ggg = gggs_from_esdc_group([path_esdc])[0]

        path_node = ggg.top_event_node
        landmark_node = ggg.node_for_esdc(path_landmark_esdc)
        if(v):
            print path_landmark_esdc
        path_factor = ggg.esdc_to_factor(path_esdc)
        landmark_factor = ggg.esdc_to_factor(path_landmark_esdc)
        if(v):
            print "landmark", landmark_factor
        for phi_node in ggg.phi_nodes:
            ggg.set_evidence_for_node(phi_node, True)
        if(v):
            print "text", text

        results = []
        for p in particles: 
            results.append((p, self.score_particle(landmark_ind, ggg, p, path_node, 
                                                   landmark_node, 
                                                   path_factor, 
                                                   landmark_factor)))

        return FigureText(figure_esdc), results
