#EKF SLAM: prediction step

from numpy import *
from numpy.linalg import *


'''
Takes the current state of pose data/map/other stored data and the input odometry and calculates the predictions for the pose/map data and covariance matrix.
'''
def ekf_prediction_step(u_t,x,P,t,Q):

   # u_t : input velocities (x,y,theta)
   # x   : robot position/landmark positions
   # P   : covariance matrix of x
   # t   : length of timestep
   # Q   : covariance matrix of noise
   
   #given u_t, x calculate new updated robot position
   c_p = (x[0],x[1],x[2]) #current robot position given the current map in (x,y,theta) coordinates
   updated_robot_position = [] #updated robot position given input u_t, relative to main map
   c = math.cos(c_p[2])
   s = math.sin(c_p[2])
   updated_robot_position.append(u_t[0] * c * t - u_t[1] * s * t + c_p[0]) #updated x position
   updated_robot_position.append(u_t[0] * s * t + u_t[1] * c * t + c_p[1]) #updated y position
   updated_robot_position.append(u_t[2] * t + c_p[2]) #updated theta position
   
   #calculate Jacobians
   F = eye(len(x)) #jacobian of update function with respect to x
   F[0, 2] = -t * (u_t[0] * s + u_t[1] * c)
   F[1, 2] = t * (u_t[0] * c - u_t[1] * s)
   W = zeros((len(x),3),float)#jacobian of update function with respect to noise
   W[0,0] = t
   W[1,1] = t
   W[2,2] = t
   
   #calculate updated covariance matrix
   P_t = dot(dot(F,P),F.T) + dot(dot(W,Q),W.T) #F * P * F^T + W * Q * W^T
   #print "P_t", P_t
   #print dot(dot(W,Q),W.T)
   #print W
   #print Q
   
   '''print "EKF PREDICTION STEP"
   print "F", F
   print dot(dot(F,P),F.T)
   print "W", W
   print dot(dot(W,Q),W.T)
   print "P", P
   print "P_t", P_t'''
      
   #reset variables
   x[0] = updated_robot_position[0]
   x[1] = updated_robot_position[1]
   x[2] = updated_robot_position[2]
   
   P = P_t
   
   #print x
   
   return (x,P)
