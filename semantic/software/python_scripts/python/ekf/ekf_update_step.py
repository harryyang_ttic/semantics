#EKF SLAM: measurement update step

from numpy import *
from numpy.linalg import *

'''
Takes the landmark-associated data, predicted pose/map and covariance matrix, and performs an EKF measurement update.
'''
def ekf_update_step(d_t,z_t,x,P,R):

   # d_t : data associated landmarks
   # z_t : actual positions of landmarks
   # x   : robot position/landmark positions
   # P   : covariance matrix of x
   # t   : length of timestep
   # R   : covariance matrix of measurement noise
   
   if len(x) == 3 or len(d_t)-len(x) == len(z_t):
      return (x,P)

   c = math.cos(x[2])
   s = math.sin(x[2])
   xr = x[0]
   yr = x[1]
   for i in range((len(x)-3)/2):
      idx = 3 + 2*i
      if d_t[idx] == 0. and d_t[idx+1] == 0.:
         continue

      xm = x[idx]
      ym = x[idx + 1]
      
      H = zeros((2, len(x)))
      # Jacobians wrt robot (x,y,theta)
      H[0, 0] = -c
      H[0, 1] = -s
      H[0, 2] = -1*(xm - xr)*s + (ym - yr)*c
      H[1, 0] = s
      H[1, 1] = -c
      H[1, 2] = -1*(xm - xr)*c - (ym - yr)*s

      # Jacobians wrt map (x,y)
      H[0, idx] = c
      H[0, idx+1] = s
      H[1, idx] = -s
      H[1, idx+1] = c

      zx_pred =  (xm - xr)*c + (ym - yr)*s
      zy_pred = -(xm - xr)*s + (ym - yr)*c

      innov = array([d_t[idx] - zx_pred, d_t[idx+1] - zy_pred])
      #print "innov: ", innov
      
      S = dot (dot(H,P), H.transpose()) + R
      K = dot (dot(P, H.transpose()), linalg.inv(S))
      x_new = x + dot (K, innov)
      P_new = dot (eye(len(x)) - dot(K,H), P)
      
      x = x_new
      P = P_new

   return (x,P)
   
