#import cPickle
import pickle
from slam.language_label_t import language_label_t
from language_observation import slu_api

import basewindow 
from g3.cost_functions.gui import crfFeatureWeights
from semantic_mapping.semantic_mapping_state import particle_to_context

def main():
    print "particles"
    particles, channel, data = pickle.load(open("pck/label_handler_args_extended_language_down_hall.pck"))
    app = basewindow.makeApp()
    slu = slu_api.SluApi()

    language_label = language_label_t.decode(data)
    text = str(language_label.update)

    figure_text, results = slu.score_particles(text, particles[0:1])
    #print results

    featureBrowser = crfFeatureWeights.MainWindow()
    featureBrowser.show()
    r = results[0][1][0]
    #featureBrowser.load(particle_to_context(particles[0]), slu.cost_function, r.path_factor)
    featureBrowser.load(particle_to_context(particles[0]), slu.cost_function, r.landmark_factor)

    print "exec"
    app.exec_()



if __name__ == "__main__":
    main()


