'''##!/usr/bin/python
#EKF SLAM (online)'''

import sys
import threading
import math

from lcm import LCM
from lcm import EventLog
from erlcm.xyz_point_list_t import xyz_point_list_t
from erlcm.xyz_point_t import xyz_point_t
from bot_core.pose_t import pose_t
from common.quaternion import Quaternion

class Node():
   def __init__(self,pose_data=[0,0,0],labels=[],lidar=None):
      self.pose = pose_data
      self.lang_labels = labels
      self.lidar_data = lidar
      
   def setpose(self,pose_data):
      self.pose = pose_data
   
   def setlabels(self,labels):
      self.lang_labels = labels
   
   def setlidar(self,lidar):
      self.lidar_data=lidar
      
   def getpose(self):
      return self.pose
      
   def getlabels(self):
      return self.lang_labels
      
   def getlidar(self):
      return self.lidar_data
      

class lcm_listener():
   def __init__(self):
      self._running = True
      self.lc = LCM()
      self.pose_sub = self.lc.subscribe("POSE", self.pose_handler)
      self.pose = None
      self.time = None
      self.nodes = []
      self.discrete_distance = 1
      
   def run(self):
      print "Started LCM listener"
      #print "test"
      
      while self._running:
         try:
            self.lc.handle()
         except KeyboardInterrupt:
            print "\n============== Interrupt =================\n"
            self._running = False
            pass
            
   def pose_handler(self, channel, data):
      decoded_pose = pose_t.decode(data)
      self.time = decoded_pose.utime
      self.pose = [decoded_pose.pos[0],decoded_pose.pos[1],decoded_pose.orientation]
      self.have_pose = True
      if len(self.nodes) == 0:
         self.nodes.append(Node(pose_data=self.pose))
      else:
         prev = self.nodes[len(self.nodes)-1].getpose()
         if math.sqrt((prev[0]-self.pose[0])**2 + (prev[1]-self.pose[1])**2) >= self.discrete_distance:
            self.nodes.append(Node(pose_data=self.pose))
      self.post_message()
         
   def post_message(self):
      print "posted message"
      #export lcm message. Channel: TOPOLOGICAL_GRAPH_XYZ_LIST; Message type: erlcm_xyz_point_list_t
      msg = xyz_point_list_t()
      msg.utime = self.time
      msg.no_points = len(self.nodes)
      ptlist = []
      for i in range(len(self.nodes)):
         xyzpt = xyz_point_t()
         a = self.nodes[i].getpose()
         xyzpt.xyz = [a[0],a[1],0]
         ptlist.append(xyzpt)      
      msg.points = ptlist
      self.lc.publish("TOPOLOGICAL_GRAPH_XYZ_LIST",msg.encode())
   
         
      
if __name__ == "__main__":
   background = lcm_listener()
   bg_thread = threading.Thread( target=background.run )
   bg_thread.start()
