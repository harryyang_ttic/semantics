#!/usr/bin/python
#
# extracts estimated pose of detected cylinders and
# converts them to (x,y) observations in the body frame.

import sys

from lcm import EventLog
from erlcm_cylinder_list_t import cylinder_list_t
from bot_core.pose_t import pose_t
from common.quaternion import Quaternion

fname = sys.argv[1]
log = EventLog(fname, "r")

print >> sys.stderr, "opened %s" % fname

have_pose = 0

for e in log:
    if e.channel == "POSE":
        pos = []
        quat = []
        pose = pose_t.decode (e.data)
        have_pose = 1
    # CYLINDER_LIST denotes Velodyne detections and may be in local frame
    if e.channel == "CYLINDER_LIST":
        if have_pose != 1:
            continue
        cylinder_list = cylinder_list_t.decode (e.data)
        for cylinder in cylinder_list.list:
            # Assuming that z = 0 in the local frame (valid if axis is vertical)
            pos_temp = [cylinder.line_point[0] - pose.pos[0], cylinder.line_point[1] - pose.pos[1], 0]
            quat = Quaternion (pose.orientation)
            pos_body = quat.rotate_rev (pos_temp)
            #print "%d %f %f %f" % (cylinder_list.utime, cylinder.line_point[0], cylinder.line_point[1], cylinder.line_point[2])
            print "%d %f %f 0.0" % (cylinder_list.utime, pos_body[0], pos_body[1])

    if e.channel == "LIDAR_CYLINDER_LIST":
        cylinder_list = cylinder_list_t.decode (e.data)
        for cylinder in cylinder_list.list:
            print "%d %f %f %f" % (cylinder_list.utime, cylinder.line_point[0], cylinder.line_point[1], cylinder.line_point[2])
       
