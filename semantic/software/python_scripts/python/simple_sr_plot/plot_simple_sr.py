#import matplotlib.pyplot as plt
import pylab as mpl
import math 
import numpy as na

#pose = [x,y,t]

#POSE1 - POSE2
def minus(pose1, pose2):
    delta = [0,0,0]
    dx = pose1[0] - pose2[0]
    dy = pose1[1] - pose2[1]
    c = math.cos(pose2[2])
    s = math.sin(pose2[2])
    delta[0] = dx * c + dy * s
    delta[1] = -dx * s + dy * c
    delta[2] = pose1[2] - pose2[2]
    return delta

def get_prob(pose1, sr, pose2):
    if sr == 'near':
        near_threshold = 10.0
        dx = math.hypot(pose1[0] - pose2[0], pose1[1] - pose2[1])
        dist = near_threshold - dx; 
        sigma = 1.0
        prob = 1/ (1 + math.exp(-dist/sigma)); 
        return prob

    elif sr == 'left':
        delta = minus(pose1, pose2)
        threshold = 5.0
        t_pose = [0,10,0]
        rel_pose = minus(t_pose, delta)
        #print delta
        #print rel_pose
        dist = threshold - math.hypot(rel_pose[0], rel_pose[1]) 
        sigma = 1.0
        prob = 1/ (1 + math.exp(-dist/sigma)); 
        #print prob
        return prob

    elif sr == 'right':
        delta = minus(pose1, pose2)
        threshold = 10.0
        t_pose = [0,-10,0]
        rel_pose = minus(t_pose, delta)
        #print delta
        #print rel_pose
        dist = threshold - math.hypot(rel_pose[0], rel_pose[1]) 
        sigma = 5.0
        prob = 1/ (1 + math.exp(-dist/sigma)); 
        #print prob
        return prob
    elif sr == 'infront':
        delta = minus(pose1, pose2)
        threshold = 5.0
        t_pose = [10,0,0]
        rel_pose = minus(t_pose, delta)
        #print delta
        #print rel_pose
        dist = threshold - math.hypot(rel_pose[0], rel_pose[1]) 
        sigma = 1.0
        prob = 1/ (1 + math.exp(-dist/sigma)); 
        #print prob
        return prob
    elif sr == 'behind':
        delta = minus(pose1, pose2)
        threshold = 5.0
        t_pose = [-10,0,0]
        rel_pose = minus(t_pose, delta)
        #print delta
        #print rel_pose
        dist = threshold - math.hypot(rel_pose[0], rel_pose[1]) 
        sigma = 1.0
        prob = 1/ (1 + math.exp(-dist/sigma)); 
        #print prob
        return prob
    else:
        return 1.0

def main():
    pose2 = [0,0,0]
    
    dx = 0.1
    dy = 0.1
    
    size = 20
    
    values_x = int(size / dx)
    values_y = int(size / dy)

    results = []

    p_x = []
    p_y = []

    probs = na.empty((int(values_x * 2), int(values_y*2)))
    probs[:] = na.nan

    SR = 'right'

    pose1 = [0,-10,0]
    p = get_prob(pose1, SR, pose2)
    
    for i in range(2*values_x):
        for j in range(2*values_y):
            pose1 = [(i - values_x) * dx, (j-values_y)* dy, 0]
            #p_x.append((i - values_x) * dx)
            #p_y.append((j-values_y)* dy)
            #prob = 
            #print i,j
            #draw function seems to be flipped 
            probs[j,i] = get_prob(pose1, SR, pose2)
            #results.append(prob)

    #plt.scatter(p_x, p_y, c=results)
    #plt.show()
    figure = mpl.figure()
    axes = figure.gca()

    print probs

    axes.imshow(probs, origin="lower", extent=(-size * dx, size * dx, -size * dy, size * dy),
                       cmap=mpl.cm.winter, interpolation='nearest')

    #drawUtils.drawGrounding(axes, agent)
    #drawUtils.drawGrounding(axes, landmark)

    #if title:
    #    figure.suptitle( title )

    #axes.axis([xmin, xmax, ymin, ymax])
    figure.canvas.draw()
    mpl.show()
if __name__ == "__main__":
   main()
