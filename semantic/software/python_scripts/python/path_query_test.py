import path_query
from time import sleep
from PyQt4.QtCore import SIGNAL, QSocketNotifier
import basewindow 
pquery = None

def lcm_activated(i):
    global pquery
    particle_id = 1
    node_start_id = 1
    node_goal_id = 10

    ret = pquery.getPathPython(particle_id, node_start_id, node_goal_id)
    print ret
    
      
def main():
    global pquery
    pquery = path_query.pathQuery(False);
    app = basewindow.makeApp()
    
    socket_notifier = QSocketNotifier(pquery.lcm_fileno,
                                      QSocketNotifier.Read)
    
    
    app.connect(socket_notifier,
                SIGNAL("activated(int)"),
                lcm_activated)
    
    app.exec_()

        
        # ret = pquery.getPathPython(particle_id, node_start_id, node_goal_id)
        # print ret
        # sleep(1)
    




if __name__ == "__main__":
    main()
