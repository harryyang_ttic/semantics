# Node class for topological graph SLAM
class LabelProbability():
    def __init__(self, dist=None):
        self.prob = {}
        self.max_label = -1
        self.max_prob = 0;
        
        if(dist != None):
            for i in dist.classes: 
                self.prob[i.type] = i.probability
                if(i.probability > self.max_prob):
                    self.max_prob = i.probability
                    self.max_label = i.type

    def best_label(self):
        return self.max_label

    def getdictionary(self):
        return {}

    def getprobability(self, s):
        #replace with functioning versions - although not sure if we need to use this or the landmark prob should be handled in the C++ module 
        return 0.9
        '''for i in range(len(self.labels)):
         if self.labels[i] == s:
            return self.observation_frequency[i] /self.total_obs
            return 0'''

class Region():
   def __init__(self, node=None):
       if(node != None):
           self.id = node.node_id
           self.xy = node.xy
           self.heading = node.heading
           self.label_dist = LabelProbability(node.region_label_dist)
      #self.bounding_box = [(1,1),(-1,1),(1,-1),(-1,-1)]
           self.bounding_box = []
           for i in range(node.no_points):
               self.bounding_box.append( (node.x_coords[i], node.y_coords[i]))

   #this node only has id, xy theta 
   def set_region_from_node(self, n_id, node_pose):
       self.id = n_id
       self.xy = node_pose['xy']
       self.heading = node_pose['theta']
       
       self.label_dist = LabelProbability()
       
       self.bounding_box = [(1,1),(-1,1),(1,-1),(-1,-1)]
           
