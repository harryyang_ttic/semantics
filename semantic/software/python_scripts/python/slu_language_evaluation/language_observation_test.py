import unittest
import cPickle

from slam.lang_label_t import lang_label_t
import slu_api


class TestCase(unittest.TestCase):
    def runTest(self):
        pass

    def testSluCommand(self):
        particles, channel, data = cPickle.load(open("pck/label_handler_args_extended_language_2.pck"))
        lang_label = lang_label_t.decode(data)
        text = str(lang_label.update)
        slu = slu_api.SluApi()
        figure_text, results = slu.score_particles(text, particles)
        print "figure text", figure_text
        for particle, result_list in results:
            for result in result_list:
                print "******", figure_text.keywords
                print result.figure_map_node
                print result.landmark_map_node
                print result.probability

        self.fail()
