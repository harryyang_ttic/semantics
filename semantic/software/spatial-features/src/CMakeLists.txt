add_definitions(-fpermissive -c -pipe -Wall -W -DQT_SHARED -DQT_NO_DEBUG -DQT_THREAD_SUPPORT)

# Create a shared library libhello.so with a single source file
add_library(slu-spatial-features SHARED
  avs.cpp  gsl_utilities.cpp  math2d.cpp  math3d.cpp  nearest_neighbor.cpp  spatial_feature_extractor.cpp  spatial_features.cpp)

# make the header public
# install it to include/hello
pods_install_headers(avs.h  gsl_utilities.h  math2d.h  math3d.h  named_enum.h  nearest_neighbor.h  spatial_feature_extractor.h  spatial_features.h DESTINATION slu_spatial_features)

# make the library public
pods_install_libraries(slu-spatial-features)

target_link_libraries(slu-spatial-features
  -lgsl -lgslcblas 
)

# uncomment these lines to link against another library via pkg-config
set(REQUIRED_PACKAGES gsl)
#pods_use_pkg_config_packages(hello ${REQUIRED_PACKAGES})

# create a pkg-config file for the library, to make it easier for other
# software to use.
pods_install_pkg_config_file(slu-spatial-features
    CFLAGS
    LIBS -lslu-spatial-features
    REQUIRES ${REQUIRED_PACKAGES}
    VERSION 0.0.1)
