#ifndef __lcmtypes_er_lcmtypes_hpp__
#define __lcmtypes_er_lcmtypes_hpp__

#include "erlcm/place_classification_class_t.hpp"
#include "erlcm/gridmap_t.hpp"
#include "erlcm/cylinder_model_t.hpp"
#include "erlcm/place_classification_t.hpp"
#include "erlcm/raw_odometry_msg_t.hpp"
#include "erlcm/pose_list_t.hpp"
#include "erlcm/gridmap_config_t.hpp"
#include "erlcm/slam_pose_transform_list_t.hpp"
#include "erlcm/slam_full_graph_node_t.hpp"
#include "erlcm/people_pos_msg_t.hpp"
#include "erlcm/rigid_transform_list_t.hpp"
#include "erlcm/person_status_t.hpp"
#include "erlcm/multi_gridmap_t.hpp"
#include "erlcm/place_classification_debug_t.hpp"
#include "erlcm/slam_full_graph_t.hpp"
#include "erlcm/laser_feature_t.hpp"
#include "erlcm/position_t.hpp"
#include "erlcm/cylinder_list_t.hpp"
#include "erlcm/localize_reinitialize_cmd_t.hpp"
#include "erlcm/rigid_transform_t.hpp"
#include "erlcm/slam_node_init_t.hpp"
#include "erlcm/floor_gridmap_t.hpp"
#include "erlcm/slam_pose_transform_t.hpp"
#include "erlcm/annotation_t.hpp"

#endif
