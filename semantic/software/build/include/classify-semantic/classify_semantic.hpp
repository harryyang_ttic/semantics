#ifndef _LIBSEMANTIC_CLASSIFY_H
#define _LIBSEMANTIC_CLASSIFY_H

//#ifdef __cplusplus
//extern "C" {
//#endif

#include <svm/svm.h>
#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <lcm/lcm.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include <glib.h>
#include <bot_core/bot_core.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>
#include <lcmtypes/perception_lcmtypes.h>
#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>
#include <laser_features/laser_feature.h>
#include <full_laser/full_laser.h>
#include <lcmtypes/perception_place_classification_class_t.h>
#include <image-features/image_feature_extractor.hpp>
#include <CSystem.h>
#include <CImage.h>
#include <CCrfh.h>
#include <QString>

#define ANNOTATION_BIN_SIZE 100
#define THRESHOLD 0.4

#define IMAGE_BUFFER_SIZE 100
#define LASER_BUFFER_SIZE 400

//#define v

typedef struct svm_model svm_model_t;
typedef struct svm_node svm_node_t;

/*typedef enum {
    CLASS_DOORWAY= PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_DOORWAY, 
    CLASS_CONFERENCE_ROOM = PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_CONFERENCE_ROOM, 
    CLASS_OFFICE= PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_OFFICE, 
    CLASS_LAB= PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_LAB, 
    CLASS_OPEN_AREA= PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_OPEN_AREA, 
    CLASS_HALLWAY= PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_HALLWAY, 
    CLASS_ELEVATOR= PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_ELEVATOR, 
    CLASS_CORRIDOR= PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_CORRIDOR,  //we should throw out some these redundant classes
    CLASS_LARGE_MEETING_ROOM= PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_LARGE_MEETING_ROOM, 
    CLASS_LARGE_CLASSROOM= PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_CLASSROOM 
    } class_type_t;*/
const int CLASS_DOORWAY= PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_DOORWAY;
const int CLASS_CONFERENCE_ROOM = PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_CONFERENCE_ROOM;
const int CLASS_OFFICE= PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_OFFICE;
const int CLASS_LAB= PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_LAB;
const int CLASS_OPEN_AREA= PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_OPEN_AREA;
const int CLASS_HALLWAY= PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_HALLWAY;
const int CLASS_ELEVATOR= PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_ELEVATOR; 
const int CLASS_CORRIDOR= PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_CORRIDOR;  //we should throw out some these redundant classes
const int CLASS_LARGE_MEETING_ROOM= PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_LARGE_MEETING_ROOM; 
const int CLASS_LARGE_CLASSROOM= PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_CLASSROOM; 

typedef struct 
{
    //class_type_t type;
    int type;
    double prob;
} semantic_class_t;

typedef struct
{
    int count;
    semantic_class_t *classes;
} classification_result_t;

typedef struct
{
    svm_model_t *model;
    image_feature_extractor_t *extractor;
    int no_class; 
    int *class_labels;
    int **class_counts;
    int64_t last_processed;
    bool publish;
} image_classification_model_t;

typedef struct
{
    svm_model_t *model;
    feature_config_t config;
    int no_class; 
    int *class_labels;
    int **class_counts;
    int64_t last_processed;
} laser_classification_model_t;

typedef struct
{
    lcm_t *lcm;

    image_classification_model_t img_classifier;
    laser_classification_model_t laser_classifier;

    full_laser_state_t *full_laser;
    GMutex *mutex; 

    BotPtrCircular *laser_history;
    BotPtrCircular *image_history;
    
    bool publish;

} semantic_classifier_t;

semantic_classifier_t *semantic_classifier_create(char *laser_model_path, char *image_model_path, bool publish=true, bool handle_messages = false);

semantic_classifier_t *semantic_classifier_create(bool publish=true, bool handle_messages=false);

void publish_classification(semantic_classifier_t *s, int64_t utime);

classification_result_t *get_classification_laser(semantic_classifier_t *s, bot_core_planar_lidar_t *laser);

classification_result_t *get_classification_image(semantic_classifier_t *s, bot_core_image_t *image);

void get_classification(semantic_classifier_t *s, bot_core_planar_lidar_t *laser, bot_core_image_t *image, classification_result_t &laser_result, classification_result_t &image_result);

void publish_classification(semantic_classifier_t *s, bot_core_planar_lidar_t *laser, bot_core_image_t *image);

void publish_classification_latest(semantic_classifier_t *s);

void classification_result_t_destroy(classification_result_t *p);

classification_result_t *classification_result_t_copy(classification_result_t *p);

void print_classification_result_t(classification_result_t *p);

char* get_class_name_from_id(int class_id);
#endif
