#ifndef ER_RENDERERS_H_
#define ER_RENDERERS_H_
#include <bot_vis/bot_vis.h>
#include <bot_frames/bot_frames.h>

#ifdef __cplusplus
extern "C" {
#endif

    void
    setup_renderer_advanced_topological_graph (BotViewer *viewer, int priority, BotParam * param);

    /*void
    setup_renderer_region_graph (BotViewer *viewer, int priority, BotParam * param);
    */
    void 
    add_wheelchair_model_renderer_to_viewer(BotViewer *viewer, int render_priority, 
                                            BotParam * param, BotFrames * frames);

    void add_person_model_renderer_to_viewer(BotViewer *viewer, int render_priority, BotParam * param, BotFrames * frames, lcm_t *_lcm);

    void setup_renderer_graph_annotation (BotViewer *viewer, int priority, BotParam * param);

    //
    //void quad_waypoint_add_renderer_to_viewer(BotViewer *viewer, int render_priority, lcm_t * lcm);
    //    void add_quad_model_renderer_to_viewer(BotViewer *viewer, int render_priority, BotParam * param, BotFrames * frames);

    //void setup_renderer_occupancy_map (BotViewer *viewer, int priority, BotParam * param);
    
    //void grid_add_renderer_to_viewer(BotViewer *viewer, int render_priority);

    //void add_wheelchair_model_renderer_to_viewer(BotViewer *viewer, int render_priority, BotParam * param, BotFrames * frames);
    //void setup_renderer_gridmap (BotViewer *viewer, int priority, lcm_t *_lcm, BotParam * param);
    //void add_person_model_renderer_to_viewer(BotViewer *viewer, int render_priority, BotParam * param, BotFrames * frames, lcm_t *lcm);
    //void setup_renderer_host_status (BotViewer *viewer, int priority);
    //void renderer_sensor_status_new (BotViewer *viewer);
    //void setup_renderer_robot_status (BotViewer *viewer, int priority);
    //void setup_renderer_robot_commands (BotViewer *viewer, int priority);

#ifdef __cplusplus
}
#endif

#endif
