#ifndef ER_RENDERERS_H_
#define ER_RENDERERS_H_
#include <bot_vis/bot_vis.h>
#include <bot_frames/bot_frames.h>

#ifdef __cplusplus
extern "C" {
#endif
    void
    setup_renderer_region_graph (BotViewer *viewer, int priority, BotParam * param);

#ifdef __cplusplus
}
#endif

#endif
