'''##!/usr/bin/python
#EKF SLAM (online)'''

from ekf_prediction_step import ekf_prediction_step
from ekf_update_step import ekf_update_step
from data_association import data_association
from add_new_features import add_new_features
import matplotlib.pyplot as plt
import matplotlib
from numpy import *
from numpy.linalg import *
import time
import sys
import threading

from lcm import LCM
from lcm import EventLog
from erlcm.raw_odometry_msg_t import raw_odometry_msg_t
from erlcm.cylinder_list_t import cylinder_list_t
from erlcm.xyz_point_list_t import xyz_point_list_t
from erlcm.xyz_point_t import xyz_point_t
from slam.graph_particle_list_t import graph_particle_list_t
from slam.graph_particle_t import graph_particle_t
from slam.graph_node_t import graph_node_t
from slam.graph_node_list_t import graph_node_list_t
from slam.graph_edge_t import graph_edge_t
from slam.graph_node_t import graph_node_t
from slam.init_node_t import init_node_t
from bot_core.pose_t import pose_t
from common.quaternion import Quaternion

class lcm_listener():
   def __init__(self):
      self._running = True
      self.lc = LCM()
      self.odom_sub = self.lc.subscribe("ODOMETRY",self.odom_handler)
      self.meas_sub1 =self.lc.subscribe("CYLINDER_LIST", self.cylinder_handler)
      self.meas_sub2 =self.lc.subscribe("LIDAR_CYLINDER_LIST", self.lidar_handler)
      self.pose_sub = self.lc.subscribe("POSE", self.pose_handler)
      self.x = array([0.,0.,0.]) #pose data + map
      self.P = P = array([[0.,0.,0.],[0.,0.,0.],[0.,0.,0.]]) #initial covariance
      self.Q = [[1.,0,0],[0,1.,0],[0,0,2.5]] #covariance matrix of noise in motion
      self.R = array([[5.,0.],[0.,5.0]]) #covariance matrix of noise in measurement
      self.time = 0
      self.moved = False
      self.pose = None
      self.have_pose = False
      self.lock = 0
      self.xpath = []
      
   def run(self):
      print "Started LCM listener"
      #print "test"
      
      while self._running:
         try:
            self.lc.handle()
         except KeyboardInterrupt:
            print "\n============== Interrupt =================\n"
            self._running = False
            pass
            
   def pose_handler(self, channel, data):
      self.pose = pose_t.decode(data)
      self.have_pose = True

   def odom_handler(self, channel, data):
      print "odometry handler"
      if self.lock == 0:
         self.lock = 1
         odom = raw_odometry_msg_t.decode (data)
         u_t = [odom.tv, 0.0, odom.rv]
         t = odom.utime - self.time
         self.time = odom.utime
         t = t * 1.0 / 10.0**6
         if not u_t[0] == 0 or self.moved == True:
            (self.x,self.P) = ekf_prediction_step(u_t,self.x,self.P,t,self.Q)
            self.moved = True
            self.post_message(self.x,self.P)
            self.xpath.append([-1*self.x[0],-1*self.x[1],0])
            #print "xpos", [self.x[0],self.x[1],0]
         self.lock = 0
      else:
         print "MISSED odometry"
      
   def cylinder_handler(self, channel, data):
      print "cylinder handler"
      if self.lock == 0:
         self.lock = 1
         z_t = []
         if not self.have_pose:
           pass
         else:
            cylinder_list = cylinder_list_t.decode (data)
            for cylinder in cylinder_list.list:
               # Assuming that z = 0 in the local frame (valid if axis is vertical)
               pos_temp = [cylinder.line_point[0] - self.pose.pos[0], cylinder.line_point[1] - self.pose.pos[1], 0]
               quat = Quaternion (self.pose.orientation)
               pos_body = quat.rotate_rev (pos_temp)
               z_t.append(pos_body[0])
               z_t.append(pos_body[1])
            if self.moved:
               d_t = data_association(z_t,self.x,self.P)
               (self.x,self.P) = ekf_update_step(d_t,z_t,self.x,self.P,self.R)
               (self.x,self.P) = add_new_features(d_t,z_t,self.x,self.P,self.R)
               self.post_message(self.x,self.P)
         self.lock = 0
      else:
         print "MISSED cylinder"        
   
   def lidar_handler(self, channel, data):
      print "lidar handler"
      if self.lock == 0:
         self.lock = 1
         z_t = []
         cylinder_list = cylinder_list_t.decode (data)
         for cylinder in cylinder_list.list:
            z_t.append(cylinder.line_point[0])
            z_t.append(cylinder.line_point[1])
      
         if self.moved:
            d_t = data_association(z_t,self.x,self.P)
            (self.x,self.P) = ekf_update_step(d_t,z_t,self.x,self.P,self.R)
            (self.x,self.P) = add_new_features(d_t,z_t,self.x,self.P,self.R)
            self.post_message(self.x,self.P)
         self.lock = 0
      else:
         print "MISSED lidar"
         
   def post_message(self,x,P):
      print "posted message"
      msg = graph_particle_list_t()
      msg.no_particles = 1
      msg.utime = self.time
      parlist = []
      par = graph_particle_t()
      par.id = 0
      par.no_nodes = 1
      nodelist = []
      newnode = graph_node_t()
      newnode.id = 0
      newnode.utime = 0
      newnode.xy = [x[0],x[1]]
      newnode.heading = x[2]
      newnode.pofz = .5
      newnode.cov = [P[0][0],P[0][1],P[0][2],P[1][0],P[1][1],P[1][2],P[2][0],P[2][1],P[2][2]]
      newnode.is_supernode = 1
      nodelist.append(newnode)
      par.node_list = nodelist
      par.no_edges = 0
      par.edge_list = []
      par.weight = .5
      par.weight_multiplier = 0
      parlist.append(par)
      msg.particle_list.append(par)
      self.lc.publish("PARTICLE_RESULT",msg.encode())

      
      
      
      
      '''#export lcm message. Channel: POST_XYZ_LIST; Message type: erlcm_xyz_point_list_t
      msg = xyz_point_list_t()
      msg.utime = self.time
      msg.no_points = len(self.xpath)+(len(x)-3)/2
      ptlist = []
      for i in range (len(self.xpath)+((len(x)-3)/2)):
         xyzpt = xyz_point_t()
         if i < len(self.xpath):
            xyzpt.xyz = self.xpath[i]
         else:
            xyzpt.xyz = [-1*x[3+2*(i - len(self.xpath))], -1*x[4+2*(i - len(self.xpath))], 0]
         ptlist.append(xyzpt)
         
         #xyzpt.xyz = [x[3+2*i], x[3+2*i+1],0]
         #xyzpt.xyz = [x[0],x[1],0]       
      msg.points = ptlist
      self.lc.publish("POST_XYZ_LIST",msg.encode())'''
   
         
      
if __name__ == "__main__":
   background = lcm_listener()
   bg_thread = threading.Thread( target=background.run )
   bg_thread.start()

'''

def add_new_features(d_t,z_t,x,P,R):
   
   # d_t : data associated landmarks
   # z_t : actual positions of landmarks
   # x   : robot position/landmark positions
   # P   : covariance matrix of x
   # R   : covariance matrix of measurement noise
   
   
   #compute n - number of new landmarks
   n = (len(d_t) - len(x)) / 2

   # incrementally add new features
   s = math.sin(x[2])
   c = math.cos(x[2])
   l = len(x)
   for i in range(n):
      idx = l + 2*i
      xm_r = d_t[idx]
      ym_r = d_t[idx+1]

      F1 = [[1, 0, -xm_r*s - ym_r*c], [0, 1, xm_r*c - ym_r*s]]
      F = concatenate((F1,zeros((2,len(x)-3))), 1)

      xm_w = x[0] + xm_r*c - ym_r*s
      ym_w = x[1] + xm_r*s + ym_r*c
      x = append(x,xm_w)
      x = append(x,ym_w)

      FP = dot(F,P)
      P1 = concatenate((P,FP.transpose()),1)
      P2 = concatenate((FP, dot(FP,F.transpose()) + R), 1)

      P = concatenate((P1,P2))

   
   return (x,P)
   
   
   
def ekf_prediction_step(u_t,x,P,t,Q):

   # u_t : input velocities (x,y,theta)
   # x   : robot position/landmark positions
   # P   : covariance matrix of x
   # t   : length of timestep
   # Q   : covariance matrix of noise
   
   #given u_t, x calculate new updated robot position
   c_p = (x[0],x[1],x[2]) #current robot position given the current map in (x,y,theta) coordinates
   updated_robot_position = [] #updated robot position given input u_t, relative to main map
   c = math.cos(c_p[2])
   s = math.sin(c_p[2])
   updated_robot_position.append(u_t[0] * c * t - u_t[1] * s * t + c_p[0]) #updated x position
   updated_robot_position.append(u_t[0] * s * t + u_t[1] * c * t + c_p[1]) #updated y position
   updated_robot_position.append(u_t[2] * t + c_p[2]) #updated theta position
   
   #calculate Jacobians
   F = eye(len(x)) #jacobian of update function with respect to x
   F[0, 2] = -t * (u_t[0] * s + u_t[1] * c)
   F[1, 2] = t * (u_t[0] * c - u_t[1] * s)
   W = zeros((len(x),3),float)#jacobian of update function with respect to noise
   W[0,0] = t
   W[1,1] = t
   W[2,2] = t
   
   #calculate updated covariance matrix
   P_t = dot(dot(F,P),F.T) + dot(dot(W,Q),W.T) #F * P * F^T + W * Q * W^T
   #print "P_t", P_t
   #print dot(dot(W,Q),W.T)
   #print W
   #print Q
   
   print "EKF PREDICTION STEP"
   print "F", F
   print dot(dot(F,P),F.T)
   print "W", W
   print dot(dot(W,Q),W.T)
   print "P", P
   print "P_t", P_t
      
   #reset variables
   x[0] = updated_robot_position[0]
   x[1] = updated_robot_position[1]
   x[2] = updated_robot_position[2]
   
   P = P_t
   
   #print x
   
   return (x,P)
   
   
   
def ekf_update_step(d_t,z_t,x,P,R):

   # d_t : data associated landmarks
   # z_t : actual positions of landmarks
   # x   : robot position/landmark positions
   # P   : covariance matrix of x
   # t   : length of timestep
   # R   : covariance matrix of measurement noise
   
   if len(x) == 3 or len(d_t)-len(x) == len(z_t):
      return (x,P)

   c = math.cos(x[2])
   s = math.sin(x[2])
   xr = x[0]
   yr = x[1]
   for i in range((len(x)-3)/2):
      idx = 3 + 2*i
      if d_t[idx] == 0. and d_t[idx+1] == 0.:
         continue

      xm = x[idx]
      ym = x[idx + 1]
      
      H = zeros((2, len(x)))
      # Jacobians wrt robot (x,y,theta)
      H[0, 0] = -c
      H[0, 1] = -s
      H[0, 2] = -1*(xm - xr)*s + (ym - yr)*c
      H[1, 0] = s
      H[1, 1] = -c
      H[1, 2] = -1*(xm - xr)*c - (ym - yr)*s

      # Jacobians wrt map (x,y)
      H[0, idx] = c
      H[0, idx+1] = s
      H[1, idx] = -s
      H[1, idx+1] = c

      zx_pred =  (xm - xr)*c + (ym - yr)*s
      zy_pred = -(xm - xr)*s + (ym - yr)*c

      innov = array([d_t[idx] - zx_pred, d_t[idx+1] - zy_pred])
      #print "innov: ", innov
      
      S = dot (dot(H,P), H.transpose()) + R
      K = dot (dot(P, H.transpose()), linalg.inv(S))
      x_new = x + dot (K, innov)
      P_new = dot (eye(len(x)) - dot(K,H), P)
      
      x = x_new
      P = P_new

   return (x,P)



def data_association(z_t,x,P):

   chivalue = 5.991#40#9.21#5.991#4#9.21#5.991#1#1.5#2#3#5.991#2.5#5.991 #.3#1.2 #.005    #based on values gotten
   #v1 = considering closest as a match, others as new landmarks
   #v2 = considering closest a match, ignoring others
   #v3 = ignoring any duplicate matches

   #goal: return a d_t which has 0's for robot pose, 0's for previously seen landmarks that aren't viewed here, and measurements elsewhere---with new, unseen measurements past the end of len(x)
   d_t = zeros(len(x))
   landmarks = []
   for i in range(len(x)):
      landmarks.append([])
   
   Pinv = inv(P)
   
   for m in range(len(z_t)/2):
      smallest = None
      corresponding_landmark = None
      
      for i in range((len(x)-3)/2):
         diff = zeros(len(x)) #calculate appropriate x - mu
         
         x_val_expected = x[i*2+3]
         x_val_measured = x[0] + z_t[2*m]*math.cos(x[2]) - z_t[2*m+1]*math.sin(x[2])
         y_val_expected = x[i*2+4]
         y_val_measured = x[1] + z_t[2*m]*math.sin(x[2]) + z_t[2*m+1]*math.cos(x[2])
         diff[i*2+3] = x_val_expected - x_val_measured #difference in x values 
         diff[i*2+4] = y_val_expected - y_val_measured #difference in y values
         
         #d = math.sqrt(dot(dot(diff,Pinv),diff.T)) #Mahalanobis distance
         d = (dot(dot(diff,Pinv),diff.T)) #Mahalanobis distance
         if smallest == None or d < smallest:
            smallest = d
            corresponding_landmark = i
      
      if not smallest == None and smallest < chivalue: #then we've found a good match for the measurement
         #d_t[corresponding_landmark*2+3] = z_t[m*2]
         #d_t[corresponding_landmark*2+4] = z_t[m*2+1]
         #if corresponding_landmark in previdentified:
         #   print "ERROR: REPLACED LANDMARK"
         landmarks[corresponding_landmark].append(  ( (z_t[m*2],z_t[m*2+1]), smallest)  )
      else: #mark as new measurement--add on to end
         d_t.resize(len(d_t)+2)
         d_t[len(d_t)-2] = z_t[m*2]
         d_t[len(d_t)-1] = z_t[m*2+1]
         
   for i in range(len(landmarks)):
      if len(landmarks[i]) == 0:
         pass
      elif len(landmarks[i]) == 1:
         ((zx,zy),s) = landmarks[i][0]
         d_t[i*2+3] = zx
         d_t[i*2+4] = zy
      else:
         comp = landmarks[i]
         ((zx,zy),sm) = comp[0]
         ptr = 0
         for j in range(len(comp)):
            ((zx,zy),s) = comp[j]
            if s < sm:
               sm = s
               ptr = j
         ((zx,zy),s) = comp[ptr]
         d_t[i*2+3] = zx
         d_t[i*2+4] = zy
         #for j in range(len(comp)):
         #   if j == ptr:
         #      pass
         #   else:
         #      ((zx,zy),s) = comp[j]
         #      d_t.resize(len(d_t)+2)
         #      d_t[len(d_t)-2] = zx
         #      d_t[len(d_t)-1] = zy

   return d_t'''
