#EKF SLAM: data association

from numpy import *
from numpy.linalg import *

'''
Takes in current pose/map data and covariance matrix along with the current timestep's lidar data and returns the landmark-associated data.
'''
def data_association(z_t,x,P):

   chivalue = 10#5.991#40#9.21#5.991#4#9.21#5.991#1#1.5#2#3#5.991#2.5#5.991 #.3#1.2 #.005    #based on values gotten
   #v1 = considering closest as a match, others as new landmarks
   #v2 = considering closest a match, ignoring others
   #v3 = ignoring any duplicate matches

   #goal: return a d_t which has 0's for robot pose, 0's for previously seen landmarks that aren't viewed here, and measurements elsewhere---with new, unseen measurements past the end of len(x)
   d_t = zeros(len(x))
   landmarks = []
   for i in range(len(x)):
      landmarks.append([])
   
   Pinv = inv(P)
   
   for m in range(len(z_t)/2):
      smallest = None
      corresponding_landmark = None
      
      for i in range((len(x)-3)/2):
         diff = zeros(len(x)) #calculate appropriate x - mu
         
         x_val_expected = x[i*2+3]
         x_val_measured = x[0] + z_t[2*m]*math.cos(x[2]) - z_t[2*m+1]*math.sin(x[2])
         y_val_expected = x[i*2+4]
         y_val_measured = x[1] + z_t[2*m]*math.sin(x[2]) + z_t[2*m+1]*math.cos(x[2])
         diff[i*2+3] = x_val_expected - x_val_measured #difference in x values 
         diff[i*2+4] = y_val_expected - y_val_measured #difference in y values
         
         #d = math.sqrt(dot(dot(diff,Pinv),diff.T)) #Mahalanobis distance
         d = (dot(dot(diff,Pinv),diff.T)) #Mahalanobis distance
         if smallest == None or d < smallest:
            smallest = d
            corresponding_landmark = i
      
      if not smallest == None and smallest < chivalue: #then we've found a good match for the measurement
         #d_t[corresponding_landmark*2+3] = z_t[m*2]
         #d_t[corresponding_landmark*2+4] = z_t[m*2+1]
         #if corresponding_landmark in previdentified:
         #   print "ERROR: REPLACED LANDMARK"
         landmarks[corresponding_landmark].append(  ( (z_t[m*2],z_t[m*2+1]), smallest)  )
      else: #mark as new measurement--add on to end
         d_t.resize(len(d_t)+2)
         d_t[len(d_t)-2] = z_t[m*2]
         d_t[len(d_t)-1] = z_t[m*2+1]
         
   for i in range(len(landmarks)):
      if len(landmarks[i]) == 0:
         pass
      elif len(landmarks[i]) == 1:
         ((zx,zy),s) = landmarks[i][0]
         d_t[i*2+3] = zx
         d_t[i*2+4] = zy
      '''else:
         comp = landmarks[i]
         ((zx,zy),sm) = comp[0]
         ptr = 0
         for j in range(len(comp)):
            ((zx,zy),s) = comp[j]
            if s < sm:
               sm = s
               ptr = j
         ((zx,zy),s) = comp[ptr]
         d_t[i*2+3] = zx
         d_t[i*2+4] = zy
         for j in range(len(comp)):
            if j == ptr:
               pass
            else:
               ((zx,zy),s) = comp[j]
               d_t.resize(len(d_t)+2)
               d_t[len(d_t)-2] = zx
               d_t[len(d_t)-1] = zy'''

   return d_t
