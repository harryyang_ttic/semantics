##    * functions
##          o output_as_speech( text )
import threading
import festival
import time
from optparse import OptionParser
import pygtk
import gtk
import lcm
import functools
from slam.language_question_t import language_question_t
from slam.language_label_t import language_label_t

class lcm_listener(threading.Thread):
    def __init__(self,callback, speak_descriptions=False):
        threading.Thread.__init__(self)
        self.callback = callback
        self.lc = lcm.LCM()
        self.subscription_question = self.lc.subscribe("SLAM_SR_QUESTION", self.question_handler)
        if speak_descriptions:
            print "Listening to descriptions"
            self.subscription_description = self.lc.subscribe("LANGUAGE_LABEL", self.language_handler)
            
        self._running = True        

    def question_handler(self, channel, data):
        msg = language_question_t.decode(data)
        #gobject.idle_add(self.callback, "turn_right")
        if(msg.id == -1):
            print "Clear question command heard" 
            return
        t = threading.Thread( target=functools.partial(self.callback , msg.question))
        t.start()

    def language_handler(self, channel, data):
        msg = language_label_t.decode(data)
        
        label = ""
        if msg.constraint_type ==  language_label_t.TYPE_SIMPLE:
            label = "we are in the " + msg.update
        elif msg.constraint_type == language_label_t.TYPE_COMPLEX:
            label = msg.update

        print "Heard Description " , label

        t = threading.Thread( target=functools.partial(self.callback , label))
        t.start()

    def run(self):
        print "Started LCM Listener"
        try:
            while self._running:
                self.lc.handle()
        except KeyboardInterrupt:
            pass

class speech_synthesizer:
    def __init__( self):
        self.synthesizer = festival.Festival()
        self.ss_active = threading.Condition()
        self.speaking = False

    def output_as_speech( self, pharse ):
        self.ss_active.acquire()
        self.synthesizer.say( pharse )
        self.ss_active.notifyAll()
        self.ss_active.release()
        
        print "++++++++++++++++++++++++++++++"
        print pharse

    def interrupt_synthesizer( self ):
        self.synthesizer.close()


if __name__ == "__main__":    
    parser = OptionParser()
    parser.add_option("-p", "--path", dest="path",action="store",
                     help="File Path")

    parser.add_option("-s", "--speak-description", dest="speak_description",action="store_true", 
                      default=False, help="Speak Label Description")

    (options, args) = parser.parse_args()
    
    synth = speech_synthesizer()
    synth.output_as_speech("Welcome, I will be asking the questions here")

    ##This needs the gtk thread 

    background = lcm_listener(synth.output_as_speech, options.speak_description)
    background.start()

    #Add simple gui here if you want 

    gtk.gdk.threads_init()
    gtk.main()
