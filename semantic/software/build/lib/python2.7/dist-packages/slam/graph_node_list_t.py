"""LCM type definitions
This file automatically generated by lcm.
DO NOT MODIFY BY HAND!!!!
"""

try:
    import cStringIO.StringIO as BytesIO
except ImportError:
    from io import BytesIO
import struct

import slam.graph_node_t

class graph_node_list_t(object):
    __slots__ = ["utime", "no_nodes", "nodes"]

    def __init__(self):
        self.utime = 0
        self.no_nodes = 0
        self.nodes = []

    def encode(self):
        buf = BytesIO()
        buf.write(graph_node_list_t._get_packed_fingerprint())
        self._encode_one(buf)
        return buf.getvalue()

    def _encode_one(self, buf):
        buf.write(struct.pack(">qi", self.utime, self.no_nodes))
        for i0 in range(self.no_nodes):
            assert self.nodes[i0]._get_packed_fingerprint() == slam.graph_node_t._get_packed_fingerprint()
            self.nodes[i0]._encode_one(buf)

    def decode(data):
        if hasattr(data, 'read'):
            buf = data
        else:
            buf = BytesIO(data)
        if buf.read(8) != graph_node_list_t._get_packed_fingerprint():
            raise ValueError("Decode error")
        return graph_node_list_t._decode_one(buf)
    decode = staticmethod(decode)

    def _decode_one(buf):
        self = graph_node_list_t()
        self.utime, self.no_nodes = struct.unpack(">qi", buf.read(12))
        self.nodes = []
        for i0 in range(self.no_nodes):
            self.nodes.append(slam.graph_node_t._decode_one(buf))
        return self
    _decode_one = staticmethod(_decode_one)

    _hash = None
    def _get_hash_recursive(parents):
        if graph_node_list_t in parents: return 0
        newparents = parents + [graph_node_list_t]
        tmphash = (0xed56be2abd664c53+ slam.graph_node_t._get_hash_recursive(newparents)) & 0xffffffffffffffff
        tmphash  = (((tmphash<<1)&0xffffffffffffffff)  + (tmphash>>63)) & 0xffffffffffffffff
        return tmphash
    _get_hash_recursive = staticmethod(_get_hash_recursive)
    _packed_fingerprint = None

    def _get_packed_fingerprint():
        if graph_node_list_t._packed_fingerprint is None:
            graph_node_list_t._packed_fingerprint = struct.pack(">Q", graph_node_list_t._get_hash_recursive([]))
        return graph_node_list_t._packed_fingerprint
    _get_packed_fingerprint = staticmethod(_get_packed_fingerprint)

