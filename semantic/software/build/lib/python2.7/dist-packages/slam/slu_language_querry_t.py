"""LCM type definitions
This file automatically generated by lcm.
DO NOT MODIFY BY HAND!!!!
"""

try:
    import cStringIO.StringIO as BytesIO
except ImportError:
    from io import BytesIO
import struct

import slam.graph_path_node_t

class slu_language_querry_t(object):
    __slots__ = ["id", "no_region_nodes", "region_nodes", "language_node"]

    def __init__(self):
        self.id = 0
        self.no_region_nodes = 0
        self.region_nodes = []
        self.language_node = slam.graph_path_node_t()

    def encode(self):
        buf = BytesIO()
        buf.write(slu_language_querry_t._get_packed_fingerprint())
        self._encode_one(buf)
        return buf.getvalue()

    def _encode_one(self, buf):
        buf.write(struct.pack(">qi", self.id, self.no_region_nodes))
        for i0 in range(self.no_region_nodes):
            assert self.region_nodes[i0]._get_packed_fingerprint() == slam.graph_path_node_t._get_packed_fingerprint()
            self.region_nodes[i0]._encode_one(buf)
        assert self.language_node._get_packed_fingerprint() == slam.graph_path_node_t._get_packed_fingerprint()
        self.language_node._encode_one(buf)

    def decode(data):
        if hasattr(data, 'read'):
            buf = data
        else:
            buf = BytesIO(data)
        if buf.read(8) != slu_language_querry_t._get_packed_fingerprint():
            raise ValueError("Decode error")
        return slu_language_querry_t._decode_one(buf)
    decode = staticmethod(decode)

    def _decode_one(buf):
        self = slu_language_querry_t()
        self.id, self.no_region_nodes = struct.unpack(">qi", buf.read(12))
        self.region_nodes = []
        for i0 in range(self.no_region_nodes):
            self.region_nodes.append(slam.graph_path_node_t._decode_one(buf))
        self.language_node = slam.graph_path_node_t._decode_one(buf)
        return self
    _decode_one = staticmethod(_decode_one)

    _hash = None
    def _get_hash_recursive(parents):
        if slu_language_querry_t in parents: return 0
        newparents = parents + [slu_language_querry_t]
        tmphash = (0x336af0963cae754f+ slam.graph_path_node_t._get_hash_recursive(newparents)+ slam.graph_path_node_t._get_hash_recursive(newparents)) & 0xffffffffffffffff
        tmphash  = (((tmphash<<1)&0xffffffffffffffff)  + (tmphash>>63)) & 0xffffffffffffffff
        return tmphash
    _get_hash_recursive = staticmethod(_get_hash_recursive)
    _packed_fingerprint = None

    def _get_packed_fingerprint():
        if slu_language_querry_t._packed_fingerprint is None:
            slu_language_querry_t._packed_fingerprint = struct.pack(">Q", slu_language_querry_t._get_hash_recursive([]))
        return slu_language_querry_t._packed_fingerprint
    _get_packed_fingerprint = staticmethod(_get_packed_fingerprint)

