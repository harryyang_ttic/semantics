import numpy as na

class LabelDistribution():
   """
   Label distribution class for language_observation script
   """
   def __init__(self):
      
      #self.labels = ['unspecified', 'gym', 'hallway', 'conference room', 'kitchen', 'elevator lobby']
      self.labels = ['courtyard', 'gym', 'hallway', 'amphitheater', 'cafeteria', 'elevator lobby', 'entrance', 'lobby']

      self.num_labels = len(self.labels)
      self.total_obs = len(self.labels)
      self.observation_frequency = na.ones(len(self.labels))
      self.count_labels = []
      for i in range(self.num_labels):
         self.count_labels.append(0)
         
   def get_main_label(self):
      m = 0
      for i in range(self.num_labels):
         if self.observation_frequency[i] > self.observation_frequency[m]:
            m = i
      return m

   def is_uniform(self):
      """
      True if all labels are equally likely; false otherwise
      """
      if na.any(na.array(self.observation_frequency) - self.observation_frequency[0]):
         return False
      else:
         return True

   def best_label(self):
      if self.is_uniform():
         return ""
      else:
         return self.labels[na.argmax(self.observation_frequency)]
      
   def add(self, l, amount=1):
      self.observation_frequency[l]+=amount
      self.total_obs += amount
      if amount == 1:
         self.count_labels[l]+=1
   
   def getlabels(self):
      rtrn = []
      for i in range(len(self.count_labels)):
         for j in range(self.count_labels[i]):
            rtrn.append(i)
      #print "count labels: ", self.count_labels
      #print "rtrn: ", rtrn
      return rtrn
      
   def printdata(self):
      print "Total observations: ", self.total_obs
      s = "obs dist: "
      for i in range(self.num_labels):
         s += str(self.observation_frequency[i]) + " "
      print s
      
   def getprobability(self, s):
      """
      Returns the probability for multinomial key 's', or 0 if s is unknown.
      """
      for i in range(len(self.labels)):
         if self.labels[i] == s:
            return self.observation_frequency[i]/self.total_obs
      return 0
   
   def getdictionary(self):
      """
      Returns a dictionary of string: probability for the multinomial
      """
      d = dict()
      for i in self.labels:
         d[i] = self.getprobability(i);
      return d
