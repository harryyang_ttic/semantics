# topological graph slam
from particle import Particle
from label_distribution import LabelDistribution
from PyQt4.QtCore import SIGNAL, QSocketNotifier, QObject, QTimer
from node import Node
from edge import Edge
import slu_api
from semantic_mapping.semantic_mapping_state import particle_to_context

from numpy import log
#from numpy.linalg import 
import scipy
#from scipy import *
#from scipy.integrate import*
import sys
import random
import traceback
import time

from lcm import LCM
from slam.slu_result_list_t import slu_result_list_t
from slam.slu_result_t import slu_result_t
from slam.slu_querry_t import slu_querry_t
from slam.graph_edge_t import graph_edge_t
from slam.node_label_list_t import node_label_list_t
from slam.graph_particle_list_t import graph_particle_list_t
from slam.node_probability_t import node_probability_t
from slam.dirichlet_update_t import dirichlet_update_t
from slam.language_label_t import language_label_t
from slam.language_edge_t import language_edge_t


import basewindow 
from g3.cost_functions.gui import crfFeatureWeights


class lcm_listener():
   def __init__(self):
      #self.slu_api = slu_api.SluApi()
      self._running = True
      self.lc = LCM()
      ## We are given a complex language as well as a corresponding particle list 
      ## This needs to be converted to the python particle definition and then 
      ## Sent over to SLU and then scored 
      ## The resulting scores need to be sent back to the isam module - which 
      ## updates the Dir distributions of the nodes and then proposes new language edges 
      self.querry_sub = self.lc.subscribe("ISAM_SLU_LANGUAGE", self.slu_querry_handler)
      self.received_slam_message = 0
      self.time = 0
      self.num_particles = 0
      self.particles = []
      self.similarity_threshold = .2
      self.labelsToAdd = []
      ### How to make this mapping not be static ??
      self.keyword_lookup = {'courtyard' : 0, 'gym' : 1, 'hallway' : 2, 'amphitheater' : 3, 'cafeteria' : 4, 'elevator lobby' : 5, 'entrance': 6, 'lobby':7}
      self.slu = slu_api.SluApi()
      ### what is this used for?? 
      self.saliency = [.8, .9, .1, .95, .5, .4, .7]
      self.featureBrowser = crfFeatureWeights.MainWindow()
      self.featureBrowser.show()
      print "made slu"

   def lcm_filenos(self):
      return [self.lc.fileno(),
              #self.slu_api.pquery.lcm_fileno,
              ]
              
   def activated(self, i):
      print "Activate Called"
      self.lc.handle()
      print "Activate Returned"

   def run(self):
      print "Started LCM listener"
      
      while self._running:
         try:
            self.lc.handle()
            
         except KeyboardInterrupt:
            print "\n============== Interrupt =================\n"
            self._running = False
            pass
   
   def decode_particle_msg(self, particle_list):
      self.num_particles = particle_list.no_particles
      new_particles = []
      for i in range(particle_list.no_particles):
         particle = Particle()
         par = particle_list.particle_list[i]
         particle.id = par.id
         particle.num_nodes = par.no_nodes
         particle.weight = par.weight
         particle.nodelist = []
         #print "------------------------------------------\nParticle Id: ", par.id
         for j in range(par.no_nodes):
            node = par.node_list[j]
            nodeinst = Node()
            nodeinst.id = node.id
            nodeinst.xy = node.xy
            nodeinst.heading = node.heading
            nodeinst.pofz = node.pofz
            nodeinst.is_supernode = node.is_supernode
            nodeinst.cov = node.cov
            nodeinst.parent_supernode = node.parent_supernode
            ## Bounding box
            bb = []
            for i in range(node.no_points):
               bb.append( (node.x_coords[i], node.y_coords[i]) )
            nodeinst.bounding_box = bb

            ld = node.labeldist
            
            nodeinst.label_dist = LabelDistribution()
            nodeinst.label_dist.num_labels = ld.num_labels
            nodeinst.label_dist.total_obs = ld.total_obs
            
            #print "Label distribution size : ", len(nodeinst.label_dist.observation_frequency)
            #print "Msg Label Size : " , ld.num_labels
            
            for k in range(ld.num_labels):
               nodeinst.label_dist.observation_frequency[k] = ld.observation_frequency[k]

            particle.nodelist.append(nodeinst)
            #The dict is prob not useful now - but we will keep it around for now 
            particle.nodelist_dict[nodeinst.id] = nodeinst
            
         particle.edges = []
         for j in range(par.no_edges):
            ## We should prob get rid of the bad edges 
            edge = par.edge_list[j]
            #Add only sucessful ones
            if(edge.status == graph_edge_t.STATUS_SUCCESS):
               edgeinst = Edge()
               edgeinst.node1 = edge.node_id_1
               edgeinst.node2 = edge.node_id_2
               edgeinst.edge_id = edge.id
               edgeinst.status = edge.status
               edgeinst.type = edge.type               
               #This seems to want to keep around if an edge is new - i dont think we will need that now 
               edgeinst.new = True
               
               edgeinst.transformation = edge.transformation
               edgeinst.cov = edge.cov
               particle.edges.append(edgeinst)
         ## Doublecheck with stephanie that we dont need to add logical constraints between previous and current 
         ## supernodes
         new_particles.append(particle)
      return new_particles

   def send_slu_update(self, result):
      msg = slu_result_list_t() 
      msg.utime = 0 
      msg.size = len(result.keys())
      msg.results = []
      for p_id in result.keys():
         part_res = result[p_id]
         p_result = slu_result_t()
         p_result.particle_id = p_id
         p_result.no_labels = len(part_res.keys())
         p_result.labels = []
         print "Particle ID : " + str(p_id)
         for l in part_res.keys():
            nd_list = part_res[l]
            #These are the node prob pairs for each label
            nd_label_list = node_label_list_t()
            nd_label_list.label = l
            
            nd_label_list.nodes = []

            ##Sum up and normalize and send
            node_prob = {}
            full_prob = 0
            for nd in nd_list:
               if(node_prob.has_key(nd[0])):
                  node_prob[nd[0]] = [nd[0], node_prob[nd[0]][1] + nd[1]]
               else:
                  node_prob[nd[0]] = [nd[0], nd[1]]
               full_prob += nd[1]
                  

            print node_prob.keys()

            print node_prob

            #for nd in nd_list:
            #   #print "Node ID : " + str(nd[0])  + " Prob : " + str(nd[1])
            #   nd_t = node_probability_t()
            #   nd_t.id = nd[0]
            #   nd_t.prob = nd[1]
            #   nd_label_list.nodes.append(nd_t)
            
            for nd_key in node_prob.keys():
               print "Node ID : " + str(nd_key)  + " Prob : " + str(node_prob[nd_key][1]) #/ full_prob)
               nd_t = node_probability_t()
               nd_t.id = node_prob[nd_key][0]#nd_key
               nd_t.prob = node_prob[nd_key][1]#node_prob[nd_key] #/ full_prob
               nd_label_list.nodes.append(nd_t)

            nd_label_list.num = len(node_prob.keys())

            p_result.labels.append(nd_label_list)

         msg.results.append(p_result)

      print "Message Formed" 
      self.lc.publish("SLU_LANG_RESULT",msg.encode())

   def send_slu_update_old(self, result):
      msg = slu_result_list_t() 
      msg.utime = 0 
      msg.size = len(result.keys())
      msg.results = []
      for p_id in result.keys():
         part_res = result[p_id]
         p_result = slu_result_t()
         p_result.particle_id = p_id
         p_result.no_labels = len(part_res.keys())
         p_result.labels = []
         for l in part_res.keys():
            nd_list = part_res[l]
            #These are the node prob pairs for each label
            nd_label_list = node_label_list_t()
            nd_label_list.label = l
            nd_label_list.num = len(nd_list)
            nd_label_list.nodes = []

            ##Sum up and normalize and send
            
            for nd in nd_list:
               #print "Node ID : " + str(nd[0])  + " Prob : " + str(nd[1])
               nd_t = node_probability_t()
               nd_t.id = nd[0]
               nd_t.prob = nd[1]
               nd_label_list.nodes.append(nd_t)
            p_result.labels.append(nd_label_list)

         msg.results.append(p_result)

      print "Message Formed" 
      self.lc.publish("SLU_LANG_RESULT",msg.encode())

   def slu_querry_handler(self, channel, data):
      msg = slu_querry_t.decode(data)
      
      label = msg.utterance
      particle_msg = msg.plist
      
      particle_list = self.decode_particle_msg(particle_msg)

      print "Received SLU request"

      ##Dump pickle files

      ## Call SLU 
      figure_text, results = self.slu.score_particles(label, particle_list)

      print "Done Processing"
      print "Figure Text", figure_text

      ### Done - lets package this up and send it off to iSAM
      starting_context = particle_to_context(results[0][0])

      result_to_send = {}

      for particle, result_list in results:
         print "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\nPARTICLE\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
         print particle
         
         result_for_particle = {}
         for k in figure_text.keywords: 
            l = self.keyword_lookup[k]  
            #this is the result for each particle 
            nodes_to_update = []
            for result in result_list:
               ## result.figure_map_node  # this is the node for which the prob is returned (not the id - the node) 
               ### wonder what the figure text is ?? - can it be more than one label
               ## result.probability is the liklihood 
               #print "Node ID: " + str(result.figure_map_node.id) + " => Prob " + str(result.probability)
               res = [result.figure_map_node.id, result.probability]
               nodes_to_update.append(res)
         
            result_for_particle[l] = nodes_to_update

         result_to_send[particle.id] = result_for_particle
      
         
      self.send_slu_update(result_to_send)
      ## These tend to cause segfault in Sachi's machine
      print "Calling show"
      #featureBrowser = crfFeatureWeights.MainWindow()
      #featureBrowser.show()
      particle, result_list = results[0]

      self.featureBrowser.load(starting_context, self.slu.cost_function, result_list[0].path_factor)
      
      print "Done"
      #import cPickle
      #cPickle.dump((self.particles, channel, data), open("label_handler_args.pck", "w"))
      
      #self.app.exec_() 

def main():
   import signal
   import os
   
   #app = basewindow.makeApp()
   #slu = slu_api.SluApi()
   
   def exitAll():
        os.killpg(os.getpgrp(), signal.SIGINT)

   def handler(sig, arg):
        os.kill(os.getpid(), signal.SIGTERM)
   signal.signal(signal.SIGINT, handler)
      

        
   app = basewindow.makeApp()

   lcm = lcm_listener()

   for fileno in lcm.lcm_filenos():
      socket_notifier = QSocketNotifier(fileno, QSocketNotifier.Read)
      app.connect(socket_notifier,
                  SIGNAL("activated(int)"),
                  lcm.activated)

        
   #app.connect(socket_notifier,
   #            SIGNAL("activated(int)"),
   #            lcm.activated)

   #timer = QTimer()
   #timer.setInterval(100)
   #timer.setSingleShot(False)
   #app.connect(timer, SIGNAL('timeout()'),print_status)
                          #glWidget.spin)
   #timer.start()

   #featureBrowser = crfFeatureWeights.MainWindow()
   #featureBrowser.show()


   #while bg_thread.is_alive():
      #bg_thread.join(1)
   print "Calling App.exec"
   app.exec_()
      
if __name__ == "__main__":
   main()











