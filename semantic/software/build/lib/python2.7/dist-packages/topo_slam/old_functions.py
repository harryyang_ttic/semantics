#old functions from topo_graph_slam.py

   def send_init_node_message(self, utime):
      msg = init_node_t()
      msg.id = self.particles[0].num_nodes
      msg.utime = utime
      self.lc.publish("NODE_INIT",msg.encode())
      print "init node message sent"
      
      
      
        def add_edge(self,i,j):
      print "----------------------------------------\nparticle : " , i  , "\nnode: ", j
      #calculate the distance between the current position and the old node
      d = self.calc_distance(i,j)
      if d > self.constraint_dist_threshold:
         print "Nodes too far - skipping" 
      else:
         #print "finaldistance: ", d              
         #calculate the probability of adding an edge between these two nodes
         p = self.calc_probability(d)
         #print "probability: ", p, "\n\n"
         #generate a random number and add an edge if necessary
         r = random.random()
         if r < p:
            #type of edge = scanmatch
            e = Edge(node1=self.particles[i].nodelist[self.particles[i].num_nodes-1].id, node2=self.particles[i].nodelist[j].id, _id=len(self.particles[i].edges), flag=0,type_edge=2)
            self.particles[i].edges.append(e)
         
         
         
         
            def add_dist_edge(self,i,j):
      d = self.calc_distance(i,j)
      if d > self.constraint_dist_threshold:
         print "Nodes too far - skipping" 
      else:
         p = self.calc_probability(d)
         if (self.particles[i].num_nodes -1)- j > 3 or (self.particles[i].num_nodes -1)- j == 1:
            typeofedge=0
            if (self.particles[i].num_nodes -1)- j > 3:
               typeofedge = 2
            else:
               typeofedge = 1
            r = random.random()
            if r < p:
               e = Edge(node1=self.particles[i].nodelist[self.particles[i].num_nodes-1].id, node2=self.particles[i].nodelist[j].id, _id=len(self.particles[i].edges), flag=0,type_edge=typeofedge)
               self.particles[i].edges.append(e)
         
         
         
         
         
   def add_edge_alt_model(self,i,j):
      #calculate the distance between the current position and the old node
      d = self.calc_distance(i,j)
      
      if(d > self.constraint_dist_threshold):
         print "Nodes too far - skipping\n"
               
      ###################### Lets try a different node creation function 
      #calculate the probability of adding an edge between these two nodes

      #Sachi - added this different prob threshold calculation method
      no_edges_1 = self.particles[i].nodelist[self.particles[i].num_nodes-1].no_constraints
      no_edges_2 = self.particles[i].nodelist[j].no_constraints
      edge_gap = abs(self.particles[i].num_nodes-1 - j)
      p = self.calc_probability_less(d, no_edges_1, no_edges_2, edge_gap)
      print "probability: ", p, "\n\n"
      #generate a random number and add an edge if necessary
      r = random.random()
      typeofedge = 0
      if self.particles[i].nodelist[self.particles[i].num_nodes-2].id - self.particles[i].nodelist[j].id > 2:
         typeofedge = 2
      elif self.particles[i].nodelist[self.particles[i].num_nodes-2].id - self.particles[i].nodelist[j].id == 0:
         typeofedge = 1
      if r < p:
         e = Edge(node1=self.particles[i].nodelist[self.particles[i].num_nodes-1].id, node2=self.particles[i].nodelist[j].id, _id=len(self.particles[i].edges), flag=0,type_edge=typeofedge)
         self.particles[i].edges.append(e) 
         self.particles[i].nodelist[self.particles[i].num_nodes-1].no_constraints += 1
         self.particles[i].nodelist[j].no_constraints += 1
            
            
            
            
            
                              
            
      

            
   def calc_distance(self, particle_id, node_id):
      node = self.particles[particle_id].nodelist[node_id]
      x1 = node.xy[0]
      y1 = node.xy[1]
      prevnode = self.particles[particle_id].nodelist[self.particles[particle_id].num_nodes-2]
      x2 = prevnode.xy[0]
      y2 = prevnode.xy[1]
      #print "x1, y1, x2, y2: ", x1, y1, x2, y2
      radius = math.sqrt( (self.prev_pose.pos[0] - self.pose.pos[0])**2 + (self.prev_pose.pos[1] - self.pose.pos[1])**2 + (self.prev_pose.pos[2] - self.pose.pos[2])**2 )
      print "radius: " , radius
      print "distance: ", math.sqrt((x1-x2)**2 + (y1-y2)**2)
      return max( math.sqrt((x1-x2)**2 + (y1-y2)**2) - radius, 0)
      
      
      




   def calc_probability_less(self, d, no_edges_1, no_edges_2, edge_gap):
      #to add the previous
      if(d == 0):
         return 1
      
      avg_no_edges = (no_edges_1 + no_edges_2)/2
      #do we cause edges farther away to have a high connection likelihood?
      avg_edge_count = 4
      threshold = (1.0 / (self.const* (d**4) + 1)) * 1/(abs(avg_no_edges - avg_edge_count) +1)
      print "Avg no edges : ", avg_no_edges, "Prob Threshold : ",  threshold
      return threshold
      
      
      
            
      
      
      
      
   def calc_probability(self, d):
      return 1.0 / (self.const* (d**4) + 1)
      
      
      
      
      
      
      '''graph_particle = graph_particle_t.decode(data)
      graph_edge = graph_particle.edge_list[0]
      e = Edge(node1=graph_edge.node_id_1, node2=graph_edge_.node_id_2, flag=0, type_edge=4)
      for i in self.particles:
         if i.id == graph_particle.id:
            i.add_edge(e)'''
            
      '''graph_edge = graph_edge_t.decode(data)
      e = Edge(node1=graph_edge.node_id_1, node2=graph_edge_.node_id_2, flag=0, type_edge=4)
      for i in self.particles:
         if i.id == graph_edge.particle_id:
            i.add_edge(e)'''
