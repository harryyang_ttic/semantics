import sys
import threading
import math
#from PyQt4 import QtCore
#from PyQt4.QtCore import SIGNAL, QSocketNotifier
#import basewindow

from lcm import LCM
from slam.language_label_t import language_label_t
from bot_core.pose_t import pose_t


class lcm_listener(object):
    def __init__(self, language_fname):

        self.language_list = []
        self.language_list_full = []
        f = open(language_fname, 'r')
        for line in f:
            line = line.strip()
            if(len(line) == 0):
                break
            if(line[0] == '#'):
                continue
            self.language_list.append(line.split(','))
            self.language_list_full.append(line.split(','))

        for i in self.language_list:
            print "Loaded : ", i

        self.lcm = LCM()
        self.pose_sub = self.lcm.subscribe("POSE", self.pose_handler)
        self.slam_status_sub = self.lcm.subscribe("SLAM_STATUS", self.slam_status_handler)
        self._running = True

    def lcm_filenos(self):
        return [self.lcm.fileno()]
      
    def activated(self, i):
        self.lcm.handle()

    def run(self):
        while self._running:
            try:
                self.lcm.handle()
            except KeyboardInterrupt:
                print "\n============================\n"
                self._running = False
                pass

    def pose_handler (self, channel, data):
        pose = pose_t.decode (data)

        if (len(self.language_list) > 0):
            if(self.language_list[0][0] == ''):
                print "Error - Invalid Time - poping"
                self.language_list.pop(0)
            if (pose.utime > int(self.language_list[0][0])):
                if (math.fabs(pose.utime - int(self.language_list[0][0]))/1.0e6 > 0.5): #if the observation is too old - remove but don't publish
                    self.language_list.pop(0)
                    print "Observation too old - removing"
                else:
                    msg = language_label_t()
                    msg.utime = int(self.language_list[0][0])
                    msg.label = int(self.language_list[0][1])
                    msg.constraint_type = int(self.language_list[0][2])
                    msg.update = str(self.language_list[0][3])
                    self.lcm.publish("LANGUAGE_LABEL", msg.encode())
                    print "\nPublishing LANGUAGE LABEL with label = " + str(msg.label) + " constraint type = " + str(msg.constraint_type) + " and string: " + msg.update + "\n"
                    
                    # Pop it off the list
                    self.language_list.pop(0)

    def slam_status_handler (self, channel, data):
        print "\nSLAM reset - Reloading annotations\n"

        self.language_list = list(self.language_list_full)


def main():
    import os
    import signal

    def exitAll():
        os.killpg (os.getpgrp(), signal.SIGINT)

    def handler (sig, arg):
        os.kill (os.getpid(), signal.SIGTERM)
    signal.signal (signal.SIGINT, handler)

    language_fname = sys.argv[1]


    lcm = lcm_listener (language_fname)

    bg_thread = threading.Thread (target=lcm.run)
    bg_thread.start()

    #try:
    #
    #except (KeyboardInterrupt, SystemExit):
        


    #app = basewindow.makeApp()

    #for fileno in lcm.lcm_filenos():
    #    socket_notifier = QSocketNotifier(fileno, QSocketNotifier.Read)
    #    QtCore.QObject.connect(socket_notifier,
    #                           SIGNAL("activated(int)"),
    #                           lcm.activated)

        
    #app.connect(socket_notifier,
    #            SIGNAL("activated(int)"),
    #            lcm.activated)


    #app.exec_()


if __name__ == "__main__":
    main()
