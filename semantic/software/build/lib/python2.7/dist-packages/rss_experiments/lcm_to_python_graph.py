import time
from particle import Particle
from label_distribution import LabelDistribution
from node import Node
from edge import Edge
import sys
import threading
import pickle

from slam.particle_request_t import particle_request_t
from slam.graph_edge_t import graph_edge_t
from slam.node_label_list_t import node_label_list_t
from slam.graph_particle_list_t import graph_particle_list_t
from slam.node_probability_t import node_probability_t
from slam.dirichlet_update_t import dirichlet_update_t
from slam.language_label_t import language_label_t
from slam.language_edge_t import language_edge_t

from lcm import LCM


class lcm_listener():
   def __init__(self, _file_path):
      self._running = True
      self.lc = LCM()
      self.file_path = _file_path
      self.querry_sub = self.lc.subscribe("PARTICLE_ISAM_RESULT", self.particle_handler)
      self.particle_save_request = self.lc.subscribe("PARTICLE_SAVE_REQUEST", self.particle_request_handler)
      self.to_save_particle_id = -1
      self.last_particle_message = None
            
   def run(self):
      print "Started LCM listener"
      
      while self._running:
         try:
            self.lc.handle()
         
         #this doesn't work - but the interupt handler in the main thread works 
         except KeyboardInterrupt:
            print "\n============== Interrupt =================\n"
            self._running = False
            pass

   def particle_handler(self, channel, data):
       print "."
       self.last_particle_message = graph_particle_list_t.decode(data)

   def particle_request_handler(self, channel, data):
       msg = particle_request_t.decode(data)
       self.to_save_particle_id = msg.particle_id
       particle = self.decode_valid_particle_from_message()
       print particle
       if particle == None:
          print "No valid Particle"
          return 
       print "Saving Particle" 

       for edge in particle.edges:
          print " Node : " , edge.node1 , " === Node : " , edge.node2

       f = open(self.file_path, "w");
       for edge in particle.edges:
          f.write(str(edge.node1) + "," + str(edge.node2)+ ",1\n")
       f.close()
   
       '''output = open(self.file_path, 'wb')
       print "File " 
       print output
       pickle.dump(output, particle)
       output.close()'''
       

   def decode_valid_particle_from_message(self):
       particle_list = self.last_particle_message
       if(particle_list == None):
           print "No Particle Saved"
           return None
       for i in range(particle_list.no_particles):
          par = particle_list.particle_list[i]
          if(par.id != self.to_save_particle_id):
              continue
          self.to_save_particle_id = -1
          
          particle = Particle()
          particle.populate_with_lcm_message(par)
          return particle

   def decode_valid_particle_from_message_old(self):
       particle_list = self.last_particle_message
       if(particle_list == None):
           print "No Particle Saved"
           return None
       for i in range(particle_list.no_particles):
          par = particle_list.particle_list[i]
          if(par.id != self.to_save_particle_id):
              continue
          self.to_save_particle_id = -1
          
          particle = Particle()

          particle.id = par.id
          particle.num_nodes = par.no_nodes
          particle.weight = par.weight
          particle.nodelist = []
          #print "------------------------------------------\nParticle Id: ", par.id
          for j in range(par.no_nodes):
              node = par.node_list[j]
              nodeinst = Node()
              nodeinst.id = node.id
              nodeinst.xy = node.xy
              nodeinst.heading = node.heading
              nodeinst.pofz = node.pofz
              nodeinst.is_supernode = node.is_supernode
              nodeinst.cov = node.cov
              nodeinst.parent_supernode = node.parent_supernode
            ## Bounding box
              bb = []
              for i in range(node.no_points):
                  bb.append( (node.x_coords[i], node.y_coords[i]) )
              nodeinst.bounding_box = bb
              
              ld = node.labeldist
            
              nodeinst.label_dist = LabelDistribution()
              nodeinst.label_dist.num_labels = ld.num_labels
              nodeinst.label_dist.total_obs = ld.total_obs
            
            #print "Label distribution size : ", len(nodeinst.label_dist.observation_frequency)
            #print "Msg Label Size : " , ld.num_labels
              
              for k in range(ld.num_labels):
                  nodeinst.label_dist.observation_frequency[k] = ld.observation_frequency[k]

                  particle.nodelist.append(nodeinst)
            #The dict is prob not useful now - but we will keep it around for now 
                  particle.nodelist_dict[nodeinst.id] = nodeinst
            
          particle.edges = []
          for j in range(par.no_edges):
            ## We should prob get rid of the bad edges 
              edge = par.edge_list[j]
            #Add only sucessful ones
              if(edge.status == graph_edge_t.STATUS_SUCCESS):
                  edgeinst = Edge()
                  edgeinst.node1 = edge.node_id_1
                  edgeinst.node2 = edge.node_id_2
                  edgeinst.edge_id = edge.id
                  edgeinst.status = edge.status
                  edgeinst.type = edge.type               
               #This seems to want to keep around if an edge is new - i dont think we will need that now 
                  edgeinst.new = True
               
                  edgeinst.transformation = edge.transformation
                  edgeinst.cov = edge.cov
                  particle.edges.append(edgeinst)
         ## Doublecheck with stephanie that we dont need to add logical constraints between previous and current 
         ## supernodes
          return particle


if __name__ == "__main__":
    if(len(sys.argv) < 2):
        print "Usage : " , sys.argv[0] , " path_to_save "
        exit()

    background = lcm_listener(sys.argv[1])
    bg_thread = threading.Thread( target=background.run )
    try:
        bg_thread.daemon = True
        bg_thread.start()
        
        while True: time.sleep(100)
    except (KeyboardInterrupt, SystemExit):
        print '\n! Received keyboard interrupt, quitting threads.\n'
