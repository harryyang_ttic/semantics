import random
import sys
sys.path.append('..')
sys.path.append('/usr/lib/graphviz/python/')
sys.path.append('/usr/lib64/graphviz/python/')

# Import pygraph
from pygraph.classes.graph import graph
from pygraph.classes.digraph import digraph
from pygraph.algorithms.searching import breadth_first_search
from pygraph.readwrite.dot import write
from pygraph.algorithms.filters import find
from pygraph.algorithms.minmax import shortest_path
from numpy import array
#opens first file, reads it and gets edges/nodes

def create_graph(str): #str = name of graph text file
    gr = graph()
    nodes = []
    edges = []
    edge_dict = {}
    for line in open(str):
        if line == '\n' or line == '' or line == ' ':
            pass
        else:
            line = line.strip()
            data = line.split(',')
            node1 = data[0]
            node2 = data[1]
            cost = float(data[2])
            edges.append(((node1,node2),cost))       
    for edge in edges:
        if(edge[0][0] not in nodes):
            nodes.append(edge[0][0])
        if(edge[0][1] not in nodes):
            nodes.append(edge[0][1])
#add edges/nodes to graph
    print "Done reading nodes"
    #Need to only add edges that are not in the graph (the message contains edges for regions that go both ways

    unique_edges = []
    for edge in edges: 
        reverse_edge = ((edge[0][1], edge[0][0]), edge[1])
        #print edge
        #print reverse_edge
        #print edge not in unique_edges
        #print reverse_edge not in unique_edges
        if (edge not in unique_edges and reverse_edge not in unique_edges):
            #print "Adding edge : " , edge
            unique_edges.append(edge)

    print "Done reading edges"

    gr.add_nodes(nodes)
    for edge in unique_edges:
        #print "Edge between " , edge[0][0], " , " , edge[0][1]
        gr.add_edge(edge[0], edge[1])

    print "Done adding edges"
    return gr

def graph_analysis_single_graph(gr,N,L):
    full_result = {}
    results = []
    nodes = gr.nodes()[:]
    distances = []
    for i in range(N):
        start = nodes[random.randint(0,len(nodes) - 1)]
        possible = nodes[:]
        possible.remove(start)
        remove = []
        for node in possible:
            if(abs((int(start) - int(node))) < L):
                remove.append(node)

        for i in remove:
            if i in possible:
                possible.remove(i)

        if(i%100==0):
            print "Samples : ", i

        if(len(possible) > 0):
            end = possible[random.randint(0,len(possible) - 1)]
            search1 = shortest_path(gr,start)

            cost1 = search1[1][end]
            distances.append(cost1)
            results.append({'start': start, 'end': end, 'cost': cost1})
    

    nums = array(distances)
    print "Standrad Mean : ", nums.mean(axis=0)
    print "Standrad Deviation : ",  nums.std(axis=0)
    full_result['mean'] = nums.mean(axis=0)
    full_result['std'] = nums.std(axis=0)
    full_result['pairs'] = results
    #total1 = 0
    #for result in results:
    #    total1 += result['cost']
    #avg1 = float(total1)/N
    #print 'avg cost of graph = ' + str(avg1)
    
    return full_result

#Gets the shortest path from two random points and records it for N amount of times.
def graph_analysis(gr, gr2, N, L):
    result_g1 = graph_analysis_single_graph(gr, N, L)
    result_g2 = graph_analysis_single_graph(gr2, N, L)
    print "\n\n=======================================================\n"
    print "Graph 1 => Mean (std) : " , result_g1['mean'], "(", result_g1['std'] , ")"
    print "Graph 2 => Mean (std) : " , result_g2['mean'], "(", result_g2['std'] , ")"
    
#Gets the shortest path from two random points and records it for N amount of times.
def graph_analysis_same_pairs(gr, gr2, N, L):
    #exceptions:
    if len(gr.nodes()) != len(gr2.nodes()):
        print "Graph nodes not equal, please choose different files ", len(gr.nodes()), " , " , len(gr2.nodes())
        return

    #Set L to be the minimum distance from start/end nodes
    #Set N to be the number of iterations to get average
    results = []
    nodes = gr.nodes()[:]
    for i in range(N):
        start = nodes[random.randint(0,len(nodes) - 1)]
        possible = nodes[:]
        possible.remove(start)
        remove = []
        for node in possible:
            if(abs((int(start) - int(node))) < L):
                remove.append(node)

        for i in remove:
            if i in possible:
                possible.remove(i)

        if(len(possible) > 0):
            end = possible[random.randint(0,len(possible) - 1)]
            search1 = shortest_path(gr,start)
            search2 = shortest_path(gr2,start)

            cost1 = search1[1][end]
            cost2 = search2[1][end]
            results.append({'start': start, 'end': end, 'cost1': cost1, 'cost2': cost2})
    
    total1 = 0
    total2 = 0
    for result in results:
        total1 += result['cost1']
        total2 += result['cost2']
    avg1 = float(total1)/N
    avg2 = float(total2)/N
    print 'avg cost of graph1 = ' + str(avg1)
    print 'avg cost of graph2 = ' + str(avg2)

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print "Usage : " , sys.argv[0] , " graph1.file graph2.file [no_iterations] [no_close_nodes_to_skip]"
        exit()
    file1 = sys.argv[1]
    file2 = sys.argv[2]

    print "Loading graphs " , file1, " and " , file2
    if(len(sys.argv) >3):
        N = int(sys.argv[3])
    else:
        N = 1000
    if(len(sys.argv) > 4):
        L = int(sys.argv[4])
    else:
        L = 1
    gr1 = create_graph(file1)
    gr2 = create_graph(file2)
    graph_analysis(gr1, gr2, N, L)
    
    
