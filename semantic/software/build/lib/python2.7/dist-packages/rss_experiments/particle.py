from label_distribution import LabelDistribution
from node import Node
from edge import Edge
from slam.particle_request_t import particle_request_t
from slam.graph_edge_t import graph_edge_t
from slam.node_label_list_t import node_label_list_t
from slam.graph_particle_list_t import graph_particle_list_t
from slam.node_probability_t import node_probability_t
from slam.dirichlet_update_t import dirichlet_update_t
from slam.language_label_t import language_label_t
from slam.language_edge_t import language_edge_t

class Particle():
   def __init__(self, particle_id = 0, num = 0, weight = 1.0, par = None):
      if par == None:
         self.id = particle_id
         self.num_nodes = num
         self.nodelist = []
         self.nodelist_dict = {}
         self.edges = []
         self.weight = weight
         self.next_edge_id = 0
      else:
         self.id = particle_id
         self.num_nodes = par.num_nodes
         self.nodelist = par.nodelist[:]
         self.edges = par.edges[:]
         self.weight = 0
         if len(self.edges) > 0:
            self.next_edge_id = self.edges[len(self.edges)-1].edge_id+1
         else:
            self.next_edge_id = 0

   def children_from_topological_map(self, node):
      children = []
      for edge in self.edges:
         if edge.node1 == node.id:
            children.append(self.node_for_id(edge.node2))
         if edge.node2 == node.id:
            children.append(self.node_for_id(edge.node1))
      return children
           
   def add_edge(self, e):
      e.edge_id = self.next_edge_id
      self.next_edge_id += 1
      self.edges.append(e)
      
   def get_current_position_node(self):
      return self.nodelist[self.num_nodes-1]

   def get_current_position_xy(self):
      n = self.get_current_position_node()
      return (n.xy[0], n.xy[1])
   
   def node_for_id(self, node_id):
      for n in self.nodelist:
         if n.id == node_id:
            return n
      raise ValueError("No node for id: " + `node_id`)

   @property
   def node_id_to_child_ids(self):
      self.node_id_to_child_ids = self.make_node_id_to_child_ids_shortest_path()      
      return self.node_id_to_child_ids
   
   def populate_with_lcm_message(self, par):
       self.id = par.id
       self.num_nodes = par.no_nodes
       self.weight = par.weight
       self.nodelist = []
      #print "------------------------------------------\nParticle Id: ", par.id
       for j in range(par.no_nodes):
           node = par.node_list[j]
           nodeinst = Node()
           nodeinst.id = node.id
           nodeinst.xy = node.xy
           nodeinst.heading = node.heading
           nodeinst.pofz = node.pofz
           nodeinst.is_supernode = node.is_supernode
           nodeinst.cov = node.cov
           nodeinst.parent_supernode = node.parent_supernode
        ## Bounding box
           bb = []
           for i in range(node.no_points):
               bb.append( (node.x_coords[i], node.y_coords[i]) )
           nodeinst.bounding_box = bb
               
           ld = node.labeldist

           nodeinst.label_dist = LabelDistribution()
           nodeinst.label_dist.num_labels = ld.num_labels
           nodeinst.label_dist.total_obs = ld.total_obs
           
        #print "Label distribution size : ", len(nodeinst.label_dist.observation_frequency)
        #print "Msg Label Size : " , ld.num_labels
           
           for k in range(ld.num_labels):
               nodeinst.label_dist.observation_frequency[k] = ld.observation_frequency[k]
               
           self.nodelist.append(nodeinst)
        #The dict is prob not useful now - but we will keep it around for now 
           self.nodelist_dict[nodeinst.id] = nodeinst

       self.edges = []
       for j in range(par.no_edges):
        ## We should prob get rid of the bad edges 
           edge = par.edge_list[j]
        #Add only sucessful ones
           if(edge.status == graph_edge_t.STATUS_SUCCESS):
               edgeinst = Edge()
               edgeinst.node1 = edge.node_id_1
               edgeinst.node2 = edge.node_id_2
               edgeinst.edge_id = edge.id
               edgeinst.status = edge.status
               edgeinst.type = edge.type               
           #This seems to want to keep around if an edge is new - i dont think we will need that now 
               edgeinst.new = True
               
               edgeinst.transformation = edge.transformation
               edgeinst.cov = edge.cov
               self.edges.append(edgeinst)

       
