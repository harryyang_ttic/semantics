#!/usr/bin/python
#
# extracts GPS uncertainty data from a log file.  Specifically, the MSE
# estimates of lat, lon, elevation, and orientation

import sys

from lcm import EventLog
from erlcm.raw_odometry_msg_t import raw_odometry_msg_t

fname = sys.argv[1]
log = EventLog(fname, "r")

print >> sys.stderr, "opened %s" % fname


for e in log:
    if e.channel == "ODOMETRY":
        odom = raw_odometry_msg_t.decode (e.data)

        print "%d %f %f" % (odom.utime, odom.tv, odom.rv)
