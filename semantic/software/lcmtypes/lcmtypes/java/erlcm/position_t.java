/* LCM type definition class file
 * This file was automatically generated by lcm-gen
 * DO NOT MODIFY BY HAND!!!!
 */

package erlcm;
 
import java.io.*;
import java.util.*;
import lcm.lcm.*;
 
public final class position_t implements lcm.lcm.LCMEncodable
{
    public double x;
    public double y;
    public double z;
 
    public position_t()
    {
    }
 
    public static final long LCM_FINGERPRINT;
    public static final long LCM_FINGERPRINT_BASE = 0x573f2fdd2f76508fL;
 
    static {
        LCM_FINGERPRINT = _hashRecursive(new ArrayList<Class<?>>());
    }
 
    public static long _hashRecursive(ArrayList<Class<?>> classes)
    {
        if (classes.contains(erlcm.position_t.class))
            return 0L;
 
        classes.add(erlcm.position_t.class);
        long hash = LCM_FINGERPRINT_BASE
            ;
        classes.remove(classes.size() - 1);
        return (hash<<1) + ((hash>>63)&1);
    }
 
    public void encode(DataOutput outs) throws IOException
    {
        outs.writeLong(LCM_FINGERPRINT);
        _encodeRecursive(outs);
    }
 
    public void _encodeRecursive(DataOutput outs) throws IOException
    {
        outs.writeDouble(this.x); 
 
        outs.writeDouble(this.y); 
 
        outs.writeDouble(this.z); 
 
    }
 
    public position_t(byte[] data) throws IOException
    {
        this(new LCMDataInputStream(data));
    }
 
    public position_t(DataInput ins) throws IOException
    {
        if (ins.readLong() != LCM_FINGERPRINT)
            throw new IOException("LCM Decode error: bad fingerprint");
 
        _decodeRecursive(ins);
    }
 
    public static erlcm.position_t _decodeRecursiveFactory(DataInput ins) throws IOException
    {
        erlcm.position_t o = new erlcm.position_t();
        o._decodeRecursive(ins);
        return o;
    }
 
    public void _decodeRecursive(DataInput ins) throws IOException
    {
        this.x = ins.readDouble();
 
        this.y = ins.readDouble();
 
        this.z = ins.readDouble();
 
    }
 
    public erlcm.position_t copy()
    {
        erlcm.position_t outobj = new erlcm.position_t();
        outobj.x = this.x;
 
        outobj.y = this.y;
 
        outobj.z = this.z;
 
        return outobj;
    }
 
}

