"""LCM type definitions
This file automatically generated by lcm.
DO NOT MODIFY BY HAND!!!!
"""

try:
    import cStringIO.StringIO as BytesIO
except ImportError:
    from io import BytesIO
import struct

class slam_pose_transform_t(object):
    __slots__ = ["utime", "id", "pos", "t"]

    def __init__(self):
        self.utime = 0
        self.id = 0
        self.pos = [ 0.0 for dim0 in range(2) ]
        self.t = 0.0

    def encode(self):
        buf = BytesIO()
        buf.write(slam_pose_transform_t._get_packed_fingerprint())
        self._encode_one(buf)
        return buf.getvalue()

    def _encode_one(self, buf):
        buf.write(struct.pack(">qi", self.utime, self.id))
        buf.write(struct.pack('>2d', *self.pos[:2]))
        buf.write(struct.pack(">d", self.t))

    def decode(data):
        if hasattr(data, 'read'):
            buf = data
        else:
            buf = BytesIO(data)
        if buf.read(8) != slam_pose_transform_t._get_packed_fingerprint():
            raise ValueError("Decode error")
        return slam_pose_transform_t._decode_one(buf)
    decode = staticmethod(decode)

    def _decode_one(buf):
        self = slam_pose_transform_t()
        self.utime, self.id = struct.unpack(">qi", buf.read(12))
        self.pos = struct.unpack('>2d', buf.read(16))
        self.t = struct.unpack(">d", buf.read(8))[0]
        return self
    _decode_one = staticmethod(_decode_one)

    _hash = None
    def _get_hash_recursive(parents):
        if slam_pose_transform_t in parents: return 0
        tmphash = (0x33941f246fb4decc) & 0xffffffffffffffff
        tmphash  = (((tmphash<<1)&0xffffffffffffffff)  + (tmphash>>63)) & 0xffffffffffffffff
        return tmphash
    _get_hash_recursive = staticmethod(_get_hash_recursive)
    _packed_fingerprint = None

    def _get_packed_fingerprint():
        if slam_pose_transform_t._packed_fingerprint is None:
            slam_pose_transform_t._packed_fingerprint = struct.pack(">Q", slam_pose_transform_t._get_hash_recursive([]))
        return slam_pose_transform_t._packed_fingerprint
    _get_packed_fingerprint = staticmethod(_get_packed_fingerprint)

