# Install script for directory: /home/harry/Documents/Robotics/semantic/software/lcmtypes

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/home/harry/Documents/Robotics/semantic/software/build")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Release")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/pod-build/lib/liblcmtypes_er-lcmtypes.a")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes" TYPE FILE FILES
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/c/lcmtypes/erlcm_slam_node_init_t.h"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/c/lcmtypes/erlcm_cylinder_model_t.h"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/c/lcmtypes/erlcm_floor_gridmap_t.h"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/c/lcmtypes/erlcm_rigid_transform_t.h"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/c/lcmtypes/erlcm_raw_odometry_msg_t.h"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/c/lcmtypes/erlcm_gridmap_config_t.h"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/c/lcmtypes/erlcm_laser_feature_t.h"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/c/lcmtypes/erlcm_place_classification_debug_t.h"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/c/lcmtypes/erlcm_people_pos_msg_t.h"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/c/lcmtypes/erlcm_slam_pose_transform_t.h"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/c/lcmtypes/erlcm_cylinder_list_t.h"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/c/lcmtypes/erlcm_position_t.h"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/c/lcmtypes/erlcm_slam_pose_transform_list_t.h"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/c/lcmtypes/erlcm_slam_full_graph_t.h"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/c/lcmtypes/erlcm_multi_gridmap_t.h"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/c/lcmtypes/erlcm_pose_list_t.h"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/c/lcmtypes/erlcm_annotation_t.h"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/c/lcmtypes/erlcm_rigid_transform_list_t.h"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/c/lcmtypes/erlcm_localize_reinitialize_cmd_t.h"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/c/lcmtypes/erlcm_gridmap_t.h"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/c/lcmtypes/erlcm_person_status_t.h"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/c/lcmtypes/erlcm_slam_full_graph_node_t.h"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/c/lcmtypes/erlcm_place_classification_class_t.h"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/c/lcmtypes/erlcm_place_classification_t.h"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/c/lcmtypes/er_lcmtypes.h"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/pod-build/lib/pkgconfig/lcmtypes_er-lcmtypes.pc")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/cpp/lcmtypes/erlcm/place_classification_class_t.hpp")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/cpp/lcmtypes/erlcm/gridmap_t.hpp")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/cpp/lcmtypes/erlcm/cylinder_model_t.hpp")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/cpp/lcmtypes/erlcm/place_classification_t.hpp")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/cpp/lcmtypes/erlcm/raw_odometry_msg_t.hpp")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/cpp/lcmtypes/erlcm/pose_list_t.hpp")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/cpp/lcmtypes/erlcm/gridmap_config_t.hpp")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/cpp/lcmtypes/erlcm/slam_pose_transform_list_t.hpp")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/cpp/lcmtypes/erlcm/slam_full_graph_node_t.hpp")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/cpp/lcmtypes/erlcm/people_pos_msg_t.hpp")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/cpp/lcmtypes/erlcm/rigid_transform_list_t.hpp")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/cpp/lcmtypes/erlcm/person_status_t.hpp")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/cpp/lcmtypes/erlcm/multi_gridmap_t.hpp")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/cpp/lcmtypes/erlcm/place_classification_debug_t.hpp")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/cpp/lcmtypes/erlcm/slam_full_graph_t.hpp")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/cpp/lcmtypes/erlcm/laser_feature_t.hpp")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/cpp/lcmtypes/erlcm/position_t.hpp")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/cpp/lcmtypes/erlcm/cylinder_list_t.hpp")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/cpp/lcmtypes/erlcm/localize_reinitialize_cmd_t.hpp")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/cpp/lcmtypes/erlcm/rigid_transform_t.hpp")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/cpp/lcmtypes/erlcm/slam_node_init_t.hpp")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/cpp/lcmtypes/erlcm/floor_gridmap_t.hpp")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/cpp/lcmtypes/erlcm/slam_pose_transform_t.hpp")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/cpp/lcmtypes/erlcm/annotation_t.hpp")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/lcmtypes" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/cpp/lcmtypes/er_lcmtypes.hpp")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/java" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/pod-build/lcmtypes_er-lcmtypes.jar")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm/localize_reinitialize_cmd_t.py")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/python/erlcm/localize_reinitialize_cmd_t.py")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm/annotation_t.py")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/python/erlcm/annotation_t.py")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm/slam_pose_transform_list_t.py")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/python/erlcm/slam_pose_transform_list_t.py")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm/place_classification_class_t.py")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/python/erlcm/place_classification_class_t.py")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm/rigid_transform_t.py")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/python/erlcm/rigid_transform_t.py")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm/cylinder_model_t.py")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/python/erlcm/cylinder_model_t.py")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm/person_status_t.py")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/python/erlcm/person_status_t.py")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm/pose_list_t.py")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/python/erlcm/pose_list_t.py")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm/multi_gridmap_t.py")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/python/erlcm/multi_gridmap_t.py")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm/slam_node_init_t.py")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/python/erlcm/slam_node_init_t.py")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm/place_classification_debug_t.py")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/python/erlcm/place_classification_debug_t.py")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm/gridmap_t.py")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/python/erlcm/gridmap_t.py")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm/gridmap_config_t.py")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/python/erlcm/gridmap_config_t.py")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm/people_pos_msg_t.py")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/python/erlcm/people_pos_msg_t.py")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm/__init__.py")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/python/erlcm/__init__.py")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm/raw_odometry_msg_t.py")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/python/erlcm/raw_odometry_msg_t.py")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm/rigid_transform_list_t.py")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/python/erlcm/rigid_transform_list_t.py")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm/laser_feature_t.py")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/python/erlcm/laser_feature_t.py")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm/slam_full_graph_node_t.py")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/python/erlcm/slam_full_graph_node_t.py")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm/position_t.py")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/python/erlcm/position_t.py")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm/slam_full_graph_t.py")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/python/erlcm/slam_full_graph_t.py")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm/place_classification_t.py")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/python/erlcm/place_classification_t.py")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm/slam_pose_transform_t.py")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/python/erlcm/slam_pose_transform_t.py")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm/floor_gridmap_t.py")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/python/erlcm/floor_gridmap_t.py")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm/cylinder_list_t.py")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/harry/Documents/Robotics/semantic/software/build/lib/python2.7/dist-packages/erlcm" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/python/erlcm/cylinder_list_t.py")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/lcmtypes" TYPE FILE FILES
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/erlcm_slam_node_init_t.lcm"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/erlcm_slam_pose_transform_t.lcm"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/erlcm_annotation_t.lcm"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/erlcm_person_status_t.lcm"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/erlcm_localize_reinitialize_cmd_t.lcm"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/erlcm_place_classification_debug_t.lcm"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/erlcm_people_pos_msg_t.lcm"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/erlcm_place_classification_class_t.lcm"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/erlcm_position_t.lcm"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/erlcm_rigid_transform_t.lcm"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/erlcm_cylinder_model_t.lcm"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/erlcm_laser_feature_t.lcm"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/erlcm_pose_list_t.lcm"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/erlcm_cylinder_list_t.lcm"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/erlcm_multi_gridmap_t.lcm"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/erlcm_place_classification_t.lcm"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/erlcm_gridmap_t.lcm"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/erlcm_gridmap_config_t.lcm"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/erlcm_slam_full_graph_t.lcm"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/erlcm_raw_odometry_msg_t.lcm"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/erlcm_floor_gridmap_t.lcm"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/erlcm_slam_full_graph_node_t.lcm"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/erlcm_rigid_transform_list_t.lcm"
    "/home/harry/Documents/Robotics/semantic/software/lcmtypes/lcmtypes/erlcm_slam_pose_transform_list_t.lcm"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/lcmtypes/pod-build/lib/pkgconfig/lcmtypes_er-lcmtypes.pc")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(CMAKE_INSTALL_COMPONENT)
  SET(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
ELSE(CMAKE_INSTALL_COMPONENT)
  SET(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
ENDIF(CMAKE_INSTALL_COMPONENT)

FILE(WRITE "/home/harry/Documents/Robotics/semantic/software/lcmtypes/pod-build/${CMAKE_INSTALL_MANIFEST}" "")
FOREACH(file ${CMAKE_INSTALL_MANIFEST_FILES})
  FILE(APPEND "/home/harry/Documents/Robotics/semantic/software/lcmtypes/pod-build/${CMAKE_INSTALL_MANIFEST}" "${file}\n")
ENDFOREACH(file)
