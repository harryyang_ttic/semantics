FILE(REMOVE_RECURSE
  "../lcmtypes/c"
  "../lcmtypes/cpp"
  "../lcmtypes/java"
  "../lcmtypes/python"
  "CMakeFiles/lcmtypes_er-lcmtypes_jar"
  "lcmtypes_er-lcmtypes.jar"
  "../lcmtypes/java/erlcm/slam_pose_transform_t.class"
  "../lcmtypes/java/erlcm/position_t.class"
  "../lcmtypes/java/erlcm/laser_feature_t.class"
  "../lcmtypes/java/erlcm/multi_gridmap_t.class"
  "../lcmtypes/java/erlcm/rigid_transform_list_t.class"
  "../lcmtypes/java/erlcm/slam_full_graph_node_t.class"
  "../lcmtypes/java/erlcm/gridmap_config_t.class"
  "../lcmtypes/java/erlcm/place_classification_class_t.class"
  "../lcmtypes/java/erlcm/person_status_t.class"
  "../lcmtypes/java/erlcm/place_classification_t.class"
  "../lcmtypes/java/erlcm/slam_node_init_t.class"
  "../lcmtypes/java/erlcm/annotation_t.class"
  "../lcmtypes/java/erlcm/cylinder_model_t.class"
  "../lcmtypes/java/erlcm/pose_list_t.class"
  "../lcmtypes/java/erlcm/people_pos_msg_t.class"
  "../lcmtypes/java/erlcm/rigid_transform_t.class"
  "../lcmtypes/java/erlcm/floor_gridmap_t.class"
  "../lcmtypes/java/erlcm/localize_reinitialize_cmd_t.class"
  "../lcmtypes/java/erlcm/slam_full_graph_t.class"
  "../lcmtypes/java/erlcm/gridmap_t.class"
  "../lcmtypes/java/erlcm/raw_odometry_msg_t.class"
  "../lcmtypes/java/erlcm/slam_pose_transform_list_t.class"
  "../lcmtypes/java/erlcm/place_classification_debug_t.class"
  "../lcmtypes/java/erlcm/cylinder_list_t.class"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/lcmtypes_er-lcmtypes_jar.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
