#ifndef __lcmtypes_er_lcmtypes_h__
#define __lcmtypes_er_lcmtypes_h__

#include "erlcm_slam_node_init_t.h"
#include "erlcm_cylinder_model_t.h"
#include "erlcm_floor_gridmap_t.h"
#include "erlcm_rigid_transform_t.h"
#include "erlcm_raw_odometry_msg_t.h"
#include "erlcm_gridmap_config_t.h"
#include "erlcm_laser_feature_t.h"
#include "erlcm_place_classification_debug_t.h"
#include "erlcm_people_pos_msg_t.h"
#include "erlcm_slam_pose_transform_t.h"
#include "erlcm_cylinder_list_t.h"
#include "erlcm_position_t.h"
#include "erlcm_slam_pose_transform_list_t.h"
#include "erlcm_slam_full_graph_t.h"
#include "erlcm_multi_gridmap_t.h"
#include "erlcm_pose_list_t.h"
#include "erlcm_annotation_t.h"
#include "erlcm_rigid_transform_list_t.h"
#include "erlcm_localize_reinitialize_cmd_t.h"
#include "erlcm_gridmap_t.h"
#include "erlcm_person_status_t.h"
#include "erlcm_slam_full_graph_node_t.h"
#include "erlcm_place_classification_class_t.h"
#include "erlcm_place_classification_t.h"

#endif
