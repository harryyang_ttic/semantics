# Install script for directory: /home/harry/Documents/Robotics/semantic/software/externals/libcrfh/src

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/home/harry/Documents/Robotics/semantic/software/build")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "RELEASE")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  IF(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/crfh-extract" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/crfh-extract")
    FILE(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/crfh-extract"
         RPATH "/home/harry/Documents/Robotics/semantic/software/build/lib")
  ENDIF()
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/home/harry/Documents/Robotics/semantic/software/externals/libcrfh/pod-build/bin/crfh-extract")
  IF(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/crfh-extract" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/crfh-extract")
    FILE(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/crfh-extract"
         OLD_RPATH "/home/harry/Documents/Robotics/semantic/software/externals/libcrfh/pod-build/lib:/home/harry/Documents/Robotics/semantic/software/build/lib:"
         NEW_RPATH "/home/harry/Documents/Robotics/semantic/software/build/lib")
    IF(CMAKE_INSTALL_DO_STRIP)
      EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/crfh-extract")
    ENDIF(CMAKE_INSTALL_DO_STRIP)
  ENDIF()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  IF(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libcrfh.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libcrfh.so")
    FILE(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libcrfh.so"
         RPATH "/home/harry/Documents/Robotics/semantic/software/build/lib")
  ENDIF()
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES "/home/harry/Documents/Robotics/semantic/software/externals/libcrfh/pod-build/lib/libcrfh.so")
  IF(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libcrfh.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libcrfh.so")
    FILE(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libcrfh.so"
         OLD_RPATH "/home/harry/Documents/Robotics/semantic/software/externals/libcrfh/pod-build/lib:/home/harry/Documents/Robotics/semantic/software/build/lib:"
         NEW_RPATH "/home/harry/Documents/Robotics/semantic/software/build/lib")
    IF(CMAKE_INSTALL_DO_STRIP)
      EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libcrfh.so")
    ENDIF(CMAKE_INSTALL_DO_STRIP)
  ENDIF()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE FILE FILES
    "/home/harry/Documents/Robotics/semantic/software/externals/libcrfh/src/src/libCRFH.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libcrfh/src/src/CImage.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libcrfh/src/src/global.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libcrfh/src/src/CMatrix.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libcrfh/src/src/CFilter.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libcrfh/src/src/CFilterCache.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libcrfh/src/src/CPerformance.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libcrfh/src/src/CChannelCache.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libcrfh/src/src/CSystem.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libcrfh/src/src/CSparseVector.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libcrfh/src/src/CDescriptorList.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libcrfh/src/src/CDescriptor.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libcrfh/src/src/CScaleSpaceCache.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libcrfh/src/src/CCrfh.h"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/doc" TYPE FILE FILES
    "/home/harry/Documents/Robotics/semantic/software/externals/libcrfh/src/doc/CHANGELOG"
    "/home/harry/Documents/Robotics/semantic/software/externals/libcrfh/src/doc/LICENSE"
    "/home/harry/Documents/Robotics/semantic/software/externals/libcrfh/src/doc/README"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

