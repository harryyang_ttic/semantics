# CMake generated Testfile for 
# Source directory: /home/harry/Documents/Robotics/semantic/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3
# Build directory: /home/harry/Documents/Robotics/semantic/software/externals/Eigen_pod/pod-build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(Eigen)
SUBDIRS(doc)
SUBDIRS(test)
SUBDIRS(blas)
SUBDIRS(lapack)
SUBDIRS(unsupported)
SUBDIRS(demos)
SUBDIRS(scripts)
SUBDIRS(bench/spbench)
