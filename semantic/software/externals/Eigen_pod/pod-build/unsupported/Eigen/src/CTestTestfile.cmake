# CMake generated Testfile for 
# Source directory: /home/harry/Documents/Robotics/semantic/software/externals/Eigen_pod/eigen-eigen-ca142d0540d3/unsupported/Eigen/src
# Build directory: /home/harry/Documents/Robotics/semantic/software/externals/Eigen_pod/pod-build/unsupported/Eigen/src
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(AutoDiff)
SUBDIRS(BVH)
SUBDIRS(FFT)
SUBDIRS(IterativeSolvers)
SUBDIRS(MatrixFunctions)
SUBDIRS(MoreVectorization)
SUBDIRS(NonLinearOptimization)
SUBDIRS(NumericalDiff)
SUBDIRS(Polynomials)
SUBDIRS(Skyline)
SUBDIRS(SparseExtra)
SUBDIRS(KroneckerProduct)
SUBDIRS(Splines)
