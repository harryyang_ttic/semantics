#ifndef VOXELMAP_H_
#define VOXELMAP_H_

//#include <common3d/agile-math_util.h>
//#include <lcmtypes/wheelchair3d_lcmtypes.h>
//#include <common3d_utils/math_util.h>

#include <bot_core/bot_core.h>
#include <lcmtypes/er_lcmtypes.h>

using namespace std;

class VoxelMapIndexer {
public:
  //metadata
  double xyz0[3], xyz1[3];
  double metersPerPixel[3];
  int dimensions[3];

protected:
  //get linear index into storage arrays
  inline int getInd(int ixyz[3]) const
  {
    return ixyz[2] * (dimensions[0] * dimensions[1]) + ixyz[1] * dimensions[0] + ixyz[0];
  }

  static inline int iclamp(int v, int minv, int maxv)
  {
    return bot_max(minv, bot_min(v, maxv));
  }

  
public:
  inline bool worldToTable(double xyz[3], int ixyz[3]) const
  {
    bool clamped = false;
    for (int i = 0; i < 3; i++) {
      ixyz[i] = round((xyz[i] - xyz0[i]) / metersPerPixel[i]);
      if (ixyz[i] <= 0 || ixyz[i] >= dimensions[i] - 1) {
        ixyz[i] = iclamp(ixyz[i], 0, dimensions[i] - 1);
        clamped = true;
      }
    }
    return clamped;
  }

  inline void tableToWorld(int ixyz[3], double * xyz) const
  {
    for (int i = 0; i < 3; i++)
      xyz[i] = ((double) ixyz[i]) * metersPerPixel[i] + xyz0[i];
  }
};

class VoxelMap: public VoxelMapIndexer {
public:
  VoxelMap(double _xyz0[3], double _xyz1[3], double _metersPerPixel[3], uint8_t _maxEvidence=64);
  virtual ~VoxelMap();
  void reset();
  erlcm_voxel_map_t * get_voxel_map_t(int blur = 0);

  inline float readValue(int ixyz[3]) const
  {
    int ind = getInd(ixyz);
    if (!visits[ind])
      return 0.5;
    else
      return (float) hits[ind] / (float) visits[ind];
  }
  inline float readValue(double xyz[3]) const
  {
    int ixyz[3];
    worldToTable(xyz, ixyz);
    return readValue(ixyz);
  }
  inline void updateValue(int ixyz[3], int hit, int visit = 1, bool updateBorder = true)
  {
    if (!updateBorder) {
      for (int i = 0; i < 3; i++) {
        if (ixyz[i] <= 0 || ixyz[i] >= dimensions[i] - 1)
          return;
      }
    }
    int ind = getInd(ixyz);
    //TODO: NEED to figure out what to do with capping if hit/visit isn't 1
    if (visits[ind] < maxEvidence) {
      visits[ind] += visit;
      hits[ind] += hit;
    }
    else if (hit > 0 && hits[ind] < maxEvidence)
      hits[ind] += hit;
    else if (hit == 0 && hits[ind] > 0)
      hits[ind] -= visit;
  }
  inline void updateValue(double xyz[3], int hit, int visit = 1, bool updateBorder = true)
  {
    int ixyz[3];
    int border = worldToTable(xyz, ixyz);
    if (!border || updateBorder)
      updateValue(ixyz, hit);
  }

  inline int readVisits(int ixyz[3])
  {
    return visits[getInd(ixyz)];
  }

  void raytrace(int origin[3], int endpoint[3], int hit_inc = 1, int visit_inc = 1, bool updateBorder = true);
  void raytrace(double origin[3], double endpoint[3], int hit_inc = 1, int visit_inc = 1, bool updateBorder = true);
  const float * getFloatMap() const;
  const float * getBlurredFloatMap(int blur = 0) const;
  inline int getNumCells()
  {
    return num_cells;
  }
  //storage arrays
private:
  int num_cells;
  uint8_t * hits;
  uint8_t * visits;
  float * likelihoods;
  uint8_t maxEvidence; //cap evidence to prevent overflow, and adapt faster
  //voxel_map message for publishing
  erlcm_voxel_map_t * vox_map_msg;

};

class VoxelMapReader: public VoxelMapIndexer {
public:
  virtual ~VoxelMapReader();
  VoxelMapReader(const erlcm_voxel_map_t * msg);
  VoxelMapReader(const VoxelMap * voxmap);
  void set(const VoxelMap * voxmap, int blur = 0);
  erlcm_voxel_map_t * get_voxel_map_t();

  inline float readValue(int ixyz[3]) const
  {
    return likelihoods[getInd(ixyz)];
  }
  inline float readValue(double xyz[3]) const
  {
    int ixyz[3];
    worldToTable(xyz, ixyz);
    return readValue(ixyz);
  }

  inline const float * getFloatMap() const
  {
    return likelihoods;
  }

  //storage arrays
private:
  int num_cells;
  float * likelihoods;
  erlcm_voxel_map_t * vox_map_msg;
};

#endif /*VOXELMAP_H_*/
