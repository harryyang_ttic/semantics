FILE(REMOVE_RECURSE
  "../lcmtypes/c"
  "../lcmtypes/cpp"
  "../lcmtypes/java"
  "../lcmtypes/python"
  "CMakeFiles/lcmtypes_octomap-utils_jar"
  "lcmtypes_octomap-utils.jar"
  "../lcmtypes/java/octomap/node_t.class"
  "../lcmtypes/java/octomap/raw_t.class"
  "../lcmtypes/java/octomap/file_t.class"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/lcmtypes_octomap-utils_jar.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
