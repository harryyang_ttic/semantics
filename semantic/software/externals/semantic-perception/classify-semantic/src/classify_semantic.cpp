#include "classify_semantic.hpp"
#include <er_common/path_util.h>
#include <bot_param/param_client.h>
//#include <lcmtypes/bot2_param.h>

using namespace std;


const char *s1 = "doorway";
const char *s2 = "conference_room";
const char *s3 = "office";
const char *s4 = "lab";
const char *s5 = "open_area";
const char *s6 = "hallway";
const char *s7 = "elevator";
const char *s8 = "corridor";
const char *s9 = "large_meeting_room";
const char *s10 = "classroom";


/*string s1 = "doorway";
  string s2 = "conference_room";
  string s3 = "office";
  string s4 = "lab";
  string s5 = "open_area";
  string s6 = "hallway";
  string s7 = "elevator";
  string s8 = "corridor";
  string s9 = "large_meeting_room";
  string s10 = "classroom";
*/
int get_id_from_name(char *annotation){
    if(!strncmp(annotation, s1, 100)){
	return PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_DOORWAY;//1;
    }
    else if(!strncmp(annotation, s2, 100)){
	return PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_CONFERENCE_ROOM;
    }
    else if(!strncmp(annotation, s3, 100)){
	return PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_OFFICE;
    }
    else if(!strncmp(annotation, s4, 100)){
	return PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_LAB;
    }
    else if(!strncmp(annotation, s5, 100)){
	return PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_OPEN_AREA;
    }
    else if(!strncmp(annotation, s6, 100)){
	return PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_HALLWAY;
    }
    else if(!strncmp(annotation, s7, 100)){
	return PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_ELEVATOR;
    }
    else if(!strncmp(annotation, s8, 100)){
	return PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_CORRIDOR;
    }
    else if(!strncmp(annotation, s9, 100)){
	return PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_LARGE_MEETING_ROOM;
    }
    else if(!strncmp(annotation, s10, 100)){
        return PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_CLASSROOM;
    }
    else{
        fprintf(stderr, "Error - Unknown Class : %s\n", annotation);
        return -1;
    }    
}

void classification_result_t_destroy(classification_result_t *p){
    if(p->classes)
        free(p->classes);
    free(p);
}

classification_result_t *classification_result_t_copy(classification_result_t *p){
    if(p){
        classification_result_t *result = (classification_result_t *) calloc(1, sizeof(classification_result_t));
        if(p->classes){
            result->count = p->count;
            result->classes = (semantic_class_t *) calloc(result->count, sizeof(semantic_class_t));
            memcpy(result->classes, p->classes, result->count * sizeof(semantic_class_t));
        }
        return result;
    }
    return NULL;
}



void print_classification_result_t(classification_result_t *p){
    if(p){
        for(int i=0; i < p->count; i++){
            fprintf(stderr, "[%d] %s - Prob : %.3f\n", p->classes[i].type, get_class_name_from_id(p->classes[i].type) ,p->classes[i].prob);
        }
    }
}

char* get_class_name_from_id(int class_id){
    char *class_name = NULL;
    if(class_id == PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_DOORWAY){
        class_name = "Doorway";
    }
    else if(class_id == PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_CONFERENCE_ROOM){
        class_name = "Conference Room";
    }
    else if(class_id == PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_OFFICE){
        class_name = "Office";
    }
    else if(class_id == PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_LAB){
        class_name = "Lab";
    }
    else if(class_id == PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_OPEN_AREA){
        class_name = "Open Area";
    }
    else if(class_id == PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_HALLWAY){
        class_name = "Hallway";
    }
    else if(class_id == PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_ELEVATOR){
        class_name = "Elevator";
    }
    else if(class_id == PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_CORRIDOR){
        class_name = "Corridor"; //what is the difference here??
    }
    else if(class_id == PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_LARGE_MEETING_ROOM){
        class_name = "Large Meeting Room";
    }
    else if(class_id = PERCEPTION_PLACE_CLASSIFICATION_CLASS_T_CLASSROOM){
	class_name = "Classroom";
    }
    else{        
        class_name = "Unknown";
    }
    return class_name;
}


//Check whether classifier is confident with the classification
int check_classification(double *values, int no_class){
    for(int i = 0; i < no_class;i++){
        if(values[i] > THRESHOLD)
            return 1;
    }
    return 0;
}

static void laser_destroy(void *user, void *p)
{
    if (p) {
        bot_core_planar_lidar_t *laser = (bot_core_planar_lidar_t*) p;
        bot_core_planar_lidar_t_destroy(laser);
    }
}

static void image_destroy(void *user, void *p)
{
    if (p) {
        bot_core_image_t *img = (bot_core_image_t*) p;
        bot_core_image_t_destroy(img);
    }
}

classification_result_t * classify_image(semantic_classifier_t *s, bot_core_image_t *img){
    classification_result_t *result = (classification_result_t *) calloc(1, sizeof(classification_result_t));
    const bot_core_image_t *image = img;

    //features look ok - is it the model??
    CSvmNode *feature_values = calculate_image_features(s->img_classifier.extractor, image);//calculate_image_features(image, s);
    svm_node_t *test_pt;
    int no_features = 0;
    while(1){
        if(feature_values[no_features].index == -1)
            break;
        no_features++;
    }

    //fprintf(stderr, "Image => No of features : %d\n", no_features);
    test_pt = (svm_node_t *) calloc(no_features + 1, sizeof(svm_node_t));
    for(int i = 0; i < no_features; i++){
        test_pt[i].index = feature_values[i].index;
        test_pt[i].value = feature_values[i].value;
    }

    test_pt[no_features].index = -1;
    
#ifdef v
    int i=0;
    while(1){
        if(feature_values[i].index == -1)
            break;
        fprintf(stderr, "\t[%d] : %f\n", feature_values[i].index , feature_values[i].value);
        i++;
    }
#endif
    test_pt[no_features].index = -1;

    //predict label
    int label = (int)svm_predict(s->img_classifier.model, test_pt);
    //fprintf(stderr, "Predicted Class ID : %d\n", label);

    double probability[s->img_classifier.no_class];
    if(svm_check_probability_model(s->img_classifier.model) == 0){
        fprintf(stderr, "Model does not have probability information.\n");
        for(int i = 0;i < s->img_classifier.no_class;i++)
            probability[i] = 0;
    }

    double value = svm_predict_probability(s->img_classifier.model, test_pt, probability);
    free(test_pt);

    char* class_name = get_class_name_from_id(label);

    //fprintf(stderr, "%s\n", class_name);
    //    fprintf(stderr, "Prob \n");
    
    result->count = s->img_classifier.no_class;
    result->classes = (semantic_class_t *) calloc(result->count, sizeof(semantic_class_t));

    for(int i=0; i < s->img_classifier.no_class; i++){
        result->classes[i].type =  s->img_classifier.class_labels[i];
        result->classes[i].prob = probability[i];
        //fprintf(stderr, "[%d] - %1.3f ", s->img_classifier.class_labels[i], probability[i]);
    }
    if(s->publish){
        perception_place_classification_t msg;
        msg.utime = image->utime;
        msg.label = get_class_name_from_id(label);
        msg.pose_utime = image->utime;
        msg.sensor_utime = image->utime;
        
        msg.no_class = s->img_classifier.no_class;
        msg.classes = (perception_place_classification_class_t *) calloc(msg.no_class, sizeof(perception_place_classification_class_t));
        
        for(int i=0; i < s->img_classifier.no_class; i++){
            msg.classes[i].name = s->img_classifier.class_labels[i];
            msg.classes[i].probability = probability[i];
        }
        perception_place_classification_t_publish(s->lcm, "IMAGE_PLACE_CLASSIFICATION", &msg);
    }
    //#endif

    //int obs_index = get_index_from_id(label, s);
    
    delete feature_values; 
    return result;
}

classification_result_t * classify_laser_scan(semantic_classifier_t *s, bot_core_planar_lidar_t *laser){
    classification_result_t *result = (classification_result_t *) calloc(1, sizeof(classification_result_t));
    int no_features = s->laser_classifier.config.no_of_fourier_coefficient * 4 + 38;
  
    svm_node_t *test_pt = calculate_svm_features(laser, s->laser_classifier.config);
    //predict label
    int label = (int)svm_predict(s->laser_classifier.model, test_pt);
           
    double probability[s->laser_classifier.no_class];
    if(svm_check_probability_model(s->laser_classifier.model) == 0){
        fprintf(stderr, "Model does not have probability information.\n");
        for(int i = 0;i < s->laser_classifier.no_class;i++)
            probability[i] = 0;
    }

    double value = svm_predict_probability(s->laser_classifier.model, test_pt, probability);
    free(test_pt);
    
    char* class_name = get_class_name_from_id(label);

    //fprintf(stderr, "\nPredicted : %s - ID : %d\n", class_name, label);    
    //    fprintf(stderr, "Prob \n");

    result->count = s->laser_classifier.no_class;
    result->classes = (semantic_class_t *) calloc(result->count, sizeof(semantic_class_t));

    //this is messing thins=gs up 
    for(int i=0; i < s->laser_classifier.no_class; i++){
        //fprintf(stderr, "[%d] - %.3f\n", s->laser_classifier.class_labels[i], probability[i]);
        result->classes[i].type =  s->laser_classifier.class_labels[i];
        result->classes[i].prob = probability[i];
    }

    if(s->publish){
        perception_place_classification_t msg;
        msg.utime = laser->utime;
        msg.label = get_class_name_from_id(label);
        msg.pose_utime = laser->utime;//last_utime;
        msg.sensor_utime = laser->utime;//last_utime;
        msg.sensor_type = PERCEPTION_PLACE_CLASSIFICATION_T_SENSOR_TYPE_SIMULATE_360_SKIRT;

        msg.no_class = s->laser_classifier.no_class;
        msg.classes = (perception_place_classification_class_t *) calloc(msg.no_class, sizeof(perception_place_classification_class_t));
        
        for(int i=0; i < s->laser_classifier.no_class; i++){
            msg.classes[i].name = s->laser_classifier.class_labels[i];
            msg.classes[i].probability = probability[i];
        }
        
        perception_place_classification_t_publish(s->lcm, "LASER_PLACE_CLASSIFICATION", &msg);
    }
    return result;
}

void publish_classification(semantic_classifier_t *s, bot_core_planar_lidar_t *laser, bot_core_image_t *image){
    if(image != NULL){
        //classify the image 
        classify_image(s, image);        
    }
    if(laser != NULL){
        //classify laser 
        classify_laser_scan(s, laser);
    }
}

classification_result_t *get_classification_laser(semantic_classifier_t *s, bot_core_planar_lidar_t *laser){

    if(laser != NULL){
        //classify laser 
        return classify_laser_scan(s, laser);
    }
    return NULL;
}

classification_result_t *get_classification_image(semantic_classifier_t *s, bot_core_image_t *image){

    if(image != NULL){
        //classify image 
        return classify_image(s, image);
    }
    return NULL;
}

void get_classification(semantic_classifier_t *s, bot_core_planar_lidar_t *laser, bot_core_image_t *image,
                        classification_result_t &laser_result, classification_result_t &image_result){
    if(image != NULL){
        //classify the image 
        classification_result_t *result = classify_image(s, image);        
        classification_result_t_destroy(result);
    }
    if(laser != NULL){
        //classify laser 
        classification_result_t *result = classify_laser_scan(s, laser);
        classification_result_t_destroy(result);
    }
}


void get_classification(semantic_classifier_t *s, int64_t utime){
    bot_core_planar_lidar_t *laser = NULL;
    bot_core_image_t *image = NULL;
    //the lock needs to be there because the lcm thread might not be the one that calls this classify function 
    g_mutex_lock (s->mutex);
    
    double image_time_gap = 5.0;
    for(int i = 0; i < bot_ptr_circular_size(s->image_history); i++){
        bot_core_image_t *c_img = (bot_core_image_t *) bot_ptr_circular_index(s->image_history, i);
        if(fabs((c_img->utime - utime)/1.0e6) < image_time_gap){ 
            image_time_gap = (c_img->utime - utime)/1.0e6;
            image = c_img;
        }
    } 
    if(image != NULL){
        image = bot_core_image_t_copy(image);
        fprintf(stderr, "Found Image at time gap : %f\n", image_time_gap);
    }
    else{
        g_mutex_unlock (s->mutex);
        return;
    }

    double laser_time_gap = 5.0;

    for(int i=0; i < bot_ptr_circular_size(s->laser_history); i++){
        bot_core_planar_lidar_t *c_laser = (bot_core_planar_lidar_t *) bot_ptr_circular_index(s->laser_history, i);
        if(fabs((c_laser->utime - utime)/1.0e6) < laser_time_gap){
            laser = c_laser;
            laser_time_gap = (c_laser->utime - utime)/1.0e6;            
        }
    }
    if(laser != NULL){
        laser = bot_core_planar_lidar_t_copy(laser);
    }
    else{
        g_mutex_unlock (s->mutex);
        return;
    }
              
    g_mutex_unlock (s->mutex);
        
    if(image != NULL){
        //classify the image 
        classification_result_t *result = classify_image(s, image);
        classification_result_t_destroy(result);
        bot_core_image_t_destroy(image);
    }
    if(laser != NULL){
        //classify laser 
        classification_result_t *result = classify_laser_scan(s, laser);
        classification_result_t_destroy(result);
        bot_core_planar_lidar_t_destroy(laser);
    }
}

void publish_classification_latest(semantic_classifier_t *s){
    bot_core_planar_lidar_t *laser = NULL;
    bot_core_image_t *image = NULL;
    //the lock needs to be there because the lcm thread might not be the one that calls this classify function 

    g_mutex_lock (s->mutex);
    if(bot_ptr_circular_size(s->image_history)>0){
        image = (bot_core_image_t *) bot_ptr_circular_index(s->image_history, 0);
    } 

    int64_t utime = bot_timestamp_now();
    if(image != NULL){
        image = bot_core_image_t_copy(image);
        utime = image->utime;
    }
    else{
        g_mutex_unlock (s->mutex);
        return;
    }

    fprintf(stderr, "Checking Lasers\n");
    double laser_time_gap = 5.0;

    for(int i=0; i < bot_ptr_circular_size(s->laser_history); i++){
        bot_core_planar_lidar_t *c_laser = (bot_core_planar_lidar_t *) bot_ptr_circular_index(s->laser_history, i);
        if(fabs((c_laser->utime - utime)/1.0e6) < laser_time_gap){
            laser = c_laser;
            laser_time_gap = (c_laser->utime - utime)/1.0e6;            
        }
    }
    if(laser != NULL){
        laser = bot_core_planar_lidar_t_copy(laser);
    }
    else{
        g_mutex_unlock (s->mutex);
        return; 
    }
              
    g_mutex_unlock (s->mutex);
        
    if(image != NULL){
        //classify the image 
        classification_result_t *result = classify_image(s, image);
        bot_core_image_t_destroy(image);
        classification_result_t_destroy(result);
    }
    if(laser != NULL){
        //classify laser 
        classification_result_t *result = classify_laser_scan(s, laser);
        bot_core_planar_lidar_t_destroy(laser);
        classification_result_t_destroy(result);
    }
}

static void
image_handler(const lcm_recv_buf_t *rbuf, const char *channel,
              const bot_core_image_t *msg, void *user)
{
    semantic_classifier_t *s = (semantic_classifier_t *)user;
    fprintf(stderr, "I\n");
    g_mutex_lock (s->mutex);
    bot_ptr_circular_add(s->image_history, bot_core_image_t_copy(msg));
    g_mutex_unlock (s->mutex); 
}

static void
full_laser_update_handler(int64_t utime, bot_core_planar_lidar_t *msg, void *user)
{
    semantic_classifier_t *s = (semantic_classifier_t *)user;
    fprintf(stderr, ".");
    g_mutex_lock (s->mutex);
    bot_ptr_circular_add(s->laser_history, bot_core_planar_lidar_t_copy(msg));
    g_mutex_unlock (s->mutex); 
}

int load_models(semantic_classifier_t *state, char *laser_model_path, char *image_model_path, bool handle_messages){
    if(laser_model_path != NULL){
        state->laser_classifier.model = svm_load_model(laser_model_path);
        if(state->laser_classifier.model == NULL){
            fprintf(stderr, "Error loading laser model at : %s\n", laser_model_path);
            free(state);
            return -1;
        }
        else{
            if(handle_messages){
                fprintf(stderr, "Model Classifier - handling messages\n");
                state->full_laser = full_laser_init(state->lcm, 360, &full_laser_update_handler, state);
            }
            fprintf(stderr, "Model loaded success.\n");

            state->laser_classifier.no_class = svm_get_nr_class(state->laser_classifier.model);
            state->laser_classifier.class_labels = (int *) calloc(state->laser_classifier.no_class, sizeof(int));
            state->laser_classifier.class_counts = (int **) calloc(state->laser_classifier.no_class, sizeof(int *));

            state->laser_classifier.config.min_range = 0.1;
            state->laser_classifier.config.max_range = 30;
            state->laser_classifier.config.cutoff = 5;
            state->laser_classifier.config.gap_threshold_1 = 3;
            state->laser_classifier.config.gap_threshold_2 = 0.5;
            state->laser_classifier.config.deviation = 0.001;
            state->laser_classifier.config.no_of_fourier_coefficient = 2;
            state->laser_classifier.config.no_beam_skip = 3;
            svm_get_labels(state->laser_classifier.model, state->laser_classifier.class_labels);

            fprintf(stderr, "Loaded Laser Classes : %d\n", state->laser_classifier.no_class);
            for(int i=0; i < state->laser_classifier.no_class; i++){
                fprintf(stderr, "Class ID : %d - Label : %s\n", state->laser_classifier.class_labels[i], get_class_name_from_id(state->laser_classifier.class_labels[i]));
            }
            state->laser_classifier.last_processed = 0;
        }
    }

    if(image_model_path != NULL){
        state->img_classifier.model = svm_load_model(image_model_path);
        if(state->img_classifier.model == NULL){
            fprintf(stderr, "Error loading image model at : %s\n", image_model_path);
            //actually we should free the loaded laser model as well 
            free(state);
            return -1;
        }
        if(handle_messages){
            fprintf(stderr, "Model Classifier - handling messages\n");
        }
        bot_core_image_t_subscribe(state->lcm,"DRAGONFLY_IMAGE", image_handler, state);
        state->img_classifier.extractor = create_image_feature_extractor();
        state->img_classifier.no_class = svm_get_nr_class(state->img_classifier.model);
        state->img_classifier.class_labels = (int *) calloc(state->img_classifier.no_class, sizeof(int));
        state->img_classifier.class_counts = (int **) calloc(state->img_classifier.no_class, sizeof(int *));
        svm_get_labels(state->img_classifier.model, state->img_classifier.class_labels);
        state->img_classifier.last_processed = 0;

        fprintf(stderr, "\nLoaded Image Classes : %d\n", state->img_classifier.no_class);
        for(int i=0; i < state->img_classifier.no_class; i++){
            fprintf(stderr, "Class ID : %d - Label : %s\n", state->img_classifier.class_labels[i], get_class_name_from_id(state->img_classifier.class_labels[i]));
        }
    }

    state->laser_history = bot_ptr_circular_new(LASER_BUFFER_SIZE, laser_destroy, NULL);
    state->image_history = bot_ptr_circular_new(IMAGE_BUFFER_SIZE, image_destroy, NULL);
    fprintf(stderr, "Done loading classifier\n");
    return 0;
}

//create this from reading the params in the param server 
semantic_classifier_t *semantic_classifier_create(bool publish, bool handle_messages){
    semantic_classifier_t *state = (semantic_classifier_t *) calloc (1, sizeof(semantic_classifier_t)); 

    const char *data_path = getDataPath();
    
    state->mutex =  g_mutex_new();
    state->publish = publish;

    //hmm this is calling get global
    state->lcm = NULL;
    if(handle_messages){
        state->lcm = bot_lcm_get_global(NULL);
        bot_glib_mainloop_attach_lcm (state->lcm);
    }
    else{
        fprintf(stdout, "Semantic Perception - not handling lcm messages\n");
        state->lcm = lcm_create(NULL);
    }
    
    BotParam *param = bot_param_new_from_server(state->lcm, 0);
    
    //return state;
    
    char key[200];

    sprintf(key,"semantic_classification.image.model_path");
       
    char *im_path;
    if (0 != bot_param_get_str(param, key, &im_path)){
        fprintf(stderr, "Image model not defined : %s\n", key);
        free(state);
        return NULL;
    }
    char image_model_path[1000];
    sprintf(image_model_path, "%s/%s", data_path, im_path);
    free(im_path);

    fprintf(stderr, "Image Path : %s\n", image_model_path);

    sprintf(key,"semantic_classification.laser.model_path");
       
    char *laser_path;
    if (0 != bot_param_get_str(param, key, &laser_path)){
        fprintf(stderr, "Image model not defined : %s\n", key);
        free(state);
        return NULL;
    }

    char laser_model_path[1000];
    sprintf(laser_model_path, "%s/%s", data_path, laser_path);
    free(laser_path);
    
    fprintf(stderr, "Laser Path : %s\n", laser_model_path);
    
    int status = load_models(state, laser_model_path, image_model_path, handle_messages);
    
    return state;
}



semantic_classifier_t *semantic_classifier_create(char *laser_model_path, char *image_model_path, bool publish, bool handle_messages){
    semantic_classifier_t *state = (semantic_classifier_t *) calloc (1, sizeof(semantic_classifier_t)); 
    
    state->mutex =  g_mutex_new();
    state->publish = publish;
    if(handle_messages){
        state->lcm = bot_lcm_get_global(NULL);
        bot_glib_mainloop_attach_lcm (state->lcm);
    }
    else{
        fprintf(stdout, "Semantic Perception - not handling lcm messages\n");
        state->lcm = lcm_create(NULL);
    }

    int status = load_models(state, laser_model_path, image_model_path, handle_messages);
    return state;
}
