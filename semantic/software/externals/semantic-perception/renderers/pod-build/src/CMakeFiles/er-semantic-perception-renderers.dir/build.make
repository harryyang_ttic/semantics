# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/harry/Documents/Robotics/semantic/software/externals/semantic-perception/renderers

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/harry/Documents/Robotics/semantic/software/externals/semantic-perception/renderers/pod-build

# Include any dependencies generated for this target.
include src/CMakeFiles/er-semantic-perception-renderers.dir/depend.make

# Include the progress variables for this target.
include src/CMakeFiles/er-semantic-perception-renderers.dir/progress.make

# Include the compile flags for this target's objects.
include src/CMakeFiles/er-semantic-perception-renderers.dir/flags.make

src/CMakeFiles/er-semantic-perception-renderers.dir/renderer_log_annotation.c.o: src/CMakeFiles/er-semantic-perception-renderers.dir/flags.make
src/CMakeFiles/er-semantic-perception-renderers.dir/renderer_log_annotation.c.o: ../src/renderer_log_annotation.c
	$(CMAKE_COMMAND) -E cmake_progress_report /home/harry/Documents/Robotics/semantic/software/externals/semantic-perception/renderers/pod-build/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building C object src/CMakeFiles/er-semantic-perception-renderers.dir/renderer_log_annotation.c.o"
	cd /home/harry/Documents/Robotics/semantic/software/externals/semantic-perception/renderers/pod-build/src && /usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -o CMakeFiles/er-semantic-perception-renderers.dir/renderer_log_annotation.c.o   -c /home/harry/Documents/Robotics/semantic/software/externals/semantic-perception/renderers/src/renderer_log_annotation.c

src/CMakeFiles/er-semantic-perception-renderers.dir/renderer_log_annotation.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/er-semantic-perception-renderers.dir/renderer_log_annotation.c.i"
	cd /home/harry/Documents/Robotics/semantic/software/externals/semantic-perception/renderers/pod-build/src && /usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -E /home/harry/Documents/Robotics/semantic/software/externals/semantic-perception/renderers/src/renderer_log_annotation.c > CMakeFiles/er-semantic-perception-renderers.dir/renderer_log_annotation.c.i

src/CMakeFiles/er-semantic-perception-renderers.dir/renderer_log_annotation.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/er-semantic-perception-renderers.dir/renderer_log_annotation.c.s"
	cd /home/harry/Documents/Robotics/semantic/software/externals/semantic-perception/renderers/pod-build/src && /usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -S /home/harry/Documents/Robotics/semantic/software/externals/semantic-perception/renderers/src/renderer_log_annotation.c -o CMakeFiles/er-semantic-perception-renderers.dir/renderer_log_annotation.c.s

src/CMakeFiles/er-semantic-perception-renderers.dir/renderer_log_annotation.c.o.requires:
.PHONY : src/CMakeFiles/er-semantic-perception-renderers.dir/renderer_log_annotation.c.o.requires

src/CMakeFiles/er-semantic-perception-renderers.dir/renderer_log_annotation.c.o.provides: src/CMakeFiles/er-semantic-perception-renderers.dir/renderer_log_annotation.c.o.requires
	$(MAKE) -f src/CMakeFiles/er-semantic-perception-renderers.dir/build.make src/CMakeFiles/er-semantic-perception-renderers.dir/renderer_log_annotation.c.o.provides.build
.PHONY : src/CMakeFiles/er-semantic-perception-renderers.dir/renderer_log_annotation.c.o.provides

src/CMakeFiles/er-semantic-perception-renderers.dir/renderer_log_annotation.c.o.provides.build: src/CMakeFiles/er-semantic-perception-renderers.dir/renderer_log_annotation.c.o

src/CMakeFiles/er-semantic-perception-renderers.dir/renderer_place_classification.c.o: src/CMakeFiles/er-semantic-perception-renderers.dir/flags.make
src/CMakeFiles/er-semantic-perception-renderers.dir/renderer_place_classification.c.o: ../src/renderer_place_classification.c
	$(CMAKE_COMMAND) -E cmake_progress_report /home/harry/Documents/Robotics/semantic/software/externals/semantic-perception/renderers/pod-build/CMakeFiles $(CMAKE_PROGRESS_2)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building C object src/CMakeFiles/er-semantic-perception-renderers.dir/renderer_place_classification.c.o"
	cd /home/harry/Documents/Robotics/semantic/software/externals/semantic-perception/renderers/pod-build/src && /usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -o CMakeFiles/er-semantic-perception-renderers.dir/renderer_place_classification.c.o   -c /home/harry/Documents/Robotics/semantic/software/externals/semantic-perception/renderers/src/renderer_place_classification.c

src/CMakeFiles/er-semantic-perception-renderers.dir/renderer_place_classification.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/er-semantic-perception-renderers.dir/renderer_place_classification.c.i"
	cd /home/harry/Documents/Robotics/semantic/software/externals/semantic-perception/renderers/pod-build/src && /usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -E /home/harry/Documents/Robotics/semantic/software/externals/semantic-perception/renderers/src/renderer_place_classification.c > CMakeFiles/er-semantic-perception-renderers.dir/renderer_place_classification.c.i

src/CMakeFiles/er-semantic-perception-renderers.dir/renderer_place_classification.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/er-semantic-perception-renderers.dir/renderer_place_classification.c.s"
	cd /home/harry/Documents/Robotics/semantic/software/externals/semantic-perception/renderers/pod-build/src && /usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -S /home/harry/Documents/Robotics/semantic/software/externals/semantic-perception/renderers/src/renderer_place_classification.c -o CMakeFiles/er-semantic-perception-renderers.dir/renderer_place_classification.c.s

src/CMakeFiles/er-semantic-perception-renderers.dir/renderer_place_classification.c.o.requires:
.PHONY : src/CMakeFiles/er-semantic-perception-renderers.dir/renderer_place_classification.c.o.requires

src/CMakeFiles/er-semantic-perception-renderers.dir/renderer_place_classification.c.o.provides: src/CMakeFiles/er-semantic-perception-renderers.dir/renderer_place_classification.c.o.requires
	$(MAKE) -f src/CMakeFiles/er-semantic-perception-renderers.dir/build.make src/CMakeFiles/er-semantic-perception-renderers.dir/renderer_place_classification.c.o.provides.build
.PHONY : src/CMakeFiles/er-semantic-perception-renderers.dir/renderer_place_classification.c.o.provides

src/CMakeFiles/er-semantic-perception-renderers.dir/renderer_place_classification.c.o.provides.build: src/CMakeFiles/er-semantic-perception-renderers.dir/renderer_place_classification.c.o

# Object files for target er-semantic-perception-renderers
er__semantic__perception__renderers_OBJECTS = \
"CMakeFiles/er-semantic-perception-renderers.dir/renderer_log_annotation.c.o" \
"CMakeFiles/er-semantic-perception-renderers.dir/renderer_place_classification.c.o"

# External object files for target er-semantic-perception-renderers
er__semantic__perception__renderers_EXTERNAL_OBJECTS =

lib/liber-semantic-perception-renderers.so: src/CMakeFiles/er-semantic-perception-renderers.dir/renderer_log_annotation.c.o
lib/liber-semantic-perception-renderers.so: src/CMakeFiles/er-semantic-perception-renderers.dir/renderer_place_classification.c.o
lib/liber-semantic-perception-renderers.so: src/CMakeFiles/er-semantic-perception-renderers.dir/build.make
lib/liber-semantic-perception-renderers.so: src/CMakeFiles/er-semantic-perception-renderers.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking C shared library ../lib/liber-semantic-perception-renderers.so"
	cd /home/harry/Documents/Robotics/semantic/software/externals/semantic-perception/renderers/pod-build/src && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/er-semantic-perception-renderers.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
src/CMakeFiles/er-semantic-perception-renderers.dir/build: lib/liber-semantic-perception-renderers.so
.PHONY : src/CMakeFiles/er-semantic-perception-renderers.dir/build

src/CMakeFiles/er-semantic-perception-renderers.dir/requires: src/CMakeFiles/er-semantic-perception-renderers.dir/renderer_log_annotation.c.o.requires
src/CMakeFiles/er-semantic-perception-renderers.dir/requires: src/CMakeFiles/er-semantic-perception-renderers.dir/renderer_place_classification.c.o.requires
.PHONY : src/CMakeFiles/er-semantic-perception-renderers.dir/requires

src/CMakeFiles/er-semantic-perception-renderers.dir/clean:
	cd /home/harry/Documents/Robotics/semantic/software/externals/semantic-perception/renderers/pod-build/src && $(CMAKE_COMMAND) -P CMakeFiles/er-semantic-perception-renderers.dir/cmake_clean.cmake
.PHONY : src/CMakeFiles/er-semantic-perception-renderers.dir/clean

src/CMakeFiles/er-semantic-perception-renderers.dir/depend:
	cd /home/harry/Documents/Robotics/semantic/software/externals/semantic-perception/renderers/pod-build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/harry/Documents/Robotics/semantic/software/externals/semantic-perception/renderers /home/harry/Documents/Robotics/semantic/software/externals/semantic-perception/renderers/src /home/harry/Documents/Robotics/semantic/software/externals/semantic-perception/renderers/pod-build /home/harry/Documents/Robotics/semantic/software/externals/semantic-perception/renderers/pod-build/src /home/harry/Documents/Robotics/semantic/software/externals/semantic-perception/renderers/pod-build/src/CMakeFiles/er-semantic-perception-renderers.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : src/CMakeFiles/er-semantic-perception-renderers.dir/depend

