//Add a executable er-place-classifier to classify places
//Requires: er-simulate-360-laser(SKIRT_SIM messages)
//Publish: place_classification_t & place_classification_debug_t messages


#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <lcm/lcm.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include <glib.h>
#include <bot_core/bot_core.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>
#include <lcmtypes/perception_lcmtypes.h>
#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>
#include <classify-semantic/classify_semantic.hpp>

typedef struct
{
    lcm_t *lcm;
    GMainLoop *mainloop;

    pthread_t  classification_thread;
    
    GMutex *mutex; 

    int num_annotations;
    int correct;
    int total;
    int doorway;

    int l_processed; 
    int i_processed; 
    semantic_classifier_t *classifier;
}state_t;

static void usage(const char* progname)
{
    fprintf (stderr, "Usage: %s [options]\n"
             "\n"
             "Options:\n"
             "  -m MODEL  Specify SVM model\n"
             "  -d BINARY_MODEL       Add doorways\n"
             "  -h        This help message\n", 
             g_path_get_basename(progname));
    exit(1);
}

gboolean heartbeat_cb (gpointer data)
{
    state_t *s = (state_t *)data;
    return TRUE;
}

static void *classification_thread(void *user)
{
    state_t * s = (state_t *) user;

    printf("Classification thread started\n");
    int status = 0;
    while(1){
        int64_t utime = bot_timestamp_now();
        //g_mutex_lock (s->mutex);
        publish_classification_latest(s->classifier);
        //g_mutex_unlock (s->mutex);
    }
}

int 
main(int argc, char **argv)
{

    const char *optstring = "i:l:h";

    int c;
    int doorway = 0;
    char *laser_model_path = NULL;
    char *image_model_path = NULL;
    char *log_file = NULL;
    char *doorway_model = NULL;
    int ground_truth = 0;

    while ((c = getopt (argc, argv, optstring)) >= 0) {
        switch (c) {
        case 'l': //choose SVM model
            laser_model_path = strdup(optarg);
            printf("Using Laser SVM model \"%s\"\n", laser_model_path);
            break; 
        case 'i': //choose SVM model
            image_model_path = strdup(optarg);
            printf("Using Image SVM model \"%s\"\n", image_model_path);
            break; 
        case 'h': //help
            usage(argv[0]);
            break;
        default:
            usage(argv[0]);
            break;
        }
    }

    
    state_t *state = (state_t*) calloc(1, sizeof(state_t));

    //this does not attach the lcm to glib mainloop - so we need to do this mannully
    state->lcm =  bot_lcm_get_global(NULL);
  
    state->mainloop = g_main_loop_new( NULL, FALSE );  

    if(laser_model_path == NULL || image_model_path == NULL){
        //usage(argv[0]);
        state->classifier = semantic_classifier_create(true);
    }
    else{
        state->classifier = semantic_classifier_create(laser_model_path, image_model_path, true);
    }

    state->mutex =  g_mutex_new();

    if(state->classifier == NULL){
        fprintf(stderr, "Error Loading models\n");
        return -1;                    
    }

    if (!state->mainloop) {
        printf("Couldn't create main loop\n");
        return -1;
    }

    bot_glib_mainloop_attach_lcm (state->lcm);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);

    //or we make the thread handle the lcm and do the callback ?? 
    pthread_create(&state->classification_thread , NULL, classification_thread, state);
    
    //    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
  
    bot_glib_mainloop_detach_lcm(state->lcm);
}
