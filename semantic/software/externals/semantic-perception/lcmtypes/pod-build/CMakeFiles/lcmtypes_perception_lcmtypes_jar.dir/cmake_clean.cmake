FILE(REMOVE_RECURSE
  "../lcmtypes/c"
  "../lcmtypes/cpp"
  "../lcmtypes/java"
  "../lcmtypes/python"
  "CMakeFiles/lcmtypes_perception_lcmtypes_jar"
  "lcmtypes_perception_lcmtypes.jar"
  "../lcmtypes/java/perception/laser_feature_t.class"
  "../lcmtypes/java/perception/laser_annotation_t.class"
  "../lcmtypes/java/perception/place_classification_class_t.class"
  "../lcmtypes/java/perception/place_classification_t.class"
  "../lcmtypes/java/perception/annotation_t.class"
  "../lcmtypes/java/perception/image_annotation_t.class"
  "../lcmtypes/java/perception/annotation_list_t.class"
  "../lcmtypes/java/perception/place_classification_debug_t.class"
  "../lcmtypes/java/perception/HMM_place_classification_debug_t.class"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/lcmtypes_perception_lcmtypes_jar.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
