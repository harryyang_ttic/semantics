#ifndef __lcmtypes_perception_lcmtypes_hpp__
#define __lcmtypes_perception_lcmtypes_hpp__

#include "perception/place_classification_class_t.hpp"
#include "perception/place_classification_t.hpp"
#include "perception/image_annotation_t.hpp"
#include "perception/annotation_list_t.hpp"
#include "perception/place_classification_debug_t.hpp"
#include "perception/laser_feature_t.hpp"
#include "perception/HMM_place_classification_debug_t.hpp"
#include "perception/laser_annotation_t.hpp"
#include "perception/annotation_t.hpp"

#endif
