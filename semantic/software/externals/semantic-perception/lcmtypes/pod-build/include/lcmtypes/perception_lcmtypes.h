#ifndef __lcmtypes_perception_lcmtypes_h__
#define __lcmtypes_perception_lcmtypes_h__

#include "perception_annotation_t.h"
#include "perception_place_classification_debug_t.h"
#include "perception_place_classification_t.h"
#include "perception_HMM_place_classification_debug_t.h"
#include "perception_image_annotation_t.h"
#include "perception_laser_annotation_t.h"
#include "perception_place_classification_class_t.h"
#include "perception_laser_feature_t.h"
#include "perception_annotation_list_t.h"

#endif
