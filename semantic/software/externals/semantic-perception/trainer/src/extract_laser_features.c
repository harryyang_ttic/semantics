//This script collect laser_annotation_t messages, compute feature values and write to files
//Requires: er-laser-annotation(laser_annotation_t messages)
//Output: A "data.txt" text file contains feature values


#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <bot_core/bot_core.h>
#include <lcm/lcm.h>

#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>
#include <lcmtypes/perception_lcmtypes.h>
#include <lcmtypes/perception_place_classification_class_t.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>

#include <laser_features/laser_feature.h>

char *s1 = "doorway";
char *s2 = "conference_room";
char *s3 = "office";
char *s4 = "lab";
char *s5 = "open_area";
char *s6 = "hallway";
char *s7 = "elevator";
char *s8 = "corridor";
char *s9 = "large_meeting_room";
char *s10 = "classroom";

//three categories 

//room = 0
//hallway = 1
//openarea = 2

int get_class_id(char *annotation){
    if(!strcmp(annotation, "room")){
        return 0;
    }
    else if(!strcmp(annotation, "hallway")){
        return 1;
    }
    else if(!strcmp(annotation, "openarea")){
        return 2;
    }
}

typedef struct
{
    lcm_t      *lcm;
    GMainLoop *mainloop;
    perception_laser_annotation_t *last_laser_annotation;
    int verbose;
    char *file_name;
    GHashTable *class_count;
    int skip_doorways;
    FILE *file;
} laser_annotation;


void print_stat(gpointer key,
                  gpointer value,
                  gpointer user_data){
    char *class_name = NULL;
    int class_id = *(int *) key;

    if(class_id == 0){
        class_name = "room";
    }
    else if(class_id == 1){
        class_name = "hallway";
    }
    else if(class_id == 2){
        class_name = "openarea";
    }
    else{        
        class_name = "unknown";
    }
    fprintf(stderr, "Class Name [%d] : %s Count : %d\n", *(int *) key, class_name, *(int *) value);
}

void print_stats(laser_annotation *self){
    //GHashTableIter iter;
    //gpointer key, value;
    
    //g_hash_table_iter_init (&iter, self->class_count);
    g_hash_table_foreach(self->class_count, (GHFunc) print_stat, NULL);
    
}

static void
on_laser_annotation (const lcm_recv_buf_t *rbuf, const char *channel,
             const perception_laser_annotation_t *msg, void *user)
{
//    fprintf(stderr, "Receive a log annotation\n");
    FILE *file;
    static int count = 0;
    count++;
    fprintf(stderr, "Count: %d\n", count);
    laser_annotation *self= (laser_annotation *)user;
    file = self->file;
    
    if(file == NULL){
        fprintf(stderr, "Error - Unable to open file\n");
        return;
    }

    if(self->last_laser_annotation != NULL){
	perception_laser_annotation_t_destroy(self->last_laser_annotation);
    }

    self->last_laser_annotation = perception_laser_annotation_t_copy(msg);
    bot_core_planar_lidar_t laser = self->last_laser_annotation->laser;
    char *annotation = self->last_laser_annotation->annotation;
    int label =  get_class_id(annotation);

    fprintf(stderr, "Annotation : %s - Class ID : %d\n", annotation, label);

    if(label < 0){
        fprintf(stderr, "Skipping\n");
        return;
    }
    
    GHashTableIter iter;
    gpointer key, value;
    
    int *class_count = (int *) g_hash_table_lookup(self->class_count, &label);
    if(!class_count){
        class_count = (int *)calloc(1,sizeof(int));
        int *id = (int *)calloc(1,sizeof(int));
        *id = label;
        *class_count = 0;
        g_hash_table_insert(self->class_count, id, class_count);
    }
    else{
        *class_count = *class_count+1;
    }
    
    //Change the feature configurations here. min_range, max_range, threshold, etc...
    feature_config_t config;
    config.min_range = 0.1;
    config.max_range = 30;
    config.cutoff = 5;
    config.gap_threshold_1 = 3;
    config.gap_threshold_2 = 0.5;
    config.deviation = 0.001;
    config.no_of_fourier_coefficient = 2;
    config.no_beam_skip = 3; //no beam skip (2 would skip 1 beam)

    svm_node_t *features = calculate_svm_features(&laser, config);

    fprintf(file, "%d ", label);
    //fprintf(stderr, "%d ", label);
    
    int i=0;
    while(1){
        if(features[i].index >=0){
            fprintf(file, "%d:%f ", features[i].index, features[i].value);
            //fprintf(stderr, "%d:%f ", features[i].index, features[i].value);
        }
        else{
            break;
        }
        i++;
    }
    fprintf(file, "\n");
    //fprintf(stderr, "\n");
}

void subscribe_to_channels(laser_annotation *self)
{
    perception_laser_annotation_t_subscribe(self->lcm, "LASER_ANNOTATION", on_laser_annotation, self);
}  

static void usage(const char* progname)
{
    fprintf (stderr, "Usage: %s [options]\n"
             "\n"
             "Options:\n"
             "  -l LOG             Specify Log File to save training data\n"
             "  -i input_lcm_file  (optinal) Specify LCM log file to read annotated laser data from\n", 
             "  -d                 Include Doorways\n", 
             "  -v                 Verbose Mode\n", 
             "  -h                 This help message\n", 
             g_path_get_basename(progname));
    exit(1);
}


int 
main(int argc, char **argv)
{
    laser_annotation *state = (laser_annotation*) calloc(1, sizeof(laser_annotation));
    const char *optstring = "i:l:vhd";

    int c;
    state->file_name = NULL;
    state->skip_doorways = 1;
    char *input_log_file = NULL;
    while ((c = getopt (argc, argv, optstring)) >= 0) {
        switch (c) {
        case 'l':
            state->file_name = strdup(optarg);  //output
            fprintf(stderr, "Saving to LOG file %s\n", state->file_name);
            break;
        case 'i':
            input_log_file = strdup(optarg);  //annotated lcm log
            fprintf(stderr, "Reading from LOG file %s\n", input_log_file);
            break;
        case 'd':
            state->skip_doorways = 0;
            fprintf(stderr, "Training for doorways as well\n");
            break;
        case 'v':
            fprintf(stderr, "Verbose mode\n");
            state->verbose = 1;
            break;
        case 'h': //help
            usage(argv[0]);
            break;
        default:
            usage(argv[0]);
            break;
        }
    }

    fprintf(stderr, "Don't use the live mode - the handler can drop messages (because a lot of messages are sent out in bursts\n");

    if(state->file_name == NULL){
        fprintf(stderr, "No log file specified - defaulting to data.txt\n");
        //state->file = fopen("data.txt", "a");
        state->file_name = strdup("data.txt");
    }

    //this does not attach the lcm to glib mainloop - so we need to do this mannully
    state->lcm =  bot_lcm_get_global(NULL);

    state->class_count = g_hash_table_new(g_int_hash, g_int_equal);

    state->file = fopen(state->file_name, "w");

    if(input_log_file){
        fprintf(stderr, "System is loading the annotation messages in log to calculate features.\n");
        lcm_eventlog_t *read_log = lcm_eventlog_create(input_log_file, "r");
        fprintf(stderr, "Log open success.\n");
  
        //Adding to annotation circular buffer
        char *annotation_channel_name = "LASER_ANNOTATION";
        int count = 0;
        for(lcm_eventlog_event_t *event = lcm_eventlog_read_next_event(read_log); event != NULL; event = lcm_eventlog_read_next_event(read_log)){
            int decode_status = 0;
            if(strcmp(annotation_channel_name, event->channel) == 0){
                perception_laser_annotation_t *msg = (perception_laser_annotation_t *)calloc(1, sizeof(perception_laser_annotation_t));
                decode_status = perception_laser_annotation_t_decode(event->data, 0, event->datalen, msg);
                if(decode_status < 0){
                    fprintf(stderr, "Error decoding message.\n");
                    //return -1;
                }
                count++;
                on_laser_annotation(NULL, annotation_channel_name, msg, state);
                perception_laser_annotation_t_destroy(msg);
            }
            lcm_eventlog_free_event(event);
        }
        fclose(state->file);
        print_stats(state);
        fprintf(stderr, "No of examples : %d\n", count);
        return 0;
    }

    else{
        subscribe_to_channels(state);
  
        state->mainloop = g_main_loop_new( NULL, FALSE );  
  
        if (!state->mainloop) {
            printf("Couldn't create main loop\n");
            return -1;
        }

        //add lcm to mainloop 
        bot_glib_mainloop_attach_lcm (state->lcm);

        //read_parameters_from_conf(state);

        //adding proper exiting 
        bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
        //    fprintf(stderr, "Starting Main Loop\n");

        ///////////////////////////////////////////////
        g_main_loop_run(state->mainloop);
  
        bot_glib_mainloop_detach_lcm(state->lcm);
        return 0;
    }
}


