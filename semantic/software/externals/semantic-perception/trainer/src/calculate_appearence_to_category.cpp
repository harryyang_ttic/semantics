//Subscribe to log_annotation_t and planar_lidar_t messages. Once receive an annotation, publish laser_annotation_t message
//Requires: er-simulate-360-laser
//Output: publish laser_annotation_t message


#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <map>
#include <string>
#include <bot_core/bot_core.h>
#include <iostream>

#define SENSOR_DECIMATION_DISTANCE 0.1
#define LIDAR_LIST_SIZE 4000

using namespace std;

static char** split_csv (char line[])
{
    const char* tok = ",";
    char * split;
    //Get number of commas in line. How many tokens are there?
    //Right now we expect this to be four (start, end, label, channel).
    int count = 0;
    char * pch;
    pch = strchr(line, ',');
    while (pch != NULL){
        count++;
        pch = strchr(pch+1, ',');
    }
    //One more token than comma
    count++;

    split = strtok (line, tok);
    char** tokens = (char**) calloc((count+1), (sizeof(split)));
    int j = 0; 
    while (split != NULL){
        tokens[j] = split;
        split = strtok (NULL, tok);
        j++;
    }
    return tokens;
}

  
static int calculate_annotations_from_file(char *file_name, map<string, map<string, int> > &results)
{
    //map<string, map<string, int> >
    
    map<string, map<string, int> >::iterator it_appearance;
    map<string, int>::iterator it_category;

    FILE *ann = fopen(file_name, "r+");
    if (ann != NULL){
	char line [150];
	//Initialize an lcm message for each line of annotation log
	while (fgets (line, sizeof line, ann) != NULL){
            char **tokens = split_csv(line);
            char *appearence_type = tokens[3];
            char *region_type = tokens[2];
            
            it_appearance = results.find(appearence_type);
            if(it_appearance == results.end()){
                fprintf(stdout, "Adding region appearance : %s\n", appearence_type);
                map<string, int> count;
                string r_type(region_type);
                count.insert(make_pair(r_type, 1));
                results.insert(make_pair(appearence_type, count));
            }
            else{
                it_category = it_appearance->second.find(region_type);
                if(it_category == it_appearance->second.end()){
                    string r_type(region_type);
                    it_appearance->second.insert(make_pair(r_type, 1));
                }
                else{
                    it_category->second += 1;
                }
            }            
        }

        
	fprintf(stderr, "Annotations loaded from file.\n");
	return 0;
    }
    else{
        fprintf(stderr, "Error: please specify an annotation file.\n");
        return -1;
    }
}

static void usage(const char* progname)
{
    fprintf (stderr, "Usage: %s [options]\n"
             "\n"
             "Options:\n"
             "  -n        Use nodding laser\n"
             "  -h        This help message\n"
	     "  -a        Annotation filename\n"
	     "  -l        Log filename\n"
	     "  -c        Image channel\n",
             progname);
    exit(1);
}

int 
main(int argc, char **argv)
{

    const char *optstring = "ha:";

    int c;
    int nodding = 0;
    char * log_path = NULL;
    char * channel = strdup("DRAGONFLY_IMAGE");
    int annotations_from_file = 0;
    char *annotation_file = NULL;

    /*while ((c = getopt (argc, argv, optstring)) >= 0) {
        switch (c) {
        case 'h': //help
            usage(argv[0]);
            break;
	case 'a': //Annotation text file
	    annotation_file = strdup(optarg);
	    annotations_from_file = 1;
            break;    
        default:
            usage(argv[0]);
            break;
        }
        }*/

    map<string, map<string, int> > results;

    int no_files = argc - 1; 

    if(argc <2){
        fprintf(stderr, "No file names given\n");
    }

    for(int i=0; i < no_files; i++){
        char *file_name = argv[1+i];
        calculate_annotations_from_file(file_name, results);
    }

    map<string, map<string, int> >::iterator it_appearance;
    map<string, int>::iterator it_category;

    for(it_appearance = results.begin(); it_appearance != results.end(); it_appearance++){
        int count = 0; 
        for(it_category = it_appearance->second.begin(); it_category != it_appearance->second.end(); it_category++){
            count += it_category->second;
        }
        fprintf(stdout, "Appearance : %s (%d)\n", it_appearance->first.c_str(), count);
        for(it_category = it_appearance->second.begin(); it_category != it_appearance->second.end(); it_category++){
            fprintf(stdout, "\tType : %s -> %f\n", it_category->first.c_str(), it_category->second/ (double) count);
        }
    }

}


