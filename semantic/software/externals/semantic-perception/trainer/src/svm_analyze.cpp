//This script test the accuracy of svm-predict of one-vs-one classifier in libsvm

#include <map>
#include <svm/svm.h>
#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <lcm/lcm.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

//#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>
#include <laser_features/laser_feature.h>

using namespace std;

int 
main(int argc, char **argv)
{
    int c;
    const char *optstring = "i:o:m:";
    char *output_file = NULL;
    char *input_data_file = NULL;
    char *model_path = NULL;
    while ((c = getopt (argc, argv, optstring)) >= 0) {
        switch (c) {
        case 'o':
            output_file = strdup(optarg);  //choose log file for accuracy checking
            fprintf(stderr, "Saving to Training %s_train.txt\n\tTesting : %s_test.txt\n", output_file, output_file);
            break;
        case 'i':
            input_data_file = strdup(optarg);  //choose log file for accuracy checking
            fprintf(stderr, "Reading from Data file %s\n", input_data_file);
            break;
        case 'm':
            model_path = strdup(optarg);  //choose log file for accuracy checking
            fprintf(stderr, "Model Path %s\n", model_path);
            break;
        case 'h': //help
            //usage(argv[0]);
            break;
        default:
            //usage(argv[0]);
            break;
        }
    }

    if(input_data_file == NULL){
        fprintf(stderr, "No input data file specified\n");
        //usage(argv[0]);
    }
    if(model_path == NULL){
        fprintf(stderr, "No model specified\n");
        //usage(argv[0]);
    }

    FILE *f;
    f = fopen(input_data_file, "r");
    char line[500000];

    //ground truth class, predicted class, count 
    map<int, map<int, int *> > class_results;

    typedef struct svm_node svm_node_t;
    typedef struct svm_model svm_model_t;
    svm_model_t *model;
    svm_node_t *test_pt = NULL;
    model = (svm_model_t *)calloc(1, sizeof(svm_model_t));

    if(model_path != NULL){
        model = svm_load_model(model_path);
        if(model == NULL)
            fprintf(stderr, "Error loading model\n");
        else
            fprintf(stderr, "Model load success.\n");
    }

    //GHashTable *class_glists;

    //class_glists = g_hash_table_new(g_int_hash, g_int_equal);
    map<int, map <int, int*> >::iterator it_predicted;
    map<int, int*>::iterator it_ground;

    int no_features = 20;

    test_pt = (svm_node_t *)calloc(no_features, sizeof(svm_node_t));
    //test_pt[no_features - 1].index = -1;
  
    int correct = 0;
    int total = 0;
  
    while(fgets(line, sizeof(line), f) != NULL){

        char *label = strtok(line," \t\n");
        int l = atoi(label);
                                        
        char *idx, *val, *endptr;
        int num = 0;
        while(1){
            if(num == no_features - 1){
                no_features *= 2;
                test_pt = (svm_node_t *) realloc(test_pt, no_features * sizeof(svm_node_t));
            }
                    
            idx = strtok(NULL,":");
            int ind = strtod(idx, &endptr);
            val = strtok(NULL," \t");

            if(val == NULL)
                break;
            double v = strtod(val,&endptr);
            //			fprintf(stderr,"Strings : %s %s\n", idx, val);
            //fprintf(stderr, "%d %f\n", ind, v);
            test_pt[num].index = ind;
            test_pt[num].value = v;
            //			fprintf(stderr, "%d\n", ind);
            num++;
        }
        test_pt[num].index = -1;

        int lb = (int)svm_predict(model, test_pt);
        //fprintf(stderr, "Ground Truth Label:%d -> Predicted : %d\n", l, lb);

        it_predicted = class_results.find(lb);
            
        if(it_predicted == class_results.end()){
            map<int, int*> ground_map;
            fprintf(stderr, "Inserting Predicted Class : %d\n", lb);
            class_results.insert(make_pair(lb, ground_map));

            int *count = (int *) calloc(1,sizeof(int));
            *count = 1;
            ground_map.insert(make_pair(l, count));
            fprintf(stderr, "Inserting count for GT class : %d\n", l);
        }
        else{
            it_ground = it_predicted->second.find(l);
            if(it_ground == it_predicted->second.end()){
                int *count = (int *) calloc(1,sizeof(int));
                *count = 1;
                it_predicted->second.insert(make_pair(l, count));
                fprintf(stderr, "Inserting count for GT class : %d\n", l);
            }
            else{
                int *count = it_ground->second;
                //fprintf(stderr, "Count : %d\n", *count);
                //fprintf(stderr, "Count : %p, %p\n", (void *) count, (void *)it_predicted->second);
                *count = *count + 1;
            }
        }

        if(lb == l){
            correct++;
        }
        total++;
    }

    for(it_predicted = class_results.begin(); it_predicted != class_results.end(); it_predicted++){
        map<int, int*> ground_map = it_predicted->second;
        fprintf(stderr, "Predicted class : %d\n", it_predicted->first);
        
        int total = 0;
        for(it_ground = ground_map.begin(); it_ground != ground_map.end(); it_ground++){
            total += *it_ground->second;            
        }
        
        for(it_ground = ground_map.begin(); it_ground != ground_map.end(); it_ground++){
            fprintf(stderr, "\t%d : %f\n", it_ground->first, *it_ground->second / (double) total);
        }
    }

    fprintf(stderr, "Accuracy using svm_predict in libsvm is %f\n", correct/(double)total);
}
