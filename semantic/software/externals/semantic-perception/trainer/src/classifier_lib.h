#ifndef CLASSIFIER_LIB_H
#define CLASSIFIER_LIB_H

#include <svm/svm.h>
#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <lcm/lcm.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include <glib.h>
#include <bot_core/bot_core.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>
#include <lcmtypes/perception_lcmtypes.h>
#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>
#include <laser_features/laser_feature.h>
#include <full_laser/full_laser.h>

#ifdef __cplusplus
extern "C" {
#endif

  
  typedef struct svm_model svm_model_t;
  typedef struct svm_node svm_node_t;

  //takes in a laser message & a SVM model
  perception_place_classification_t *
  classify_place(bot_core_planar_lidar_t *laser, svm_model_t *model, svm_model_t *doorway_model);


#ifdef __cplusplus
}
#endif

#endif
