# Install script for directory: /home/harry/Documents/Robotics/semantic/software/externals/libdai

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/home/harry/Documents/Robotics/semantic/software/build")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Release")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/dai" TYPE FILE FILES
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/fbp.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/jtree.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/cbp.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/lc.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/regiongraph.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/daialg.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/smallset.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/weightedgraph.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/hak.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/mf.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/factorgraph.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/enum.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/bp_dual.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/evidence.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/emalg.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/bbp.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/clustergraph.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/var.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/prob.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/alldai.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/mr.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/exceptions.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/io.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/exactinf.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/dag.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/graph.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/index.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/properties.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/varset.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/gibbs.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/trwbp.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/bipgraph.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/doc.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/util.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/decmap.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/factor.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/bp.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/include/dai/treeep.h"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  IF(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/liblibdai.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/liblibdai.so")
    FILE(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/liblibdai.so"
         RPATH "/home/harry/Documents/Robotics/semantic/software/build/lib")
  ENDIF()
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES "/home/harry/Documents/Robotics/semantic/software/externals/libdai/pod-build/lib/liblibdai.so")
  IF(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/liblibdai.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/liblibdai.so")
    FILE(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/liblibdai.so"
         OLD_RPATH "/home/harry/Documents/Robotics/semantic/software/externals/libdai/pod-build/lib:/home/harry/Documents/Robotics/semantic/software/build/lib:"
         NEW_RPATH "/home/harry/Documents/Robotics/semantic/software/build/lib")
    IF(CMAKE_INSTALL_DO_STRIP)
      EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/liblibdai.so")
    ENDIF(CMAKE_INSTALL_DO_STRIP)
  ENDIF()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/externals/libdai/pod-build/lib/pkgconfig/libdai.pc")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  IF(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/dai-fg2dot" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/dai-fg2dot")
    FILE(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/dai-fg2dot"
         RPATH "/home/harry/Documents/Robotics/semantic/software/build/lib")
  ENDIF()
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/home/harry/Documents/Robotics/semantic/software/externals/libdai/pod-build/bin/dai-fg2dot")
  IF(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/dai-fg2dot" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/dai-fg2dot")
    FILE(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/dai-fg2dot"
         OLD_RPATH "/home/harry/Documents/Robotics/semantic/software/externals/libdai/pod-build/lib:/home/harry/Documents/Robotics/semantic/software/build/lib:"
         NEW_RPATH "/home/harry/Documents/Robotics/semantic/software/build/lib")
    IF(CMAKE_INSTALL_DO_STRIP)
      EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/dai-fg2dot")
    ENDIF(CMAKE_INSTALL_DO_STRIP)
  ENDIF()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  IF(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/dai-fginfo" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/dai-fginfo")
    FILE(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/dai-fginfo"
         RPATH "/home/harry/Documents/Robotics/semantic/software/build/lib")
  ENDIF()
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/home/harry/Documents/Robotics/semantic/software/externals/libdai/pod-build/bin/dai-fginfo")
  IF(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/dai-fginfo" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/dai-fginfo")
    FILE(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/dai-fginfo"
         OLD_RPATH "/home/harry/Documents/Robotics/semantic/software/externals/libdai/pod-build/lib:/home/harry/Documents/Robotics/semantic/software/build/lib:"
         NEW_RPATH "/home/harry/Documents/Robotics/semantic/software/build/lib")
    IF(CMAKE_INSTALL_DO_STRIP)
      EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/dai-fginfo")
    ENDIF(CMAKE_INSTALL_DO_STRIP)
  ENDIF()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(CMAKE_INSTALL_COMPONENT)
  SET(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
ELSE(CMAKE_INSTALL_COMPONENT)
  SET(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
ENDIF(CMAKE_INSTALL_COMPONENT)

FILE(WRITE "/home/harry/Documents/Robotics/semantic/software/externals/libdai/pod-build/${CMAKE_INSTALL_MANIFEST}" "")
FOREACH(file ${CMAKE_INSTALL_MANIFEST_FILES})
  FILE(APPEND "/home/harry/Documents/Robotics/semantic/software/externals/libdai/pod-build/${CMAKE_INSTALL_MANIFEST}" "${file}\n")
ENDFOREACH(file)
