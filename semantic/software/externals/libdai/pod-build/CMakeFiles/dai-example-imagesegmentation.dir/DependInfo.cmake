# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/harry/Documents/Robotics/semantic/software/externals/libdai/libDAI-0.3.0/examples/example_imagesegmentation.cpp" "/home/harry/Documents/Robotics/semantic/software/externals/libdai/pod-build/CMakeFiles/dai-example-imagesegmentation.dir/libDAI-0.3.0/examples/example_imagesegmentation.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "DAI_WITH_BP"
  "DAI_WITH_CBP"
  "DAI_WITH_DECMAP"
  "DAI_WITH_FBP"
  "DAI_WITH_GIBBS"
  "DAI_WITH_HAK"
  "DAI_WITH_JTREE"
  "DAI_WITH_LC"
  "DAI_WITH_MF"
  "DAI_WITH_MR"
  "DAI_WITH_TREEEP"
  "DAI_WITH_TRWBP"
  "NEW_CIMG"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "include"
  "/home/harry/Documents/Robotics/semantic/software/build/include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
