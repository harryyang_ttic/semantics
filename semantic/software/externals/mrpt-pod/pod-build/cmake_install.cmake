# Install script for directory: /home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/mrpt-read-only

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/home/harry/Documents/Robotics/semantic/software/build")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Release")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES
    "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/pkgconfig/mrpt-maps.pc"
    "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/pkgconfig/mrpt-kinematics.pc"
    "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/pkgconfig/mrpt-srba.pc"
    "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/pkgconfig/mrpt-graphslam.pc"
    "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/pkgconfig/mrpt-graphs.pc"
    "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/pkgconfig/mrpt-scanmatching.pc"
    "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/pkgconfig/mrpt-base.pc"
    "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/pkgconfig/mrpt-obs.pc"
    "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/pkgconfig/mrpt-hwdrivers.pc"
    "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/pkgconfig/mrpt-detectors.pc"
    "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/pkgconfig/mrpt-topography.pc"
    "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/pkgconfig/mrpt-bayes.pc"
    "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/pkgconfig/mrpt-reactivenav.pc"
    "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/pkgconfig/mrpt-vision.pc"
    "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/pkgconfig/mrpt-gui.pc"
    "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/pkgconfig/mrpt-slam.pc"
    "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/pkgconfig/mrpt-opengl.pc"
    "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/pkgconfig/mrpt-hmtslam.pc"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/mrpt" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/unix-install/MRPTConfig.cmake")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/doc/mrpt-doc/" TYPE DIRECTORY FILES "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/mrpt-read-only/doc/html" REGEX "/\\.svn$" EXCLUDE)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/doc/mrpt-doc/" TYPE DIRECTORY FILES "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/mrpt-read-only/samples" REGEX "/\\.svn$" EXCLUDE)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/doc/mrpt-doc" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/mrpt-read-only/doc/mrpt_example1.tar.gz")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share" TYPE DIRECTORY FILES "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/mrpt-read-only/share/applications" REGEX "/\\.svn$" EXCLUDE)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share" TYPE DIRECTORY FILES "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/mrpt-read-only/share/mrpt" REGEX "/\\.svn$" EXCLUDE)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share" TYPE DIRECTORY FILES "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/mrpt-read-only/share/pixmaps" REGEX "/\\.svn$" EXCLUDE)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share" TYPE DIRECTORY FILES "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/mrpt-read-only/share/mime" REGEX "/\\.svn$" EXCLUDE)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/mrpt/hmtslam/include/" TYPE DIRECTORY FILES "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/mrpt-read-only/libs/hmtslam/include/" REGEX "/\\.svn$" EXCLUDE)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/mrpt/graphs/include/" TYPE DIRECTORY FILES "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/mrpt-read-only/libs/graphs/include/" REGEX "/\\.svn$" EXCLUDE)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/mrpt/base/include/" TYPE DIRECTORY FILES "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/mrpt-read-only/libs/base/include/" REGEX "/\\.svn$" EXCLUDE)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/mrpt/vision/include/" TYPE DIRECTORY FILES "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/mrpt-read-only/libs/vision/include/" REGEX "/\\.svn$" EXCLUDE)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/mrpt/hwdrivers/include/" TYPE DIRECTORY FILES "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/mrpt-read-only/libs/hwdrivers/include/" REGEX "/\\.svn$" EXCLUDE)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/mrpt/topography/include/" TYPE DIRECTORY FILES "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/mrpt-read-only/libs/topography/include/" REGEX "/\\.svn$" EXCLUDE)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/mrpt/kinematics/include/" TYPE DIRECTORY FILES "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/mrpt-read-only/libs/kinematics/include/" REGEX "/\\.svn$" EXCLUDE)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/mrpt/opengl/include/" TYPE DIRECTORY FILES "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/mrpt-read-only/libs/opengl/include/" REGEX "/\\.svn$" EXCLUDE)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/mrpt/gui/include/" TYPE DIRECTORY FILES "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/mrpt-read-only/libs/gui/include/" REGEX "/\\.svn$" EXCLUDE)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/mrpt/scanmatching/include/" TYPE DIRECTORY FILES "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/mrpt-read-only/libs/scanmatching/include/" REGEX "/\\.svn$" EXCLUDE)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/mrpt/bayes/include/" TYPE DIRECTORY FILES "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/mrpt-read-only/libs/bayes/include/" REGEX "/\\.svn$" EXCLUDE)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/mrpt/detectors/include/" TYPE DIRECTORY FILES "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/mrpt-read-only/libs/detectors/include/" REGEX "/\\.svn$" EXCLUDE)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/mrpt/obs/include/" TYPE DIRECTORY FILES "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/mrpt-read-only/libs/obs/include/" REGEX "/\\.svn$" EXCLUDE)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/mrpt/graphslam/include/" TYPE DIRECTORY FILES "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/mrpt-read-only/libs/graphslam/include/" REGEX "/\\.svn$" EXCLUDE)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/mrpt/srba/include/" TYPE DIRECTORY FILES "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/mrpt-read-only/libs/srba/include/" REGEX "/\\.svn$" EXCLUDE)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/mrpt/reactivenav/include/" TYPE DIRECTORY FILES "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/mrpt-read-only/libs/reactivenav/include/" REGEX "/\\.svn$" EXCLUDE)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/mrpt/slam/include/" TYPE DIRECTORY FILES "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/mrpt-read-only/libs/slam/include/" REGEX "/\\.svn$" EXCLUDE)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/mrpt/maps/include/" TYPE DIRECTORY FILES "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/mrpt-read-only/libs/maps/include/" REGEX "/\\.svn$" EXCLUDE)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/mrpt/mrpt-config/mrpt" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/include/mrpt-config/unix//mrpt/config.h")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/mrpt/mrpt-config/mrpt" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/include/mrpt-config/unix//mrpt/version.h")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/mrpt/base/include/" TYPE DIRECTORY FILES "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/mrpt-read-only/otherlibs/eigen3/Eigen" REGEX "/\\.svn$" EXCLUDE)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/mrpt/base/include/" TYPE DIRECTORY FILES "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/mrpt-read-only/otherlibs/eigen3/unsupported" REGEX "/\\.svn$" EXCLUDE)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  INCLUDE("/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/otherlibs/cmake_install.cmake")
  INCLUDE("/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/libs/cmake_install.cmake")
  INCLUDE("/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/apps/cmake_install.cmake")
  INCLUDE("/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/doc/cmake_install.cmake")
  INCLUDE("/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/otherlibs/gtest-1.6.0/cmake_install.cmake")
  INCLUDE("/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/tests/cmake_install.cmake")

ENDIF(NOT CMAKE_INSTALL_LOCAL_ONLY)

IF(CMAKE_INSTALL_COMPONENT)
  SET(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
ELSE(CMAKE_INSTALL_COMPONENT)
  SET(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
ENDIF(CMAKE_INSTALL_COMPONENT)

FILE(WRITE "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/${CMAKE_INSTALL_MANIFEST}" "")
FOREACH(file ${CMAKE_INSTALL_MANIFEST_FILES})
  FILE(APPEND "/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/${CMAKE_INSTALL_MANIFEST}" "${file}\n")
ENDFOREACH(file)
