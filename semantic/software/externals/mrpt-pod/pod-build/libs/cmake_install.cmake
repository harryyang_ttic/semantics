# Install script for directory: /home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/mrpt-read-only/libs

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/home/harry/Documents/Robotics/semantic/software/build")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Release")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  INCLUDE("/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/libs/hmtslam/cmake_install.cmake")
  INCLUDE("/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/libs/graphs/cmake_install.cmake")
  INCLUDE("/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/libs/base/cmake_install.cmake")
  INCLUDE("/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/libs/vision/cmake_install.cmake")
  INCLUDE("/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/libs/hwdrivers/cmake_install.cmake")
  INCLUDE("/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/libs/topography/cmake_install.cmake")
  INCLUDE("/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/libs/kinematics/cmake_install.cmake")
  INCLUDE("/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/libs/opengl/cmake_install.cmake")
  INCLUDE("/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/libs/gui/cmake_install.cmake")
  INCLUDE("/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/libs/scanmatching/cmake_install.cmake")
  INCLUDE("/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/libs/bayes/cmake_install.cmake")
  INCLUDE("/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/libs/detectors/cmake_install.cmake")
  INCLUDE("/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/libs/obs/cmake_install.cmake")
  INCLUDE("/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/libs/graphslam/cmake_install.cmake")
  INCLUDE("/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/libs/srba/cmake_install.cmake")
  INCLUDE("/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/libs/reactivenav/cmake_install.cmake")
  INCLUDE("/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/libs/pbmap/cmake_install.cmake")
  INCLUDE("/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/libs/slam/cmake_install.cmake")
  INCLUDE("/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/pod-build/libs/maps/cmake_install.cmake")

ENDIF(NOT CMAKE_INSTALL_LOCAL_ONLY)

