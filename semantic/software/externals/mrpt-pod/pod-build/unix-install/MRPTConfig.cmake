# =========================================================================
#  The Mobile Robot Programming Toolkit (MRPT) CMake configuration file
#
#             ** File generated automatically, do not modify **
#
#  Usage from an external project: 
#   In your CMakeLists.txt, add these lines:
#
#    FIND_PACKAGE( MRPT REQUIRED )  # Default: Use mrpt-slam & mrpt-topography
#     or
#    FIND_PACKAGE( MRPT REQUIRED base vision hwdrivers ... )
#
#    TARGET_LINK_LIBRARIES(MY_TARGET_NAME ${MRPT_LIBS})
#
#   
#   The libraries or "modules" which can be included are
#   (see the full list and description in: http://www.mrpt.org/Libraries )
#     - base           --> libmrpt-base
#     - opengl         --> libmrpt-opengl
#     - hwdrivers      --> libmrpt-hwdrivers
#     - reactivenav    --> libmrpt-reactivenav
#     - hmtslam        --> libmrpt-hmtslam
#
#   This file will define the following variables:
#    - MRPT_LIBS: The list of libraries to links against.
#    - MRPT_VERSION: The MRPT version (e.g. "1.0.0"). 
#    - MRPT_VERSION_{MAJOR,MINOR,PATCH}: 3 variables for the version parts
#
#   Optional input variables:
#    - MRPT_DONT_USE_DBG_LIBS: If defined at input, before the 
#         "FIND_PACKAGE(MRPT...)", and set to "1", the release libraries
#         will be used even for linking against "Debug" CMake builds.
#
# =========================================================================

# MRPT version numbers:
SET(MRPT_VERSION 1.0.2)
SET(MRPT_VERSION_MAJOR 1)
SET(MRPT_VERSION_MINOR 0)
SET(MRPT_VERSION_PATCH 2)


# Extract the directory where *this* file has been installed (determined at cmake run-time)
get_filename_component(THIS_MRPT_CONFIG_PATH "${CMAKE_CURRENT_LIST_FILE}" PATH)

# MRPT source dir
SET(MRPT_SOURCE_DIR "/home/harry/Documents/Robotics/semantic/software/build")
SET(MRPT_LIBS_INCL_DIR "/home/harry/Documents/Robotics/semantic/software/build/include/mrpt")

# MRPT binary dir
SET(MRPT_DIR "/home/harry/Documents/Robotics/semantic/software/build")

# MRPT include directory for "config.h"
SET(MRPT_CONFIG_DIR "/home/harry/Documents/Robotics/semantic/software/build/include/mrpt/mrpt-config/")

#MESSAGE(STATUS "MRPT_FIND_COMPONENTS: ${MRPT_FIND_COMPONENTS}")

# ======================================================
# Include directories where Eigen3 headers are... if 
#  they are not embedded under mrpt-base headers:
# ======================================================
IF (NOT 1 OR NOT 1)
	INCLUDE_DIRECTORIES("/home/harry/Documents/Robotics/semantic/software/externals/mrpt-pod/mrpt-read-only/otherlibs/eigen3")
ENDIF (NOT 1 OR NOT 1)

# If using GCC and -Wall, eigen3 headers raise some warnings: silent them:
IF(CMAKE_COMPILER_IS_GNUCXX)
	SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-long-long -Wno-variadic-macros")
ENDIF(CMAKE_COMPILER_IS_GNUCXX)


# ======================================================
# Include directories to add to the user project:
# ======================================================
INCLUDE_DIRECTORIES(${MRPT_CONFIG_DIR})

# OpenCV library:
IF(1)    # CMAKE_MRPT_HAS_OPENCV_SYSTEM
	# Using system opencv lib: 
	# Users must link against OpenCV only if MRPT is not a dynamic library:
	IF (NOT 1)  # CMAKE_MRPT_BUILD_SHARED_LIB
		LINK_DIRECTORIES("")  # OPENCV_LIBDIR
	ENDIF (NOT 1)
ENDIF(1)



# ==========================================================
# Default "MRPT_FIND_COMPONENTS" -> "slam" + "topography",
#  for backward compatibility (< MRPT 0.9.0)
# ==========================================================
IF ("${MRPT_FIND_COMPONENTS}" STREQUAL "")
	SET(MRPT_FIND_COMPONENTS slam topography)
ENDIF ("${MRPT_FIND_COMPONENTS}" STREQUAL "")

# ======================================================
# The list of MRPT libs/modules in this build
# ======================================================
SET(ALL_MRPT_LIBS "mrpt-hmtslam;mrpt-graphs;mrpt-base;mrpt-vision;mrpt-hwdrivers;mrpt-topography;mrpt-kinematics;mrpt-opengl;mrpt-gui;mrpt-scanmatching;mrpt-bayes;mrpt-detectors;mrpt-obs;mrpt-graphslam;mrpt-srba;mrpt-reactivenav;mrpt-slam;mrpt-maps")
#  Start of DECLARE_LIBS_DEPS 
 set_property(GLOBAL PROPERTY "mrpt-hmtslam_LIB_DEPS" "mrpt-slam;mrpt-graphslam;mrpt-graphs;mrpt-vision;mrpt-maps;mrpt-base;mrpt-obs;mrpt-opengl")
 set_property(GLOBAL PROPERTY "mrpt-graphs_LIB_DEPS" "mrpt-base")
 set_property(GLOBAL PROPERTY "mrpt-base_LIB_DEPS" "")
 set_property(GLOBAL PROPERTY "mrpt-vision_LIB_DEPS" "mrpt-obs;mrpt-opengl;mrpt-base;mrpt-opengl")
 set_property(GLOBAL PROPERTY "mrpt-hwdrivers_LIB_DEPS" "mrpt-base;mrpt-maps;mrpt-obs;mrpt-gui;mrpt-opengl;mrpt-maps")
 set_property(GLOBAL PROPERTY "mrpt-topography_LIB_DEPS" "mrpt-base;mrpt-obs;mrpt-opengl;mrpt-scanmatching")
 set_property(GLOBAL PROPERTY "mrpt-kinematics_LIB_DEPS" "mrpt-opengl;mrpt-base")
 set_property(GLOBAL PROPERTY "mrpt-opengl_LIB_DEPS" "mrpt-base")
 set_property(GLOBAL PROPERTY "mrpt-gui_LIB_DEPS" "mrpt-opengl;mrpt-base")
 set_property(GLOBAL PROPERTY "mrpt-scanmatching_LIB_DEPS" "mrpt-base")
 set_property(GLOBAL PROPERTY "mrpt-bayes_LIB_DEPS" "mrpt-base")
 set_property(GLOBAL PROPERTY "mrpt-detectors_LIB_DEPS" "mrpt-vision;mrpt-maps;mrpt-gui;mrpt-slam;mrpt-base;mrpt-opengl;mrpt-obs")
 set_property(GLOBAL PROPERTY "mrpt-obs_LIB_DEPS" "mrpt-opengl;mrpt-base")
 set_property(GLOBAL PROPERTY "mrpt-graphslam_LIB_DEPS" "mrpt-slam;mrpt-bayes;mrpt-graphs;mrpt-vision;mrpt-scanmatching;mrpt-maps;mrpt-obs;mrpt-opengl;mrpt-base")
 set_property(GLOBAL PROPERTY "mrpt-srba_LIB_DEPS" "mrpt-opengl;mrpt-base")
 set_property(GLOBAL PROPERTY "mrpt-reactivenav_LIB_DEPS" "mrpt-graphs;mrpt-maps;mrpt-obs;mrpt-opengl;mrpt-base")
 set_property(GLOBAL PROPERTY "mrpt-slam_LIB_DEPS" "mrpt-bayes;mrpt-graphs;mrpt-vision;mrpt-scanmatching;mrpt-maps;mrpt-obs;mrpt-opengl;mrpt-base")
 set_property(GLOBAL PROPERTY "mrpt-maps_LIB_DEPS" "mrpt-obs;mrpt-opengl;mrpt-base")

#  End of DECLARE_LIBS_DEPS 

#  Start of DECLARE_LIBS_HDR_ONLY
 set_property(GLOBAL PROPERTY "mrpt-hmtslam_LIB_IS_HEADERS_ONLY" "0")
 set_property(GLOBAL PROPERTY "mrpt-graphs_LIB_IS_HEADERS_ONLY" "1")
 set_property(GLOBAL PROPERTY "mrpt-base_LIB_IS_HEADERS_ONLY" "0")
 set_property(GLOBAL PROPERTY "mrpt-vision_LIB_IS_HEADERS_ONLY" "0")
 set_property(GLOBAL PROPERTY "mrpt-hwdrivers_LIB_IS_HEADERS_ONLY" "0")
 set_property(GLOBAL PROPERTY "mrpt-topography_LIB_IS_HEADERS_ONLY" "0")
 set_property(GLOBAL PROPERTY "mrpt-kinematics_LIB_IS_HEADERS_ONLY" "0")
 set_property(GLOBAL PROPERTY "mrpt-opengl_LIB_IS_HEADERS_ONLY" "0")
 set_property(GLOBAL PROPERTY "mrpt-gui_LIB_IS_HEADERS_ONLY" "0")
 set_property(GLOBAL PROPERTY "mrpt-scanmatching_LIB_IS_HEADERS_ONLY" "0")
 set_property(GLOBAL PROPERTY "mrpt-bayes_LIB_IS_HEADERS_ONLY" "1")
 set_property(GLOBAL PROPERTY "mrpt-detectors_LIB_IS_HEADERS_ONLY" "0")
 set_property(GLOBAL PROPERTY "mrpt-obs_LIB_IS_HEADERS_ONLY" "0")
 set_property(GLOBAL PROPERTY "mrpt-graphslam_LIB_IS_HEADERS_ONLY" "1")
 set_property(GLOBAL PROPERTY "mrpt-srba_LIB_IS_HEADERS_ONLY" "1")
 set_property(GLOBAL PROPERTY "mrpt-reactivenav_LIB_IS_HEADERS_ONLY" "0")
 set_property(GLOBAL PROPERTY "mrpt-slam_LIB_IS_HEADERS_ONLY" "0")
 set_property(GLOBAL PROPERTY "mrpt-maps_LIB_IS_HEADERS_ONLY" "0")

#  End of DECLARE_LIBS_HDR_ONLY

# Add hierarchical dependencies of MRPT libraries,
#  and replace "base"->"mrpt-base", etc.
# ================================================
SET(FINAL_LIST_COMPONENTS "")
FOREACH(_MOD ${MRPT_FIND_COMPONENTS})
	STRING(TOLOWER ${_MOD} _MOD)
	SET(_NAM "mrpt-${_MOD}")
	get_property(_LIB_DEP GLOBAL PROPERTY "${_NAM}_LIB_DEPS")

	# Add deps:
	IF (NOT "${_LIB_DEP}" STREQUAL "")
		#MESSAGE(STATUS "ADD: ${_LIB_DEP}")
		LIST(APPEND FINAL_LIST_COMPONENTS ${_LIB_DEP})
	ENDIF (NOT "${_LIB_DEP}" STREQUAL "")

	# Add lib itself:
	IF (NOT "${_NAM}" STREQUAL "mrpt-core")
		LIST(APPEND FINAL_LIST_COMPONENTS ${_NAM})
	ENDIF (NOT "${_NAM}" STREQUAL "mrpt-core")
ENDFOREACH(_MOD)
SET(MRPT_FIND_COMPONENTS ${FINAL_LIST_COMPONENTS})
list(REMOVE_DUPLICATES MRPT_FIND_COMPONENTS)

# ======================================================
# Check:
#  All libraries must exist in the MRPT build. 
#  Compare "MRPT_FIND_COMPONENTS" against "ALL_MRPT_LIBS":
# ======================================================
#MESSAGE(STATUS "LIBS: ${ALL_MRPT_LIBS}")
FOREACH(MRPTLIB ${MRPT_FIND_COMPONENTS})
	list(FIND ALL_MRPT_LIBS "${MRPTLIB}" _LIB_IDX)
	IF (_LIB_IDX EQUAL -1)
		MESSAGE(FATAL_ERROR "ERROR: MRPT_FIND_COMPONENTS contains '${MRPTLIB}', not built in mrpt (built ones: ${ALL_MRPT_LIBS})")
	ENDIF (_LIB_IDX EQUAL -1)
ENDFOREACH(MRPTLIB)
#MESSAGE(STATUS "MRPT_FIND_COMPONENTS: ${MRPT_FIND_COMPONENTS}")


# ======================================================
#   MRPT list of libraries the user should link against:
# ======================================================
SET(MRPT_LIBS "")

SET(MRPT_COMP_HWDRIVERS 0  INTERNAL)
# Process the list:
FOREACH(MRPTLIB ${MRPT_FIND_COMPONENTS})
	STRING(TOLOWER ${MRPTLIB} MRPTLIB)
	STRING(REGEX REPLACE "mrpt-(.*)" "\\1" MRPTLIB ${MRPTLIB})

	# The include dir:
	INCLUDE_DIRECTORIES("${MRPT_LIBS_INCL_DIR}/${MRPTLIB}/include")

	# List of link libs only needed in GCC. In MSVC we use pragma link libs.
	IF(NOT MSVC AND NOT BORLAND)
		# If the required lib is header-only, don't add to the list of libs to link!
		get_property(_LIB_HDRONLY GLOBAL PROPERTY "mrpt-${MRPTLIB}_LIB_IS_HEADERS_ONLY")
		IF(NOT _LIB_HDRONLY)
			IF(NOT MRPT_DONT_USE_DBG_LIBS)
				LIST(APPEND MRPT_LIBS  optimized mrpt-${MRPTLIB} debug mrpt-${MRPTLIB}-dbg)
			ELSE(NOT MRPT_DONT_USE_DBG_LIBS)
				LIST(APPEND MRPT_LIBS  mrpt-${MRPTLIB})
			ENDIF(NOT MRPT_DONT_USE_DBG_LIBS)
		ENDIF(NOT _LIB_HDRONLY)
	ENDIF(NOT MSVC AND NOT BORLAND)
	
	IF(MRPTLIB MATCHES "hwdrivers")
		SET(MRPT_COMP_HWDRIVERS 1)
	ENDIF(MRPTLIB MATCHES "hwdrivers")
ENDFOREACH(MRPTLIB)

# Handle special cases:
# libs for ffmpeg in win32 (only if building static libs)
IF(MRPT_COMP_HWDRIVERS AND NOT 1)
	IF( MATCHES "ON")  # MRPT_HAS_FFMPEG_WIN32
		FOREACH(FFMPGLIB ";avcodec;avformat;avutil;swscale")
			LIST(APPEND MRPT_LIBS general "${FFMPGLIB}")
		ENDFOREACH(FFMPGLIB)
	ENDIF( MATCHES "ON")
ENDIF(MRPT_COMP_HWDRIVERS AND NOT 1)

# ======================================================
# Support for wxWidgets: We only need wx libs at user
#  link time if MRPT libs are static.
# ======================================================
IF(0 AND NOT 1)  # CMAKE_MRPT_HAS_WXWIDGETS AND NOT CMAKE_MRPT_BUILD_SHARED_LIB_ONOFF
	SET(wxWidgets_USE_LIBS base core gl adv aui)

	IF(CMAKE_BUILD_TYPE MATCHES "Debug")
		SET(wxWidgets_USE_DEBUG "ON")
	ELSE(CMAKE_BUILD_TYPE MATCHES "Debug")
		SET(wxWidgets_USE_REL_AND_DBG "ON")
	ENDIF(CMAKE_BUILD_TYPE MATCHES "Debug")

	FIND_PACKAGE(wxWidgets)
	IF(wxWidgets_FOUND)
		# Include wxWidgets macros
		INCLUDE(${wxWidgets_USE_FILE})
		# ${wxWidgets_LIBRARIES}  will contain the libraries that should be added through TARGET_LINK_LIBRARIES(...)
		LINK_DIRECTORIES(${wxWidgets_LIBRARY_DIRS})

		IF(MSVC)
			ADD_DEFINITIONS(-DwxUSE_NO_MANIFEST=1)
		ENDIF(MSVC)

		LIST(APPEND MRPT_LIBS ${wxWidgets_LIBRARIES})
		IF(MSVC)
			ADD_DEFINITIONS(-D_CRT_SECURE_NO_WARNINGS)
		ENDIF(MSVC)
	ELSE(wxWidgets_FOUND)
		MESSAGE("Warning: MRPT was compiled with wxWidgets, but CMake is not able to locate wxWidgets automatically. Please, set wxWidgets_ROOT_DIR to the lib directory manually.\n You can safely ignore this warning if you don't intend to use MRPT GUI classes or wxWidgets directly from your application.")
	ENDIF(wxWidgets_FOUND)
ENDIF(0 AND NOT 1)

# ======================================================
# Support for Bumblebee
# ======================================================
IF(WIN32 AND NOT 1)
	IF(0)   # CMAKE_MRPT_HAS_BUMBLEBEE
		LINK_DIRECTORIES("/lib")
		LINK_DIRECTORIES("/lib")
	ENDIF(0)
ENDIF(WIN32 AND NOT 1)

# ======================================================
# Link directories to add to the user project:
# ======================================================
LINK_DIRECTORIES(${MRPT_DIR}/lib)

