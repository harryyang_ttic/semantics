#include "full_laser.h"

static void
on_front_laser (const lcm_recv_buf_t *rbuf, const char *channel,
                const bot_core_planar_lidar_t *msg, void *user)
{
    full_laser_state_t *s = (full_laser_state_t *)user;

    if(s->last_front_laser != NULL){
        bot_core_planar_lidar_t_destroy(s->last_front_laser);
    }
    s->last_front_laser = bot_core_planar_lidar_t_copy(msg);

    if(s->last_rear_laser==NULL || (s->last_rear_laser->utime - msg->utime)/1.0e6 > 0.5){
        return; 
    }
  
    for(int i=0; i < s->no_bins; i++){
        s->readings[i] = 30;
    }

    int wierd = 0; 

    for(int i = 0; i < s->last_front_laser->nranges - 1;i++){
        if( s->last_front_laser->ranges[i] < 0.01) 
            continue;
        //Transform from laser frame to body frame if needed
        double pose[3];
        double body[3];
        double theta = s->last_front_laser->rad0 + i * s->last_front_laser->radstep;
        //laser frame
        pose[0] = s->last_front_laser->ranges[i] * cos(theta);
        pose[1] = s->last_front_laser->ranges[i] * sin(theta);
        pose[2] = 0;
        //this is fine for now - but no motion compensation ??
        //if need to motion compensate with the rear laser - need to transform to local 
        //and then to the body for the time of the front laser
        //get it in body frame
        int trans = bot_frames_transform_vec(s->frames, "SKIRT_FRONT", "body", pose, body);
    
        if(fabs(body[0]) < 0.5 && fabs(body[1]) < 0.3)
            continue;
        //get the heading 
        double heading = atan2(body[1], body[0]);
        double range = hypot(body[0], body[1]);
        //get the bin id 
        int id = bot_theta_to_int(heading, s->no_bins);

        if(s->readings[id] > range){
            s->readings[id] = range;
        }
    }


    for(int i = 0; i < s->last_rear_laser->nranges - 1;i++){
        if( s->last_rear_laser->ranges[i] < 0.01) 
            continue;
        //Transform from laser frame to body frame if needed
        double pose[3];
        double body[3];
        double theta = s->last_rear_laser->rad0 + i * s->last_rear_laser->radstep;
        //laser frame
        pose[0] = s->last_rear_laser->ranges[i] * cos(theta);
        pose[1] = s->last_rear_laser->ranges[i] * sin(theta);
        pose[2] = 0;
        //get it in body frame
        int trans = bot_frames_transform_vec(s->frames, "SKIRT_REAR", "body", pose, body);
        if(fabs(body[0]) < 0.5 && fabs(body[1]) < 0.3)
            continue;
        //get the heading 
        double heading = atan2(body[1], body[0]);
        double range = hypot(body[0], body[1]);
        //get the bin id 
        int id = bot_theta_to_int(heading, s->no_bins);

        if(s->readings[id] > range){
            s->readings[id] = range;
        }
    }

    int prev_valid = -1;
    for(int i=0; i < s->no_bins; i++){
        double r = s->readings[i];
	
        if(r < 30 && r > 0.1){
            prev_valid = i;
        }
        if(r >=30 || r <= 0.1){
            int next_valid = -1;
            for(int j = i+1;j < s->no_bins;j++){
                if(s->readings[j] < 30 && s->readings[j] > 0.1){
                    next_valid = j;
                    break;
                }
            }
            if(prev_valid != -1 && next_valid != -1)
                s->readings[i] = (s->readings[prev_valid] + s->readings[next_valid]) / (double)2;
        }
    }
    
    s->full_laser->utime = msg->utime;
    s->full_laser->nranges = s->no_bins;
    s->full_laser->ranges = s->readings;
    s->full_laser->nintensities = 0; 
    s->full_laser->intensities = NULL;
    s->full_laser->rad0 = 0;
    s->full_laser->radstep = 2*M_PI/ (s->no_bins -1); 

    //do the callback if ready 
    if(s->on_update){
        s->on_update->callback_func(msg->utime , s->full_laser, s->on_update->user);
    }    
}

static void
on_rear_laser (const lcm_recv_buf_t *rbuf, const char *channel,
               const bot_core_planar_lidar_t *msg, void *user)
{
    full_laser_state_t *s = (full_laser_state_t *)user;
    
    //might want to make this a buffer??
    if(s->last_rear_laser != NULL){
        bot_core_planar_lidar_t_destroy(s->last_rear_laser);
    }
    s->last_rear_laser = bot_core_planar_lidar_t_copy(msg);
}

full_laser_state_t * full_laser_init(lcm_t *lcm, int no_bins, on_full_laser_update_t *callback, void *user){

    if(no_bins <= 0 || lcm == NULL)
        return NULL;


    
    full_laser_state_t *state = (full_laser_state_t*) calloc(1, sizeof(full_laser_state_t));
    
    state->no_bins = no_bins;
    state->lcm = lcm; 
    bot_glib_mainloop_attach_lcm (state->lcm);
    state->param = bot_param_new_from_server(state->lcm, 1);
    
    state->on_update = NULL;
    if(callback != NULL){
        state->on_update = (on_full_laser_update_handler_t *) calloc(1, sizeof(on_full_laser_update_handler_t));
        state->on_update->callback_func = callback;
        state->on_update->user = user;
    }
    
    state->last_front_laser = NULL;
    state->last_rear_laser = NULL;
    state->frames = bot_frames_get_global (state->lcm, state->param);

    state->readings = (float *) calloc(no_bins, sizeof(float));

    state->full_laser = (bot_core_planar_lidar_t *) calloc(1, sizeof(bot_core_planar_lidar_t));

    //should be changed 
    bot_core_planar_lidar_t_subscribe(state->lcm, "SKIRT_FRONT", on_front_laser, state);
    bot_core_planar_lidar_t_subscribe(state->lcm, "SKIRT_REAR", on_rear_laser, state);
}
