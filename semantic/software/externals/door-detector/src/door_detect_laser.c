//Use laser to detect doorways.

#include "door_detect_laser.h"


void destroy_xy(xy_t *point){
    free(point);
}

void destroy_line(line_t *line){
    free(line);
}

void destroy_door(doorway_t *door){
    free(door);
}

void destroy_doors(doors_t *doors){
    if(doors->doors != NULL)
        free(doors->doors);
    free(doors);
}

//Global variable
GList *line_segments;


double calc_dist(double gradient, double intersect, double x, double y)
{
    double a = gradient;
    int b = -1;
    double c = intersect;
    double dist = fabs(a * x + b * y + c) / (double)hypot(a, b);
    return dist;
}

double calc_gradient(double start[2], double end[2])
{
    double gradient = (double)(end[1] - start[1]) / (end[0] - start[0]);
    return gradient;
}

double calc_intersect(double start[2], double gradient)
{
    double intersect = start[1] - gradient * start[0];
    return intersect;
}

//Different templates for doorway
//Reference: "Approaches to mobile robot localization in indoor environments"

doorway_t * findParLines(line_t *l1, line_t *l2, bot_core_planar_lidar_t *laser, BotFrames *frame)
{
  
    double length1 = hypot(l1->start_xy[0] - l1->end_xy[0], l1->start_xy[1] - l1->end_xy[1]);
    double length2 = hypot(l2->start_xy[0] - l2->end_xy[0], l2->start_xy[1] - l2->end_xy[1]);

    //If line segments are too short
    if(length1 < 0.2 || length2 < 0.2)
        return NULL;
  
    double start1_x = l1->start_xy[0];
    double start1_y = l1->start_xy[1];
    double end1_x = l1->end_xy[0];
    double end1_y = l1->end_xy[1];

    double start2_x = l2->start_xy[0];
    double start2_y = l2->start_xy[1];
    double end2_x = l2->end_xy[0];
    double end2_y = l2->end_xy[1];

    /*
     * Some constants to determine whether the two lines can form a doorway.
     * Can play with constant to improve accuracy
     */

    //dist from start1 to second line
    double dis;
    dis = calc_dist(l2->gradient, l2->intersect, start1_x, start1_y);
    if(dis > 0.15)
        return NULL;

    //end1 to second line
    dis = calc_dist(l2->gradient, l2->intersect, end1_x, end1_y);
    if(dis > 0.15)
        return NULL;

    //dist from start2 to first line
    dis = calc_dist(l1->gradient, l1->intersect, start2_x, start2_y);
    if(dis > 0.15)
        return NULL;

    //end2 to first line
    dis = calc_dist(l1->gradient, l1->intersect, end2_x, end2_y);
    if(dis > 0.15)
        return NULL;

    //If gradient differ to much
    if(fabs(l1->gradient - l2->gradient) > 0.1)
        return NULL;

    //If can continue until here, two lines are approximately at the same line, check dist
    //between points
    double dist1 = hypot(l1->start_xy[0] - l2->end_xy[0], l1->start_xy[1] - l2->end_xy[1]);
    double dist2 = hypot(l1->end_xy[0] - l2->start_xy[0], l1->end_xy[1] - l2->start_xy[1]);
  
    if(!(dist1 > 0.5 && dist1 < 1.5) && !(dist2 > 0.5 && dist2 < 1.5))
        return NULL;
  
    //Check whether half of points between start_index & end_index are 0.2m away from the door line
    int start = -1;
    int end = -1;
    double start_coord[2];
    double end_coord[2];


    double dist[4];
    dist[0] = hypot(l1->start_xy[0] - l2->start_xy[0], l1->start_xy[1] - l2->start_xy[1]);
    dist[1] = hypot(l1->start_xy[0] - l2->end_xy[0], l1->start_xy[1] - l2->end_xy[1]);
    dist[2] = hypot(l1->end_xy[0] - l2->start_xy[0], l1->end_xy[1] - l2->start_xy[1]);
    dist[3] = hypot(l1->end_xy[0] - l2->end_xy[0], l1->end_xy[1] - l2->end_xy[1]);

    double m = dist[0];
    int min_index = 0;

    for(int i = 1; i < 4;i++){
        if(dist[i] < m){
            m = dist[i];
            min_index = i;
        }
    }

    if(min_index == 0){
        if(l1->start_index > l2->start_index){
            start = l2->start_index;
            end = l1->start_index;
        }
        else{
            start = l1->start_index;
            end = l2->start_index;
        }
        start_coord[0] = l2->start_xy[0];
        start_coord[1] = l2->start_xy[1];
        end_coord[0] = l1->start_xy[0];
        end_coord[1] = l1->start_xy[1];
    }
    if(min_index == 1){
        if(l1->start_index > l2->end_index){
            start = l2->end_index;
            end = l1->start_index;
        }
        else{
            start = l1->start_index;
            end = l2->end_index;
        }
        start_coord[0] = l2->end_xy[0];
        start_coord[1] = l2->end_xy[1];
        end_coord[0] = l1->start_xy[0];
        end_coord[1] = l1->start_xy[1];
    }
    if(min_index == 2){
        if(l1->end_index > l2->start_index){
            start = l2->start_index;
            end = l1->end_index;
        }
        else{
            start = l1->end_index;
            end = l2->start_index;
        }
        start_coord[0] = l2->start_xy[0];
        start_coord[1] = l2->start_xy[1];
        end_coord[0] = l1->end_xy[0];
        end_coord[1] = l1->end_xy[1];
    }
    if(min_index == 3){
        if(l1->end_index > l2->end_index){
            start = l2->end_index;
            end = l1->end_index;
        }
        else{
            start = l1->end_index;
            end = l2->end_index;
        }
        start_coord[0] = l2->end_xy[0];
        start_coord[1] = l2->end_xy[1];
        end_coord[0] = l1->end_xy[0];
        end_coord[1] = l1->end_xy[1];
    }

    if(hypot(end_coord[0] - start_coord[0], end_coord[1] - start_coord[1]) < 0.3 
       && hypot(end_coord[0] - start_coord[0], end_coord[1] - start_coord[1]) > 2)
        return NULL;


    /*
      if(l1->start_index > l2->end_index){
      start = l2->end_index;
      end = l1->start_index;
      start_coord[0] = l2->end_xy[0];
      start_coord[1] = l2->end_xy[1];
      end_coord[0] = l1->start_xy[0];
      end_coord[1] = l1->start_xy[1];
      }
      else if(l2->start_index > l1->end_index){
      start = l1->end_index;
      end = l2->start_index;
      start_coord[0] = l1->end_xy[0];
      start_coord[1] = l1->end_xy[1];
      end_coord[0] = l2->start_xy[0];
      end_coord[1] = l2->start_xy[1];
      }
    */

    //Ensure most of points between start & end index are on the other side
    double p[3];
    double transition_matrix[12];
    bot_frames_get_trans_mat_4x4_with_utime(frame, "body", "local", laser->utime, transition_matrix);
    p[0] = transition_matrix[3];
    p[1] = transition_matrix[7];
    p[2] = 0;

    double d1 = hypot(p[0] - start_coord[0], p[1] - start_coord[1]);
    double d2 = hypot(p[0] - end_coord[0], p[1] - end_coord[1]);
    double min_dist;
    if(d1 <= d2)
        min_dist = d1;
    else
        min_dist = d2;

    int c = 0;
    for(int i = start;i <= end;i++){
        if(laser->ranges[i] > min_dist)
            c++;
    }

    if(c <= (double)(end - start) * 0.8 )
        return NULL;


    int count = 0;
    int count_far = 0;
    for(int i = start;i <= end;i++){
        double xyz_sensor1[3];
        double theta1 = laser->rad0 + i * laser->radstep;
        xyz_sensor1[0] = laser->ranges[i] * cos(theta1);
        xyz_sensor1[1] = laser->ranges[i] * sin(theta1);
        xyz_sensor1[2] = 0;
        double xyz_local1[3];
        bot_frames_transform_vec(frame, "SKIRT_FRONT", "local", xyz_sensor1, xyz_local1);
        //Distance between the point & the line
        double dist = calc_dist(l1->gradient, l1->intersect, xyz_local1[0], xyz_local1[1]);
        //0.2m apart from the doorway line
        if(dist > 0.15)
            count++;
    }

    if(end - start < 20)
        return NULL;

    if(count >= (end - start) / 2){// && count_far >= (end - start) / 10){
        doorway_t *doorway = (doorway_t *)calloc(1, sizeof(doorway_t));
        doorway->start[0] = start_coord[0];
        doorway->start[1] = start_coord[1];
        doorway->end[0] = end_coord[0];
        doorway->end[1] = end_coord[1];
        doorway->length = hypot(end_coord[0] - start_coord[0], end_coord[1] - start_coord[1]);
        doorway->utime = laser->utime;
        return doorway;
    }
    else
        return NULL;
}



doorway_t *findOutRightLines(line_t *l1, line_t *l2, bot_core_planar_lidar_t *laser, BotFrames *frame){


    double length1 = hypot(l1->start_xy[0] - l1->end_xy[0], l1->start_xy[1] - l1->end_xy[1]);
    double length2 = hypot(l2->start_xy[0] - l2->end_xy[0], l2->start_xy[1] - l2->end_xy[1]);

    //If line segments are too short
    if(length1 < 0.2 || length2 < 0.2)
        return NULL;

  
    double d1;
    double d2;
    d1 = calc_dist(l1->gradient, l1->intersect, l2->start_xy[0], l2->start_xy[1]);
    d2 = calc_dist(l1->gradient, l1->intersect, l2->end_xy[0], l2->end_xy[1]);
  
    line_t doorway_line;
    line_t wing_line;
    int updated = 0;
    double point_on_doorway_line[2];

    //Check one point is on doorway line
    if((d1 < 0.15 && d2 > 0.3) || (d1 > 0.3 && d2 < 0.15)){
        doorway_line.start_xy[0] = l1->start_xy[0];
        doorway_line.start_xy[1] = l1->start_xy[1];
        doorway_line.end_xy[0] = l1->end_xy[0];
        doorway_line.end_xy[1] = l1->end_xy[1];
        doorway_line.gradient = l1->gradient;
        doorway_line.intersect = l1->intersect;

        wing_line.start_xy[0] = l2->start_xy[0];
        wing_line.start_xy[1] = l2->start_xy[1];
        wing_line.end_xy[0] = l2->end_xy[0];
        wing_line.end_xy[1] = l2->end_xy[1];
        wing_line.gradient = l2->gradient;
        wing_line.intersect = l2->intersect;

        updated = 1;
    
        if(d1 < d2){
            point_on_doorway_line[0] = l2->start_xy[0];
            point_on_doorway_line[1] = l2->start_xy[1];
        }
        else{
            point_on_doorway_line[0] = l2->end_xy[0];
            point_on_doorway_line[1] = l2->end_xy[1];
        }
    }

    d1 = calc_dist(l2->gradient, l2->intersect, l1->start_xy[0], l1->start_xy[1]);
    d2 = calc_dist(l2->gradient, l2->intersect, l1->end_xy[0], l1->end_xy[1]);

    if((d1 < 0.15 && d2 > 0.5) || (d1 > 0.5 && d2 < 0.15)){
        doorway_line.start_xy[0] = l2->start_xy[0];
        doorway_line.start_xy[1] = l2->start_xy[1];
        doorway_line.end_xy[0] = l2->end_xy[0];
        doorway_line.end_xy[1] = l2->end_xy[1];
        doorway_line.gradient = l2->gradient;
        doorway_line.intersect = l2->intersect;

        wing_line.start_xy[0] = l1->start_xy[0];
        wing_line.start_xy[1] = l1->start_xy[1];
        wing_line.end_xy[0] = l1->end_xy[0];
        wing_line.end_xy[1] = l1->end_xy[1];
        wing_line.gradient = l1->gradient;
        wing_line.intersect = l1->intersect;

        updated = 1;

        if(d1 < d2){
            point_on_doorway_line[0] = l1->start_xy[0];
            point_on_doorway_line[1] = l1->start_xy[1];
        }
        else{
            point_on_doorway_line[0] = l1->end_xy[0];
            point_on_doorway_line[1] = l1->end_xy[1];
        }
    }

    if(!updated)
        return NULL;
    else{
        double width;
        double doorway_point[2];
        double dist1 = hypot(point_on_doorway_line[0] - doorway_line.start_xy[0], 
                             point_on_doorway_line[1] - doorway_line.start_xy[1]);
        double dist2 = hypot(point_on_doorway_line[0] - doorway_line.end_xy[0], 
                             point_on_doorway_line[1] - doorway_line.end_xy[1]);
        if(dist1 < dist2){
            width = dist1;
            doorway_point[0] = doorway_line.start_xy[0];
            doorway_point[1] = doorway_line.start_xy[1];
        }
        else{
            width = dist2;
            doorway_point[0] = doorway_line.end_xy[0];
            doorway_point[1] = doorway_line.end_xy[1];
        }

        double wing_length = hypot(wing_line.start_xy[0] - wing_line.end_xy[0],
                                   wing_line.start_xy[1] - wing_line.end_xy[1]);

        //Check wing length & door width is approximately the same(within 0.2m)
        if(fabs(width - wing_length) < 0.1 * width && fabs(width - wing_length) < 0.1 * wing_length
           && width > 0.5 && width < 1.2 && wing_length > 0.5 && wing_length < 1.2){
            doorway_t *door = (doorway_t *)calloc(1, sizeof(doorway_t));
            door->start[0] = doorway_point[0];
            door->start[1] = doorway_point[1];
            door->end[0] = point_on_doorway_line[0];
            door->end[1] = point_on_doorway_line[1];
            door->length = hypot(door->end[0] - door->start[0], 
                                 door->end[1] - door->start[1]);
            door->utime = laser->utime;
            return door;
        }
        else
            return NULL;
    }
}




//perform a regression fit of a straight line to a set of points xy
//reference: http://mathpages.com/home/kmath110.htm
line_t *extract_line_from_points(bot_core_planar_lidar_t *laser, int start_index, int end_index, 
                                 BotFrames *frame)
{
    xy_t points[end_index - start_index + 1];
    double sumx = 0;
    double sumy = 0;
    for(int i = start_index;i <= end_index;i++){
        double xyz_sensor1[3];
        double theta1 = laser->rad0 + i * laser->radstep;
        xyz_sensor1[0] = laser->ranges[i] * cos(theta1);
        xyz_sensor1[1] = laser->ranges[i] * sin(theta1);
        xyz_sensor1[2] = 0;
        double xyz_local1[3];
        bot_frames_transform_vec(frame, "SKIRT_FRONT", "local", xyz_sensor1, xyz_local1);
        points[i - start_index].xy[0] = xyz_local1[0];
        points[i - start_index].xy[1] = xyz_local1[1];
        sumx += xyz_local1[0];
        sumy += xyz_local1[1];
    }
    double meanx = (double)sumx / (end_index - start_index + 1);
    double meany = (double)sumy / (end_index - start_index + 1);
  
    //move centroid to origin
    for(int i = 0;i <= end_index - start_index;i++){
        points[i].xy[0] -= meanx;
        points[i].xy[1] -= meany;
    }
  
    //calculate sum of x^2-y^2 and sum of xy and A
    double sum1 = 0;
    double sum2 = 0;
    for(int i = 0;i <= end_index - start_index;i++){
        sum1 += ((points[i].xy[0] * points[i].xy[0]) - (points[i].xy[1] * points[i].xy[1]));
        sum2 += (points[i].xy[0] * points[i].xy[1]);
    }
    double quadratic = (double)sum1 / sum2;
  
    //calculate gradient
    double gradient1 = ((0 - quadratic) + sqrt(pow(quadratic, 2) - 4 * (-1))) / 2;
    double gradient2 = ((0 - quadratic) - sqrt(pow(quadratic, 2) - 4 * (-1))) / 2;
    double gradient;

    //Determine which gradient is maximum & which is minimum
    double dist1 = 0;
    double dist2 = 0;
    for(int i = 0;i <= end_index - start_index;i++){
        dist1 += fabs(gradient1 * points[i].xy[0] - points[i].xy[1]) / sqrt(1 + gradient1 * gradient1);
        dist2 += fabs(gradient2 * points[i].xy[0] - points[i].xy[1]) / sqrt(1 + gradient2 * gradient2);
    }

    if(dist1 <= dist2)
        gradient = gradient1;
    else
        gradient = gradient2;

    //get intersect
    double intersect = meany - gradient * meanx;

    //Return the line struct
    line_t *line = (line_t *)calloc(1, sizeof(line_t));
    line->start_xy[0] = points[0].xy[0] + meanx;
    line->start_xy[1] = points[0].xy[1] + meany;
    line->end_xy[0] = points[end_index - start_index].xy[0] + meanx;
    line->end_xy[1] = points[end_index - start_index].xy[1] + meany;
    line->gradient = gradient;
    line->intersect = intersect;
    line->start_index = start_index;
    line->end_index = end_index;

    return line;
}


//split & merge algorithm to detect line segments
void split_and_merge(bot_core_planar_lidar_t *laser, int start_index, 
                     int end_index, double threshold, int count, BotFrames *frames)//, GList *line_segments)
{

    if(end_index - start_index < count)
        return;

    //Fit a line to points
    line_t *line = extract_line_from_points(laser, start_index, end_index, frames);
  
    double max_dist = -1;
    int index;
    for(int i = start_index; i <= end_index;i++){
        double xyz_sensor1[3];
        double theta1 = laser->rad0 + i * laser->radstep;
        xyz_sensor1[0] = laser->ranges[i] * cos(theta1);
        xyz_sensor1[1] = laser->ranges[i] * sin(theta1);
        xyz_sensor1[2] = 0;
        double xyz_local1[3];
        bot_frames_transform_vec(frames, "SKIRT_FRONT", "local", xyz_sensor1, xyz_local1);
        double dist = calc_dist(line->gradient, line->intersect, xyz_local1[0], xyz_local1[1]);
        if(dist > max_dist){
            max_dist = dist;
            index = i;
        }
    }

    //If distance is too large, split these points into two sets
    if(max_dist > threshold){
        free(line);
        split_and_merge(laser, start_index, index - 1, threshold, count, frames);//, line_segments);
        split_and_merge(laser, index + 1, end_index, threshold, count, frames);//, line_segments);
    }
    else{
        line_segments = g_list_prepend(line_segments, line);
        return;
    }
}


doors_t *detect_doors(bot_core_planar_lidar_t *laser, BotFrames *frames)
{
  
    split_and_merge(laser, 0, laser->nranges, 0.05, 25, frames);//, line_segments);

    GList *candidates = NULL;
    int doorway_num = 0;

    for(int i = 0;i < g_list_length(line_segments);i++){
        line_t *line = (line_t *)g_list_nth(line_segments, i)->data;
    
        for(int j = i + 1;j < g_list_length(line_segments);j++){
            line_t *line2 = (line_t *)g_list_nth(line_segments, j)->data;
            doorway_t *par = findParLines(line, line2, laser, frames);
            doorway_t *second = findOutRightLines(line, line2, laser, frames);
            if(par != NULL){
	
                //Find smallest dist between points
                double dist[4];
                dist[0] = hypot(line->start_xy[0] - line2->start_xy[0], line->start_xy[1] - line2->start_xy[1]);
                dist[1] = hypot(line->start_xy[0] - line2->end_xy[0], line->start_xy[1] - line2->end_xy[1]);
                dist[2] = hypot(line->end_xy[0] - line2->start_xy[0], line->end_xy[1] - line2->start_xy[1]);
                dist[3] = hypot(line->end_xy[0] - line2->end_xy[0], line->end_xy[1] - line2->end_xy[1]);

                double min = dist[0];
                int index = 0;
                for(int i = 1;i < 4;i++){
                    if(dist[i] < min){
                        min = dist[i];
                        index = i;
                    }
                }

                double p1[2];
                double p2[2];
                if(index == 0){
                    p1[0] = line->start_xy[0];
                    p1[1] = line->start_xy[1];
                    p2[0] = line2->start_xy[0];
                    p2[1] = line2->start_xy[1];
                }
                else if(index == 1){
                    p1[0] = line->start_xy[0];
                    p1[1] = line->start_xy[1];
                    p2[0] = line2->end_xy[0];
                    p2[1] = line2->end_xy[1];
                }
                else if(index == 2){
                    p1[0] = line->end_xy[0];
                    p1[1] = line->end_xy[1];
                    p2[0] = line2->start_xy[0];
                    p2[1] = line2->start_xy[1];
                }
                else if(index == 3){
                    p1[0] = line->end_xy[0];
                    p1[1] = line->end_xy[1];
                    p2[0] = line2->end_xy[0];
                    p2[1] = line2->end_xy[1];
                }


                doorway_t *cand = (doorway_t *)calloc(1, sizeof(doorway_t));
                cand->start[0] = par->start[0];
                cand->start[1] = par->start[1];
                cand->end[0] = par->end[0];
                cand->end[1] = par->end[1];
                cand->length = par->length;

                candidates = g_list_prepend(candidates, cand);

                free(par);
            }
      
            if(second != NULL){
                doorway_t *cand = (doorway_t *)calloc(1, sizeof(doorway_t));
                cand->start[0] = second->start[0];
                cand->start[1] = second->start[1];
                cand->end[0] = second->end[0];
                cand->end[1] = second->end[1];
                cand->length = second->length;

                candidates = g_list_prepend(candidates, cand);

                free(second);
            }
        }
    }

    if(line_segments != NULL){
        g_list_free(line_segments);
        line_segments = NULL;
    }

    doors_t * doors = (doors_t *)calloc(1, sizeof(doors_t));
    doors->num = g_list_length(candidates);
    doors->doors = (doorway_t *)calloc(doors->num, sizeof(doorway_t));
    for(int i = 0;i < doors->num;i++){
        doorway_t *second = (doorway_t *)g_list_nth(candidates, i)->data;
        double local[3];
        local[0] = second->start[0];
        local[1] = second->start[1];
        local[2] = 0;
        double local_end[3];
        local_end[0] = second->end[0];
        local_end[1] = second->end[1];
        local_end[2] = 0;
        /*
          double r1[3];
          double r2[3];
          bot_frames_transform_vec(frames, "local", "body", local, r1);
          bot_frames_transform_vec(frames, "local", "body", local_end, r2);
        */
        doors->doors[i].start[0] = local[0];
        doors->doors[i].start[1] = local[1];
        doors->doors[i].end[0] = local_end[0];
        doors->doors[i].end[1] = local_end[1];
        doors->doors[i].length = second->length;
        doors->doors[i].utime = laser->utime;
    }

    g_list_free(candidates);
    candidates = NULL;
  
    return doors;
}


int check_intersect(double start[2], double end[2], double min_dist, double max_dist)
{

    //Must have different sign in y
    if(start[1] * end[1] > 0)
        return 0;


    double gradient = calc_gradient(start, end);
    double intersect = calc_intersect(start, gradient);

    double value = (0 - (double)intersect / gradient);

    fprintf(stderr, "Distance: %f\n", value);

    if(value <= max_dist && value >= min_dist)
        return 1;
    else
        return 0;
}

int check_intersect_between_poses(double pos1[2], double pos2[2], double start[2], double end[2])
{    
    double gradient_p = (pos2[1] - pos1[1]) / (pos2[0] - pos1[0]);
    double intersect_p = pos1[1] - gradient_p * pos1[0];
  
    double gradient_d = (end[1] - start[1]) / (end[0] - start[0]);
    double intersect_d = start[1] - gradient_d * start[0];

    //If parallel
    if(gradient_p == gradient_d)
        return 0;

    double x = (intersect_d - intersect_p) / (gradient_p - gradient_d);
    double y = gradient_d * x + intersect_d;

    //if both points on same side
    if((start[0] - x) * (end[0] - x) > 0)
        return 0;

    //convert to parametric form 
    double x1 = pos1[0]; 
    double t1 = (pos2[0]-x1); 
    double t = (x-x1); 
    
    if(t/t1  > 0 && t/t1 < 1){
        //the intersection is between the two poses
        return 1;
    }
    return 0;
}

int check_intersect_local(double pos[2], double heading, double start[2], double end[2], 
                          double min_dist, double max_dist)
{
    double gradient = tan(heading);
    double intersect = pos[1] - gradient * pos[0];
  
    double gradient1 = (end[1] - start[1]) / (end[0] - start[0]);
    double intersect1 = start[1] - gradient1 * start[0];

    //If parallel
    if(gradient == gradient1)
        return 0;

    double x = (intersect1 - intersect) / (gradient - gradient1);
    double y = gradient * x + intersect;

    //if both points on same side
    if((start[0] - x) * (end[0] - x) > 0)
        return 0;
  
    //Check the intersect is in front or behind
    double diff = x - pos[0];
    double dist = hypot(x - pos[0], y - pos[1]);

    //in front
    if(diff * cos(heading) >= 0){
        if(dist >= min_dist && dist <= max_dist)
            return 1;
        else
            return 0;
    }

    //behind
    else{
        if(min_dist >= 0)
            return 0;
        else if(dist <= fabs(min_dist))
            return 1;
        else 
            return 0;
    }
}


doorway_t *detect_doorway_infront(object_door_list_t *msg, double pos[2], double heading)
{
    if(msg == NULL)
        return NULL;
    for(int i = 0;i < msg->no_door;i++){
        if(check_intersect_local(pos, heading, msg->doors[i].point1, msg->doors[i].point2, -0.2, 0.8)){
            doorway_t *door = (doorway_t *) calloc(1, sizeof(doorway_t));
            door->utime = msg->utime;
            door->start[0] = msg->doors[i].point1[0];
            door->start[1] = msg->doors[i].point1[1];
            door->end[0] = msg->doors[i].point2[0];
            door->end[1] = msg->doors[i].point2[1];
            return door;
        }
    }
    return NULL;
}

doorway_t *detect_doorway_infront_at_dist(object_door_list_t *msg, double pos[2], double heading, double min_dist, double max_dist)
{
    if(msg == NULL)
        return NULL;
    for(int i = 0;i < msg->no_door;i++){
        if(check_intersect_local(pos, heading, msg->doors[i].point1, msg->doors[i].point2, min_dist, max_dist)){
            doorway_t *door = (doorway_t *) calloc(1, sizeof(doorway_t));
            door->utime = msg->utime;
            door->start[0] = msg->doors[i].point1[0];
            door->start[1] = msg->doors[i].point1[1];
            door->end[0] = msg->doors[i].point2[0];
            door->end[1] = msg->doors[i].point2[1];
            return door;
        }
    }
    return NULL;
}


doorway_t *detect_doorway_at_pos(object_door_list_t *msg, double pos[2], double heading, double min_dist, double max_dist)
{
    if(msg == NULL)
        return NULL;
  
    for(int i = 0; i < msg->no_door;i++){
        if (check_intersect_local(pos, heading, msg->doors[i].point1, msg->doors[i].point2, min_dist, max_dist)){
            doorway_t *door = (doorway_t *) calloc(1, sizeof(doorway_t));
            door->utime = msg->utime;
            door->start[0] = msg->doors[i].point1[0];
            door->start[1] = msg->doors[i].point1[1];
            door->end[0] = msg->doors[i].point2[0];
            door->end[1] = msg->doors[i].point2[1];
            return door;
        }
    }

    return NULL;
}

doorway_t *detect_doorway_between_pos(object_door_list_t *msg, double pos_1[2], double pos_2[2])
{
    if(msg == NULL)
        return NULL;
  
    for(int i = 0; i < msg->no_door;i++){
        if (check_intersect_between_poses(pos_1, pos_2, msg->doors[i].point1, msg->doors[i].point2)){
            doorway_t *door = (doorway_t *) calloc(1, sizeof(doorway_t));
            door->utime = msg->utime;
            door->start[0] = msg->doors[i].point1[0];
            door->start[1] = msg->doors[i].point1[1];
            door->end[0] = msg->doors[i].point2[0];
            door->end[1] = msg->doors[i].point2[1];
            return door;
        }
    }

    return NULL;
}

