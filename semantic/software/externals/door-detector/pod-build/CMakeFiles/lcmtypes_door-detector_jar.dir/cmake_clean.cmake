FILE(REMOVE_RECURSE
  "../lcmtypes/c"
  "../lcmtypes/cpp"
  "../lcmtypes/java"
  "../lcmtypes/python"
  "CMakeFiles/lcmtypes_door-detector_jar"
  "lcmtypes_door-detector.jar"
  "../lcmtypes/java/object/door_list_t.class"
  "../lcmtypes/java/object/door_t.class"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/lcmtypes_door-detector_jar.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
