"""LCM type definitions
This file automatically generated by lcm.
DO NOT MODIFY BY HAND!!!!
"""

try:
    import cStringIO.StringIO as BytesIO
except ImportError:
    from io import BytesIO
import struct

class pixel_map_t(object):
    __slots__ = ["utime", "dimensions", "xy0", "xy1", "mpp", "compressed", "data_type", "datasize", "mapData"]

    TYPE_UNKNOWN = 0
    TYPE_FLOAT = 1
    TYPE_DOUBLE = 2
    TYPE_INT32 = 3
    TYPE_UINT32 = 4
    TYPE_INT16 = 5
    TYPE_UINT16 = 6
    TYPE_INT8 = 7
    TYPE_UINT8 = 8

    def __init__(self):
        self.utime = 0
        self.dimensions = [ 0 for dim0 in range(2) ]
        self.xy0 = [ 0.0 for dim0 in range(2) ]
        self.xy1 = [ 0.0 for dim0 in range(2) ]
        self.mpp = 0.0
        self.compressed = 0
        self.data_type = 0
        self.datasize = 0
        self.mapData = ""

    def encode(self):
        buf = BytesIO()
        buf.write(pixel_map_t._get_packed_fingerprint())
        self._encode_one(buf)
        return buf.getvalue()

    def _encode_one(self, buf):
        buf.write(struct.pack(">q", self.utime))
        buf.write(struct.pack('>2i', *self.dimensions[:2]))
        buf.write(struct.pack('>2d', *self.xy0[:2]))
        buf.write(struct.pack('>2d', *self.xy1[:2]))
        buf.write(struct.pack(">dBbi", self.mpp, self.compressed, self.data_type, self.datasize))
        buf.write(bytearray(self.mapData[:self.datasize]))

    def decode(data):
        if hasattr(data, 'read'):
            buf = data
        else:
            buf = BytesIO(data)
        if buf.read(8) != pixel_map_t._get_packed_fingerprint():
            raise ValueError("Decode error")
        return pixel_map_t._decode_one(buf)
    decode = staticmethod(decode)

    def _decode_one(buf):
        self = pixel_map_t()
        self.utime = struct.unpack(">q", buf.read(8))[0]
        self.dimensions = struct.unpack('>2i', buf.read(8))
        self.xy0 = struct.unpack('>2d', buf.read(16))
        self.xy1 = struct.unpack('>2d', buf.read(16))
        self.mpp, self.compressed, self.data_type, self.datasize = struct.unpack(">dBbi", buf.read(14))
        self.mapData = buf.read(self.datasize)
        return self
    _decode_one = staticmethod(_decode_one)

    _hash = None
    def _get_hash_recursive(parents):
        if pixel_map_t in parents: return 0
        tmphash = (0xf2584cd266709883) & 0xffffffffffffffff
        tmphash  = (((tmphash<<1)&0xffffffffffffffff)  + (tmphash>>63)) & 0xffffffffffffffff
        return tmphash
    _get_hash_recursive = staticmethod(_get_hash_recursive)
    _packed_fingerprint = None

    def _get_packed_fingerprint():
        if pixel_map_t._packed_fingerprint is None:
            pixel_map_t._packed_fingerprint = struct.pack(">Q", pixel_map_t._get_hash_recursive([]))
        return pixel_map_t._packed_fingerprint
    _get_packed_fingerprint = staticmethod(_get_packed_fingerprint)

