#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <gsl/gsl_complex.h>
#include <gsl/gsl_complex_math.h>

#include "laser_feature.h"


//helper function to convert from polar coordinate to cartesian. 
void polar_to_rect(double r, double theta, double *x, double *y){
  *x = r*cos(theta);
  *y = r*sin(theta);
  return;
}

//#1:Calculate range difference between two beams b1 and b2
double calculate_feature_range_diff(bot_core_planar_lidar_t *laser, int b1, int b2, double min, double max){
  if((b1 < 0 || b1 >= laser->nranges) || (b2 < 0 || b2 >= laser->nranges)){
    fprintf(stderr, "ERROR: beam1 or beam2 out of range.\n");
    return 0;
  }
  if((laser->ranges[b1] < min) || (laser->ranges[b2] > max)){
    //fprintf(stderr, "ERROR: Invalid beam length.\n");
    return 0;
  }
  return (double)(laser->ranges[b1] - laser->ranges[b2]);
}

//#2:Calculate average of difference between consecutive beams
double calculate_feature_avg_diff(bot_core_planar_lidar_t *laser, double min, double max){
  double sum = 0.0;
  int i1, i2;
  int valid = 0;
  for(int i = 0; i < laser->nranges - 1;i++){
    i1 = i;
    i2 = i + 1;
		
    //fprintf(stderr, "%d - %f %f - min : %f max : %f\n", i1, laser->ranges[i1], laser->ranges[i2], 
    //min, max);

    if((laser->ranges[i1] < min || laser->ranges[i1] > max) || 
       (laser->ranges[i2] < min || laser->ranges[i2] > max)){
      //fprintf(stderr, "Falied check\n");
      continue;
    }	
			
    sum += (double)fabs(laser->ranges[i1] - laser->ranges[i2]);
    valid++;
  }
	
  if(valid > 0){
    double result = sum / valid;
    //	  fprintf(stderr, "Sum : %f Size : %d => Result : %f\n", sum , valid, result);
    return (result);
  }
  fprintf(stderr,"Error - No valid beams\n");
  return 0;
}

//standard deviation of different between consecutive beams
double calculate_feature_stddev_diff(bot_core_planar_lidar_t *laser, double min, double max){
  double mean = calculate_feature_avg_diff(laser, min, max);
  double sum = 0.0;
  int valid = 0;
  for(int i = 0;i < laser->nranges;i++){
    if(laser->ranges[i] < min || laser->ranges[i] > max){
      continue;
    }
    double v = laser->ranges[i] - mean;
    sum += (v * v);
    valid++;
  }
  return sum / (double)valid;
}

//#3:average difference between CUTOFF consecutive beams
double calculate_feature_cutoff_diff(bot_core_planar_lidar_t *laser, double length, double min, double max){
  double sum = 0.0;
  int i1, i2;
  double r1, r2;
  int valid = 0;
  for(int i = 0; i < laser->nranges - 1;i++){
    i1 = i;
    i2 = i + 1;
    if(laser->ranges[i1] < min || laser->ranges[i1] > max || laser->ranges[i2] < min || laser->ranges[i2] > max){
      continue;
    }	

    if(laser->ranges[i1] > length){
      r1 = length;
    }
    else{
      r1 = laser->ranges[i1];
    }	
    if(laser->ranges[i2] > length){
      r2 = length;
    }
    else{
      r2 = laser->ranges[i2];
    }
    sum += (double)fabs(r1 - r2);
    valid++;
  }
  return (double)sum / valid;
}

//#5:area of the region covered by lasers using cross product
//reference: www.softsurfer.com/Archive/algorithm_0101/algorithm_0101.htm#Polygons
double calculate_feature_area_by_crossproduct(bot_core_planar_lidar_t *laser, double min, double max){
  double theta;
  double area = 0.0;
  double x;
  double y;
  double coordinate[laser->nranges][2];
  for(int i = 0; i < laser->nranges; i++){
    if(laser->ranges[i] < min || laser->ranges[i] > max){
      continue;
    }
    theta = laser->rad0 + i * laser->radstep;
    polar_to_rect(laser->ranges[i], theta, &x, &y);
    coordinate[i][0] = x;
    coordinate[i][1] = y;
  }
  int i, j, k;
  for(i = 1, j = 2, k = 0;i < laser->nranges - 1; i++, j++, k++){
    if((laser->ranges[i]) < min || (laser->ranges[i] > max) 
       || (laser->ranges[j] < min) || (laser->ranges[j] > max)
       || (laser->ranges[k] < min) || (laser->ranges[k] > max)){
      continue;
    }
    area += fabs(coordinate[i][0]) * fabs(coordinate[j][1] - coordinate[k][1]);
  }
  area = (double)area / 2.0;
  return area;
}

//#6: perimeter of the area covered by laser
double calculate_feature_perimeter(bot_core_planar_lidar_t *laser, double min, double max){
  int next;
  double perimeter = 0.0;
  double theta1, theta2;
  double x1, x2;
  double y1, y2;
  for(int i = 0; i < laser->nranges - 1; i++){
    next = i + 1;
    if(laser->ranges[i] < min || laser->ranges[i] > max || laser->ranges[next] < min || laser->ranges[next] > max){
      continue;
    }
    theta1 = laser->rad0 + i * laser->radstep;
    theta2 = laser->rad0 + next * laser->radstep;
    polar_to_rect(laser->ranges[i], theta1, &x1, &y1);
    polar_to_rect(laser->ranges[next], theta2, &x2, &y2);
    perimeter += hypot(x1 - x2, y1 - y2);
  }
  return perimeter;
}


//#7: perimeter / area
double calculate_feature_perimeter_over_area(bot_core_planar_lidar_t *laser, double min, double max){
  return (double)calculate_feature_perimeter(laser, min, max) / calculate_feature_area_by_crossproduct(laser, min, max);
}

//#8: average of beam length
double calculate_feature_avg_beam_length(bot_core_planar_lidar_t *laser, double min, double max){
  double sum = 0.0;
  int valid = 0;
  for(int i = 0;i < laser->nranges;i++){
    if(laser->ranges[i] < min || laser->ranges[i] > max){
      continue;
    }
    sum += laser->ranges[i];
    valid++;
  }
  return (double)sum / valid;
}

//#8: standard deviation of beam length
double calculate_feature_stddev_beam_length(bot_core_planar_lidar_t *laser, double min, double max){
  double mean = calculate_feature_avg_beam_length(laser, min, max);
  double sum = 0.0;
  int valid = 0;
  for(int i = 0;i < laser->nranges;i++){
    if(laser->ranges[i] < min || laser->ranges[i] > max){
      continue;
    }
    double v = laser->ranges[i] - mean;
    sum += (v * v);
    valid++;
  }
  return (double)sum / valid;
}

//#9: mean of the distance to the centroid
double calculate_feature_mean_dist_centroid(bot_core_planar_lidar_t *laser, double min, double max){
  double cx;
  double cy;
  double sumx = 0.0;
  double sumy = 0.0;
  double theta1, theta2, x1, x2, y1, y2;
  double area = calculate_feature_area_by_crossproduct(laser, min, max);
  double coordinate[laser->nranges][2];
  int next;
  int valid = 0;
  for(int i = 0; i < laser->nranges - 1; i++){
    next = i + 1;
    if(laser->ranges[i] < min || laser->ranges[i] > max || laser->ranges[next] < min || laser->ranges[next] > max){
      continue;
    }
    theta1 = laser->rad0 + i * laser->radstep;
    theta2 = laser->rad0 + next * laser->radstep;
    polar_to_rect(laser->ranges[i], theta1, &x1, &y1);
    polar_to_rect(laser->ranges[next], theta2, &x2, &y2);
    coordinate[i][0] = x1;
    coordinate[i][1] = y1;
    coordinate[next][0] = x2; 
    coordinate[next][1] = y2;
    sumx += (x1 + x2) * (x1 * y2 - x2 * y1);
    sumy += (y1 + y2) * (x1 * y2 - x2 * y1);
    valid++;
  }
  cx = sumx / (6.0 * area);
  cy = sumy / (6.0 * area);

  //mean of distances
  double sum = 0.0;
  for(int i = 0; i < laser->nranges; i++){
    if(laser->ranges[i] < min || laser->ranges[i] > max){
      continue;
    }
    sum += hypot(coordinate[i][0] - cx, coordinate[i][1] - cy);
  }
  return (double)sum / valid;
}

//max distance to centroid
double calculate_feature_max_dist_centroid(bot_core_planar_lidar_t *laser, double min, double max){
  double cx;
  double cy;
  double sumx = 0.0;
  double sumy = 0.0;
  double theta1, theta2, x1, x2, y1, y2;
  double area = calculate_feature_area_by_crossproduct(laser, min, max);
  double coordinate[laser->nranges][2];
  int next;
  int valid = 0;
  for(int i = 0; i < laser->nranges - 1; i++){
    next = i + 1;
    if(laser->ranges[i] < min || laser->ranges[i] > max || laser->ranges[next] < min || laser->ranges[next] > max){
      continue;
    }
    theta1 = laser->rad0 + i * laser->radstep;
    theta2 = laser->rad0 + next * laser->radstep;
    polar_to_rect(laser->ranges[i], theta1, &x1, &y1);
    polar_to_rect(laser->ranges[next], theta2, &x2, &y2);
    coordinate[i][0] = x1;
    coordinate[i][1] = y1;
    coordinate[next][0] = x2; 
    coordinate[next][1] = y2;
    sumx += (x1 + x2) * (x1 * y2 - x2 * y1);
    sumy += (y1 + y2) * (x1 * y2 - x2 * y1);
    valid++;
  }
  cx = sumx / (6.0 * area);
  cy = sumy / (6.0 * area);

  //max of distances
  double m = 0.0;
  for(int i = 0; i < laser->nranges; i++){
    if(laser->ranges[i] < min || laser->ranges[i] > max){
      continue;
    }
    double dist = hypot(coordinate[i][0] - cx, coordinate[i][1] - cy);
    if(dist > m){
      m = dist;
    }
  }
  return m;
}


//#9_sd: standard deviation of the distance to the centroid
double calculate_feature_std_dist_centroid(bot_core_planar_lidar_t *laser, double min, double max){
  double cx;
  double cy;
  double sumx = 0.0;
  double sumy = 0.0;
  double theta1, theta2, x1, x2, y1, y2;
  double area = calculate_feature_area_by_crossproduct(laser, min, max);
  double coordinate[laser->nranges][2];
  int next;
  int valid = 0;
  for(int i = 0; i < laser->nranges - 1; i++){
    next = i + 1;
    if(laser->ranges[i] < min || laser->ranges[i] > max || laser->ranges[next] < min || laser->ranges[next] > max){
      continue;
    }
    theta1 = laser->rad0 + i * laser->radstep;
    theta2 = laser->rad0 + next * laser->radstep;
    polar_to_rect(laser->ranges[i], theta1, &x1, &y1);
    polar_to_rect(laser->ranges[next], theta2, &x2, &y2);
    coordinate[i][0] = x1;
    coordinate[i][1] = y1;
    coordinate[next][0] = x2; 
    coordinate[next][1] = y2;
    sumx += (x1 + x2) * (x1 * y2 - x2 * y1);
    sumy += (y1 + y2) * (x1 * y2 - x2 * y1);
    valid++;
  }
  cx = sumx / (6.0 * area);
  cy = sumy / (6.0 * area);

  //mean of distances from feature #9
  double mean = calculate_feature_mean_dist_centroid(laser, min, max);
  double sum = 0.0;
  double v;
  for(int i = 0; i < laser->nranges; i++){
    if(laser->ranges[i] < min || laser->ranges[i] > max){
      continue;
    }
    v = hypot(coordinate[i][0] - cx, coordinate[i][1] - cy) - mean;
    v = v * v;
    sum += v;
  }
  double std = sum / ((double)valid);
  std = sqrt(std);
  return std;
}


//#10: number N of sides to a regular digital polygon (Computer and robot vision VI, p61)
double calculate_feature_sides_to_polygon(bot_core_planar_lidar_t *laser, double min, double max){
  double ratio = (double)calculate_feature_mean_dist_centroid(laser, min, max) / calculate_feature_std_dist_centroid(laser, min, max);
  double n = 1.4111 * pow(ratio, 0.4724);
  return n;
}

//TODO: 12


//#13: No. of gaps, threhold = 0.5
double calculate_feature_no_gaps(bot_core_planar_lidar_t *laser, double min, double max, double threhold){
  int gaps = 0;
  int next;
  double diff;
  for(int i = 0;i < laser->nranges - 1;i++){
    next = i + 1;
    if(laser->ranges[i] < min || laser->ranges[i] > max || laser->ranges[next] < min || laser->ranges[next] > max){
      continue;
    }
    diff = fabs(laser->ranges[i] - laser->ranges[next]);
    if(diff > threhold){
      gaps++;
    }
  }
  return (double)gaps;
}

//TODO #14: No. of points that lie in lines extracted from scans

//#15:
complex_result_t *calculate_feature_fourier_descriptor(bot_core_planar_lidar_t *laser, double min, double max, int no_of_fourier_coefficient){	
  int n_coefficients = no_of_fourier_coefficient;
  int size = no_of_fourier_coefficient * 2 + 1;
  gsl_complex coefficients[size];
  //	*size_result = size;
  gsl_complex descriptors[size];
  complex_result_t *complex_result = (complex_result_t *) calloc(1, sizeof(complex_result_t));
  gsl_complex V[laser->nranges];//  = (gsl_complex *) calloc(laser->nranges, sizeof(gsl_complex));
  double x; 
  double y;
  int index;
  double theta;
  for (int j = 0; j < laser->nranges; j++) {
    theta = laser->rad0 + j * laser->radstep;
    polar_to_rect(laser->ranges[j], theta, &x, &y);
    V[j] = gsl_complex_rect(x, y);
  }

	
  // perimeter
  // problems with division by 0
  double perimeter = 0.000000000000000000000001;
  gsl_complex diff;
  for(int i = 0; i < laser->nranges - 1; i++) {
    int j = i + 1;
    diff = gsl_complex_sub(V[j], V[i]);
    perimeter += gsl_complex_abs(diff);
  }

  // differences
  gsl_complex diff_x[laser->nranges - 1];
  double diff_x_abs[laser->nranges - 1];
  for(int i = 0; i < laser->nranges - 1; i++) {
    int j = i + 1;
    diff_x[i] = gsl_complex_sub(V[j], V[i]);
    diff_x_abs[i] = gsl_complex_abs(diff_x[i]);
    // problems with 0 values
    diff_x_abs[i] = diff_x_abs[i] + 0.000000000000000000000001;
  }

  // normalized differences
  gsl_complex diff_z[laser->nranges - 1];
  for(int i = 0; i < laser->nranges - 1; i++) {
    diff_z[i] = gsl_complex_div_real( diff_x[i], diff_x_abs[i] );
  }
  // values of tk
  double tk[laser->nranges - 1];
  tk[0] = 0;

  for(int k = 1; k < laser->nranges - 1; k++) {
    double acc = 0;
    for(int i = 0; i < k; i++) {
      acc += gsl_complex_abs( diff_x[i] );
    }
    tk[k] = acc;
  }

  // linienschwerpunkte
  gsl_complex a, b, c, d;
  d = gsl_complex_rect(0, 0);
  for (int k = 0; k < laser->nranges - 1; k++) {
    int l= k + 1;
    a = gsl_complex_add(V[k], V[l]);
    b = gsl_complex_mul_real( a, diff_x_abs[k] );
    d = gsl_complex_add( d, b);
  }
  coefficients[n_coefficients] = gsl_complex_div_real(d, 2.0*perimeter);

  //cout << "coeff[0]:" << GSL_REAL(coefficients[n_coefficients]) << "+"
  //	 << GSL_IMAG(coefficients[n_coefficients]) << "i" << endl;

  // coefficients
  for (int num = -n_coefficients; num <= n_coefficients; num++) {
    if (num != 0) {
      index = num + n_coefficients;
      d = gsl_complex_rect(0, 0);
      for (int k = 0; k < laser->nranges - 1; k++) {
	int l;
	if ( k == 0) {
	  l = laser->nranges - 2;
	}
	else {
	  l = k - 1;
	}
	a = gsl_complex_sub( diff_z[l], diff_z[k] );
	double e = - num * 2.0 * M_PI * tk[k] / perimeter;
	b = gsl_complex_polar(1, e);
	c = gsl_complex_mul(a, b);
	d = gsl_complex_add(d, c);
      }
      double factor;
      factor = perimeter / ((2.0 * M_PI * num)*(2.0 * M_PI * num));
      coefficients[index] = gsl_complex_mul_real(d, factor);
    }
  }


  //descriptors
  double arg, arg1, arg2;
  double abs1;
  double abs;
  double factor;
  double ex;
  arg1 = gsl_complex_arg(coefficients[n_coefficients + 1] );
  arg2 = gsl_complex_arg(coefficients[n_coefficients + 2] );
  abs1 = gsl_complex_abs(coefficients[n_coefficients + 1]);

  for(int i = -n_coefficients; i <= n_coefficients; i++) {
    index = i + n_coefficients;
    arg = gsl_complex_arg(coefficients[index] );
    ex = arg + (1.0 -(double)i ) * arg2 - (2.0-(double)i) * arg1;
    a = gsl_complex_polar(1.0, ex);
    abs = gsl_complex_abs(coefficients[index]);
    factor = abs / abs1;
    descriptors[index] = gsl_complex_mul_real(a, factor);
  }
  /*		if(index == 2 * n_coefficients){
		fprintf(stderr, "test last index: %f, %f\n", descriptors[index].dat[0], descriptors[index].dat[1]);
		}
  */	
  //	gsl_complex V[laser->nranges];//  = (gsl_complex *) calloc(laser->nranges, sizeof(gsl_complex));
  complex_number_t *complex = (complex_number_t *) calloc(size, sizeof(complex_number_t));
  for(int i = 0; i < size;i++){
    complex[i].real = descriptors[i].dat[0];
    complex[i].complex = descriptors[i].dat[1];
  }
  complex_result->results = complex;
  complex_result->size = size;
  return complex_result;
}
/*	
// axis of the ellipse with coefficients 1 and -1
double a_abs = gsl_complex_abs( coefficients[n_coefficients+1] );
double b_abs = gsl_complex_abs( coefficients[n_coefficients-1] );
major_axis_len = a_abs + b_abs;
minor_axis_len = fabs(a_abs - b_abs);

double a_arg, b_arg;
a_arg = gsl_complex_arg( coefficients[n_coefficients+1] );
b_arg = gsl_complex_arg( coefficients[n_coefficients-1] );

major_axis_angle = (a_arg + b_arg) / 2.0;
minor_axis_angle = major_axis_angle + M_PI_2;

polar_to_rect(major_axis_len, major_axis_angle, &major_axis.x, &major_axis.y);
polar_to_rect(minor_axis_len, minor_axis_angle, &minor_axis.x, &minor_axis.y);

major_axis.x += GSL_REAL(coefficients[n_coefficients]);
major_axis.y += GSL_IMAG(coefficients[n_coefficients]);

minor_axis.x += GSL_REAL(coefficients[n_coefficients]);
minor_axis.y += GSL_IMAG(coefficients[n_coefficients]);


// axis of the ellipse with descriptors 1 and -1
a_abs = gsl_complex_abs( descriptors[n_coefficients+1] );
b_abs = gsl_complex_abs( descriptors[n_coefficients-1] );
major_axis_len_d = a_abs + b_abs;
minor_axis_len_d = fabs(a_abs - b_abs);

a_arg = gsl_complex_arg( descriptors[n_coefficients+1] );
b_arg = gsl_complex_arg( descriptors[n_coefficients-1] );

major_axis_angle_d = (a_arg + b_arg) / 2.0;
minor_axis_angle_d = major_axis_angle_d + M_PI_2;

polar_to_rect(major_axis_len_d, major_axis_angle_d, &major_axis_d.x, &major_axis_d.y);
polar_to_rect(minor_axis_len_d, minor_axis_angle_d, &minor_axis_d.x, &minor_axis_d.y);
*/


//#16: 
complex_result_t calculate_feature_ith_fourier_descriptor(bot_core_planar_lidar_t *laser, double min, double max, int no_coefficient, int index){
  if(index > 2 * no_coefficient){
    fprintf(stderr, "Index out of range.\n");
  }
  complex_result_t *descriptors = calculate_feature_fourier_descriptor(laser, min, max, no_coefficient);
  return descriptors[index];
}

//#17 & 18:
int calculate_feature_major_minor_length_axis_coefficients(bot_core_planar_lidar_t *laser, double min, double max, double result[2]){
  int n_coefficients = 100;
  gsl_complex coefficients[n_coefficients*2 +1];
  gsl_complex V[laser->nranges];
  double x; 
  double y;
  int index;
  double theta;
  for (int j = 0; j < laser->nranges; j++) {
    theta = laser->rad0 + j * laser->radstep;
    polar_to_rect(laser->ranges[j], theta, &x, &y);
    V[j] = gsl_complex_rect(x, y);
  }

	
  // perimeter
  // problems with division by 0
  double perimeter = 0.000000000000000000000001;
  gsl_complex diff;
  for(int i = 0; i < laser->nranges - 1; i++) {
    int j = i + 1;
    diff = gsl_complex_sub(V[j], V[i]);
    perimeter += gsl_complex_abs(diff);
  }

  // differences
  gsl_complex diff_x[laser->nranges - 1];
  double diff_x_abs[laser->nranges - 1];
  for(int i = 0; i < laser->nranges - 1; i++) {
    int j = i + 1;
    diff_x[i] = gsl_complex_sub(V[j], V[i]);
    diff_x_abs[i] = gsl_complex_abs(diff_x[i]);
    // problems with 0 values
    diff_x_abs[i] = diff_x_abs[i] + 0.000000000000000000000001;
  }

  // normalized differences
  gsl_complex diff_z[laser->nranges - 1];
  for(int i = 0; i < laser->nranges - 1; i++) {
    diff_z[i] = gsl_complex_div_real( diff_x[i], diff_x_abs[i] );
  }
  // values of tk
  double tk[laser->nranges - 1];
  tk[0] = 0;

  for(int k = 1; k < laser->nranges - 1; k++) {
    double acc = 0;
    for(int i = 0; i < k; i++) {
      acc += gsl_complex_abs( diff_x[i] );
    }
    tk[k] = acc;
  }

  // linienschwerpunkte
  gsl_complex a, b, c, d;
  d = gsl_complex_rect(0, 0);
  for (int k = 0; k < laser->nranges - 1; k++) {
    int l= k + 1;
    a = gsl_complex_add(V[k], V[l]);
    b = gsl_complex_mul_real( a, diff_x_abs[k] );
    d = gsl_complex_add( d, b);
  }
  coefficients[n_coefficients] = gsl_complex_div_real(d, 2.0*perimeter);

  // coefficients
  for (int num = -n_coefficients; num <= n_coefficients; num++) {
    if (num != 0) {
      index = num + n_coefficients;
      d = gsl_complex_rect(0, 0);
      for (int k = 0; k < laser->nranges - 1; k++) {
	int l;
	if ( k == 0) {
	  l = laser->nranges - 2;
	}
	else {
	  l = k - 1;
	}
	a = gsl_complex_sub( diff_z[l], diff_z[k] );
	double e = - num * 2.0 * M_PI * tk[k] / perimeter;
	b = gsl_complex_polar(1, e);
	c = gsl_complex_mul(a, b);
	d = gsl_complex_add(d, c);
      }
      double factor;
      factor = perimeter / ((2.0 * M_PI * num)*(2.0 * M_PI * num));
      coefficients[index] = gsl_complex_mul_real(d, factor);
    }
  }
  double a_abs = gsl_complex_abs( coefficients[n_coefficients+1] );
  double b_abs = gsl_complex_abs( coefficients[n_coefficients-1] );
  double major_axis_len = a_abs + b_abs;
  double minor_axis_len = fabs(a_abs - b_abs);	
  //	gsl_complex *descriptors = (gsl_complex *) calloc(*result_size, sizeof(gsl_complex));
  //	double *result = (double *) calloc(2, sizeof(double));
  result[0] = minor_axis_len;
  result[1] = major_axis_len;
  return 0;
}

//#19:
double calculate_feature_major_to_minor_coefficient(bot_core_planar_lidar_t *laser, double min, double max){
  double result[2];
  int suc = calculate_feature_major_minor_length_axis_coefficients(laser, min, max, result);
  double ratio = (double)result[1] / result[0];
  return ratio;
}

//#20: invariants
double *calculate_feature_moments_invariants(bot_core_planar_lidar_t *laser, double min, double max){
  double coordinate[laser->nranges][2];
  double theta;
  double x, y;

  for(int i = 0;i < laser->nranges;i++){
    theta = laser->rad0 + i * laser->radstep;
    polar_to_rect(laser->ranges[i], theta, &x, &y);
    coordinate[i][0] = x;
    coordinate[i][1] = y;
  }

  // center
  double ux = 0;
  int valid = 0;
  for (int i = 0; i < laser->nranges; i++) {
    if(laser->ranges[i] < min || laser->ranges[i] > max){
      continue;
    }
    ux += coordinate[i][0];
    valid++;
  }
  ux = ux / (double)valid;

  double uy = 0;
  for (int i = 0; i < laser->nranges; i++) {
    if(laser->ranges[i] < min || laser->ranges[i] > max){
      continue;
    }
    uy += coordinate[i][1];
  }
  uy = uy / valid;

  //	fprintf(stderr, "ux & uy: %f, %f\n", ux, uy);
  // central moments
  double u11 = 0;
  for (int i = 0; i < laser->nranges; i++) {
    if(laser->ranges[i] < min || laser->ranges[i] > max){
      continue;
    }
    u11 += (coordinate[i][0] - ux)*(coordinate[i][1] - uy);
  }

  double u20 = 0;
  for (int i = 0; i < laser->nranges; i++) {
    if(laser->ranges[i] < min || laser->ranges[i] > max){
      continue;
    }
    u20 += (coordinate[i][0] - ux)*(coordinate[i][0] - ux);
  }

  //	fprintf(stderr, "u20: %f\n", u20);
  double u02 = 0;
  for (int i = 0; i < laser->nranges; i++) {
    if(laser->ranges[i] < min || laser->ranges[i] > max){
      continue;
    }
    u02 += (coordinate[i][1] - uy)*(coordinate[i][1] - uy);
  }

  double u30 = 0;
  for (int i = 0; i < laser->nranges; i++) {
    if(laser->ranges[i] < min || laser->ranges[i] > max){
      continue;
    }
    u30 += (coordinate[i][0] - ux)*(coordinate[i][0] - ux)*(coordinate[i][0] - ux);
  }

  double u12 = 0;
  for (int i = 0; i < laser->nranges; i++) {
    if(laser->ranges[i] < min || laser->ranges[i] > max){
      continue;
    }
    u12 += (coordinate[i][0] - ux)*(coordinate[i][1] - uy)*(coordinate[i][1] - uy);
  }

  double u21 = 0;
  for (int i = 0; i < laser->nranges; i++) {
    if(laser->ranges[i] < min || laser->ranges[i] > max){
      continue;
    }
    u21 += (coordinate[i][0] - ux)*(coordinate[i][0] - ux)*(coordinate[i][1] - uy);
  }


  double u03 = 0;
  for (int i = 0; i < laser->nranges; i++) {
    if(laser->ranges[i] < min || laser->ranges[i] > max){
      continue;
    }
    u03 += (coordinate[i][1] - uy)*(coordinate[i][1] - uy)*(coordinate[i][1] - uy);
  }

  double mu;
  //	fprintf(stderr, "valid: %d\n", valid);
  double u00 = valid * valid;

  // normalized central moments
  mu = ((1.0 + 1.0)/2.0) + 1.0;
  double n_u11 = u11 / pow(u00, mu);
  /*	fprintf(stderr, "u11: %f\n", u11);
	fprintf(stderr, "pow: %f\n", pow(u00, mu));
	fprintf(stderr, "n_u11: %Lf\n", n_u11);
  */
  mu = ((2.0 + 0.0)/2.0) +1.0;
  double n_u20= u20 / pow(u00, mu);

  mu = ((0.0 + 2.0) / 2.0) +1.0;
  double n_u02= u02 / pow(u00, mu);

  mu = ((1.0 + 2.0 ) / 2.0) +1.0;
  double n_u12= u12 / pow(u00, mu);

  mu = ((2.0 + 1.0)/2.0) +1.0;
  double n_u21= u21 / pow(u00, mu);

  mu = ((3.0 + 0.0)/2.0) +1.0;
  double n_u30= u30 / pow(u00, mu);

  mu = ((0.0 + 3.0)/2.0) +1.0;
  double n_u03= u03 / pow(u00, mu);

  // invariants
  double *inv = (double *) calloc(7, sizeof(double));
	
  inv[0] = n_u20 + n_u02;

  inv[1] = pow(n_u20 - n_u02, 2) + 4.0*n_u11*n_u11;

  inv[2] = pow(n_u30 - 3.0*n_u12, 2) + pow(3.0*n_u21 - n_u03, 2);

  inv[3] = pow(n_u30 + n_u12, 2) + pow(n_u21 + n_u03, 2);

  inv[4] = (n_u30 - 3.0*n_u12)*(n_u30 + n_u12)*( pow(n_u30 + n_u12,2) - 3.0*pow(n_u21+n_u03,2) )
    + (3.0*n_u21 - n_u03)*(n_u21 + n_u03)*( 3.0*pow(n_u30 + n_u12,2) - pow(n_u21+n_u03,2));

  inv[5] = (n_u20-n_u02)*( pow(n_u30+n_u12,2) - pow(n_u21+n_u03,2)) +
    4.0*n_u11*(n_u30 +n_u12)*(n_u21+n_u03);

  inv[6] = (3.0*n_u21-n_u03)*(n_u30+n_u12)*( pow(n_u30+n_u12,2) - 3.0*pow(n_u21+n_u03,2))
    + (3.0*n_u12-n_u30)*(n_u21+n_u03)*( 3.0*pow(n_u30+n_u12,2) - pow(n_u21+n_u03,2));
  return inv;
}

//#22: calculate compactness and eccentricity
int calculate_feature_compactness_and_eccentricity(bot_core_planar_lidar_t *laser, double min, double max, double result[2]){
  double coordinate[laser->nranges][2];
  double theta;
  double x, y;

  for(int i = 0;i < laser->nranges;i++){
    theta = laser->rad0 + i * laser->radstep;
    polar_to_rect(laser->ranges[i], theta, &x, &y);
    coordinate[i][0] = x;
    coordinate[i][1] = y;
  }

  // center
  double ux = 0;
  int valid = 0;
  for (int i = 0; i < laser->nranges; i++) {
    if(laser->ranges[i] < min || laser->ranges[i] > max){
      continue;
    }
    ux += coordinate[i][0];
    valid++;
  }
  ux = ux / (double)valid;

  double uy = 0;
  for (int i = 0; i < laser->nranges; i++) {
    if(laser->ranges[i] < min || laser->ranges[i] > max){
      continue;
    }
    uy += coordinate[i][1];
  }
  uy = uy / valid;

  //	fprintf(stderr, "ux & uy: %f, %f\n", ux, uy);
  // central moments
  double u11 = 0;
  for (int i = 0; i < laser->nranges; i++) {
    if(laser->ranges[i] < min || laser->ranges[i] > max){
      continue;
    }
    u11 += (coordinate[i][0] - ux)*(coordinate[i][1] - uy);
  }

  double u20 = 0;
  for (int i = 0; i < laser->nranges; i++) {
    if(laser->ranges[i] < min || laser->ranges[i] > max){
      continue;
    }
    u20 += (coordinate[i][0] - ux)*(coordinate[i][0] - ux);
  }

  //	fprintf(stderr, "u20: %f\n", u20);
  double u02 = 0;
  for (int i = 0; i < laser->nranges; i++) {
    if(laser->ranges[i] < min || laser->ranges[i] > max){
      continue;
    }
    u02 += (coordinate[i][1] - uy)*(coordinate[i][1] - uy);
  }

  double u30 = 0;
  for (int i = 0; i < laser->nranges; i++) {
    if(laser->ranges[i] < min || laser->ranges[i] > max){
      continue;
    }
    u30 += (coordinate[i][0] - ux)*(coordinate[i][0] - ux)*(coordinate[i][0] - ux);
  }

  double u12 = 0;
  for (int i = 0; i < laser->nranges; i++) {
    if(laser->ranges[i] < min || laser->ranges[i] > max){
      continue;
    }
    u12 += (coordinate[i][0] - ux)*(coordinate[i][1] - uy)*(coordinate[i][1] - uy);
  }

  double u21 = 0;
  for (int i = 0; i < laser->nranges; i++) {
    if(laser->ranges[i] < min || laser->ranges[i] > max){
      continue;
    }
    u21 += (coordinate[i][0] - ux)*(coordinate[i][0] - ux)*(coordinate[i][1] - uy);
  }


  double u03 = 0;
  for (int i = 0; i < laser->nranges; i++) {
    if(laser->ranges[i] < min || laser->ranges[i] > max){
      continue;
    }
    u03 += (coordinate[i][1] - uy)*(coordinate[i][1] - uy)*(coordinate[i][1] - uy);
  }

  double u00 = valid * valid;
  //double *result = (double *) calloc(2, sizeof(double));
  double compact = u00 / (u20 + u02);
  double ecc = sqrt(pow(u20 - u02, 2) + 4.0 * u11* u11) / (u20+ u02);
  result[0] = compact;
  result[1] = ecc;
  //return result;
  return 0;
}


//#24: 4pi*area/sqrt(perimeter)
double calculate_feature_PI_area_over_sqrt_perimeter(bot_core_planar_lidar_t *laser, double min, double max){
  double area = calculate_feature_area_by_crossproduct(laser, min, max);
  double perimeter = calculate_feature_perimeter(laser, min, max);
  double value = 4.0 * M_PI * area / (double)sqrt(perimeter);
  return value;
}

//#25: Look for the two local minima in beams length with minimum value. 
//Distance between these two minima. 
double calculate_feature_dist_local_minima(bot_core_planar_lidar_t *laser, double min, double max, double deviation){
  double min1 = 10000;
  int index1=-1;

  double first_min= 10000;
  int first_index = -1;
  int last_index =-1;
  int npoints=0;
  int start=0;
  for(int i=0; i<laser->nranges; i++) {
    // look for the first min
    if (laser->ranges[i] < (first_min - deviation) ) {
      start = 1;
      first_min = laser->ranges[i];
      first_index = i;
      npoints = 1;
      last_index = first_index;
      min1 = laser->ranges[i];
    }
    else
      if ( start &&
	   (laser->ranges[i] > (first_min - deviation)) &&
	   (laser->ranges[i] < (first_min + deviation))   ) {
	// still in minimum
	first_min = laser->ranges[i];
	last_index = i;
	npoints++;
	min1 += laser->ranges[i];
      }
      else {
	start= 0;
	// larger value
	// do nothing
      }
  }
  index1 = (last_index + first_index) / 2;
  min1 = min1 / (double)npoints;

  double min2 = 10000;
  int index2 = -1;
  double first_min2 = 10000;
  int first_index2 = -1;
  int last_index2 = -1;
  int again = 0;
  int npoints2 = 0;

  start = 0;
  int next = 0;

  for(int i=0; i<laser->nranges; i++) {
    // index not in range
    next = 0;
    if ( first_index < last_index ){
      if ( (i < first_index) || (i > last_index) ) {
	next = 1;
      }
    }
    else {
      if ( (i > last_index) && (i < first_index) ) {
	next = 1;
      }
    }
    if ( next ) {
      // look for the first min
      if ( laser->ranges[i] < (first_min2 - deviation) ) {
	start = 1;
	first_min2 = laser->ranges[i];
	first_index2 = i;
	npoints2 = 1;
	last_index2 = first_index2;
	min2 = laser->ranges[i];
      }
      else
	if ( start &&
	     (laser->ranges[i] > (first_min2 - deviation)) &&
	     (laser->ranges[i] < (first_min2 + deviation))   ) {
	  // still in minimum
	  first_min2 = laser->ranges[i];
	  last_index2 = i;
	  npoints2++;
	  min2 += laser->ranges[i];
	}
	else {
	  // larger value
	  // do nothing
	  start = 0;
	}
    }
  }

  index2 = (last_index2 + first_index2) / 2;
  min2 = min2 / (double)npoints2;

  // distance between points
  double px1, py1;
  double px2, py2;
  polar_to_rect(laser->ranges[index1], laser->rad0 + index1 * laser->radstep, &px1, &py1);
  polar_to_rect(laser->ranges[index2], laser->rad0 + index2 * laser->radstep, &px2, &py2);
  double distance = hypot(px1 - px2, py1 - py2);
  return distance;
}

//#26:
double calculate_feature_index_diff_local_minima(bot_core_planar_lidar_t *laser, double min, double max, double deviation){
  double min1 = 10000;
  int index1=-1;

  double first_min= 10000;
  int first_index = -1;
  int last_index =-1;
  int npoints=0;
  int start=0;
  for(int i=0; i<laser->nranges; i++) {
    // look for the first min
    if (laser->ranges[i] < (first_min - deviation) ) {
      start = 1;
      first_min = laser->ranges[i];
      first_index = i;
      npoints = 1;
      last_index = first_index;
      min1 = laser->ranges[i];
    }
    else
      if ( start &&
	   (laser->ranges[i] > (first_min - deviation)) &&
	   (laser->ranges[i] < (first_min + deviation))   ) {
	// still in minimum
	first_min = laser->ranges[i];
	last_index = i;
	npoints++;
	min1 += laser->ranges[i];
      }
      else {
	start= 0;
	// larger value
	// do nothing
      }
  }
  index1 = (last_index + first_index) / 2;
  min1 = min1 / (double)npoints;

  double min2 = 10000;
  int index2 = -1;
  double first_min2 = 10000;
  int first_index2 = -1;
  int last_index2 = -1;
  int again = 0;
  int npoints2 = 0;

  start = 0;
  int next = 0;

  for(int i=0; i<laser->nranges; i++) {
    // index not in range
    next = 0;
    if ( first_index < last_index ){
      if ( (i < first_index) || (i > last_index) ) {
	next = 1;
      }
    }
    else {
      if ( (i > last_index) && (i < first_index) ) {
	next = 1;
      }
    }
    if ( next ) {
      // look for the first min
      if ( laser->ranges[i] < (first_min2 - deviation) ) {
	start = 1;
	first_min2 = laser->ranges[i];
	first_index2 = i;
	npoints2 = 1;
	last_index2 = first_index2;
	min2 = laser->ranges[i];
      }
      else
	if ( start &&
	     (laser->ranges[i] > (first_min2 - deviation)) &&
	     (laser->ranges[i] < (first_min2 + deviation))   ) {
	  // still in minimum
	  first_min2 = laser->ranges[i];
	  last_index2 = i;
	  npoints2++;
	  min2 += laser->ranges[i];
	}
	else {
	  // larger value
	  // do nothing
	  start = 0;
	}
    }
  }



  index2 = (last_index2 + first_index2) / 2;
  min2 = min2 / (double)npoints2;

  // index offset between two min points
  return (double)fabs(index2 - index1);
}


//#27: average of relation between consecutive beams bi/bi+1
double calculate_feature_relation_consecutive_beams(bot_core_planar_lidar_t *laser, double min, double max){
  double m = 0.0001;
  double sum = 0.0;
  int valid = 0;
  double ratio;
  for(int i = 0; i < laser->nranges - 1;i++){
    if(laser->ranges[i] < min || laser->ranges[i] > max || laser->ranges[i + 1] < min || laser->ranges[i + 1] > max ){
      continue;
    }
    ratio = ((double)laser->ranges[i] +  m)/ (laser->ranges[i + 1] + m);
    if(ratio > 1.0){
      ratio = 1.0 / ratio;
    }
    sum += ratio;
    valid++;
  }
  return (double)sum / valid;	
}

//#28: average of length of beams / maximum length
double calculate_feature_average_length_over_maxlen(bot_core_planar_lidar_t *laser, double min, double max){
  double m = 0.0001;
  double sum = 0.0;
  int valid = 0;
  double value; 
  double maximum = 0.0;
  for(int i = 0; i < laser->nranges;i++){
    if(laser->ranges[i] < min || laser->ranges[i] > max){
      continue;
    }
    double value = laser->ranges[i] + m;
    sum += value;
    valid++;
    if(value > maximum){
      maximum = value;
    }
  }
  double mean = (double)sum / valid;
  return mean / maximum;
}


//#29: mean of the distance to the centroid / max distance
double calculate_feature_mean_dist_centroid_over_maxdist(bot_core_planar_lidar_t *laser, double min, double max){
  double mean = calculate_feature_mean_dist_centroid(laser, min, max);
  double maximum = calculate_feature_max_dist_centroid(laser, min, max);
  return mean / maximum;
}

//#30: number of relative gaps.
// One gap: b_i / b_{i+1} > threshold
double calculate_feature_no_of_relative_gaps(bot_core_planar_lidar_t *laser, double min, double max, double threhold){
  double m = 0.0001;
  int gaps = 0;
  double ratio;
  //	int count = 0;
  for(int i = 0; i < laser->nranges - 1;i++){
    if(laser->ranges[i] < min || laser->ranges[i] > max || laser->ranges[i + 1] < min || laser->ranges[i + 1] > max){
      continue;
    }
    ratio = (double)(laser->ranges[i] + m) / (laser->ranges[i + 1] + m);
    if(ratio > 1.0){
      ratio = 1.0 / ratio;
    }
    //		count++;
    if(ratio < threhold){
      gaps++;
    }
  }
  //	fprintf(stderr, "No:%d\n", count);
  return (double)gaps;
}

//#31: area*4*PI / perimeter**2 
double calculate_feature_area_PI_perimeter(bot_core_planar_lidar_t *laser, double min, double max){
  double area = calculate_feature_area_by_crossproduct(laser, min, max);
  double perimeter = calculate_feature_perimeter(laser, min, max);
  double value = 4.0 * M_PI * area / (double)pow(perimeter, 2);
  return value;
}	

//#32: kurtosis.....
double calculate_feautre_kurtosis(bot_core_planar_lidar_t *laser, double min, double max){
  double avg = calculate_feature_avg_beam_length(laser, min, max);
  double std = calculate_feature_stddev_beam_length(laser, min, max);
  double sum = 0.0;
  int valid = 0;
  for(int i = 0;i < laser->nranges;i++){
    if(laser->ranges[i] < min || laser->ranges[i] > max){
      continue;
    }
    double offset = laser->ranges[i] - avg;
    sum += pow(offset, 4);
    valid++;
  }
  double result = sum / ((double)valid * pow(std, 4));
  return result;
}

//#33: difference_gaps / num - 1
double calculate_feature_difference_gap_to_num(bot_core_planar_lidar_t *laser, double min, double max, double threshold){
  int gaps = calculate_feature_no_gaps(laser, min, max, threshold);
  int valid = 0;
  for(int i = 0; i < laser->nranges;i++){
    if(laser->ranges[i] >= min && laser->ranges[i] <= max)
      valid++;
  }
  return (double)gaps / valid;
}

//#34: relative_gaps / num - 1
double calculate_feature_relative_gap_to_num(bot_core_planar_lidar_t *laser, double min, double max, double threshold){
  int gaps = calculate_feature_no_of_relative_gaps(laser, min, max, threshold);
  int valid = 0;
  for(int i = 0; i < laser->nranges;i++){
    if(laser->ranges[i] >= min && laser->ranges[i] <= max)
      valid++;
  }
  return (double)gaps / valid;
}

void raw_laser_features_t_destroy(raw_laser_features_t *rf){
    free(rf);
}

void polygonal_laser_features_t_destroy(polygonal_laser_features_t *pf){
    if(pf->fourier_transform_descriptors != NULL && pf->fourier_transform_descriptors->results != NULL)
        free(pf->fourier_transform_descriptors->results);
    if(pf->fourier_transform_descriptors != NULL)
        free(pf->fourier_transform_descriptors);
    free(pf);
}

bot_core_planar_lidar_t *skip_beam(bot_core_planar_lidar_t *l, int no_beam_skip){
  bot_core_planar_lidar_t *laser = (bot_core_planar_lidar_t *)calloc(1, sizeof(bot_core_planar_lidar_t));
  laser->utime = l->utime;
  laser->rad0 = l->rad0;
  laser->radstep = l->radstep * no_beam_skip;
  laser->nranges = l->nranges / no_beam_skip;
  laser->ranges = (float *)calloc(laser->nranges, sizeof(float));
  for(int i = 0;i < laser->nranges;i++){
    laser->ranges[i] = l->ranges[i*no_beam_skip];
  }
  return laser;
}

//Extract all raw features
raw_laser_features_t *extract_raw_laser_features(bot_core_planar_lidar_t *l, feature_config_t config){
  //	fprintf(stderr,"Test Feature: %f\n", config.min_range);
  bot_core_planar_lidar_t *laser = (bot_core_planar_lidar_t *)calloc(1, sizeof(bot_core_planar_lidar_t));
  laser = skip_beam(l, config.no_beam_skip);
  raw_laser_features_t *raw_features = (raw_laser_features_t *) calloc(1, sizeof(raw_laser_features_t));
  raw_features->avg_difference_consecutive_beams = calculate_feature_avg_diff(laser, config.min_range, config.max_range);
  raw_features->std_dev_difference_consecutive_beams = calculate_feature_stddev_diff(laser, config.min_range, config.max_range);
  raw_features->cutoff_avg_difference_consecutive_beams = calculate_feature_cutoff_diff(laser, config.cutoff, config.min_range, config.max_range);
  raw_features->avg_beam_length = calculate_feature_avg_beam_length(laser, config.min_range, config.max_range);
  raw_features->std_dev_beam_length = calculate_feature_stddev_beam_length(laser, config.min_range, config.max_range);
  raw_features->no_gaps_in_scan_threshold_1 = calculate_feature_no_gaps(laser, config.min_range, config.max_range, config.gap_threshold_1);
  raw_features->no_relative_gaps = calculate_feature_no_of_relative_gaps(laser, config.min_range, config.max_range, config.gap_threshold_2);
  raw_features->no_gaps_to_num_beam = calculate_feature_difference_gap_to_num(laser, config.min_range, config.max_range, config.gap_threshold_1);
  raw_features->no_relative_gaps_to_num_beam = calculate_feature_relative_gap_to_num(laser, config.min_range, config.max_range, config.gap_threshold_2);
  raw_features->distance_between_two_smalled_local_minima = calculate_feature_dist_local_minima(laser, config.min_range, config.max_range, config.deviation);
  raw_features->index_distance_between_two_smalled_local_minima = calculate_feature_index_diff_local_minima(laser, config.min_range, config.max_range, config.deviation);
  raw_features->avg_ratio_consecutive_beams = calculate_feature_relation_consecutive_beams(laser, config.min_range, config.max_range);
  raw_features->avg_to_max_beam_length = calculate_feature_average_length_over_maxlen(laser, config.min_range, config.max_range);
  raw_features->size = 13;
  bot_core_planar_lidar_t_destroy(laser);
  return raw_features;
}


//Extract polygonal laser features
polygonal_laser_features_t *extract_polygonal_laser_features(bot_core_planar_lidar_t *l, feature_config_t config){
  polygonal_laser_features_t *polygonal_features = (polygonal_laser_features_t *) calloc(1, sizeof(polygonal_laser_features_t));
  bot_core_planar_lidar_t *laser = (bot_core_planar_lidar_t *)calloc(1, sizeof(bot_core_planar_lidar_t));
  laser = skip_beam(l, config.no_beam_skip);
  //fprintf(stderr, "start_area: %f\n", calculate_feature_area_by_crossproduct(l, config.min_range, config.max_range));
  polygonal_features->area = calculate_feature_area_by_crossproduct(laser, config.min_range, config.max_range);
  polygonal_features->perimeter = calculate_feature_perimeter(laser, config.min_range, config.max_range);
  polygonal_features->area_to_perimeter = calculate_feature_perimeter_over_area(laser, config.min_range, config.max_range);
  polygonal_features->PI_area_to_sqrt_perimeter = calculate_feature_PI_area_over_sqrt_perimeter(laser, config.min_range, config.max_range);
  polygonal_features->PI_area_to_perimeter_pow = calculate_feature_area_PI_perimeter(laser, config.min_range, config.max_range);
  polygonal_features->sides_of_polygon = calculate_feature_sides_to_polygon(laser, config.min_range, config.max_range);
  polygonal_features->fourier_transform_descriptors = (complex_result_t *)calloc(1, sizeof(complex_result_t ));
  polygonal_features->fourier_transform_descriptors = calculate_feature_fourier_descriptor(laser, config.min_range, config.max_range, config.no_of_fourier_coefficient);
  double result[2];
  int suc = calculate_feature_major_minor_length_axis_coefficients(laser, config.min_range, config.max_range, result);
  polygonal_features->major_ellipse_axis_length_coefficient = result[1];
  polygonal_features->minor_ellipse_axis_length_coefficient = result[0];
  polygonal_features->major_to_minor_coefficient = calculate_feature_major_to_minor_coefficient(laser, config.min_range, config.max_range);
  //polygonal_features->central_moment_invariants = (double *)calloc(1, sizeof(double));
  for(int i = 0;i < 7;i++)
    polygonal_features->central_moment_invariants[i] = calculate_feature_moments_invariants(laser, config.min_range, config.max_range)[i];
  double compact_ecc[2];
  suc = calculate_feature_compactness_and_eccentricity(laser, config.min_range, config.max_range, compact_ecc);
  polygonal_features->normalized_feature_of_compactness = compact_ecc[0];
  polygonal_features->normalized_feature_of_eccentricity = compact_ecc[1];
  polygonal_features->mean_centroid_to_shape_boundary = calculate_feature_mean_dist_centroid(laser, config.min_range, config.max_range);
  polygonal_features->max_centroid_to_shape_boundary = calculate_feature_mean_dist_centroid(laser, config.min_range, config.max_range);
  polygonal_features->std_dev_centroid_to_shape_boundry = calculate_feature_std_dist_centroid(laser, config.min_range, config.max_range);
  polygonal_features->mean_centroid_over_max_centroid = calculate_feature_mean_dist_centroid_over_maxdist(laser, config.min_range, config.max_range);
  polygonal_features->kurtosis = calculate_feautre_kurtosis(laser, config.min_range, config.max_range);
  polygonal_features->size = 23 + polygonal_features->fourier_transform_descriptors->size;
  bot_core_planar_lidar_t_destroy(laser);
  return polygonal_features;
}

void features_t_destroy(features_t *ft){
    polygonal_laser_features_t *pf= &ft->polygonal_features;
    if(pf->fourier_transform_descriptors != NULL && pf->fourier_transform_descriptors->results != NULL)
        free(pf->fourier_transform_descriptors->results);
    if(pf->fourier_transform_descriptors != NULL)
        free(pf->fourier_transform_descriptors);
    free(ft);
}


//Extract difference between two features
features_t *extract_features_difference(bot_core_planar_lidar_t *l1, bot_core_planar_lidar_t *l2, feature_config_t config){
  features_t *features = (features_t *)calloc(1, sizeof(features_t)); 

  raw_laser_features_t *raw_features1 = extract_raw_laser_features(l1, config);
  raw_laser_features_t *raw_features2 = extract_raw_laser_features(l2, config);
  polygonal_laser_features_t *polygonal_features1 = (polygonal_laser_features_t *)calloc(1, sizeof(polygonal_laser_features_t));
  polygonal_features1 = extract_polygonal_laser_features(l1, config);
  polygonal_laser_features_t *polygonal_features2 = (polygonal_laser_features_t *)calloc(1, sizeof(polygonal_laser_features_t));
  polygonal_features2 = extract_polygonal_laser_features(l2, config);
  
  features->raw_features.avg_difference_consecutive_beams = raw_features1->avg_difference_consecutive_beams - raw_features2->avg_difference_consecutive_beams;
  features->raw_features.std_dev_difference_consecutive_beams = raw_features1->avg_difference_consecutive_beams - raw_features2->avg_difference_consecutive_beams;
  features->raw_features.cutoff_avg_difference_consecutive_beams = raw_features1->cutoff_avg_difference_consecutive_beams - raw_features2->cutoff_avg_difference_consecutive_beams;
  features->raw_features.avg_beam_length = raw_features1->avg_beam_length - raw_features2->avg_beam_length;
  features->raw_features.std_dev_beam_length = raw_features1->std_dev_beam_length - raw_features2->std_dev_beam_length;
  features->raw_features.no_gaps_in_scan_threshold_1 = raw_features1->no_gaps_in_scan_threshold_1 - raw_features2->no_gaps_in_scan_threshold_1;
  features->raw_features.no_relative_gaps = raw_features1->no_relative_gaps - raw_features2->no_relative_gaps;
  features->raw_features.no_gaps_to_num_beam = raw_features1->no_gaps_to_num_beam - raw_features2->no_gaps_to_num_beam;
  features->raw_features.no_relative_gaps_to_num_beam = raw_features1->no_relative_gaps_to_num_beam - raw_features2->no_relative_gaps_to_num_beam;
  features->raw_features.distance_between_two_smalled_local_minima = raw_features1->distance_between_two_smalled_local_minima - raw_features2->distance_between_two_smalled_local_minima;
  features->raw_features.index_distance_between_two_smalled_local_minima = raw_features1->index_distance_between_two_smalled_local_minima - raw_features2->index_distance_between_two_smalled_local_minima;
  features->raw_features.avg_ratio_consecutive_beams = raw_features1->avg_ratio_consecutive_beams - raw_features2->avg_ratio_consecutive_beams;
  features->raw_features.avg_to_max_beam_length = raw_features1->avg_to_max_beam_length - raw_features2->avg_to_max_beam_length;
  features->raw_features.size = 13;
  
  //Contour
  features->polygonal_features.area = polygonal_features1->area - polygonal_features2->area;
  features->polygonal_features.perimeter = polygonal_features1->perimeter - polygonal_features2->perimeter;
  features->polygonal_features.area_to_perimeter = polygonal_features1->area_to_perimeter - polygonal_features2->area_to_perimeter;
  features->polygonal_features.PI_area_to_sqrt_perimeter = polygonal_features1->PI_area_to_sqrt_perimeter- polygonal_features2->PI_area_to_sqrt_perimeter;
  features->polygonal_features.PI_area_to_perimeter_pow = polygonal_features1->PI_area_to_perimeter_pow - polygonal_features2->PI_area_to_perimeter_pow;
  features->polygonal_features.sides_of_polygon = polygonal_features1->sides_of_polygon - polygonal_features2->sides_of_polygon;
  complex_result_t *r1;// = (complex_result_t *)calloc(1, sizeof(complex_result_t));
  complex_result_t *r2;// = (complex_result_t *)calloc(1, sizeof(complex_result_t));
  r1 = polygonal_features1->fourier_transform_descriptors;
  r2 = polygonal_features2->fourier_transform_descriptors;
  //complex_result_t *r = (complex_result_t *)calloc(1, sizeof(complex_result_t));
  features->polygonal_features.fourier_transform_descriptors = (complex_result_t *)calloc(1, sizeof(complex_result_t));
  //r->size = r1->size;
  features->polygonal_features.fourier_transform_descriptors->size = r1->size;
  //complex_number_t *result = (complex_number_t *)calloc(1, sizeof(complex_number_t));
  features->polygonal_features.fourier_transform_descriptors->results = (complex_number_t *)calloc(r1->size, sizeof(complex_number_t));
  complex_number_t *result1 = r1->results;
  complex_number_t *result2 = r2->results;
  for(int j = 0;j < r1->size;j++){
    features->polygonal_features.fourier_transform_descriptors->results[j].real = result1[j].real - result2[j].real;
    features->polygonal_features.fourier_transform_descriptors->results[j].complex = result1[j].complex - result2[j].complex;
  }
  
  features->polygonal_features.major_ellipse_axis_length_coefficient = polygonal_features1->major_ellipse_axis_length_coefficient - polygonal_features2->major_ellipse_axis_length_coefficient;
  features->polygonal_features.minor_ellipse_axis_length_coefficient = polygonal_features1->minor_ellipse_axis_length_coefficient - polygonal_features2->minor_ellipse_axis_length_coefficient;
  features->polygonal_features.major_to_minor_coefficient = polygonal_features1->major_to_minor_coefficient - polygonal_features2->major_to_minor_coefficient;
  
  //features->polygonal_features.central_moment_invariants = (double *)calloc(1, sizeof(double));
  //fprintf(stderr, "calculating central moment.\n");
  //double *cmi = (double *)calloc(1, sizeof(double));
  //double cmi[7];
  for(int j = 0; j < 7;j++)
    features->polygonal_features.central_moment_invariants[j] = polygonal_features1->central_moment_invariants[j] - polygonal_features2->central_moment_invariants[j];
  //= cmi;
   
  features->polygonal_features.normalized_feature_of_compactness = polygonal_features1->normalized_feature_of_compactness - polygonal_features2->normalized_feature_of_compactness;
  features->polygonal_features.normalized_feature_of_eccentricity = polygonal_features1->normalized_feature_of_eccentricity - polygonal_features2->normalized_feature_of_eccentricity;
  features->polygonal_features.mean_centroid_to_shape_boundary = polygonal_features1->mean_centroid_to_shape_boundary - polygonal_features2->mean_centroid_to_shape_boundary;
  features->polygonal_features.max_centroid_to_shape_boundary = polygonal_features1->max_centroid_to_shape_boundary - polygonal_features2->max_centroid_to_shape_boundary;
  features->polygonal_features.std_dev_centroid_to_shape_boundry = polygonal_features1->std_dev_centroid_to_shape_boundry - polygonal_features2->std_dev_centroid_to_shape_boundry;
  features->polygonal_features.mean_centroid_over_max_centroid = polygonal_features1->mean_centroid_over_max_centroid - polygonal_features2->mean_centroid_over_max_centroid;
  features->polygonal_features.kurtosis = polygonal_features1->kurtosis - polygonal_features2->kurtosis;
  features->polygonal_features.size = polygonal_features1->size;

  //features->size = (int *)calloc(1, sizeof(int));
  features->size = features->raw_features.size + features->polygonal_features.size;
  //fprintf(stderr, "Size: %d\n", polygonal_features1->size);
  
  raw_laser_features_t_destroy(raw_features1);
  raw_laser_features_t_destroy(raw_features2);
  polygonal_laser_features_t_destroy(polygonal_features1);
  polygonal_laser_features_t_destroy(polygonal_features2);

  return features;
}


void write_difference_features(int label, char *file_name, feature_config_t config, 
			       bot_core_planar_lidar_t *l1, bot_core_planar_lidar_t *l2)
{
  features_t *features = extract_features_difference(l1, l2, config);
  FILE *file = fopen(file_name, "a");
  raw_laser_features_t *raw_features = &features->raw_features;
  polygonal_laser_features_t *polygonal_features = &features->polygonal_features;

  fprintf(file, "%d ", label);
  fprintf(file, "%d:%f ", 1, raw_features->avg_difference_consecutive_beams);
  fprintf(file, "%d:%f ", 2, raw_features->std_dev_difference_consecutive_beams);
  fprintf(file, "%d:%f ", 3, raw_features->cutoff_avg_difference_consecutive_beams);
  fprintf(file, "%d:%f ", 4, raw_features->avg_beam_length);
  fprintf(file, "%d:%f ", 5, raw_features->std_dev_beam_length);
  fprintf(file, "%d:%f ", 6, raw_features->no_gaps_in_scan_threshold_1);
  fprintf(file, "%d:%f ", 7, raw_features->no_relative_gaps);
  fprintf(file, "%d:%f ", 8, raw_features->no_gaps_to_num_beam);
  fprintf(file, "%d:%f ", 9, raw_features->no_relative_gaps_to_num_beam);
  fprintf(file, "%d:%f ", 10, raw_features->distance_between_two_smalled_local_minima);
  fprintf(file, "%d:%f ", 11, raw_features->index_distance_between_two_smalled_local_minima);
  fprintf(file, "%d:%f ", 12, raw_features->avg_ratio_consecutive_beams);
  fprintf(file, "%d:%f ", 13, raw_features->avg_to_max_beam_length);
  fprintf(file, "%d:%f ", 14, polygonal_features->area);
  fprintf(file, "%d:%f ", 15, polygonal_features->perimeter);
  fprintf(file, "%d:%f ", 16, polygonal_features->area_to_perimeter);
  fprintf(file, "%d:%f ", 17, polygonal_features->PI_area_to_sqrt_perimeter);
  fprintf(file, "%d:%f ", 18, polygonal_features->PI_area_to_perimeter_pow);
  fprintf(file, "%d:%f ", 19, polygonal_features->sides_of_polygon);
  for(int i = 0; i <= config.no_of_fourier_coefficient * 2; i++){
    //	fprintf(stderr, "Testing: %f\n", polygonal_features->fourier_transform_descriptors[1].results->real);
    fprintf(file, "%d:%f %d:%f ", 20 + i * 2, polygonal_features->fourier_transform_descriptors[0].results->real, 21 + i * 2, polygonal_features->fourier_transform_descriptors[0].results->complex);
  }
  int start = config.no_of_fourier_coefficient * 4 + 22;
  fprintf(file, "%d:%f ", start, polygonal_features->major_ellipse_axis_length_coefficient);
  fprintf(file, "%d:%f ", start + 1, polygonal_features->minor_ellipse_axis_length_coefficient);
  fprintf(file, "%d:%f ", start + 2, polygonal_features->major_to_minor_coefficient);
  fprintf(file, "%d:%f ", start + 3, polygonal_features->central_moment_invariants[0]);
  fprintf(file, "%d:%f ", start + 4, polygonal_features->central_moment_invariants[1]);
  fprintf(file, "%d:%f ", start + 5, polygonal_features->central_moment_invariants[2]);
  fprintf(file, "%d:%f ", start + 6, polygonal_features->central_moment_invariants[3]);
  fprintf(file, "%d:%f ", start + 7, polygonal_features->central_moment_invariants[4]);
  fprintf(file, "%d:%f ", start + 8, polygonal_features->central_moment_invariants[5]);
  fprintf(file, "%d:%f ", start + 9, polygonal_features->central_moment_invariants[6]);
  fprintf(file, "%d:%f ", start + 10, polygonal_features->normalized_feature_of_compactness);
  fprintf(file, "%d:%f ", start + 11, polygonal_features->normalized_feature_of_eccentricity);
  fprintf(file, "%d:%f ", start + 12, polygonal_features->mean_centroid_to_shape_boundary);
  fprintf(file, "%d:%f ", start + 13, polygonal_features->max_centroid_to_shape_boundary);
  fprintf(file, "%d:%f ", start + 14, polygonal_features->mean_centroid_over_max_centroid);
  fprintf(file, "%d:%f ", start + 15, polygonal_features->std_dev_centroid_to_shape_boundry);
  fprintf(file, "%d:%f\n", start + 16, polygonal_features->kurtosis);

  features_t_destroy(features);
  fclose(file);

}















