# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_C
  "/home/harry/Documents/Robotics/semantic/software/externals/kdtree/src/kdtree.c" "/home/harry/Documents/Robotics/semantic/software/externals/kdtree/pod-build/src/CMakeFiles/kdtree.dir/kdtree.c.o"
  )
SET(CMAKE_C_COMPILER_ID "GNU")

# Pairs of files generated by the same build rule.
SET(CMAKE_MULTIPLE_OUTPUT_PAIRS
  "/home/harry/Documents/Robotics/semantic/software/externals/kdtree/pod-build/lib/libkdtree.so" "/home/harry/Documents/Robotics/semantic/software/externals/kdtree/pod-build/lib/libkdtree.so.0.5.5"
  )


# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "include"
  "/home/harry/Documents/Robotics/semantic/software/build/include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
