# Install script for directory: /home/harry/Documents/Robotics/semantic/software/externals/apriltags

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/home/harry/Documents/Robotics/semantic/software/build")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Release")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  IF(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libapriltags.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libapriltags.so")
    FILE(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libapriltags.so"
         RPATH "/home/harry/Documents/Robotics/semantic/software/build/lib")
  ENDIF()
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES "/home/harry/Documents/Robotics/semantic/software/externals/apriltags/pod-build/lib/libapriltags.so")
  IF(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libapriltags.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libapriltags.so")
    FILE(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libapriltags.so"
         OLD_RPATH "/home/harry/Documents/Robotics/semantic/software/externals/apriltags/pod-build/lib:/home/harry/Documents/Robotics/semantic/software/build/lib:"
         NEW_RPATH "/home/harry/Documents/Robotics/semantic/software/build/lib")
    IF(CMAKE_INSTALL_DO_STRIP)
      EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libapriltags.so")
    ENDIF(CMAKE_INSTALL_DO_STRIP)
  ENDIF()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/AprilTags" TYPE FILE FILES
    "/home/harry/Documents/Robotics/semantic/software/externals/apriltags/AprilTags/pch.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/apriltags/AprilTags/MathUtil.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/apriltags/AprilTags/Tag36h11.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/apriltags/AprilTags/FloatImage.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/apriltags/AprilTags/Tag16h5.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/apriltags/AprilTags/Tag25h9.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/apriltags/AprilTags/UnionFindSimple.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/apriltags/AprilTags/TagDetection.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/apriltags/AprilTags/Gaussian.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/apriltags/AprilTags/XYWeight.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/apriltags/AprilTags/GLineSegment2D.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/apriltags/AprilTags/Tag25h7.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/apriltags/AprilTags/Gridder.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/apriltags/AprilTags/Tag36h9.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/apriltags/AprilTags/GrayModel.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/apriltags/AprilTags/TagFamily.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/apriltags/AprilTags/TagDetector.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/apriltags/AprilTags/Homography33.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/apriltags/AprilTags/Quad.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/apriltags/AprilTags/Edge.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/apriltags/AprilTags/Tag16h5_other.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/apriltags/AprilTags/Segment.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/apriltags/AprilTags/GLine2D.h"
    "/home/harry/Documents/Robotics/semantic/software/externals/apriltags/AprilTags/Tag36h11_other.h"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "/home/harry/Documents/Robotics/semantic/software/externals/apriltags/pod-build/lib/pkgconfig/apriltags.pc")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(CMAKE_INSTALL_COMPONENT)
  SET(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
ELSE(CMAKE_INSTALL_COMPONENT)
  SET(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
ENDIF(CMAKE_INSTALL_COMPONENT)

FILE(WRITE "/home/harry/Documents/Robotics/semantic/software/externals/apriltags/pod-build/${CMAKE_INSTALL_MANIFEST}" "")
FOREACH(file ${CMAKE_INSTALL_MANIFEST_FILES})
  FILE(APPEND "/home/harry/Documents/Robotics/semantic/software/externals/apriltags/pod-build/${CMAKE_INSTALL_MANIFEST}" "${file}\n")
ENDFOREACH(file)
