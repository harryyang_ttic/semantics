#import wordnet_util as wu
from optparse import OptionParser
import readonly_shelf
#global variables for flickr and wordnet similarity
from memoized import memoized
#from nltk.corpus import wordnet as wn


wmap = None;

def merge(dict1, dict2):
    #check_dicts(dict1, dict2)
    result = {}
    result.update(dict1)
    result.update(dict2)
    return result


@memoized
def wordnet_similarity(kword, qword):
    load_wmap()
    kword = str(kword)
    if (wmap.has_key(kword) and wmap[kword].has_key(qword)):
        return wmap[kword][qword]
    else:
        #print "recomputing", kword, qword
        return wu.get_word_similarity_path(kword,qword)
    
lmap = None; 


def load_lmap(fname):
    global lmap;
    
    if(lmap == None):
    #fname = "/Users/mwalter/research/code/semantic/data/flickr/flickr_cache.shelf"
        try:
            lmap = readonly_shelf.open(fname)
        except:
            print "can't open", fname
            raise
                                   

def load_wmap():
    global wmap
    if(wmap == None):
        try:
            fname = TKLIB_HOME+"/data/wordnet/wordnet_cache.shelf"
            wmap = readonly_shelf.open(fname)
        except:
            print "can't open", fname
            raise
                                   

def flickr_similarity(kword, qword):
    #load_lmap()    
    kword = str(kword)

    if lmap.has_key(kword) and lmap[kword].has_key(qword):
        return lmap[kword][qword]

    return 0;

def load_maps():
    load_lmap()
    load_wmap()


def compute_probabilities(types, labels, flickr=True, synonyms=None):
    probability_map = {}
    for type in T:
        total_count = 0
        max_count = 0
        probability_map.setdefault(type, {})
        for label in L:
            count = float(flickr_similarity(type,label))
            if (count > max_count):
                max_count = count
            probability_map[type][label] = count
            total_count = total_count + count

        # Consider synonyms for the type
        if synonyms is not None:
            if type in synonyms:
                for synonyms in synonyms[type]:
                    for label in L:
                        count = float(flickr_similarity(type,label))
                        if (count > max_count):
                            max_count = count
                        probability_map[type][label] += count
                        total_count = total_count + count

        # Take care of instances where there is zero cooccurrence when label=type
        for label in L:
            if (label == type) & (probability_map[type][label] == 0):
                probability_map[type][label] = max_count
                total_count = total_count + max_count

        if total_count != 0:
            for label in L:
                probability_map[type][label] = probability_map[type][label]/total_count

    return probability_map

def print_probabilities(probability_map):
    for type in probability_map.keys():
        for label in probability_map[type].keys():
            print '(%s, %s, %f)' % (type, label, probability_map[type][label])

        print '\n'

if __name__ == "__main__":

    parser = OptionParser()
    (options, args) = parser.parse_args()
    if (len(args) == 0):
        fname = "/Users/mwalter/research/code/semantic/data/flickr/flickr_cache.shelf"
    else:
        fname = args[0]

    S = {'office': ['workspace','room','kitchen']}
    L = ["office", "conferenceroom", "lab", "classroom", "gym", "kitchen", "supplycloset", "lobby", "courtyard", "elevator", "elevatorlobby"]
    T = ["office", "conferenceroom", "lab", "classroom", "lounge", "hallway"]

    load_lmap(fname)
    probability_map = compute_probabilities(T,L,True,S) 
    print_probabilities(probability_map)
