from optparse import OptionParser
import shelve

def readwrite_shelf_open(filename, writeback=True):
    import anydbm
 	#return shelve.Shelf.__init__(self, anydbm.open(filename, flag='rw'), protocol=2, writeback=writeback)
    return DbfilenameReadonlyShelf(filename, writeback=writeback)

class DbfilenameReadonlyShelf(shelve.Shelf):
    """
    Shelf implementation using the "anydbm" generic dbm interface,
    read only.  Gets rid of annoying error message on shutdown when it
    tries to write back.
    """
    
    def __init__(self, filename, writeback):
        import anydbm
        shelve.Shelf.__init__(self, anydbm.open(filename, flag='w'), protocol=2, writeback=writeback)

    def __del__(self):
        self.dict.close()

def make_symmetric(fname):
    try:
        prior = readwrite_shelf_open(fname)
    except:
        print "can't open", fname
        raise
    """
    There are entries where prior[key1][key2] exists, but not key2 in prior.
    This fixes that problem by putting in the corresponding entries. 
    """
    print "making symmetric"

    new_map = {}
    for key1 in prior.keys():
        for key2 in prior[key1].keys():
            if not key2 in prior:
                new_map.setdefault(key2, {})
                new_map[key2][key1] = prior[key1][key2]

    for key in new_map:
        assert not key in prior
        prior[key] = new_map[key]
    print "fixed", len(new_map), "entries"


    
if __name__ == "__main__":
    parser = OptionParser()
    #parser.add_option("-f", "--file", dest="filename",
    #help="write output to FILE", metavar="FILE")
    (options, args) = parser.parse_args()#
    if (len(args) != 1):
        print 'usage: flickr_make_symmetric.py <path_to_flickr_shelf_file>'
    else:
        make_symmetric(args[0])
    
