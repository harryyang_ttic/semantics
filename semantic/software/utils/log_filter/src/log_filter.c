//Subscribe to log_annotation_t and planar_lidar_t messages. Once receive an annotation, publish laser_annotation_t message
//Requires: er-simulate-360-laser
//Output: publish laser_annotation_t message


#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <bot_core/bot_core.h>
#include <lcm/lcm.h>

static void usage(const char* progname)
{
    fprintf (stderr, "Usage: %s [options]\n"
             "\n"
             "Options:\n"
	     "  -l        Log filename (to load)\n"
             "  -n        New log filename (to write)\n"
	     "  -h        help\n",
             g_path_get_basename(progname));
    exit(1);
}

int 
main(int argc, char **argv)
{

    const char *optstring = "hl:n:";

    int c;
    int nodding = 0;
    char * log_path = NULL;
    char *write_log_name = NULL;
    char *rename_channel = strdup("DRAGONFLY_IMAGE_RECTIFIED");
    char *replace_channel = strdup("DRAGONFLY_IMAGE");

    while ((c = getopt (argc, argv, optstring)) >= 0) {
        switch (c) {
        case 'h': //help
            usage(argv[0]);
            break;
	case 'l': //Log file
	    log_path = strdup(optarg);
	    break;
        case 'n': //Log file
	    write_log_name = strdup(optarg);
	    break;
        default:
            usage(argv[0]);
            break;
        }
    }

    if(!log_path){
        usage(argv[0]);
        return -1;
    }

    if(!write_log_name){
        fprintf(stderr, "No write log specified - writing to default\n");
        write_log_name = (char *) calloc(strlen(log_path) + 5, sizeof(char));
        sprintf(write_log_name, "new_%s", log_path);
        fprintf(stderr, "Write Name : %s\n", write_log_name);
    }
    
    lcm_eventlog_t *write_log = lcm_eventlog_create(write_log_name, "w");
    lcm_eventlog_t *read_log = lcm_eventlog_create(log_path, "r"); // problem here?
    fprintf(stderr, "Log open success.\n");
    
    //Adding to annotation circular buffer
    lcm_eventlog_event_t *event = NULL;//(lcm_eventlog_event_t *)calloc(1, sizeof(lcm_eventlog_event_t));
    int skip_count = 0;
    int rename_count = 0;

    for(event = lcm_eventlog_read_next_event(read_log); event != NULL; event = lcm_eventlog_read_next_event(read_log)){
        
        if(!strcmp(replace_channel, event->channel)){
            //skip writing this to log 
            skip_count++;
            lcm_eventlog_free_event(event);
        }
        else if(!strcmp(rename_channel, event->channel)){
            //free(event->channel);
            //event->channel = strdup(replace_channel);
            int channellen = strlen(replace_channel);
            //int64_t mem_sz = sizeof(lcm_eventlog_event_t) + channellen + 1 + event->datalen;//sizeof(event->data);//rbuf->data_size;

            lcm_eventlog_event_t *le = (lcm_eventlog_event_t*) calloc(1, sizeof(lcm_eventlog_event_t));//malloc(mem_sz);
            //memset(le, 0, mem_sz);

            le->timestamp = event->timestamp;
            le->channellen = channellen;
            le->datalen = event->datalen;

            le->channel = strdup(replace_channel);//((char*)le) + sizeof(lcm_eventlog_event_t);
            //strcpy(le->channel, replace_channel);
            le->data = (char *) malloc(event->datalen);//le->channel + channellen+1;

            //fprintf(stderr, "Length : %d - %d\n", event->datalen, (int) sizeof(event->data));
            memcpy(le->data, event->data, event->datalen);

            rename_count++;
            if(0 != lcm_eventlog_write_event(write_log, le)){
                fprintf(stderr, "Error\n");
            }
            lcm_eventlog_free_event(event);
            lcm_eventlog_free_event(le);
        }
        else{
            if(0 != lcm_eventlog_write_event(write_log, event)){
                fprintf(stderr, "Error\n");
            }
            lcm_eventlog_free_event(event);
        }
    }
    
    lcm_eventlog_destroy(read_log);
    lcm_eventlog_destroy(write_log);
    fprintf(stderr, "\nSkipped : %d - Renamed : %d\n", skip_count, rename_count);
}
