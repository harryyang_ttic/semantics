/**
 * @file april_tags.cpp
 * @brief Example application for April tags library
 * @author: Michael Kaess
 *
 * Opens the first available camera (typically a built in camera in a
 * laptop) and continuously detects April tags in the incoming
 * images. Detections are both visualized in the live image and shown
 * in the text console. Optionally allows selecting of a specific
 * camera in case multiple ones are present and specifying image
 * resolution as long as supported by the camera. Also includes the
 * option to send tag detections via a serial port, for example when
 * running on a Raspberry Pi that is connected to an Arduino.
 */

using namespace std;

#include <iostream>
#include <cstring>
#include <vector>
#include <sys/time.h>

const string usage = "\n"
    "Usage:\n"
    "  apriltags_demo [OPTION...] [deviceID]\n"
    "\n"
    "Options:\n"
    "  -h  -?          Show help options\n"
    "  -a              Arduino (send tag ids over serial port)\n"
    "  -d              disable graphics\n"
    "  -C <bbxhh>      Tag family (default 36h11)\n"
    "  -F <fx>         Focal length in pixels\n"
    "  -W <width>      Image width (default 640, availability depends on camera)\n"
    "  -H <height>     Image height (default 480, availability depends on camera)\n"
    "  -S <size>       Tag size (square black frame) in meters\n"
    "  -E <exposure>   Manually set camera exposure (default auto; range 0-10000)\n"
    "  -G <gain>       Manually set camera gain (default auto; range 0-255)\n"
    "  -B <brightness> Manually set the camera brightness (default 128; range 0-255)\n"
    "\n";

const string intro = "\n"
    "April tags test code\n"
    "(C) 2012-2013 Massachusetts Institute of Technology\n"
    "Michael Kaess\n"
    "\n";


#ifndef __APPLE__
#define EXPOSURE_CONTROL // only works in Linux
#endif

#include <GL/gl.h>

#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <opencv2/nonfree/features2d.hpp>

#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_util.h>
#include <bot_lcmgl_client/lcmgl.h>

//#include <perception_opencv_utils/opencv_utils.hpp>
//#include <image_io_utils/image_io_utils.hpp>
#include <jpeg-utils/jpeg-utils.h>

#include <lcmtypes/perception_object_detection_list_t.h>
#include <lcmtypes/perception_status_t.h>
//#include <ConciseArgs>
//#include <geom_utils/geometry.h>
//#include <occ_map/PixelMap.hpp>

#ifdef EXPOSURE_CONTROL
#include <libv4l2.h>
#include <linux/videodev2.h>
#include <fcntl.h>
#include <errno.h>
#endif

#define OBS_DIST_THRESHOLD 5.0
#define PRUNE_OBJECT_THRESHOLD 10.0 //prune objects outside 10m distance

// OpenCV library for easy access to USB camera and drawing of images
// on screen
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/highgui/highgui_c.h>
#include <opencv2/imgproc/types_c.h>

// April tags detector and various families that can be selected by command line option
#include "AprilTags/TagDetector.h"
#include "AprilTags/Tag16h5.h"
#include "AprilTags/Tag25h7.h"
#include "AprilTags/Tag25h9.h"
#include "AprilTags/Tag36h9.h"
#include "AprilTags/Tag36h11.h"


// Needed for getopt / command line options processing
#include <unistd.h>
extern int optind;
extern char *optarg;

const char* window_name = "apriltags_demo";

class DetObject;
typedef pair<pair<DetObject *, DetObject *>, double> detectionPair;

// utility function to provide current system time (used below in
// determining frame rate at which images are being processed)
double tic() {
    struct timeval t;
    gettimeofday(&t, NULL);
    return ((double)t.tv_sec + ((double)t.tv_usec)/1000000.);
}


#include <cmath>

#ifndef PI
const double PI = 3.14159265358979323846;
#endif
const double TWOPI = 2.0*PI;

/**
 * Normalize angle to be within the interval [-pi,pi].
 */
inline double standardRad(double t) {
    if (t >= 0.) {
        t = fmod(t+PI, TWOPI) - PI;
    } else {
        t = fmod(t-PI, -TWOPI) + PI;
    }
    return t;
}

void wRo_to_euler(const Eigen::Matrix3d& wRo, double& yaw, double& pitch, double& roll) {
    yaw = standardRad(atan2(wRo(1,0), wRo(0,0)));
    double c = cos(yaw);
    double s = sin(yaw);
    pitch = standardRad(atan2(-wRo(2,0), wRo(0,0)*c + wRo(1,0)*s));
    roll  = standardRad(atan2(wRo(0,2)*s - wRo(1,2)*c, -wRo(0,1)*s + wRo(1,1)*c));
}

class DetObject 
{
public:
    BotTrans object_to_body;
    int64_t utime_c; //utime at which the object was first observed
    int64_t utime_l; 
    int no_obs;
    int id;
    int type;
    BotTrans object_to_local; //to check if its the same object - and to remove when its outside the robot's boundary   
    
    DetObject(){
        utime_c = 0;
        utime_l = 0;
        no_obs = 1;
        id =0;
    }

    void print(){
        fprintf(stderr, "Object [%d] - %d (%d) => (%.2f,%.2f,%.2f)\n", id, type, no_obs, object_to_local.trans_vec[0], 
                object_to_local.trans_vec[1], object_to_local.trans_vec[2]);
    }

    double getDistanceToObject(DetObject *new_object){
        double dx = object_to_local.trans_vec[0] - new_object->object_to_local.trans_vec[0];
        double dy = object_to_local.trans_vec[1] - new_object->object_to_local.trans_vec[1];
        return hypot(dx,dy);
    }
    
    void updateDetection(DetObject *new_observation, int64_t utime){
        object_to_local = new_observation->object_to_local;
        object_to_body = new_observation->object_to_body;
        no_obs++;
        utime_l = utime;
    }
    
    DetObject *copy(int _id){
        DetObject *copy = new DetObject();
        copy->id = _id;
        copy->utime_c = utime_c;
        copy->utime_l = utime_l;
        copy->no_obs = no_obs;
        copy->object_to_local = object_to_local;
        copy->object_to_body = object_to_body;
        return copy;
    }
    double getDistanceToBody(BotTrans bodyToLocal){
        return hypot(object_to_local.trans_vec[0] - bodyToLocal.trans_vec[0], object_to_local.trans_vec[1] - bodyToLocal.trans_vec[1]);
    }
};

bool comparePairDistance (detectionPair p1, detectionPair p2) { 
    if(p1.second < p2.second)
        return true;
    return false;
}

class Detector 
{

public:

    lcm_t *lcm;
    BotFrames *frames; 
    BotParam *param;
    bot_lcmgl_t * lcmgl;
    bot_lcmgl_t * lcmgl_new_detections;
    BotCamTrans *camtrans;
    char * coord_frame;
    GMutex *mutex;
    pthread_t  process_thread;
    int verbose;
    char *channel;

    int detection_count;

    AprilTags::TagDetector* m_tagDetector;
    AprilTags::TagCodes m_tagCodes;

    map<int, int> detection_object_map;

    bot_core_image_t *last_img;
    bot_core_planar_lidar_t *last_laser;

    vector<DetObject *> detections;

    cv::Mat img, img_gray, img_processed;
    int64_t img_utime;
    bool m_draw; // draw image and April tag detections?
    bool m_arduino; // send tag detections to serial port?

    int m_width; // image size in pixels
    int m_height;
    double m_tagSize; // April tag side length in meters of square black frame
    double m_fx; // camera focal length in pixels
    double m_fy;
    double m_px; // camera principal point
    double m_py;

    bool img_setup;

    // default constructor
    Detector() :
        // default settings, most can be modified through command line options (see below)
        m_tagDetector(NULL),
        last_img(NULL),
	last_laser(NULL),
        m_tagCodes(AprilTags::tagCodes36h11),
        channel("DRAGONFLY_IMAGE"),
        m_draw(true),
        m_arduino(false),

        m_width(640),
        m_height(480),
        m_tagSize(0.1032),
        m_fx(612.486),
        m_fy(614.032),
        m_px(637.539),
        m_py(426.127)

    {
        img_setup = false;
        detection_count = 0; 
        lcm =  bot_lcm_get_global(NULL);

        lcmgl = bot_lcmgl_init(lcm, "apriltags-detections");
        lcmgl_new_detections = bot_lcmgl_init(lcm, "apriltags-new-detections");
        param = bot_param_new_from_server(lcm, 1);
        frames = bot_frames_get_global (lcm, param);              
        mutex = g_mutex_new();
        m_tagDetector = new AprilTags::TagDetector(m_tagCodes);
        init_map();
    }

    /*bool comparePairDistance (detectionPair p1, detectionPair p2) { 
        if(p1.second < p2.second)
            return true;
        return false;
        }*/

    void drawCurrentDetections(){
        drawDetections(lcmgl, detections,0);
    }

    void drawDetections(bot_lcmgl_t *d_lcmgl, vector<DetObject *> obj_detections, int red){
        double point_size=10.0;
        //fprintf(stderr, "No of new detections : %d\n", (int) obj_detections.size());

        bot_lcmgl_point_size(d_lcmgl, point_size);
        if(red)
            bot_lcmgl_color3f(d_lcmgl, 1.0, 0, 0);
        else
            bot_lcmgl_color3f(d_lcmgl, 0, 0, 1.0);

        bot_lcmgl_begin(lcmgl, GL_POINTS);
        for(int i=0; i < obj_detections.size();i++){
            bot_lcmgl_vertex3f(d_lcmgl, obj_detections[i]->object_to_local.trans_vec[0], obj_detections[i]->object_to_local.trans_vec[1], obj_detections[i]->object_to_local.trans_vec[2]); 
        }
        bot_lcmgl_end(d_lcmgl);
        bot_lcmgl_switch_buffer(d_lcmgl);
    }

    void publishCurrentObjectDetections(int64_t utime){
        publishObjectDetections(utime, detections);
    }

    void publishObjectDetections(int64_t utime, vector<DetObject *> obj_detections){
        if(obj_detections.size()==0)
            return;
        
        fprintf(stderr, "Publishing Current Observations : %d\n", (int) obj_detections.size());
        perception_object_detection_list_t *msg = (perception_object_detection_list_t *) calloc(1,sizeof(perception_object_detection_list_t)); 
        msg->utime = utime;
        msg->count = obj_detections.size();
        msg->objects = (perception_object_detection_t *) calloc(msg->count, sizeof(perception_object_detection_t));
        for(int i=0; i < obj_detections.size();i++){             
            perception_object_detection_t *obj = &msg->objects[i];
            obj->id = obj_detections[i]->id;
            obj->type = obj_detections[i]->type;
            obj->utime_created = obj_detections[i]->utime_c;
            obj->utime_last_seen = obj_detections[i]->utime_l;
            obj->no_observations = obj_detections[i]->no_obs;

            BotTrans local_to_body_tf;
            
            bot_frames_get_trans_with_utime(frames, "local", 
                                            "body",
                                            utime, 
                                            &local_to_body_tf); 

            //update the object to body pose 
            if(obj_detections[i]->utime_l != utime){
                BotTrans object_to_body; 
                bot_trans_apply_trans_to(&local_to_body_tf, &obj_detections[i]->object_to_local, &object_to_body);
                obj_detections[i]->object_to_body = object_to_body; 
            }

            obj->pos[0] = obj_detections[i]->object_to_local.trans_vec[0];
            obj->pos[1] = obj_detections[i]->object_to_local.trans_vec[1];
            obj->pos[2] = obj_detections[i]->object_to_local.trans_vec[2];
            
            obj->quat[0] = obj_detections[i]->object_to_local.rot_quat[0];
            obj->quat[1] = obj_detections[i]->object_to_local.rot_quat[1];
            obj->quat[2] = obj_detections[i]->object_to_local.rot_quat[2];
            obj->quat[3] = obj_detections[i]->object_to_local.rot_quat[3];
        }
        perception_object_detection_list_t_publish(lcm, "OBJECT_DETECTION_LIST", msg);
        perception_object_detection_list_t_destroy(msg);
    }

    void pruneFarObjects(int64_t utime){
        if(detections.size()==0)
            return;
        BotTrans body_to_local_tf;
        
        bot_frames_get_trans_with_utime(frames, "body", 
                                        "local",
                                        utime, 
                                        &body_to_local_tf); 

        vector<DetObject *> valid_objects;
        vector<DetObject *> delete_objects;
        for(int i=0; i < detections.size();i++){             
            double dist = detections[i]->getDistanceToBody(body_to_local_tf);
            if(dist < PRUNE_OBJECT_THRESHOLD){
                valid_objects.push_back(detections[i]);
            }
            else{
                delete_objects.push_back(detections[i]);
                fprintf(stderr, "Pruning Object\n");
                detections[i]->print();
            }
        }
        
        detections = valid_objects;

        for(int i=0; i < delete_objects.size(); i++){
            delete delete_objects[i];
        }
    }

    int updateDetections(vector<DetObject *> &new_detections, int64_t utime){     
        double match_threshold = 0.5;
        vector<detectionPair> close_pairs;
        
        //fprintf(stderr, "Updating Detections\n");
        for(int i=0; i < detections.size(); i++){
            DetObject *dt_o = detections[i];
            //fprintf(stderr, "Found Object : %d\n", dt_o.id);
            for(int j=0; j < new_detections.size(); j++){
                DetObject *dt_n = new_detections[j];
                //fprintf(stderr, "Type : %d - %d\n", dt_o->type, dt_n->type);
                if(dt_n->type == dt_o->type){
                    //fprintf(stderr, "\tNew Object : %d - Dist : %f\n", dt_n.id, dt_o.getDistanceToObject(dt_n));
                    close_pairs.push_back(make_pair(make_pair(dt_n,dt_o), dt_o->getDistanceToObject(dt_n)));                
                }
            }
        }
        
        std::sort(close_pairs.begin(), close_pairs.end(), comparePairDistance);

        //remove the first one that is close 
        //if its close enough - flag as found 
        //otherwise - add new 
        //pop all observations to that object from the vector 
        vector<detectionPair> matched_pairs;
        fprintf(stderr, "No of close pairs : %d\n", (int) close_pairs.size());
        
        while(close_pairs.size() > 0){
            vector<detectionPair> new_close_pairs;
            //fprintf(stderr, "Checking close pairs : %d-%d => %f\n", close_pairs[0].first.first->id, 
            //close_pairs[0].first.second->id, close_pairs[0].second);
            //if(close_pairs[0].second < match_threshold){
            //fprintf(stderr, "Found Node Pair : %d-%d => %f\n", close_pairs[0].first.first->id, 
            //close_pairs[0].first.second->id, close_pairs[0].second);
            matched_pairs.push_back(close_pairs[0]);
            //}
            int added_new_id = close_pairs[0].first.first->id;
            int added_current_id = close_pairs[0].first.second->id;
            //remove all pairs for that detection 
            for(int i=0; i < close_pairs.size(); i++){
                //remove the pairs for the new node and also any pairs that have the closes matched node
                if(close_pairs[i].first.first->id != added_new_id && close_pairs[i].first.second->id != added_current_id){
                    new_close_pairs.push_back(close_pairs[i]);                    
                }
            }
            close_pairs = new_close_pairs;
            //fprintf(stderr, "Close pair size : %d\n", (int) close_pairs.size());
        }

        /*fprintf(stderr, "\n===================================================\n");
        for(int i=0; i < matched_pairs.size(); i++){
            fprintf(stderr, "Matched Node Pair : %d-%d => %f\n", matched_pairs[i].first.first->id, 
                    close_pairs[i].first.second->id, close_pairs[i].second);
        }
        fprintf(stderr, "\n===================================================\n");*/
        
        //drawDetections(lcmgl_new_detections, new_detections, 1);
        
        vector<DetObject *> current_observations;

        for(int j=0; j < new_detections.size(); j++){
            int found = 0;
            for(int i=0; i < matched_pairs.size(); i++){
                if(matched_pairs[i].first.first->id == new_detections[j]->id){
                    if(matched_pairs[i].second < match_threshold){
                        matched_pairs[i].first.second->updateDetection(new_detections[j], utime);
                        fprintf(stderr, "Object found :%f => Updating location\n", matched_pairs[i].second);
                        found = 1;
                        current_observations.push_back(matched_pairs[i].first.second);
                        delete new_detections[j];
                    }
                    else{
                        fprintf(stderr, "New observation\n");
                        matched_pairs[i].first.first->print();
                        fprintf(stderr, "Closest observation\n");
                        matched_pairs[i].first.second->print();
                        fprintf(stderr, "Distance : %.3f\n", matched_pairs[i].first.first->getDistanceToObject(matched_pairs[i].first.second));
                        fprintf(stderr, "Closest match too far :%f\n", matched_pairs[i].second);
                    }
                    break;
                }                
            }
            if(found == 0){
                fprintf(stderr, "New Object detected - Adding to list\n");
                new_detections[j]->id = detection_count++;
                detections.push_back(new_detections[j]);
                current_observations.push_back(new_detections[j]);
            }
        }

        fprintf(stderr, "\n===================================================\n");
        for(int i=0; i < detections.size();i++){
            detections[i]->print();
        }
        fprintf(stderr, "\n===================================================\n");
        
        publishObjectDetections(last_img->utime, current_observations);
        /*for(int j=0; j < new_detections.size(); j++){
            delete new_detections[j];
            }*/
    }

    const char* get_name(int type){
        if(type == PERCEPTION_OBJECT_DETECTION_T_TYPE_MONITOR){
            return "monitor";
        }
        if(type == PERCEPTION_OBJECT_DETECTION_T_TYPE_KEYBOARD){
            return "keyboard";
        }
        if(type == PERCEPTION_OBJECT_DETECTION_T_TYPE_LAPTOP){
            return "laptop";
        }
        if(type == PERCEPTION_OBJECT_DETECTION_T_TYPE_MICROWAVE){
            return "microwave";
        }
        if(type == PERCEPTION_OBJECT_DETECTION_T_TYPE_REFRIGERATOR){
            return "refrigerator";
        }
        if(type == PERCEPTION_OBJECT_DETECTION_T_TYPE_COUCH){
            return "couch";
        }
        if(type == PERCEPTION_OBJECT_DETECTION_T_TYPE_TABLE){
            return "table";
        }
        if(type == PERCEPTION_OBJECT_DETECTION_T_TYPE_CHAIR){
            return "chair";
        }
        if(type == PERCEPTION_OBJECT_DETECTION_T_TYPE_CABINET){
            return "cabinet";            
        }
        if(type == PERCEPTION_OBJECT_DETECTION_T_TYPE_ELEVATOR_DOOR){
            return "elevator_door";
        }
        if(type == PERCEPTION_OBJECT_DETECTION_T_TYPE_SINK){
            return "sink";
        }
        if(type == PERCEPTION_OBJECT_DETECTION_T_TYPE_PRINTER){
            return "printer";
        }
        if(type == PERCEPTION_OBJECT_DETECTION_T_TYPE_TRASHCAN){
            return "trashcan";
        }
        if(type == PERCEPTION_OBJECT_DETECTION_T_TYPE_STAIRS){
            return "stairs";
        }
        return "unknown";
    }
    
    void init_map(){
        detection_object_map.insert(make_pair(0, PERCEPTION_OBJECT_DETECTION_T_TYPE_MONITOR));
        detection_object_map.insert(make_pair(1, PERCEPTION_OBJECT_DETECTION_T_TYPE_PRINTER));
        detection_object_map.insert(make_pair(2, PERCEPTION_OBJECT_DETECTION_T_TYPE_REFRIGERATOR));
        detection_object_map.insert(make_pair(3, PERCEPTION_OBJECT_DETECTION_T_TYPE_MICROWAVE));
        detection_object_map.insert(make_pair(4, PERCEPTION_OBJECT_DETECTION_T_TYPE_KEYBOARD));        
        detection_object_map.insert(make_pair(5, PERCEPTION_OBJECT_DETECTION_T_TYPE_LAPTOP));
        detection_object_map.insert(make_pair(6, PERCEPTION_OBJECT_DETECTION_T_TYPE_COUCH));
        detection_object_map.insert(make_pair(7, PERCEPTION_OBJECT_DETECTION_T_TYPE_TABLE));
        detection_object_map.insert(make_pair(8, PERCEPTION_OBJECT_DETECTION_T_TYPE_CHAIR));
        detection_object_map.insert(make_pair(9, PERCEPTION_OBJECT_DETECTION_T_TYPE_CABINET));
        detection_object_map.insert(make_pair(10, PERCEPTION_OBJECT_DETECTION_T_TYPE_ELEVATOR_DOOR));
        detection_object_map.insert(make_pair(11, PERCEPTION_OBJECT_DETECTION_T_TYPE_SINK));
        detection_object_map.insert(make_pair(12, PERCEPTION_OBJECT_DETECTION_T_TYPE_TRASHCAN));
        detection_object_map.insert(make_pair(13, PERCEPTION_OBJECT_DETECTION_T_TYPE_STAIRS));
    }

    // changing the tag family
    void setTagCodes(string s) {
        if (s=="16h5") {
            m_tagCodes = AprilTags::tagCodes16h5;
        } else if (s=="25h7") {
            m_tagCodes = AprilTags::tagCodes25h7;
        } else if (s=="25h9") {
            m_tagCodes = AprilTags::tagCodes25h9;
        } else if (s=="36h9") {
            m_tagCodes = AprilTags::tagCodes36h9;
        } else if (s=="36h11") {
            m_tagCodes = AprilTags::tagCodes36h11;
        } else {
            cout << "Invalid tag family specified" << endl;
            exit(1);
        }
    }

    // parse command line options to change default behavior
    void parseOptions(int argc, char* argv[]) {
        int c;
        while ((c = getopt(argc, argv, ":h?adc:C:F:H:S:W:E:G:B:")) != -1) {
            // Each option character has to be in the string in getopt();
            // the first colon changes the error character from '?' to ':';
            // a colon after an option means that there is an extra
            // parameter to this option; 'W' is a reserved character
            switch (c) {
            case 'h':
            case '?':
                cout << intro;
                cout << usage;
                exit(0);
                break;
            case 'a':
                m_arduino = true;
                break;
            case 'd':
                m_draw = false;
                break;
            case 'c':
                channel = strdup(optarg);
                break;
            case 'C':
                setTagCodes(optarg);
                break;
            case 'F':
                m_fx = atof(optarg);
                m_fy = m_fx;
                break;
            case 'H':
                m_height = atoi(optarg);
                m_py = m_height/2;
                break;
            case 'S':
                m_tagSize = atof(optarg);
                break;
            case 'W':
                m_width = atoi(optarg);
                m_px = m_width/2;
                break;
            case ':': // unknown option, from getopt
                cout << intro;
                cout << usage;
                exit(1);
                break;
            }
        }
    }
   
    void clearDetections(){
        fprintf(stderr, "Clearing detections\n");
        vector<DetObject *> cleared_detections; 
        for(int i=0; i < detections.size(); i++){
            delete detections[i];
        }
        detections = cleared_detections;
    }

    void detectObjects(int64_t utime, vector<AprilTags::TagDetection> &tag_detections){
        if(tag_detections.size() == 0)
            return; 

        Eigen::Matrix3d F;
        F <<
            1, 0,  0,
            0,  -1,  0,
            0,  0,  1;

        //transform base map to particle's map frame - according to nd2 
        BotTrans sensor_to_body_tf;

        bot_frames_get_trans_with_utime(frames, "dragonfly", 
                                        "body",
                                        utime, 
                                        &sensor_to_body_tf);  

        //transform base map to particle's map frame - according to nd2 
        BotTrans body_to_local_tf;

        bot_frames_get_trans_with_utime(frames, "body", 
                                        "local",
                                        utime, 
                                        &body_to_local_tf);  

        vector<DetObject *> new_detections;
        map<int,int>::iterator it;
        for (int i=0; i<tag_detections.size(); i++) {
            printDetection(tag_detections[i]);

            it = detection_object_map.find(tag_detections[i].id);
            if(it == detection_object_map.end()){
                fprintf(stderr, "Unknown object\n");
                continue;
            }

            // NOTE: for this to be accurate, it is necessary to use the
            // actual camera parameters here as well as the actual tag size
            // (m_fx, m_fy, m_px, m_py, m_tagSize)

            Eigen::Vector3d translation;
            Eigen::Matrix3d rotation;
            tag_detections[i].getRelativeTranslationRotation(m_tagSize, m_fx, m_fy, m_px, m_py,
                                                     translation, rotation);

            
            Eigen::Matrix3d fixed_rot = F*rotation;
            double yaw, pitch, roll;
            wRo_to_euler(fixed_rot, yaw, pitch, roll);

            double pos[3] = {translation(0), translation(1), translation(2)};

            BotTrans object_to_sensor_tf;

            object_to_sensor_tf.trans_vec[0] = -translation(1);
            object_to_sensor_tf.trans_vec[1] = -translation(2);
            object_to_sensor_tf.trans_vec[2] = translation(0);

            double rpy[3] = {0};

            bot_roll_pitch_yaw_to_quat(rpy, object_to_sensor_tf.rot_quat);
            
            BotTrans object_to_body_tf;
            bot_trans_apply_trans_to(&sensor_to_body_tf, &object_to_sensor_tf, &object_to_body_tf);

            BotTrans object_to_local_tf;
            bot_trans_apply_trans_to(&body_to_local_tf, &object_to_body_tf, &object_to_local_tf);

            double dist = hypot(object_to_body_tf.trans_vec[0], object_to_body_tf.trans_vec[1]);

            if(dist > OBS_DIST_THRESHOLD)
                continue;
            
            DetObject *n_det = new DetObject();
            n_det->type = it->second;
            n_det->object_to_body = object_to_body_tf;
            n_det->object_to_local = object_to_local_tf;
            n_det->utime_c = utime;
            n_det->utime_l = utime;
            n_det->id = new_detections.size();
            new_detections.push_back(n_det);
        }

        updateDetections(new_detections, utime);
        //we should keep the detections locally and decide if its a new detection or not 
        //perception_object_detection_list_t_publish(lcm, "OBJECT_DETECTION_LIST", msg);
        //perception_object_detection_list_t_destroy(msg);
    }

    void printDetection(AprilTags::TagDetection& detection) const {
        //have a library of the id's published by someone else         
        cout << "  Id: " << detection.id
          << " (Hamming: " << detection.hammingDistance << ")";

        // recovering the relative pose of a tag:

        // NOTE: for this to be accurate, it is necessary to use the
        // actual camera parameters here as well as the actual tag size
        // (m_fx, m_fy, m_px, m_py, m_tagSize)

        Eigen::Vector3d translation;
        Eigen::Matrix3d rotation;
        detection.getRelativeTranslationRotation(m_tagSize, m_fx, m_fy, m_px, m_py,
                                                 translation, rotation);

        Eigen::Matrix3d F;
        F <<
            1, 0,  0,
            0,  -1,  0,
            0,  0,  1;
        Eigen::Matrix3d fixed_rot = F*rotation;
        double yaw, pitch, roll;
        wRo_to_euler(fixed_rot, yaw, pitch, roll);

        /*cout << "  distance=" << translation.norm()
             << "m, x=" << translation(0)
             << ", y=" << translation(1)
             << ", z=" << translation(2)
             << ", yaw=" << yaw
             << ", pitch=" << pitch
             << ", roll=" << roll
             << endl;
        */
        // Also note that for SLAM/multi-view application it is better to
        // use reprojection error of corner points, because the noise in
        // this relative pose is very non-Gaussian; see iSAM source code
        // for suitable factors.
    }

    int 
    decode_image(const bot_core_image_t * msg, cv::Mat& img, cv::Mat& img_gray)
    {
        int ch = msg->row_stride / (msg->width); 
    
        if(ch==0)
            ch = 3;

        if(ch == 1){
            fprintf(stderr, "No color channels\n");
            return 0;
        }
        if(msg->pixelformat == BOT_CORE_IMAGE_T_PIXEL_FORMAT_GRAY){
            fprintf(stderr, "Image is gray\n");
            //should handle this 
            return 0;
        }
    
        if (img.empty() || img.rows != msg->height || img.cols != msg->width)
            img.create(msg->height, msg->width, CV_8UC3);

        // extract image data
        switch (msg->pixelformat) {
        case BOT_CORE_IMAGE_T_PIXEL_FORMAT_RGB:
            fprintf(stderr, "RGB\n");
            memcpy(img.data, msg->data, sizeof(uint8_t) * msg->width * msg->height * 3);
            cv::cvtColor(img, img, CV_RGB2BGR);
            cv::cvtColor(img, img_gray, CV_BGR2GRAY);
            break;
        case BOT_CORE_IMAGE_T_PIXEL_FORMAT_MJPEG:
            if (ch == 3) { 
                jpeg_decompress_8u_rgb(msg->data,
                                       msg->size,
                                       img.data,
                                       msg->width,
                                       msg->height,
                                       msg->width * ch);
                cv::cvtColor(img, img, CV_RGB2BGR);
                cv::cvtColor(img, img_gray, CV_BGR2GRAY);
            } 
            break;
        default:
            fprintf(stderr, "Unrecognized image format\n");
            return 0;
            break;
        }
        return 1;
    }

    int detect_on_last_image(){
        g_mutex_lock(mutex);
        if(last_img == NULL){
            //fprintf(stderr, "Last image NULL\n");
            g_mutex_unlock(mutex);
            return 0;
        }
        
        if(img_utime == last_img->utime){
            g_mutex_unlock(mutex);
            return 0;
        }

        bot_core_image_t *copy = bot_core_image_t_copy(last_img);        
        g_mutex_unlock(mutex);

        if(!img_setup){
            img_setup = true;
            fprintf(stderr, "Setting up the projection stuff - only done once\n");
            fprintf(stderr, "Width : %d Height : %d -> Row stride : %d\n", copy->width, copy->height, copy->row_stride);
            m_width = copy->width;
            m_height = copy->height;            
        }  

        if (img.empty() || img.rows != copy->height || img.cols != copy->width) { 
            if (copy->pixelformat == BOT_CORE_IMAGE_T_PIXEL_FORMAT_GRAY) { 
                std::cerr << "ERROR: Incoming image is grayscale!! Cannot perform road detection!!!" << std::endl;
                assert(0);
            } else { 
                std::cerr << "One time creation of image" << std::endl;
                img.create(copy->height, copy->width, CV_8UC3);
            }
        }
    
        int d_status = decode_image(copy, img, img_gray);    
        if(d_status == 0){
            fprintf(stderr, "Error decoding image\n");
            return 0;
        }
        //fprintf(stderr, "Decoded image\n");
        
        vector<AprilTags::TagDetection> new_detections = m_tagDetector->extractTags(img_gray);

        pruneFarObjects(last_img->utime);
        detectObjects(last_img->utime, new_detections);
        
        drawCurrentDetections();

        //also need to publish the poses of the detections 

        // show the current image including any detections
        if (m_draw) {
            for (int i=0; i<new_detections.size(); i++) {
                // also highlight in the image
                new_detections[i].draw(img);
            }
            imshow(window_name, img);
            //img_processed = img.clone();
        }

        img_utime = copy->utime; 
        bot_core_image_t_destroy(copy);
        return 1;
    }

    // The processing loop where images are retrieved, tags detected,
    // and information about detections generated
    void loop() {
        // Main lcm handle
        while(1) { 
            unsigned char c = cv::waitKey(1) & 0xff;
            lcm_handle(lcm);

            if (c == 'q') {
                break;  
            } else if ( c == 'c' ) {      
                cv::imshow("Captured Image", img);
            }            
        }

        return;
    }    

}; // Detector


int 
decode_image(const bot_core_image_t * msg, cv::Mat& img, cv::Mat& img_gray)
{
    int ch = msg->row_stride / (msg->width); 
    
    if(ch==0)
        ch = 3;

    if(ch == 1){
        fprintf(stderr, "No color channels\n");
        return 0;
    }
    if(msg->pixelformat == BOT_CORE_IMAGE_T_PIXEL_FORMAT_GRAY){
        fprintf(stderr, "Image is gray\n");
        //should handle this 
        return 0;
    }
    
    if (img.empty() || img.rows != msg->height || img.cols != msg->width)
        img.create(msg->height, msg->width, CV_8UC3);

    // extract image data
    fprintf(stderr, "Image format : %d - Channels : %d\n", msg->pixelformat, ch);
    switch (msg->pixelformat) {
    case BOT_CORE_IMAGE_T_PIXEL_FORMAT_RGB:
        fprintf(stderr, "RGB\n");
        memcpy(img.data, msg->data, sizeof(uint8_t) * msg->width * msg->height * 3);
        cv::cvtColor(img, img, CV_RGB2BGR);
        cv::cvtColor(img, img_gray, CV_BGR2GRAY);
        break;
    case BOT_CORE_IMAGE_T_PIXEL_FORMAT_MJPEG:
        if (ch == 3) { 
            jpeg_decompress_8u_rgb(msg->data,
                                   msg->size,
                                   img.data,
                                   msg->width,
                                   msg->height,
                                   msg->width * ch);
            cv::cvtColor(img, img, CV_RGB2BGR);
            fprintf(stderr, "JPEG\n");
            cv::cvtColor(img, img_gray, CV_BGR2GRAY);
        } 
        break;
    default:
        fprintf(stderr, "Unrecognized image format\n");
        return 0;
        break;
    }
    return 1;
}

void on_laser(const lcm_recv_buf_t *rbuf, const char * channel, 
              const bot_core_planar_lidar_t * msg, void * user) {  
    Detector *detector = (Detector *) user;
    g_mutex_lock(detector->mutex);
    if(detector->last_laser != NULL){
        bot_core_planar_lidar_t_destroy(detector->last_laser);        
    }
    
    int64_t utime = bot_timestamp_now();
    //fprintf(stderr, "Laser Delta : %.3f\n", (msg->utime - utime)/1.0e6);
    if(detector->last_img){
        fprintf(stderr, "Laser To Image Delta : %.3f\n", (msg->utime - detector->last_img->utime)/1.0e6);
    }
    detector->last_laser = bot_core_planar_lidar_t_copy(msg);
    g_mutex_unlock(detector->mutex);
}

void on_image(const lcm_recv_buf_t *rbuf, const char * channel, 
              const bot_core_image_t * msg, void * user) {  
    Detector *detector = (Detector *) user;
    g_mutex_lock(detector->mutex);
    if(detector->last_img != NULL){
        bot_core_image_t_destroy(detector->last_img);        
    }
    
    int64_t utime = bot_timestamp_now();
    //fprintf(stderr, "Image Delta : %.3f\n", (msg->utime - utime)/1.0e6);
    //if(detector->last_laser){
    // fprintf(stderr, "Laser To Image Delta : %.3f\n", (msg->utime - detector->last_laser->utime)/1.0e6);
    //}
    detector->last_img = bot_core_image_t_copy(msg);
    g_mutex_unlock(detector->mutex);
}

void on_status(const lcm_recv_buf_t *rbuf, const char * channel, 
              const perception_status_t * msg, void * user) {  
    Detector *detector = (Detector *) user;
    if(msg->status == PERCEPTION_STATUS_T_RESET){
        g_mutex_lock(detector->mutex);
        detector->clearDetections();
        g_mutex_unlock(detector->mutex);
    }
}



//not used right now 
static void *track_tags(void *user)
{
    Detector * self = (Detector *) user;

    int status = 0;
    while(1){
        int processed = self->detect_on_last_image();   
        if(processed){            
            usleep(500);
        }
        else{            
            usleep(1000);
        }
    }
}

// here is were everything begins
int main(int argc, char* argv[]) {
    Detector detector;

    // process command line options
    detector.parseOptions(argc, argv);

    // setup image source, window for drawing, serial port...
    bot_core_image_t_subscribe(detector.lcm, detector.channel, on_image, &detector);
    perception_status_t_subscribe(detector.lcm, "SLAM_STATUS", on_status, &detector);
    //bot_core_planar_lidar_t_subscribe(detector.lcm, "SKIRT_FRONT", on_laser, &detector);
    // the actual processing loop where tags are detected and visualized
    
    pthread_create(&detector.process_thread , NULL, track_tags, &detector);

    detector.loop();

    return 0;
}
