FILE(REMOVE_RECURSE
  "../lcmtypes/c"
  "../lcmtypes/cpp"
  "../lcmtypes/java"
  "../lcmtypes/python"
  "CMakeFiles/lcmtypes_objects_jar"
  "lcmtypes_objects.jar"
  "../lcmtypes/java/perception/status_t.class"
  "../lcmtypes/java/perception/object_detection_list_t.class"
  "../lcmtypes/java/perception/object_detection_t.class"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/lcmtypes_objects_jar.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
