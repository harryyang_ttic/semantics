/*
 * NodeScan.cpp
 *
 *  Created on: Oct 22, 2009
 *      Author: abachrac
 */

#include "NodeScan.h"

using namespace scanmatch;

NodeScan::NodeScan(int64_t _utime, int _node_id, Scan * _scan, bool _is_supernode):
    utime(_utime), node_id(_node_id), scan(_scan)
{
    allScans.push_back(scan);
    pose2d_node = NULL;
    inc_constraint_sm = NULL;
    inc_constraint_odom = NULL;
    laser = NULL;
    is_supernode = _is_supernode;

    //create a kd tree 
    kd_points = (ann_t *) calloc(1,sizeof(ann_t));
    //add the scans 
    kd_points->dataPts = annAllocPts(scan->numPoints, 2);
    kd_points->no_points = scan->numPoints;
    //put the points in the data structure 
    for (unsigned i = 0; i < scan->numPoints; i++) {     
        kd_points->dataPts[i][0] = scan->points[i].x;
        kd_points->dataPts[i][1] = scan->points[i].y;
    }

    kd_tree = new ANNkd_tree(kd_points->dataPts, 
                             kd_points->no_points,
                             2);
    max_prob = 0;
}

NodeScan::~NodeScan()
{
    delete scan;
    //mighe want to look at this 
    delete inc_constraint_sm;
    delete inc_constraint_odom;
    if(laser)
        bot_core_planar_lidar_t_destroy(laser);

    /*map<int, PoseToPoseTransform *>::iterator s_it;
    for ( s_it= sm_constraints.begin() ; s_it != sm_constraints.end(); s_it++ ){
        fprintf(stderr, "Deleting constraints in the node\n");
        delete s_it->second;
        }*/
}

int NodeScan::updateScan()
{
    fprintf (stdout, "NodeScan::updateScan() - Nothing Happens!!!\n");
    return 0;
}
