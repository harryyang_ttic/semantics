/*
 * IsamSlam.h
 *
 *  Created on: Oct 22, 2009
 *      Author: abachrac
 */

#ifndef REGIONSLAM_H_
#define REGIONSLAM_H_
#include <scanmatch/ScanMatcher.hpp>
#include <isam/isam.h>
#include "AnchorPose.h"
#include "NodeScan.h"
#include <bot_core/bot_core.h>
#include <bot_lcmgl_client/lcmgl.h>
#include <pthread.h>
#include <er_lcmtypes/lcm_channel_names.h>
#include <simpleMMap/GridMap.hpp>
#include <vector>
#include <bot_frames/bot_frames.h>
#include <lcmtype/slam_lcmtypes.h>

// batch solve with variable reordering and relinearization every MOD_BATCH steps
const int MOD_BATCH = 100;
// for incremental steps, solve by backsubstitution every MOD_SOLVE steps
const int MOD_SOLVE = 10;

using namespace std;
using namespace scanmatch;

class RegionSlam {
public:
  RegionSlam(lcm_t * _lcm, double metersPerPixel_, double thetaResolution_, int useGradientAscentPolish_, int useMultires_,
           int doDrawing_, bool useThreads_, BotFrames *_frames);
  virtual ~RegionSlam();

  void doLoopClosing(int loop_close_ind);
  void addNodeToSlam(Pose2d &prev_curr_tranf, Noise &cov, Scan * scan, 
		     Scan * maxRangeScan, int64_t utime,
		     double height, double rp[2],int floor_no);

  void addNodeToSlamAnchor(Pose2d &prev_curr_tranf, Noise &cov, Scan * scan, Scan * maxRangeScan, int64_t utime,
				       double height, double rp[2], int floor_no, int region_no);

  Pose2d getCurrentPose(){
    if (useThreads)
       pthread_mutex_lock(&trajectory_mutex);
    Pose2d curr_pose;
    if (!trajectory.empty()){
        if(trajectory.back()->ap){
            curr_pose = trajectory.back()->ap->anchor2d_node->value().oplus(trajectory.back()->pose2d_node->value());
        }
        else{
            curr_pose = trajectory.back()->pose2d_node->value();
        }
    }
    else
      curr_pose = origin_node->value();
    if (useThreads)
      pthread_mutex_unlock(&trajectory_mutex);
    return curr_pose;
  }

  void drawGraph();
  void renderCurrentGridmap();
  void checkForUpdates();
  void draw3DPointCloud();

  AnchorPose * getAnchorPose(int region_id);
  
  BotFrames *frames;
  Pose2d_Node * origin_node;
  ScanMatcher * sm; //scan matcher object for loop closing
  Slam * slam; //isam slam object
  std::vector<NodeScan *> trajectory;
  std::vector<AnchorPose *> anchors;
  int loopClosedUpTill;
  int last_region_no;

  GridMap *gridmap;
  double gridmapMetersPerPixel;
  double gridmapMargin;
  unsigned int kernelSize; 
  unsigned char * squareKernel;
  unsigned char * lineKernel;
  unsigned char * diagLineKernel;
  float gridmapFreeIncrement;
  
  int doDrawing;
  lcm_t * lcm;

  //threading stuff
  bool useThreads;
  int killThread;
  pthread_t loop_closer;
  
  static void * loopClose_thread_func(RegionSlam *parent);
  pthread_mutex_t trajectory_mutex;
  pthread_cond_t loop_closer_cond;
};

#endif /* ISAMSLAM_H_ */
