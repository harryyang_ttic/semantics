#include "SlamGraph.hpp"


// The number and labels must match slam_language_label_t.lcm and the topological graph renderer
LabelDistribution::LabelDistribution(){
    num_labels = 7;
    total_obs = 1;
    labels.push_back("courtyard");
    saliency.push_back (0.15);
    labels.push_back("gym");
    saliency.push_back (0.15);
    labels.push_back("hallway");
    saliency.push_back (0.5);
    labels.push_back("amphitheater");
    saliency.push_back (0.15);
    labels.push_back("cafeteria");
    saliency.push_back (0.17);
    labels.push_back("elevator lobby");
    saliency.push_back (0.17);
    labels.push_back("entrance");
    saliency.push_back (0.2);
    //observation_frequency.push_back(1);  Commented out lines are code for with an 'unspecified' label
    for(int i=0; i<num_labels; i++){       // bhomberg 09/11/12
    //for(int i=0; i<num_labels-1; i++){
      //observation_frequency.push_back(1.0/(num_labels-1));
      observation_frequency.push_back(1.0/(num_labels));
    }
    for (int i=0; i<num_labels; i++)
        count.push_back (0);

    last_direct_obs_id = -1;
      
}

// // The number and labels must match slam_language_label_t.lcm and the topological graph renderer
// LabelDistribution::LabelDistribution(vector<double> obs){
//     num_labels = 7;
//     total_obs = 2;

//     labels.push_back("courtyard");
//     saliency.push_back (0.15);
//     labels.push_back("gym");
//     saliency.push_back (0.15);
//     labels.push_back("hallway");
//     saliency.push_back (0.5);
//     labels.push_back("amphitheater");
//     saliency.push_back (0.15);
//     labels.push_back("cafeteria");
//     saliency.push_back (0.17);
//     labels.push_back("elevator lobby");
//     saliency.push_back (0.17);
//     labels.push_back("entrance");
//     saliency.push_back (0.2);
       
//     for(int i=0; i<num_labels; i++)
//       observation_frequency.push_back(obs.at(i));

//     for (int i=0; i<num_labels; i++)
//         count.push_back (count.at(i));

//     for (int i=0; i < labeled_node_ids.size(); i++)
//         labeled_node

//     //observation_frequency.push_back(1);
//     //for(int i=0; i<num_labels-1; i++){
//     //  observation_frequency.push_back(1.0/(num_labels-1));
//     //}
      
// }


// Incorporate direct observation by incrementing specific label
void LabelDistribution::addObservation(int l, double increment, int lang_update_id) {

    total_obs+=increment;
    observation_frequency.at(l) = observation_frequency.at(l) + increment;
    
    count.at(l) = count.at(l) + 1;
    labeled_node_ids.push_back (lang_update_id);
}

// Incorporate direct observation by incrementing specific label
void LabelDistribution::addDirectObservation(int l, double increment, int lang_update_id) {
    total_obs+=increment;
    observation_frequency.at(l) = observation_frequency.at(l) + increment;
    
    count.at(l) = count.at(l) + 1;
    labeled_node_ids.push_back (lang_update_id);
    last_direct_obs_id = lang_update_id;
}

int LabelDistribution::bleedLabels (LabelDistribution *ld_src){
    double increment = LABEL_BLEED_INCREMENT;
    int last_lang_update_id = ld_src->last_direct_obs_id;

    int bled = 0;
    for (int i=0; i < ld_src->count.size(); i++) {
        if (ld_src->count.at(i) >= 1) {
            fprintf(stderr, "LD Src : %d - Obs Freq : %d\n", ld_src->count.size(), 
                    observation_frequency.size());
            total_obs += increment;
            observation_frequency.at(i) = observation_frequency.at(i) + increment;
            bled = 1;
        }
    }

    // Now copy over the originating labeled node ids from the source
    if (bled) {
        if(last_lang_update_id != -1){
            labeled_node_ids.push_back(last_lang_update_id);
        }

        /*vector <int>::iterator it;
        for (int i=0; i < ld_src->labeled_node_ids.size(); i++) {
            it = find (labeled_node_ids.begin(), labeled_node_ids.end(), ld_src->labeled_node_ids.at(i));
            if (it == labeled_node_ids.end())
                labeled_node_ids.push_back (lang_update_id);//ld_src->labeled_node_ids.at(i));
                }*/
    }
    return bled;
}

LabelDistribution * LabelDistribution::copy(){
    LabelDistribution *ld = new LabelDistribution();
    ld->total_obs = total_obs;

    for(int i=0; i < num_labels; i++)
        ld->observation_frequency.at(i) = observation_frequency.at(i);
    
    for (int i=0; i < num_labels; i++)
        ld->count.at(i) = count.at(i);
    
    for (int i=0; i < labeled_node_ids.size(); i++)
        ld->labeled_node_ids.push_back (labeled_node_ids.at(i));
   
    ld->last_direct_obs_id = last_direct_obs_id;

    return ld;
}


bool LabelDistribution::areLabelsSalient () {

    double observation_frequency_total;
    // Find the most observed label
    for (int i=0; i < num_labels; i++) 
        observation_frequency_total += observation_frequency.at(i);

    for (int i=0; i < num_labels; i++) {
        if (observation_frequency.at(i) / observation_frequency_total > saliency.at(i))
            return true;
    }

    return false;
}

    

