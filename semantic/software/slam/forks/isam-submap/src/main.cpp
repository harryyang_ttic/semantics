/**
 * @file anchorNodes.cpp
 * @brief Multiple relative pose graphs with relative constraints.
 * @author Michael Kaess
 * @version $Id: anchorNodes.cpp 6335 2012-03-22 23:13:52Z kaess $
 *
 * For details on the concept of anchor nodes see:
 * “Multiple Relative Pose Graphs for Robust Cooperative Mapping”
 * B. Kim, M. Kaess, L. Fletcher, J. Leonard, A. Bachrach, N. Roy, and S. Teller
 * IEEE Intl. Conf. on Robotics and Automation, ICRA, (Anchorage, Alaska), May 2010, pp. 3185-3192.
 * online available at http://www.cc.gatech.edu/~kaess/pub/Kim10icra.html
 *
 * Copyright (C) 2009-2012 Massachusetts Institute of Technology.
 * Michael Kaess, Hordur Johannsson, David Rosen and John J. Leonard
 *
 * This file is part of iSAM.
 *
 * iSAM is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * iSAM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with iSAM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "SlamParticle.hpp"
#include <iostream>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

#include <opencv/highgui.h>
#include <getopt.h>

#include <lcm/lcm.h>
#include <bot_core/bot_core.h>
//bot param stuff
#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_lcmgl_client/lcmgl.h>

#include <gsl/gsl_blas.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_sf_bessel.h>
#include <gsl/gsl_integration.h>
#include <math.h>

#include <scanmatch/ScanMatcher.hpp>
#include <lcmtypes/sm_rigid_transform_2d_t.h>
#include <bot_frames/bot_frames.h>
#include <lcmtypes/er_lcmtypes.h>

#include <isam/isam.h>
#include <isam/Anchor.h>

#include <occ_map/PixelMap.hpp>

//#include "RegionSlam.h"
#include "NodeScan.h"
#include <lcmtypes/slam_lcmtypes.h>
#include <lcmtypes/slam_graph_particle_list_t.h>
#include <lcmtypes/slam_language_edge_t.h>
#include <lcmtypes/slam_dirichlet_update_t.h>
//#include <lcmtypes/object_door_list_t.h>
#include <lcmtypes/slam_init_node_t.h>
#include "SlamGraph.hpp"


#include <door_detector_laser/door_detect_laser.h>

#define RESET_COLOR "\e[m"
#define MAKE_GREEN "\e[32m"

#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif 

using namespace scanmatch;
using namespace std;
using namespace isam;
using namespace Eigen;

//add a new node every this many meters
#define LINEAR_DIST_TO_ADD 1.0//0.5//1.0
#define LINEAR_DIST_TO_ADD_SUPERNODE 5.0//5.0//6.0//3.0//0.5//1.0
//and or this many radians
#define ANGULAR_DIST_TO_ADD 1.0
#define ANGULAR_DIST_TO_ADD_SUPERNODE 2*M_PI//2.0
#define LASER_DATA_CIRC_SIZE 80

typedef struct _laser_odom_t{
    //Pose2d *odom; 
    sm_rigid_transform_2d_t *odom;
    bot_core_planar_lidar_t *laser;
    bot_core_planar_lidar_t *rlaser;
    int force_supernode;
} laser_odom_t; 

typedef struct {
    lcm_t * lcm;
    //RegionSlam * regionslam;
    ScanMatcher *sm;
    ScanMatcher *sm_incremental;
    ScanMatcher *sm_loop;
    ScanMatcher *sm_lc_low; //low resolution scanmatcher to use to get a good startoff point
    int mode;
    BotFrames *frames;
    BotParam *param;
    bot_lcmgl_t * lcmgl_basic;
    bot_lcmgl_t * lcmgl_frame_test;
    bot_lcmgl_t * lcmgl_debug;
    bot_lcmgl_t * lcmgl_covariance;
    bot_lcmgl_t * lcmgl_particles;
    bot_lcmgl_t * lcmgl_sm_basic;
    bot_lcmgl_t * lcmgl_sm_graph;
    bot_lcmgl_t * lcmgl_sm_prior;
    bot_lcmgl_t * lcmgl_sm_result;
    bot_lcmgl_t * lcmgl_sm_result_low;
    bot_lcmgl_t * lcmgl_loop_closures;
    int ignoreLanguage;
    bool useLowResPrior;
    int useSMCov; 
    int scanmatchBeforeAdd;
    int probMode;
    int useCopy;
    bool useOnlyRegion;
    int alternateScanmatch;
    bool addDummyConstraints;
    bool useICP;
    int noScanTransformCheck;
    int strictScanmatch; //use other checks to make sure that the scanmatch was good
    double odometryConfidence;
    slam_pixel_map_request_t *map_request; 
    int publish_map;
    int useOdom; 
    char * chan;
    char * rchan;
    int draw_scanmatch; 
    int draw_map;
    int max_prob_particle_id;
    int keepDeadEdges;
    int publish_occupancy_maps;
    int64_t publish_occupancy_maps_last_utime;
    double sm_acceptance_threshold;
    object_door_list_t *door_list; 
    slam_init_node_t *node_init;
    int beam_skip; //downsample ranges by only taking 1 out of every beam_skip points
    double spatialDecimationThresh; //don't discard a point if its range is more than this many std devs from the mean range (end of hallway)
    double maxRange; //discard beams with reading further than this value
    double maxUsableRange; //only draw map out to this far for MaxRanges...
    float validBeamAngles[2]; //valid part of the field of view of the laser in radians, 0 is the center beam
    Scan * last_scan;
    sm_rigid_transform_2d_t * prev_odom; //odometry computed outside module
    sm_rigid_transform_2d_t * prev_added_odom; //odometry computed outside module
    sm_rigid_transform_2d_t * prev_super_odom; //odometry computed outside module
    sm_rigid_transform_2d_t * prev_door_odom; //odometry computed outside module
    sm_rigid_transform_2d_t * prev_sm_odom; //internally scan matched odometry
    int resample;
    int verbose;
    int slave_mode;
    int use_doorways;

    BotPtrCircular   *front_laser_circ;
    BotPtrCircular   *rear_laser_circ;
    
    //when a new init message is heard - a scan is added to the hash table - and also 
    //a scan match is done with the previous node 
    //the constraint is added to the relavent nodes - for reuse     
    GHashTable *slam_nodes;

    vector<laser_odom_t *> lasers_to_add;
    vector<NodeScan *> slam_nodes_to_add;

    vector<slam_language_edge_t *> language_edges_to_add;
    vector<slam_dirichlet_update_t *> dirichlet_updates_to_add;
    vector<slam_language_label_t *> language_labels_to_add;
    int got_label;

    vector<SlamParticle *> slam_particles;
    NodeScan * last_slampose;

    int waiting_for_slu; 
    //we should only get one?? 
    slam_slu_result_list_t *slu_result; 
    int last_slampose_ind;
    int last_slampose_for_process_ind;
    int node_count;
    int bad_mode;

    int particle_id_to_train;

    SlamGraph *basic_graph; //a slam graph that doesn't have loop closure constraints - used for scan matching 

    bot_core_planar_lidar_t * r_laser_msg;
    bot_core_pose_t *last_deadreakon; 
    int64_t r_utime;

    int64_t last_node_sent_utime;

    pthread_t  scanmatch_thread;
    pthread_t  laser_process_thread;

    int no_particles;

    GMutex *mutex;
    GMutex *mutex_language;
    GMutex *mutex_particles;
    GMutex *mutex_laser_queue;
    GMutex *mutex_lasers;

    int64_t last_graph_processed_time;

    int use_frames; 
    int region;
    
    int next_id;
    int find_ground_truth;
    FloatPixelMap *local_px_map; // Local occupancy map used for measurement likelihood
} app_t;

bot_core_planar_lidar_t *get_closest_rear_laser(app_t *app, int64_t utime);

//lets do this based on interaction from the viewer (so when we get a request for a particular map 
//publish that out
 
// Publishes occupancy grid maps for the most likely particle
void publish_occupancy_maps (app_t *app) {

    // Determine most-likely particle id
    int max_particle_idx = 0;
    double weight_max = 0;

    for (int i=0; i < app->slam_particles.size(); i++) {
        SlamParticle *particle = app->slam_particles.at(i);

        if (particle->normalized_weight > weight_max) {
            weight_max = particle->normalized_weight;
            max_particle_idx = i;
        }
    }

    SlamParticle *particle = app->slam_particles.at(max_particle_idx);
    
    particle->publishOccupancyMap ();
}

slam_graph_particle_list_t *get_slam_particle_msg(app_t *app){
    slam_graph_particle_list_t *msg = (slam_graph_particle_list_t *) calloc(1, sizeof(slam_graph_particle_list_t));
    msg->utime = bot_timestamp_now();
    msg->no_particles = app->slam_particles.size();
    
    msg->particle_list = (slam_graph_particle_t *) calloc(msg->no_particles, sizeof(slam_graph_particle_t));
    msg->status = SLAM_GRAPH_PARTICLE_LIST_T_STATUS_PROCESSED;

    for(int i=0; i < msg->no_particles; i++){
        SlamParticle *part = app->slam_particles.at(i);
        part->getLCMMessageFromGraph(&msg->particle_list[i]);
    }
    return msg;
}

//Creates a particle list message from the SlamGraphs and then publishes that 
//Will be used by the renderer and the speech module
void publish_slam_particles(app_t *app){
    slam_graph_particle_list_t *msg = (slam_graph_particle_list_t *) calloc(1, sizeof(slam_graph_particle_list_t));
    msg->utime = bot_timestamp_now();
    msg->no_particles = app->slam_particles.size();

    msg->particle_list = (slam_graph_particle_t *) calloc(msg->no_particles, sizeof(slam_graph_particle_t));
    msg->status = SLAM_GRAPH_PARTICLE_LIST_T_STATUS_PROCESSED;

    for(int i=0; i < msg->no_particles; i++){
        SlamParticle *part = app->slam_particles.at(i);
        part->getLCMMessageFromGraph(&msg->particle_list[i]);
    }

    slam_graph_particle_list_t_publish(app->lcm, "PARTICLE_ISAM_RESULT", msg);
    slam_graph_particle_list_t_destroy(msg);
}

//publish the local to global transforms for each particle 
//the renderer will pick the valid one and publish the transform 
void publish_slam_transforms(app_t *app){
    erlcm_rigid_transform_list_t msg;
    msg.utime = bot_timestamp_now();
    msg.num = app->slam_particles.size();
    msg.list = (erlcm_rigid_transform_t *) calloc(msg.num, sizeof(erlcm_rigid_transform_t));
    bot_lcmgl_t *lcmgl = app->lcmgl_frame_test;
    bot_lcmgl_point_size(lcmgl, 10);
    bot_lcmgl_color3f(lcmgl, 1.0, 0, 0);
    bot_lcmgl_begin(lcmgl, GL_POINTS);

    for(int i=0; i < app->slam_particles.size(); i++){
        SlamParticle *part = app->slam_particles.at(i);

        BotTrans global_to_local = part->getBotTransForCurrentNode();
        msg.list[i].id = part->graph_id;
        bot_core_rigid_transform_t *bt_transform = &msg.list[i].transform;
        memcpy (bt_transform->trans, global_to_local.trans_vec, 3*sizeof(double));
        memcpy (bt_transform->quat, global_to_local.rot_quat, 4*sizeof(double));
        //lets lcmgl it??
        Pose2d last_pose = part->getLastPose();
        double pos[3] = {last_pose.x(),last_pose.y(),0};
        double l_pos[3];
        bot_trans_apply_vec(&global_to_local, pos, l_pos);
        bot_lcmgl_vertex3f(lcmgl, pos[0], pos[1], pos[2]);          
        //bot_lcmgl_vertex3f(lcmgl, l_pos[0], l_pos[1], l_pos[2]);          
    }

    bot_lcmgl_end(lcmgl);
    bot_lcmgl_switch_buffer(lcmgl);
     
    erlcm_rigid_transform_list_t_publish(app->lcm, "SLAM_TRANSFORMS", &msg);
    free(msg.list);
}


//go through the vector - and pop the old stuff up to the last supernode
//if there are no new nodes - don't update 
int get_new_slam_nodes(app_t *app, vector<NodeScan *> *new_node_list){

    if(app->slam_nodes_to_add.size() == 0)
        return 0;
    
    int has_supernode = 0;
    int added_new_node = 0;
    //go through the new pose queue - and copy over nodes upto the first supernode 
    while (!app->slam_nodes_to_add.empty()){
        NodeScan *pose = app->slam_nodes_to_add.back();
        app->slam_nodes_to_add.pop_back();
        if (app->verbose)
            fprintf(stderr, "Slam node id : %d => Super : %d\n", pose->node_id, pose->is_supernode);
        if(pose != NULL){
            g_hash_table_insert(app->slam_nodes, &(pose->node_id), pose);
            new_node_list->push_back(pose);
            added_new_node = 1;
            app->last_slampose_for_process_ind = pose->node_id; //this is not used - so we should take out at some point 
            if(pose->is_supernode){
                has_supernode = 1;
                break;
            }
        }
    }
    if(has_supernode)
        return 2;
    else if(added_new_node)
        return 1;
    else
        return 0;
}

/*
 * Publish the laser scan points out - for rendering 
 */
void publish_slampose(app_t *app, Scan *scan, int64_t utime, int64_t id){

    slam_laser_pose_t msg;
    msg.utime = utime;
    msg.id = id;
    msg.rp[0] = 0;
    msg.rp[1] = 0;
    msg.pl.no = scan->numPoints;
    msg.pl.points = (slam_scan_point_t *) calloc(scan->numPoints, sizeof(slam_scan_point_t));
    for (unsigned i = 0; i < scan->numPoints; i++) {  
        msg.pl.points[i].pos[0] =  scan->points[i].x;
        msg.pl.points[i].pos[1] =  scan->points[i].y;
    }
    slam_laser_pose_t_publish(app->lcm, "SLAM_POSE_LASER_POINTS", &msg);
    free(msg.pl.points);
}


//signal handling to exit program 
sig_atomic_t still_groovy = 1;

static void sig_action(int signal, siginfo_t *s, void *user)
{
    still_groovy = 0;
}


Scan *get_maxrange_scan(float * ranges, int numPoints, double thetaStart, 
                        double thetaStep, double maxRange,
                        double maxUsableRange, double validRangeStart, double validRangeEnd)
{
    Scan * scan = new Scan();
    smPoint * points = (smPoint *) malloc(numPoints * sizeof(smPoint));
    scan->points = points;
    int numMaxRanges = 0;
    double theta = thetaStart;
    Contour * contour = new Contour();
    scan->contours.push_back(contour);
    for (int i = 0; i < numPoints; i++) {
        double r = ranges[i];
        if ((r <= 0.01) || (r >= maxRange)) {
            continue;
        }
        if (r < .1) {
            r = maxRange;
        }
        if (r >= maxRange && theta > validRangeStart && theta < validRangeEnd) {
            r = maxUsableRange;
            //project to body centered coordinates
            points[numMaxRanges].x = r * cos(theta);
            points[numMaxRanges].y = r * sin(theta);
            contour->points.push_back(points[numMaxRanges]);
            numMaxRanges++;

        }
        else if (contour->points.size() > 0) {
            //end that contour
            if (contour->points.size() > 2) {
                //clear out middle points
                contour->points[1] = contour->points.back();
                contour->points.resize(2);
            }
            contour = new Contour();
            scan->contours.push_back(contour);
        }
        else if (contour->points.size() == 1) {
            //discard single maxrange beams...
            contour->points.clear();
        }
        theta += thetaStep;
    }

    if (contour->points.size() < 2) {
        scan->contours.pop_back();
        delete contour;
    }

    points = (smPoint*) realloc(points, numMaxRanges * sizeof(smPoint));
    scan->numPoints = numMaxRanges;
    scan->ppoints = (smPoint *) malloc(numMaxRanges * sizeof(smPoint));
    memcpy(scan->ppoints, points, numMaxRanges * sizeof(smPoint));
    return scan;
}

static void add_to_queue(const bot_core_planar_lidar_t * laser_msg, const sm_rigid_transform_2d_t * odom,
                         int64_t utime, app_t * app, int force_supernode = 0)
{
    static int first_scan = 1;
    static int first_laser_scan = 1;
    
    //compute the distance between this scan and the last one that got added.
    double dist[2];
    sm_vector_sub_2d(odom->pos, app->prev_odom->pos, dist);

    double ld = sm_norm(SMPOINT(dist));
    double ad = sm_angle_subtract(odom->theta, app->prev_odom->theta);
    bool addScan = false;

    if(force_supernode){
        //make sure its not the same door 
        double door_dist[2];
        sm_vector_sub_2d(odom->pos, app->prev_door_odom->pos, door_dist);
        double ld_door = sm_norm(SMPOINT(dist));
        if(ld_door > 1.0){
            if (app->verbose)
                fprintf(stderr, "================ Found Doorway\n");
            if (app->prev_door_odom != NULL)
                sm_rigid_transform_2d_t_destroy(app->prev_door_odom);
            
            app->prev_door_odom = sm_rigid_transform_2d_t_copy(odom);
            addScan = true;
        }
        else{
            force_supernode = 0;
        }
    }

    if(!app->slave_mode){
        //this should be taken off when node creation is automatic
        if (fabs(ld) > LINEAR_DIST_TO_ADD) 
            addScan = true;

        if (fabs(ad) > ANGULAR_DIST_TO_ADD) 
            addScan = true;
        
        if(first_scan){
            addScan = true;
            first_scan = 0;
        }       
    }

    //if in slave mode - we add all new requests 
    if(app->slave_mode && app->node_init != NULL)
        addScan = true; 

    if(!addScan)
        return;

    //get the odometry delta
    Pose2d curr_pose(odom->pos[0], odom->pos[1], odom->theta);
    Pose2d prev_pose(app->prev_odom->pos[0], app->prev_odom->pos[1], app->prev_odom->theta);
    Pose2d prev_curr_tranf = curr_pose.ominus(prev_pose);

    if (app->prev_odom != NULL)
        sm_rigid_transform_2d_t_destroy(app->prev_odom);
    app->prev_odom = sm_rigid_transform_2d_t_copy(odom);

    laser_odom_t *l_odom = (laser_odom_t *) calloc(1, sizeof(laser_odom_t));

    bot_core_planar_lidar_t *rlaser = get_closest_rear_laser(app, laser_msg->utime);
    
    l_odom->laser = bot_core_planar_lidar_t_copy(laser_msg);
    l_odom->rlaser = NULL;
    
    if(rlaser != NULL){
        l_odom->rlaser = bot_core_planar_lidar_t_copy(rlaser);
    }

    l_odom->odom =  sm_rigid_transform_2d_t_copy(odom);//new Pose2d(odom);//(prev_curr_tranf);
    l_odom->force_supernode = force_supernode;

    //lock a mutex
    g_mutex_lock(app->mutex_laser_queue);
    app->lasers_to_add.push_back(l_odom);
    g_mutex_unlock(app->mutex_laser_queue);
}

inline int
sm_projectRangesToPointsLocal(const float * ranges, int numPoints, double thetaStart,
                              double thetaStep, smPoint * points, double maxRange = 1e10,
                              double validRangeStart = -1000, double validRangeEnd = 1000,
                              double * aveValidRange = NULL, double * stddevValidRange = NULL)
{
    int count = 0;
    double aveRange = 0;
    double aveRangeSq = 0;

    double theta = thetaStart;
    for (int i = 0; i < numPoints; i++) {
        if (ranges[i] > .1 && ranges[i] < maxRange && theta > validRangeStart
            && theta < validRangeEnd) { //hokuyo driver seems to report maxRanges as .001 :-/
            //project to body centered coordinates
            points[count].x = ranges[i] * cos(theta);
            points[count].y = ranges[i] * sin(theta);
            count++;
            aveRange += ranges[i];
            aveRangeSq += sm_sq(ranges[i]);
        }

        theta += thetaStep;
    }

    aveRange /= (double) count;
    aveRangeSq /= (double) count;

    if (aveValidRange != NULL)
        *aveValidRange = aveRange;
    if (stddevValidRange != NULL)
        *stddevValidRange = sqrt(aveRangeSq - sm_sq(aveRange));

    return count;
}

//adding a filter to reject outliers 
inline int
sm_projectRangesAndDecimateLocal(int beamskip, float spatialDecimationThresh,
                                 const float * ranges, int numPoints, double thetaStart, double thetaStep,
                                 smPoint * points, double maxRange = 1e10, double validRangeStart =
                                 -1000, double validRangeEnd = 1000)
{
    int lastAdd = -1000;
    double aveRange;
    double stdDevRange;
    int numValidPoints = sm_projectRangesToPointsLocal(ranges, numPoints,
                                                  thetaStart, thetaStep, points, maxRange, validRangeStart,
                                                  validRangeEnd, &aveRange, &stdDevRange);

    smPoint origin =
        { 0, 0 };
    smPoint lastAddPoint =
        { 0, 0 };
    int numDecPoints = 0;

    int window_size = 5;

    fprintf(stderr, "\n\n\nAvg Range : %f Std Dev : %f\n\n\n", aveRange , stdDevRange);

    for (int i = 0; i < numValidPoints; i++) {
        //add every beamSkip beam, unless points are more than spatialDecimationThresh, or more than 1.8 stdDevs more than ave range
        if ((i - lastAdd) > beamskip || sm_dist(&points[i], &lastAddPoint)
            > spatialDecimationThresh || sm_dist(&points[i], &origin)
            > (aveRange + 1.8 * stdDevRange)) {
            lastAdd = i;
            lastAddPoint = points[i];
            points[numDecPoints] = points[i]; // ok since i >= numDecPoints
            numDecPoints++;
        }
    }

    smPoint * pointsNew = (smPoint *) calloc(numDecPoints, sizeof(smPoint));
    int prevAdded = -1;
    int newCount = 0;
    for (int i = 0; i < numDecPoints; i++) {
        int min = fmax(0, i-window_size);
        int max = fmin(numDecPoints-1, i + window_size);

        double r = hypot(points[i].x, points[i].y);
        double gap = 0;
        if(prevAdded >0){
            gap = hypot(points[i].x - points[prevAdded].x, points[i].y - points[prevAdded].y);
        }

        /*if(gap > 10.0){
            fprintf(stderr, "skipping\n");
            continue;
            }*/

        double mean = 0;
        for(int j=min; j <= max; j++){
            mean += hypot(points[j].x, points[j].y);
        }
        mean /= (max - min + 1);

        if(fabs(r - mean) > 5.0){
            fprintf(stderr, "Outside the mean %f - skipping : %f\n", mean, fabs(r - mean));
            continue;
        }
        prevAdded = i;
        pointsNew[newCount].x = points[i].x;
        pointsNew[newCount].y = points[i].y;
        newCount++;
    }

    memcpy(points, pointsNew, sizeof(smPoint)*newCount);
    free(pointsNew);
    numDecPoints = newCount;

    return numDecPoints;
}

smPoint *get_scan_points_from_laser(bot_core_planar_lidar_t *laser_msg, app_t *app, char *frame, int *noPoints){
    smPoint * points = (smPoint *) calloc(laser_msg->nranges, sizeof(smPoint));
    int numValidPoints = sm_projectRangesAndDecimate(app->beam_skip, app->spatialDecimationThresh, laser_msg->ranges,
                                                         laser_msg->nranges, laser_msg->rad0, laser_msg->radstep, points, 
                                                         app->maxRange, app->validBeamAngles[0], app->validBeamAngles[1]);
    *noPoints = numValidPoints;
    if (numValidPoints < 30) {
        fprintf(stderr, "WARNING! NOT ENOUGH VALID POINTS! numValid=%d\n", numValidPoints);
        free(points);
        *noPoints = 0;
        return NULL;
    }
    else {
        points = (smPoint *) realloc(points, numValidPoints * sizeof(smPoint));//crop memory down to size
    }
    //adding laser offset
    double sensor_to_body[12];
    if (!bot_frames_get_trans_mat_3x4 (app->frames, frame, 
                                       "body",
                                       sensor_to_body)) {
        fprintf (stderr, "Error getting bot_frames transformation from %s to body!\n", app->chan);
        free (points);
        *noPoints = 0;
        return NULL;
    }
    //transform to body pose
    double pos_s[3] = {0}, pos_b[3];
    for(int i=0; i < numValidPoints; i++){
        pos_s[0] = points[i].x;
        pos_s[1] = points[i].y;
        bot_vector_affine_transform_3x4_3d (sensor_to_body, pos_s , pos_b);
        points[i].x = pos_b[0];
        points[i].y = pos_b[1];
    }
    return points;
}

smPoint *get_scan_points_from_laser_compensated(bot_core_planar_lidar_t *laser_msg, app_t *app, char *frame, int *noPoints, int64_t utime){
    smPoint * points = (smPoint *) calloc(laser_msg->nranges, sizeof(smPoint));
    int numValidPoints = sm_projectRangesAndDecimate(app->beam_skip, app->spatialDecimationThresh, laser_msg->ranges,
                                                     laser_msg->nranges, laser_msg->rad0, laser_msg->radstep, points, 
                                                     app->maxRange, -M_PI/4, M_PI/4); //app->validBeamAngles[0], app->validBeamAngles[1]);
    *noPoints = numValidPoints;
    if (numValidPoints < 30) {
        fprintf(stderr, "WARNING! NOT ENOUGH VALID POINTS! numValid=%d\n", numValidPoints);
        free(points);
        *noPoints = 0;
        return NULL;
    }
    else {
        points = (smPoint *) realloc(points, numValidPoints * sizeof(smPoint));//crop memory down to size
    }
    //adding laser offset
    BotTrans sensor_to_local_sensor_time;
    BotTrans local_to_body_comp_time;

    int status_1 = bot_frames_get_trans_with_utime(app->frames, frame, 
                                    "local",
                                    laser_msg->utime, 
                                    &sensor_to_local_sensor_time);

    int status_2 = bot_frames_get_trans_with_utime(app->frames, "local", 
                                    "body",
                                    utime, 
                                    &local_to_body_comp_time);

    if(!status_1 || !status_2){
        fprintf (stderr, "Error getting bot_frames transformation from %s to body!\n", app->chan);
        free (points);
        *noPoints = 0;
        return NULL;
    }

    //transform base map to particle's map frame - according to nd2 
    BotTrans sensor_to_body_frame;
    bot_trans_apply_trans_to(&local_to_body_comp_time, &sensor_to_local_sensor_time, &sensor_to_body_frame);
    
    double pos_s[3] = {0}, pos_b[3];
    for(int i=0; i < numValidPoints; i++){
        pos_s[0] = points[i].x;
        pos_s[1] = points[i].y;
        bot_trans_apply_vec(&sensor_to_body_frame, pos_s , pos_b);
        points[i].x = pos_b[0];
        points[i].y = pos_b[1];
    }

    //transform to body pose
    
    return points;
}


void process_laser_scan(app_t * app, laser_odom_t *l_odom){
    static int first_scan = 1; 
    static int first_laser_scan = 1;

    //rotate the cov to body frame
    double Rcov[9];
    Matrix3d cv(Rcov);
    
    bot_core_planar_lidar_t * laser_msg = l_odom->laser;
    bot_core_planar_lidar_t * r_laser_msg = l_odom->rlaser;
    int64_t utime = laser_msg->utime;
    int force_supernode = l_odom->force_supernode;
    static double dist_traveled = 0;
    static double angle_turned = 0;
    sm_rigid_transform_2d_t *odom = l_odom->odom;

    //get the odometry delta
    Pose2d curr_pose(odom->pos[0], odom->pos[1], odom->theta);
    Pose2d prev_pose(app->prev_added_odom->pos[0], app->prev_added_odom->pos[1], app->prev_added_odom->theta);
    Pose2d prev_curr_tranf = curr_pose.ominus(prev_pose);

    double dist[2];
    sm_vector_sub_2d(odom->pos, app->prev_added_odom->pos, dist);

    double ld = sm_norm(SMPOINT(dist));
    double ad = sm_angle_subtract(odom->theta, app->prev_odom->theta);
    
    if (app->prev_added_odom != NULL)
        sm_rigid_transform_2d_t_destroy(app->prev_added_odom);
    app->prev_added_odom = sm_rigid_transform_2d_t_copy(odom);
    
    dist_traveled += fabs(ld);
    angle_turned += fabs(prev_curr_tranf.t());
    
    if(app->verbose){
        fprintf(stderr, "Scan is %f meters from last add, adding scan\n", ld);
        fprintf(stderr, "Scan is %f degrees from last add, adding scan\n", prev_curr_tranf.t()* 180.0/M_PI);
    }

    int numValidPoints = 0;
    smPoint * points = get_scan_points_from_laser(laser_msg, app, app->chan , &numValidPoints);

    if(points == NULL)
        return;
    
    int r_numValidPoints = 0;
    smPoint * r_points = NULL;
    if(r_laser_msg != NULL){
        r_points =  get_scan_points_from_laser_compensated(r_laser_msg, app, app->rchan , &r_numValidPoints, laser_msg->utime);
            //get_scan_points_from_laser(r_laser_msg, app, app->rchan , &r_numValidPoints);
    }

    ScanTransform T;
    //just use the zero transform... it'll get updated the first time its needed
    memset(&T, 0, sizeof(T));

    Scan * scan;

    int no_of_points = numValidPoints + r_numValidPoints;
    smPoint *full_points =  (smPoint *) calloc(no_of_points, sizeof(smPoint));
    memcpy(full_points, points, sizeof(smPoint) * numValidPoints);
    memcpy(&full_points[numValidPoints], r_points, sizeof(smPoint) * r_numValidPoints);

    fprintf(stderr, "Total no of Points : %d\n", no_of_points);

    //scan = new Scan(numValidPoints, points, T, SM_HOKUYO_UTM, utime, true);
    scan = new Scan(no_of_points, full_points, T, SM_HOKUYO_UTM, utime, true);
  
    //first node wont have a constraint with another node 
    //add locks here somewhere also - if needed
    
    int node_id = app->node_count;
    if(app->slave_mode){
        if (app->verbose)
            fprintf(stderr, "New node id : %d\n", (int) app->node_init->id);
        node_id = app->node_init->id;            
    }
    
    publish_slampose(app, scan, utime, node_id);
    
    //compute the distance between this scan and the last one that got added.

    double dr = hypot(prev_curr_tranf.x(), prev_curr_tranf.y());
    double alpha1 = atan2(prev_curr_tranf.y(), prev_curr_tranf.x());
    double alpha2 = prev_curr_tranf.t() - alpha1;
    
    //double lambda1 = 0.0001*0.0001, lambda2 = 0.00001, lambda3 = 0.00001, lambda4 = 0.0001*0.0001; 
    double lambda1 = 0.001*0.001, lambda2 = 0.00001, lambda3 = 0.00001, lambda4 = 0.001*0.001; 
    
    Matrix3d V;
    V << -dr * sin(alpha1), dr, 0, dr * cos(alpha1), dr ,0, 1, 0, 1;

    Matrix3d M;
    
    M << lambda1 * pow(alpha1,2) + lambda2 * pow(dr,2), 0, 0, 
        0, lambda3 * pow(dr,2) + lambda4 * pow(alpha1,2) + lambda4 * pow(alpha2,2), 0, 
        0, 0, (lambda1 * pow(alpha2,2) + lambda2 * pow(dr,2));
    
    Matrix3d motion_cov = V * M * V.transpose();
    
    Noise *cov;
    if(first_laser_scan){
        cov =  new SqrtInformation(10000. * eye(3));
    }
    else{
        cov = new Covariance(motion_cov);
    }
    
    //************* Do the incremental scan match **************//
    
    sm_tictoc("IncrementalSMRefineMent");
    //do incremental scan matching to refine the odometry estimate
    Pose2d prev_sm_pose(app->prev_sm_odom->pos[0], app->prev_sm_odom->pos[1], app->prev_sm_odom->theta);
    Pose2d curr_sm_pose = prev_sm_pose.oplus(prev_curr_tranf);
    ScanTransform sm_prior;
    memset(&sm_prior, 0, sizeof(sm_prior));
    sm_prior.x = curr_sm_pose.x();
    sm_prior.y = curr_sm_pose.y();
    sm_prior.theta = curr_sm_pose.t();
    sm_prior.score = app->odometryConfidence; //wide prior
    
    int64_t s_utime = bot_timestamp_now();

    ScanTransform r = app->sm->matchSuccessive(points, numValidPoints, SM_HOKUYO_UTM, sm_get_utime(), false, &sm_prior);
    int64_t e_utime = bot_timestamp_now();

    curr_sm_pose.set(r.x, r.y, r.theta);
    sm_rigid_transform_2d_t sm_odom;
    memset(&sm_odom, 0, sizeof(sm_odom));
    sm_odom.utime = laser_msg->utime;
    sm_odom.pos[0] = r.x;
    sm_odom.pos[1] = r.y;
    sm_odom.theta = r.theta;
    memcpy(sm_odom.cov, r.sigma, 9 * sizeof(double));
    if (app->prev_sm_odom != NULL)
        sm_rigid_transform_2d_t_destroy(app->prev_sm_odom);
    app->prev_sm_odom = sm_rigid_transform_2d_t_copy(&sm_odom);
        
    Pose2d prev_curr_sm_tranf = curr_sm_pose.ominus(prev_sm_pose);
    
    double Rcov_sm[9];
    sm_rotateCov2D(r.sigma, -r.theta, Rcov_sm);
    Matrix3d cv_sm(Rcov_sm); 

    //Bianca - Covariance - previous to current constraint 
    Noise *cov_sm;
    if(app->useSMCov){
        if(first_laser_scan){
            cov_sm =  new SqrtInformation(10000. * eye(3));
        }
        else{
            cov_sm = new Covariance(cv_sm);
        }
    }
    else{
        if(first_laser_scan){
            cov_sm =  new SqrtInformation(10000. * eye(3));
        }
        else{
            double cov_hardcode[9] = { .0002, 0, 0, 0, .0002, 0, 0, 0, .0001 };
            Matrix3d cv_hardcode(cov_hardcode);
            cov_sm = new Covariance(cv_hardcode);
        }
    } 

    sm_tictoc("IncrementalSMRefineMent");

    bot_lcmgl_t *lcmgl = app->lcmgl_debug;
    if(app->last_scan != NULL){
        if(0){
            bot_lcmgl_t *lcmgl = app->lcmgl_debug;
            bot_lcmgl_point_size(lcmgl, 2);
            bot_lcmgl_begin(lcmgl, GL_POINTS);
            double pBody[3] = { 0, 0, 0 };
            double pLocal[3];
            bot_lcmgl_color3f(lcmgl, 1.0, 0.5, 0);
            for (unsigned i = 0; i < app->last_scan->numPoints; i++) {        
                pBody[0] = app->last_scan->points[i].x;
                pBody[1] = app->last_scan->points[i].y;
                bot_lcmgl_vertex3f(lcmgl, pBody[0], pBody[1], pBody[2]);
            }
            bot_lcmgl_end(lcmgl);
            
            BotTrans bodyToLocal;
            bodyToLocal.trans_vec[0] = prev_curr_sm_tranf.x();
            bodyToLocal.trans_vec[1] = prev_curr_sm_tranf.y();
            bodyToLocal.trans_vec[2] = 0;
            double rpy[3] = { 0, 0, prev_curr_sm_tranf.t()};
            bot_roll_pitch_yaw_to_quat(rpy, bodyToLocal.rot_quat);
            
            bot_lcmgl_point_size(lcmgl, 2);
            bot_lcmgl_begin(lcmgl, GL_POINTS);
            bot_lcmgl_color3f(lcmgl, 0, 0.5, 0.5);
            for (unsigned i = 0; i < scan->numPoints; i++) {        
                pBody[0] = scan->points[i].x;
                pBody[1] = scan->points[i].y;
                bot_trans_apply_vec(&bodyToLocal, pBody, pLocal);
                bot_lcmgl_vertex3f(lcmgl, pLocal[0], pLocal[1], pLocal[2]);                
            }
            bot_lcmgl_end(lcmgl);
            bot_lcmgl_switch_buffer(lcmgl);
        }
    }
    
    delete app->last_scan;
    
    app->last_scan = new Scan(numValidPoints, points, T, SM_HOKUYO_UTM, utime, true);

    // We are now done with points
    free (points);
    free (r_points);
    free (full_points);

    publish_slampose(app, scan, utime, node_id);
    
    double hitpct = r.hits / (double) numValidPoints;//scan->numPoints;
    
    bool is_super = false;
    
    double super_dist[2];
    sm_vector_sub_2d(odom->pos, app->prev_super_odom->pos, super_dist);
    //compute the distance between this scan and the last one that got added.
    double s_ld = sm_norm(SMPOINT(super_dist));
    double s_ad = sm_angle_subtract(odom->theta, app->prev_super_odom->theta);
    //this is wrong 
    //do the super node based on distance 
    if(dist_traveled > LINEAR_DIST_TO_ADD_SUPERNODE || angle_turned > ANGULAR_DIST_TO_ADD_SUPERNODE){
        is_super = true;
        dist_traveled = 0;
        angle_turned = 0;
    }
    
    if(force_supernode){
        is_super = true;
        dist_traveled = 0;
        angle_turned = 0;
    }

    first_laser_scan = 0;

    if (app->verbose)
        fprintf(stderr, "Node added => Node id : %d - Is Supernode : %d\n",node_id, is_super);

    //supernode is added - and the last supernode position is saved 
    if(is_super){
        if (app->prev_super_odom != NULL)
            sm_rigid_transform_2d_t_destroy(app->prev_super_odom);
        app->prev_super_odom = sm_rigid_transform_2d_t_copy(odom);
    }

    //get the BotTrans - for local to body 
    BotTrans body_to_local;
    if(!bot_frames_get_trans_with_utime(app->frames, "body", "local", laser_msg->utime, &body_to_local)){
        fprintf(stderr, "Error Getting Bot Trans\n");
    }

    //add a new Slam node 
    NodeScan * slampose = new NodeScan(utime, node_id, scan, is_super);
    slampose->laser = bot_core_planar_lidar_t_copy(laser_msg);
    slampose->body_to_local = body_to_local;
    app->node_count++;         
    PoseToPoseTransform *constraint_sm = new PoseToPoseTransform(new Pose2d(prev_curr_sm_tranf), slampose, app->last_slampose, SLAM_GRAPH_EDGE_T_TYPE_SM_INC, cov_sm, hitpct, ld, ad, 1);//cov_hc);
    
    slampose->inc_constraint_sm = constraint_sm; 

    //odom constraint added 
    PoseToPoseTransform *constraint_odom = new PoseToPoseTransform(new Pose2d(prev_curr_tranf), slampose, app->last_slampose, SLAM_GRAPH_EDGE_T_TYPE_ODOM_INC, cov, 1.0, ld, ad,  1);
    //constraint is added to the current node (transform is respective to - i.e. in the previous node's pov)
    slampose->inc_constraint_odom = constraint_odom; 
    
    g_mutex_lock(app->mutex);
    app->last_slampose = slampose;
    app->last_slampose_ind = slampose->node_id;
    if(app->slave_mode){
        vector<NodeScan *>::iterator it = app->slam_nodes_to_add.begin();
        app->slam_nodes_to_add.insert(it, slampose);
    }
    else{
        if (app->verbose)
            fprintf (stdout, "----------------------adding slam node\n");
        vector<NodeScan *>::iterator it = app->slam_nodes_to_add.begin();
        app->slam_nodes_to_add.insert(it, slampose);
    }
    g_mutex_unlock(app->mutex);

    //destroy the node init - after its handled
    if(app->node_init != NULL){
        slam_init_node_t_destroy(app->node_init);
        app->node_init = NULL;
    }
    
    sm_tictoc("addScanToGraph");

    return;
}

int process_laser_queue(app_t *app){
    //go through the queue 
    int to_add = 0;
    g_mutex_lock(app->mutex_laser_queue);
    if(app->lasers_to_add.size() > 0)
        to_add = 1;
    g_mutex_unlock(app->mutex_laser_queue);
    while(to_add){
        g_mutex_lock(app->mutex_laser_queue);
        fprintf(stderr, "Processing Laser in queue\n");
        laser_odom_t *l_odom = app->lasers_to_add.at(0);

        app->lasers_to_add.erase(app->lasers_to_add.begin());

        if(app->lasers_to_add.size() > 0)
            to_add =1;
        else
            to_add = 0;
        g_mutex_unlock(app->mutex_laser_queue);
        process_laser_scan(app, l_odom);        

        //free the data 
        sm_rigid_transform_2d_t_destroy(l_odom->odom);
        bot_core_planar_lidar_t_destroy(l_odom->laser);
        free(l_odom);
    }
    return 1;
}


void app_destroy(app_t *app)
{
    //TODO: there is a double free mess cuz scan matcher free's scans that are held onto elsewhere :-/
    //LEAK everything for now!!!

    if (app->mutex)
        g_mutex_free (app->mutex);

    if (app->mutex_particles)
        g_mutex_free (app->mutex_particles);

    if (app->mutex_lasers)
        g_mutex_free (app->mutex_lasers);

    // dump timing stats
    sm_tictoc(NULL);
    exit (1);

}


//free function to clean up the circular buffer data 
void
circ_free_lidar_data(void *user, void *p) {
    bot_core_planar_lidar_t *np = (bot_core_planar_lidar_t *) p; 
    bot_core_planar_lidar_t_destroy(np);
}

//process the laser scan 
void process_laser(bot_core_planar_lidar_t *flaser, app_t *app, int64_t utime, int force_supernode = 0){
    if(app->useOdom){
        sm_rigid_transform_2d_t odom;
        memset(&odom, 0, sizeof(odom));

        double rpy[3];
        
        //this covariance is ignored 
        double cov[9] = { 0 };
        cov[0] = 0.0005;
        cov[1] = 0;
        cov[3] = 0;
        cov[4] = 0.0005; //about 2 cm of std dev 
        cov[8] = 0.0002;
        
        if(!app->use_frames){
            if(app->last_deadreakon != NULL){
                //use the odom msg
                memcpy(odom.pos, app->last_deadreakon->pos, 2 * sizeof(double));
                bot_quat_to_roll_pitch_yaw(app->last_deadreakon->orientation, rpy);
            }
            else
                return;
        }
        else{ 
            int64_t frame_utime;
            //this can be fine as long as its been updated 
            int status = bot_frames_get_latest_timestamp(app->frames, "body",
                                                         "local",&frame_utime);

            if(!status || fabs(frame_utime - utime) / 1.0e6 > 10.0){
                fprintf(stderr, "Frames hasn't been updated yet - retruning %f - %f => Gap : %f\n", frame_utime/1.0e6, utime /1.0e6, fabs(frame_utime - utime) / 1.0e6);
                return;
            }

            double body_to_local[12];
            if (!bot_frames_get_trans_mat_3x4_with_utime (app->frames, "body",
                                                          "local",  flaser->utime,
                                                          body_to_local)) {
                fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
                return;        
            }
            double pose_b[3] = {.0}, pose_l[3];
            bot_vector_affine_transform_3x4_3d (body_to_local, 
                                                pose_b, 
                                                pose_l);
            odom.pos[0] = pose_l[0];
            odom.pos[1] = pose_l[1];

            BotTrans body_to_local_t;

            if (!bot_frames_get_trans_with_utime (app->frames, "body", "local", flaser->utime, 
                                                  &body_to_local_t)){
                fprintf (stderr, "frame error\n");
                return;
            }
            
            double rpy_b[3] = {0};
            double quat_b[4], quat_l[4];
            bot_roll_pitch_yaw_to_quat (rpy_b, quat_b);
            
            bot_quat_mult (quat_l, body_to_local_t.rot_quat, quat_b);
            bot_quat_to_roll_pitch_yaw (quat_l, rpy);
        }

        odom.theta = rpy[2];
        //cov was published as body frame already... rotate to global for now
        double Rcov[9];
        sm_rotateCov2D(cov, odom.theta, odom.cov);

        static int64_t utime_prev = flaser->utime;

        sm_tictoc("aligned_laser_handler");
        add_to_queue(flaser, &odom, flaser->utime, app, force_supernode);
        sm_tictoc("aligned_laser_handler");
    }
    else{
        //if we dont have odom - use scanmatching 
        smPoint * points = (smPoint *) calloc(flaser->nranges, sizeof(smPoint));
        int numValidPoints = sm_projectRangesAndDecimate(app->beam_skip, app->spatialDecimationThresh, flaser->ranges,
                                                         flaser->nranges, flaser->rad0, flaser->radstep, points, 
                                                         app->maxRange, app->validBeamAngles[0], app->validBeamAngles[1]);
        if (numValidPoints < 30) {
            fprintf(stderr, "WARNING! NOT ENOUGH VALID POINTS! numValid=%d\n", numValidPoints);
            free (points);
            return;
        }

        double sensor_to_body[12];
        if (!bot_frames_get_trans_mat_3x4 (app->frames, app->chan,
                                           "body",
                                           sensor_to_body)) {
            fprintf (stderr, "Error getting bot_frames transformation from %s to body!\n", app->chan);
            free (points);
            return;
        }

        //transform to body pose
        double pos_s[3] = {0};
        double pos_b[3] = {0};
        for(int i=0; i < numValidPoints; i++){
            pos_s[0] = points[i].x;
            pos_s[1] = points[i].y;
            bot_vector_affine_transform_3x4_3d (sensor_to_body, pos_s , pos_b);
            points[i].x = pos_b[0];
            points[i].y = pos_b[1];
        }
        
        ////////////////////////////////////////////////////////////////////
        //Actually do the matching
        ////////////////////////////////////////////////////////////////////
        //don't have a better estimate than prev, so just set prior to NULL
        ScanTransform r = app->sm_incremental->matchSuccessive(points, numValidPoints, 
                                                               SM_HOKUYO_UTM, sm_get_utime(), NULL); 

        sm_rigid_transform_2d_t odom;
        memset(&odom, 0, sizeof(odom));
        odom.utime = flaser->utime;
        odom.pos[0] = r.x;
        odom.pos[1] = r.y;
        odom.theta = r.theta;
        memcpy(odom.cov, r.sigma, 9 * sizeof(double));

        //  app->sm_incremental->drawGUI(points, numValidPoints, r, NULL,"ScanMatcher");
        
        free(points);
        sm_tictoc("laser_handler");
        add_to_queue(flaser, &odom, flaser->utime, app, force_supernode);
    }
}

static void slam_command_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                                 const char * channel __attribute__((unused)), const slam_command_t * msg,
                                 void * user  __attribute__((unused)))
{
    app_t * app = (app_t *) user;
    if(msg->command == SLAM_COMMAND_T_RUN_SM_TRAINING){
        //might need to lock this also 
        app->particle_id_to_train = msg->particle_ind;
    }
    //if we get this - set flag to stop the slam and then do a proper scanmatch (also use its own local region)
}

// Compare language_label_t by utime
bool compare_by_utime (slam_language_label_t *first, slam_language_label_t *second)
{
    if (first->utime <= second->utime)
        return true;
    else
        return false;
}

void publish_map_id(app_t *app, int64_t id){
    for (int i=0; i < app->slam_particles.size(); i++) {
        SlamParticle *particle = app->slam_particles.at(i);
        if(id == particle->graph_id){
            particle->publishOccupancyMap ();
            fprintf(stderr, "Publishing Occ-map\n");
            break;
        }
    }    
}

static void pixel_map_request_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                                      const char * channel __attribute__((unused)), 
                                      const slam_pixel_map_request_t * msg, void * user  __attribute__((unused))){
    // Currently only handle simple language
    app_t *app = (app_t *) user;
    
    //add this to the state
    g_mutex_lock(app->mutex);
    if(app->map_request != NULL){
        slam_pixel_map_request_t_destroy(app->map_request);
    }
    app->map_request = slam_pixel_map_request_t_copy(msg);
    g_mutex_unlock(app->mutex);
    
    
}

static void language_label_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                                   const char * channel __attribute__((unused)), 
                                   const slam_language_label_t * msg, void * user  __attribute__((unused)))
{
    // Currently only handle simple language
    app_t *app = (app_t *) user;
    fprintf(stderr, "\n\n++++++++ Language Message received (%s) +++++\n", msg->update);
    if(msg->constraint_type == SLAM_LANGUAGE_LABEL_T_TYPE_SIMPLE && msg->label == -1){
        fprintf(stderr, "Invalid Label ID\n");
        return;
    }

    app->got_label = 1;
    // Add the label to the list and sort by utime
    g_mutex_lock (app->mutex);
    app->language_labels_to_add.push_back (slam_language_label_t_copy (msg));
    g_mutex_unlock (app->mutex);   

    return;
}

static void slu_result_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                               const char * channel __attribute__((unused)), const slam_slu_result_list_t * msg,
                               void * user  __attribute__((unused)))
{
    fprintf (stderr, "SLU result received\n");
    app_t * app = (app_t *) user;

    //if we get this - set flag to stop the slam and then do a proper scanmatch (also use its own local region)
    g_mutex_lock(app->mutex);
    if(app->waiting_for_slu == 1){
        fprintf(stderr, "Received outstanding SLU result\n");
        //lets add it to the 
        if(app->slu_result != NULL){
            fprintf(stderr, "Error : we have an unprocessed result\n");
            slam_slu_result_list_t_destroy(app->slu_result);
        }
        app->slu_result = slam_slu_result_list_t_copy(msg);        
    }
    g_mutex_unlock(app->mutex);
}

static void language_edge_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                                  const char * channel __attribute__((unused)), const slam_language_edge_t * msg,
                                  void * user  __attribute__((unused)))
{
    fprintf (stderr, "ERROR: Should not be in language_edge_handler\n");
    app_t * app = (app_t *) user;

    //if we get this - set flag to stop the slam and then do a proper scanmatch (also use its own local region)
    g_mutex_lock(app->mutex);
    app->language_edges_to_add.push_back(slam_language_edge_t_copy(msg));
    g_mutex_unlock(app->mutex);
}

static void dirichlet_update_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), const slam_dirichlet_update_t * msg,
                                     void * user  __attribute__((unused)))
{
    app_t * app = (app_t *) user;

    g_mutex_lock(app->mutex);
    app->dirichlet_updates_to_add.push_back(slam_dirichlet_update_t_copy(msg));
    g_mutex_unlock(app->mutex);
}

static void door_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                         const char * channel __attribute__((unused)), 
                         const object_door_list_t * msg,
                         void * user  __attribute__((unused)))
{
    app_t * app = (app_t *) user;
    
    //if we get this - set flag to stop the slam and then do a proper scanmatch (also use its own local region)
    //g_mutex_lock(app->mutex);
    if(app->door_list != NULL)
        object_door_list_t_destroy(app->door_list);
    
    app->door_list = object_door_list_t_copy(msg);
}

////////////////////////////////////////////////////////////////////
//where the laser is put to the circular buffer 
////////////////////////////////////////////////////////////////////
bot_core_planar_lidar_t *get_closest_rear_laser(app_t *app, int64_t utime){
    double min_utime = 0.5;//we dont consider if the closest laser is 0.5 s apart
    int close_ind = -1;
    bot_core_planar_lidar_t *rlaser = NULL;

    //fprintf(stderr , "Rear Laser Queue Size : %d\n", bot_ptr_circular_size(app->rear_laser_circ));

    for (int i=0; i<(bot_ptr_circular_size(app->rear_laser_circ)); i++) {
        bot_core_planar_lidar_t *laser = (bot_core_planar_lidar_t *) bot_ptr_circular_index(app->rear_laser_circ, i);
        double time_diff = fabs(laser->utime - utime)  / 1.0e6;

        if(time_diff< min_utime){
            min_utime = time_diff;
            close_ind = i;
        }
    }

    if(close_ind == -1){
        fprintf(stderr, "No matching laser scan found\n");
        return NULL;
    }
  
    rlaser = (bot_core_planar_lidar_t *) bot_ptr_circular_index(app->rear_laser_circ, close_ind);
    return rlaser;
}

static void r_laser_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), const bot_core_planar_lidar_t * msg,
                            void * user  __attribute__((unused)))
{
    app_t * app = (app_t *) user;
    
    bot_ptr_circular_add (app->rear_laser_circ, bot_core_planar_lidar_t_copy(msg));

}

static void laser_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), const bot_core_planar_lidar_t * msg,
                          void * user  __attribute__((unused)))
{
    app_t * app = (app_t *) user;
    
    if(app->slave_mode){
        bot_ptr_circular_add (app->front_laser_circ,  bot_core_planar_lidar_t_copy(msg));
        //if there is an unhandled node init msg - check if this is it 

        if(app->node_init != NULL){
            double min_utime = 0.5;//we dont consider if the closest laser is 0.5 s apart
            int close_ind = -1;
            bot_core_planar_lidar_t *flaser = NULL;

            for (int i=0; i<(bot_ptr_circular_size(app->front_laser_circ)); i++) {
                bot_core_planar_lidar_t *laser = (bot_core_planar_lidar_t *) bot_ptr_circular_index(app->front_laser_circ, i);
                double time_diff = fabs(laser->utime - msg->utime)  / 1.0e6;

                if(time_diff< min_utime){
                    min_utime = time_diff;
                    close_ind = i;
                }
            }
            
            if(close_ind == -1){
                fprintf(stderr, "No matching laser scan found\n");
                return;
            }
            flaser = (bot_core_planar_lidar_t *) bot_ptr_circular_index(app->front_laser_circ, close_ind);
            
            process_laser(flaser,app, msg->utime);

            if(app->node_init != NULL){
                slam_init_node_t_destroy(app->node_init);
                app->node_init = NULL;
            }
        }
    }
    else{
        if(app->use_doorways){
            //adding laser offset
            BotTrans body_to_local;
            if (!bot_frames_get_trans_with_utime (app->frames, "body",
                                                  "local", msg->utime, 
                                                  &body_to_local)) {
                fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
                return;
            }
            
            double pos_l[2] = {body_to_local.trans_vec[0], body_to_local.trans_vec[1]};
            
            double rpy[3];
            bot_quat_to_roll_pitch_yaw(body_to_local.rot_quat, rpy);
            
            doorway_t *doorway = NULL;
            if(app->door_list)
                doorway = detect_doorway_at_pos(app->door_list, pos_l, rpy[2], -0.4, 0.8);
            
            int found_door = 0;
            if(doorway != NULL){
                found_door = 1;
            }        
            else{
            
            }

            //check the id and skip if its the same door that was added last 
            
            process_laser( (bot_core_planar_lidar_t *) msg,app, msg->utime, found_door);
            destroy_door(doorway);
        }
        else
            process_laser( (bot_core_planar_lidar_t *) msg,app, msg->utime);
    }
    
    static int64_t utime_prev = msg->utime;
}


gboolean check_updated  (gpointer key, gpointer value, gpointer user_data){
    SlamGraph *graph = (SlamGraph *) value;

    if(graph->updated == 0){
        return TRUE;
    }
    return FALSE;
}

void destroy_slam_graph(gpointer data){
    SlamGraph *graph = (SlamGraph *) data;

    delete graph;
}//addSlamNode

//basic graph that doesnt have the loop closures - used mainly for scanmatching 
int slam_on_basic_graph(app_t *app, int64_t utime){
    
    if(g_hash_table_size(app->slam_nodes) < 2){
        if(app->verbose)
            fprintf(stderr, "Not enough nodes in the graph\n");
        return -1;
    }

    int64_t start = bot_timestamp_now();
    
    //get the graph 
    int new_graph = 0;
    if(app->basic_graph == NULL){
        new_graph = 1;
        fprintf(stderr, "Graph Not found - creating new graph\n");
        app->basic_graph = new SlamGraph(utime);
    }

    SlamGraph *graph = app->basic_graph;

    map<int, SlamNode *>::iterator it;

    GHashTableIter iter;
    gpointer key, value;
    
    g_hash_table_iter_init (&iter, app->slam_nodes);

    vector<NodeScan *> new_nodes;

    //add new nodes to the graph
    //the hash table messes the order up 
    int node_added = 0;
    
    for(int i=0; i < g_hash_table_size(app->slam_nodes); i++){
        NodeScan *pose = (NodeScan *) g_hash_table_lookup(app->slam_nodes, &i);
        if(!pose)
            continue;
        
        int node_ind = pose->node_id;
        
        it = graph->slam_nodes.find(node_ind);
        
        //check if this is valid 
        if(it != graph->slam_nodes.end()){
            SlamNode *node = it->second;
            if(app->verbose)
                fprintf(stderr, "Basic Graph Node [%d] found in Slamgraph - skipping => Slam Pose Pointer %p\n", node_ind, (void *) node->slam_pose); 
            continue;
        }

        if(app->verbose)
            fprintf(stderr, "Basic Graph Node [%d] not found - adding node\n", node_ind);
        graph->addSlamNode(pose);
        node_added =1; 
        new_nodes.push_back(pose);
    }

    if(node_added == 0)
        return -1;

    if(app->verbose){
        fprintf(stderr, "\n============================================================\n");
        fprintf(stderr, "Basic Slam graph\n");
        fprintf(stderr,"Adding basic constraints\n");
    }

    int64_t end_node = bot_timestamp_now();
    if(app->verbose)
        fprintf(stderr, "Time to add node : %f\n", (end_node - start)/1.0e6);

    //add the constraint to the origin node
    if(new_graph){
        if (app->verbose)
            fprintf(stderr, "Adding constraint to the origin\n");
        //the constraint to the prior is not going to be in the edge list // need to add that before 
        int first_node_id = 0;

        SlamNode *node = graph->slam_nodes.find(first_node_id)->second;
        
        if(node == NULL){
            fprintf(stderr, "First node not found - this should not have happened\n");
        }
        else{
            if (app->verbose)
                fprintf(stderr, "First node found in Slam graph - adding constraint\n"); 
            
            //what do we use to build the local map??
            if (node->slam_pose->inc_constraint_sm->hitpct > .7) {
                PoseToPoseTransform *p2p_constraint = node->slam_pose->inc_constraint_sm; 
                graph->addOriginConstraint(node, *p2p_constraint->transform, *p2p_constraint->noise);                
            }
            else {
                PoseToPoseTransform *p2p_constraint = node->slam_pose->inc_constraint_odom; 
                graph->addOriginConstraint(node, *p2p_constraint->transform, *p2p_constraint->noise);                     
            }
        }
    }

    if(app->verbose){
        fprintf(stderr, "Done\n");
    }
    
    //add the current to previous constraints 
    for(int i=0; i < g_hash_table_size(app->slam_nodes); i++){
        NodeScan *pose = (NodeScan *) g_hash_table_lookup(app->slam_nodes, &i);
        if(pose == NULL)
            continue;
        if(pose->inc_constraint_sm == NULL){
            fprintf(stderr, "There is no incremental constraint\n");
        }

        PoseToPoseTransform *p2p_constraint = pose->inc_constraint_sm;
        Pose2d_Pose2d_Factor* constraint;

        if(p2p_constraint->node_relative_to == NULL){
            continue;
        }

        SlamNode *node1 = graph->slam_nodes.find(pose->node_id)->second;
        SlamNode *node2 = graph->slam_nodes.find(p2p_constraint->node_relative_to->node_id)->second;

        if(node1 == NULL || node2 == NULL){
            fprintf(stderr, "Error - one or more nodes not found\n");
            continue;
        }
        
        //check if the constraint exists - and if so add the constraint 
        //node id is the constraint respective to - i.e. current_node (id_1) respective to (id_2)
        //so find the constraint if it exists 
        int has_constraint = graph->hasConstraint(node1->id);//, node2->id);
        
        if(has_constraint == -1){
            fprintf(stderr, "Crap - node not added - im going to segfault now : %d %d\n", 
                    (int) node1->id, (int) node2->id);
            continue;
        }
        if(has_constraint == 1){
            if(app->verbose){
                fprintf(stderr, "Found added constraint - skipping\n");
            }
            continue;                
        }
        
        if(app->verbose){
            fprintf(stderr, "Sucess - found the constraint :) \n");
        }
        
        graph->addConstraint(node1->id, node1, node2, node1, node2, *p2p_constraint->transform, *p2p_constraint->noise, p2p_constraint->hitpct, SLAM_GRAPH_EDGE_T_TYPE_SM_INC,  SLAM_GRAPH_EDGE_T_STATUS_SUCCESS);
        
        if(app->verbose){
            fprintf(stderr, "\tAdding new constraints - %d %d\n", (int) node1->id, (int) node2->id);
        } 
    }

    int64_t end_constraint = bot_timestamp_now();

    //run optimization 
    graph->runSlam();


    int64_t end = bot_timestamp_now();
    
    if(app->verbose)
        fprintf(stderr, "Basic Time taken : %f\n", (end - start)/1.0e6);

    //go through the constraints again and do scan matching here again
    bot_lcmgl_t *lcmgl = app->lcmgl_basic;
    //draw the map 

    if(0){//app->draw_map){
        
        bot_lcmgl_color3f(lcmgl, 0.0, 1.0, 0);

        for ( it= graph->slam_nodes.begin() ; it != graph->slam_nodes.end(); it++ ){
            SlamNode *node = it->second;
            NodeScan *pose = node->slam_pose;
            if(pose == NULL)
                continue;
            Pose2d value = node->getPose();
            double pose_l[3] = {value.x(), value.y(), 0};
            bot_lcmgl_circle(lcmgl, pose_l, .3);        

            BotTrans bodyToLocal;
            bodyToLocal.trans_vec[0] = value.x();
            bodyToLocal.trans_vec[1] = value.y();
            bodyToLocal.trans_vec[2] = 0.0;
            double rpy[3] = { 0, 0, value.t() };
            bot_roll_pitch_yaw_to_quat(rpy, bodyToLocal.rot_quat);
            double pBody[3] = { 0, 0, 0 };
            double pLocal[3];
            Scan *scan = pose->scan;

            bot_lcmgl_point_size(lcmgl, 2);
            bot_lcmgl_begin(lcmgl, GL_POINTS);
        
            for (unsigned i = 0; i < scan->numPoints; i++) {        
                pBody[0] = scan->points[i].x;
                pBody[1] = scan->points[i].y;
                bot_trans_apply_vec(&bodyToLocal, pBody, pLocal);
                bot_lcmgl_vertex3f(lcmgl, pLocal[0], pLocal[1], pLocal[2]);
            }
            bot_lcmgl_end(lcmgl);
        }

        //might be worth drawing the constraints        
        bot_lcmgl_switch_buffer(lcmgl);
    }
}

int renormalize_particles(app_t *app){

    //normalize weights
    if (app->verbose)
        fprintf(stderr, "Before renormalizing\n");
    double sum_weights = 0.0;
    for(int i=0; i < app->slam_particles.size(); i++){
        SlamParticle *p = app->slam_particles.at(i);
        sum_weights += exp(p->weight);
    }
    if(sum_weights == 0){
        fprintf(stderr, "Error! Sum of weights is 0! - assigning equal weights");
        for(int i=0; i < app->slam_particles.size(); i++){
            SlamParticle *p = app->slam_particles.at(i);
            p->normalized_weight = log(1.0/(double) app->slam_particles.size());
            p->weight = p->normalized_weight;
        } 
        return 1; 
    }

    //renormalizing the weights - otherwise things can get too small and mess up 
    
    if (app->verbose)
        fprintf(stderr, "After renormalizing\n");
    for(int i=0; i < app->slam_particles.size(); i++){
        SlamParticle *p = app->slam_particles.at(i);
        p->normalized_weight = p->weight - log(sum_weights);
        p->weight = p->normalized_weight;
    } 
   
    return 1; //succeeded at renormalizing
}

int resample_particles(app_t *app, int64_t utime){


    //fprintf(stderr, MAKE_GREEN "this will show up green" RESET_COLOR "\n");
    //does this kill the diversity??

    renormalize_particles(app);
    
    //calculate N_eff
    double sum_sq_weights = 0.0;
    for(int i=0; i < app->slam_particles.size(); i++){
        SlamParticle *p = app->slam_particles.at(i);
        sum_sq_weights += pow(exp(p->normalized_weight),2);
    }

    double n_eff = 1.0 / sum_sq_weights;
    double threshold  = app->slam_particles.size() / 2.0;//app->slam_particles.size()/2.0; 
    //if n_eff value greater than critical value
    if(n_eff <= threshold){
        fprintf(stderr, "\n\n+++++++++++++Resampling+++++++++++++++++++\n");
        //create a new vector
        vector<SlamParticle *> new_slam_particles;
        //sample from particles
        double *weights = new double[app->slam_particles.size()];
        for(int i=0; i<app->slam_particles.size(); i++){
            if(i==0)
                weights[i] = exp(app->slam_particles.at(i)->normalized_weight);
            else    
                weights[i] = weights[i-1]+ exp(app->slam_particles.at(i)->normalized_weight);
        }

        for(int i=0; i<app->slam_particles.size(); i++){
            fprintf(stderr, "Particle id : %d => Cumalative weight : %f\n",  i, weights[i]);
        }
        
        int *par_ids = new int[app->slam_particles.size()];
        for(int i=0; i<app->slam_particles.size(); i++)
            par_ids[i] = 0;

        for(int i=0; i<app->slam_particles.size(); i++){
            double r = rand() / (double) RAND_MAX;
            for(int j=0; j<app->slam_particles.size(); j++){
                if(j==0){
                    if(r < weights[j])
                        par_ids[j]++;
                }
                else{
                    if(r > weights[j-1] && r < weights[j])
                        par_ids[j]++;
                }                
            }
        }

        vector<SlamParticle *> to_adjust_particles;

        for(int i=0; i<app->slam_particles.size(); i++){
            int n = par_ids[i];

            if(n ==0){
                to_adjust_particles.push_back(app->slam_particles.at(i));
                //add to a list to be adjusted 
            }
        }

        for(int i=0; i<app->slam_particles.size(); i++){
            int n = par_ids[i];
            fprintf(stderr, "New particles : %d -> %d => weight : %f\n", i, n, app->slam_particles.at(i)->weight);
            if(n >0){
                //keep the old particle 
                new_slam_particles.push_back(app->slam_particles.at(i));
                       
                if(n > 1){              
                    //create copies of the same particle and add to the list
                    for(int j=0; j<n-1; j++){
                        to_adjust_particles.at(to_adjust_particles.size()-1)->adjust(app->next_id, utime, app->slam_particles.at(i));
                        to_adjust_particles.pop_back();
                        app->next_id++;
                    }
                }
            }
        }
        
        for(int i=0; i < app->slam_particles.size(); i++){
            SlamParticle *p = app->slam_particles.at(i);
            p->weight = log(1/(double) app->slam_particles.size());
        }

        fprintf(stderr, "=========== No of samples : %d - weight : %f\n", (int) app->slam_particles.size(), 
                log(1/(double) app->slam_particles.size()));

        delete par_ids;
        delete weights;

        return 1; //did resample
    }
    return 0; //didn't resample
}

int resample_particles_copy(app_t *app, int64_t utime){


    //fprintf(stderr, MAKE_GREEN "this will show up green" RESET_COLOR "\n");
    //does this kill the diversity??

    renormalize_particles(app);
    
    //calculate N_eff
    double sum_sq_weights = 0.0;
    for(int i=0; i < app->slam_particles.size(); i++){
        SlamParticle *p = app->slam_particles.at(i);
        sum_sq_weights += pow(exp(p->normalized_weight),2);
    }

    double n_eff = 1.0 / sum_sq_weights;
    double threshold  = app->slam_particles.size() / 2.0;
    //if n_eff value greater than critical value
    if(n_eff <= threshold){
        fprintf(stderr, "\n\n+++++++++++++Resampling+++++++++++++++++++\n");
        //create a new vector
        vector<SlamParticle *> new_slam_particles;
        //sample from particles
        double *weights = new double[app->slam_particles.size()];
        for(int i=0; i<app->slam_particles.size(); i++){
            if(i==0)
                weights[i] = exp(app->slam_particles.at(i)->normalized_weight);
            else    
                weights[i] = weights[i-1]+ exp(app->slam_particles.at(i)->normalized_weight);
        }

        for(int i=0; i<app->slam_particles.size(); i++){
            fprintf(stderr, "Particle id : %d => Cumalative weight : %f\n",  i, weights[i]);
        }
        
        int *par_ids = new int[app->slam_particles.size()];
        for(int i=0; i<app->slam_particles.size(); i++)
            par_ids[i] = 0;

        for(int i=0; i<app->slam_particles.size(); i++){
            double r = rand() / (double) RAND_MAX;
            for(int j=0; j<app->slam_particles.size(); j++){
                if(j==0){
                    if(r < weights[j])
                        par_ids[j]++;
                }
                else{
                    if(r > weights[j-1] && r < weights[j])
                        par_ids[j]++;
                }                
            }
        }

        //return new particles
        for(int i=0; i<app->slam_particles.size(); i++){
            int n = par_ids[i];
            fprintf(stderr, "New particles : %d -> %d => weight : %f\n", i, n, app->slam_particles.at(i)->weight);
            if(n >0){
                //keep the old particle 
                new_slam_particles.push_back(app->slam_particles.at(i));
                       
                if(n > 1){              
                    //create copies of the same particle and add to the list
                    for(int j=0; j<n-1; j++){
                        SlamParticle *part = app->slam_particles.at(i)->copy(utime, app->next_id);
                        new_slam_particles.push_back(part);
                        app->next_id++;
                    }
                }
            }
            else{
                delete app->slam_particles.at(i);
            }
        }

        app->slam_particles = new_slam_particles;
        
        for(int i=0; i < app->slam_particles.size(); i++){
            SlamParticle *p = app->slam_particles.at(i);
            p->weight = log(1/(double) app->slam_particles.size());
        }

        fprintf(stderr, "=========== No of samples : %d - weight : %f\n", (int) app->slam_particles.size(), 
                log(1/(double) app->slam_particles.size()));

        delete par_ids;
        delete weights;

        return 1; //did resample
    }
    return 0; //didn't resample
}

int update_slam_particles(app_t *app, int64_t utime, vector<NodeScan *> new_nodes, 
                          vector<slam_language_label_t *> new_language_labels) {
    //get the graph 
    //update the slam particles by adding the new nodes and then adding edges 
    BotTrans body_to_laser;
    bot_frames_get_trans(app->frames, "body", app->chan, &body_to_laser);

    //fprintf(stderr, "\nProcessing Slam Particles\n");
    //int64_t s_utime = bot_timestamp_now();
    if(app->slam_particles.size() == 0){
        for(int i=0; i < app->no_particles; i++){
            
            SlamParticle *part = new SlamParticle(utime, app->next_id, 
                                                  app->lcm,
                                                  app->sm_loop, 
                                                  app->sm_lc_low, 
                                                  app->useLowResPrior,
                                                  app->scanmatchBeforeAdd, 
                                                  body_to_laser, 
                                                  app->local_px_map,
                                                  app->lcmgl_sm_basic, 
                                                  app->lcmgl_sm_graph, 
                                                  app->lcmgl_sm_prior, 
                                                  app->lcmgl_sm_result, 
                                                  app->lcmgl_sm_result_low, 
                                                  app->lcmgl_loop_closures,
                                                  app->useOnlyRegion, 
                                                  app->addDummyConstraints, 
                                                  app->useICP, 
                                                  app->draw_scanmatch
                                                  );
            app->next_id++;
            
            app->slam_particles.push_back(part);
        }
    }
    //int64_t e_utime = bot_timestamp_now();
    //fprintf(stderr, "Time to create SLAM particles : %f\n", (e_utime - s_utime) / 1.0e6);

    // Update the Dirichlet according to language_label_t
    fprintf(stderr, MAKE_GREEN "Num of new language labels: %d" RESET_COLOR "\n", (int) new_language_labels.size());

    double max_prob_particle = 0.0;
    
    // add language labels 
    for(int i=0; i < app->slam_particles.size(); i++){
        double m_prob = 0; 

        SlamParticle *part = app->slam_particles.at(i);
        //fprintf(stderr, "------------------------------ Particle [%d] --------------------------\n", part->graph_id);
        part->addNodesAndEdges(new_nodes, app->basic_graph, new_language_labels, app->probMode, app->find_ground_truth, &m_prob, app->ignoreLanguage);
        part->calculateObservationProbability(app->probMode, &m_prob);
        //fprintf(stderr, "--------------------------------------------------------------------\n\n");

        if(max_prob_particle < m_prob)
            max_prob_particle = m_prob;
    }

    if(max_prob_particle == 0)
        max_prob_particle = 0.1;
    
    double max_weight = -1e10;
    for(int i=0; i < app->slam_particles.size(); i++){
        SlamParticle *part = app->slam_particles.at(i);
        if(max_weight < part->weight){
            max_weight = part->weight;
            app->max_prob_particle_id = part->graph_id;
        }
    }
    

    //use this to reweight/resample
    if(app->resample){
        if(app->useCopy)
            resample_particles_copy(app, utime);
        else
            resample_particles(app, utime);
    }
    else
        renormalize_particles(app);
    
    if(app->max_prob_particle_id >= 0 && app->publish_map){
        publish_map_id(app, app->max_prob_particle_id);
    }
    
    return 1;
}

void clear_dead_edges(app_t *app){
    for(int i=0; i < app->slam_particles.size(); i++){
        SlamParticle *part = app->slam_particles.at(i);
        part->clearDummyConstraints();
    }
}

int process_for_complex_language(app_t *app, int64_t utime, vector<NodeScan *> new_nodes, 
                                 vector<slam_language_label_t *> new_language_labels, 
                                 slam_language_label_t  *complex_language) {
    //get the graph 
    //update the slam particles by adding the new nodes and then adding edges 
    BotTrans body_to_laser;
    bot_frames_get_trans(app->frames, "body", app->chan, &body_to_laser);

    fprintf(stderr, "\nProcessing Slam Particles\n");
    if(app->slam_particles.size() == 0){
        for(int i=0; i < app->no_particles; i++){
            SlamParticle *part = new SlamParticle(utime, app->next_id, 
                                                  app->lcm,
                                                  app->sm_loop, 
                                                  app->sm_lc_low, 
                                                  app->useLowResPrior,
                                                  app->scanmatchBeforeAdd, 
                                                  body_to_laser, 
                                                  app->local_px_map,
                                                  app->lcmgl_sm_basic, 
                                                  app->lcmgl_sm_graph, 
                                                  app->lcmgl_sm_prior, 
                                                  app->lcmgl_sm_result, 
                                                  app->lcmgl_sm_result_low, 
                                                  app->lcmgl_loop_closures,
                                                  app->useOnlyRegion, 
                                                  app->addDummyConstraints, 
                                                  app->useICP, 
                                                  app->draw_scanmatch
                                                  );
            app->next_id++;
            
            app->slam_particles.push_back(part);
        }
    }

    // Update the Dirichlet according to language_label_t
    fprintf(stderr, MAKE_GREEN "Num of new language labels: %d" RESET_COLOR "\n", (int) new_language_labels.size());

    double max_prob_particle = 0.0;
    
    
    // add language labels 
    for(int i=0; i < app->slam_particles.size(); i++){
        double m_prob = 0; 

        SlamParticle *part = app->slam_particles.at(i);
        //fprintf(stderr, "------------------------------ Particle [%d] --------------------------\n", part->graph_id);
        part->addNodesAndEdges(new_nodes, app->basic_graph, new_language_labels, app->probMode, app->find_ground_truth, &m_prob, app->ignoreLanguage);
        //fprintf(stderr, "--------------------------------------------------------------------\n");

        if(max_prob_particle < m_prob)
            max_prob_particle = m_prob;
    }
    //dont update the weights for this - this should be done when slu has returned 

    //create a querry message and send to SLU
    slam_graph_particle_list_t *p_msg = get_slam_particle_msg(app);
    slam_slu_querry_t *msg = (slam_slu_querry_t *) calloc(1, sizeof(slam_slu_querry_t));
    msg->utime = bot_timestamp_now();
    msg->plist = *p_msg;
    msg->utterance = strdup(complex_language->update);
    slam_slu_querry_t_publish(app->lcm, "ISAM_SLU_LANGUAGE", msg);
    //need to add a lock 
    g_mutex_lock(app->mutex);
    app->waiting_for_slu = 1;
    g_mutex_unlock(app->mutex);
    
    return 1;
}

//returns 0 if no particles or no new message
int process_graph_particles(app_t *app){
    
    //copy over the remaining nodes - before processing the particles 
    GHashTableIter iter;
    gpointer key, value;

    g_mutex_lock(app->mutex);
    
    //check if there are any requests for a map 
    if(app->map_request != NULL){
        publish_map_id(app, app->map_request->particle_id);
        slam_pixel_map_request_t_destroy(app->map_request);
        app->map_request = NULL;
    }
    
    //check for any language results we were waiting for 
    if(app->waiting_for_slu == 1){
        if(app->slu_result){
            fprintf(stderr, "Found the SLU result - processing\n");
            //process the result for the particles 
            for(int i=0; i < app->slam_particles.size(); i++){
                SlamParticle *part = app->slam_particles.at(i);
                int matching_id = -1;
                for(int j = 0; j < app->slu_result->size; j++){
                    if(app->slu_result->results[j].particle_id == part->graph_id){
                        matching_id = j;
                        break;
                    }
                }
                if(matching_id >= 0){
                    part->addComplexLanguageUpdates(app->basic_graph, &app->slu_result->results[matching_id]);
                    double m_prob = 0; 
                    part->calculateObservationProbability(app->probMode, &m_prob);
                }
                else{
                    fprintf(stderr, "Error : Have not found matching result\n");
                }
            }
            
            //should we not resample here as well???
            
            
            slam_slu_result_list_t_destroy(app->slu_result);
            app->slu_result = NULL;

            app->waiting_for_slu = 0;
        }
        else{
            fprintf(stderr, ".");
            g_mutex_unlock(app->mutex);
            return 2;
        }        
    }

    vector<NodeScan *> new_nodes;
        
    //clear the old one
    //app->language_edges_to_add.clear();
    //app->dirichlet_updates_to_add.clear();
    
    int added = get_new_slam_nodes(app, &new_nodes);//update_slam_nodes(app);

    if (added > 0){
        fprintf(stderr, "Processing Slam poses - To Add : %d\n", added);
        if(app->verbose)
            fprintf (stdout, "--------- added = %d\n", added);
    }
    vector<slam_language_label_t *> new_language_labels;
    vector<slam_language_label_t *>::iterator itr;

    //should there be only one complex language that we handle now?? - or multiple??
    slam_language_label_t *matched_complex_language = NULL;

    if(app->language_labels_to_add.size() > 0){
        //fprintf(stderr, "======== Outstanding Language Labels : %d =======\n", 
        //app->language_labels_to_add.size());
    }

    for (itr = app->language_labels_to_add.begin(); itr != app->language_labels_to_add.end();) {
        slam_language_label_t *label = (slam_language_label_t *) *itr;    

        int remove = 0;

        if(label->constraint_type == SLAM_LANGUAGE_LABEL_T_TYPE_COMPLEX){
            //copy over the complex 
            if(matched_complex_language != NULL){
                continue;
            }
        }
       
        for (int j=0; j < new_nodes.size(); j++) {
            NodeScan *node_scan = new_nodes.at(j);
            fprintf (stdout, "Checking new node with delta t = %.2f\n",
                     ((double)(node_scan->utime - label->utime))/1E6);
            if (node_scan->is_supernode && (node_scan->utime >= label->utime)) {
                if(label->constraint_type == SLAM_LANGUAGE_LABEL_T_TYPE_SIMPLE){
                    new_language_labels.push_back (label);                    
                }
                else{
                    fprintf(stderr, "+++++ Adding complex language +++++\n");
                    matched_complex_language = label;
                }
                remove = 1;
                fprintf (stdout, "Found new node with delta t = %.2f\n",
                         ((double)(node_scan->utime - label->utime))/1E6);
                break;
            }
        }
        if (remove == 1)
            app->language_labels_to_add.erase (itr);
        else
            ++itr;
    }

    g_mutex_unlock(app->mutex);
    
    if(app->particle_id_to_train >=0){
        //run the scanmatch routine on the correct slam particle
        for(int i=0; i < app->slam_particles.size(); i++){
            SlamParticle *part = app->slam_particles.at(i);
            if(part->graph_id == app->particle_id_to_train){
                part->runScanMatchTraining();
            }
            
        }
        app->particle_id_to_train = -1;        
    }
    
    if(added == 0 && new_language_labels.size() == 0 && matched_complex_language == NULL) {
        //no new nodes - not processing
        return 0; 
    }
    
    int64_t now = bot_timestamp_now();
    app->last_node_sent_utime = now;
    if(app->verbose)
        fprintf(stderr, "----------- First node set ---------- \n");
    slam_on_basic_graph(app, now);
    
    // if we have any matched complex language - process it differently 
    // This will add nodes, process the simple language and distance based edges and then 
    // query SLU for the complex language 
    // After the SLU result - we will update the graph with the new langauge labels 
    // then propose new edges - and then calculate the measurements
    if(matched_complex_language != NULL){
        fprintf(stderr, "++++++++ Have complex language - need to query SLU - sending message +++++\n");
        process_for_complex_language(app, app->last_node_sent_utime, new_nodes, 
                                     new_language_labels, matched_complex_language);
        slam_language_label_t_destroy(matched_complex_language);
        app->waiting_for_slu = 1;
    }

    else{  
        fprintf(stderr, "Running SLAM Particle update\n");
        
        update_slam_particles(app, app->last_node_sent_utime, new_nodes, 
                              new_language_labels); //new_language_edges, new_dirichlet_updates);
        
        
        //for drawing the loop closures 
        if(added == 2)
            bot_lcmgl_switch_buffer(app->lcmgl_loop_closures);
        
        publish_slam_particles(app);
        //calculate the transforms ??
        publish_slam_transforms(app);
        
        if(app->keepDeadEdges ==0){
            clear_dead_edges(app);
        }
        
        // publish occupancy grid maps
        if (app->publish_occupancy_maps) {
            double dt = (now - app->publish_occupancy_maps_last_utime)/1000000.0;
            if (dt > 10.0) {
                publish_occupancy_maps (app);
                app->publish_occupancy_maps_last_utime = now;
            }
        }           
    }

    fprintf(stderr, "Done\n");

    return 1;
}

static void *laser_process_thread(void *user)
{
    app_t * app = (app_t *) user;

    printf("isam-particles: laser_process_thread()\n");
    int status = 0;
    while(1){
        //sleep a bit if we have no new graph messages
        status = process_laser_queue(app);
        if(status==0)
            usleep(500);
    }
}

static void *scanmatch_thread(void *user)
{
    app_t * app = (app_t *) user;

    printf("isam-particles: loop_closure_thread()\n");
    int status = 0;
    while(1){
        status = process_graph_particles(app);
        //sleep a bit if we have no new graph messages
        if(status==0)
            usleep(5000);

        if(status==2)
            usleep(50000);
    }
}

static void usage(const char *name)
{
    fprintf(stderr, "usage: %s [options]\n"
            "\n"
            "  -h, --help                      Shows this help text and exits\n"
            "  -a,  --aligned                  Subscribe to aligned_laser (ROBOT_LASER) msgs\n"
            "  -c, --chan <LCM CHANNEL>        Input lcm channel default:\"LASER\" or \"ROBOT_LASER\"\n"
            "  -s, --scanmatch                 Run Incremental scan matcher every time a node gets added to map\n"
            "  -d, --draw                      Show window with scan matches \n"
            "  -p, --publish_pose              publish POSE messages\n"
            "  -v, --verbose                   Be verbose\n"
            "  -m, --mode  \"HOKUYO_UTM\"|\"SICK\" configures low-level options.\n"
            "\n"
            "Low-level laser options:\n"
            "  -A, --mask <min,max>            Mask min max angles in (radians)\n"
            "  -B, --beamskip <n>              Skip every n beams \n"
            "  -D, --decimation <value>        Spatial decimation threshold (meters?)\n"
            "  -F                              Use frames to align laser scans\n"
            "  -R                              Use regions\n"
            "  -S                              Use Slave mode - pose additions will be triggered by external source\n"
            "  -M, --range <range>             Maximum range (meters)\n", name);
}

int main(int argc, char *argv[])
{
    setlinebuf(stdout);

    app_t *app = (app_t *) calloc(1, sizeof(app_t));
    app->prev_odom = (sm_rigid_transform_2d_t *) calloc(1, sizeof(sm_rigid_transform_2d_t)); //set initial prev_odom to zero...
    app->prev_sm_odom = (sm_rigid_transform_2d_t *) calloc(1, sizeof(sm_rigid_transform_2d_t)); //set initial prev_odom to zero...

    app->prev_added_odom = (sm_rigid_transform_2d_t *) calloc(1, sizeof(sm_rigid_transform_2d_t)); //set initial prev_odom to zero...

    app->prev_super_odom = (sm_rigid_transform_2d_t *) calloc(1, sizeof(sm_rigid_transform_2d_t)); //set initial prev_odom to zero...
    app->prev_door_odom = (sm_rigid_transform_2d_t *) calloc(1, sizeof(sm_rigid_transform_2d_t)); 
    
    memset(app->prev_odom, 0, sizeof(sm_rigid_transform_2d_t));
    memset(app->prev_sm_odom, 0, sizeof(sm_rigid_transform_2d_t));
    app->particle_id_to_train = -1;
    int draw_scanmatch = 0;
    bool alignLaser = false;
    app->chan = NULL;
    app->rchan = NULL;
    app->useICP = true;
    app->probMode = 1;
    app->verbose = 0;
    app->publish_map = 0;
    app->map_request = NULL;
    app->noScanTransformCheck = 0;
    app->useLowResPrior = true;
    app->door_list = NULL;
    //making these default
    app->scanmatchBeforeAdd = 1;
    app->use_frames = 1;
    app->odometryConfidence = LINEAR_DIST_TO_ADD;
    alignLaser = true;
    app->useOdom = 1;
    app->slave_mode = 0;
    app->no_particles = 2;
    app->slu_result = NULL;
    // set to default values
    app->validBeamAngles[0] = -100;
    app->validBeamAngles[1] = +100;
    app->beam_skip = 3;
    app->spatialDecimationThresh = .2;
    app->maxRange = 29.7; //20.0
    app->maxUsableRange = 8;
    app->find_ground_truth = 0;
    app->useCopy = 0;

    app->sm_acceptance_threshold = 0.8;
    
    app->last_slampose_ind = -1;
    app->last_slampose_for_process_ind = -1;
    app->last_scan = NULL;

    app->node_init = NULL;
    app->basic_graph = NULL;
    app->useOnlyRegion = true;
    app->addDummyConstraints = true;
    app->resample = 1;
    app->use_doorways = 0;
    app->keepDeadEdges = 0;
    
    /* LCM */
    app->lcm = lcm_create(NULL);
    if (!app->lcm) {
        fprintf(stderr, "ERROR: lcm_create() failed\n");
        return 1;
    }   
   
    const char *optstring = "t:hruc:dafvmM:P:wLboNODM:pn:s::ISRgC";
    char c;
    struct option long_opts[] = { { "help", no_argument, 0, 'h' },
                                  {  "copy", no_argument, 0, 'C' },
                                  {  "icp", no_argument, 0, 'I' },
                                  { "chan", required_argument, 0, 'c' },
                                  { "no_language", required_argument, 0, 'N' },
                                  { "draw_scanmatch", no_argument, 0,'S' },
                                  { "use_doorways", no_argument, 0,'d' },
                                  { "use_scanmatch_cov", no_argument, 0, 'u'},
                                  { "no_low_res", no_argument, 0, 'L'},
                                  { "bad_mode", no_argument, 0,'b' },
                                  { "draw_map", no_argument, 0, 'm' },
                                  { "publish_occupancy_maps", no_argument, 0, 'O' },
                                  { "prob_mode", required_argument, 0, 'P' },
                                  { "range", required_argument, 0, 'M' },
                                  { "no_resample", required_argument, 0,'R' },
                                  //{ "slave_mode", no_argument, 0,'S' },
                                  { "strict_scanmatch", no_argument, 0,'T' },
                                  { "dummy_constraints", no_argument, 0, 'D' },    
                                  //{ "countour", required_argument, 0, 'C' },  //draw contour map
                                  { "publish_gridmap", no_argument, 0, 'p' },
                                  { "no_particles", required_argument,   0, 'n' },
                                  { "use_odom", required_argument,   0, 'o' },
                                  { "alternate_scanmatch", no_argument, 0, 'a' },
                                  { "scanmatch", optional_argument, 0, 's' },
                                  { "wide_scan", no_argument, 0, 'w' },
                                  { "use_rear_laser", no_argument, 0, 'r' },
                                  { "verbose", no_argument, 0,'v' },
                                  { "scanmatch_threashold", required_argument, 0,'t' },
                                  { "ground_truth", no_argument, 0,'g' },
                                  { 0, 0, 0, 0 } };

    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
        switch (c) {
        case 'N':
            app->ignoreLanguage = 1;
            break;
        case 'r':
            app->rchan = strdup("SKIRT_REAR");
            break;
        case 'd':
            app->use_doorways = 1;
            break;
        case 'I':
            app->useICP = 1;
            break;
        case 'c':
            free(app->chan);
            app->chan = strdup(optarg);
            fprintf(stderr,"Main Laser Channel : %s\n", app->chan);
            break;
        case 'C':
            app->useCopy = 1;
        case 'u':
            app->useSMCov = 1;
            break;
        case 'b':
            app->bad_mode = 1;
            break;
        case 'n':
            app->no_particles = atoi(optarg);
        case 't':
            app->mode = 1;
            break;
        case 'o':
            app->scanmatchBeforeAdd = 0;
            break;
        case 'R':
            app->resample = 0;
            break;
        case 'L':
            app->useLowResPrior = false;
            break;
        case 'S':
            app->draw_scanmatch = 1;
            break;
        case 'O':
            app->publish_occupancy_maps = 1;
            break;
        case 'w':
            app->useOnlyRegion = false;
            break;
        case 'm':
            app->sm_acceptance_threshold = atof(optarg);
            break;
        case 'v':
            app->verbose = 1;
            break;
        case 'p':
            app->publish_map = 1;
            break;
        case 's':
            app->scanmatchBeforeAdd = 1;
            if (optarg != NULL) {
                app->odometryConfidence = atof(optarg);
            }
            break;
        case 'M':
            app->maxRange = strtod(optarg, 0);
            break;
        case 'P':
            app->probMode = atoi(optarg);
            break;
        case 'D':
            app->keepDeadEdges = 1;
            //app->addDummyConstraints = true;
            break;
        case 'T':
            app->strictScanmatch = 1;
            break;
            /*case 'S':
              app->slave_mode = 1;
              break;*/
       
        case 'a':
            app->alternateScanmatch = 1;
            break;
        case 'g':
            app->find_ground_truth = 1;
            app->no_particles = 1;
            break;
        case 'h':
        default:
            usage(argv[0]);
            return 1;
        }
    }

    fprintf(stderr, "Setting strict Scanmatch\n");
    app->strictScanmatch = 1;
    
    app->param = bot_param_new_from_server(app->lcm, 1);
    app->max_prob_particle_id = -1;
    app->frames = bot_frames_get_global (app->lcm, app->param);
    app->lcmgl_basic = bot_lcmgl_init(app->lcm, "slam-particle-nodes_basic");
    app->lcmgl_frame_test = bot_lcmgl_init(app->lcm, "slam-particle-farmes_test");

    app->lcmgl_debug = bot_lcmgl_init(app->lcm, "slam-particle-nodes_debug");
    //for the basic map - transfomed 
    app->lcmgl_sm_basic = bot_lcmgl_init(app->lcm, "slam-particle-nodes_sm_basic");
    //for the actual graph - that is being scanmatched for 
    app->lcmgl_sm_graph = bot_lcmgl_init(app->lcm, "slam-particle-nodes_sm_graph");
    //original scan position 
    app->lcmgl_sm_prior = bot_lcmgl_init(app->lcm, "slam-particle-nodes_sm_prior");
    //scanmatched result
    app->lcmgl_sm_result = bot_lcmgl_init(app->lcm, "slam-particle-nodes_sm_result");
    app->lcmgl_sm_result_low = bot_lcmgl_init(app->lcm, "slam-particle-nodes_sm_result_low");

    app->lcmgl_particles = bot_lcmgl_init(app->lcm, "slam-particle-nodes_particle");
    
    app->lcmgl_covariance = bot_lcmgl_init(app->lcm, "slam-particle-nodes_covariance");

    app->lcmgl_loop_closures = bot_lcmgl_init(app->lcm, "slam-particle-nodes_loop_closures");
    app->node_count = 0;
    app->last_slampose = NULL;

    if (app->chan == NULL) {
        if (alignLaser)
            app->chan = strdup("SKIRT_FRONT");
        else
            app->chan = strdup("LASER");
    }

    if (app->verbose) {
        if (alignLaser)
            printf("INFO: Listening for robot_laser msgs on %s\n", app->chan);
        else
            printf("INFO: Listening for laser msgs on %s\n", app->chan);

        printf("INFO: publish_map:%d\n", app->publish_map);
        printf("INFO: Max Range:%lf\n", app->maxRange);
        printf("INFO: SpatialDecimationThresh:%lf\n", app->spatialDecimationThresh);
        printf("INFO: Beam Skip:%d\n", app->beam_skip);
        printf("INFO: validRange:%f,%f\n", app->validBeamAngles[0], app->validBeamAngles[1]);
    }

    //hardcoded loop closing scan matcher params
    double metersPerPixel_lc = 0.02; //translational resolution for the brute force search
    double thetaResolution_lc = 0.01; //angular step size for the brute force search
   
    int useGradientAscentPolish_lc = 1; //use gradient descent to improve estimate after brute force search

    //Sachi - try this with resulution 5 
    int useMultires_lc = 5; // low resolution will have resolution metersPerPixel * 2^useMultiRes
    bool useThreads_lc = true;

    //hardcoded scan matcher params
    double metersPerPixel = 0.02; //translational resolution for the brute force search
    double thetaResolution = 0.02; //angular step size for the brute force search
    
    if(app->bad_mode){
        metersPerPixel_lc = 0.1;
        thetaResolution_lc = 0.05;
        metersPerPixel = 0.1;
        thetaResolution = 0.1;
    }

    sm_incremental_matching_modes_t matchingMode = SM_GRID_COORD; //use gradient descent to improve estimate after brute force search
    int useMultires = 3; // low resolution will have resolution metersPerPixel * 2^useMultiRes

    double initialSearchRangeXY = .15; //nominal range that will be searched over
    double initialSearchRangeTheta = .1;

    //SHOULD be set greater than the initialSearchRange
    double maxSearchRangeXY = .3; //if a good match isn't found I'll expand and try again up to this size...
    double maxSearchRangeTheta = .2; //if a good match isn't found I'll expand and try again up to this size...

    int maxNumScans = 30; //keep around this many scans in the history
    double addScanHitThresh = .90; //add a new scan to the map when the number of "hits" drops below this

    int useThreads = 1;

    //create the actual scan matcher object
    /*app->regionslam = new RegionSlam(app->lcm, metersPerPixel_lc, 
      thetaResolution_lc, useGradientAscentPolish_lc, 
      useMultires_lc,
      draw_scanmatch , useThreads_lc, app->frames);*/
  

    //create the incremental scan matcher object
    app->sm_incremental = new ScanMatcher(metersPerPixel, thetaResolution, useMultires, useThreads, false);
    //set the scan matcher to start at pi/2...
    ScanTransform startPose;
    memset(&startPose, 0, sizeof(startPose));
    app->sm_incremental->initSuccessiveMatchingParams(maxNumScans, initialSearchRangeXY, maxSearchRangeXY,
                                                      initialSearchRangeTheta, maxSearchRangeTheta, matchingMode, addScanHitThresh, false, .3, &startPose);

    
    app->sm_loop = new ScanMatcher(metersPerPixel_lc, thetaResolution_lc, useMultires_lc, useThreads_lc, false);

    //create the SLAM scan matcher object
    app->sm = new ScanMatcher(metersPerPixel, thetaResolution, useMultires, useThreads, false);

    //set the scan matcher to start at pi/2...
    app->sm->initSuccessiveMatchingParams(maxNumScans, LINEAR_DIST_TO_ADD, LINEAR_DIST_TO_ADD, ANGULAR_DIST_TO_ADD,
                                          ANGULAR_DIST_TO_ADD, matchingMode, addScanHitThresh, false, .3, &startPose);

    //set the scan matcher to start at pi/2... cuz it looks better
    memset(&app->sm->currentPose, 0, sizeof(app->sm->currentPose));
    app->sm->currentPose.theta = M_PI / 2;
    app->sm->prevPose = app->sm->currentPose;
    app->r_laser_msg = NULL;
    app->r_utime = 0;

    //hardcoded loop closing scan matcher params
    double metersPerPixel_low_res = 0.4; //translational resolution for the brute force search
    double thetaResolution_low_res = 0.2; //angular step size for the brute force search
   
    int useGradientAscentPolish_low_res = 0; //use gradient descent to improve estimate after brute force search

    //Sachi - try this with resulution 5 
    int useMultires_low_res = 3; // low resolution will have resolution metersPerPixel * 2^useMultiRes
    bool useThreads_low_res = true;//false;

    //create the SLAM scan matcher object
    app->sm_lc_low = new ScanMatcher(metersPerPixel_low_res, thetaResolution_low_res, useMultires_low_res, useThreads_low_res, false);

    app->slam_nodes = g_hash_table_new(g_int_hash, g_int_equal);

    app->front_laser_circ = bot_ptr_circular_new (LASER_DATA_CIRC_SIZE,
                                                  circ_free_lidar_data, app);
    
    app->rear_laser_circ = bot_ptr_circular_new (LASER_DATA_CIRC_SIZE,
                                                 circ_free_lidar_data, app);

    // Allocate the pixel map for the local occupancy grid
    double xy0[2] = {-15.0, -15.0};
    double xy1[2] = {15.0, 15.0};
    double res = 0.1;

    app->local_px_map = new FloatPixelMap(xy0, xy1, res, 0, true, true);

    app->next_id = 0;

    if (app->sm->isUsingIPP())
        fprintf(stderr, "Using IPP\n");
    else
        fprintf(stderr, "NOT using IPP\n");

    bot_core_planar_lidar_t_subscribe(app->lcm, app->chan, laser_handler, app);

    if(app->rchan != NULL){
        bot_core_planar_lidar_t_subscribe(app->lcm, app->rchan, r_laser_handler, app);
    }

    slam_command_t_subscribe(app->lcm, "SLAM_COMMAND", slam_command_handler, app);
    
    if(app->slave_mode){
        fprintf(stderr,"Slave mode : Subscribing to external trigger\n");
    }
    
    slam_language_label_t_subscribe (app->lcm, "LANGUAGE_LABEL", language_label_handler, app);

    if(app->ignoreLanguage == 0)
        fprintf(stderr, "Ignoring language\n");
    slam_pixel_map_request_t_subscribe (app->lcm, "PIXEL_MAP_REQUEST", pixel_map_request_handler, app);
    
    slam_slu_result_list_t_subscribe(app->lcm, "SLU_LANG_RESULT", slu_result_handler, app);
    
    if(app->use_doorways)
        object_door_list_t_subscribe(app->lcm, "DOOR_LIST", door_handler, app);
    
    app->mutex = g_mutex_new();
    app->mutex_language = g_mutex_new();
    app->mutex_particles = g_mutex_new();
    app->mutex_lasers = g_mutex_new();
    app->mutex_laser_queue = g_mutex_new();

    //reset the viewer 
    slam_status_t r_msg;
    r_msg.utime = bot_timestamp_now();
    r_msg.status = SLAM_STATUS_T_RESET;
    slam_status_t_publish(app->lcm, "SLAM_STATUS", &r_msg);

    pthread_create(&app->scanmatch_thread , NULL, scanmatch_thread, app);

    pthread_create(&app->laser_process_thread , NULL, laser_process_thread, app);

    struct sigaction new_action;
    new_action.sa_sigaction = sig_action;
    sigemptyset(&new_action.sa_mask);
    new_action.sa_flags = 0;

    sigaction(SIGINT, &new_action, NULL);
    sigaction(SIGTERM, &new_action, NULL);
    sigaction(SIGKILL, &new_action, NULL);
    sigaction(SIGHUP, &new_action, NULL);

    
    /* sit and wait for messages */
    while (still_groovy)
        lcm_handle(app->lcm);

    app_destroy(app);

    return 0;
}
