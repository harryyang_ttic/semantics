#ifndef SLAMPARTICLE_H_
#define SLAMPARTICLE_H_

#include <mrpt_sm_utils/mrpt_scanmatcher.hpp>
#include "SlamGraph.hpp"
#include <scanmatch/ScanMatcher.hpp>
#include <lcm/lcm.h>
#include <bot_core/bot_core.h>
#include <bot_lcmgl_client/lcmgl.h>
#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif 

#include "Utils.hpp"
#include <laser_features/laser_feature.h>
#include <lcmtypes/slam_language_edge_t.h>
#include <lcmtypes/slam_language_label_t.h>
#include <lcmtypes/slam_graph_edge_t.h>
#include <lcmtypes/slam_slu_result_list_t.h>
#include <lcmtypes/slam_pixel_map_request_t.h>

#include <gsl/gsl_blas.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_sf_bessel.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_randist.h>
#include <math.h>
#include <occ_map/PixelMap.hpp>
#include <laser_utils/LaserSim2D.hpp>

#define NO_OF_LANG_EDGES 4

using namespace occ_map;
using namespace laser_util;
using namespace std;
using namespace scanmatch;

class SlamParticle {
public:
    SlamParticle(int64_t _utime, int _graph_id, 
                 lcm_t *lcm, 
                 ScanMatcher *_sm, 
                 ScanMatcher *_sm_low_res, 
                 bool _useLowResPrior, 
                 bool _scanmatchBeforeAdd,
                 BotTrans body_to_laser, 
                 FloatPixelMap *local_px_map,
                 bot_lcmgl_t *_lcmgl_sm_basic,
                 bot_lcmgl_t *_lcmgl_sm_graph, 
                 bot_lcmgl_t *_lcmgl_sm_prior,
                 bot_lcmgl_t *_lcmgl_sm_result, 
                 bot_lcmgl_t *_lcmgl_sm_result_low, 
                 bot_lcmgl_t *_lcmgl_loop_closures, 
                 bool _scanmatchOnlyRegion, 
                 bool _addDummyConstraints, 
                 bool _useICP, 
                 bool _drawScanmatch);
    
    ~SlamParticle();

    BotTrans getBotTransFromPose(Pose2d pose);

    Pose2d getPoseFromBotTrans(BotTrans tf);
    
    int getCorrespondance(BotTrans node_match_to_target, SlamNode *target, SlamNode *match);
    
    SlamParticle *copy(int64_t utime, int new_id);

    void adjust(int id, int64_t utime, SlamParticle *part);
    
    ScanTransform doScanMatchLanguageICP(SlamGraph *basic_graph, int64_t node_id_1, int64_t node_id_2, 
                                         Pose2d *transf, Noise *cov_hc, int *accepted, 
                                         int64_t *used_ind_1, int64_t *used_ind_2, 
                                         double *hit_percentage, int use_prior);

    ScanTransform doScanMatchLanguageICPOrig(SlamGraph *basic_graph, int64_t node_id_1, int64_t node_id_2, 
                                         Pose2d *transf, Noise *cov_hc, int *accepted, 
                                         int64_t *used_ind_1, int64_t *used_ind_2, 
                                         double *hit_percentage, int use_prior);

    //does scanmatch between two nodes
    void drawBasicMapInParticleFrame(SlamGraph *basic_graph, int valid_supernode1, int valid_supernode2, int start_ind, int end_ind, BotTrans base_frame_to_particle_frame);
    //draw the prior
    void drawScan(bot_lcmgl_t *lcmgl, NodeScan *scan, ScanTransform T, double color[3]);

    Pose2d getLastPose();
    BotTrans getBotTransForCurrentNode();
    
    void drawPoints(bot_lcmgl_t *lcmgl, pointlist2d_t *list, ScanTransform *T, double color[3], int transform); 

    void addScansToScanMatcher(ScanMatcher *sm, SlamGraph *basic_graph, int start_ind, 
                                             int end_ind, int region_supernode_id, 
                                             int match_supernode_id, 
                                             BotTrans base_frame_to_particle_frame);
    
    int getClosestNodes(int sn_id_1, int sn_id_2, int *cl_ind_1, int *cl_ind_2, double *dist);//SlamNode *cl_node1, SlamNode *cl_node2);

    ScanTransform doScanMatch(SlamGraph *basic_graph, int64_t node_id_1, int64_t node_id_2,
                              Pose2d *transf, 
                              Noise *cov_hc, int *accepted, int64_t *used_ind_1, int64_t *used_ind_2, double *hit_percentage);
    
    int addLanguageEdges(SlamGraph *basic_graph, map<int, SlamNode *> lang_updated_nodes);
    void addComplexLanguageUpdates(SlamGraph *basic_graph, slam_slu_result_t *slu_msg); 

    //adds nodes and creates edges 
    void addNodesAndEdges(vector<NodeScan *> new_nodes, SlamGraph *basic_graph, int probMode);

    //adds nodes and creates edges 
    void addNodesAndEdges(vector<NodeScan *> new_nodes, SlamGraph *basic_graph, vector<slam_language_label_t *> new_laguage_labels, 
                          int probMode, int find_ground_truth, double *maximum_probability, int ignoreLanguageForEdges);
    //void addNodesAndEdges(vector<NodeScan *> new_nodes, SlamGraph *basic_graph, vector<slam_language_edge_t *> new_laguage_edges, int probMode, int find_ground_truth, double *maximum_probability);

    void doScanMatchCheck(SlamGraph *basic_graph, int64_t node_id_1, int64_t node_id_2, 
                          int *accepted, int64_t *used_ind_1, int64_t *used_ind_2);
    
    BotTrans getTransform(SlamNode *basic_nd, SlamNode *graphNode);
    BotTrans getTransformPrior(SlamNode *basic_nd, SlamNode *graph_nd_actual, SlamNode *graph_nd_proposed);
    void runScanMatchTraining();

    void updateWeight(int probMode, vector<SlamNode *> nodes_to_process);
    
    double compareScans(const bot_core_planar_lidar_t *sim_laser, bot_core_planar_lidar_t *actual_laser);

    double pHit (double z_star, double z);
    double pShort (double z_star, double z);
    double scanLikelihood(const bot_core_planar_lidar_t *sim_laser, bot_core_planar_lidar_t *actual_laser);
    
    void processMeasurementLikelihood(int probMode);

    void addLoopClosures(vector<SlamNode *> nodes_to_process, SlamGraph *basic_graph, int find_ground_truth);
    
    double getProbOfMeasurement(SlamNode *last_nd);
    double getProbOfMeasurementSM(SlamNode *last_nd);
    double getProbOfMeasurementLikelihood(SlamNode *last_nd, double *max_prob);
    double getProbOfMeasurementLikelihoodFixed(SlamNode *last_nd, double *maximum_probability);
    //converts the SlamGraph to a slam_graph_particle_t msg
    void getLCMMessageFromGraph(slam_graph_particle_t *particle);
    void publishOccupancyMap ();
    MatrixXd getObservationCovarianceInParticleFrame(double pBody[3], SlamNode *nd);
    double getObservationProbability(MatrixXd cov_match_in_particle_frame, MatrixXd cov_querry_in_particle_frame, 
                                     double pMatch[2], double pQuery[2]);

    double getObservationProbabilityOld(MatrixXd cov_match_in_particle_frame, MatrixXd cov_querry_in_particle_frame, 
                                     double pMatch[2], double pQuery[2]);

    double getObservationProbabilityFixed(double pMatch[2], double pQuery[2]);

    void calculateObservationProbability(int probMode,  double *maximum_probability);

    double getProbOfMeasurementLikelihoodCount(SlamNode *last_nd, double *maximum_probability);

    double getProbOfMeasurementLikelihoodComplex(SlamNode *last_nd, double *maximum_probability);

    vector <slam_language_edge_t *> proposeLanguageEdges (map<nodePairKey, double> edge_similarities, int no_edges);

    MatrixXd getLaserCov(double pBody[3]);
    
    double getObservationProbabilityComplex(MatrixXd qPoseMPoseCov, MatrixXd pQuerryCov, MatrixXd pMatchCov, 
                                     double qLaserPos[2], double mLaserPos[2], 
                                     Pose2d qPose, Pose2d mPose);
    

    double calculateSimilarity (LabelDistribution *ldist1, LabelDistribution *ldist2);
    int bleedTemporal (SlamNode *node);
    void printEdges();

    void clearDummyConstraints();

    pointlist2d_t *getRegionPointsFromTransformSuperNode(SlamGraph *basic_graph, int start_ind, int end_ind, int region_supernode_id, int use_only_region);

    Scan *getRegionFromTransform(SlamGraph *basic_graph, int start_ind, int end_ind, int region_supernode_id, BotTrans base_frame_to_particle_frame, int use_only_region);

    pointlist2d_t * getRegionPointsFromTransform(SlamGraph *basic_graph, int start_ind, int end_ind, int region_supernode_id, BotTrans base_frame_to_particle_frame, int use_only_region);

    lcm_t *lcm;
    int64_t utime; //time the graph was created 
    int graph_id; //graph id
    SlamGraph *graph; //slam graph 
    ScanMatcher *sm; //scanmatch object for loopclosing
    ScanMatcher *sm_low_res;
    double weight; 
    double normalized_weight; 
    bool verbose;
    bool scanmatchOnlyRegion; //scanmatch only the nodes that belog to particular supernode
    bool scanmatchBeforeAdd; 
    bool noScanTransformCheck; 
    bool addDummyConstraints;
    bool draw_scanmatch;
    bool doWideMatchOnFail;
    double sm_acceptance_threshold;
    double lang_sm_acceptance_threshold;
    double inc_sm_acceptance_threshold;
    bool strictScanmatch;
    bool useSMCov;
    bool useLowResPrior;
    double max_dist_for_lc; 
    bot_lcmgl_t *lcmgl_sm_basic;
    bot_lcmgl_t *lcmgl_sm_graph;
    bot_lcmgl_t *lcmgl_sm_prior;
    bot_lcmgl_t *lcmgl_sm_result;
    bot_lcmgl_t *lcmgl_sm_result_low;
    bot_lcmgl_t *lcmgl_loop_closures;
    BotTrans body_to_laser;
    BotTrans laser_to_body;
    FloatPixelMap *local_px_map;
    bool useICP; 
    int count_for_batch_optimize;
    gsl_rng *rng;

    map <nodePairKey, PoseToPoseTransform *> languageSMCache;
    int language_event_count;
    vector<SlamNode *> unprocessed_slam_nodes; 
    //map <nodePairKey, PoseToPoseTransform *> langConst;
};

#endif
