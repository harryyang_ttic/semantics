/*
 * SlamPose.cpp
 *
 *  Created on: Oct 22, 2009
 *      Author: abachrac
 */

#include "AnchorPose.h"

AnchorPose::AnchorPose(int64_t _utime, Slam *slam, int _node_id, int _region_no) :
  utime(_utime), node_id(_node_id), region_id(_region_no)
{
  anchor2d_node = new Anchor2d_Node(slam);
  
  constraint_ids.push_back(node_id - 1); 
}

AnchorPose::~AnchorPose()
{
  delete anchor2d_node;
  constraint_ids.clear();
}
