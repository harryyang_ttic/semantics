/*
 * SlamPose.h
 *
 *  Created on: Oct 22, 2009
 *      Author: abachrac
 */

#ifndef ANCHORPOSE_H_
#define ANCHORPOSE_H_
#include <vector>
#include <isam/isam.h>
#include "isam/Anchor.h"

using namespace std;
using namespace isam;

class AnchorPose {
public:
  AnchorPose(int64_t _utime, Slam *slam, int _node_id, int _region_no);

  virtual ~AnchorPose();

  int64_t utime;
  int node_id;

  int region_id;
  
  Anchor2d_Node *anchor2d_node;
  vector<int> constraint_ids;

};

#endif /* ANCHORPOSE_H_ */
