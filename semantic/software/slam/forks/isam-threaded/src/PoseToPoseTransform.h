#ifndef POSE_TO_POSE_H_
#define POSE_TO_POSE_H_

#include <vector>
#include <isam/isam.h>

using namespace isam;

class PoseToPoseTransform {
public:

    Pose2d transform;
    NodeScan *node_1;
    NodeScan *node_2;
};

#endif 
