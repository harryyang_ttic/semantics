#include "SlamGraph.hpp"

SlamNode::SlamNode(NodeScan *_slam_pose){
    id = _slam_pose->node_id;
    slam_pose = _slam_pose;
    pose2d_node = new Pose2d_Node();
    processed = 0;
    pofz = 0;
    parent_supernode = -1;
    labeldist = new LabelDistribution();
    prob_used = 0;
    prob_of_scan = (double *) calloc(slam_pose->scan->numPoints, sizeof(double));
    max_scan_prob = 0; 
    
    //do the bounding box here - and in the update do the transform 
    pointlist2d_t *ptr = pointlist2d_new (slam_pose->scan->numPoints);
    for(int i=0; i<slam_pose->scan->numPoints; i++){//iterate over smpoints
      ptr->points[i].x = slam_pose->scan->points[i].x; 
      ptr->points[i].y = slam_pose->scan->points[i].y; 
    } 

    bounding_box_bot_frame = convexhull_simple_polygon_2d(ptr);
    bounding_box_global = pointlist2d_new(bounding_box_bot_frame->npoints);

    pointlist2d_free(ptr);
}

SlamNode::SlamNode(SlamNode *_node){
    id = _node->slam_pose->node_id;
    slam_pose = _node->slam_pose;
    pose2d_node = new Pose2d_Node();
    processed = 0;
    pofz = 0;
    parent_supernode = _node->parent_supernode;
    labeldist = _node->labeldist->copy();
    prob_used = 0;
    prob_of_scan = (double *) calloc(slam_pose->scan->numPoints, sizeof(double));
    max_scan_prob = 0; 

    //do the bounding box here - and in the update do the transform 
    pointlist2d_t *ptr = pointlist2d_new (slam_pose->scan->numPoints);
    for(int i=0; i<slam_pose->scan->numPoints; i++){//iterate over smpoints
      ptr->points[i].x = slam_pose->scan->points[i].x; 
      ptr->points[i].y = slam_pose->scan->points[i].y; 
    } 
    bounding_box_bot_frame = convexhull_simple_polygon_2d(ptr);
    bounding_box_global = pointlist2d_new(bounding_box_bot_frame->npoints);

    pointlist2d_free(ptr);
}

/*SlamNode::SlamNode(NodeScan *_slam_pose, LabelDistribution *ld){
    id = _slam_pose->node_id;
    slam_pose = _slam_pose;
    pose2d_node = new Pose2d_Node();
    processed = 0;
    pofz = 0;
    parent_supernode = -1;
    labeldist = ld->copy();
    prob_used = 0;
    prob_of_scan = (double *) calloc(slam_pose->scan->numPoints, sizeof(double));
    max_scan_prob = 0; 

    //do the bounding box here - and in the update do the transform 
    pointlist2d_t *ptr = pointlist2d_new (slam_pose->scan->numPoints);
    for(int i=0; i<slam_pose->scan->numPoints; i++){//iterate over smpoints
      ptr->points[i].x = slam_pose->scan->points[i].x; 
      ptr->points[i].y = slam_pose->scan->points[i].y; 
    } 
    bounding_box_bot_frame = convexhull_simple_polygon_2d(ptr);
    bounding_box_global = pointlist2d_new(bounding_box_bot_frame->npoints);

    pointlist2d_free(ptr);
}*/

void SlamNode::updateBoundingBox(){
    BotTrans bodyToLocal;
    Pose2d pose = getPose();
    bodyToLocal.trans_vec[0] = pose.x();
    bodyToLocal.trans_vec[1] = pose.y();
    bodyToLocal.trans_vec[2] = 0.0;
    double rpyQueryBody[3] = {0.0, 0.0, pose.t()};
    bot_roll_pitch_yaw_to_quat (rpyQueryBody, bodyToLocal.rot_quat);

    double pBody[3] =  {.0}, pLocal[3];

    if(bounding_box_global == NULL){
        bounding_box_global = pointlist2d_new(bounding_box_bot_frame->npoints);
    }
    
    for(int i=0; i< bounding_box_bot_frame->npoints; i++){//iterate over smpoints
        pBody[0] = bounding_box_bot_frame->points[i].x; 
        pBody[1] = bounding_box_bot_frame->points[i].y; 
        bot_trans_apply_vec ( &bodyToLocal, pBody, pLocal);
      
        bounding_box_global->points[i].x = pLocal[0];
        bounding_box_global->points[i].y = pLocal[1];
    } 
}

SlamNode::~SlamNode(){
    delete pose2d_node;
    delete labeldist;
    map<int, SlamConstraint *>::iterator s_it;
    free(prob_of_scan);
    if(bounding_box_bot_frame != NULL){
        pointlist2d_free(bounding_box_bot_frame);
    }
    if(bounding_box_global != NULL){
        pointlist2d_free(bounding_box_global);
    }
    
    //delete the constraints
    /*for ( s_it= constraints.begin() ; s_it != constraints.end(); s_it++ ){
        fprintf(stderr, "Deleting constraints in the node\n");
        delete s_it->second;
        }*/
}

void SlamNode::resetProbability(){
    max_scan_prob = 0; 
    memset(prob_of_scan, 0, slam_pose->scan->numPoints * sizeof(double));
}

Pose2d SlamNode::getPose(){
    return pose2d_node->value();
}
