#include "SlamGraph.hpp"

SlamConstraint::SlamConstraint(int _id, SlamNode *_node1,  SlamNode *_node2, SlamNode *_actualnode1, 
                               SlamNode *_actualnode2, Pose2d _transform, Noise _noise, 
                               double _hitpct, int _valid_sm, int32_t _type, int32_t _status)
{
    id = _id;
    node1 = _node1;
    node2 = _node2; 
    actualnode1 = _actualnode1;
    actualnode2 = _actualnode2;
    hitpct = _hitpct;
    valid_sm = _valid_sm;
    type = _type;
    status = _status;
    transform = Pose2d(_transform);
    noise = Noise(_noise);
    ct_pose2d =  new Pose2d_Pose2d_Factor(node2->pose2d_node, node1->pose2d_node, 
                                          transform, noise);
    processed = 0;
    pofz = -1;
}

SlamConstraint::~SlamConstraint()
{
    if(ct_pose2d != NULL)
      delete ct_pose2d;
    //maybe create the constraint here and delete it 
}
