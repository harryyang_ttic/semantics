#include <isam/isam.h>

class Pose2d_Pose2d_Point2d_Factor : public FactorT<Point2d>{//Pose2d> {
  Pose2d_Node* _pose1;
  Pose2d_Node* _pose2;
  
public:

  /**
   * Constructor.
   * @param pose1 The pose from which the measurement starts.
   * @param pose2 The pose to which the measurement extends.
   * @param measure The relative measurement from pose1 to pose2 (pose2 in pose1's frame).
   * @param noise The 3x3 square root information matrix (upper triangular).
   */

    //see how this works out 
  Pose2d_Pose2d_Point2d_Factor(Pose2d_Node* pose1, Pose2d_Node* pose2,
                               const Point2d& measure, const Noise& noise)
      : //FactorT<Pose2d>("Pose2d_Pose2d_Point2d_Factor", 2, noise, measure),
      //_pose1(pose1), _pose2(pose2) {
      FactorT<Point2d>("Pose2d_Pose2d_Point2d_Factor", 2, noise, measure),
      _pose1(pose1), _pose2(pose2) {

      _nodes.resize(2);
      
      _nodes[0] = pose1;
      _nodes[1] = pose2;
      
      //_pose1 = pose1;
      //_pose2 = pose2;
  }
  
  /*Pose2d_Pose2d_Point2d_Factor(Pose2d_Node* pose1, Pose2d_Node* pose2,
                               const Point2d& measure, const Noise& noise)
        : FactorT<Point2d>("Pose2d_Pose2d_Point2d_Factor", 2, noise), _measure(measure){
      //_pose1(pose1), _pose2(pose2)

      _nodes.resize(2);
      
      _nodes[0] = pose1;
      _nodes[1] = pose2;
  }*/

  void initialize() {
    Pose2d_Node* pose1 = _pose1;
    Pose2d_Node* pose2 = _pose2;
    require(pose1->initialized() && pose2->initialized(),
        "slam2d: Pose2d_Pose2d_Point2d_Factor requires pose1 and pose2 to be initialized");
  }

  Eigen::VectorXd basic_error(Selector s = LINPOINT) const {
      Pose2d p1(_nodes[0]->vector(s));
      Pose2d p2(_nodes[1]->vector(s));
      Point2d p_2d_1(p1.x(), p1.y());//p1.transform_to(pt);
      Point2d p_2d_2(p2.x(), p2.y());// = //p2.transform_to(pt);
      Point2d p(p_2d_2.x() - p_2d_1.x(), p_2d_2.y() - p_2d_1.y()); // = //p_2d_1.transform_to(p_2d_2);
      Eigen::VectorXd predicted = p.vector();
      return (predicted - _measure.vector());
    
      /*Pose2d predicted;
        if (_nodes.size()==4) {
        Pose2d a1(_nodes[2]->vector(s));
        Pose2d a2(_nodes[3]->vector(s));
        // see notes above
        predicted = (a2.oplus(p2)).ominus(a1.oplus(p1));
        } else {
        predicted = p2.ominus(p1);
        }

        //this needs to be a size 2 vector (since we told it that it will only have two elements - when creating the superclass
        Eigen::VectorXd err = predicted.vector() - _measure.vector();
        err(2) = standardRad(err(2));
        return err;*/
  }
      
    //we could write a new jacobian for just the two elements x,y - otherwise we could just not implement it 
    //in which case it will use a numerical method to derive a jacobian (less efficient) 
    /*Jacobian jacobian() {
    
    if (_nodes.size()==4) { // symbolic available only without anchor nodes
      return Factor::jacobian();
    } else {
      //const Pose2d& p1 = _pose1->value0();
      //const Pose2d& p2 = _pose2->value0();
      //Pose2d p = p2.ominus(p1);
      //double c = cos(p1.t());
      //double s = sin(p1.t());

      double dx = _measure.x();
      double dy = _measure.y();
      //double dt = standardRad(p.t() - _measure.t());

      Eigen::MatrixXd M1(2,3);
      M1 <<
        -1, 0,  0,
        0,  -1,  0;
      M1 = sqrtinf() * M1;

      Eigen::MatrixXd M2(2,3);
      M2 <<
        1,   0,   0.,
        0,  1,   0.;
      M2 = sqrtinf() * M2;

      Point2d pp(dx, dy);

      Eigen::VectorXd r = sqrtinf() * pp.vector();

      Jacobian jac(r);
      jac.add_term(_pose1, M1);
      jac.add_term(_pose2, M2);

      return jac;
    }
    }*/
};
