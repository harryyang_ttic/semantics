/*
 * NodeScan.h
 *
 *  Created on: Oct 22, 2009
 *      Author: abachrac
 */

#ifndef SLAMGRAPH_H_
#define SLAMGRAPH_H_
#include <vector>
#include <map>
#include <isam/isam.h>
#include <geom_utils/geometry.h>
#include <geom_utils/convexhull.h>

#include <lcmtypes/slam_graph_particle_list_t.h>

#include "NodeScan.h"
#include "Pose2d_Pose2d_Point2d_Factor.hpp"

using namespace std;
using namespace scanmatch;
using namespace isam;
using namespace Eigen;

#define SUPERNODE_SEARCH_DISTANCE 20
#define LABEL_INCREMENT 1.0
#define LABEL_BLEED_INCREMENT 0.5

typedef pair<int, int> nodePairKey;

class SlamNode;

class SlamConstraint;

class LabelDistribution;

class ConstraintKey {    
    int id_1;
    int id_2;
    
    ConstraintKey();
    ConstraintKey(int _id_1, int _id_2);
};


class SlamGraph {
public:
    SlamGraph(int64_t _utime);//, int _graph_id);
    
    virtual ~SlamGraph();

    SlamNode * addSlamNode(NodeScan *pose);
    SlamNode * getSlamNode(int64_t id);
    void addOriginConstraint(SlamNode *node1, Pose2d transform, Noise noise);
    SlamConstraint * addConstraint(int id, SlamNode *node1,  SlamNode *node2, SlamNode *actualnode1, 
                                   SlamNode *actualnode2, Pose2d transform, Noise noise, 
                                   double hitpct, int32_t type, int32_t status);
    void addDummyConstraint(int id, SlamNode *node1,  SlamNode *node2, SlamNode *actualnode1, 
                            SlamNode *actualnode2, Pose2d transform, Noise noise, 
                            double hit_pct, int32_t type, int32_t status);
    SlamConstraint * addConstraint(SlamNode *node1,  SlamNode *node2, SlamNode *actualnode1, 
                                   SlamNode *actualnode2, Pose2d transform, Noise noise, 
                                   double hitpct, int32_t type, int32_t status);
    void addDummyConstraint(SlamNode *node1,  SlamNode *node2, SlamNode *actualnode1, 
                            SlamNode *actualnode2, Pose2d transform, Noise noise, 
                            double hit_pct, int32_t type, int32_t status);
    int hasConstraint(int cons_id);
    int hasConstraint(int id_1, int id_2, int32_t type); 
    int hasConstraint(int id_1, int id_2); 
    int getConstraintID(int id_1, int id_2, int32_t type);
    int removeConstraint(int id_1, int id_2, int32_t type);
    int removeConstraint(int constraint_id);
    SlamNode * addSlamNode(SlamNode *_node);
    //SlamNode * addSlamNode(NodeScan *pose, LabelDistribution *ld);
    //not sure why this is a pointer
    int hasConstraint(int const_id, int32_t *type, int32_t *status);
    MatrixXd getCovariances(SlamNode *node1, SlamNode *node2);
    MatrixXd getCovariance(SlamNode *node);
    map<int,MatrixXd> getCovariances(SlamNode *nd1, vector<SlamNode *> nd_list);
    double calculateProbability(SlamNode *node1, SlamNode *node2, Matrix3d cov_sm, Pose2d sm_constraint);
    
    void updateSupernodeIDs();
    void updateBoundingBoxes();
    
    int getNextSupernodeID(int ind, int search_distance);
    int getPreviousSupernodeID(int ind, int search_distance);
    //this will be changed soon 
    int belongsToCurrentSupernode(int ind, int curr_sn, int prev_sn, int next_sn);

    //this will update slam and fill the covariances 
    void runSlam();
    void clearDummyConstraints();
    SlamConstraint * getConstraint(int const_id);
    
    int64_t utime;
    int updated; //this should be set to 1 after the particle is processed
    
    int status;

    Pose2d_Node *origin_node;
    Pose2d_Factor *prior;

    vector<SlamNode *> slam_node_list;
    map<int, SlamNode *> slam_nodes; //deleted on destroy
    map<int, SlamConstraint *> slam_constraints; //deleted on destroy
    map<int, SlamConstraint *> failed_slam_constraints; //deleted on destroy
    int no_edges_added;
    vector<nodePairKey> proposed_language_edges;
    //this should prob be how we use it 
    Pose2d_Pose2d_Factor *origin_constraint; //deleted on destroy

private:
    Slam *slam;

};

class LabelDistribution {
public:
    int num_labels;
    vector<string> labels;
    vector<double> saliency; // Saliency threshold (>= 1/num_labels) for the labels. Lower means more salient
    int last_direct_obs_id;
    double total_obs;
    vector<double> observation_frequency;
    vector<int> count;
    vector<int> labeled_node_ids; // IDs from which the distribution was bled;
    void addDirectObservation(int l, double increment, int lang_update_id);
    void addObservation(int l, double increment, int src_node_id);
    int bleedLabels (LabelDistribution *ld_src);
    LabelDistribution();
    //LabelDistribution(vector<double> obs);
    LabelDistribution * copy();
    bool areLabelsSalient();

};

class SlamNode {
public:
    int id;
    int processed; 
    int prob_used; 
    NodeScan *slam_pose; //should not be deleted
    int parent_supernode;
    double cov[9]; 
    SlamNode(NodeScan *_slam_pose);
    //SlamNode(NodeScan *_slam_pose, LabelDistribution *ld);
    SlamNode(SlamNode *_node);//, LabelDistribution *ld);
    void updateBoundingBox();
    double pofz;
    pointlist2d_t *bounding_box_bot_frame;
    pointlist2d_t *bounding_box_global;

    virtual ~SlamNode();
    Pose2d getPose();
    void resetProbability();
    
    map<int, SlamConstraint *> constraints; //this is keyed on the id of the second node - should be deleted on destroy
    Pose2d_Node *pose2d_node; //should be delted on deletion
    LabelDistribution *labeldist;

    double *prob_of_scan; 
    double max_scan_prob; 

    BotTrans queryBodyToMatchBody;
    int valid;
};

class SlamConstraint{
public:
    int id;
    SlamNode *node1;
    SlamNode *node2;
    SlamNode *actualnode1;
    SlamNode *actualnode2;
    Pose2d_Pose2d_Factor *ct_pose2d; //should be deleted on deletion
    double hitpct;
    int valid_sm;
    double pofz;
    Pose2d transform;
    Noise noise;
    int processed; 
    int32_t type;
    int32_t status;
    SlamConstraint(int id, SlamNode *_node1,  SlamNode *_node2, SlamNode *_actualnode1, SlamNode *_actualnode2, Pose2d transform, Noise noise, double hitpct, int valid_sm, int32_t type, int32_t status);
    virtual ~SlamConstraint();
};

#endif
