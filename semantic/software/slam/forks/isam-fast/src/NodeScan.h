/*
 * NodeScan.h
 *
 *  Created on: Oct 22, 2009
 *      Author: abachrac
 */

#ifndef SLAMPOSE_H_
#define SLAMPOSE_H_
//#include <bot/scanmatch/Scan.hpp>
#include <scanmatch/Scan.hpp>
#include <vector>
#include <isam/isam.h>
//#include "AnchorPose.h"
//#include "PoseToPoseTransform.h"
#include <bot_core/bot_core.h>
// ANN declarations
#include <ANN/ANN.h> 
#include <ANN/ANNx.h>
#include <ANN/ANNperf.h>

using namespace std;
using namespace scanmatch;
using namespace isam;

class PoseToPoseTransform;

typedef struct{
    ANNpointArray dataPts;
    int no_points;
} ann_t;

class NodeScan {
public:
    NodeScan(int64_t _utime, int _node_id, Scan * _scan, bool _is_supernode);
    
    virtual ~NodeScan();
    ann_t *kd_points;
    ANNkd_tree *kd_tree;
    int updateScan();
    
    int64_t utime;
    int node_id;
    Scan * scan;
    double max_prob; 
    Pose2d_Node *pose2d_node;
    bot_core_planar_lidar_t *laser;
    bool is_supernode;

    vector<Scan *> allScans;
    PoseToPoseTransform * inc_constraint_odom;
    PoseToPoseTransform * inc_constraint_sm;
    //vector<PoseToPoseTransform *> constraints;
    map<int, vector<PoseToPoseTransform *> *> sm_full_constraints;
    map<int, PoseToPoseTransform *> sm_lang_constraints;
    map<int, PoseToPoseTransform *> sm_constraints;
    
};

class PoseToPoseTransform {
 public:
        
    Pose2d *transform;
    NodeScan *node_current;
    NodeScan *node_relative_to;

    //some indication of the prior 
    double distance_difference;
    double angle_difference;

    Noise *noise;
    double hitpct; 
    int accepted; 
    int32_t type;

    PoseToPoseTransform(Pose2d *_transform, NodeScan *_node_current, NodeScan *_node_relative_to, int32_t _type,  
                        Noise *_noise, double _hitpct, double dist_diff, double ang_diff, int _accepted){
        node_current = _node_current;
        node_relative_to = _node_relative_to;
        type = _type;
        transform = _transform; 
        noise = _noise;
        hitpct = _hitpct;
        accepted = _accepted;
        distance_difference = dist_diff;
        angle_difference = ang_diff;
    }        
};


#endif /* SLAMPOSE_H_ */
