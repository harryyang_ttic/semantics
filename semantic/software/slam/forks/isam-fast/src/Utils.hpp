#ifndef SEMANTIC_UTILS_H_
#define SEMANTIC_UTILS_H_

#include <gsl/gsl_blas.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_sf_bessel.h>
#include <gsl/gsl_integration.h>
#include <math.h>
#include "SlamGraph.hpp"

typedef struct _dist_params 
{ 
    double mu; 
    double sigma;
} dist_params;

//BotTrans getBotTransFromPose(double x, double y, double theta);

double rice_dist_probability (double x, void * p);
int check_to_add(SlamNode *node1, SlamNode *node2, int find_ground_truth);
int check_to_add_basic(SlamNode *node1, SlamNode *node2, int find_ground_truth, MatrixXd cov);
double get_observation_probability(double mu, double sigma);

#endif
