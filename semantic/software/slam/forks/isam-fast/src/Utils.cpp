#include "Utils.hpp"

/*BotTrans getBotTransFromPose(double x, double y, double theta){

    BotTrans trans;
    trans.trans_vec[0] = x;
    trans.trans_vec[1] = y;
    trans.trans_vec[2] = 0.0;
    double rpy[3] = { 0, 0, theta };
    bot_roll_pitch_yaw_to_quat(rpy, trans.rot_quat);
    return trans;
    }*/
 
double rice_dist_probability (double x, void * p) {
    dist_params * params = (dist_params *)p;
    double mu = (params->mu);
    double sigma = (params->sigma);
    
    double temp = x/ pow(sigma, 2) * exp( -(pow(x,2) + pow(mu,2)) / (2 * pow(sigma,2)));
    double temp2 = temp * gsl_sf_bessel_I0(x*mu/ pow(sigma,2));
    double rtrn = temp2 / (1.0 + 0.8*pow(mu,2)); //was 0.2
    return  rtrn;
}

double folded_gaussian_probability_for_dist(double x, void *p){
    dist_params * params = (dist_params *)p;
    double mu = (params->mu);
    double sigma = (params->sigma);

    if(x >= 0){
        double prob1 = 1/ (sigma * pow(2 * M_PI, 0.5)) * (exp (-pow((-x - mu),2)/(2 * pow(sigma,2))) + exp (-pow((x - mu),2)/(2 * pow(sigma,2))));
        double prob = prob1 / (1.0 + 0.4*pow(x,2));
        return prob; 
    }
    else{
        return 0;
    } 
}

double folded_gaussian_probability(double x, void *p){
    dist_params * params = (dist_params *)p;
    double mu = (params->mu);
    double sigma = (params->sigma);

    //not sure which one we should use
    double sigma1 = 0.4;

    if(x >= 0){
        double prob1 = 1/ (sigma * pow(2 * M_PI, 0.5)) * (exp (-pow((-x - mu),2)/(2 * pow(sigma,2))) + exp (-pow((x - mu),2)/(2 * pow(sigma,2))));
        //double prob = prob1 * 4 / (sigma1 * pow(2 * M_PI, 0.5)) * exp (-pow(x,2)/(2 * pow(sigma1,2))) ;
        if(x < 0.5) 
            return prob1; 
        else if(x < 1.0)
            prob1 *= (1- 2 * (x - 0.5)); 
        else 
            prob1 = 0; 
        return prob1;
    }
    else{
        return 0;
    } 
}

double get_observation_probability(double mu, double sigma){
    gsl_function F;
    dist_params params = {mu, sigma};
        
    F.function = &folded_gaussian_probability;
    F.params = &params;

    int64_t s_time = bot_timestamp_now();

    double result = 1.0;
    double abserr = 1.0;
    double epsabs = .001;
    double epsrel = .001;
    size_t limit = 20000;
    gsl_integration_workspace * workspace = gsl_integration_workspace_alloc (limit);
       
    int error = gsl_integration_qags (&F, 0, 1.0, epsabs, epsrel, limit,  workspace, &result, &abserr);

    int64_t e_time = bot_timestamp_now();

    //fprintf(stderr, "\n\n\n========== ++++++++++ Time taken for integration : %f\n", (e_time - s_time)/1.0e6);
    //fprintf(stderr, "Result : %f\n", result);
    
    gsl_integration_workspace_free (workspace);

    //i think we dont need to do this 
    double min_prob = 0.05; 
    if(result < min_prob)
        result = min_prob; 

    return result;
}

//Bianca - to fill 
int check_to_add(SlamNode *node1, SlamNode *node2, int find_ground_truth){
    gsl_set_error_handler_off();
    Pose2d p1 = node1->getPose();
    Pose2d p2 = node2->getPose();
    
    double dist = hypot(p1.x() - p2.x(), p1.y() - p2.y());
    //fprintf(stderr, "\t\t Distance: %f\n", dist);
    
    if(find_ground_truth){
      if(dist < 10)
         return 1;
      else return 0;
    }
    
    double sigma = node1->cov[0] + node1->cov[4] + node2->cov[0] + node2->cov[4];
    sigma = sigma/2;
    
    //fprintf(stderr, "\t\t Sigma: %f\n", sigma);
    
    //if (dist < 8){
       

    double threshold = 0;
    //fprintf(stderr, "\t\t Threshold: %f\n", threshold);
       
    gsl_function F;
    dist_params params = {dist, .3};
        
    F.function = &rice_dist_probability;
    F.params = &params;
              
    double x = 1.0;
    double y = 1.0;
       
    double * result = &x;
    double * abserr = &y;
    double epsabs = .001;
    double epsrel = .001;
    size_t limit = 2000000;
    gsl_integration_workspace * workspace = gsl_integration_workspace_alloc (limit);
    limit = 2000000;  
    //fprintf(stderr, "Made workspace\n"); 
       
    int error = gsl_integration_qags (&F, 0.0001, 8.0, epsabs, epsrel, limit,  workspace, result, abserr);
    //fprintf(stderr, "Integrated\n");
    if (error != 0 && dist > 20)
        threshold = 0;
    else if (error !=0)
        threshold = 1 / (pow(dist,2) + 1);
    else threshold = *result;
       

   
    gsl_integration_workspace_free (workspace);
       
       
    //this should be a function of the distance, cov etc 

    double rand_val = rand() / (double) RAND_MAX;
       
    //fprintf(stderr, "got result and random value\n");
    /*fprintf(stderr, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");
      fprintf(stderr, "\tEdge information - Node %d to Node %d\n", node1->id, node2->id);
      fprintf(stderr, "\tDistance: %f; Threshold: %f; Random value: %f\n", dist, threshold, rand_val);
      fprintf(stderr, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");
    */

    if(rand_val < threshold)
        return 1;
    return 0;
    //}
    //if(0){
    //    fprintf(stderr, "\t\t %d - %d => %f => threshold : %f - rand : %f\n",
    //            node1->id, node2->id, 
    //            dist, threshold, rand_val);
    //}

    return 0;
}

//Bianca - to fill 
int get_prob_for_edge(SlamNode *node1, SlamNode *node2, int find_ground_truth){
    gsl_set_error_handler_off();
    Pose2d p1 = node1->getPose();
    Pose2d p2 = node2->getPose();
    
    double dist = hypot(p1.x() - p2.x(), p1.y() - p2.y());
    //fprintf(stderr, "\t\t Distance: %f\n", dist);
    
    if(find_ground_truth){
      if(dist < 10)
         return 1;
      else return 0;
    }
    
    double sigma = node1->cov[0] + node1->cov[4] + node2->cov[0] + node2->cov[4];
    sigma = sigma/2;
    
    //fprintf(stderr, "\t\t Sigma: %f\n", sigma);
    
    //if (dist < 8){
       

    double threshold = 0;
    //fprintf(stderr, "\t\t Threshold: %f\n", threshold);
       
    gsl_function F;
    dist_params params = {dist, .3};
        
    F.function = &rice_dist_probability;
    F.params = &params;
              
    double x = 1.0;
    double y = 1.0;
       
    double * result = &x;
    double * abserr = &y;
    double epsabs = .001;
    double epsrel = .001;
    size_t limit = 2000000;
    gsl_integration_workspace * workspace = gsl_integration_workspace_alloc (limit);
    limit = 2000000;  
    //fprintf(stderr, "Made workspace\n"); 
       
    int error = gsl_integration_qags (&F, 0.0001, 8.0, epsabs, epsrel, limit,  workspace, result, abserr);
    //fprintf(stderr, "Integrated\n");
    if (error != 0 && dist > 20)
        threshold = 0;
    else if (error !=0)
        threshold = 1 / (pow(dist,2) + 1);
    else threshold = *result;
       

   
    gsl_integration_workspace_free (workspace);
       
       
    //this should be a function of the distance, cov etc 

    double rand_val = rand() / (double) RAND_MAX;
       
    //fprintf(stderr, "got result and random value\n");
    /*fprintf(stderr, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");
      fprintf(stderr, "\tEdge information - Node %d to Node %d\n", node1->id, node2->id);
      fprintf(stderr, "\tDistance: %f; Threshold: %f; Random value: %f\n", dist, threshold, rand_val);
      fprintf(stderr, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");
    */

    if(rand_val < threshold)
        return 1;
    return 0;
    //}
    //if(0){
    //    fprintf(stderr, "\t\t %d - %d => %f => threshold : %f - rand : %f\n",
    //            node1->id, node2->id, 
    //            dist, threshold, rand_val);
    //}

    return 0;
}


//Bianca - to fill 
int check_to_add_basic(SlamNode *node1, SlamNode *node2, int find_ground_truth, MatrixXd cov){
    //fprintf(stderr, "+++++++++ Checking to add\n");

    gsl_set_error_handler_off();
    Pose2d p1 = node1->getPose();
    Pose2d p2 = node2->getPose();
    
    double dist = hypot(p1.x() - p2.x(), p1.y() - p2.y());
    //fprintf(stderr, "\t\t Distance: %f\n", dist);
    
    if(find_ground_truth){
      if(dist < 10)
         return 1;
      else return 0;
    }

    if(dist > 20)
        return 0;
    
    //double sigma = node1->cov[0] + node1->cov[4] + node2->cov[0] + node2->cov[4];

    MatrixXd H(1,6);
    H(0,0) = (p1.x() - p2.x()) / dist;
    H(0,1) = (p1.y() - p2.y()) / dist;
    H(0,2) = 0;
    H(0,3) = -(p1.x() - p2.x()) / dist;
    H(0,4) = -(p1.y() - p2.y()) / dist;
    H(0,5) = 0;

    MatrixXd cov_r = H * cov * H.transpose();

    //cout << "Result Distribution => Mean : " <<  dist << "Cov " << cov_r(0,0) << endl;

    //cout << "[%d] - [%d] Result Distribution (1) => Mean : " <<  dist << "Cov " << cov_r(0,0) << endl;

    double threshold = 0;
    double sigma = pow(cov_r(0,0),0.5);
  
    if(0)
        fprintf(stderr, "[%d] - [%d] => mu (%f) sigma (%f)\t", node1->id, node2->id, 
                dist, sigma);
    
    double a = fmax(0,dist - 2 * sigma);
    double b = dist + 2 * sigma;
    gsl_function F;
    dist_params params = {dist, sigma};
        
    F.function = &folded_gaussian_probability_for_dist;
    F.params = &params;
              
    double result = 1.0;
    double abserr = 1.0;
    double epsabs = .001;
    double epsrel = .001;
    size_t limit = 20000;

    gsl_integration_workspace * workspace = gsl_integration_workspace_alloc (limit);

    //fprintf(stderr, "Made workspace\n"); 
       
    int error = gsl_integration_qags (&F, a, b, epsabs, epsrel, limit,  workspace, &result, &abserr);
    
    if(0)
        fprintf(stderr, "Result : %f\n", result);

    if (error != 0 && dist > 20)
        threshold = 0;
    else if (error !=0)
        threshold = 1 / (pow(dist,2) + 1);
    else threshold = result;
   
    gsl_integration_workspace_free (workspace);
       
       
    //this should be a function of the distance, cov etc 

    double rand_val = rand() / (double) RAND_MAX;
       
    //fprintf(stderr, "got result and random value\n");
    //fprintf(stderr, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");
    // fprintf(stderr, "\tEdge information - Node %d to Node %d\n", node1->id, node2->id);
    //fprintf(stderr, "\tDistance: %f; Threshold: %f; Random value: %f\n", dist, threshold, rand_val);
    // fprintf(stderr, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");
    
    if(rand_val < threshold){
        //fprintf(stderr, " - Proposed\n");
        return 1;
    }
    //fprintf(stderr, " - Rejected\n");
    return 0;
    //}
    //if(0){
    //    fprintf(stderr, "\t\t %d - %d => %f => threshold : %f - rand : %f\n",
    //            node1->id, node2->id, 
    //            dist, threshold, rand_val);
    //}

}
