#include "SlamParticle.hpp"

#define RESET_COLOR "\e[m"
#define MAKE_GREEN "\e[31m"

#define SCAN_Z_MAX 20.0
#define SCAN_Z_MIN 0.01
#define PIXMAP_RESOLUTION 0.1
#define PIXMAP_HIT_INC 0.5
#define MIN_EDGE_SIMILARITY 0.2 // Minimum similarity for proposed edges
#define PROB_PROPOSE_LANGUAGE_EDGE 0.5 // Probability of proposing language edge

SlamParticle::SlamParticle(int64_t _utime, int _graph_id, 
                           lcm_t *_lcm,
                           ScanMatcher *_sm, 
                           ScanMatcher *_sm_low_res,
                           bool _useLowResPrior,
                           bool _scanmatchBeforeAdd,
                           BotTrans _body_to_laser, 
                           FloatPixelMap *_local_px_map = NULL,                           
                           bot_lcmgl_t *_lcmgl_sm_basic = NULL,
                           bot_lcmgl_t *_lcmgl_sm_graph = NULL,
                           bot_lcmgl_t *_lcmgl_sm_prior = NULL,
                           bot_lcmgl_t *_lcmgl_sm_result = NULL,
                           bot_lcmgl_t *_lcmgl_sm_result_low = NULL,
                           bot_lcmgl_t *_lcmgl_loop_closures = NULL, 
                           bool _scanmatchOnlyRegion = true, 
                           bool _addDummyConstraints = false, 
                           bool _useICP = false, 
                           bool _drawScanmatch = false){

    graph = new SlamGraph(_utime);
    graph_id = _graph_id;    
    sm = _sm;
    verbose = false;
    scanmatchBeforeAdd = _scanmatchBeforeAdd;
    scanmatchOnlyRegion = _scanmatchOnlyRegion;//true;
    noScanTransformCheck = false;
    addDummyConstraints = _addDummyConstraints;//true;
    draw_scanmatch = _drawScanmatch;
    doWideMatchOnFail = false;
    sm_acceptance_threshold = 0.8;//0.8; //0.7 - can create some bad scanmatches 
    inc_sm_acceptance_threshold = 0.7;//0.5; 
    lang_sm_acceptance_threshold = 0.5;
    useICP = _useICP; 
    strictScanmatch = false;
    useSMCov = false;
    lcmgl_sm_basic = _lcmgl_sm_basic;
    lcmgl_sm_graph = _lcmgl_sm_graph;
    lcmgl_sm_prior = _lcmgl_sm_prior;
    lcmgl_sm_result = _lcmgl_sm_result;
    local_px_map = _local_px_map;
    lcmgl_loop_closures = _lcmgl_loop_closures;
    lcmgl_sm_result_low = _lcmgl_sm_result_low;
    sm_low_res = _sm_low_res;
    useLowResPrior = _useLowResPrior;
    max_dist_for_lc = 10.0;
    weight = 1;
    lcm = _lcm;
    body_to_laser = _body_to_laser;
    laser_to_body = body_to_laser;
    bot_trans_invert(&laser_to_body);
    count_for_batch_optimize=1;
    language_event_count = 0;

    // Create and seed the random number generator
    const gsl_rng_type * T = gsl_rng_default;
    rng = gsl_rng_alloc (T);
    
    unsigned int seed;
    struct timeval tv;
    FILE *devrandom;
    
    if(1){
        gettimeofday(&tv,0);
        seed = tv.tv_sec + tv.tv_usec;
    }
    else{
        //this seems pretty slow for some reason 
        if ((devrandom = fopen("/dev/random","r")) == NULL) {
            gettimeofday(&tv,0);
            seed = tv.tv_sec + tv.tv_usec;
        } else {
            fread(&seed,sizeof(seed),1,devrandom);
            fclose(devrandom);
        }
    }

    gsl_rng_set (rng, seed);
}

SlamParticle::~SlamParticle(){
    delete graph;
}

Pose2d SlamParticle::getPoseFromBotTrans(BotTrans tf){
    double rpy[3];
    bot_quat_to_roll_pitch_yaw(tf.rot_quat, rpy);
    Pose2d pose(tf.trans_vec[0],tf.trans_vec[1],rpy[2]);
    return pose;
}

BotTrans SlamParticle::getBotTransFromPose(Pose2d pose){
    BotTrans trans;
    trans.trans_vec[0] = pose.x(); 
    trans.trans_vec[1] = pose.y(); 
    trans.trans_vec[2] = 0.0;

    double rpy[3] = { 0, 0, pose.t() };
    bot_roll_pitch_yaw_to_quat(rpy, trans.rot_quat);
    
    return trans; 
}

int SlamParticle::getCorrespondance(BotTrans node_match_to_target, SlamNode *target, SlamNode *match){
    Scan *match_scan = match->slam_pose->scan;
    double pQuery[3] = {.0},  pMatchInTarget[3];
    ANNpoint qp = annAllocPt(2);
    double radius_bound = 0.2;//0.05;//0.3; //was 0.05 
    int query_size = 1;
    double epsilon =  0.05;//0.1;
    ANNidxArray  nnIdx = new ANNidx[query_size];
    ANNdistArray dists = new ANNdist[query_size];

    int matched = 0;

    for(unsigned i = 0; i < match_scan->numPoints; i++){
        pQuery[0] = match_scan->points[i].x;
        pQuery[1] = match_scan->points[i].y;

        // Transform the query point into the local frame
        bot_trans_apply_vec(&node_match_to_target, pQuery, pMatchInTarget);

        qp[0] = pMatchInTarget[0];
        qp[1] = pMatchInTarget[1];
        
        int64_t search_utime = bot_timestamp_now();
        
        target->slam_pose->kd_tree->annkFRSearch (qp,                    // query point
                                                  ANN_POW(radius_bound), // squared radius search bound
                                                  query_size,            // number of near neighbors
                                                  nnIdx,                 // nearest neighbors (returned)
                                                  dists,                 // distance (returned)
                                                  epsilon);
        
        for(int l=0;l< query_size ;l++){
            if(nnIdx[l] < 0){//because we have reached the end of the valid neighbours
                break; 
                //continue;
            }
            //count++;
            matched++;
            break;
        }
    }
    
    //this is not strictly correct as this is not what we scanmatch for ICP - we scanmatch the entire region 
    //why not use a gridmap ?? 
    return matched;
}

void SlamParticle::adjust(int id, int64_t utime, SlamParticle *part){
    //take the given particle and adjust the graph to match the given particle - this should be less 
    graph_id = id; 
    
    map<int, SlamConstraint *>::iterator c_it;

    //create a new set of constraints that match the old graph

    //update the label distribution 
    //add the nodes and the constraints 
    for(int i=0; i< part->graph->slam_node_list.size(); i++){
        
        SlamNode *ref_node = (SlamNode *) part->graph->slam_node_list.at(i);
        
        //create the nodes 
        SlamNode *adj_node = (SlamNode *) graph->getSlamNode(ref_node->id);

        //delete the label distribution 
        delete adj_node->labeldist;
        //make a copy of the old one 
        adj_node->labeldist = ref_node->labeldist->copy(); 
        adj_node->prob_used = ref_node->prob_used;
    }


    //add the constraints that are present in the part (but not in the current graph) 
    for ( c_it= part->graph->slam_constraints.begin() ; c_it != part->graph->slam_constraints.end(); c_it++ ){
        

        //check if there is a constraint added with the same id - otherwise add it 
        //skip the incremental ones
        //SlamConstraint *constraint  = (SlamConstraint *) c_it->second;
        SlamConstraint *edge = (SlamConstraint *) c_it->second;
        if(edge->type == SLAM_GRAPH_EDGE_T_TYPE_SM_INC || edge->type == SLAM_GRAPH_EDGE_T_TYPE_ODOM_INC){
            if(verbose)
                fprintf(stderr, "Incremental constraint : %d - %d => already added - skipping : %d\n", 
                        (int) edge->node1->id, (int) edge->node2->id, edge->type);
            continue;
        }
        else {
            //add the constraint 
            if(verbose)
                fprintf(stderr, "Adding Loop closure constraint from the old graph : %d - %d\n", 
                        (int) edge->node1->id, (int) edge->node2->id);

            SlamNode *node1 = graph->getSlamNode(edge->node1->id);
            SlamNode *node2 = graph->getSlamNode(edge->node2->id);
            
            SlamNode *actualnode1 = graph->getSlamNode(edge->actualnode1->id);
            SlamNode *actualnode2 = graph->getSlamNode(edge->actualnode2->id);

            //check if this constraint is already there 
            if(graph->hasConstraint(edge->node1->id, edge->node2->id, edge->type)){
                continue;
            }
            else{
                SlamConstraint* new_ct  = graph->addConstraint(node1, node2, actualnode1, actualnode2, edge->transform, 
                                                               edge->noise, edge->hitpct, edge->type , edge->status);
                new_ct->pofz = edge->pofz;
            }
            
            
        }
    }

    vector<int> constraints_to_remove;

    for ( c_it= graph->slam_constraints.begin() ; c_it != graph->slam_constraints.end(); c_it++ ){
        
        //check if there is a constraint added with the same id - otherwise add it 
        //skip the incremental ones
        SlamConstraint *edge = (SlamConstraint *) c_it->second;
        if(edge->type == SLAM_GRAPH_EDGE_T_TYPE_SM_INC || edge->type == SLAM_GRAPH_EDGE_T_TYPE_ODOM_INC){
            if(verbose)
                fprintf(stderr, "Incremental constraint : %d - %d => already added - skipping : %d\n", 
                        (int) edge->node1->id, (int) edge->node2->id, edge->type);
            continue;
        }
        
        else {
            //add the constraint 
            if(verbose)
                fprintf(stderr, "Adding Loop closure constraint from the old graph : %d - %d\n", 
                        (int) edge->node1->id, (int) edge->node2->id);

            SlamNode *node1 = graph->getSlamNode(edge->node1->id);
            SlamNode *node2 = graph->getSlamNode(edge->node2->id);
            
            SlamNode *actualnode1 = graph->getSlamNode(edge->actualnode1->id);
            SlamNode *actualnode2 = graph->getSlamNode(edge->actualnode2->id);

            //check if this constraint is already there 
            /*if(part->graph->hasConstraint(edge->node1->id, edge->node2->id, edge->type)){
                continue;
            }
            //the model graph doesnt have this constraint - remove it 
            else{
                //might be because we are iterating and deleting the items at the same time 
                //graph->removeConstraint(edge->node1->id, edge->node2->id, edge->type);
                constraints_to_remove.push_back();
                }*/
            if(part->graph->hasConstraint(edge->node1->id, edge->node2->id, edge->type)){
                continue;
            }
            else{
                int id = graph->getConstraintID(edge->node1->id, edge->node2->id, edge->type);
                if(id>=0){
                    if (verbose)
                        fprintf(stderr, "Removing constraint : %d\n", id);
                    constraints_to_remove.push_back(id);
                }
            }
        }
    }

    //now remove the constraints 
    for(int i=0; i < constraints_to_remove.size(); i++){
        graph->removeConstraint(constraints_to_remove.at(i));
    }

    map<int, SlamConstraint *>::iterator iter;
    for ( iter = graph->slam_constraints.begin() ; iter != graph->slam_constraints.end(); iter++ ){        
        SlamConstraint *constr = iter->second;
        constr->processed = 1;
    }
    
    /*for(int j=0; j< graph->slam_constraints.size(); j++){
        SlamConstraint *constr = graph->slam_constraints.at(j);
        constr->processed = 1;
        }*/
    graph->status = 0;
    //this should run batch optimization
    graph->runSlam();    
    weight = part->weight;
}


SlamParticle * SlamParticle::copy(int64_t utime, int new_id){

   
    SlamParticle *new_part = new SlamParticle(utime, new_id, lcm, sm, sm_low_res, 
                                              useLowResPrior, 
                                              scanmatchBeforeAdd,
                                              body_to_laser,
                                              local_px_map,                                              
                                              lcmgl_sm_basic, 
                                              lcmgl_sm_graph, 
                                              lcmgl_sm_prior, 
                                              lcmgl_sm_result, 
                                              lcmgl_sm_result_low, 
                                              lcmgl_loop_closures, 
                                              scanmatchOnlyRegion, 
                                              addDummyConstraints,
                                              useICP, 
                                              draw_scanmatch);

    fprintf(stderr, "++++++++++ [[[[ Creating a new graph : %d ]]]] \n", new_id);
    //add the nodes and the constraints 
    for(int i=0; i< graph->slam_node_list.size(); i++){
        //create the nodes 
        SlamNode *orig_node = (SlamNode *) graph->slam_node_list.at(i);

        SlamNode *node = new_part->graph->addSlamNode(orig_node); //->slam_pose, orig_node->labeldist);
        node->prob_used = orig_node->prob_used;
    }
    
    //add the constraint to the origin node
    int first_node_id = 0;
    
    SlamNode *node = new_part->graph->getSlamNode(first_node_id);//graph->slam_nodes.find(first_node_id)->second;
        
    if(node == NULL){
        fprintf(stderr, "First node not found - this should not have happened\n");
    }
    else{
        //this one doesnt matter - set to small for either type
        if(node->slam_pose->inc_constraint_sm != NULL){
            PoseToPoseTransform *p2p_constraint = node->slam_pose->inc_constraint_sm; 
            double cov_hardcode[9] = { .0001, 0, 0, 0, .0001, 0, 0, 0, .0001 };
            Matrix3d cv_hardcode(cov_hardcode);
            Noise cov_hc= SqrtInformation(10000. * eye(3));//Covariance(cv_hardcode);
            new_part->graph->addOriginConstraint(node, *p2p_constraint->transform, *p2p_constraint->noise);
        }
    }  
         
    //add the edges 
    for(int i=0; i < new_part->graph->slam_node_list.size(); i++){
        SlamNode *node1 = new_part->graph->slam_node_list.at(i);
        if(node1->processed)  //we already added edges to this guy - skipping 
            continue;
    
        if(node1->id == 0){ //first node - no inc constraint
            node1->processed = 1;
            continue;
        }
        int prev_id = node1->id - 1;
        SlamNode *node2 = new_part->graph->getSlamNode(prev_id);

        if(node2 == NULL)
            continue;

        PoseToPoseTransform *p2p_constraint = NULL;
        
        int32_t type, status; 
        
        // Use only odometry if requested, otherwise use scanmatching and fall back
        // on odometry if the incremental scan match is bad
        if (scanmatchBeforeAdd && (node1->slam_pose->inc_constraint_sm->hitpct > inc_sm_acceptance_threshold)) {
            type = SLAM_GRAPH_EDGE_T_TYPE_SM_INC;
            p2p_constraint = node1->slam_pose->inc_constraint_sm;
        }
        else {
            type =  SLAM_GRAPH_EDGE_T_TYPE_ODOM_INC;
            p2p_constraint = node1->slam_pose->inc_constraint_odom;
        }

        if(p2p_constraint == NULL || p2p_constraint->node_relative_to == NULL){
            continue;
        }
        
        if(node1 == NULL || node2 == NULL){
            fprintf(stderr, "Error - one or more nodes not found\n");
            continue;
        }
        
        if(p2p_constraint->node_relative_to->node_id != prev_id){
            if(verbose){
                fprintf(stderr, "Not an incremental constraint - skipping for now\n");
            }
            continue;
        }

        //check if the constraint exists - and if so add the constraint 
        //node id is the constraint respective to - i.e. current_node (id_1) respective to (id_2)
        //so find the constraint if it exists 
        
        new_part->graph->addConstraint(node1, node2, node1, node2, *p2p_constraint->transform, *p2p_constraint->noise, p2p_constraint->hitpct, type, SLAM_GRAPH_EDGE_T_STATUS_SUCCESS);
        

        node1->processed = 1;
        
        if(verbose)
            fprintf(stderr, "\tAdding new constraints - %d %d\n", (int) node1->id, (int) node2->id);
    }   

    new_part->graph->runSlam(); 

    map<int, SlamConstraint *>::iterator c_it;

    //create a new set of constraints that match the old graph
    for ( c_it= graph->slam_constraints.begin() ; c_it != graph->slam_constraints.end(); c_it++ ){
        //check if there is a constraint added with the same id - otherwise add it 
        //skip the incremental ones
        //SlamConstraint *constraint  = (SlamConstraint *) c_it->second;
        SlamConstraint *edge = (SlamConstraint *) c_it->second;
        if(edge->type == SLAM_GRAPH_EDGE_T_TYPE_SM_INC || edge->type == SLAM_GRAPH_EDGE_T_TYPE_ODOM_INC){
            if(verbose)
                fprintf(stderr, "Incremental constraint : %d - %d => already added - skipping : %d\n", 
                        (int) edge->node1->id, (int) edge->node2->id, edge->type);
            continue;
        }
        else {
            //add the constraint 
            if(verbose)
                fprintf(stderr, "Adding Loop closure constraint from the old graph : %d - %d\n", 
                        (int) edge->node1->id, (int) edge->node2->id);

            SlamNode *node1 = new_part->graph->getSlamNode(edge->node1->id);
            SlamNode *node2 = new_part->graph->getSlamNode(edge->node2->id);
            SlamNode *actualnode1 = new_part->graph->getSlamNode(edge->actualnode1->id);
            SlamNode *actualnode2 = new_part->graph->getSlamNode(edge->actualnode2->id);
            SlamConstraint* new_ct  = new_part->graph->addConstraint(node1, node2, actualnode1, actualnode2, edge->transform, 
                                                                     edge->noise, edge->hitpct, edge->type , edge->status);
            new_ct->pofz = edge->pofz;
        }
    }
    
    for(int j=0; j< new_part->graph->slam_constraints.size(); j++){
        SlamConstraint *constr = new_part->graph->slam_constraints.at(j);
        constr->processed = 1;
    }
    
    new_part->graph->runSlam();    
    new_part->weight = weight;

    return new_part;
}

void SlamParticle::printEdges(){
   fprintf(stderr, "Edge matrix\n");
   fprintf(stderr, "N: ");
   for(int i=0; i<graph->slam_nodes.size(); i++)
      fprintf(stderr, "\t%d",i);
   //fprintf(stderr, "\n");
   for(int i=0; i<graph->slam_nodes.size(); i++){
      fprintf(stderr, "\n%d",i);
      for(int j=0; j<graph->slam_nodes.size(); j++){
         bool print = false;
         for(int e=0; e<graph->slam_constraints.size(); e++){
            if(graph->slam_constraints.at(e)->node1->id == graph->slam_nodes.at(i)->id){
               if(graph->slam_constraints.at(e)->node2->id == graph->slam_nodes.at(j)->id){
                  if(graph->slam_constraints.at(e)->type == SLAM_GRAPH_EDGE_T_TYPE_LANGUAGE)
                     fprintf(stderr, "\tL");
                  else if(graph->slam_constraints.at(e)->type == SLAM_GRAPH_EDGE_T_TYPE_SM_LC)
                     fprintf(stderr, "\tS");
                  else
                     fprintf(stderr, "\tE");
                  print = true;
               }
               //else{
               //fprintf(stderr, "\t");
               //}
            }
            //else{
            //   fprintf(stderr, "\t");
            //}
         }
         if(!print)
            fprintf(stderr, "\t");
      }
   }
   fprintf(stderr, "\n");
}

BotTrans SlamParticle::getTransform(SlamNode *basic_nd, SlamNode *graph_nd){
    Pose2d nd_new = graph_nd->getPose();
    Pose2d nd_new_basic = basic_nd->getPose();
 
    //node in particle frame
    BotTrans nd_to_particle_frame;
    nd_to_particle_frame.trans_vec[0] = nd_new.x();
    nd_to_particle_frame.trans_vec[1] = nd_new.y();
    nd_to_particle_frame.trans_vec[2] = 0.0;
    double rpy_b_to_pf[3] = { 0, 0, nd_new.t() };
    bot_roll_pitch_yaw_to_quat(rpy_b_to_pf, nd_to_particle_frame.rot_quat);
    
    //node2 in the base map's frame (needed since we use the base map to build the region 
    BotTrans nd_to_base_frame;
    nd_to_base_frame.trans_vec[0] = nd_new_basic.x();
    nd_to_base_frame.trans_vec[1] = nd_new_basic.y();
    nd_to_base_frame.trans_vec[2] = 0.0;
    double rpy_b_to_bf[3] = { 0, 0, nd_new_basic.t() };
    bot_roll_pitch_yaw_to_quat(rpy_b_to_bf, nd_to_base_frame.rot_quat);

    bot_trans_invert(&nd_to_base_frame);

    BotTrans base_frame_to_nd = nd_to_base_frame;

    //transform base map to particle's map frame - according to nd2 
    BotTrans base_frame_to_particle_frame;
    bot_trans_apply_trans_to(&nd_to_particle_frame, &base_frame_to_nd, &base_frame_to_particle_frame);
    
    return base_frame_to_particle_frame;
}

BotTrans SlamParticle::getTransformPrior(SlamNode *basic_nd, SlamNode *graph_nd_actual, SlamNode *graph_nd_proposed){
    Pose2d nd_new_actual = graph_nd_actual->getPose();
    Pose2d nd_new_proposed = graph_nd_proposed->getPose();
    Pose2d nd_new_basic = basic_nd->getPose();
 
    //node in particle frame
    BotTrans nd_to_particle_frame;
    nd_to_particle_frame.trans_vec[0] = nd_new_proposed.x();
    nd_to_particle_frame.trans_vec[1] = nd_new_proposed.y();
    nd_to_particle_frame.trans_vec[2] = 0.0;
    double rpy_b_to_pf[3] = { 0, 0, nd_new_actual.t() };
    bot_roll_pitch_yaw_to_quat(rpy_b_to_pf, nd_to_particle_frame.rot_quat);
    
    //node2 in the base map's frame (needed since we use the base map to build the region 
    BotTrans nd_to_base_frame;
    nd_to_base_frame.trans_vec[0] = nd_new_basic.x();
    nd_to_base_frame.trans_vec[1] = nd_new_basic.y();
    nd_to_base_frame.trans_vec[2] = 0.0;
    double rpy_b_to_bf[3] = { 0, 0, nd_new_basic.t() };
    bot_roll_pitch_yaw_to_quat(rpy_b_to_bf, nd_to_base_frame.rot_quat);

    bot_trans_invert(&nd_to_base_frame);

    BotTrans base_frame_to_nd = nd_to_base_frame;

    //transform base map to particle's map frame - according to nd2 
    BotTrans base_frame_to_particle_frame;
    bot_trans_apply_trans_to(&nd_to_particle_frame, &base_frame_to_nd, &base_frame_to_particle_frame);
    
    return base_frame_to_particle_frame;
}

Scan * SlamParticle::getRegionFromTransform(SlamGraph *basic_graph, int start_ind, int end_ind, int region_supernode_id, BotTrans base_frame_to_particle_frame, int use_only_region = 1){
    Scan * scan = new Scan();
    
    smPoint * points = NULL;
    double pBody[3] = { 0, 0, 0 };
    double pLocal[3];
    
    for(int k = start_ind; k <= end_ind; k++){       
        if(use_only_region && graph->getSlamNode(k)->parent_supernode != region_supernode_id)
            continue;
        
        SlamNode *nd_basic = basic_graph->getSlamNode(k);
         
        NodeScan *pose_for_points = nd_basic->slam_pose;

        points = (smPoint *) realloc(points, (scan->numPoints + pose_for_points->scan->numPoints) * sizeof(smPoint));
        
        Pose2d value_basic = nd_basic->getPose();

        BotTrans nd_curr_to_base_frame;
        nd_curr_to_base_frame.trans_vec[0] = value_basic.x();
        nd_curr_to_base_frame.trans_vec[1] = value_basic.y();
        nd_curr_to_base_frame.trans_vec[2] = 0.0;
        double rpy[3] = { 0, 0, value_basic.t() };
        bot_roll_pitch_yaw_to_quat(rpy, nd_curr_to_base_frame.rot_quat);
            
        //now we want the body to particle frame 
        BotTrans nd_curr_to_particle_frame;
        bot_trans_apply_trans_to(&base_frame_to_particle_frame, &nd_curr_to_base_frame, &nd_curr_to_particle_frame);             
        for (unsigned i = 0; i < pose_for_points->scan->numPoints; i++) {
            pBody[0] = pose_for_points->scan->points[i].x;
            pBody[1] = pose_for_points->scan->points[i].y;
            bot_trans_apply_vec(&nd_curr_to_particle_frame, pBody, pLocal);
            points[i+ scan->numPoints].x = pLocal[0];
            points[i+ scan->numPoints].y = pLocal[1];
        }
        scan->numPoints += pose_for_points->scan->numPoints;        
    }
    
    scan->ppoints = (smPoint *) malloc(scan->numPoints * sizeof(smPoint));
    scan->points = points;
    return scan;
}

//get the points in the frame of the supernode 
pointlist2d_t * SlamParticle::getRegionPointsFromTransformSuperNode(SlamGraph *basic_graph, int start_ind, int end_ind, int region_supernode_id, int use_only_region = 1){
    double pBody[3] = { 0, 0, 0 };
    double pLocal[3];

    int no_points = 0;

    SlamNode *super_node = basic_graph->getSlamNode(region_supernode_id);

    BotTrans sn_to_base_frame = getBotTransFromPose(super_node->getPose());
    BotTrans base_frame_to_sn = sn_to_base_frame;
    bot_trans_invert(&base_frame_to_sn);
    
    for(int k = start_ind; k <= end_ind; k++){       
        if(use_only_region && graph->getSlamNode(k)->parent_supernode != region_supernode_id)
            continue;
        
        SlamNode *nd_basic = basic_graph->getSlamNode(k);
         
        NodeScan *pose_for_points = nd_basic->slam_pose;

        no_points += pose_for_points->scan->numPoints; 
    }
    
    pointlist2d_t *region = pointlist2d_new(no_points);

    int c = 0;    

    for(int k = start_ind; k <= end_ind; k++){       
        if(use_only_region && graph->getSlamNode(k)->parent_supernode != region_supernode_id)
            continue;
        
        SlamNode *nd_basic = basic_graph->getSlamNode(k);
         
        NodeScan *pose_for_points = nd_basic->slam_pose;

        Pose2d value_basic = nd_basic->getPose();

        BotTrans nd_curr_to_base_frame = getBotTransFromPose(value_basic);

        BotTrans nd_curr_to_sn; 
        
        bot_trans_apply_trans_to(&base_frame_to_sn, &nd_curr_to_base_frame, &nd_curr_to_sn);

        for (unsigned i = 0; i < pose_for_points->scan->numPoints; i++) {
            pBody[0] = pose_for_points->scan->points[i].x;
            pBody[1] = pose_for_points->scan->points[i].y;
            bot_trans_apply_vec(&nd_curr_to_sn, pBody, pLocal);
            region->points[c].x = pLocal[0];
            region->points[c].y = pLocal[1];
            c++; 
        }
    }

    return region;
}

pointlist2d_t * SlamParticle::getRegionPointsFromTransform(SlamGraph *basic_graph, int start_ind, int end_ind, int region_supernode_id, BotTrans base_frame_to_particle_frame, int use_only_region = 1){
    double pBody[3] = { 0, 0, 0 };
    double pLocal[3];

    int no_points = 0;
    
    for(int k = start_ind; k <= end_ind; k++){       
        if(use_only_region && graph->getSlamNode(k)->parent_supernode != region_supernode_id)
            continue;
        
        SlamNode *nd_basic = basic_graph->getSlamNode(k);
         
        NodeScan *pose_for_points = nd_basic->slam_pose;

        no_points += pose_for_points->scan->numPoints; 
    }
    
    pointlist2d_t *region = pointlist2d_new(no_points);

    int c = 0;    

    for(int k = start_ind; k <= end_ind; k++){       
        if(use_only_region && graph->getSlamNode(k)->parent_supernode != region_supernode_id)
            continue;
        
        SlamNode *nd_basic = basic_graph->getSlamNode(k);
         
        NodeScan *pose_for_points = nd_basic->slam_pose;

        Pose2d value_basic = nd_basic->getPose();

        BotTrans nd_curr_to_base_frame;
        nd_curr_to_base_frame.trans_vec[0] = value_basic.x();
        nd_curr_to_base_frame.trans_vec[1] = value_basic.y();
        nd_curr_to_base_frame.trans_vec[2] = 0.0;
        double rpy[3] = { 0, 0, value_basic.t() };
        bot_roll_pitch_yaw_to_quat(rpy, nd_curr_to_base_frame.rot_quat);
            
        //now we want the body to particle frame 
        BotTrans nd_curr_to_particle_frame;
        bot_trans_apply_trans_to(&base_frame_to_particle_frame, &nd_curr_to_base_frame, &nd_curr_to_particle_frame);             
        for (unsigned i = 0; i < pose_for_points->scan->numPoints; i++) {
            pBody[0] = pose_for_points->scan->points[i].x;
            pBody[1] = pose_for_points->scan->points[i].y;
            bot_trans_apply_vec(&nd_curr_to_particle_frame, pBody, pLocal);
            region->points[c].x = pLocal[0];
            region->points[c].y = pLocal[1];
            c++; 
        }
    }

    return region;
}

//to do with the scanmatching 
void SlamParticle::addScansToScanMatcher(ScanMatcher *_sm, SlamGraph *basic_graph, int start_ind, int end_ind, int region_supernode_id, int match_supernode_id, BotTrans base_frame_to_particle_frame){
    for(int k = start_ind; k <= end_ind; k++){       
        BotTrans bodyToLocal;
        
        double pBody[3] = { 0, 0, 0 };
        double pLocal[3];
        
        //lets only use the scanmatch region if told 
        if(scanmatchOnlyRegion && graph->getSlamNode(k)->parent_supernode != region_supernode_id)
            continue;

        if(k == match_supernode_id)
            continue;
        
        SlamNode *nd_basic = basic_graph->getSlamNode(k);

        if(graph->getSlamNode(k) == NULL)
            break;

        if(nd_basic==NULL)
            break;
        
        NodeScan *pose_for_points = nd_basic->slam_pose;

        Pose2d value_basic = nd_basic->getPose();

        BotTrans nd_curr_to_base_frame;
        nd_curr_to_base_frame.trans_vec[0] = value_basic.x();
        nd_curr_to_base_frame.trans_vec[1] = value_basic.y();
        nd_curr_to_base_frame.trans_vec[2] = 0.0;
        double rpy[3] = { 0, 0, value_basic.t() };
        bot_roll_pitch_yaw_to_quat(rpy, nd_curr_to_base_frame.rot_quat);
            
        //now we want the body to particle frame 
        BotTrans nd_curr_to_particle_frame;
        bot_trans_apply_trans_to(&base_frame_to_particle_frame, &nd_curr_to_base_frame, &nd_curr_to_particle_frame);                       
        
        ScanTransform T;
        T.x = nd_curr_to_particle_frame.trans_vec[0];
        T.y = nd_curr_to_particle_frame.trans_vec[1];

        double rpy_to_frame[3];
        bot_quat_to_roll_pitch_yaw(nd_curr_to_particle_frame.rot_quat, rpy_to_frame);
        T.theta = rpy_to_frame[2];

        // applyTransform copies everything from T to pose_for_points->scan->T
        // so we should populate all of T first
        T.score = pose_for_points->scan->T.score;
        T.hits = pose_for_points->scan->T.hits;
        for (int i=0; i<9; i++)
            T.sigma[i] = pose_for_points->scan->T.sigma[i];


        
        pose_for_points->scan->applyTransform(T);
   
        //add it to the low res one also 
        _sm->addScan(pose_for_points->scan, false);
    }    
    //tell the scanmatcher to build the map 
    _sm->addScan(NULL, true);     
}

//find closest nodes 
int SlamParticle::getClosestNodes(int sn_id_1, int sn_id_2, int *cl_ind_1, int *cl_ind_2, double *minimum_dist){
    //fprintf(stderr, "Looking for closest node : %d, %d\n", sn_id_1, sn_id_2);
    int path_size = SUPERNODE_SEARCH_DISTANCE;
    int max_id = graph->slam_node_list.size()-1;
    int start_ind_1 = fmax(0, sn_id_1 - path_size);
    int end_ind_1 = fmin(sn_id_1 + path_size, max_id);

    int start_ind_2 = fmax(0, sn_id_2 - path_size);
    int end_ind_2 = fmin( sn_id_2 + path_size, max_id);
    
    double min_dist = 1000;
    //search near the region - and find the closest two nodes 
    for(int i = start_ind_1 ; i <= end_ind_1; i++){         
        SlamNode *nd1 = graph->getSlamNode(i);
        if(nd1 == NULL)
            continue;
        
        NodeScan *pose_1 = nd1->slam_pose;

        Pose2d p1 = nd1->getPose();

        if(nd1->parent_supernode != sn_id_1)
            continue;
       
        //search near the region - and find the closest two nodes 
        for(int j = start_ind_2 ; j <= end_ind_2; j++){         
            SlamNode *nd2 = graph->getSlamNode(j);
            if(nd2 == NULL)
                continue;

            if(fabs(nd1->id - nd2->id) <=1)
                continue;

            NodeScan *pose_2 = nd2->slam_pose;

            
            if(nd2->parent_supernode != sn_id_2)
                continue;

            Pose2d delta = nd2->getPose().ominus(p1);
            double dist = hypot(delta.x(), delta.y());

            if(dist < min_dist){
                min_dist = dist;
                *cl_ind_1 = i;
                *cl_ind_2 = j;
            }            
        }          
    }

    *minimum_dist = min_dist;
    if(min_dist < max_dist_for_lc) 
        return 1;
    return 0;
}

void SlamParticle::drawBasicMapInParticleFrame(SlamGraph *basic_graph, int valid_supernode1, int valid_supernode2, 
                                               int start_ind, int end_ind, BotTrans base_frame_to_particle_frame){
    if(!draw_scanmatch || lcmgl_sm_basic == NULL)
        return;

    bot_lcmgl_t *lcmgl = lcmgl_sm_basic;

    bot_lcmgl_point_size(lcmgl, 2);
    bot_lcmgl_color3f(lcmgl, 0, 0.5, 0.5); 
    
    for(int k = start_ind; k <= end_ind; k++){       
        
        BotTrans bodyToLocal;
            
        double pBody[3] = { 0, 0, 0 };
        double pLocal[3];
            
        //lets only use the local region 
        if(scanmatchOnlyRegion && graph->getSlamNode(k)->parent_supernode != valid_supernode2)
            continue;
                        
        if(k == valid_supernode1)
            continue;
            
        SlamNode *nd_basic = basic_graph->getSlamNode(k);
            
        if(graph->getSlamNode(k) == NULL)
            break;
            
        if(nd_basic==NULL)
            break;
            
        NodeScan *pose_for_points = nd_basic->slam_pose;
            
        Pose2d value_basic = nd_basic->getPose();
            
        BotTrans nd_curr_to_base_frame;
        nd_curr_to_base_frame.trans_vec[0] = value_basic.x();
        nd_curr_to_base_frame.trans_vec[1] = value_basic.y();
        nd_curr_to_base_frame.trans_vec[2] = 0.0;
        double rpy[3] = { 0, 0, value_basic.t() };
        bot_roll_pitch_yaw_to_quat(rpy, nd_curr_to_base_frame.rot_quat);
            
        //now we want the body to particle frame 
        BotTrans nd_curr_to_particle_frame;
        bot_trans_apply_trans_to(&base_frame_to_particle_frame, &nd_curr_to_base_frame, &nd_curr_to_particle_frame);                       
        bot_lcmgl_begin(lcmgl, GL_POINTS);
        for (unsigned i = 0; i < pose_for_points->scan->numPoints; i++) {        
            pBody[0] = pose_for_points->scan->points[i].x;
            pBody[1] = pose_for_points->scan->points[i].y;
            bot_trans_apply_vec(&nd_curr_to_particle_frame, pBody, pLocal);
            bot_lcmgl_vertex3f(lcmgl, pLocal[0], pLocal[1], pLocal[2]);
        }
        bot_lcmgl_end(lcmgl);
    }
    bot_lcmgl_switch_buffer(lcmgl);
}

void SlamParticle::drawScan(bot_lcmgl_t *lcmgl, NodeScan *pose_to_match, ScanTransform T, double color[3]){
    if(!draw_scanmatch || lcmgl == NULL)
        return;

    if(pose_to_match == NULL){
        bot_lcmgl_switch_buffer(lcmgl);
    }

    BotTrans bodyToLocal;
    bot_lcmgl_color3f(lcmgl, color[0], color[1], color[2]); 
    double pBody[3] = { 0, 0, 0 };
    double pLocal[3];
    bot_lcmgl_point_size(lcmgl, 2);
    bot_lcmgl_begin(lcmgl, GL_POINTS);
        
    bodyToLocal.trans_vec[0] = T.x;
    bodyToLocal.trans_vec[1] = T.y;
    bodyToLocal.trans_vec[2] = 0.0;
    double rpy[3] = { 0, 0, T.theta};
    bot_roll_pitch_yaw_to_quat(rpy, bodyToLocal.rot_quat);
        
    for (unsigned i = 0; i < pose_to_match->scan->numPoints; i++) {        
        pBody[0] = pose_to_match->scan->points[i].x;
        pBody[1] = pose_to_match->scan->points[i].y;
        bot_trans_apply_vec(&bodyToLocal, pBody, pLocal);
        bot_lcmgl_vertex3f(lcmgl, pLocal[0], pLocal[1], pLocal[2]);
    }
    bot_lcmgl_end(lcmgl);
    bot_lcmgl_switch_buffer(lcmgl);
}

void SlamParticle::drawPoints(bot_lcmgl_t *lcmgl, pointlist2d_t *list, ScanTransform *T, double color[3], int transform){
    if(lcmgl == NULL)
        return;

    BotTrans bodyToLocal;
    bot_lcmgl_color3f(lcmgl, color[0], color[1], color[2]); 
    double pBody[3] = { 0, 0, 0 };
    double pLocal[3];
    bot_lcmgl_point_size(lcmgl, 2);
    bot_lcmgl_begin(lcmgl, GL_POINTS);

    if(transform){
        bodyToLocal.trans_vec[0] = T->x;
        bodyToLocal.trans_vec[1] = T->y;
        bodyToLocal.trans_vec[2] = 0.0;
        double rpy[3] = { 0, 0, T->theta};
        bot_roll_pitch_yaw_to_quat(rpy, bodyToLocal.rot_quat);
    }

    for (unsigned i = 0; i < list->npoints; i++) {        
        pBody[0] = list->points[i].x;
        pBody[1] = list->points[i].y;
        if(transform){
            bot_trans_apply_vec(&bodyToLocal, pBody, pLocal);
            bot_lcmgl_vertex3f(lcmgl, pLocal[0], pLocal[1], pLocal[2]);
        }
        else{
            bot_lcmgl_vertex3f(lcmgl, pBody[0], pBody[1], pBody[2]);
        }
    }
    bot_lcmgl_end(lcmgl);
    bot_lcmgl_switch_buffer(lcmgl);
}


// Scanmatcher used for distance-based loop closure edges
//any node before that supernode and after the previous one is considered to belong that supernode 
ScanTransform SlamParticle::doScanMatch(SlamGraph *basic_graph, int64_t node_id_1, int64_t node_id_2, Pose2d *transf, 
                                        Noise *cov_hc, int *accepted, int64_t *used_ind_1, int64_t *used_ind_2, 
                                        double *hit_percentage){
    if(verbose)
        fprintf(stderr, "+++++++++++ Called loop closure Scanmatch ++++++++\n");
    
    int max_id = graph->slam_node_list.size()-1;
    //use the basic graph for the local region
    int path_size = SUPERNODE_SEARCH_DISTANCE;
    //need to add the neighbours
    sm_tictoc("loopClosureSM");

    int start_ind_1 = fmax(0, node_id_1 - path_size);
    int end_ind_1 = fmin(node_id_1 + path_size, max_id);

    int start_ind_2 = fmax(0, node_id_2 - path_size);
    int end_ind_2 = fmin(node_id_2 + path_size, max_id);
    
    double min_dist = 1000;
    int min_ind_1 = node_id_1;
    int min_ind_2 = node_id_2;

    int sm_count_1 = 0;
    int sm_count_2 = 0;

    int sm_bad_found =0;

    //search near the region - and find the closest two nodes 
    for(int i = start_ind_1 ; i <= end_ind_1; i++){         
        SlamNode *nd1 = graph->getSlamNode(i);
        if(nd1 == NULL)
            continue;

        if(nd1->slam_pose->inc_constraint_sm->hitpct >= inc_sm_acceptance_threshold){
            sm_count_1++;
        }
        else{
            sm_bad_found = 1;
        }
        
        NodeScan *pose_1 = nd1->slam_pose;

        Pose2d p1 = nd1->getPose();

        if(nd1->parent_supernode != node_id_1)
            continue;
       
        //search near the region - and find the closest two nodes 
        for(int j = start_ind_2 ; j <= end_ind_2; j++){         
            SlamNode *nd2 = graph->getSlamNode(j);
            if(nd2 == NULL)
                continue;

            if(nd2->slam_pose->inc_constraint_sm->hitpct >= inc_sm_acceptance_threshold){
                sm_count_2++;
            }
            else{
                sm_bad_found = 1;
            }
            
            NodeScan *pose_2 = nd2->slam_pose;
            
            if(nd2->parent_supernode != node_id_2)
                continue;

            Pose2d delta = nd2->getPose().ominus(p1);
            double dist = hypot(delta.x(), delta.y());
            if(dist < min_dist){
                min_dist = dist;
                min_ind_1 = i;
            }            
        }          
    }

    int outside = 1;
    if(sm_count_2 >=3 && sm_count_1 >=3  && sm_bad_found == 0){
        outside = 0; 
    }
    
    if(verbose)
        fprintf(stderr, "Req : %d - %d Found closest : %d-%d => %f\n", 
                (int) node_id_1, (int) node_id_2, min_ind_1, min_ind_2, min_dist);
    
    //reject if its too far 
    if(min_dist > max_dist_for_lc || outside){
        if(min_dist > max_dist_for_lc){
            if (verbose)
                fprintf(stderr, "Too far - rejecting scanmatch\n");
        }
        else{
            if (verbose)
                fprintf(stderr, "One or more outside regions in the proposed match - rejecting\n");
        }
        *accepted = 0;
        ScanTransform st;
        memset (&st, 0, sizeof(st));
        st.hits = 0;
        *hit_percentage = 0;
        return st;
    }
    
    //set the actual nodes matched 
    *used_ind_1 = min_ind_1;
    *used_ind_2 = min_ind_2;

    //local region is built around a region of the supernode
    int start_ind = fmax(0, min_ind_2 - path_size);
    int end_ind = fmin(min_ind_2 + path_size, max_id);

    if(verbose)
        fprintf(stderr, "Doing scanmatch (to) (from): Edges %d - %d => Actual => %d, %d Node range %d - %d\n", 
                (int) node_id_2, (int) node_id_1,
                min_ind_1, min_ind_2, 
                (int) start_ind, (int) end_ind);
    

    //the node around which the local region is built 
    SlamNode *nd_for_transforms = graph->getSlamNode(min_ind_2);
    SlamNode *nd_for_transforms_basic = basic_graph->getSlamNode(min_ind_2);
    
    Pose2d nd_new = nd_for_transforms->getPose();
    Pose2d nd_new_basic = nd_for_transforms_basic->getPose();

    BotTrans nd2_to_particle_frame;
    nd2_to_particle_frame.trans_vec[0] = nd_new.x();
    nd2_to_particle_frame.trans_vec[1] = nd_new.y();
    nd2_to_particle_frame.trans_vec[2] = 0.0;
    double rpy_b_to_pf[3] = { 0, 0, nd_new.t() };
    bot_roll_pitch_yaw_to_quat(rpy_b_to_pf, nd2_to_particle_frame.rot_quat);
    
    BotTrans base_frame_to_particle_frame = getTransform(nd_for_transforms_basic, 
                                                         nd_for_transforms);
                                                         
    //now we have the transform from going from the base_frame to particle frame 
    //we should find the body_to_base transform for each of the particles 
    //and derive the transform that takes us from the Body fram to Particle Frame 

    SlamNode *node1_min = graph->getSlamNode(min_ind_1);//slam_nodes.find(min_ind_1)->second;
    SlamNode *node2 = graph->getSlamNode(node_id_2);
    SlamNode *node1_basic = graph->getSlamNode(min_ind_1);
    NodeScan *pose_to_match = node1_basic->slam_pose;

    SlamNode *node1_act = graph->getSlamNode(node_id_1);

    //figure out the transform that will take us from a constraint in the closest node's frame 
    //to the node1 (the supernode) 

    BotTrans nd1_to_particle_frame;
    nd1_to_particle_frame.trans_vec[0] = node1_act->getPose().x();
    nd1_to_particle_frame.trans_vec[1] = node1_act->getPose().y();
    nd1_to_particle_frame.trans_vec[2] = 0.0;
    double rpy_nd1_to_pf[3] = { 0, 0, node1_act->getPose().t() };
    bot_roll_pitch_yaw_to_quat(rpy_nd1_to_pf, nd1_to_particle_frame.rot_quat);
    
    BotTrans nd_min_to_particle_frame;
    nd_min_to_particle_frame.trans_vec[0] = node1_min->getPose().x();
    nd_min_to_particle_frame.trans_vec[1] = node1_min->getPose().y();
    nd_min_to_particle_frame.trans_vec[2] = 0.0;
    double rpy_nd_min_to_pf[3] = { 0, 0, node1_min->getPose().t()};
    bot_roll_pitch_yaw_to_quat(rpy_nd_min_to_pf, nd_min_to_particle_frame.rot_quat);

    BotTrans particle_frame_to_nd_min = nd_min_to_particle_frame;
    
    bot_trans_invert(&particle_frame_to_nd_min);
    
    BotTrans particle_frame_to_nd2 = nd2_to_particle_frame;

    bot_trans_invert(&particle_frame_to_nd2);
    
    //transform node1 in nd_min's frame 
    BotTrans nd1_to_nd_min;
    bot_trans_apply_trans_to(&particle_frame_to_nd_min, &nd1_to_particle_frame,  &nd1_to_nd_min);

    Pose2d tocheck_value = node1_min->getPose();
    Pose2d close_value = node2->getPose();

    ScanTransform newT;
    newT.x = tocheck_value.x();
    newT.y = tocheck_value.y();
    newT.theta = tocheck_value.t();

    // applyTransform copies everything from newT to pose_to_match->scan->T
    // so we should populate all of newT first
    newT.score = pose_to_match->scan->T.score;
    newT.hits = pose_to_match->scan->T.hits;
    for (int i=0; i<9; i++)
        newT.sigma[i] = pose_to_match->scan->T.sigma[i];

    pose_to_match->scan->applyTransform(newT);

    ScanTransform lc_low_res;

    //build the map in the low resolution scanmatcher 
    if(useLowResPrior){
        
        addScansToScanMatcher(sm_low_res, basic_graph,start_ind,end_ind, node_id_2, node_id_1, base_frame_to_particle_frame);

        //this might need to be increased again ?? 
        //control the size of the scan match search area 
        //?? Is this too much?? - might need to add rigidity check
        lc_low_res = sm_low_res->gridMatch(pose_to_match->scan->points, pose_to_match->scan->numPoints,
                                           &pose_to_match->scan->T, 3.0, 3.0, M_PI / 6.0);
    
        double hitpct_low_res = lc_low_res.hits / (double) pose_to_match->scan->numPoints;
        if (verbose)
            fprintf(stderr, "Low res hit rate : %f\n", hitpct_low_res);

        if(draw_scanmatch){
            //remember to clear the scans before exiting 
            double color_low_prior[3] = {.0};
            color_low_prior[0] = .5;
            color_low_prior[2] = .5;
            
            //draw the high resolution prior
            drawScan(lcmgl_sm_result_low, pose_to_match, lc_low_res, color_low_prior);
        }

        //clear the low res scans 
        sm_low_res->clearScans(false);    
        
        //see if its somewhat good??
        if(hitpct_low_res < 0.5){
            fprintf(stderr, "We got poor scanmatch from the low res - declaring failure\n");
            *accepted = 0;
            sm->clearScans(false);    
            *hit_percentage = 0;
            return lc_low_res; 
        }        
    }

    //try it again with the same prior
    pose_to_match->scan->applyTransform(newT);

    //draw the region use to scanmatch with 
    drawBasicMapInParticleFrame(basic_graph, node_id_1, node_id_2,start_ind, end_ind, base_frame_to_particle_frame);
   
    if (verbose)
        fprintf(stderr, "\t SM - Doing gridmap sm - small window serach with high res sm\n");
    
    //first we search using the high resolution scanmatcher - using a small window 
    //if we directly use the low res sm results - this can mess us up in hallways 
    
    //add the relavent scans the the scanmatcher 
    addScansToScanMatcher(sm, basic_graph,start_ind,end_ind, node_id_2, node_id_1, base_frame_to_particle_frame);
    
    ScanTransform lc_r = sm->gridMatch(pose_to_match->scan->points, pose_to_match->scan->numPoints,
                                       &pose_to_match->scan->T, 1.0, 1.0, M_PI / 12.0);
    
    //remember to clear the scans before exiting 
    double color_prior[3] = {.0};
    color_prior[2] = 1.0;
    //draw the high resolution prior
    drawScan(lcmgl_sm_prior, pose_to_match, newT, color_prior);

    //this transform is in the wrong frame - need to transform to the correct frame

    double hitpct = lc_r.hits / (double) pose_to_match->scan->numPoints;
    if(verbose)
        fprintf(stderr, "Scan match - Small search : %f\n", hitpct);
    
    *accepted = 1;

    double accept_prob = sm_acceptance_threshold;
    
    if(hitpct < accept_prob){
        *accepted = 0;
    }

    if(*accepted && strictScanmatch){
        double sxx = lc_r.sigma[0];
        double sxy = lc_r.sigma[1];
        double syy = lc_r.sigma[4];
        double stt = lc_r.sigma[8];

        double sigma[9];
        memcpy(sigma, lc_r.sigma, 9 * sizeof(double));
        double evals[3] = { 0 };
        double evals_sq[9] = { 0 };
        double evecs[9] = { 0 };
        CvMat cv_sigma = cvMat(3, 3, CV_64FC1, sigma);
        CvMat cv_evals = cvMat(3, 1, CV_64FC1, evals);
        CvMat cv_evecs = cvMat(3, 3, CV_64FC1, evecs);
        cvEigenVV(&cv_sigma, &cv_evecs, &cv_evals);
        if (evals[0] < .005) {
            if(verbose)
                fprintf(stderr, "Accepted sxx=%f,sxy=%f,syy=%f,stt=%f, eigs=[%f %f %f]\n", sxx, sxy, syy, stt, evals[0], evals[1],
                        evals[2]);
            *accepted = 1;
        }
        else {
            if(verbose)
                fprintf(stderr, "REJECTED sxx=%f,sxy=%f,syy=%f,stt=%f, eigs=[%f %f %f]\n", sxx, sxy, syy, stt, evals[0],
                        evals[1], evals[2]);
            *accepted = 0;
        }    
    }

    bot_lcmgl_t *lcmgl = NULL;

    if(*accepted==1){
        //calculate the proper transform 
        
        double sxx = lc_r.sigma[0];
        double sxy = lc_r.sigma[1];
        double syy = lc_r.sigma[4];
        double stt = lc_r.sigma[8];
    
        BotTrans nd_min_sm_to_particle_frame;
        nd_min_sm_to_particle_frame.trans_vec[0] = lc_r.x; 
        nd_min_sm_to_particle_frame.trans_vec[1] = lc_r.y; 
        nd_min_sm_to_particle_frame.trans_vec[2] = 0.0;
        double rpy_nd_min_sm_to_pf[3] = { 0, 0, lc_r.theta}; 
        bot_roll_pitch_yaw_to_quat(rpy_nd_min_sm_to_pf, nd_min_sm_to_particle_frame.rot_quat);

        //transform that scanmatch gives us 
        BotTrans nd_min_to_nd2;
        bot_trans_apply_trans_to(&particle_frame_to_nd2, &nd_min_sm_to_particle_frame, &nd_min_to_nd2);

        //based on Scanmatch
        BotTrans nd_1_to_nd2;
        bot_trans_apply_trans_to(&nd_min_to_nd2, &nd1_to_nd_min, &nd_1_to_nd2);

        double rpy_transform[3];
        bot_quat_to_roll_pitch_yaw(nd_1_to_nd2.rot_quat, rpy_transform);

        *transf = Pose2d(nd_1_to_nd2.trans_vec[0], nd_1_to_nd2.trans_vec[1], rpy_transform[2]);


        double Rcov[9];
        sm_rotateCov2D(lc_r.sigma, lc_r.theta, Rcov);
        Matrix3d cv_sm(Rcov);
            
        if(useSMCov){
            *cov_hc = Covariance(cv_sm);
        }
        else{
            double cov_hardcode[9] = { .0002, 0, 0, 0, .0002, 0, 0, 0, .0001 };
            Matrix3d cv_hc(cov_hardcode);
            *cov_hc = Covariance(cv_hc);
        }        

        //draw the scanmatch result 
        if(draw_scanmatch){
            double color[3] = {.0};
            if(*accepted){
                color[1] = 1.0;
                drawScan(lcmgl_sm_result, pose_to_match, lc_r, color);
            }
            else{
                color[0] = 1.0;
                drawScan(lcmgl_sm_result, pose_to_match, lc_r, color);
            }
        }

        if(1 && lcmgl_loop_closures){
            lcmgl = lcmgl_loop_closures;
            if(*accepted){ 
                bot_lcmgl_color3f(lcmgl, 0.0, 1.0, 0.); 
            }
            else{
                bot_lcmgl_color3f(lcmgl, 0.0, 0.0, 0.); 
            }
            bot_lcmgl_line_width(lcmgl, 3);
            bot_lcmgl_begin(lcmgl, GL_LINES);
            bot_lcmgl_vertex3f(lcmgl, node1_min->getPose().x(), node1_min->getPose().y(), 0);
            bot_lcmgl_vertex3f(lcmgl, node2->getPose().x(), node2->getPose().y(), 0);
            bot_lcmgl_end(lcmgl);
        }
        
        //if we are returning here 
        //clear the scanmatch scans 
        sm->clearScans(false);

        *hit_percentage = hitpct;

        sm_tictoc("loopClosureSM");
        return lc_r;    
    }


    pose_to_match->scan->applyTransform(lc_low_res);

    //the small window scan didn't suceed - lets see if its worth doing a high res wide scan 
    //if this fails we will reject - otherwise we waste a lot of time to match bad scans 
   
    //gridmatch takes time 
    int64_t sm_start = bot_timestamp_now();
    
    if (verbose)
        fprintf(stderr, "\t SM - Doing gridmap sm - from low sm prior\n");
    
    //do refinement 
    lc_r = sm->gridMatch(pose_to_match->scan->points, pose_to_match->scan->numPoints,
                         &pose_to_match->scan->T, 1.0, 1.0, M_PI / 12.0);
    //&pose_to_match->scan->T, 1.0, 1.0, M_PI / 12.0);

    //this transform is in the wrong frame - need to transform to the correct frame

    hitpct = lc_r.hits / (double) pose_to_match->scan->numPoints;
    if(verbose)
        fprintf(stderr, "Scan match - Small search : %f\n", hitpct);
    
    *accepted = 1;

    if(hitpct < accept_prob){
        *accepted = 0;
    }

    sm->clearScans(false);

    if(*accepted && strictScanmatch){
        double sxx = lc_r.sigma[0];
        double sxy = lc_r.sigma[1];
        double syy = lc_r.sigma[4];
        double stt = lc_r.sigma[8];

        double sigma[9];
        memcpy(sigma, lc_r.sigma, 9 * sizeof(double));
        double evals[3] = { 0 };
        double evals_sq[9] = { 0 };
        double evecs[9] = { 0 };
        CvMat cv_sigma = cvMat(3, 3, CV_64FC1, sigma);
        CvMat cv_evals = cvMat(3, 1, CV_64FC1, evals);
        CvMat cv_evecs = cvMat(3, 3, CV_64FC1, evecs);
        cvEigenVV(&cv_sigma, &cv_evecs, &cv_evals);
        if (evals[0] < .005) {
            if(verbose)
                fprintf(stderr, "Accepted sxx=%f,sxy=%f,syy=%f,stt=%f, eigs=[%f %f %f]\n", sxx, sxy, syy, stt, evals[0], evals[1],
                        evals[2]);
            *accepted = 1;
        }
        else {
            if(verbose)
                fprintf(stderr, "REJECTED sxx=%f,sxy=%f,syy=%f,stt=%f, eigs=[%f %f %f]\n", sxx, sxy, syy, stt, evals[0],
                        evals[1], evals[2]);
            *accepted = 0;
        }    
    }

    if(draw_scanmatch){
        double color[3] = {.0};
        if(*accepted){
            color[1] = 1.0;
            drawScan(lcmgl_sm_result, pose_to_match, lc_r, color);
        }
        else{
            color[0] = 1.0;
            drawScan(lcmgl_sm_result, pose_to_match, lc_r, color);
        }
    }

    if(1 && lcmgl_loop_closures){
        lcmgl = lcmgl_loop_closures;
        if(*accepted){ 
            bot_lcmgl_color3f(lcmgl, 0.0, 1.0, 0.); 
        }
        else{
            bot_lcmgl_color3f(lcmgl, 0.0, 0.0, 0.); 
        }
        bot_lcmgl_line_width(lcmgl, 3);
        bot_lcmgl_begin(lcmgl, GL_LINES);
        bot_lcmgl_vertex3f(lcmgl, node1_min->getPose().x(), node1_min->getPose().y(), 0);
        bot_lcmgl_vertex3f(lcmgl, node2->getPose().x(), node2->getPose().y(), 0);
        bot_lcmgl_end(lcmgl);
    }

    //distance check 
    //not sure if we need to have this one 
    if (0 && *accepted) {
        Pose2d newToCheckValue(lc_r.x, lc_r.y, lc_r.theta);
        Pose2d delta = close_value.ominus(newToCheckValue);
        if (sqrt(sm_sq(delta.x()) + sm_sq(delta.y())) > 3) {
            if(verbose)
                fprintf(stderr, "REJECTED dx=%f, dy=%f, dt = %f\n", delta.x(), delta.y(), bot_to_degrees(delta.t()));
            *accepted = 0;
        }
    }

    int64_t sm_end = bot_timestamp_now();

    if (verbose)
        fprintf(stderr, "Scanmatch time : %f => Hits : %d Total : %d\n", (sm_end - sm_start) /1.0e6, 
                lc_r.hits, pose_to_match->scan->numPoints);
        
    double sxx = lc_r.sigma[0];
    double sxy = lc_r.sigma[1];
    double syy = lc_r.sigma[4];
    double stt = lc_r.sigma[8];
        
    if(verbose)
        fprintf(stderr, "Scanmatch time : %f => Hits : %d Total : %d => Sigma : %f, %f, %f, %f\n", (sm_end - sm_start) /1.0e6, 
                lc_r.hits, 
                pose_to_match->scan->numPoints, 
                sxx, sxy, syy, stt
                );
    
    Pose2d matchedPos(lc_r.x, lc_r.y, lc_r.theta);
    Pose2d closestPos = node2->getPose();
    //transf = new Pose2d(closestPos.ominus(matchedPos));
    
    BotTrans nd_min_sm_to_particle_frame;
    nd_min_sm_to_particle_frame.trans_vec[0] = lc_r.x; //node1->getPose().x();
    nd_min_sm_to_particle_frame.trans_vec[1] = lc_r.y; //node1->getPose().y();
    nd_min_sm_to_particle_frame.trans_vec[2] = 0.0;
    double rpy_nd_min_sm_to_pf[3] = { 0, 0, lc_r.theta}; //node1->getPose().t()};
    bot_roll_pitch_yaw_to_quat(rpy_nd_min_sm_to_pf, nd_min_sm_to_particle_frame.rot_quat);

    //transform that scanmatch gives us 
    BotTrans nd_min_to_nd2;
    bot_trans_apply_trans_to(&particle_frame_to_nd2, &nd_min_sm_to_particle_frame, &nd_min_to_nd2);

    //based on Scanmatch
    BotTrans nd_1_to_nd2;
    bot_trans_apply_trans_to(&nd_min_to_nd2, &nd1_to_nd_min, &nd_1_to_nd2);

    double rpy_transform[3];
    bot_quat_to_roll_pitch_yaw(nd_1_to_nd2.rot_quat, rpy_transform);

    *transf = Pose2d(nd_1_to_nd2.trans_vec[0], nd_1_to_nd2.trans_vec[1], rpy_transform[2]);
        
    double Rcov[9];
    sm_rotateCov2D(lc_r.sigma, lc_r.theta, Rcov);

    Matrix3d cv_sm(Rcov);
            
    if(useSMCov){
        *cov_hc = Covariance(cv_sm);
    }
    else{
        double cov_hardcode[9] = { .0002, 0, 0, 0, .0002, 0, 0, 0, .0001 };
        Matrix3d cv_hc(cov_hardcode);
        *cov_hc = Covariance(cv_hc);
    }

    sm_tictoc("loopClosureSM");
    *hit_percentage = hitpct;
    return lc_r;    
}

// Main scanmatch routine used for language-proposed edges
//any node before that supernode and after the previous one is considered to belong that supernode 
ScanTransform SlamParticle::doScanMatchLanguageICP(SlamGraph *basic_graph, int64_t node_id_1, int64_t node_id_2, Pose2d *transf, 
                                                   Noise *cov_hc, int *accepted, int64_t *used_ind_1, int64_t *used_ind_2, 
                                                   double *hit_percentage, int use_prior){
    fprintf(stderr, "+++++++++++ Called loop closure Scanmatch ++++++++\n");

    //get the points for the loop 

    int max_id = graph->slam_node_list.size()-1;
    //use the basic graph for the local region
    int path_size = 5;//SUPERNODE_SEARCH_DISTANCE;
    //need to add the neighbours
    sm_tictoc("loopClosureSM");

    int start_ind_1 = fmax(0, node_id_1 - path_size);
    int end_ind_1 = fmin(node_id_1 + path_size, max_id);

    int start_ind_2 = fmax(0, node_id_2 - path_size);
    int end_ind_2 = fmin(node_id_2 + path_size, max_id);


    int min_ind_1 = node_id_1;
    int min_ind_2 = node_id_2;
    
    //the node around which the local region is built 
    SlamNode *nd_2_for_transform = graph->getSlamNode(min_ind_2);
    SlamNode *nd_2_for_transform_basic = basic_graph->getSlamNode(min_ind_2);
    
    BotTrans base_frame_to_particle_frame_2 = getTransform(nd_2_for_transform_basic, 
                                                           nd_2_for_transform);

    //local region is built around a region of the supernode
    int start_ind_target = fmax(0, min_ind_2 - path_size);
    int end_ind_target = fmin(min_ind_2 + path_size, max_id);

    //not sure if we should use the region or the close by ones 
    
    pointlist2d_t *target = getRegionPointsFromTransformSuperNode(basic_graph, start_ind_target, end_ind_target, 
                                                                  nd_2_for_transform->parent_supernode, 
                                                                  1);

    /*BotTrans body_to_pf_2; 
      body_to_pf_2.trans_vec[]*/
    
    double color[3] = {0, 0, 1.0};
    
    //drawPoints(lcmgl_sm_graph, target, NULL, color, 0);
    
    //the node around which the local region is built 
    SlamNode *nd_1_for_transform = graph->getSlamNode(min_ind_1);
    SlamNode *nd_1_for_transform_basic = basic_graph->getSlamNode(min_ind_1);
    
    
    BotTrans base_frame_to_particle_frame_1 = getTransform(nd_1_for_transform_basic, 
                                                    nd_1_for_transform);

    BotTrans base_frame_to_particle_frame_1_prior = getTransformPrior(nd_1_for_transform_basic, 
                                                                      nd_1_for_transform, nd_2_for_transform);
    
    
    //local region is built around a region of the supernode
    int start_ind_match = fmax(0, min_ind_1 - path_size);
    int end_ind_match = fmin(min_ind_1 + path_size, max_id);

    pointlist2d_t *match = NULL;
    //match needs to be the other region 
    //try the same scan with a transform 
    //this should scan perfectly if things are working ok 

    base_frame_to_particle_frame_2.trans_vec[0] += 10;
    base_frame_to_particle_frame_2.trans_vec[1] += 5;

    match = getRegionPointsFromTransformSuperNode(basic_graph, start_ind_match, end_ind_match, 
                                                  nd_1_for_transform->parent_supernode, 
                                                  1);

    int match_count = match->npoints;

    color[0] = 1;
    color[2] = 0;

    double f_score = 0; 
    int matched_points = 0; 
    double max_corr_dist = .2; 

    pointlist2d_t *result = NULL;

    int converged = 0; 
    double hit_rate = 0;
    int align_scans = 0;

    BotTrans sm_to_target;
    
    Pose2d nd2_pose = nd_2_for_transform->getPose();
    BotTrans target_to_pf = getBotTransFromPose(nd2_pose);

    Pose2d nd1_pose = nd_1_for_transform->getPose();
    BotTrans match_to_pf = getBotTransFromPose(nd1_pose);

    result = doScanMatchMRPTFull(target, 
                                 match, 
                                 target_to_pf, 
                                 match_to_pf, 
                                 &sm_to_target, 
                                 &f_score,
                                 lcmgl_sm_graph, 
                                 lcmgl_sm_prior, 
                                 lcmgl_sm_result,
                                 align_scans, 
                                 0.7,
                                 0);

    hit_rate =  f_score;

    fprintf(stderr, "\n\nICP Solution => Hit rate : %f\n", 
            hit_rate);
    
    pointlist2d_free(match);
    pointlist2d_free(target);
    
    double hit_rate_threshold = lang_sm_acceptance_threshold;
    int matched_points_threshold = 400;
    double f_score_threshold = 0.5;//0.08;
    double hit_rate_threshold_2 = 0.3;
    double shift_dist_threshold = 80.0;//20.0;
    
    if(result != NULL && 
       (hit_rate > hit_rate_threshold)){
        *accepted = 1;
        *hit_percentage = hit_rate;

        fprintf(stderr, "ICP - Adding Language \n");
    }
    else{
        *accepted = 0;
        *hit_percentage = 0;
    }

    if(result != NULL){
        pointlist2d_free(result);
    }

    ScanTransform lc_r; 
    lc_r.x = sm_to_target.trans_vec[0];
    lc_r.y = sm_to_target.trans_vec[1];
    double rpy_r[3];
    bot_quat_to_roll_pitch_yaw(sm_to_target.rot_quat, rpy_r);
    lc_r.theta = rpy_r[2];
    
    BotTrans nd_1_sm_to_nd_2 = sm_to_target;

    double rpy_transform[3];
    bot_quat_to_roll_pitch_yaw(nd_1_sm_to_nd_2.rot_quat, rpy_transform);

    fprintf(stderr, "ICP Result => Constraint  : %f,%f,%f\n", nd_1_sm_to_nd_2.trans_vec[0], nd_1_sm_to_nd_2.trans_vec[1], rpy_transform[2]);

    *transf = Pose2d(nd_1_sm_to_nd_2.trans_vec[0], nd_1_sm_to_nd_2.trans_vec[1], rpy_transform[2]);
    
    double cov_hardcode[9] = { .0002, 0, 0, 0, .0002, 0, 0, 0, .0001 };
    Matrix3d cv_hc(cov_hardcode);
    *cov_hc = Covariance(cv_hc);

    lc_r.hits = 0;

    return lc_r;    
}



//any node before that supernode and after the previous one is considered to belong that supernode 
void SlamParticle::doScanMatchCheck(SlamGraph *basic_graph, int64_t node_id_1, int64_t node_id_2, 
                                    int *accepted, int64_t *used_ind_1, int64_t *used_ind_2){

    fprintf(stderr, "+++++++++++ Called loop closure Scanmatch check (checking to see if the scanmatch whould suceed) ++++++++\n");
    int path_size = SUPERNODE_SEARCH_DISTANCE;
    //find the previous and next supernodes 
    int prev_supernode_id = -1;
    int next_supernode_id = -1;
    int supernode_search_distance = SUPERNODE_SEARCH_DISTANCE;
    int max_id = graph->slam_node_list.size() -1;
    int start_super_ind_2 = fmax(0, node_id_2 - supernode_search_distance);
    //could be that this is not in the other graph 
    int end_super_ind_2 = fmin(node_id_2 + supernode_search_distance, max_id);
    
    int start_super_ind_1 = fmax(0, node_id_1 - supernode_search_distance);
    //could be that this is not in the other graph 
    int end_super_ind_1 = fmin(node_id_1 + supernode_search_distance, max_id);
    

    prev_supernode_id = graph->getPreviousSupernodeID(node_id_2, supernode_search_distance);
    next_supernode_id = graph->getNextSupernodeID(node_id_2, supernode_search_distance);

    int start_ind_1 = fmax(0, node_id_1 - path_size);
    int end_ind_1 = fmin(node_id_1 + path_size, max_id);

    int start_ind_2 = fmax(0, node_id_2 - path_size);
    int end_ind_2 = fmin(node_id_2 + path_size, max_id);
    
    double min_dist = 1000;
    int min_ind_1 = node_id_1;
    int min_ind_2 = node_id_2;


    //calculate the dist from node 1 
    SlamNode *nd1 = graph->getSlamNode(node_id_1);

    //fprintf(stderr, "Prev Supernode : %d\n" , prev_supernode_id);
    //fprintf(stderr, "Next Supernode : %d\n" , next_supernode_id);

    //search near the region - and find the closest two nodes 
    for(int i = start_ind_2 ; i <= end_ind_2; i++){         
        //its possible for this slam graph to not have the last nodes applied (the basic graph will have them all) 
        SlamNode *nd2 = graph->getSlamNode(i);
        if(nd2 == NULL)
            continue;
        
        if(i < 0 || i > max_id)
            break;
        
        NodeScan *pose_2 = nd2->slam_pose;

        int belongs_to_current = graph->belongsToCurrentSupernode(i, node_id_2, prev_supernode_id, next_supernode_id);
        //fprintf(stderr, "[%d] Belongs to Current [%d] Supernode : %d\n", 
        //i, (int) node_id_2, belongs_to_current);
        if(!belongs_to_current)
            continue;

        Pose2d delta = nd2->getPose();
        delta = delta.ominus(nd1->getPose());
        double dist = hypot(delta.x(), delta.y());
        if(dist < min_dist){
            min_dist = dist;
            min_ind_2 = i;
        }        
    }
    
    if(verbose)
        fprintf(stderr, "Req : %d - %d Found closest : %d-%d => %f\n", 
                (int) node_id_1, (int) node_id_2, min_ind_1, min_ind_2, min_dist);
    
    if(min_dist > max_dist_for_lc){
        if (verbose)
            fprintf(stderr, "Too far - rejecting scanmatch\n");
        *accepted = 0;
        return;
    }
    
    *used_ind_1 = min_ind_1;
    *used_ind_2 = min_ind_2;

    //local region is built over the the previous set of nodes 
    int start_ind = fmax(0, min_ind_2 - path_size);
    int end_ind = fmin(min_ind_2 + path_size, max_id);

    if(verbose)
        fprintf(stderr, "Doing scanmatch (to) (from): Edges %d - %d => Actual => %d, %d Node range %d - %d\n", 
                (int) node_id_2, (int) node_id_1,
                min_ind_1, min_ind_2, 
                (int) start_ind, (int) end_ind);
    

    //find the transfrom 
    SlamNode *nd_for_transforms = graph->getSlamNode(min_ind_2);
    SlamNode *nd_for_transforms_basic = basic_graph->getSlamNode(min_ind_2);
    
    Pose2d nd_new = nd_for_transforms->getPose();
    Pose2d nd_new_basic = nd_for_transforms_basic->getPose();
    
    NodeScan *pose_for_transforms = nd_for_transforms->slam_pose;
    
    bot_lcmgl_t *lcmgl = NULL;
    if(draw_scanmatch && lcmgl_sm_basic){
        lcmgl = lcmgl_sm_basic;
        bot_lcmgl_point_size(lcmgl, 2);
        bot_lcmgl_color3f(lcmgl, 0, 0.5, 0.5); 
    }

    BotTrans bodyToParticleFrame;
    bodyToParticleFrame.trans_vec[0] = nd_new.x();
    bodyToParticleFrame.trans_vec[1] = nd_new.y();
    bodyToParticleFrame.trans_vec[2] = 0.0;
    double rpy_b_to_pf[3] = { 0, 0, nd_new.t() };
    bot_roll_pitch_yaw_to_quat(rpy_b_to_pf, bodyToParticleFrame.rot_quat);

    BotTrans bodyToBaseFrame;
    bodyToBaseFrame.trans_vec[0] = nd_new_basic.x();
    bodyToBaseFrame.trans_vec[1] = nd_new_basic.y();
    bodyToBaseFrame.trans_vec[2] = 0.0;
    double rpy_b_to_bf[3] = { 0, 0, nd_new_basic.t() };
    bot_roll_pitch_yaw_to_quat(rpy_b_to_bf, bodyToBaseFrame.rot_quat);

    bot_trans_invert(&bodyToBaseFrame);

    BotTrans BaseFrameToParticleFrame;
    bot_trans_apply_trans_to(&bodyToParticleFrame, &bodyToBaseFrame, &BaseFrameToParticleFrame);
    
    //now we have the transform from going from the base_frame to particle frame 
    //we should find the body_to_base transform for each of the particles 
    //and derive the transform that takes us from the Body fram to Particle Frame 

    //we have found the closest node in the region using the given graph
    
    //put these points to some data structure 
    //and compare 
    //use a kd-tree??

    for(int k = start_ind; k <= end_ind; k++){       
        BotTrans bodyToLocal;
        
        double pBody[3] = { 0, 0, 0 };
        double pLocal[3];

        //lets only use the local region 
        if(scanmatchOnlyRegion && !graph->belongsToCurrentSupernode(k, node_id_2, prev_supernode_id, next_supernode_id))
            continue;

        if(k == node_id_1)
            continue;
        
        SlamNode *nd_basic = basic_graph->getSlamNode(k);

        if(graph->getSlamNode(k) == NULL)
            break;

        if(nd_basic==NULL)
            break;
        
        NodeScan *pose_for_points = nd_basic->slam_pose;

        Pose2d value_basic = nd_basic->getPose();

        BotTrans bodyCurrToBaseFrame;
        bodyCurrToBaseFrame.trans_vec[0] = value_basic.x();
        bodyCurrToBaseFrame.trans_vec[1] = value_basic.y();
        bodyCurrToBaseFrame.trans_vec[2] = 0.0;
        double rpy[3] = { 0, 0, value_basic.t() };
        bot_roll_pitch_yaw_to_quat(rpy, bodyCurrToBaseFrame.rot_quat);

        //now we want the body to particle frame 
        BotTrans bodyCurrToParticleFrame;
        bot_trans_apply_trans_to(&BaseFrameToParticleFrame, &bodyCurrToBaseFrame, &bodyCurrToParticleFrame);
        
        ScanTransform T;
        T.x = bodyCurrToParticleFrame.trans_vec[0];
        T.y = bodyCurrToParticleFrame.trans_vec[1];

        double rpy_to_frame[3];
        bot_quat_to_roll_pitch_yaw(bodyCurrToParticleFrame.rot_quat, rpy_to_frame);
        T.theta = rpy_to_frame[2];
        
    // applyTransform copies everything from T to pose_for_points->scan->T
    // so we should populate all of T first
    T.score = pose_for_points->scan->T.score;
    T.hits = pose_for_points->scan->T.hits;
    for (int i=0; i<9; i++)
      T.sigma[i] = pose_for_points->scan->T.sigma[i];


        pose_for_points->scan->applyTransform(T);

        if(draw_scanmatch && lcmgl != NULL){
            bot_lcmgl_begin(lcmgl, GL_POINTS);
            for (unsigned i = 0; i < pose_for_points->scan->numPoints; i++) {        
                pBody[0] = pose_for_points->scan->points[i].x;
                pBody[1] = pose_for_points->scan->points[i].y;
                bot_trans_apply_vec(&bodyCurrToParticleFrame, pBody, pLocal);
                bot_lcmgl_vertex3f(lcmgl, pLocal[0], pLocal[1], pLocal[2]);
            }
            bot_lcmgl_end(lcmgl);
        }

        sm->addScan(pose_for_points->scan, false);
    }

    if(draw_scanmatch){
        if(lcmgl){
            bot_lcmgl_switch_buffer(lcmgl);
        }
        
        lcmgl = lcmgl_sm_graph;
        bot_lcmgl_color3f(lcmgl, 1.0, 0.5, 0.); 
        bot_lcmgl_point_size(lcmgl, 2);
    }
    //we have found the closest node in the region using the given graph

    if(draw_scanmatch && lcmgl){
        for(int k = start_ind; k <= end_ind; k++){
            if(scanmatchOnlyRegion && !graph->belongsToCurrentSupernode(k, node_id_2, prev_supernode_id, next_supernode_id))
                continue;

            if(k == node_id_1)
                continue;

            //SlamNode *nd = graph->slam_nodes.find(k)->second;
            SlamNode *nd_basic = graph->getSlamNode(k);
            
            if(nd_basic == NULL)
                break;
            
            NodeScan *pose_for_points = nd_basic->slam_pose;
            
            Pose2d value = nd_basic->getPose();
            
            BotTrans bodyToLocal;
            
            double pBody[3] = { 0, 0, 0 };
            double pLocal[3];
            
            bot_lcmgl_begin(lcmgl, GL_POINTS);
            
            bodyToLocal.trans_vec[0] = value.x();
            bodyToLocal.trans_vec[1] = value.y();
            bodyToLocal.trans_vec[2] = 0.0;
            double rpy[3] = { 0, 0, value.t() };
            bot_roll_pitch_yaw_to_quat(rpy, bodyToLocal.rot_quat);
            
            for (unsigned i = 0; i < pose_for_points->scan->numPoints; i++) {        
                pBody[0] = pose_for_points->scan->points[i].x;
                pBody[1] = pose_for_points->scan->points[i].y;
                bot_trans_apply_vec(&bodyToLocal, pBody, pLocal);
                bot_lcmgl_vertex3f(lcmgl, pLocal[0], pLocal[1], pLocal[2]);
            }
            bot_lcmgl_end(lcmgl);
        }
        
        bot_lcmgl_switch_buffer(lcmgl);
    }

    //this tells it to rebuild the map - should be done after the scans are added 
    sm->addScan(NULL, true);      
    
    SlamNode *node1 = graph->slam_nodes.find(node_id_1)->second;
    SlamNode *node2 = graph->slam_nodes.find(node_id_2)->second;
    SlamNode *node1_basic = graph->slam_nodes.find(node_id_1)->second;
    NodeScan *pose_to_match = node1_basic->slam_pose;
    
    Pose2d tocheck_value = node1->getPose();
    Pose2d close_value = node2->getPose();

    ScanTransform newT;
    newT.x = tocheck_value.x();
    newT.y = tocheck_value.y();
    newT.theta = tocheck_value.t();

    // applyTransform copies everything from newT to pose_to_match->scan->T
    // so we should populate all of newT first
    newT.score = pose_to_match->scan->T.score;
    newT.hits = pose_to_match->scan->T.hits;
    for (int i=0; i<9; i++)
        newT.sigma[i] = pose_to_match->scan->T.sigma[i];

    pose_to_match->scan->applyTransform(newT);

    lcmgl = lcmgl_sm_prior;
    
    if(draw_scanmatch && lcmgl){
        BotTrans bodyToLocal;
        bot_lcmgl_color3f(lcmgl, 0.0, 0.0, 1.0); 
        double pBody[3] = { 0, 0, 0 };
        double pLocal[3];
        bot_lcmgl_point_size(lcmgl, 2);
        bot_lcmgl_begin(lcmgl, GL_POINTS);
        
        bodyToLocal.trans_vec[0] =  tocheck_value.x();
        bodyToLocal.trans_vec[1] = tocheck_value.y();
        bodyToLocal.trans_vec[2] = 0.0;
        double rpy[3] = { 0, 0, tocheck_value.t() };
        bot_roll_pitch_yaw_to_quat(rpy, bodyToLocal.rot_quat);

        for (unsigned i = 0; i < pose_to_match->scan->numPoints; i++) {        
            pBody[0] = pose_to_match->scan->points[i].x;
            pBody[1] = pose_to_match->scan->points[i].y;
            bot_trans_apply_vec(&bodyToLocal, pBody, pLocal);
            bot_lcmgl_vertex3f(lcmgl, pLocal[0], pLocal[1], pLocal[2]);
        }
        bot_lcmgl_end(lcmgl);
        bot_lcmgl_switch_buffer(lcmgl);
    }
    
    //gridmatch takes time 
    int64_t sm_start = bot_timestamp_now();
    
    //this might need to be increased again ?? 
    //control the size of the scan match search area 
    ScanTransform lc_r = sm->gridMatch(pose_to_match->scan->points, pose_to_match->scan->numPoints,
                                       &pose_to_match->scan->T, 0.1, 0.1, M_PI / 24.0);

    //this transform is in the wrong frame - need to transform to the correct frame

    double hitpct = lc_r.hits / (double) pose_to_match->scan->numPoints;
    if(verbose)
        fprintf(stderr, "Scan match - Small search : %f\n", hitpct);

    *accepted = 1;
    
    double accept_prob = sm_acceptance_threshold;
    
    if(hitpct < accept_prob){
        if(doWideMatchOnFail){
            fprintf(stderr, "Scanmatch failed - running a wider scan\n");
            
            //lc_r = sm->gridMatch(pose_to_match->scan->points, pose_to_match->scan->numPoints,
            //&pose_to_match->scan->T, 2.0, 2.0, M_PI / 6.0); 
        
            //clear the scans from the scanmatcher 
        
            hitpct = lc_r.hits / (double) pose_to_match->scan->numPoints;  
            if(verbose)
                fprintf(stderr, "Scan match - Large search : %f\n", hitpct);
        
            if(hitpct < accept_prob){
                *accepted = 1;
            }
        }
        *accepted = 0;
    }

    sm->clearScans(false);

    if(*accepted && strictScanmatch){
        double sxx = lc_r.sigma[0];
        double sxy = lc_r.sigma[1];
        double syy = lc_r.sigma[4];
        double stt = lc_r.sigma[8];

        double sigma[9];
        memcpy(sigma, lc_r.sigma, 9 * sizeof(double));
        double evals[3] = { 0 };
        double evals_sq[9] = { 0 };
        double evecs[9] = { 0 };
        CvMat cv_sigma = cvMat(3, 3, CV_64FC1, sigma);
        CvMat cv_evals = cvMat(3, 1, CV_64FC1, evals);
        CvMat cv_evecs = cvMat(3, 3, CV_64FC1, evecs);
        cvEigenVV(&cv_sigma, &cv_evecs, &cv_evals);
        if (evals[0] < .005) {
            if(verbose)
                fprintf(stderr, "Accepted sxx=%f,sxy=%f,syy=%f,stt=%f, eigs=[%f %f %f]\n", sxx, sxy, syy, stt, evals[0], evals[1],
                        evals[2]);
            *accepted = 1;
        }
        else {
            if(verbose)
                fprintf(stderr, "REJECTED sxx=%f,sxy=%f,syy=%f,stt=%f, eigs=[%f %f %f]\n", sxx, sxy, syy, stt, evals[0],
                        evals[1], evals[2]);
            *accepted = 0;
        }    
    }

    if(draw_scanmatch && lcmgl_sm_result) {
        lcmgl = lcmgl_sm_result;
        bot_lcmgl_point_size(lcmgl, 2);
        BotTrans bodyToLocal;
        if(*accepted){ 
            bot_lcmgl_color3f(lcmgl, 0.0, 1.0, 0.); 
        }
        else{
            bot_lcmgl_color3f(lcmgl, 1.0, 0.0, 0.); 
        }
        double pBody[3] = { 0, 0, 0 };
        double pLocal[3];
        bot_lcmgl_begin(lcmgl, GL_POINTS);
        
        bodyToLocal.trans_vec[0] =  lc_r.x;
        bodyToLocal.trans_vec[1] = lc_r.y;
        bodyToLocal.trans_vec[2] = 0.0;
        double rpy[3] = { 0, 0, lc_r.theta };
        bot_roll_pitch_yaw_to_quat(rpy, bodyToLocal.rot_quat);
        
        for (unsigned i = 0; i < pose_to_match->scan->numPoints; i++) {        
            pBody[0] = pose_to_match->scan->points[i].x;
            pBody[1] = pose_to_match->scan->points[i].y;
            bot_trans_apply_vec(&bodyToLocal, pBody, pLocal);
            bot_lcmgl_vertex3f(lcmgl, pLocal[0], pLocal[1], pLocal[2]);
        }
        bot_lcmgl_end(lcmgl);
        bot_lcmgl_switch_buffer(lcmgl);
    }

    if(1 && lcmgl_loop_closures){
        lcmgl = lcmgl_loop_closures;
        if(*accepted){ 
            bot_lcmgl_color3f(lcmgl, 0.0, 1.0, 0.); 
        }
        else{
            bot_lcmgl_color3f(lcmgl, 0.0, 0.0, 0.); 
        }
        bot_lcmgl_line_width(lcmgl, 3);
        bot_lcmgl_begin(lcmgl, GL_LINES);
        bot_lcmgl_vertex3f(lcmgl, node1->getPose().x(), node1->getPose().y(), 0);
        bot_lcmgl_vertex3f(lcmgl, node2->getPose().x(), node2->getPose().y(), 0);
        bot_lcmgl_end(lcmgl);
    }

    //distance check 
    //not sure if we need to have this one 
    if (0 && *accepted) {
        Pose2d newToCheckValue(lc_r.x, lc_r.y, lc_r.theta);
        Pose2d delta = close_value.ominus(newToCheckValue);
        if (sqrt(sm_sq(delta.x()) + sm_sq(delta.y())) > 3) {
            if(verbose)
                fprintf(stderr, "REJECTED dx=%f, dy=%f, dt = %f\n", delta.x(), delta.y(), bot_to_degrees(delta.t()));
            *accepted = 0;
        }
    }

    int64_t sm_end = bot_timestamp_now();

    if (verbose)
        fprintf(stderr, "Scanmatch time : %f => Hits : %d Total : %d\n", (sm_end - sm_start) /1.0e6, 
            lc_r.hits, pose_to_match->scan->numPoints);
        
    double sxx = lc_r.sigma[0];
    double sxy = lc_r.sigma[1];
    double syy = lc_r.sigma[4];
    double stt = lc_r.sigma[8];
        
    if(verbose)
        fprintf(stderr, "Scanmatch time : %f => Hits : %d Total : %d => Sigma : %f, %f, %f, %f\n", 
                (sm_end - sm_start) /1.0e6, lc_r.hits, pose_to_match->scan->numPoints, sxx, sxy, syy, stt);
}

double SlamParticle::getObservationProbabilityOld(MatrixXd cov_match_in_particle_frame, MatrixXd cov_querry_in_particle_frame, 
                                               double pMatch[2], double pQuery[2]){
    //these points need to be in the particle frame - not their body frame 

    //will this have the same issue as the edge creation stuff???
    //in which case - we should evaluvate the two transforms - not the distances 
    
    //i.e. (x_match-x_querry) , (y_match - y_querry)

    MatrixXd H(1,4);
    
    H.setZero();
    
    H(0,0) = 2 * (pMatch[0] - pQuery[0]);
    H(0,1) = 2 * (pMatch[1] - pQuery[1]);
    H(0,2) = -2 * (pMatch[0] - pQuery[0]);
    H(0,3) = -2 * (pMatch[1] - pQuery[1]);

    if(0){
        fprintf(stderr, "Diff : %f,%f Match : %f, %f => Querry : %f,%f\n", 
                pMatch[0] - pQuery[0], 
                pMatch[1] - pQuery[1], 
                pMatch[0], pMatch[1], 
                pQuery[0], pQuery[1]);
    }

    double mean[2] = {pMatch[0] - pQuery[0], pMatch[1] - pQuery[1]};

    //build the full covariance 
    MatrixXd point_cov(4,4);
    point_cov.setZero(4,4);
    for(int i=0; i < 2; i++){
        for(int j=0; j < 2; j++){
            point_cov(i,j) = cov_match_in_particle_frame(i,j);
        }
    }

    for(int i=0; i < 2; i++){
        for(int j=0; j < 2; j++){
            point_cov(i+2,j+2) = cov_querry_in_particle_frame(i,j);
        }
    }

    MatrixXd cov = H * point_cov * H.transpose();
        
    //evaluate the probability 
    double deter = cov.determinant();

    MatrixXd dx(1,1);
    dx(0,0) = pow(mean[0],2) + pow(mean[1],2);

    //cout << "Z Score : " << dx(0,0) << ", " << dx(0,1)  << endl;
    //cout << "Cov " << cov <<endl;
    
    MatrixXd mh_dist = dx.transpose() * cov.inverse() * dx;

    //cout << "MH dist : " << mh_dist(0,0) << "Det " << deter <<  endl;
    //applying to gaussian probability 
    double prob = exp(-mh_dist(0,0)/2)/ ((2 * M_PI) * pow(deter, 0.5));

    double full_prob = 0.5 * prob;



    return full_prob;
}

double SlamParticle::getObservationProbabilityComplex(MatrixXd qPoseMPoseCov, MatrixXd pQuerryCov, MatrixXd pMatchCov, 
                                 double qLaserPos[2], double mLaserPos[2], 
                                 Pose2d qPose, Pose2d mPose){

    MatrixXd H(1,10);

    double X1 = qPose.x();
    double Y1 = qPose.y();
    double T1 = qPose.t();

    double X2 = mPose.x();
    double Y2 = mPose.y();
    double T2 = mPose.t();
    
    double x1 = qLaserPos[0];
    double y1 = qLaserPos[1];

    double x2 = mLaserPos[0];
    double y2 = mLaserPos[1];

    double x_q = X1 + x1 * cos(T1) - y1 * sin(T1);
    double y_q = Y1 + x1 * sin(T1) + y1 * cos(T1);

    double x_m = X2 + x2 * cos(T1) - y2 * sin(T1);
    double y_m = Y2 + x2 * sin(T1) + y2 * cos(T1);

    double D = hypot((x_q - x_m), (y_q-y_m));
    if(D == 0) 
        D = 1.0e-5;
    
    double dX = (x_q - x_m)/D;
    double dY = (y_q - y_m)/D;

    H(0,0) = dX * cos(T1) + dY * sin(T1);
    H(0,1) = -dX * sin(T1) + dY * cos(T1);
    H(0,2) = dX;
    H(0,3) = dY; 
    H(0,4) = dX * (-x1* sin(T1) - y1 * cos(T1)) + dY * (x1 * cos(T1) - y1*sin(T1));
    H(0,5) = -dX;
    H(0,6) = -dY; 
    H(0,7) = -(dX * (-x2* sin(T2) - y2 * cos(T2)) + dY * (x2 * cos(T2) - y2*sin(T2)));
    H(0,8) = -(dX * cos(T2) + dY * sin(T2));
    H(0,9) = -(-dX * sin(T2) + dY * cos(T2));
    
    MatrixXd Cov(10,10);
    Cov.setZero(10,10);

    for(int i=0; i < 2; i++){
        for(int j=0; j < 2; j++){
            Cov(i, j) = pQuerryCov(i,j);
        }
    }
    
    for(int i=0; i < 6; i++){
        for(int j=0; j < 6; j++){
            Cov(i+2, j+2) = qPoseMPoseCov(i,j);
        }
    }
        
    for(int i=0; i < 2; i++){
        for(int j=0; j < 2; j++){
            Cov(i+8, j+8) = pMatchCov(i,j);
        }
    }

    //cout << "Input Cov :\n" << Cov << endl;
    //cout << "Jacobian : \n" << H << endl;
    
    MatrixXd R = H * Cov * H.transpose();

    //cout << "Result :" <<  R << endl;
    double full_prob = get_observation_probability(D, pow(R(0,0),0.5));

    //cout << "Prob : " << full_prob << endl;
    
    return full_prob;
}

double SlamParticle::getObservationProbability(MatrixXd cov_match_in_particle_frame, MatrixXd cov_querry_in_particle_frame, 
                                               double pMatch[2], double pQuery[2]){
    //these points need to be in the particle frame - not their body frame 

    //will this have the same issue as the edge creation stuff???
    //in which case - we should evaluvate the two transforms - not the distances 
    
    //i.e. (x_match-x_querry) , (y_match - y_querry)

    MatrixXd H(1,4);

    double d = hypot(pMatch[0] - pQuery[0], pMatch[1] - pQuery[1]);
   
    if(d==0)
        d = 0.0001;

    H(0,0) = (pMatch[0] - pQuery[0])/ d;
    H(0,1) = (pMatch[1] - pQuery[1])/d;
    H(0,2) = -(pMatch[0] - pQuery[0])/d;
    H(0,3) = -(pMatch[1] - pQuery[1])/d;

    if(0){
        fprintf(stderr, "Diff : %f,%f Match : %f, %f => Querry : %f,%f\n", 
                pMatch[0] - pQuery[0], 
                pMatch[1] - pQuery[1], 
                pMatch[0], pMatch[1], 
                pQuery[0], pQuery[1]);
    }

    double mean[2] = {pMatch[0] - pQuery[0], pMatch[1] - pQuery[1]};

    //build the full covariance 
    MatrixXd point_cov(4,4);
    point_cov.setZero(4,4);
    
    for(int i=0; i < 2; i++){
        for(int j=0; j < 2; j++){
            point_cov(i,j) = cov_match_in_particle_frame(i,j);
        }
    }

    for(int i=0; i < 2; i++){
        for(int j=0; j < 2; j++){
            point_cov(i+2,j+2) = cov_querry_in_particle_frame(i,j);
        }
    }

    MatrixXd cov = H * point_cov * H.transpose();
        
    //cout << "Mean : " << d << endl << "Stdev : " << pow(cov(0,0),0.5) << endl; 

    double full_prob = get_observation_probability(d, pow(cov(0,0),0.5));//get_observation_probability(d, pow(cov(0,0),0.5));

    //double full_prob = get_observation_probability(0.4, 0.4);

    //cout << "Full Prob : " << full_prob << endl;

    return full_prob; //1;//full_prob;
}

//use a fixed covariance function
double SlamParticle::getObservationProbabilityFixed(double pMatch[2], double pQuery[2]){
    double mean[2] = {pMatch[0] - pQuery[0], pMatch[1] - pQuery[1]};

    MatrixXd cov =  MatrixXd::Zero(2,2);
    cov(0,0) = 0.1;
    cov(0,1) = cov(1,0) = 0.02;
    cov(1,1) = 0.1;

    //evaluate the probability 
    double deter = cov.determinant();

    MatrixXd dx(2,1);
    dx(0,0) = (0-mean[0]);
    dx(1,0) = (0-mean[1]);

    //cout << "Z Score : " << dx(0,0) << ", " << dx(0,1)  << endl;
    //cout << "Cov " << cov <<endl;
    
    MatrixXd mh_dist = dx.transpose() * cov.inverse() * dx;

    //cout << "MH dist : " << mh_dist(0,0) << "Det " << deter <<  endl;
    //applying to gaussian probability 
    double prob = exp(-mh_dist(0,0)/2)/ ((2 * M_PI) * pow(deter, 0.5));

    double full_prob = 0.5 * prob;
    return full_prob;
}

MatrixXd SlamParticle::getLaserCov(double pBody[3]){
    double r_var = pow(0.3, 2);
    double t_var = pow(0.2, 2);

    double x_laser_offset = laser_to_body.trans_vec[0];
    double y_laser_offset = laser_to_body.trans_vec[1];
    double rpy_laser_offset[3];
    bot_quat_to_roll_pitch_yaw (laser_to_body.rot_quat, rpy_laser_offset);
    double theta_laser_offset = rpy_laser_offset[2];
    double c_l = cos(theta_laser_offset);
    double s_l = sin(theta_laser_offset);
    
    double pLaser[3];
    //calculate the theta in the laser frame         
    bot_trans_apply_vec (&body_to_laser, pBody, pLaser);

    double r = hypot(pLaser[0] , pLaser[1]);
    double theta = atan2(pLaser[1], pLaser[0]);
    double c = cos(theta);
    double s = sin(theta);
    // Covariance of (x,y) in laser frame 
    MatrixXd cov_laser(2,2); 
    
    //this assumes a variance on both r and theta 
    Matrix2d H0  = Matrix2d::Zero();
    H0(0,0) = c_l;
    H0(1,0) = s_l;
    H0(0,1) = -r * s_l;
    H0(1,1) = r * c_l;

    MatrixXd cov_l(2,2);
    cov_l.setZero();
    cov_l(0,0) = r_var;
    cov_l(1,1) = t_var;
    
    cov_laser = H0 * cov_l * H0.transpose();
    
    MatrixXd H1(2,2); 
    H1(0,0) = H1(1,1) = c_l;
    H1(0,1) = - s_l;
    H1(1,0) = s_l;

    //cov of the query laser in body frame
    MatrixXd cov_body_querry = H1 * cov_laser * H1.transpose();
    return cov_body_querry; 
}

//this is the individual observation prob calculation done right now - this should be changed 
MatrixXd SlamParticle::getObservationCovarianceInParticleFrame(double pBody[3], SlamNode *nd){

    int64_t s_time = bot_timestamp_now();
    Pose2d tocheck_value = nd->getPose();
    double r_var = pow(0.3, 2);
    double t_var = pow(0.2, 2);

    double x_laser_offset = laser_to_body.trans_vec[0];
    double y_laser_offset = laser_to_body.trans_vec[1];
    double rpy_laser_offset[3];
    bot_quat_to_roll_pitch_yaw (laser_to_body.rot_quat, rpy_laser_offset);
    double theta_laser_offset = rpy_laser_offset[2];
    double c_l = cos(theta_laser_offset);
    double s_l = sin(theta_laser_offset);

    double pLaser[3];
    //calculate the theta in the laser frame         
    bot_trans_apply_vec (&body_to_laser, pBody, pLaser);

    double r = hypot(pLaser[0] , pLaser[1]);
    double theta = atan2(pLaser[1], pLaser[0]);
    double c = cos(theta);
    double s = sin(theta);
    // Covariance of (x,y) in laser frame 
    MatrixXd cov_laser(2,2); 
    
    //this assumes a variance on both r and theta 
    Matrix2d H0  = Matrix2d::Zero();
    H0(0,0) = c_l;
    H0(1,0) = s_l;
    H0(0,1) = -r * s_l;
    H0(1,1) = r * c_l;
 
    MatrixXd cov_l(2,2);
    cov_l.setZero();
    cov_l(0,0) = r_var;
    cov_l(1,1) = t_var;
    
    cov_laser = H0 * cov_l * H0.transpose();

    //up to here its correct 

    // we will use a fixed model 
    

    //this is the cov if you assume no uncertainity in the theta 
    
    /*cov_laser(0,0) = cov_laser(1,1) = c * c * r_var;
    cov_laser(0,1) = cov_laser(1,0) = s * s * r_var;*/
    

    //jacobian for going from laser (x,y) to body (x,y) 
    MatrixXd H1(2,2); 
    H1(0,0) = H1(1,1) = c_l;
    H1(0,1) = - s_l;
    H1(1,0) = s_l;

    //cov of the query laser in body frame
    MatrixXd cov_body_querry = H1 * cov_laser * H1.transpose();

    //this might need to have the theta uncertainity also 
        
    //cout << cov_body_querry; 
    //3x3 cov matrix for the querry node 
    MatrixXd cov_querry_body_in_local = graph->getCovariance(nd);

    //build the full covariance matrix 
    MatrixXd cov_querry_frame_and_point(5,5);

    //fill this matrix 
    cov_querry_frame_and_point.setZero(5,5);
    for(int r_id = 0; r_id < 3; r_id++){
        for(int c_id = 0; c_id < 3; c_id++){
            cov_querry_frame_and_point(r_id,c_id) = cov_querry_body_in_local(r_id,c_id);
        }
    }

    for(int r_id = 0; r_id < 2; r_id++){
        for(int c_id = 0; c_id < 2; c_id++){
            cov_querry_frame_and_point(r_id + 3,c_id +3) = cov_body_querry(r_id,c_id);
        }
    }

    double c_querry_frame = cos(tocheck_value.t());
    double s_querry_frame = sin(tocheck_value.t());
    MatrixXd H2(2,5);
    H2.setZero(2,5);
    H2(0,0) = 1;
    H2(0,2) = -s_querry_frame *pBody[0] - c_querry_frame * pBody[1];
    H2(0,3) = c_querry_frame;
    H2(0,4) = -s_querry_frame;
    H2(1,1) = 1;
    H2(1,2) = c_querry_frame *pBody[0] - s_querry_frame * pBody[1];
    H2(1,3) = s_querry_frame;
    H2(1,4) = c_querry_frame;
        
    //ok - i think the jacobian is correct 
    MatrixXd cov_querry_in_particle_frame = H2 * cov_querry_frame_and_point * H2.transpose();

    int64_t e_time = bot_timestamp_now();

    //cout << endl << cov_querry_in_particle_frame << endl;

    return cov_querry_in_particle_frame;
}

//Sachi - this is the model used right now 
// this model calculates the covariances between the two laser points ( querry point for the last scan) 
// and the matched point 
double SlamParticle::getProbOfMeasurementLikelihood(SlamNode *last_nd, double *maximum_probability){

    //querry the kd tree - find the closest point 
    Pose2d tocheck_value = last_nd->getPose();
    Scan *tocheck_scan = last_nd->slam_pose->scan;
    double dist =0;
    double pos[2], next_pos[2];
    double start[2];

    // Node->Slam_Pose->Scan laser scans are decimated and in the body frame
    // We will generate the local occupancy grid in the Query Body frame
    BotTrans localToQueryBody;
    BotTrans queryBodyToLocal;
    queryBodyToLocal.trans_vec[0] = tocheck_value.x();
    queryBodyToLocal.trans_vec[1] = tocheck_value.y();
    queryBodyToLocal.trans_vec[2] = 0.0;
    double rpyQueryBody[3] = {0.0, 0.0, tocheck_value.t()};
    bot_roll_pitch_yaw_to_quat (rpyQueryBody, queryBodyToLocal.rot_quat);
    localToQueryBody = queryBodyToLocal;
    bot_trans_invert (&localToQueryBody);

    last_nd->resetProbability();

    static int64_t total_time = 0;

    BotTrans matchBodyToLocal;
    BotTrans matchBodyToQueryBody;
    BotTrans queryBodyToMatchBody;

    double rpyMatchBody[3] = {0.0, 0.0, 0.0};

    double radius_bound = 0.2;//0.05;//0.3; //was 0.05 
    int query_size = 1;
    double epsilon =  0.05;//0.1;
   
    ANNidxArray  nnIdx = new ANNidx[query_size];
    ANNdistArray dists = new ANNdist[query_size];

    ANNpoint qp = annAllocPt(2);

    int64_t s_utime = bot_timestamp_now();

    int64_t time_to_search = 0;

    int count = 0;
    // Find the nodes (and scans) near the local search area
    double pQuery[3] = {.0};
    double pLaser[3];
    double pQueryInMatched[3];

    int valid_trees = 0;
    int kd_searches = 0;
    int first = 1;

    double dist_threshold = 10.0;

    vector<SlamNode *> valid_nodes;
    //find the valid nodes to search - and cache the transform 
    for(int k = 0; k < last_nd->id; k++){
        SlamNode *nd = graph->getSlamNode(k);
        Pose2d value = nd->getPose();
        nd->valid = 0;
        Pose2d delta = tocheck_value.ominus(value);
        dist = hypot(delta.x(), delta.y());
        if(dist < dist_threshold){
            nd->valid = 1;
            
            ScanTransform T;
            T.x = value.x();
            T.y = value.y();
            T.theta = value.t();

            // Determine the transform from Match Body ---> Query Body
            
            // Set the BotTrans from Match BODY to LOCAL
            matchBodyToLocal.trans_vec[0] = value.x();
            matchBodyToLocal.trans_vec[1] = value.y();
            matchBodyToLocal.trans_vec[2] = 0.0;
            rpyMatchBody[2] = value.t();
            bot_roll_pitch_yaw_to_quat (rpyMatchBody, matchBodyToLocal.rot_quat);
            
            // ... and now Match Body ---> Query Body
            bot_trans_apply_trans_to (&localToQueryBody, &matchBodyToLocal, &matchBodyToQueryBody);
            nd->queryBodyToMatchBody = matchBodyToQueryBody;
            bot_trans_invert(&nd->queryBodyToMatchBody);
            valid_nodes.push_back(nd);
        }
    }

    //vaid no of kd trees 
    valid_trees = valid_nodes.size();
    
    double cum_prob = 0;
    double avg_prob = 0;
    double max_prob = 0;  

    // For each point in query scan, find the closest point among the scans of each nearby node (valid_nodes)
    for(unsigned i = 0; i < tocheck_scan->numPoints; i++){
        
        pQuery[0] = tocheck_scan->points[i].x;
        pQuery[1] = tocheck_scan->points[i].y;
               
        double pQueryLocal[3];
        double pMatchedLocal[3];

        // Transform the query point into the local frame
        bot_trans_apply_vec(&queryBodyToLocal, pQuery, pQueryLocal);

        //fprintf(stderr, "Querry Point in Local frame : %f,%f\n", pQueryLocal[0], pQueryLocal[1]);

        double min_dist = 1000; 
        int close_ind  = -1;
        double pMatched[3] = {0.}; 

        // loop through the neighbor nodes to find the nearest scan point
        for(int k = 0; k < valid_nodes.size(); k++){
            SlamNode *nd = valid_nodes.at(k); 
            
            kd_searches++;

            // Transform the point from the query pose frame to the match frame
            bot_trans_apply_vec ( &nd->queryBodyToMatchBody, pQuery, pQueryInMatched);
                
            qp[0] = pQueryInMatched[0];
            qp[1] = pQueryInMatched[1];
            
            int64_t search_utime = bot_timestamp_now();

            nd->slam_pose->kd_tree->annkFRSearch (qp,                    // query point
                                                  ANN_POW(radius_bound), // squared radius search bound
                                                  query_size,            // number of near neighbors
                                                  nnIdx,                 // nearest neighbors (returned)
                                                  dists,                 // distance (returned)
                                                  epsilon);
            
            int64_t search_end_utime = bot_timestamp_now();
            time_to_search += (search_end_utime - search_utime);
            int found = 0;

            for(int l=0;l< query_size ;l++){
                if(nnIdx[l] < 0){//because we have reached the end of the valid neighbours
                    break; 
                    //continue;
                }
                //count++;
                found = 1;
                //dists[l] = sqrt(dists[l]); // MRW: Commented out
                double dist_val = sqrt(dists[l]); 
                //cout << i << " " << nnIdx[l] << " " << dists[l] << "\n";

                //nnIdx[l];
                
                if(min_dist > dist_val){
                    min_dist = dist_val;
                    ANNpointArray kdtree_points = nd->slam_pose->kd_tree->thePoints();
                    pMatched[0] = (kdtree_points[nnIdx[(l)]][(0)]);
                    pMatched[1] = (kdtree_points[nnIdx[(l)]][(1)]);
                    pMatched[2] = 0.0;
                    close_ind = k;
                }               
            }
        }

        //fprintf(stderr, "Distance - recal : %f\n", hypot(pMatched[0] - pQueryInMatched[0], pMatched[1] - pQueryInMatched[1]));
        //fprintf(stderr, "Min Tree : %d\n", close_ind);
        
        // Did we find a match?
        if(close_ind >=0 && min_dist < 0.5){
            SlamNode *nd = valid_nodes.at(close_ind);

            Pose2d match = nd->getPose();

            // Determine the transform from Match Body ---> Query Body
            
            // Set the BotTrans from Match BODY to LOCAL
            matchBodyToLocal.trans_vec[0] = match.x();
            matchBodyToLocal.trans_vec[1] = match.y();
            matchBodyToLocal.trans_vec[2] = 0.0;
            rpyMatchBody[2] = match.t();
            bot_roll_pitch_yaw_to_quat (rpyMatchBody, matchBodyToLocal.rot_quat);
            bot_trans_apply_vec (&matchBodyToLocal, pMatched, pMatchedLocal);

            int64_t s_t = bot_timestamp_now();
            
            //get the covariance of the scan point based on the uncertainity of the 
            MatrixXd cov_querry_in_particle_frame = getObservationCovarianceInParticleFrame(pQuery, last_nd);
            int64_t e_t = bot_timestamp_now();
            
            //fprintf(stderr, "Matched Point in Local frame : %f,%f\n", pMatchedLocal[0], pMatchedLocal[1]);

            //fprintf(stderr, "Dist : %f\n", hypot(pMatchedLocal[0] - pQueryLocal[0] , pMatchedLocal[1] - pQueryLocal[1]));
            
            MatrixXd cov_match_in_particle_frame = getObservationCovarianceInParticleFrame(pMatched, nd);
            //looks reasonable enough 
            //cout << endl << "Match Cov : " << endl << cov_match_in_particle_frame << endl; 
            double prob = getObservationProbability(cov_match_in_particle_frame, cov_querry_in_particle_frame, 
                    pMatchedLocal, pQueryLocal);
            //this is the fixed model - wont take in to account the cov of the nodes 
            
            //double prob =  getObservationProbabilityFixed(pMatchedLocal, pQueryLocal);

            //fprintf(stderr, "Found => Min dist : %f Prob : %f\n", min_dist, prob);
            
            if(prob > 1.0)
                prob = 1.0;

            if(prob < 0.5) 
                prob = 0.5; 

            double full_prob = prob; // 0.6 * prob + 0.4 * 0.2
            
            //fprintf(stderr, "Found => Min dist : %f Prob : %f\n", min_dist, prob);

            if(max_prob < full_prob){
                max_prob = full_prob;
            }
            //saved in the slam node - to be used when updating 
            last_nd->prob_of_scan[i] = full_prob;
            /*avg_prob += prob; //0.6 * prob + 0.4 * 0.2; 
              cum_prob += log(prob); //log(0.6 * prob + 0.4 * 0.2);// prob);*/
            
            avg_prob += full_prob; //0.6 * prob + 0.4 * 0.2; 
            cum_prob += log(full_prob);//0.6 * prob + 0.4 * 0.2);
            //fprintf(stderr, "\tProb : %f\n", prob);
            count++;

        }
        else{
            //fprintf(stderr, "Not found => Min dist : %f Prob : %f\n", min_dist, 0.4 * 0.8);
            avg_prob += 0.5;//0.4 * 0.8; 
            cum_prob += log(0.5);//0.4 * 0.8);
            last_nd->prob_of_scan[i] =  0.5;//0.4 * 0.8;//0; 
        }
    }
    
    last_nd->max_scan_prob = max_prob; 
    *maximum_probability = max_prob;
    avg_prob /= tocheck_scan->numPoints;//count, 
    
    if(verbose)
        fprintf(stderr, "Count : %d Max Prob : %f Avg : %f Cumalative Prob : (log) %f => %f \n", 
                count, max_prob, avg_prob, 
                cum_prob, exp(cum_prob));
 
    annDeallocPt(qp);
    delete [] nnIdx;
    delete [] dists; 
    int64_t e_utime = bot_timestamp_now();
    if(verbose){
        fprintf(stderr, "\t Total : %d => Matched Count : %d\n", tocheck_scan->numPoints, count);
        fprintf(stderr, "\t Valid Trees : %d => Total Searches : %d Time per search : %f\n", valid_trees, kd_searches, (time_to_search) / 1.0e6 / (double) kd_searches);
        fprintf(stderr, "Time to serach kd tree : %f\n", (time_to_search) / 1.0e6);
    }
    
    cum_prob -= log(max_prob) * count; 
    return exp(cum_prob);
    //return avg_prob; //exp(cum_prob); //avg_prob / count; //exp(cum_prob); //count / (double) tocheck_scan->numPoints;
}

//Sachi - this is the model used right now 
// this model calculates the covariances between the two laser points ( querry point for the last scan) 
// and the matched point 
//
// Matt - This is how it works now;
//        We consider three cases:
//         (i)   Scan point is within a small threshold of existing point. The likelihood is evaluated via covariance calculation
//         (ii)  Scan point corresponds to an unobserved part of the environment
//         (iii) Scan point has a match, but is twoo far away
//
//        For a given point we find the kNN among known scans for some threshold radius > point_match_threshold.
//        If a point has no NN, we assume it is of type (ii) and assign a fixed likelihood
//        If a point has a NN that is within point_match_threshold, we assume it is type (i)
//        If a point has a NN but is farther than point_match_threshold, we assume it is type (iii)

double SlamParticle::getProbOfMeasurementLikelihoodCount(SlamNode *last_nd, double *maximum_probability){

    // Settings that determine behavior
    // The distance threshold used for kNN
    double radius_bound = 0.25; // Distance threshold for NN (types (i) and (iii))
    double point_match_threshold = 0.1; // Distance threshold for valid matches (type (i))
    double prob_2 = 0.05; // Probability associated with type 2 
    double prob_3 = 0.05; // Probability associated with type 3

    //querry the kd tree - find the closest point 
    Pose2d tocheck_value = last_nd->getPose();
    Scan *tocheck_scan = last_nd->slam_pose->scan;
    double dist =0;
    double pos[2], next_pos[2];
    double start[2];

    // Node->Slam_Pose->Scan laser scans are decimated and in the body frame
    // We will generate the local occupancy grid in the Query Body frame
    BotTrans localToQueryBody;
    BotTrans queryBodyToLocal;
    queryBodyToLocal.trans_vec[0] = tocheck_value.x();
    queryBodyToLocal.trans_vec[1] = tocheck_value.y();
    queryBodyToLocal.trans_vec[2] = 0.0;
    double rpyQueryBody[3] = {0.0, 0.0, tocheck_value.t()};
    bot_roll_pitch_yaw_to_quat (rpyQueryBody, queryBodyToLocal.rot_quat);
    localToQueryBody = queryBodyToLocal;
    bot_trans_invert (&localToQueryBody);

    last_nd->resetProbability();

    BotTrans matchBodyToLocal;
    BotTrans matchBodyToQueryBody;
    BotTrans queryBodyToMatchBody;

    double rpyMatchBody[3] = {0.0, 0.0, 0.0};

    int query_size = 1;
    double epsilon =  0.01;//0.1;
   
    ANNidxArray  nnIdx = new ANNidx[query_size];
    ANNdistArray dists = new ANNdist[query_size];

    ANNpoint qp = annAllocPt(2);

    int64_t s_utime = bot_timestamp_now();

    int64_t time_to_search = 0;

    int count = 0;
    // Find the nodes (and scans) near the local search area
    double pQuery[3] = {.0};
    double pLaser[3];
    double pQueryInMatched[3];

    

    int valid_trees = 0;
    int kd_searches = 0;
    int first = 1;

    double dist_threshold = 10.0;

    vector< pair<int,int> > matched_nodeid_pointidx;

    vector<SlamNode *> valid_nodes;
    //find the valid nodes to search - and cache the transform 
    for(int k = 0; k < last_nd->id; k++){
        SlamNode *nd = graph->getSlamNode(k);
        
        // Ignore scans with the same parent supernode
        if (nd->parent_supernode == last_nd->parent_supernode)
            continue;

        if ((last_nd->id - nd->id) < 10)
            continue;

        Pose2d value = nd->getPose();
        nd->valid = 0;
        Pose2d delta = tocheck_value.ominus(value);
        dist = hypot(delta.x(), delta.y());
        if(dist < dist_threshold){
            nd->valid = 1;
            
            ScanTransform T;
            T.x = value.x();
            T.y = value.y();
            T.theta = value.t();

            // Determine the transform from Match Body ---> Query Body
            
            // Set the BotTrans from Match BODY to LOCAL
            matchBodyToLocal.trans_vec[0] = value.x();
            matchBodyToLocal.trans_vec[1] = value.y();
            matchBodyToLocal.trans_vec[2] = 0.0;
            rpyMatchBody[2] = value.t();
            bot_roll_pitch_yaw_to_quat (rpyMatchBody, matchBodyToLocal.rot_quat);
            
            // ... and now Match Body ---> Query Body
            bot_trans_apply_trans_to (&localToQueryBody, &matchBodyToLocal, &matchBodyToQueryBody);
            nd->queryBodyToMatchBody = matchBodyToQueryBody;
            bot_trans_invert(&nd->queryBodyToMatchBody);
            valid_nodes.push_back(nd);
        }
    }    
    
    //vaid no of kd trees 
    valid_trees = valid_nodes.size();
    
    double cum_prob = 0;
    double avg_prob = 0;
    double max_prob = 0;  

    int no_of_matches = 0;

    // For each point in query scan, find the closest point among the scans of each nearby node (valid_nodes)
    for(unsigned i = 0; i < tocheck_scan->numPoints; i++){

        int my_no_matches = 0;
        int point_type = 0; // Either 1, 2, or 3
        
        pQuery[0] = tocheck_scan->points[i].x;
        pQuery[1] = tocheck_scan->points[i].y;
               
        double pQueryLocal[3];
        double pMatchedLocal[3];

        // Transform the query point into the local frame
        bot_trans_apply_vec(&queryBodyToLocal, pQuery, pQueryLocal);

        //fprintf(stderr, "Querry Point in Local frame : %f,%f\n", pQueryLocal[0], pQueryLocal[1]);

        double min_dist = 1000; 
        int close_ind  = -1;
        double pMatched[3] = {0.}; 

        int match_id = -1;
        int match_idx = -1;
        // loop through the neighbor nodes to find the nearest scan point
        for(int k = 0; k < valid_nodes.size(); k++){
            SlamNode *nd = valid_nodes.at(k); 
            
            kd_searches++;

            // Transform the point from the query pose frame to the match frame
            bot_trans_apply_vec ( &nd->queryBodyToMatchBody, pQuery, pQueryInMatched);
                
            qp[0] = pQueryInMatched[0];
            qp[1] = pQueryInMatched[1];
            
            int64_t search_utime = bot_timestamp_now();

            nd->slam_pose->kd_tree->annkFRSearch (qp,                    // query point
                                                  ANN_POW(radius_bound), // squared radius search bound
                                                  query_size,            // number of near neighbors
                                                  nnIdx,                 // nearest neighbors (returned)
                                                  dists,                 // distance (returned)
                                                  epsilon);
            
            int64_t search_end_utime = bot_timestamp_now();
            time_to_search += (search_end_utime - search_utime);
            int found = 0;

            
            // Point is type 2 if there are no NN
            if (nnIdx[0] < 0)
                point_type = 2;
            else
                point_type = 3; // Assume outlier for now
            
            // Loop through the set of nearest scan points
            for(int l=0;l< query_size ;l++){
                if(nnIdx[l] < 0){//because we have reached the end of the valid neighbours
                    break; 
                    //continue;
                }
                
                double dist_val = sqrt(dists[l]); 
                
                // Doensn't pass the test to be type 1
                if (dist_val > point_match_threshold)
                    continue;

                //count++;
                found = 1;
                point_type = 1;
                no_of_matches ++;
                my_no_matches ++;
                //dists[l] = sqrt(dists[l]); // MRW: Commented out
                //cout << i << " " << nnIdx[l] << " " << dists[l] << "\n";

                //nnIdx[l];
                
                vector< pair<int,int> >::iterator pit;
                pair <int, int> test_id_idx(nd->id, nnIdx[(l)]);
                pit = find ( matched_nodeid_pointidx.begin(),  matched_nodeid_pointidx.end(), test_id_idx);

                if(min_dist > dist_val && pit ==  matched_nodeid_pointidx.end()){
                    min_dist = dist_val;
                    ANNpointArray kdtree_points = nd->slam_pose->kd_tree->thePoints();
                    pMatched[0] = (kdtree_points[nnIdx[(l)]][(0)]);
                    pMatched[1] = (kdtree_points[nnIdx[(l)]][(1)]);
                    pMatched[2] = 0.0;
                    close_ind = k;
                    
                    match_id = nd->id;
                    match_idx = nnIdx[(l)];
                }               
            }
        }

        //fprintf(stderr, "Distance - recal : %f\n", hypot(pMatched[0] - pQueryInMatched[0], pMatched[1] - pQueryInMatched[1]));
        //fprintf(stderr, "Min Tree : %d\n", close_ind);
        
        double prob;

        // Did we find a match?
        // Point type 1
        if(close_ind >=0 && min_dist < radius_bound){
            pair <int, int> id_idx(match_id, match_idx);
            matched_nodeid_pointidx.push_back (id_idx);

            SlamNode *nd = valid_nodes.at(close_ind);

            Pose2d match = nd->getPose();

            // Determine the transform from Match Body ---> Query Body
            
            // Set the BotTrans from Match BODY to LOCAL
            matchBodyToLocal.trans_vec[0] = match.x();
            matchBodyToLocal.trans_vec[1] = match.y();
            matchBodyToLocal.trans_vec[2] = 0.0;
            rpyMatchBody[2] = match.t();
            bot_roll_pitch_yaw_to_quat (rpyMatchBody, matchBodyToLocal.rot_quat);
            bot_trans_apply_vec (&matchBodyToLocal, pMatched, pMatchedLocal);


            bot_lcmgl_point_size (lcmgl_sm_graph, 3);
            bot_lcmgl_begin (lcmgl_sm_graph, GL_POINTS);
            bot_lcmgl_color3f (lcmgl_sm_graph, 0.0, 1.0, 0.0);
            bot_lcmgl_vertex3f (lcmgl_sm_graph, pQueryLocal[0], pQueryLocal[1], 0);
            bot_lcmgl_color3f (lcmgl_sm_graph, 1.0, 0.7, 0.0);
            bot_lcmgl_vertex3f (lcmgl_sm_graph, pMatchedLocal[0], pMatchedLocal[1], 0);
            bot_lcmgl_end (lcmgl_sm_graph);



            bot_lcmgl_color3f (lcmgl_sm_graph, 1.0, 0.0, 0.0);
            bot_lcmgl_line_width (lcmgl_sm_graph, 3);
            bot_lcmgl_begin (lcmgl_sm_graph, GL_LINES);
            bot_lcmgl_vertex3f (lcmgl_sm_graph, pQueryLocal[0], pQueryLocal[1], 0);
            bot_lcmgl_vertex3f (lcmgl_sm_graph, pMatchedLocal[0], pMatchedLocal[1], 0);
            bot_lcmgl_end (lcmgl_sm_graph);

            
            //fprintf(stderr, "Matched Point in Local frame : %f,%f\n", pMatchedLocal[0], pMatchedLocal[1]);
            int64_t s_t = bot_timestamp_now();    
            //get the covariance of the scan point based on the uncertainity of the 
            MatrixXd cov_querry_in_particle_frame = getObservationCovarianceInParticleFrame(pQuery, last_nd);
            int64_t e_t = bot_timestamp_now();
            //fprintf(stderr, "Dist : %f\n", hypot(pMatchedLocal[0] - pQueryLocal[0] , pMatchedLocal[1] - pQueryLocal[1]));
            
            MatrixXd cov_match_in_particle_frame = getObservationCovarianceInParticleFrame(pMatched, nd);
            //looks reasonable enough 
            //cout << endl << "Match Cov : " << endl << cov_match_in_particle_frame << endl; 
            prob = getObservationProbability(cov_match_in_particle_frame, cov_querry_in_particle_frame, 
                                                    pMatchedLocal, pQueryLocal);

            // This probability represents the number of matches as a fraction of the maximum possible (one per matched node)
            // we add 1 to each to deal with the case of scans from new areas (see below)
            //double prob = (my_no_matches+1)/(valid_nodes.size() + 1);

            //this is the fixed model - wont take in to account the cov of the nodes 
            //double prob =  getObservationProbabilityFixed(pMatchedLocal, pQueryLocal);

            //fprintf(stderr, "Found => Min dist : %f Prob : %f\n", min_dist, prob);
            
            if(prob > 1.0)
                prob = 1.0;

            //if(prob < 0.5) 
            //    prob = 0.5; 

            //if (prob < 0.1)
            //    prob = 0.1;

            //double full_prob = prob; // 0.6 * prob + 0.4 * 0.2
            
            //fprintf(stderr, "Found => Min dist : %f Prob : %f\n", min_dist, prob);

            if(max_prob < prob)
                max_prob = prob;

            //saved in the slam node - to be used when updating 
            //last_nd->prob_of_scan[i] = full_prob;
            //fprintf (stdout,"\t\t my_no_matches = %d, valid_nodes.size() = %d\n", my_no_matches, valid_nodes.size());
            /*avg_prob += prob; //0.6 * prob + 0.4 * 0.2; 
              cum_prob += log(prob); //log(0.6 * prob + 0.4 * 0.2);// prob);*/
            //avg_prob += full_prob; //0.6 * prob + 0.4 * 0.2; 
            //cum_prob += log(full_prob);//0.6 * prob + 0.4 * 0.2);
            //fprintf(stderr, "\tProb : %f\n", prob);
            count++;

        }
        else{

            if (point_type == 3) 
                prob = prob_3;
            else
                prob = prob_2;

            //fprintf(stderr, "Not found => Min dist : %f Prob : %f\n", min_dist, 0.4 * 0.8);
            //avg_prob += 0.5;//0.4 * 0.8; 
            //cum_prob += log(0.5);//0.4 * 0.8);
            //avg_prob += 1/(valid_nodes.size()+1);
            //cum_prob += log(1/(valid_nodes.size()+1));
            //last_nd->prob_of_scan[i] =  0.5;//0.4 * 0.8;//0; 
            //last_nd->prob_of_scan[i] =  1/(valid_nodes.size()+1);//0.4 * 0.8;//0; 
        }

        avg_prob += prob;
        cum_prob += log (prob);
        last_nd->prob_of_scan[i] = prob;

    }

    //fprintf(stderr, "\t\t [%d] Slam Graph No of matches : %d\n", graph_id, no_of_matches);
    
    last_nd->max_scan_prob = max_prob; 
    *maximum_probability = max_prob;
    avg_prob /= tocheck_scan->numPoints;//count, 
        
    if(verbose)
        fprintf(stderr, "Count : %d Max Prob : %f Avg : %f Cumalative Prob : (log) %f => %f \n", 
                count, max_prob, avg_prob, 
                cum_prob, exp(cum_prob));
 
    annDeallocPt(qp);
    delete [] nnIdx;
    delete [] dists; 
    int64_t e_utime = bot_timestamp_now();
    if(verbose){
        fprintf(stderr, "\t Total : %d => Matched Count : %d\n", tocheck_scan->numPoints, count);
        fprintf(stderr, "\t Valid Trees : %d => Total Searches : %d Time per search : %f\n", valid_trees, kd_searches, (time_to_search) / 1.0e6 / (double) kd_searches);
        fprintf(stderr, "Time to serach kd tree : %f\n", (time_to_search) / 1.0e6);
    }
    
    bot_lcmgl_switch_buffer (lcmgl_sm_graph);
    cum_prob -= log(max_prob) * count; 
    //return exp(cum_prob);
    return avg_prob; //exp(cum_prob); //avg_prob / count; //exp(cum_prob); //count / (double) tocheck_scan->numPoints;
}

double SlamParticle::getProbOfMeasurementLikelihoodComplex(SlamNode *last_nd, double *maximum_probability){

    // Settings that determine behavior
    // The distance threshold used for kNN
    double radius_bound = 0.25; // Distance threshold for NN (types (i) and (iii))
    double point_match_threshold = 0.1; // Distance threshold for valid matches (type (i))
    double prob_2 = 0.05; // Probability associated with type 2 
    double prob_3 = 0.05; // Probability associated with type 3

    static int64_t total_time = 0;
    static int called_count = 0;

    //querry the kd tree - find the closest point 
    Pose2d tocheck_value = last_nd->getPose();
    Scan *tocheck_scan = last_nd->slam_pose->scan;
    double dist =0;
    double pos[2], next_pos[2];
    double start[2];

    // Node->Slam_Pose->Scan laser scans are decimated and in the body frame
    // We will generate the local occupancy grid in the Query Body frame
    BotTrans localToQueryBody;
    BotTrans queryBodyToLocal;
    queryBodyToLocal.trans_vec[0] = tocheck_value.x();
    queryBodyToLocal.trans_vec[1] = tocheck_value.y();
    queryBodyToLocal.trans_vec[2] = 0.0;
    double rpyQueryBody[3] = {0.0, 0.0, tocheck_value.t()};
    bot_roll_pitch_yaw_to_quat (rpyQueryBody, queryBodyToLocal.rot_quat);
    localToQueryBody = queryBodyToLocal;
    bot_trans_invert (&localToQueryBody);

    last_nd->resetProbability();

    double X1 = tocheck_value.x();
    double Y1 = tocheck_value.y();
    double T1 = tocheck_value.t();

    BotTrans matchBodyToLocal;
    BotTrans matchBodyToQueryBody;
    BotTrans queryBodyToMatchBody;

    double rpyMatchBody[3] = {0.0, 0.0, 0.0};

    int query_size = 1;
    double epsilon =  0.01;//0.1;
   
    ANNidxArray  nnIdx = new ANNidx[query_size];
    ANNdistArray dists = new ANNdist[query_size];

    ANNpoint qp = annAllocPt(2);

    int64_t s_utime = bot_timestamp_now();

    int64_t time_to_search = 0;

    int count = 0;
    // Find the nodes (and scans) near the local search area
    double pQuery[3] = {.0};
    double pLaser[3];
    double pQueryInMatched[3];

    int valid_trees = 0;
    int kd_searches = 0;
    int first = 1;

    double dist_threshold = 10.0;

    vector< pair<int,int> > matched_nodeid_pointidx;

    vector<SlamNode *> valid_nodes;
    //find the valid nodes to search - and cache the transform 
    for(int k = 0; k < last_nd->id; k++){
        SlamNode *nd = graph->getSlamNode(k);
        
        // Ignore scans with the same parent supernode
        if (nd->parent_supernode == last_nd->parent_supernode)
            continue;

        if ((last_nd->id - nd->id) < 10)
            continue;

        Pose2d value = nd->getPose();
        nd->valid = 0;
        Pose2d delta = tocheck_value.ominus(value);
        dist = hypot(delta.x(), delta.y());
        if(dist < dist_threshold){
            nd->valid = 1;
            
            ScanTransform T;
            T.x = value.x();
            T.y = value.y();
            T.theta = value.t();

            // Determine the transform from Match Body ---> Query Body
            
            // Set the BotTrans from Match BODY to LOCAL
            matchBodyToLocal.trans_vec[0] = value.x();
            matchBodyToLocal.trans_vec[1] = value.y();
            matchBodyToLocal.trans_vec[2] = 0.0;
            rpyMatchBody[2] = value.t();
            bot_roll_pitch_yaw_to_quat (rpyMatchBody, matchBodyToLocal.rot_quat);
            
            // ... and now Match Body ---> Query Body
            bot_trans_apply_trans_to (&localToQueryBody, &matchBodyToLocal, &matchBodyToQueryBody);
            nd->queryBodyToMatchBody = matchBodyToQueryBody;
            bot_trans_invert(&nd->queryBodyToMatchBody);
            valid_nodes.push_back(nd);
            
            //MatrixXd curr_cov = graph->getCovariances(last_nd, nd);
            
            //cov.insert(make_pair(nd->id, curr_cov));
        }
    }
    
    
    
    //fprintf(stderr, "Total Time taken : %f\n", total_time / 1.0e6 / called_count );

    map <int, MatrixXd>::iterator it;
    
    //vaid no of kd trees 
    valid_trees = valid_nodes.size();
    
    double cum_prob = 0;
    double avg_prob = 0;
    double max_prob = 0;  

    int no_of_matches = 0;

    map<int, SlamNode *> match_pt_map;

    // For each point in query scan, find the closest point among the scans of each nearby node (valid_nodes)
    for(unsigned i = 0; i < tocheck_scan->numPoints; i++){
        int my_no_matches = 0;
        int point_type = 0; // Either 1, 2, or 3
        
        pQuery[0] = tocheck_scan->points[i].x;
        pQuery[1] = tocheck_scan->points[i].y;

                     
        double pQueryLocal[3];
        double pMatchedLocal[3];

        // Transform the query point into the local frame
        bot_trans_apply_vec(&queryBodyToLocal, pQuery, pQueryLocal);

        //fprintf(stderr, "Querry Point in Local frame : %f,%f\n", pQueryLocal[0], pQueryLocal[1]);

        double min_dist = 1000; 
        int close_ind  = -1;
        double pMatched[3] = {0.}; 

        int match_id = -1;
        int match_idx = -1;
        // loop through the neighbor nodes to find the nearest scan point
        for(int k = 0; k < valid_nodes.size(); k++){
            SlamNode *nd = valid_nodes.at(k); 
            
            kd_searches++;

            // Transform the point from the query pose frame to the match frame
            bot_trans_apply_vec ( &nd->queryBodyToMatchBody, pQuery, pQueryInMatched);
                
            qp[0] = pQueryInMatched[0];
            qp[1] = pQueryInMatched[1];
            
            int64_t search_utime = bot_timestamp_now();

            nd->slam_pose->kd_tree->annkFRSearch (qp,                    // query point
                                                  ANN_POW(radius_bound), // squared radius search bound
                                                  query_size,            // number of near neighbors
                                                  nnIdx,                 // nearest neighbors (returned)
                                                  dists,                 // distance (returned)
                                                  epsilon);
            
            int64_t search_end_utime = bot_timestamp_now();
            time_to_search += (search_end_utime - search_utime);
            int found = 0;

            
            // Point is type 2 if there are no NN
            if (nnIdx[0] < 0)
                point_type = 2;
            else
                point_type = 3; // Assume outlier for now
            
            // Loop through the set of nearest scan points
            for(int l=0;l< query_size ;l++){
                if(nnIdx[l] < 0){//because we have reached the end of the valid neighbours
                    break; 
                    //continue;
                }
                
                double dist_val = sqrt(dists[l]); 
                
                // Doensn't pass the test to be type 1
                if (dist_val > point_match_threshold)
                    continue;

                //count++;
                found = 1;
                point_type = 1;
                no_of_matches ++;
                my_no_matches ++;
                //dists[l] = sqrt(dists[l]); // MRW: Commented out
                //cout << i << " " << nnIdx[l] << " " << dists[l] << "\n";

                //nnIdx[l];
                
                vector< pair<int,int> >::iterator pit;
                pair <int, int> test_id_idx(nd->id, nnIdx[(l)]);
                pit = find ( matched_nodeid_pointidx.begin(),  matched_nodeid_pointidx.end(), test_id_idx);

                if(min_dist > dist_val && pit ==  matched_nodeid_pointidx.end()){
                    min_dist = dist_val;
                    ANNpointArray kdtree_points = nd->slam_pose->kd_tree->thePoints();
                    pMatched[0] = (kdtree_points[nnIdx[(l)]][(0)]);
                    pMatched[1] = (kdtree_points[nnIdx[(l)]][(1)]);
                    pMatched[2] = 0.0;
                    close_ind = k;
                    
                    match_id = nd->id;
                    match_idx = nnIdx[(l)];
                }               
            }
        }

        double prob;

        // Did we find a match?
        // Point type 1
        if(close_ind >=0 && min_dist < 0.5){            
            pair <int, int> id_idx(match_id, match_idx);
            matched_nodeid_pointidx.push_back (id_idx);
            SlamNode *nd = valid_nodes.at(close_ind);
            match_pt_map.insert(pair<int, SlamNode *>(nd->id, nd));
        }
        else{
            pair <int, int> id_idx(-1, point_type);
            matched_nodeid_pointidx.push_back (id_idx);
        }
    }

    vector<SlamNode *> cov_querry_nodes;
    map<int, SlamNode *>::iterator s_it;
    for ( s_it= match_pt_map.begin() ; s_it != match_pt_map.end(); s_it++ ){        
        cov_querry_nodes.push_back(s_it->second);
    }

    map<int, MatrixXd> cov;
    if(cov_querry_nodes.size() == 0){
        fprintf(stderr, "Error - no valid close nodes\n");
    }
    else{
        called_count++;
        int64_t s_utime = bot_timestamp_now();
        cov = graph->getCovariances(last_nd, cov_querry_nodes);
        int64_t e_utime = bot_timestamp_now();
        total_time += (e_utime - s_utime); 
    }
    
    double pMatched[3] = {0.}; 
    double pQueryLocal[3];
    double pMatchedLocal[3];
    double prob = 0;
    for(int i =0; i < matched_nodeid_pointidx.size(); i++){
        pair<int, int> m_pt =  matched_nodeid_pointidx.at(i);

        if(m_pt.first >=0){
            
            SlamNode *nd = graph->getSlamNode(m_pt.first);//valid_nodes.at(m_pt.first);//close_ind);
            
            ANNpointArray kdtree_points = nd->slam_pose->kd_tree->thePoints();
            pMatched[0] = (kdtree_points[m_pt.second][(0)]);
            pMatched[1] = (kdtree_points[m_pt.second][(1)]);
            pMatched[2] = 0.0;
            
            MatrixXd qPoseMPoseCov;
            it = cov.find(nd->id);
            if(it == cov.end()){
                //qPoseMPoseCov = graph->getCovariances(last_nd, nd);
                //cov.insert(make_pair(nd->id, qPoseMPoseCov));

                fprintf(stderr, "Error - this should not have happened\n");
                continue;
            }
            else{
                qPoseMPoseCov = it->second;
            }

            //cout << "Covariance : \n" << it->second << endl;
            
            Pose2d match = nd->getPose();

            // Determine the transform from Match Body ---> Query Body
            
            // Set the BotTrans from Match BODY to LOCAL
            matchBodyToLocal.trans_vec[0] = match.x();
            matchBodyToLocal.trans_vec[1] = match.y();
            matchBodyToLocal.trans_vec[2] = 0.0;
            rpyMatchBody[2] = match.t();
            bot_roll_pitch_yaw_to_quat (rpyMatchBody, matchBodyToLocal.rot_quat);
            bot_trans_apply_vec (&matchBodyToLocal, pMatched, pMatchedLocal);


            bot_lcmgl_point_size (lcmgl_sm_graph, 3);
            bot_lcmgl_begin (lcmgl_sm_graph, GL_POINTS);
            bot_lcmgl_color3f (lcmgl_sm_graph, 0.0, 1.0, 0.0);
            bot_lcmgl_vertex3f (lcmgl_sm_graph, pQueryLocal[0], pQueryLocal[1], 0);
            bot_lcmgl_color3f (lcmgl_sm_graph, 1.0, 0.7, 0.0);
            bot_lcmgl_vertex3f (lcmgl_sm_graph, pMatchedLocal[0], pMatchedLocal[1], 0);
            bot_lcmgl_end (lcmgl_sm_graph);



            bot_lcmgl_color3f (lcmgl_sm_graph, 1.0, 0.0, 0.0);
            bot_lcmgl_line_width (lcmgl_sm_graph, 3);
            bot_lcmgl_begin (lcmgl_sm_graph, GL_LINES);
            bot_lcmgl_vertex3f (lcmgl_sm_graph, pQueryLocal[0], pQueryLocal[1], 0);
            bot_lcmgl_vertex3f (lcmgl_sm_graph, pMatchedLocal[0], pMatchedLocal[1], 0);
            bot_lcmgl_end (lcmgl_sm_graph);

            //the point for last nd
            MatrixXd pQuerryCov = getLaserCov(pQuery);
            //the matched one 
            MatrixXd pMatchCov = getLaserCov(pMatched);



            prob = getObservationProbabilityComplex(qPoseMPoseCov, pQuerryCov, qPoseMPoseCov, 
                                                    pQuery, pMatched, tocheck_value, match);

            if(prob > 1.0)
                prob = 1.0;

            if(max_prob < prob)
                max_prob = prob;

            //saved in the slam node - to be used when updating 
            //last_nd->prob_of_scan[i] = full_prob;
            //fprintf (stdout,"\t\t my_no_matches = %d, valid_nodes.size() = %d\n", my_no_matches, valid_nodes.size());
            /*avg_prob += prob; //0.6 * prob + 0.4 * 0.2; 
              cum_prob += log(prob); //log(0.6 * prob + 0.4 * 0.2);// prob);*/
            //avg_prob += full_prob; //0.6 * prob + 0.4 * 0.2; 
            //cum_prob += log(full_prob);//0.6 * prob + 0.4 * 0.2);
            //fprintf(stderr, "\tProb : %f\n", prob);
            count++;

        }
        else{
            int point_type = m_pt.second;
            if (point_type == 3) 
                prob = prob_3;
            else
                prob = prob_2;

            //fprintf(stderr, "Not found => Min dist : %f Prob : %f\n", min_dist, 0.4 * 0.8);
            //avg_prob += 0.5;//0.4 * 0.8; 
            //cum_prob += log(0.5);//0.4 * 0.8);
            //avg_prob += 1/(valid_nodes.size()+1);
            //cum_prob += log(1/(valid_nodes.size()+1));
            //last_nd->prob_of_scan[i] =  0.5;//0.4 * 0.8;//0; 
            //last_nd->prob_of_scan[i] =  1/(valid_nodes.size()+1);//0.4 * 0.8;//0; 
        }

        avg_prob += prob;
        cum_prob += log (prob);
        last_nd->prob_of_scan[i] = prob;

    }

    fprintf(stderr, "No of Valid nodes : %d - No of Matched Nodes : %d\n", valid_nodes.size(), match_pt_map.size());

    //fprintf(stderr, "\t\t [%d] Slam Graph No of matches : %d\n", graph_id, no_of_matches);
    
    last_nd->max_scan_prob = max_prob; 
    *maximum_probability = max_prob;
    avg_prob /= tocheck_scan->numPoints;//count, 
        
    if(verbose)
        fprintf(stderr, "Count : %d Max Prob : %f Avg : %f Cumalative Prob : (log) %f => %f \n", 
                count, max_prob, avg_prob, 
                cum_prob, exp(cum_prob));
 
    annDeallocPt(qp);
    delete [] nnIdx;
    delete [] dists; 
    int64_t e_utime = bot_timestamp_now();
    if(verbose){
        fprintf(stderr, "\t Total : %d => Matched Count : %d\n", tocheck_scan->numPoints, count);
        fprintf(stderr, "\t Valid Trees : %d => Total Searches : %d Time per search : %f\n", valid_trees, kd_searches, (time_to_search) / 1.0e6 / (double) kd_searches);
        fprintf(stderr, "Time to serach kd tree : %f\n", (time_to_search) / 1.0e6);
    }
    
    bot_lcmgl_switch_buffer (lcmgl_sm_graph);
    cum_prob -= log(max_prob) * count; 
    //return exp(cum_prob);
    return avg_prob; //exp(cum_prob); //avg_prob / count; //exp(cum_prob); //count / (double) tocheck_scan->numPoints;
}

double SlamParticle::getProbOfMeasurement(SlamNode *last_nd){
    Pose2d tocheck_value = last_nd->getPose();

    // Clear the copy of the local occupancy grid
    memset (local_px_map->data, 0, local_px_map->num_cells*sizeof(float));

    double dist =0;
    double pos[2], next_pos[2];
    double start[2];


    // Node->Slam_Pose->Scan laser scans are decimated and in the body frame
    // We will generate the local occupancy grid in the Query Body frame
    BotTrans localToQueryBody;
    localToQueryBody.trans_vec[0] = tocheck_value.x();
    localToQueryBody.trans_vec[1] = tocheck_value.y();
    localToQueryBody.trans_vec[2] = 0.0;
    double rpyQueryBody[3] = {0.0, 0.0, tocheck_value.t()};
    bot_roll_pitch_yaw_to_quat (rpyQueryBody, localToQueryBody.rot_quat);
    bot_trans_invert (&localToQueryBody);

    BotTrans matchBodyToLocal;
    //BotTrans matchLaserToLocal;
    BotTrans matchBodyToQueryBody;

    double rpyMatchBody[3] = {0.0, 0.0, 0.0};
    double laserStartMatchBody[3] = {0.4460, -0.0101, 0.1};
    double laserStartQueryBody[3];
    double pMatchBody[3] =  {0.0, 0.0, 0.0};
    double pMatchBodyNext[3] = {0.0, 0.0, 0.0};
    double pQueryBody[3];
    double pQueryBodyNext[3];

    // Find the nodes (and scans) near the local search area
    for(int k = 0; k < last_nd->id; k++){
        SlamNode *nd = graph->getSlamNode(k);
        Pose2d value = nd->getPose();

        Pose2d delta = tocheck_value.ominus(value);
        dist = hypot(delta.x(), delta.y());
        if(dist < 10.0){
            //fprintf(stderr, "[%d] - at dist : %f - adding for the simulation\n", (int) nd->id, dist);
            //loop through the points and add
            ScanTransform T;
            T.x = value.x();
            T.y = value.y();
            T.theta = value.t();

            Pose2d match = nd->getPose();

            // Determine the transform from Match Body ---> Query Body
            
            // Set the BotTrans from Match BODY to LOCAL
            matchBodyToLocal.trans_vec[0] = match.x();
            matchBodyToLocal.trans_vec[1] = match.y();
            matchBodyToLocal.trans_vec[2] = 0.0;
            rpyMatchBody[2] = match.t();
            bot_roll_pitch_yaw_to_quat (rpyMatchBody, matchBodyToLocal.rot_quat);

            // ... and now Match Body ---> Query Body
            bot_trans_apply_trans_to (&localToQueryBody, &matchBodyToLocal, &matchBodyToQueryBody);

            Scan *scan = nd->slam_pose->scan;

            float clamp[2] = {0,1.0} ;
            float ray_trace_clamp[2] = {-11.0, 6};

            // Transform Match Laser (0,0,0) ---> Query Laser frame
            bot_trans_apply_vec ( &matchBodyToQueryBody, laserStartMatchBody, laserStartQueryBody);
            start[0] = laserStartQueryBody[0];
            start[1] = laserStartQueryBody[1];

            //start[0] = bodyToLocal.trans_vec[0];
            //start[1] = bodyToLocal.trans_vec[1];

            for (unsigned i = 0; i < scan->numPoints; i++) {

                pMatchBody[0] = scan->points[i].x;
                pMatchBody[1] = scan->points[i].y;

                bot_trans_apply_vec ( &matchBodyToQueryBody, pMatchBody, pQueryBody);
                pos[0] = pQueryBody[0];
                pos[1] = pQueryBody[1];

                //px_map->rayTrace(start, pos, -2, 4.24, ray_trace_clamp);
                local_px_map->rayTrace(start, pos, -1, 2, ray_trace_clamp);
                
                //doesn't work well - prob because of the scans hitting the floor 
                if(0){
                    if(i < scan->numPoints-1){
                        pMatchBodyNext[0] = scan->points[i+1].x;
                        pMatchBodyNext[1] = scan->points[i+1].y;

                        bot_trans_apply_vec ( &matchBodyToQueryBody, pMatchBodyNext, pQueryBodyNext);
                        next_pos[0] = pQueryBodyNext[0];
                        next_pos[1] = pQueryBodyNext[1];
                    
                        //px_map->rayTrace(start, pos, -2, 4.24, ray_trace_clamp);
                        local_px_map->rayTrace(pos, next_pos, 1, 0, ray_trace_clamp);
                        //draw line 
                    
                    }
                }
            }
        }            
    }

    //lets publish the pixelmap also 
    if(1){
        const occ_map_pixel_map_t *map = local_px_map->get_pixel_map_t(bot_timestamp_now());
        occ_map_pixel_map_t_publish(lcm, "PIXEL_MAP", map);
    }

    LaserSim2D laserSim(local_px_map, 1081, -2.3561945, 0.004363323, 30.0);

    Pose2d value = last_nd->getPose();
    BotTrans bodyToLocal;
    
    /* Original Approach: Convert to laser frame
    //the laser needs to be pulled from the laser pose - not the robot pose
    bodyToLocal.trans_vec[0] = value.x();
    bodyToLocal.trans_vec[1] = value.y();
    bodyToLocal.trans_vec[2] = 0.0;
    double rpy1[3] = { 0, 0, value.t()};
    bot_roll_pitch_yaw_to_quat(rpy1, bodyToLocal.rot_quat);
    double pBody[3] = { 0.45, 0, 0 };
    double pLocal[3];
    bot_trans_apply_vec(&bodyToLocal, pBody, pLocal);

    BotTrans laserToLocal;
        
    //the laser needs to be pulled from the laser pose - not the robot pose
    laserToLocal.trans_vec[0] = pLocal[0];
    laserToLocal.trans_vec[1] = pLocal[1];
    laserToLocal.trans_vec[2] = 0.0;
    double rpy[3] = { 0, 0, value.t()};
    bot_roll_pitch_yaw_to_quat(rpy, laserToLocal.rot_quat);

    const bot_core_planar_lidar_t *sim_laser = laserSim.simulate(&laserToLocal, bot_timestamp_now());
    */


    const bot_core_planar_lidar_t *sim_laser = laserSim.simulate(0, 0, 0, bot_timestamp_now());
    double prob = 0.0;
        
    if(last_nd->slam_pose->laser != NULL){
        //lets assume that these scans are aligned as they should be 
        prob = compareScans(sim_laser, last_nd->slam_pose->laser);
        //double prob2 = scanLikelihood(sim_laser, last_nd->slam_pose->laser);
    }

    if(0){
        bot_core_planar_lidar_t_publish(lcm,"SIM_SLAM_LASER", sim_laser);
        bot_core_planar_lidar_t_publish(lcm,"COM_SLAM_LASER", last_nd->slam_pose->laser);
    }

    return prob; //HACK - just to see if it makes particle diversity more reasonable
}

//try to scanmatch - and see how much we need to move to get a resonable scanmatch 
//and use the uncertainity of the pose to come to 
//a prob estimate
double SlamParticle::getProbOfMeasurementSM(SlamNode *last_nd){
    Pose2d tocheck_value = last_nd->getPose();
    double dist = 0;
    // Find the nodes (and scans) near the local search area
    for(int k = 0; k < last_nd->id; k++){
        SlamNode *nd = graph->getSlamNode(k);
        Pose2d value = nd->getPose();

        Pose2d delta = tocheck_value.ominus(value);
        dist = hypot(delta.x(), delta.y());
        if(dist < 10.0){
            //fprintf(stderr, "[%d] - at dist : %f - adding for the simulation\n", (int) nd->id, dist);
            //loop through the points and add
            ScanTransform T;
            T.x = value.x();
            T.y = value.y();
            T.theta = value.t();

        // applyTransform copies everything from T to nd->slam_pose->scan->T
        // so we should populate all of T first
        T.score = nd->slam_pose->scan->T.score;
        T.hits = nd->slam_pose->scan->T.hits;
        for (int i=0; i<9; i++)
            T.sigma[i] = nd->slam_pose->scan->T.sigma[i];

            nd->slam_pose->scan->applyTransform(T);
            sm->addScan(nd->slam_pose->scan, false);
            
        }            
    }
    sm->addScan(NULL, true);     
    
    ScanTransform T;
    T.x = tocheck_value.x();
    T.y = tocheck_value.y();
    T.theta = tocheck_value.t();

    // applyTransform copies everything from T to last_nd->slam_pose->scan->T
    // so we should populate all of T first
    T.score = last_nd->slam_pose->scan->T.score;
    T.hits = last_nd->slam_pose->scan->T.hits;
    for (int i=0; i<9; i++)
        T.sigma[i] = last_nd->slam_pose->scan->T.sigma[i];
    

    last_nd->slam_pose->scan->applyTransform(T);

    ScanTransform lc_r = sm->gridMatch(last_nd->slam_pose->scan->points, last_nd->slam_pose->scan->numPoints,
                                       &last_nd->slam_pose->scan->T, 0.2, 0.2, M_PI/24.0);//1.0, 1.0, M_PI / 12.0);

    double hit_pct = lc_r.hits/ (double) last_nd->slam_pose->scan->numPoints;
    sm->clearScans(false);   

    /*Matrix3d cov_sm = Matrix3d::Zero();
      cov_sm(0,0) = 0.00002;
      cov_sm(1,1) = 0.00002;
      cov_sm(2,2) = 0.00001;
    */
    //double prob = graph->calculateProbability(node1, node2,cov_sm, *constraint_tf->transform);
    
    //for now 
    return hit_pct;//prob; //HACK - just to see if it makes particle diversity more reasonable
}


// Implementation of the phit model from Probabilistic Robotics
double
SlamParticle::pHit (double z_star, double z)
{
    double sigma_hit = 0.2;
    double prob;
    if (z > SCAN_Z_MAX || z_star >= SCAN_Z_MAX)
        prob = 0.0;
    else {

        double prob_gsl = gsl_ran_gaussian_pdf (z-z_star, sigma_hit);

        // Unnormalized probability
        prob = (1/sqrt(2*M_PI*sigma_hit*sigma_hit)) 
            * exp(-0.5 * pow((z-z_star)/sigma_hit, 2));
                  
        // Normalization constant p(0 <= z <= zmax)
        // Using a change of variables e=z-z_star: p(-z_star <= e <= zmax-z_star) 
        //   = cdf(e; zmax-z_star) - cdf (e; -z_star)
        double eta = gsl_cdf_gaussian_P (SCAN_Z_MAX-z_star, sigma_hit) - gsl_cdf_gaussian_P (-z_star, sigma_hit);
        
        //fprintf (stdout, "++++++++ z = %.4f, z_star = %.4f, prob = %.10f, eta = %.10f, prob_gsl = %.10f\n", 
        //         z, z_star, prob, eta, prob_gsl);

        prob = prob/eta;
    }

    return prob;
}

// Implementation of the pshort model from Probabilistic Robotics
double
SlamParticle::pShort (double z_star, double z)
{
    double lambda = 0.1;
    double prob;
    if (z > z_star)
        prob = 0;
    else {
        prob = lambda * exp(-1 * lambda * z);
        
        // Normalization constant
        double eta = 1 - exp (-1*lambda*z_star);

        prob = prob/eta;
    }

    return prob;
}
        
            

// Determine the likelihood of a the actual_laser scan based upon the ray-traced version from an occupancy grid
// Algorithm implements the beam_range_finder_model() from Probabilistic Robotics
double 
SlamParticle::scanLikelihood(const bot_core_planar_lidar_t *sim_laser, bot_core_planar_lidar_t *actual_laser)
{
    // Mixing likelihoods
    double a_hit = 0.5;
    double a_short = 0.3;
    double a_max = 0.1;
    double a_rand = 0.1;

    double q = 0.0;
    double p;

    for (int i=0; i<actual_laser->nranges; i++) {
        // Ignore this scan if (i) actual range is less than min (ii) simulated range is less than min
        // or (iii) bot actual range and simulated range are greater than max
        if ( (actual_laser->ranges[i] < SCAN_Z_MIN) || (sim_laser->ranges[i] < SCAN_Z_MIN) ||
             ((actual_laser->ranges[i] > SCAN_Z_MAX) && (sim_laser->ranges[i] > SCAN_Z_MAX)))
            continue;

        double p_hit = pHit (sim_laser->ranges[i], actual_laser->ranges[i]);
        double p_short = pShort (sim_laser->ranges[i], actual_laser->ranges[i]);
        double p_rand = 1/SCAN_Z_MAX;
        double p_max = 0;

        if (actual_laser->ranges[i] == SCAN_Z_MAX)
            p_max = 1;

        
        p = a_hit * p_hit + a_short * p_short + a_max * p_max + a_rand * p_rand;
        if (verbose)
            fprintf (stdout, "----- p = %.4f, log(p) = %.4f, p_hit = %.4f, p_short = %.4f, p_max = %.4f, p_rand = %.4f\n",
                 p, log(p), p_hit, p_short, p_max, p_rand);


        q = q + log(p);
    }


    // Convert back to probabilities
    return exp(q);

}


double SlamParticle::compareScans(const bot_core_planar_lidar_t *sim_laser, bot_core_planar_lidar_t *actual_laser){
    int matched_count = 0;
    int scan_dist = 4;
    double acceptance_threshold = 0.1;
    for(int i=0; i < actual_laser->nranges; i++){
        int min_ind = fmax(0, i-scan_dist);
        int max_ind = fmin(actual_laser->nranges, i+scan_dist);
        if(actual_laser->ranges[i] < 0.01 || actual_laser->ranges[i] > 20.0)
            continue;
        for(int j=min_ind; j < max_ind; j++){
            if(sim_laser->ranges[i] < 0.01 || sim_laser->ranges[i] > 20.0)
                continue;
            if(fabs(actual_laser->ranges[i] - sim_laser->ranges[j]) < acceptance_threshold){
                matched_count++;
                break;
            }
        }
    }
    return matched_count / (double) actual_laser->nranges;
}


// Compare label similarity
double SlamParticle::calculateSimilarity (LabelDistribution *ldist1, LabelDistribution *ldist2) {

    
    vector<double> mu1 (ldist1->observation_frequency);
    vector<double> mu2 (ldist2->observation_frequency);

    for (int i=0; i < mu1.size(); i++) {
        mu1.at(i) = mu1.at(i)/ldist1->total_obs;
        mu2.at(i) = mu2.at(i)/ldist2->total_obs;
    }

    double dot_product = 0;
    for (int i=0;  i < mu1.size(); i++) 
        dot_product += mu1.at(i) * mu2.at(i);

    return dot_product;
}
        

// Sample a set of language edges
vector <slam_language_edge_t *> SlamParticle::proposeLanguageEdges (map<nodePairKey, double> edge_similarities, int no_edges) {
    int edges_to_sample = no_edges; 
    if(edges_to_sample > edge_similarities.size()){
        edges_to_sample = edge_similarities.size();
    }
    
    //we should not replace the selected edge pair before resampling 
    map<nodePairKey, double> to_sample_edges =  edge_similarities;

    vector<slam_language_edge_t *> new_language_edges;
    while(new_language_edges.size() < edges_to_sample && to_sample_edges.size() > 0){      
        
        // Normalize the similarities
        double sum_similarities = 0;
        map <nodePairKey, double>::iterator it;
    
        for (it = to_sample_edges.begin(); it != to_sample_edges.end(); it++)
            sum_similarities += (*it).second;

        // Compute accummulated similarities
        double prev_sum_similarities = 0;
        for (it = to_sample_edges.begin(); it != to_sample_edges.end(); it++) {
            (*it).second = (*it).second/sum_similarities + prev_sum_similarities;
            prev_sum_similarities = (*it).second;
        }   
            
        // Draw a uniform random number
        double u_rand = gsl_rng_uniform (rng);

        int idx = 0;
        nodePairKey selected_pair;
        for (it = to_sample_edges.begin(); it != to_sample_edges.end(); it++) {
            selected_pair = it->first;
            if (u_rand <= (*it).second)
                break;
            idx += 1;
        }

        if (verbose)
            fprintf (stdout, "--------------- u_rand = %.2f, match_idx = %d, node1 = %d, node2 = %d\n", 
                     u_rand, idx, selected_pair.first, selected_pair.second);

        slam_language_edge_t *new_edge = (slam_language_edge_t *) calloc (1, sizeof (slam_language_edge_t));
        new_edge->node_id_1 = selected_pair.first;
        new_edge->node_id_2 = selected_pair.second;
        new_edge->type = 0; // same location
        new_edge->transformation[0] = 0;
        new_edge->transformation[1] = 0;

        // Keep track of the list of proposed edges
        nodePairKey node1_node2 (selected_pair.first, selected_pair.second);
        nodePairKey node2_node1 (selected_pair.second, selected_pair.first);
        graph->proposed_language_edges.push_back (node1_node2);
        graph->proposed_language_edges.push_back (node2_node1);
        
        to_sample_edges.erase(it);
        new_language_edges.push_back (new_edge); //slam_language_edge_t_copy(new_edge));
    }

    return new_language_edges;
}
    

// Bleed from previous supernode
int SlamParticle::bleedTemporal (SlamNode *new_node){//, int lang_update_id) {
    
    int prev_supernode_id = graph->getPreviousSupernodeID (new_node->id, SUPERNODE_SEARCH_DISTANCE);
    if (prev_supernode_id != -1) {
        SlamNode *prev_node = (SlamNode *) graph->getSlamNode (prev_supernode_id);
        
        LabelDistribution *ld_src = prev_node->labeldist;
        return new_node->labeldist->bleedLabels (ld_src);
    }
    return 0;
}

// 4) Propose new language edges
int SlamParticle::addLanguageEdges(SlamGraph *basic_graph, 
				   map<int, SlamNode *> lang_updated_nodes){

    //flip a coin and decide whether we want to add a language or not
    
    double u_rand = gsl_rng_uniform (rng);
    if (u_rand < PROB_PROPOSE_LANGUAGE_EDGE)
        return 0;

    int added_lang = 0;
    map <nodePairKey, double> edge_similarities;
    //for (int i=0; i < graph->slam_node_list.size(); i++) {
    map<int, SlamNode *>::iterator n_it;
    for ( n_it= lang_updated_nodes.begin() ; n_it != lang_updated_nodes.end(); n_it++ ){        
        SlamNode *node1 = n_it->second;
        if (verbose)
            fprintf(stderr, "Considering Lanuage for Node : %d\n", node1->id);

        // Skip this node if we don't have any direct or bled observations 
        // or it's not a supernode
        if (node1->labeldist->total_obs <= 1 || !node1->slam_pose->is_supernode)
            continue;

        // Check the saliency of the node's labels
        if (!node1->labeldist->areLabelsSalient()) 
            continue;
    
        // Iterate over other nodes
        for (int j=0; j < graph->slam_node_list.size(); j++) {
            SlamNode *node2 = graph->slam_node_list.at (j);
            
            if(!node2->slam_pose->is_supernode)
                continue;

            if (verbose)
                fprintf(stderr, "Checking similarity between nodes %d - %d\n", node1->id, node2->id);
            // Skip this node if:
            //    (i)    it's the same as node1, 
            //    (ii)   we don't have any direct or bled observations, 
            //    (iii)  it's not a supernode,
            //    (iv)   there is already a constraint between the two
            //    (v)    the node's labels aren't salient
            //    (vi)   the pair are consecutive  
            //    (vii)  the nodes were bled from the same original labeled node
            //    (viii) we've already proposed a language edge
		
            // (i), (ii), (iii), and (iv)
            if (node2->labeldist->total_obs <= 1) {
                if(verbose)
                    fprintf (stdout, "\t\t rejected - not enough obs\n");
                continue;
            }
            if ((node1->id == node2->id) || graph->hasConstraint (node1->id, node2->id)) {
                if(verbose)
                    fprintf (stdout, "\t\t rejected = same node id or already have constraint\n");
                continue;
            }
            
            // (v) Check the saliency of the node's labels 
            if (!node1->labeldist->areLabelsSalient()) {
                if(verbose)
                    fprintf (stdout, "\t\t rejected - not salient\n");
                continue;
            }
            
            // (vi) Check that they are temporal
            if ( (graph->getPreviousSupernodeID (node1->id, SUPERNODE_SEARCH_DISTANCE) == node2->id) ||
                 (graph->getPreviousSupernodeID (node2->id, SUPERNODE_SEARCH_DISTANCE) == node1->id)) {
                if(verbose)
                    fprintf (stdout, "\t\t rejected - edges are temporal\n");
                continue;
            }
            
            // (vii) Check that they weren't bled from the same original labeled node
            vector <int>::iterator lit;
            int have_same_source = 0;
            for (int k = 0; k < node1->labeldist->labeled_node_ids.size(); k++) {
                lit = find (node2->labeldist->labeled_node_ids.begin(), 
                            node2->labeldist->labeled_node_ids.end(), node1->labeldist->labeled_node_ids.at(k));
                if (lit != node2->labeldist->labeled_node_ids.end()) {
                    have_same_source = 1;
                    break;
                }
            }
            
            if (have_same_source) {
                if(verbose)
                    fprintf (stdout, "\t\t rejected = have same source\n");
                continue;
            }
            
            
            // (viii) Skip if we've already proposed a language edge between the two
            vector <nodePairKey>::iterator it;
            nodePairKey node1_node2 (node1->id, node2->id);
            nodePairKey node2_node1 (node2->id, node1->id);
            if ( (edge_similarities.find(node1_node2) != edge_similarities.end()) ||
                 (edge_similarities.find(node2_node1) != edge_similarities.end())) {
                fprintf (stdout, "\t\t rejected - we just tried this pair\n");
                continue;
            }
            
            it = find (graph->proposed_language_edges.begin(), graph->proposed_language_edges.end(), node1_node2);
            if (it != graph->proposed_language_edges.end()) {
                fprintf (stdout, "\t\t rejected - it's already been proposed\n");
                continue;
            }
            
            if (verbose)
                fprintf(stderr, "Made it past first check %d - %d\t", node1->id, node2->id);
            
            double dot_product = calculateSimilarity (node1->labeldist, node2->labeldist);
            
            // Apply threshold
            if (dot_product < MIN_EDGE_SIMILARITY){
                if (verbose)
                    fprintf(stderr, "Rejected\n");
                fprintf (stdout, "\t\t rejected - similarity is %.4f < %.4f\n",
                         dot_product, MIN_EDGE_SIMILARITY);
                continue;
            }
            
            fprintf (stdout, "\t\t Added\n");
            
            if (verbose)
                fprintf(stderr, "Added\n");
            nodePairKey edge_pair (node1->id, node2->id);
            edge_similarities.insert (std::make_pair (edge_pair, dot_product));
            
        }
    }
    
    vector<slam_language_edge_t *> new_language_edges;
    
    if (!edge_similarities.empty()) 
        new_language_edges = proposeLanguageEdges (edge_similarities, NO_OF_LANG_EDGES);

    fprintf(stderr, "No of New Language Edges proposed : %d / %d\n", new_language_edges.size(), 
            edge_similarities.size());

    if (new_language_edges.size() > 0)
        fprintf(stdout, MAKE_GREEN "READY TO ADD LANGUAGE %d CONSTRAINTS" RESET_COLOR "\n",
                (int) new_language_edges.size());
    
    for(int i=0; i<new_language_edges.size(); i++){        
        if(useICP==false){            
            fprintf (stdout, "ERROR: useICP=FALSE not supported\n");
        }
        else{
            SlamNode *node1 = graph->getSlamNode(new_language_edges.at(i)->node_id_1);
            SlamNode *node2 = graph->getSlamNode(new_language_edges.at(i)->node_id_2);

            //check the cache - to see if we already scanmatched these two regions 
            //nodePairKey key(node1->id, node2->id);
            //nodePairKey key_invert(node2->id, node1->id);

            map<int, PoseToPoseTransform *>::iterator it;
            it = node1->slam_pose->sm_lang_constraints.find(node2->id);
            fprintf(stderr, "Checking cache for exisiting constraints\n");
            if(it != node1->slam_pose->sm_lang_constraints.end()){
                fprintf(stderr, "Found cached constraint for %d - %d\n", (int) node1->id, (int) node2->id);
                //use the value 
                PoseToPoseTransform *matched_tf = it->second; 
                if(matched_tf->accepted == 1){
                    fprintf(stderr, "\tConstraint matched - adding\n");
                    SlamConstraint *sl_ct = graph->addConstraint(node1, node2, node1, node2, *matched_tf->transform, *matched_tf->noise, matched_tf->hitpct, SLAM_GRAPH_EDGE_T_TYPE_LANGUAGE_FULL, SLAM_GRAPH_EDGE_T_STATUS_SUCCESS);
                    //continue;
                     break;
                }
                else{
                    if(verbose)
                        fprintf(stderr, "\tConstraint had failed - skipping\n");
                    if(addDummyConstraints)
                        graph->addDummyConstraint(node1, node2, node1, node2, *matched_tf->transform, *matched_tf->noise, matched_tf->hitpct, SLAM_GRAPH_EDGE_T_TYPE_LANGUAGE_FULL, SLAM_GRAPH_EDGE_T_STATUS_FAILED);
                    continue;
                }
            }
            fprintf(stderr, "Constraint not found\n");
                        
            //tell the scanmatcher to use the prior in the transform 
            Pose2d transf(0,0,0);

            double cov_high[9] = { .02, 0, 0, 0, .02, 0, 0, 0, .01 };
            Matrix3d cv_high(cov_high);
            Noise *cov_hc = new Covariance(cv_high);
            int accepted = 0;
            int64_t used_ind_1, used_ind_2;
            double hit_percentage = 0;

            ScanTransform lc_r = doScanMatchLanguageICP(basic_graph, new_language_edges.at(i)->node_id_1, 
                                                        new_language_edges.at(i)->node_id_2, 
                                                        &transf, 
                                                        cov_hc, 
                                                        &accepted, 
                                                        &used_ind_1, 
                                                        &used_ind_2, 
                                                        &hit_percentage, 1);

            NodeScan *pose_curr = node1->slam_pose;
            NodeScan *pose_to_match = node2->slam_pose;
            Pose2d value_1 = node1->getPose();
            Pose2d value_2 = node2->getPose();

            Pose2d delta = value_1.ominus(value_2);

            double ref_dist = hypot(delta.x(), delta.y());
            double ref_angle = delta.t();

            //we dont need to be selective about the transform for this method (as the prior doesnt influence the result) 
            PoseToPoseTransform *constraint_tf= new PoseToPoseTransform(new Pose2d(transf), 
                                                                        pose_curr, 
                                                                        pose_to_match, 
                                                                        SLAM_GRAPH_EDGE_T_TYPE_LANGUAGE_FULL,
                                                                        cov_hc, 
                                                                        hit_percentage, 
                                                                        ref_dist, 
                                                                        ref_angle, accepted);

            BotTrans nd_2_sm_to_nd_1 = getBotTransFromPose(transf);
            bot_trans_invert(&nd_2_sm_to_nd_1);
            Pose2d transf_inv = getPoseFromBotTrans(nd_2_sm_to_nd_1);
            Noise *cov_hc_inv = new Covariance(cv_high);

             //we dont need to be selective about the transform for this method (as the prior doesnt influence the result) 
            PoseToPoseTransform *constraint_tf_inv = new PoseToPoseTransform(new Pose2d(transf_inv), 
                                                                             pose_to_match, 
                                                                             pose_curr, 
                                                                             SLAM_GRAPH_EDGE_T_TYPE_LANGUAGE_FULL,
                                                                             cov_hc_inv, 
                                                                             hit_percentage, 
                                                                             ref_dist, 
                                                                             ref_angle, accepted);
            
            fprintf(stderr, "Done scanmatching regions : %d-%d => Adding to cache \n", 
                    (int) node1->id, (int) node2->id);//, (void *)  languageSMCache);
            
            node1->slam_pose->sm_lang_constraints.insert(make_pair(node2->id, constraint_tf));
            //adding the inverse 
            node2->slam_pose->sm_lang_constraints.insert(make_pair(node1->id, constraint_tf_inv));

            fprintf(stderr, "Done insert\n");

            if(accepted == 1){
                added_lang = 1;
                
                SlamConstraint *sl_ct = graph->addConstraint(node1, node2, node1, node2, *constraint_tf->transform, *constraint_tf->noise, constraint_tf->hitpct, SLAM_GRAPH_EDGE_T_TYPE_LANGUAGE_FULL, SLAM_GRAPH_EDGE_T_STATUS_SUCCESS);
                fprintf(stderr, "Adding Scanmatch based Loop closure constraint\n");
                break;
            }
            else{
                fprintf(stderr, "Failed to add Language Based scanmatch constraint - Scanmatcher failed\n");
                if(addDummyConstraints)
                    graph->addDummyConstraint(node1, node2, node1, node2, *constraint_tf->transform, *constraint_tf->noise, constraint_tf->hitpct, SLAM_GRAPH_EDGE_T_TYPE_LANGUAGE_FULL, SLAM_GRAPH_EDGE_T_STATUS_FAILED);

            }
        }
    }

    return added_lang;
}

void SlamParticle::addComplexLanguageUpdates(SlamGraph *basic_graph, slam_slu_result_t *slu_msg){

    
    map<int, SlamNode *> lang_updated_nodes;
    //should have only one label - but just to be safe 
    for(int i = 0; i < slu_msg->no_labels; i++){
        slam_node_label_list_t l_result = slu_msg->labels[i];
        fprintf(stderr, "Received updates for label : %d\n", l_result.label);
        
        for(int j = 0; j < l_result.num; j++){
            fprintf(stderr, "Node : %d - Prob : %f\n", (int) l_result.nodes[j].id, l_result.nodes[j].prob);

            SlamNode *node = (SlamNode *) graph->getSlamNode(l_result.nodes[j].id);
            LabelDistribution *ld_src = node->labeldist;
            if(l_result.nodes[j].prob < 0.2)
                continue;

            lang_updated_nodes.insert(pair<int, SlamNode *> (node->id, node));

            //add the event id here 
            ld_src->addObservation(l_result.label, l_result.nodes[j].prob, language_event_count);      
        }
    }

    language_event_count++;

    //lets call the language edge creation 
    int added_lang = addLanguageEdges(basic_graph, lang_updated_nodes);
    
    //call the propose edge function 
}

void SlamParticle::addNodesAndEdges(vector<NodeScan *> new_nodes, SlamGraph *basic_graph,
                                    vector<slam_language_label_t *> new_language_labels, int probMode,
                                    //vector<slam_language_edge_t *> new_language_edges, int probMode, 
                                    int find_ground_truth, double *maximum_probability, int ignoreLanguageForEdges){
    map<int, SlamNode *>::iterator it;

    // Steps:
    //  1) Add new nodes to graph (includes bleeding from previous supernode)
    //  2) Add new loop closure edges (includes bleeding between supernodes)
    //  3) Apply simple language:
    //      a) Update Dirichlet for preceding supernode
    //      b) Bleed to neighboring nodes
    //  4) Propose language edges

    verbose = 1;
    SlamNode *final_node = NULL;
    int max_node_ind = -1;

    map<int, SlamNode *> lang_updated_nodes;

    int node_added = 0;

    //fprintf (stdout, "-------------------------- new_language_labels.size() = %d\n", new_language_labels.size());

    for(int i=0; i < new_nodes.size(); i++){
        NodeScan *pose = (NodeScan *) new_nodes.at(i);
        if(!pose) {
            fprintf (stdout, "addNodesAndEdges: new_node pose is NULL!\n");
            continue;
        }
        
        int node_ind = pose->node_id;
 
        SlamNode *c_node = graph->getSlamNode(node_ind);

        if(c_node != NULL){
            if(verbose)
                fprintf(stderr, "Node [%d] found in Slamgraph - skipping => Slam Pose Pointer %p\n", node_ind, (void *) c_node->slam_pose); 
            
            if(max_node_ind < node_ind)
                final_node = c_node;
            
            continue;
        }

        SlamNode *node = graph->addSlamNode(pose);   

        graph->updateSupernodeIDs();

        // Bleed previous temporal supernode
        if (node->slam_pose->is_supernode){
            int bled = bleedTemporal (node);

            if(bled){
                if (verbose)
                    fprintf(stderr, "Bleeding Temporal : %d\n", (int) node->id);
                lang_updated_nodes.insert(pair<int, SlamNode *> (node->id, node));//.push_back(node);
            }
        }
        
        if(verbose)
            fprintf(stderr, "Node [%d] adding\n", node_ind);
        node_added = 1;

        if(max_node_ind < node_ind)
            final_node = node;
    }

    if(final_node == NULL)
        fprintf(stderr, "Error - No Last Pose\n");
    
    if(verbose){
        fprintf(stderr, "\n============================================================\n");
        fprintf(stderr, "Particle : %d - New nodes to add : %d\n", 
                graph_id, (int) new_nodes.size());
        fprintf(stderr,"Adding basic constraints\n");
    }

    //add the constraint to the origin node
    if(!graph->status){
        if(verbose){
            fprintf(stderr, "Adding constraint to the origin\n");
        }

        //the constraint to the prior is not going to be in the edge list // need to add that before 
        int first_node_id = 0;

        SlamNode *node = graph->getSlamNode(first_node_id);//graph->slam_nodes.find(first_node_id)->second;
        
        if(node == NULL){
            fprintf(stderr, "First node not found - this should not have happened\n");
        }
        else{
            if(verbose){
                fprintf(stderr, "First node found in Slam graph - adding constraint\n"); 
            }

            //this one doesnt matter - set to small for either type
            if(node->slam_pose->inc_constraint_sm != NULL){
                PoseToPoseTransform *p2p_constraint = node->slam_pose->inc_constraint_sm; 
                double cov_hardcode[9] = { .0001, 0, 0, 0, .0001, 0, 0, 0, .0001 };
                Matrix3d cv_hardcode(cov_hardcode);
                Noise cov_hc= SqrtInformation(10000. * eye(3));//Covariance(cv_hardcode);
                graph->addOriginConstraint(node, *p2p_constraint->transform, *p2p_constraint->noise);
            }
        }
    }

    if(verbose){
        fprintf(stderr, "Done\n");
    }

    vector<SlamNode *> nodes_to_process; 

    int inc_edge_added = 0;

    for(int i=0; i < graph->slam_node_list.size(); i++){
        SlamNode *node1 = graph->slam_node_list.at(i);
        if(node1->processed)  //we already added edges to this guy - skipping 
            continue;
    
        if(node1->id == 0){ //first node - no inc constraint
            node1->processed = 1;
            continue;
        }
        
        int prev_id = node1->id - 1;
        SlamNode *node2 = graph->getSlamNode(prev_id);

        PoseToPoseTransform *p2p_constraint = NULL;
        
        int32_t type, status;        

        // Use only odometry if requested, otherwise use scanmatching and fall back
        // on odometry if the incremental scan match is bad
        if ((scanmatchBeforeAdd) && (node1->slam_pose->inc_constraint_sm->hitpct > inc_sm_acceptance_threshold)) {
            type = SLAM_GRAPH_EDGE_T_TYPE_SM_INC;
            p2p_constraint = node1->slam_pose->inc_constraint_sm;
        }
        else {
            type =  SLAM_GRAPH_EDGE_T_TYPE_ODOM_INC;
            p2p_constraint = node1->slam_pose->inc_constraint_odom;
            if (scanmatchBeforeAdd && verbose)
                fprintf (stdout, "--------------- Scan Match Failed. Falling back on Odometry ---------------\n");
        }
        
        if(p2p_constraint == NULL || p2p_constraint->node_relative_to == NULL){
            fprintf (stdout, "p2p_constraint is NULL\n");
            continue;
        }
        
        if(node1 == NULL || node2 == NULL){
            fprintf(stderr, "Error - one or more nodes not found\n");
            continue;
        }
        
        if(p2p_constraint->node_relative_to->node_id != prev_id){
            if(verbose){
                fprintf(stderr, "Not an incremental constraint - skipping for now\n");
            }
            continue;
        }

        //check if the constraint exists - and if so add the constraint 
        //node id is the constraint respective to - i.e. current_node (id_1) respective to (id_2)
        //so find the constraint if it exists 
        
        graph->addConstraint(node1, node2, node1, node2, *p2p_constraint->transform, *p2p_constraint->noise, 
                             p2p_constraint->hitpct, type, SLAM_GRAPH_EDGE_T_STATUS_SUCCESS);
        
        
        
        if(!node1->slam_pose->is_supernode){
            node1->processed = 1;
            if(probMode != 0){
                unprocessed_slam_nodes.push_back(node1);
            }
        }

        else{
            nodes_to_process.push_back(node1);
            unprocessed_slam_nodes.push_back(node1);
        }

        inc_edge_added = 1;

        if(verbose)
            fprintf(stderr, "\tAdding new constraints - %d %d\n", (int) node1->id, (int) node2->id);
    }
    
    if(verbose)
        fprintf(stderr, "No Nodes to process : %d\n", (int) nodes_to_process.size());
    

    int64_t end_constraint = bot_timestamp_now();

    //run optimization - basic part of the graph has changed
    if(inc_edge_added || node_added){
        graph->runSlam();
    }

    // 2) Add the loop closures. When non language constraints are added between supernodes,
    //    their Dirichlet's are bled.
    addLoopClosures(nodes_to_process, basic_graph, find_ground_truth);

    // 3) Process the new_language_labels by updating the Dirichlets

    
    for (int i=0; i < new_language_labels.size(); i++) {
        slam_language_label_t *label = new_language_labels.at(i);

        int node_id = -1;
        // Look for the most recent supernode with more recent utime

        //finds the SN nodes to which the language belongs to 
        vector<SlamNode *>::reverse_iterator rit;
        for (rit = graph->slam_node_list.rbegin(); rit < graph->slam_node_list.rend(); ++rit) {
            SlamNode *node = (SlamNode *) *rit;
            if (node->slam_pose->is_supernode) {
                if (node->slam_pose->utime > label->utime)
                    node_id = node->id;
                else{ 
                    break;
                }
            }
        }
        
        if (node_id != -1) {            
            SlamNode *ref_node = (SlamNode *) graph->getSlamNode (node_id);
            if (verbose)
                fprintf(stderr, "Adding Current Language : %d\n", (int) ref_node->id);

            lang_updated_nodes.insert(pair<int, SlamNode *> (ref_node->id, ref_node));
            // Update the Dirichlet for this node

            ref_node->labeldist->addDirectObservation (label->label, LABEL_INCREMENT, language_event_count);
            if (verbose)
                fprintf (stdout, "== Incorporating direct label to update node %d\n", node_id);
            
            // Bleed to supernodes that are either directly connected (except via language constraint)
            // or connected temporally via minor nodes

            int prev_supernode_id = graph->getPreviousSupernodeID (node_id, SUPERNODE_SEARCH_DISTANCE);

            map<int, SlamConstraint *>::iterator c_it;
            for ( c_it = ref_node->constraints.begin() ; c_it != ref_node->constraints.end(); c_it++){        
                SlamConstraint *ct = c_it->second;
                
                if (ct->type == SLAM_GRAPH_EDGE_T_TYPE_LANGUAGE)
                    continue;

                int adjacent_node_id;
                SlamNode *adjacent_node;
                if (ct->node1->id == node_id)
                    adjacent_node = ct->node2;
                else
                    adjacent_node = ct->node1;

                if(prev_supernode_id == adjacent_node->id){
                    //previous one will be bled to down below
                    continue;
                }

                // If the node is a supernode, bleed to it, otherwise bleed to it's parent supernode
                // if the edge is temporal
                if (adjacent_node->slam_pose->is_supernode) {
                    lang_updated_nodes.insert(pair<int, SlamNode *> (adjacent_node->id, adjacent_node));

                    if (verbose)
                        fprintf(stderr, "Adding Connected Language : %d\n", (int) adjacent_node->id);
                    adjacent_node->labeldist->addObservation (label->label, LABEL_BLEED_INCREMENT, language_event_count);
                    if (verbose)
                        fprintf (stdout, "== Bleeding to adjacent supernode %d\n", adjacent_node->id);
                }
            }

            if (prev_supernode_id != -1) {
                SlamNode *prev_supernode = graph->getSlamNode (prev_supernode_id);
                lang_updated_nodes.insert(pair<int, SlamNode *> (prev_supernode->id, prev_supernode));

                if (verbose)
                    fprintf(stderr, "Adding Previous Language : %d\n", (int) prev_supernode->id);
                prev_supernode->labeldist->addObservation (label->label, LABEL_BLEED_INCREMENT, language_event_count);

                if (verbose)
                    fprintf (stdout, "== Bleeding to previous supernode %d\n", prev_supernode_id);
            }

            int next_supernode_id = graph->getNextSupernodeID (node_id, SUPERNODE_SEARCH_DISTANCE);
            if (next_supernode_id != -1) {
                SlamNode *next_supernode = graph->getSlamNode (next_supernode_id);
                lang_updated_nodes.insert(pair<int, SlamNode *> (next_supernode->id, next_supernode));

                if (verbose)
                    fprintf(stderr, "Adding Next Language : %d\n", (int) next_supernode->id);
                next_supernode->labeldist->addObservation (label->label, LABEL_BLEED_INCREMENT, language_event_count);

                if (verbose)
                    fprintf (stdout, "== Bleeding to next supernode %d\n", next_supernode_id);
            }
        }        
        language_event_count++;
    }

    if (verbose)
        fprintf(stderr, "++++ Nodes updated with Lang : %d\n", (int) lang_updated_nodes.size());

    // 4) Propose new language edges
    if(ignoreLanguageForEdges == 0){
        int added_lang = addLanguageEdges(basic_graph, lang_updated_nodes);
        
        if(added_lang){
            graph->status = 0;
            graph->runSlam();
        }
    }
    //verbose = 0;
    
} 

/*
  Considers all pairs of supernodes.  Finds the closest pair of nodes within their two respective 
  regions and sends them to the edge creation model.  If an edge is determined to be added, the local
  region from the first supernode is created and the closest node from the second supernode's region
  is scanmatched. If it's successful, the appropriate constraint is added to the iSAM graph.
*/
void SlamParticle::addLoopClosures(vector<SlamNode *> nodes_to_process, SlamGraph *basic_graph, int find_ground_truth){

    if(verbose)
        fprintf(stderr, "+++++ Loop closure check called\n");

    for(int k=0; k < nodes_to_process.size(); k++){
        SlamNode *node1 = nodes_to_process.at(k);
        int to_add_count = 0; 
        int sucess_count = 0; 

        int prev_supernode = graph->getPreviousSupernodeID(node1->id, SUPERNODE_SEARCH_DISTANCE);

        for(int i=0; i < graph->slam_node_list.size(); i++){
            SlamNode *node2 = graph->slam_node_list.at(i);
            if(!node2->slam_pose->is_supernode || node2->id == node1->id)// || node2->id == prev_supernode)
                continue;

            int cl_nd1_id = -1, cl_nd2_id = -1;
            double min_dist; 
            int sucess = getClosestNodes(node1->id, node2->id, &cl_nd1_id, &cl_nd2_id, &min_dist);
            
            /*fprintf(stderr, "%d - %d Closest Nodes : %d - %d : %f\n", 
                    node1->id, node2->id, cl_nd1_id, cl_nd2_id, min_dist);
            */

            if(sucess == 0)
                continue;

            SlamNode *cl_nd1 = graph->getSlamNode(cl_nd1_id);
            SlamNode *cl_nd2 = graph->getSlamNode(cl_nd2_id);

            if(cl_nd1 == NULL || cl_nd2 == NULL)
                continue;

            //is this as expensive ?? - we call this here 
            MatrixXd cov = graph->getCovariances(cl_nd1, cl_nd2);

            int to_add = check_to_add_basic(cl_nd1, cl_nd2, find_ground_truth, cov);
            
            if(verbose)
                fprintf(stderr, "To add : %d\n", to_add);
            

            if(to_add){
                to_add_count++;
                //use the same constraint
                int node_id_1 = node1->id;
                int node_id_2 = node2->id;

                Pose2d value_1 = node1->getPose();
                Pose2d value_2 = node2->getPose();

                Pose2d delta = value_1.ominus(value_2);

                double ref_dist = hypot(delta.x(), delta.y());
                double ref_angle = delta.t();
            
                //if the constraints are more than 0.8 meters 
                double distance_threshold = 0.8;
                double angle_threshold = M_PI / 16.0;

                map<int, vector<PoseToPoseTransform *> *>::iterator c_it;

                vector<PoseToPoseTransform *> *v = NULL;

                c_it = node1->slam_pose->sm_full_constraints.find(node_id_2);
        
                if(c_it != node1->slam_pose->sm_full_constraints.end()){

                    v = c_it->second;
                    //this function will now be different 
                    //search through the vector; 
                    PoseToPoseTransform *constraint_tf = NULL;

                    for(uint v_pos = 0; v_pos < v->size(); v_pos++) {
                        //for(vector<PoseToPoseTransform *>::iterator v_it = v->begin(); v_it != v->end(); ++v_it) {
                        PoseToPoseTransform *curr_tf = v->at(v_pos);
                        if(noScanTransformCheck){
                            constraint_tf = curr_tf;
                            //if this is the case - this vector will be of size 1 - so just pick the first element 
                        }
                        else if(fabs(curr_tf->distance_difference - ref_dist) < distance_threshold &&
                                fabs(curr_tf->angle_difference - ref_angle) < angle_threshold){
                            constraint_tf = curr_tf;
                        }
                    }
            
                    if(constraint_tf != NULL){
                        if(verbose)
                            fprintf(stderr, "++++++++ [%d]-[%d] Found the constraint in the cache : %f\n", 
                                    (int) node_id_1, (int) node_id_2, constraint_tf->hitpct);
                    
                        //add constraint
                        if(constraint_tf->accepted == 1){
                            sucess_count++;
                            SlamConstraint *sl_ct = graph->addConstraint(node1, node2, cl_nd1, cl_nd2, 
                                                                         *constraint_tf->transform, *constraint_tf->noise, 
                                                                         constraint_tf->hitpct, SLAM_GRAPH_EDGE_T_TYPE_SM_LC, 
                                                                         SLAM_GRAPH_EDGE_T_STATUS_SUCCESS);

                            //calculate the probability 
                            
                            int64_t p_start = bot_timestamp_now();
                           
                            Matrix3d cov_sm = Matrix3d::Zero();
                            cov_sm(0,0) = 0.00002;
                            cov_sm(1,1) = 0.00002;
                            cov_sm(2,2) = 0.00001;

                            // Not sure why we need to do this??                             
                            double prob = 1.0;//graph->calculateProbability(node1, node2,cov_sm, *constraint_tf->transform);
                            sl_ct->pofz = prob;
                            int64_t p_end = bot_timestamp_now();

                            if(verbose)
                                fprintf(stderr, "Time to calculate Prob : %f\n", (p_end-p_start)/1.0e6);
                            //prob at the point of the observation 
                        }
                        else{
                            if(addDummyConstraints){
                                graph->addDummyConstraint(node1, node2, cl_nd1, cl_nd2, *constraint_tf->transform, *constraint_tf->noise, constraint_tf->hitpct, SLAM_GRAPH_EDGE_T_TYPE_SM_LC, SLAM_GRAPH_EDGE_T_STATUS_FAILED);
                            }
                        }
        
                        continue;
                    }
                    else{
                        if(verbose)
                            fprintf(stderr, "+++++++ [%d]-[%d] Failed to find constraint - Doing scanmatch\n", 
                                    (int) node_id_1, 
                                    (int) node_id_2);
                    }
                }
                else{
                    if(verbose)
                        fprintf(stderr, "+++++++ [%d]-[%d] Failed to find constraint - Doing scanmatch\n", 
                                (int) node_id_1, 
                                (int) node_id_2);
                }

                NodeScan *pose_curr = node1->slam_pose;

                if(verbose){
                    fprintf(stderr, "No constraint found for the relavent nodes - not a current to previous constraint\n");
                }

                NodeScan *pose_to_match = node2->slam_pose;
        
                Pose2d transf;

                double cov_high[9] = { .02, 0, 0, 0, .02, 0, 0, 0, .01 };
                Matrix3d cv_high(cov_high);
                Noise *cov_hc = new Covariance(cv_high);
                int accept = 0;
                int64_t used_ind_1, used_ind_2;

                ScanTransform lc_r; 

                double hitpct = 0;//lc_r.hits / (double) pose_to_match->scan->numPoints;

                //draw some stuff on lcmgl 
                //do the scanmatch using the current graph
                lc_r = doScanMatch(basic_graph, node_id_1, node_id_2, &transf, cov_hc, &accept, 
                                   &used_ind_1, &used_ind_2, &hitpct);
                
                if (verbose)
                    fprintf(stderr, "Libbot Scanmatch Constraint : %f,%f,%f\n", transf.x(), transf.y(), transf.t());

                bool accepted = false;
        
                //add a scan match threashold of 50% for now 
                //if (hitpct > 0.1){//.50) { //was 0.6
                if (accept == 1){
                    if(verbose)
                        fprintf(stderr, "[%d] - [%d] Scan Match Sucess Acceptance : %f \n", 
                                (int) node_id_1, (int) node_id_2, hitpct);
                    accepted = true;

                    //lets add this to the constraint cache this 
                    PoseToPoseTransform *constraint_tf = new PoseToPoseTransform(new Pose2d(transf), pose_curr, pose_to_match, SLAM_GRAPH_EDGE_T_TYPE_SM_LC, cov_hc, 
                                                                                 hitpct, ref_dist, ref_angle, 1);
                    if(v != NULL){
                        //add this to the vector
                        v->push_back(constraint_tf);
                    }
                    else{
                        vector<PoseToPoseTransform *> *vec = new vector<PoseToPoseTransform *>();
                        vec->push_back(constraint_tf);
                        pose_curr->sm_full_constraints.insert ( pair<int, vector<PoseToPoseTransform *> *>(node_id_2, vec));
                    }

                    //add constraint
                    SlamConstraint *sl_ct = graph->addConstraint(node1, node2, cl_nd1, cl_nd2, *constraint_tf->transform, *constraint_tf->noise, constraint_tf->hitpct, SLAM_GRAPH_EDGE_T_TYPE_SM_LC, SLAM_GRAPH_EDGE_T_STATUS_SUCCESS);
                    sucess_count++;

                    int64_t p_start = bot_timestamp_now();
                    Matrix3d cov_sm = Matrix3d::Zero();
                    cov_sm(0,0) = 0.00002;
                    cov_sm(1,1) = 0.00002;
                    cov_sm(2,2) = 0.00001;

                    double prob = 1.0;//graph->calculateProbability(node1, node2,cov_sm, *constraint_tf->transform);
                    sl_ct->pofz = prob;
                    int64_t p_end = bot_timestamp_now();

                    if (verbose)
                        fprintf(stderr, "Time to calculate Prob : %f\n", (p_end-p_start)/1.0e6);

                }
                else {
                    if(verbose) 
                        fprintf(stderr, "[%d] - [%d] Scan match failed : %f \n", (int) node_id_1, (int) node_id_2, hitpct);
                    //maybe we should add a high cov scan ?? 

                    //lets add this to the constraint cache this 
                    PoseToPoseTransform *constraint_tf = new PoseToPoseTransform(new Pose2d(transf), pose_curr, pose_to_match, 
                                                                                 SLAM_GRAPH_EDGE_T_TYPE_SM_LC, cov_hc, 
                                                                                 hitpct, ref_dist, ref_angle, 0);

                    if(v != NULL){
                        //add this to the vector
                        v->push_back(constraint_tf);
                    }
                    else{
                        vector<PoseToPoseTransform *> *vec = new vector<PoseToPoseTransform *>();
                        vec->push_back(constraint_tf);
                        pose_curr->sm_full_constraints.insert ( pair<int, vector<PoseToPoseTransform *> *>(node_id_2, vec));
                    }
            
                    if(addDummyConstraints){
                        graph->addDummyConstraint(node1, node2, cl_nd1, cl_nd2, *constraint_tf->transform, *constraint_tf->noise, constraint_tf->hitpct, SLAM_GRAPH_EDGE_T_TYPE_SM_LC, SLAM_GRAPH_EDGE_T_STATUS_FAILED);
                    }
                }
            }
        }
        
        if (verbose)
            fprintf(stderr, "\tNode considered : %d => To add : %d Sucess : %d\n", node1->id, to_add_count, sucess_count);
        node1->processed = 1;
    }
}

void SlamParticle::clearDummyConstraints(){
    graph->clearDummyConstraints();
}

void SlamParticle::runScanMatchTraining(){
    draw_scanmatch = true;
    //add the nodes and the constraints 
    for(int i=0; i< graph->slam_node_list.size(); i++){
        //create the nodes 
        SlamNode *node1 = (SlamNode *) graph->slam_node_list.at(i);
        //add the nodes and the constraints 
        if(!node1->slam_pose->is_supernode)
            continue;
        for(int j=0; j< graph->slam_node_list.size(); j++){
            if(j== i)
                continue;
            
            SlamNode *node2 = (SlamNode *) graph->slam_node_list.at(j);
            if(!node2->slam_pose->is_supernode)
                continue;

            Pose2d transf;
            double cov_high[9] = { .02, 0, 0, 0, .02, 0, 0, 0, .01 };
            Matrix3d cv_high(cov_high);
            Noise *cov_hc = new Covariance(cv_high);
            int accept = 0;
            int64_t used_ind_1, used_ind_2;

            Pose2d pose1 = node1->getPose();
            Pose2d pose2 = node2->getPose();
            
            Pose2d delta = pose1.ominus(pose2);

            if(hypot(delta.x(), delta.y()) > 20.0){
                //skip??
                continue;
            }
            
            doScanMatchCheck(graph, node1->id, node2->id, 
                             &accept, &used_ind_1, &used_ind_2);

            if (verbose)
                fprintf(stderr, "Node %d - Node %d - Results : %d\n", node1->id, node2->id, 
                        accept);
            
            feature_config_t fconfig;
            fconfig.max_range = 30;
            fconfig.min_range = 0.1;
            fconfig.cutoff = 3;
            fconfig.gap_threshold_1 = 0.5;
            fconfig.gap_threshold_2 = 0.3;
            fconfig.deviation = 0.001;
            fconfig.no_of_fourier_coefficient = 2;
            fconfig.no_beam_skip = 3;
            
            write_difference_features(accept, (char *) "sm_results.txt", fconfig, 
                                      node1->slam_pose->laser, node2->slam_pose->laser);

            SlamNode *node1_act = (SlamNode *) graph->getSlamNode(used_ind_1);
            SlamNode *node2_act = (SlamNode *) graph->getSlamNode(used_ind_2);
                        
            write_difference_features(accept, (char *) "sm_results_act_scans.txt", fconfig, 
                                      node1_act->slam_pose->laser, node2_act->slam_pose->laser);
            
            delete cov_hc;
            
            bot_lcmgl_switch_buffer(lcmgl_sm_basic);
            bot_lcmgl_switch_buffer(lcmgl_sm_graph);
            bot_lcmgl_switch_buffer(lcmgl_sm_prior);
            bot_lcmgl_switch_buffer(lcmgl_sm_result);
            bot_lcmgl_switch_buffer(lcmgl_loop_closures);
        }
    }
    
    draw_scanmatch = false;
}

// TODO: Go through calculation of probability
//
// Matt - I updated this to update the weights based on the pofz returned from getProbOfMeasurementLikelihoodCount
void SlamParticle::calculateObservationProbability(int probMode, double *maximum_probability){

    static int64_t time_taken_1 = 0;
    static int64_t time_taken_2 = 0;
    static int times_called = 0;
    
    if(probMode != 0){
        for(int k =0; k < unprocessed_slam_nodes.size(); k++){
            SlamNode *node1 = unprocessed_slam_nodes.at(k);
            double max_prob = 0;
            // Populates unnormalized pofz for each lidar return
            int64_t s_time = bot_timestamp_now();
            //this one seems to give pretty different values 
            double pofz_new = 0;
            //double pofz_new = getProbOfMeasurementLikelihoodComplex(node1, &max_prob);//
            int64_t e_time_1 = bot_timestamp_now();
            double pofz = getProbOfMeasurementLikelihoodCount(node1, &max_prob);
            int64_t e_time_2 = bot_timestamp_now();

            time_taken_1 += (e_time_1 - s_time);
            time_taken_2 += (e_time_2 - e_time_1);
            times_called++;
            
            fprintf(stderr, "P new : %f P Old : %f\n", pofz_new, pofz);
            //fprintf(stderr, "Time Taken - New : %f Old : %f\n", (e_time_1 - s_time) / 1.0e6, 
            //      (e_time_2 - e_time_1) / 1.0e6);
            fprintf(stderr, "Avg Time Taken - New : %f Old : %f\n", (time_taken_1) / 1.0e6 / times_called, 
                    (time_taken_2) / 1.0e6 / times_called);

            double log_pofz = log(pofz);
            weight += log_pofz; 

                //getProbOfMeasurementLikelihood(node1, &max_prob);
            
            if(node1->slam_pose->max_prob < max_prob){
                node1->slam_pose->max_prob = max_prob;
            }
            
            //now this is not used 
            node1->pofz = pofz;
            *maximum_probability = max_prob; 
        }
    }
    else{
        //this method is now only valid for 
        //deprecated        
        updateWeight(probMode, unprocessed_slam_nodes);
    }

    //we should clear the unprocessed nodes (since they have been processed) 
    if(verbose)
        fprintf(stderr, "Clearing the processed nodes\n");
    unprocessed_slam_nodes.clear();

    return;

    //hmm - right now this already assumes that the pofz's have been calculated - prob should do it seperately
    //because the 
    if(probMode == 0)
        return;
    
    //mode 1 will average the weight 
    if(probMode == 1){
        for(int i=0; i< graph->slam_node_list.size(); i++){
            //create the nodes 
            SlamNode *nd = (SlamNode *) graph->slam_node_list.at(i);
            
            if(nd->prob_used)
                continue; 
            double max_prob = nd->slam_pose->max_prob; 
            if(max_prob == 0)
                max_prob = 0.1;
            nd->prob_used = 1;
            double log_pofz = 0;
            double avg_pofz = 0;
            double p = 0;
            for(int k=0; k < nd->slam_pose->scan->numPoints; k++){
                //normalize 
                p = (nd->prob_of_scan[k]/max_prob);
                /*if(p < 0.2)
                  p = 0.2;*/
                log_pofz += log(p);
                avg_pofz += p;
            }
            //to take the average 
            log_pofz = log(avg_pofz) - log((double) nd->slam_pose->scan->numPoints);

            
            //the straight up joint prob jumps too much one way or the other - Matt - debug please :) 
            if(verbose)
                fprintf(stderr, "[%d] %d => Total Probability : log : %f => %f\n", graph_id , i, 
                        log_pofz, exp(log_pofz));
                /*if (ret < 0)
                  assert(false);*/
            weight += log_pofz; 
        }
    }
    
    //mode 2 will use the product - theoritically correct - pratically sucks 
    if(probMode == 2){
        for(int i=0; i< graph->slam_node_list.size(); i++){
            //create the nodes 
            SlamNode *nd = (SlamNode *) graph->slam_node_list.at(i);

            if(nd->prob_used)
                continue; 
            double max_prob = nd->slam_pose->max_prob; 
            if(max_prob == 0)
                max_prob = 0.1;
            nd->prob_used = 1;
            double log_pofz = 0;
            double avg_pofz = 0;
            double p = 0;
            for(int k=0; k < nd->slam_pose->scan->numPoints; k++){
                //normalize 
                p = (nd->prob_of_scan[k]/max_prob);
                /*if(p < 0.2)
                  p = 0.2;*/
                log_pofz += log(p);
                avg_pofz += p;
            }
            //to take the average 
            //log_pofz = log(avg_pofz) - log((double) nd->slam_pose->scan->numPoints);

            if (verbose)
                fprintf(stderr, "Prob log : %f -> %f\n", log_pofz, exp(log_pofz)); 
        
            //the straight up joint prob jumps too much one way or the other - Matt - debug please :) 
            int ret = fprintf(stderr, "[%d] %d => Total Probability : log : %f => %f\n", graph_id , i, 
                              log_pofz, exp(log_pofz));
            /*if (ret < 0)
              assert(false);*/
            weight += log_pofz; 
        }
    }

    /*for(int i=0; i< graph->slam_node_list.size(); i++){
        //create the nodes 
        SlamNode *nd = (SlamNode *) graph->slam_node_list.at(i);

        if(nd->prob_used)
            continue; 
        double max_prob = nd->slam_pose->max_prob; 
        if(max_prob == 0)
            max_prob = 0.1;
        nd->prob_used = 1;
        double log_pofz = 0;
        double avg_pofz = 0;
        double p = 0;
        for(int k=0; k < nd->slam_pose->scan->numPoints; k++){
            //normalize 
            p = nd->prob_of_scan[k]/max_prob;
            if(p < 0.2)
                p = 0.2;
            log_pofz += log(p);
            avg_pofz += p;
        }
        //to take the average 
        log_pofz = log(avg_pofz) - log((double) nd->slam_pose->scan->numPoints);
        
        //the straight up joint prob jumps too much one way or the other - Matt - debug please :) 
        int ret = fprintf(stderr, "[%d] %d => Total Probability : log : %f => %f\n", graph_id , i, 
                          log_pofz, exp(log_pofz));
        if (ret < 0)
          assert(false);
        weight += log_pofz; 
        }*/
    
}

void SlamParticle::processMeasurementLikelihood(int probMode){
    //hmm - right now this already assumes that the pofz's have been calculated - prob should do it seperately
    //because the 
    if(probMode == 0)
        return;
    
    //mode 1 will average the weight 
    if(probMode == 1){
        for(int i=0; i< graph->slam_node_list.size(); i++){
            //create the nodes 
            SlamNode *nd = (SlamNode *) graph->slam_node_list.at(i);
            
            if(nd->prob_used)
                continue; 
            double max_prob = nd->slam_pose->max_prob; 
            if(max_prob == 0)
                max_prob = 0.1;
            nd->prob_used = 1;
            double log_pofz = 0;
            double avg_pofz = 0;
            double p = 0;
            for(int k=0; k < nd->slam_pose->scan->numPoints; k++){
                //normalize 
                p = (nd->prob_of_scan[k]/max_prob);
                /*if(p < 0.2)
                  p = 0.2;*/
                log_pofz += log(p);
                avg_pofz += p;
            }
            //to take the average 
            log_pofz = log(avg_pofz) - log((double) nd->slam_pose->scan->numPoints);

            
            //the straight up joint prob jumps too much one way or the other - Matt - debug please :) 
            int ret = fprintf(stderr, "[%d] %d => Total Probability : log : %f => %f\n", graph_id , i, 
                              log_pofz, exp(log_pofz));
                /*if (ret < 0)
                  assert(false);*/
            weight += log_pofz; 
        }
    }
    
    //mode 2 will use the product - theoritically correct - pratically sucks 
    if(probMode == 2){
        for(int i=0; i< graph->slam_node_list.size(); i++){
            //create the nodes 
            SlamNode *nd = (SlamNode *) graph->slam_node_list.at(i);

            if(nd->prob_used)
                continue; 
            double max_prob = nd->slam_pose->max_prob; 
            if(max_prob == 0)
                max_prob = 0.1;
            nd->prob_used = 1;
            double log_pofz = 0;
            double avg_pofz = 0;
            double p = 0;
            for(int k=0; k < nd->slam_pose->scan->numPoints; k++){
                //normalize 
                p = (nd->prob_of_scan[k]/max_prob);
                /*if(p < 0.2)
                  p = 0.2;*/
                log_pofz += log(p);
                avg_pofz += p;
            }
            //to take the average 
            //log_pofz = log(avg_pofz) - log((double) nd->slam_pose->scan->numPoints);

            if (verbose)
                fprintf(stderr, "Prob log : %f -> %f\n", log_pofz, exp(log_pofz)); 
        
            //the straight up joint prob jumps too much one way or the other - Matt - debug please :) 
            int ret = fprintf(stderr, "[%d] %d => Total Probability : log : %f => %f\n", graph_id , i, 
                              log_pofz, exp(log_pofz));
            /*if (ret < 0)
              assert(false);*/
            weight += log_pofz; 
        }
    }
    
    


    /*for(int i=0; i< graph->slam_node_list.size(); i++){
        //create the nodes 
        SlamNode *nd = (SlamNode *) graph->slam_node_list.at(i);

        if(nd->prob_used)
            continue; 
        double max_prob = nd->slam_pose->max_prob; 
        if(max_prob == 0)
            max_prob = 0.1;
        nd->prob_used = 1;
        double log_pofz = 0;
        double avg_pofz = 0;
        double p = 0;
        for(int k=0; k < nd->slam_pose->scan->numPoints; k++){
            //normalize 
            p = nd->prob_of_scan[k]/max_prob;
            if(p < 0.2)
                p = 0.2;
            log_pofz += log(p);
            avg_pofz += p;
        }
        //to take the average 
        log_pofz = log(avg_pofz) - log((double) nd->slam_pose->scan->numPoints);
        
        //the straight up joint prob jumps too much one way or the other - Matt - debug please :) 
        int ret = fprintf(stderr, "[%d] %d => Total Probability : log : %f => %f\n", graph_id , i, 
                          log_pofz, exp(log_pofz));
        if (ret < 0)
          assert(false);
        weight += log_pofz; 
        }*/
}

void SlamParticle::updateWeight(int probMode, vector<SlamNode *> nodes_to_process){
    if(probMode == 0){
        map<int, SlamConstraint *>::iterator c_it;
        for ( c_it= graph->slam_constraints.begin() ; c_it != graph->slam_constraints.end(); c_it++ ){     
            SlamConstraint *edge = (SlamConstraint *) c_it->second;

            if(!edge->processed && edge->type == SLAM_GRAPH_EDGE_T_TYPE_SM_LC && edge->status == SLAM_GRAPH_EDGE_T_STATUS_SUCCESS){
                double prob = edge->pofz;
                if(prob == 0){
                    prob = 0.0000000001;
                    fprintf(stderr, "XXXXXXXXXXXXXXXX Error - Edge Probability set to zero XXXXXXXXXXXXXXXXXX\n");
                }
                if(prob == -1){
                    fprintf(stderr, "XXXXXXXXXXXXXXXX Error - Edge Probability not set XXXXXXXXXXXXXXXXXX\n");
                    prob = 0.0000000001;
                }
                
                prob = prob/1000;
                
                //fprintf(stderr, MAKE_GREEN "Particle reweighting factor: %f" RESET_COLOR "\n", prob);
                
                weight += log(prob);
                edge->processed = 1;
            }
        }
    }
    
    //the problem here is that in this way - the probability can't be calculated here - it needs to be 
    //done before the new constraints are added - so needs to go to the update_particle section 
    if(probMode == 1 && nodes_to_process.size() > 0){
        for(int i=0; i<  nodes_to_process.size(); i++){
            SlamNode *nd = nodes_to_process.at(i);
            double prob = nd->pofz;
            if(prob == 0){
                prob = 0.0000000001;
            }
            weight += log(prob);
            if(verbose)
                fprintf(stderr, "\tWeight %f\n", weight);
        }
    }
}

void SlamParticle::publishOccupancyMap () {
    // First determine extent of pixel map
    double minx, miny, maxx, maxy;
    for (int i=0; i < graph->slam_node_list.size(); i++) {
        SlamNode *node = graph->slam_node_list.at(i);

        double nodex = node->getPose().x();
        double nodey = node->getPose().y();

        if (i == 0) {
            minx = nodex;
            maxx = nodex;
            miny = nodey;
            maxy = nodey;
        } else {
            if (nodex < minx)
                minx = nodex;
            else if (nodex > maxx)
                maxx = nodex;

            if (nodey < miny)
                miny = nodey;
            else if (nodey > maxy)
                maxy = nodey;
        }
    }

    double xy0[] = { minx - 10, miny - 10};
    double xy1[] = { maxx + 10, maxy + 10};

    FloatPixelMap *pixmap = new FloatPixelMap (xy0, xy1, PIXMAP_RESOLUTION, 0);
        
    // Now, populate the pixel map
    for (int i=0; i < graph->slam_node_list.size(); i++) {
      SlamNode *node = graph->slam_node_list.at(i);

        // skip nodes that have poor incremental scanmatch as indicative of being outside
        if (node->slam_pose->inc_constraint_sm->hitpct <= inc_sm_acceptance_threshold)
            continue;

        bot_core_planar_lidar_t *laser = node->slam_pose->laser;
        Scan *scan = node->slam_pose->scan;

        BotTrans bodyToLocal;
        bodyToLocal.trans_vec[0] = node->getPose().x();
        bodyToLocal.trans_vec[1] = node->getPose().y();
        bodyToLocal.trans_vec[2] = 0;
        double rpy[3] = {0.0, 0.0, node->getPose().t()};
        bot_roll_pitch_yaw_to_quat (rpy, bodyToLocal.rot_quat);
        double pBody[3] = {0, 0, 0};
        double pLocal[3];

        float clamp_bounds[2] = {0.0, 1.0};

        for (int j=0; j < scan->numPoints; j++) {
            pBody[0] = scan->points[j].x;
            pBody[1] = scan->points[j].y;
            pBody[2] = 0.0;
            bot_trans_apply_vec (&bodyToLocal, pBody, pLocal);

            // Update the pixel map by either modifying the cell that the lidar return
            // falls in (via updateValue) or performing ray tracing.
            double pXY[2] = {pLocal[0], pLocal[1]};
            if (pixmap->isInMap (pXY))
                pixmap->updateValue (pXY, PIXMAP_HIT_INC, clamp_bounds);
        }
    }

    const occ_map_pixel_map_t *pixmap_msg = pixmap->get_pixel_map_t (bot_timestamp_now());
    occ_map_pixel_map_t_publish (lcm, "PIXMAP_MAX_PARTICLE", pixmap_msg);

    delete pixmap;

}

void SlamParticle::getLCMMessageFromGraph(slam_graph_particle_t *particle){
    particle->id = graph_id;
    particle->no_nodes = graph->slam_node_list.size();
    particle->node_list = (slam_graph_node_t *) calloc(particle->no_nodes, sizeof(slam_graph_node_t));
    graph->updateBoundingBoxes();
    
    for(int i=0; i < graph->slam_node_list.size(); i++){
        SlamNode *node = graph->slam_node_list.at(i);
        
        particle->node_list[i].id = node->id;
        particle->node_list[i].utime = node->slam_pose->utime;
        particle->node_list[i].xy[0] = node->getPose().x();
        particle->node_list[i].xy[1] = node->getPose().y();
        particle->node_list[i].heading = node->getPose().t();
        particle->node_list[i].is_supernode = node->slam_pose->is_supernode;
        particle->node_list[i].parent_supernode = node->parent_supernode;
        
        //&(particle->node_list[i].labeldist = (slam_label_distribution_t *) calloc(1, sizeof(slam_label_distribution_t));
        particle->node_list[i].labeldist.num_labels = node->labeldist->num_labels;
        particle->node_list[i].labeldist.total_obs  = node->labeldist->total_obs ;
        //nothing to send over the labels atm
        //particle->node_list[i].labeldist.observation_frequency = node->labeldist->observation_frequency;
        particle->node_list[i].labeldist.observation_frequency = (double *) calloc(node->labeldist->num_labels, sizeof(double));
        for(int j=0; j<node->labeldist->num_labels; j++)
            particle->node_list[i].labeldist.observation_frequency[j] = node->labeldist->observation_frequency.at(j);
        
        particle->node_list[i].no_points = node->bounding_box_global->npoints;
        particle->node_list[i].x_coords = (double *) calloc(node->bounding_box_global->npoints, sizeof(double));
        particle->node_list[i].y_coords = (double *) calloc(node->bounding_box_global->npoints, sizeof(double));
        for(int j=0; j<node->bounding_box_global->npoints; j++){
            particle->node_list[i].x_coords[j] = node->bounding_box_global->points[j].x;
            particle->node_list[i].y_coords[j] = node->bounding_box_global->points[j].y;
        }
        particle->node_list[i].pofz = node->pofz;
        memcpy(particle->node_list[i].cov,node->cov, 9 * sizeof(double));
    }
    particle->no_edges = graph->slam_constraints.size();
    particle->edge_list = (slam_graph_edge_t *) calloc(particle->no_edges, sizeof(slam_graph_edge_t));

    map<int, SlamConstraint *>::iterator c_it;
    int j= 0;
    for ( c_it= graph->slam_constraints.begin() ; c_it != graph->slam_constraints.end(); c_it++, j++ ){        
        SlamConstraint *ct = c_it->second;
        
        particle->edge_list[j].id = ct->id;
        particle->edge_list[j].node_id_1 = ct->node1->id; 
        particle->edge_list[j].node_id_2 = ct->node2->id; 
        particle->edge_list[j].actual_scanned_node_id_1 = ct->actualnode1->id;
        particle->edge_list[j].actual_scanned_node_id_2 = ct->actualnode2->id;
        particle->edge_list[j].scanmatch_hit = ct->hitpct;
        particle->edge_list[j].type = ct->type;
        particle->edge_list[j].status = ct->status;
    }

    particle->no_rejected_edges = graph->failed_slam_constraints.size();
    particle->rejected_edge_list = (slam_graph_edge_t *) calloc(particle->no_rejected_edges, sizeof(slam_graph_edge_t));

    j= 0;
    for ( c_it= graph->failed_slam_constraints.begin() ; c_it != graph->failed_slam_constraints.end(); c_it++, j++ ){        
        SlamConstraint *ct = c_it->second;
        
        particle->rejected_edge_list[j].id = ct->id;
        particle->rejected_edge_list[j].node_id_1 = ct->node1->id; 
        particle->rejected_edge_list[j].node_id_2 = ct->node2->id; 
        particle->rejected_edge_list[j].actual_scanned_node_id_1 = ct->actualnode1->id;
        particle->rejected_edge_list[j].actual_scanned_node_id_2 = ct->actualnode2->id;
        particle->rejected_edge_list[j].scanmatch_hit = ct->hitpct;
        particle->rejected_edge_list[j].type = ct->type;
        particle->rejected_edge_list[j].status = ct->status;
    }
    particle->weight = normalized_weight;
    fprintf(stderr, "Weight : %f\n", normalized_weight);
}
 
