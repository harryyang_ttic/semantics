#include <iostream>
#include <getopt.h>
#include <set>
#include <map>
#include <vector>

#include <bot_core/bot_core.h>
#include <lcmtypes/slam_lcmtypes.h>
#include <fstream>
#include <string.h>

#include <queue>

#include <algorithm> 

using namespace std;

class label_info{
public:
    map<string, int> category_to_index;
    map<int, string> index_to_category;
    map<string, int> labels_to_index;
    
    label_info(){
        category_to_index.insert(make_pair("office",0));
        category_to_index.insert(make_pair("lab",1));
        category_to_index.insert(make_pair("conferenceroom",2));
        category_to_index.insert(make_pair("hallway",3));
        category_to_index.insert(make_pair("elevatorlobby",4));
        category_to_index.insert(make_pair("lounge",5));
        category_to_index.insert(make_pair("kitchen",6));

        map<string, int>::iterator it; 

        for(it = category_to_index.begin(); it != category_to_index.end(); it++){
            index_to_category.insert(make_pair(it->second, it->first));
        }

        labels_to_index.insert(make_pair("elevatorlobby", 0));
        labels_to_index.insert(make_pair("office", 1));
        labels_to_index.insert(make_pair("lab", 2));
        labels_to_index.insert(make_pair("hallway", 3));
        labels_to_index.insert(make_pair("hall", 4));
        labels_to_index.insert(make_pair("corridor", 5));
        labels_to_index.insert(make_pair("meetingroom", 6));
        labels_to_index.insert(make_pair("conferenceroom", 7));
        labels_to_index.insert(make_pair("kitchen", 8));
        labels_to_index.insert(make_pair("lounge", 9));
    }
    
    int get_index_for_category(string cat){
        map<string, int>::iterator it;
        it = category_to_index.find(cat);
        if(it == category_to_index.end())
            return -1;
        return it->second;
    }

    string get_index_for_category(int id){
        map<int, string>::iterator it;
        it = index_to_category.find(id);
        if(it == index_to_category.end())
            return "unknown";
        return it->second;
    }

    int get_index_for_label(string label){
        map<string, int>::iterator it;
        it = labels_to_index.find(label);
        if(it == labels_to_index.end())
            return -1;
        return it->second;
    }
};

template <class T> 
class object_compare {
public:
    object_compare(){}
    
    bool operator() (const T& lhs, const T&rhs) const
    {
        return (lhs.score < rhs.score);
    }
};

template <class T> 
class object_score_t {
public:
    T obj;
    double score_no_lang;
    double score;
    
    object_score_t(T _obj, double _score, double _score_no_lang){
        obj = _obj;
        score = _score;
        score_no_lang = _score_no_lang;
    }
    
    bool operator< (object_score_t &rhs){
        if(score < rhs.score){
            return true;
        }
        return false;
    }
};

struct annotation_t {
    int id;
    string annotation; 
    int annotation_id;
    vector<int> node_ids; //this might have to be re assesed based on the utimes - if the logs are different 
};

struct region_t {
    int id;
    
    map<int, double> type_distribution; //
    map<int, double> label_distribution; //

    map<int, double> type_distribution_no_lang; //
    map<int, double> label_distribution_no_lang; //

    vector<int> node_ids;
};

struct intersection_results{
    int annotation_id;
    string annotation;
    int region_id;
    int inter_c;
    int union_c;
    double score;
    double semantic_score; 
    double semantic_score_no_lang; 
};

class result_comparison
{
public:
    result_comparison()
    {}
  
    bool operator() (const intersection_results& lhs, const intersection_results&rhs) const
    {
        return (lhs.score < rhs.score);
    }
};

typedef priority_queue<intersection_results,std::vector<intersection_results>, result_comparison> result_queue_t;

typedef pair<region_t, double> region_result;

class region_result_comparison
{
public:
    region_result_comparison()
    {}
  
    bool operator() (const region_result& lhs, const region_result&rhs) const
    {
        return (lhs.second < rhs.second);
    }
};

typedef priority_queue<intersection_results,std::vector<region_result>, region_result_comparison> region_result_queue_t;

struct particle_t {
    int id; 
    double weight; //not log scale 
    map<int, region_t> particle_regions;
};

struct state_t {
    lcm_t *lcm;
    char *annotation_log;
    char *node_utime;
    slam_graph_region_particle_t *max_particle;
    vector<annotation_t> annotation_list;

    map<int, region_t> particle_regions;

    map<int, particle_t> particle_list; 

    result_queue_t result_queue;

    label_info info;
};

struct semantic_result_t {
    map<int, double> region_contribution;
    double score; 
    double score_no_lang;
    double weight;
};

struct annotation_result_t {
    annotation_t annot;
    map<int, semantic_result_t> result; //result for each particle 

    double avg_score; //weighted average score
    double avg_score_no_lang; //weighted average score
};

bool compare_average_results(annotation_result_t p1, annotation_result_t p2){
    if(p1.avg_score > p2.avg_score)
        return true;
    return false;
}

void print_region_result_queue(region_result_queue_t &result_queue){
    while(!result_queue.empty()){
        region_result result = result_queue.top();
        fprintf(stderr, "Region : %d -> Score : %f\n", result.first.id, result.second);
        result_queue.pop();
    }
}

void print_result_queue(state_t *self){
    fprintf(stderr, "================== Region Segmentation Accuracy =================\n");
    set<int> included_regions;
    set<int>::iterator it;    

    set<int>::iterator it_annot;    
    set<int> found_annotation;
    
    map<string, vector<intersection_results> > segmentation_accuracy; 
    map<string, vector<intersection_results> >::iterator it_results;

    
    bool skip_hallways = false;

    while(!self->result_queue.empty()){
        intersection_results result = self->result_queue.top();
        
        self->result_queue.pop();
        
        it_annot = found_annotation.find(result.annotation_id);
        if(it_annot != found_annotation.end())
            continue;

        it = included_regions.find(result.region_id);
        if(it == included_regions.end()){
            //valid intersection result 
            if(skip_hallways && !result.annotation.compare("hallway")){
                continue;
            }
            
            found_annotation.insert(result.annotation_id);
            included_regions.insert(result.region_id);
            //skip hallways

            it_results = segmentation_accuracy.find(result.annotation);
            if(it_results == segmentation_accuracy.end()){
                vector<intersection_results> results;
                results.push_back(result);
                segmentation_accuracy.insert(make_pair(result.annotation, results));
            }
            else{
                it_results->second.push_back(result);
            }
            
            double perc_improv = 100.0;
            if(result.semantic_score_no_lang > 0.00001)
                perc_improv = (result.semantic_score / result.semantic_score_no_lang -1) * 100;
            fprintf(stderr, "[%d] Result %s => Region : %d -> Score : %f (%d / %d) => Semantic Score : %f / %f (no lang) => %f\n", 
                    result.annotation_id, result.annotation.c_str(), result.region_id, result.score, 
                    result.inter_c, result.union_c, result.semantic_score, result.semantic_score_no_lang, perc_improv);
        }
    }

    fprintf(stderr, "============ Accuracy by region type ===========\n");

    for(it_results = segmentation_accuracy.begin(); it_results != segmentation_accuracy.end(); it_results++){
        fprintf(stderr, "Region Type : %s\n", it_results->first.c_str());
        vector<intersection_results> results = it_results->second;
        double avg_accuracy = 0;
        for(int i=0; i < results.size(); i++){
            intersection_results result = results[i];
            double perc_improv = 100.0;
            if(result.semantic_score_no_lang > 0.00001)
                perc_improv = (result.semantic_score / result.semantic_score_no_lang -1) * 100;

            fprintf(stderr, "\t[%d] Result %s => Region : %d -> Score : %f (%d / %d) => Semantic Score : %f / %f (no lang) => %f\n", 
                    result.annotation_id, result.annotation.c_str(), result.region_id, result.score, 
                    result.inter_c, result.union_c, result.semantic_score, result.semantic_score_no_lang, perc_improv);
            
            avg_accuracy += result.score;
        }
        avg_accuracy /= (double) results.size();
        fprintf(stderr, "=== Avg Accuracy : %f\n", avg_accuracy);        
    }

    for(it_results = segmentation_accuracy.begin(); it_results != segmentation_accuracy.end(); it_results++){
        fprintf(stderr, "Region Type : %s\n", it_results->first.c_str());
        vector<intersection_results> results = it_results->second;
        double avg_accuracy = 0;
        for(int i=0; i < results.size(); i++){
            intersection_results result = results[i];
            double perc_improv = 100.0;
            if(result.semantic_score_no_lang > 0.00001)
                perc_improv = (result.semantic_score / result.semantic_score_no_lang -1) * 100;

            avg_accuracy += result.score;
        }
        avg_accuracy /= (double) results.size();
        fprintf(stderr, "=== Avg Accuracy : %f\n", avg_accuracy);        
    }
}

int get_max_category(map<int, double> dist){
    double max_score = 0;
    int max_ind = -1;
    map<int, double>::iterator it_1;
    for(it_1 = dist.begin(); it_1 != dist.end(); it_1++){
        if(max_score < it_1->second){
            max_score = it_1->second;
            max_ind = it_1->first;
        }
    }
    return max_ind;
}

double get_liklihood_of_category(map<int, double> dist, int id){
    map<int, double>::iterator it_1;
    it_1 = dist.find(id);
    if(it_1 != dist.end()){
        return it_1->second;
    }
    return 0;
}

double cosine_sim(map<int, double> &dist1, map<int, double> &dist2){
    map<int, double>::iterator it_1;
    map<int, double>::iterator it_2;

    /*fprintf(stderr, "Dist 1\n");
    for(it_1 = dist1.begin(); it_1 != dist1.end(); it_1++){
        fprintf(stderr, "%d - %f\n", it_1->first, it_1->second);
    }

    fprintf(stderr, "Dist 2\n");
    for(it_1 = dist2.begin(); it_1 != dist2.end(); it_1++){
        fprintf(stderr, "%d - %f\n", it_1->first, it_1->second);
        }*/

    double score = 0;
    for(it_1 = dist1.begin(); it_1 != dist1.end(); it_1++){
        it_2 = dist2.find(it_1->first);
        if(it_2 != dist2.end()){
            score += it_1->second * it_2->second;
        }
    }
    return score; 
}

void request_results(state_t *self){
    slam_particle_request_t msg; 
    msg.utime = bot_timestamp_now();
    msg.particle_id = -1;
    msg.request = SLAM_PARTICLE_REQUEST_T_SAVE_PARTICLE;
    slam_particle_request_t_publish(self->lcm, "SEMANTIC_PARTICLE_RESULT_REQUEST", &msg);
}

void usage(char *argv){
    fprintf(stderr, "No usage defined\n");
}

void parse_particle(state_t *self){
    self->particle_regions.clear();
    
    for(int i=0; i < self->max_particle->no_regions; i++){
        slam_graph_region_t *region = &self->max_particle->region_list[i];
        
        region_t g_region; 

        g_region.id = region->id;
        
        for(int j=0; j< region->count; j++){
            slam_graph_node_t node = region->nodes[j]; 
            
            //fprintf(fp,"%d,%f\n", node.id, node.utime/1.0e6);

            //g_region.node_ids.insert( node.id);
            g_region.node_ids.push_back(node.id);
        }

        sort (g_region.node_ids.begin(), g_region.node_ids.end());
        
        for(int k=0; k < region->region_type_dist.count; k++){
            int t_class = region->region_type_dist.classes[k].type;
            double prob = region->region_type_dist.classes[k].probability;
            g_region.type_distribution.insert(make_pair(t_class, prob));            
        }

        for(int k=0; k < region->region_label_dist.count; k++){
            int t_class = region->region_label_dist.classes[k].type;
            double prob = region->region_label_dist.classes[k].probability;
            g_region.label_distribution.insert(make_pair(t_class, prob));            
        }

        for(int k=0; k < region->region_type_dist_no_lang.count; k++){
            int t_class = region->region_type_dist_no_lang.classes[k].type;
            double prob = region->region_type_dist_no_lang.classes[k].probability;
            g_region.type_distribution_no_lang.insert(make_pair(t_class, prob));            
        }

        for(int k=0; k < region->region_label_dist_no_lang.count; k++){
            int t_class = region->region_label_dist_no_lang.classes[k].type;
            double prob = region->region_label_dist_no_lang.classes[k].probability;
            g_region.label_distribution_no_lang.insert(make_pair(t_class, prob));            
        }

        self->particle_regions.insert(make_pair(region->id, g_region));
    }
    fprintf(stderr, "Done parsing region\n");
}

void parse_particle_list(state_t *self, const slam_graph_region_particle_list_t *msg){
    self->particle_list.clear();
    
    for(int k=0; k < msg->no_particles; k++){
        particle_t part;
        slam_graph_region_particle_t *p = &msg->particle_list[k];
        
        part.id = p->id;
        part.weight = exp(p->weight);

        for(int i=0; i < p->no_regions; i++){
            slam_graph_region_t *region = &p->region_list[i];
        
            region_t g_region; 

            g_region.id = region->id;
        
            for(int j=0; j< region->count; j++){
                slam_graph_node_t node = region->nodes[j]; 
            
                //fprintf(fp,"%d,%f\n", node.id, node.utime/1.0e6);

                //g_region.node_ids.insert( node.id);
                g_region.node_ids.push_back(node.id);
            }

            sort (g_region.node_ids.begin(), g_region.node_ids.end());
        
            for(int k=0; k < region->region_type_dist.count; k++){
                int t_class = region->region_type_dist.classes[k].type;
                double prob = region->region_type_dist.classes[k].probability;
                g_region.type_distribution.insert(make_pair(t_class, prob));            
            }

            for(int k=0; k < region->region_label_dist.count; k++){
                int t_class = region->region_label_dist.classes[k].type;
                double prob = region->region_label_dist.classes[k].probability;
                g_region.label_distribution.insert(make_pair(t_class, prob));            
            }

            for(int k=0; k < region->region_type_dist_no_lang.count; k++){
                int t_class = region->region_type_dist_no_lang.classes[k].type;
                double prob = region->region_type_dist_no_lang.classes[k].probability;
                g_region.type_distribution_no_lang.insert(make_pair(t_class, prob));            
            }

            for(int k=0; k < region->region_label_dist_no_lang.count; k++){
                int t_class = region->region_label_dist_no_lang.classes[k].type;
                double prob = region->region_label_dist_no_lang.classes[k].probability;
                g_region.label_distribution_no_lang.insert(make_pair(t_class, prob));            
            }

            part.particle_regions.insert(make_pair(region->id, g_region));
        }
        self->particle_list.insert(make_pair(p->id, part));
    }
    fprintf(stderr, "Done parsing Particle List\n");
}

void calculate_segmentation_results(state_t *self){
    fprintf(stderr, "================== Calculating Region Segmentation Accuracy =================\n");

    bool v = false;
    for(int i=0; i < self->annotation_list.size(); i++){
        annotation_t annot = self->annotation_list[i];
        int annotation_ind = self->info.get_index_for_category(annot.annotation);
        if(v)
            fprintf(stderr, "[%d] Annotation : %s => Index : %d\n", i, annot.annotation.c_str(), annotation_ind);

        //for each region calculate best overlap score 

        //get the top ones?? 

        map<int, region_t>::iterator it_r;
        for(it_r =  self->particle_regions.begin(); it_r !=  self->particle_regions.end(); it_r++){
            
            region_t region = it_r->second;
            
            int inter_max = fmin(annot.node_ids.size(), region.node_ids.size());
            int union_max = annot.node_ids.size() + region.node_ids.size();

            vector<int> inter_set(inter_max);  
            vector<int> union_set(union_max);  

            vector<int>::iterator it;
            
            it = set_intersection (annot.node_ids.begin(), annot.node_ids.end(), region.node_ids.begin(), region.node_ids.end(), inter_set.begin());

            //fprintf(stderr, "Size of Intersection : %d\n", (int) (it-inter_set.begin()));

            inter_set.resize(it-inter_set.begin());

            it = set_union (annot.node_ids.begin(), annot.node_ids.end(), region.node_ids.begin(), region.node_ids.end(), union_set.begin());

            //fprintf(stderr, "Size of Union : %d\n", (int) (it-union_set.begin()));
            
            union_set.resize(it-union_set.begin());

            if(inter_set.size()>0){
                double score = inter_set.size()/(double) union_set.size();
                if(v)
                    fprintf(stderr, "\tRegion : %d-> Inter : %d , Union : %d => Score : %.3f\n", region.id, (int) inter_set.size(), (int) union_set.size(), 
                            score);               
               
                map<int,double>::iterator it_type;
                
                double sem_score = 0;
                for(it_type = region.type_distribution.begin(); it_type != region.type_distribution.end(); it_type++){
                    if(it_type->first == annotation_ind){
                        sem_score = it_type->second;
                    }
                }

                double sem_score_no_lang = 0;
                for(it_type = region.type_distribution_no_lang.begin(); it_type != region.type_distribution_no_lang.end(); it_type++){
                    if(it_type->first == annotation_ind){
                        sem_score_no_lang = it_type->second;
                    }
                }                

                intersection_results result; 
                result.annotation = annot.annotation;
                result.annotation_id = annot.id;
                result.region_id = region.id;
                result.inter_c = inter_set.size();
                result.union_c = union_set.size();
                result.score = score;
                result.semantic_score = sem_score;
                result.semantic_score_no_lang = sem_score_no_lang;
                self->result_queue.push(result);
            }
        }
    }

    fprintf(stderr, "\n\n==========================================================\n\n");
    print_result_queue(self);
}

void calculate_results_method1(state_t *self){
    //for each region segment we calculate the label distribution based on the 
    //ground truth annotations and calculate the cosine similarity (this is more inaccurate) 
    
    region_result_queue_t queue;

    fprintf(stderr, "\n\n=============Method 1==============\n\n");

    map<int, region_t>::iterator it_r;
    for(it_r =  self->particle_regions.begin(); it_r !=  self->particle_regions.end(); it_r++){
        region_t region = it_r->second;
        fprintf(stderr, "Region : %d\n", region.id);      
        double region_size = region.node_ids.size();

        //calculate the new semantic distribution 
        map<int, double> annot_region_dist;
                
        map<int, double>::iterator it_dist;
        
        for(int i=0; i < self->annotation_list.size(); i++){
            annotation_t annot = self->annotation_list[i];
            int annotation_ind = self->info.get_index_for_category(annot.annotation);
            
            int inter_max = fmin(annot.node_ids.size(), region.node_ids.size());
            int union_max = annot.node_ids.size() + region.node_ids.size();

            vector<int> inter_set(inter_max);  

            vector<int>::iterator it;
            
            it = set_intersection (annot.node_ids.begin(), annot.node_ids.end(), region.node_ids.begin(), region.node_ids.end(), inter_set.begin());

            //fprintf(stderr, "Size of Intersection : %d\n", (int) (it-inter_set.begin()));

            inter_set.resize(it-inter_set.begin());          
            
            if(inter_set.size()>0){
                //fprintf(stderr, "\t[%d] Annotation : %s => Index : %d\n", i, annot.annotation.c_str(), annotation_ind);
                it_dist = annot_region_dist.find(annotation_ind);

                //fprintf(stderr, "Intersection Set : %d\n", (int) inter_set.size());

                if(it_dist != annot_region_dist.end()){
                    it_dist->second = it_dist->second + inter_set.size();
                }
                else{
                    annot_region_dist.insert(make_pair(annotation_ind, inter_set.size()));
                }
            }                
        }

        //Now normalize this distribution 
        for(it_dist = annot_region_dist.begin(); it_dist != annot_region_dist.end(); it_dist++){
            it_dist->second = it_dist->second / region_size; 
        }
        
        double score = cosine_sim(annot_region_dist, region.type_distribution);

        string max_region_label = self->info.get_index_for_category(get_max_category(region.type_distribution));
        string max_annot_label = self->info.get_index_for_category(get_max_category(annot_region_dist));
                                  
        fprintf(stderr, "= Region : %s -> Annotation : %s\n", max_region_label.c_str(), max_annot_label.c_str());
        fprintf(stderr, "\tRegion Size : %d => Score : %.3f\n",(int) region.node_ids.size(), score);    
        
        queue.push(make_pair(region, score));
    }

    fprintf(stderr, "\n\n==========================================================\n\n");

    print_region_result_queue(queue);
    //print_result_queue(self);
}

//here we look at the annotation regions and then evaluate the overlapping region segments 
void calculate_results_method2(state_t *self){

    //region_result_queue_t queue;

    fprintf(stderr, "\n\n=============Method 2 ==============\n\n");

    typedef object_score_t<annotation_t> annot_score_t;
    typedef priority_queue<annot_score_t,std::vector<annot_score_t>, object_compare<annot_score_t> > result_queue_t;

    result_queue_t queue; 

    map<int, region_t>::iterator it_r;
      
    for(int i=0; i < self->annotation_list.size(); i++){
        annotation_t annot = self->annotation_list[i];
        int annotation_ind = self->info.get_index_for_category(annot.annotation);
        
        double annot_size = annot.node_ids.size();
        double score = 0;
        double score_no_lang = 0;
        
        for(it_r =  self->particle_regions.begin(); it_r !=  self->particle_regions.end(); it_r++){
            region_t region = it_r->second;
            //fprintf(stderr, "Region : %d\n", region.id);      
            double region_size = region.node_ids.size();
            
            //calculate the new semantic distribution 
            map<int, double> annot_region_dist;
            
            map<int, double>::iterator it_dist;
            
            int inter_max = fmin(annot.node_ids.size(), region.node_ids.size());
            
            vector<int> inter_set(inter_max);  

            vector<int>::iterator it;
            
            it = set_intersection (annot.node_ids.begin(), annot.node_ids.end(), region.node_ids.begin(), region.node_ids.end(), inter_set.begin());

            //fprintf(stderr, "Size of Intersection : %d\n", (int) (it-inter_set.begin()));

            inter_set.resize(it-inter_set.begin());          
            
            if(inter_set.size()>0){
                //get the cosine product times the ratio of intersection to annotation size 
                score += get_liklihood_of_category(region.type_distribution, annotation_ind) * inter_set.size() / annot_size;

                score_no_lang += get_liklihood_of_category(region.type_distribution_no_lang, annotation_ind) * inter_set.size() / annot_size;
            }
        }

        //string max_region_label = self->info.get_index_for_category(get_max_category(region.type_distribution));
                                  
        fprintf(stderr, "[%d] Annotation : %s (Size : %d) => Score : %.3f / No lang : %f\n", annotation_ind, annot.annotation.c_str(), (int) annot_size, score, score_no_lang);
        
        annot_score_t res(annot, score, score_no_lang);
        queue.push(res);

    }

    fprintf(stderr, "\n\n================== Method 2 (Sorted) ====================\n\n");

    map<string, vector<annot_score_t> > semantic_accuracy; 
    map<string, vector<annot_score_t> >::iterator it_results;

    while(!queue.empty()){
        annot_score_t result = queue.top();

        it_results = semantic_accuracy.find(result.obj.annotation);
        if(it_results == semantic_accuracy.end()){
            vector<annot_score_t> results;
            results.push_back(result);
            semantic_accuracy.insert(make_pair(result.obj.annotation, results));
        }
        else{
            it_results->second.push_back(result);
        }

        //if(result.obj.annotation.compare("hallway")){
        fprintf(stderr, "[%d] Annotation (size %d) : %s -> Score : %f / No Lang : %f==> Improvement : %.2f\n", result.obj.id, (int) result.obj.node_ids.size(), 
                result.obj.annotation.c_str(), result.score, result.score_no_lang, (result.score/ result.score_no_lang - 1) * 100.0);
        //}
        queue.pop();
    }

    fprintf(stderr, "============ Accuracy by Semantic type ===========\n");

    for(it_results = semantic_accuracy.begin(); it_results != semantic_accuracy.end(); it_results++){
        fprintf(stderr, "Region Type : %s\n", it_results->first.c_str());
        vector<annot_score_t> results = it_results->second;
        double avg_accuracy = 0;
        double avg_no_lang_accuracy = 0;
        for(int i=0; i < results.size(); i++){
            annot_score_t result = results[i];
            double perc_improv = 100.0;

            fprintf(stderr, "\t[%d] Annotation (size %d) : %s -> Score : %f / No Lang : %f==> Improvement : %.2f\n", result.obj.id, (int) result.obj.node_ids.size(), 
                    result.obj.annotation.c_str(), result.score, result.score_no_lang, (result.score/ result.score_no_lang - 1) * 100.0);            
            avg_accuracy += result.score;
            avg_no_lang_accuracy += result.score_no_lang;
        }
        avg_accuracy /= (double) results.size();
        avg_no_lang_accuracy /= (double) results.size();
        fprintf(stderr, "=== Avg Accuracy : %f\n", avg_accuracy);        
        fprintf(stderr, "=== Avg No lang Accuracy : %f\n", avg_no_lang_accuracy);        
    }

    fprintf(stderr, "============ Accuracy by Semantic type ===========\n");

    for(it_results = semantic_accuracy.begin(); it_results != semantic_accuracy.end(); it_results++){
        fprintf(stderr, "Region Type : %s\n", it_results->first.c_str());
        vector<annot_score_t> results = it_results->second;
        double avg_accuracy = 0;
        double avg_no_lang_accuracy = 0;
        for(int i=0; i < results.size(); i++){
            annot_score_t result = results[i];
            double perc_improv = 100.0;

            avg_accuracy += result.score;
            avg_no_lang_accuracy += result.score_no_lang;
        }
        avg_accuracy /= (double) results.size();
        avg_no_lang_accuracy /= (double) results.size();
        fprintf(stderr, "=== Avg Accuracy : %f\n", avg_accuracy);        
        fprintf(stderr, "=== Avg No lang Accuracy : %f\n", avg_no_lang_accuracy);        
    }

    //print_region_result_queue(queue);
    //print_result_queue(self);
}

//here we look at the annotation regions and then evaluate the overlapping region segments 
void calculate_results_method2_all_particles(state_t *self){

    //region_result_queue_t queue;

    fprintf(stderr, "\n\n=============All Particles==============\n\n");

    map<int, region_t>::iterator it_r;
    
    vector<annotation_result_t> annotation_results;

    for(int i=0; i < self->annotation_list.size(); i++){
        annotation_t annot = self->annotation_list[i];
        int annotation_ind = self->info.get_index_for_category(annot.annotation);
        
        double annot_size = annot.node_ids.size();

        map<int, particle_t>::iterator it_p;

        annotation_result_t a_result;

        a_result.annot = annot;

        double overall_score = 0;
        double overall_score_no_lang = 0;
        
        for(it_p = self->particle_list.begin(); it_p != self->particle_list.end(); it_p++){
            double score = 0;
            double score_no_lang = 0;
                        
            for(it_r =  it_p->second.particle_regions.begin(); it_r !=  it_p->second.particle_regions.end(); it_r++){
                region_t region = it_r->second;
                //fprintf(stderr, "Region : %d\n", region.id);      
                double region_size = region.node_ids.size();
            
                //calculate the new semantic distribution 
                map<int, double> annot_region_dist;
            
                map<int, double>::iterator it_dist;
            
                int inter_max = fmin(annot.node_ids.size(), region.node_ids.size());
            
                vector<int> inter_set(inter_max);  

                vector<int>::iterator it;
            
                it = set_intersection (annot.node_ids.begin(), annot.node_ids.end(), region.node_ids.begin(), region.node_ids.end(), inter_set.begin());

                //fprintf(stderr, "Size of Intersection : %d\n", (int) (it-inter_set.begin()));

                inter_set.resize(it-inter_set.begin());          
            
                if(inter_set.size()>0){
                    //get the cosine product times the ratio of intersection to annotation size 
                    score += get_liklihood_of_category(region.type_distribution, annotation_ind) * inter_set.size() / annot_size;

                    score_no_lang += get_liklihood_of_category(region.type_distribution_no_lang, annotation_ind) * inter_set.size() / annot_size;
                }
            }

            //string max_region_label = self->info.get_index_for_category(get_max_category(region.type_distribution));
                                  
            fprintf(stderr, "Particle %d => [%d] Annotation : %s (Size : %d) => Score : %.3f / No lang : %f\n", it_p->second.id, annotation_ind, annot.annotation.c_str(), (int) annot_size, score, score_no_lang);

            semantic_result_t p_result; 
            p_result.score = score;
            p_result.score_no_lang = score_no_lang;
            p_result.weight = it_p->second.weight;

            overall_score += score * it_p->second.weight;
            overall_score_no_lang += score_no_lang * it_p->second.weight;

            a_result.result.insert(make_pair(it_p->second.id, p_result));
        }

        a_result.avg_score = overall_score;
        a_result.avg_score_no_lang = overall_score_no_lang;
        
        annotation_results.push_back(a_result);
        
        
        /*annot_score_t res(annot, score, score_no_lang);
          queue.push(res);*/

    }

    sort(annotation_results.begin(), annotation_results.end(), compare_average_results);
    
    fprintf(stderr, "\n\n==========================================================\n\n");

    for(int i=0; i < annotation_results.size(); i++){
        if(annotation_results[i].annot.annotation.compare("hallway")){
            fprintf(stderr, "Annotation [%d] : %s (Size : %d) => Score : %.3f / %.3f\n", annotation_results[i].annot.id, 
                    annotation_results[i].annot.annotation.c_str(), (int) annotation_results[i].annot.node_ids.size(), 
                    annotation_results[i].avg_score, annotation_results[i].avg_score_no_lang);
        }
    }

    /*fprintf(stderr, "\n\n==========================================================\n\n");

    while(!queue.empty()){
        annot_score_t result = queue.top();
        if(result.obj.annotation.compare("hallway")){
            fprintf(stderr, "[%d] Annotation (size %d) : %s -> Score : %f / No Lang : %f\n", result.obj.id, (int) result.obj.node_ids.size(), 
                    result.obj.annotation.c_str(), result.score, result.score_no_lang);
        }
        queue.pop();
        }*/

    //print_region_result_queue(queue);
    //print_result_queue(self);
}

//find the max graph 
static void on_topo_graph (const lcm_recv_buf_t *rbuf, const char *channel,
                           const slam_graph_region_particle_list_t *msg, void *user)
{
    state_t *self = (state_t *)user;
    g_assert(self);

    double max_prob = 0;

    slam_graph_region_particle_t *max_part = NULL;
    
    //we should clear this 
    for(int i=0; i <  msg->no_particles; i++){
        slam_graph_region_particle_t *p = slam_graph_region_particle_t_copy(&msg->particle_list[i]);
        
        if(max_prob < exp(p->weight)){
            max_part = p;
            max_prob = exp(p->weight);
        }
    }
    
    if(1){ //do this only for the max particle 
        
        if(max_part != NULL){
            //copy over 
            fprintf(stderr, "Found Max Particle\n");
            if(self->max_particle != NULL){
                slam_graph_region_particle_t_destroy(self->max_particle);
            }
            self->max_particle = slam_graph_region_particle_t_copy(max_part);
            parse_particle(self);
            
            //do the calculation 
            calculate_segmentation_results(self);
            //fprintf(stderr, "Method 1\n");
            //calculate_results_method1(self);        
            calculate_results_method2(self);        
        }
    }
    else{
        //do this for all the particles??  - in which case we need to amalgamate the results for each particle 
        parse_particle_list(self, msg);

        fprintf(stderr, "Parsing particle list\n");
        calculate_results_method2_all_particles(self);
    }
}

void print_annotation(state_t *self){
    for(int i=0; i < self->annotation_list.size(); i++){
        annotation_t annot = self->annotation_list[i];
        fprintf(stderr, "=== Annotation : %s\n", annot.annotation.c_str());
        vector<int>::iterator it;
        for(it = annot.node_ids.begin(); it != annot.node_ids.end(); it++){
            fprintf(stderr, "\t%d\n", *it);
        }
    }
}

void load_annotations(state_t *self){
    std::ifstream infile(self->annotation_log);
     
    for( std::string line; getline(infile, line ); ){
        cout << line << endl;
        char *str = strdup(line.c_str());
        char * pch = strtok(str, ",");
        
        int count = 0;
        annotation_t annot;
        annot.id = self->annotation_list.size();

        while (pch != NULL){
            if(count == 0){
                //skip
            }
            else if(count == 1){
                annot.annotation = string(pch);
                fprintf(stderr, "Annotation : %s\n", pch);
            }
            else{
                //printf ("%s\n",pch);
                int node_id = atoi(pch);
                annot.node_ids.push_back(node_id);
            }
            count++;
            //printf ("%s\n",pch);
            pch = strtok (NULL, ",");
        }
        
        sort (annot.node_ids.begin(), annot.node_ids.end());
        self->annotation_list.push_back(annot);
    }
    //print_annotation(self);
}

int main(int argc, char *argv[])
{    
    const char *optstring = "a:n:h";
    char c;

    state_t *self = new state_t();
    self->lcm = lcm_create(NULL);
    self->max_particle = NULL;
    
    self->annotation_log = NULL;
    self->node_utime = NULL;

    struct option long_opts[] = { { "help", no_argument, 0, 'h' },
                                  {  "annotation", required_argument, 0, 'a' },
                                  {  "node_utimes", required_argument, 0, 'n' },
                                  { 0, 0, 0, 0 } };

    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
        switch (c) {
        case 'a':
            self->annotation_log = strdup(optarg);
            break;
        case 'n':
            self->node_utime = strdup(optarg);
            break;
        case 'h':
        default:
            usage(argv[0]);
            return 1;
        }
    }
    
    if(self->annotation_log == NULL){
        usage(argv[0]);
        return -1;
    }

    if(self->node_utime == NULL){
        fprintf(stderr, "Node utime not loaded - assuming node ids are consistant\n");
    }

    //load from the file and build the struct 
    load_annotations(self);

    slam_graph_region_particle_list_t_subscribe(self->lcm, "REGION_PARTICLE_ISAM_RESULT", on_topo_graph, self);

    request_results(self);

    while (true)
        lcm_handle(self->lcm);
}
