#include "SlamGraph.hpp"

SlamGraph::SlamGraph(int64_t _utime)
{
    slam = new Slam();
    Properties prop;
    slam->set_properties(prop);
    utime = _utime;

    updated = 1;
    //add the origin node 
    origin_node = new Pose2d_Node();
    slam->add_node(origin_node);
    //add prior for the origin 
    Pose2d prior_origin(0., 0., 0.);
    Noise noise = SqrtInformation(10000. * eye(3));
    prior = new Pose2d_Factor(origin_node, prior_origin, noise);
    slam->add_factor(prior);
    no_edges_added = 0;
    status = 0;
}

 SlamGraph::~SlamGraph()
{
    delete slam;
    //might want to look at this 
    delete prior;
    delete origin_node;
    map<int, SlamNode *>::iterator it;
    for ( it= slam_nodes.begin() ; it != slam_nodes.end(); it++ )
        delete it->second;
    slam_nodes.clear();
    map<int, SlamConstraint *>::iterator c_it;
    for ( c_it= slam_constraints.begin() ; c_it != slam_constraints.end(); c_it++ ){        
        delete c_it->second;
    }

    slam_constraints.clear();
    for ( c_it= failed_slam_constraints.begin() ; c_it != failed_slam_constraints.end(); c_it++ ){        
        delete c_it->second;
    }
    failed_slam_constraints.clear();
}

void SlamGraph::runSlam()
{
    if(!status){
        status = 1; 
        fprintf (stdout, "Performing batch optimiziation\n");
        slam->batch_optimization();
    }
    else{
        fprintf(stderr, "\n\nUpdating Slam\n\n");
        slam->update();
    }
    //update the covariances 
    const Covariances& covariances =  slam->covariances().clone();

    int64_t cov_stime = bot_timestamp_now();

    //technically the first node shouldn't have a covariance - but we have a start node and then the first node
    // for(int i=0; i < slam_node_list.size(); i++){
    //     Covariances::node_lists_t node_lists;
    //     list<Node*> nodes;
    //     SlamNode *node = slam_node_list.at(i);
    //     if(node != NULL){
    //         fprintf (stdout, "++ Recovering cov for node id = %d\n", node->id);
    //         nodes.push_back(node->pose2d_node);
    //         node_lists.push_back(nodes);
    //         nodes.clear();
    //     }

    //     list<MatrixXd> cov_blocks = covariances.marginal(node_lists);
    //     int64_t cov_etime = bot_timestamp_now();
    
    //     int nd_ind = 0;
    //     for (list<MatrixXd>::iterator it = cov_blocks.begin(); it!=cov_blocks.end(); it++, nd_ind++) {
    //         MatrixXd cv = *it;
    //         for(int k=0; k < 9; k++)
    //             node->cov[k] = cv(k);
    //     }
    // }   
    


    Covariances::node_lists_t node_lists;
    list<Node*> nodes;
    //technically the first node shouldn't have a covariance - but we have a start node and then the first node
    for(int i=0; i < slam_node_list.size(); i++){
        SlamNode *node = slam_node_list.at(i);
        if(node != NULL){
            nodes.push_back(node->pose2d_node);
            node_lists.push_back(nodes);
            nodes.clear();
        }
    }

    list<MatrixXd> cov_blocks = covariances.marginal(node_lists);
    int64_t cov_etime = bot_timestamp_now();
    
    int nd_ind = 0;
    for (list<MatrixXd>::iterator it = cov_blocks.begin(); it!=cov_blocks.end(); it++, nd_ind++) {
        SlamNode *node = slam_node_list.at(nd_ind);
        MatrixXd cv = *it;
        if(node != NULL){
            for(int k=0; k < 9; k++){
                node->cov[k] = cv(k);
            }
        }
        else{
            fprintf(stderr, "Error - Covariance - something funcky going on\n");
        }
    }    
}

void SlamGraph::updateBoundingBoxes(){
    for(int i=0; i < slam_node_list.size(); i++){
        SlamNode *node = slam_node_list.at(i);
        node->updateBoundingBox();
    }
}

SlamNode * SlamGraph::getSlamNode(int64_t id){
    map<int, SlamNode *>::iterator it;
    it = slam_nodes.find(id);
    if(it == slam_nodes.end()){
        return NULL;
    }
    return it->second;
}

SlamNode * SlamGraph::addSlamNode(NodeScan *pose)
{
    //slam_nodes.push_back(node);
    //slam_nodes.insert(node);
    SlamNode *node = new SlamNode(pose);
    //LabelDistribution *ld = new LabelDistribution();
    //node->labeldist = ld;
    slam_nodes.insert ( pair<int, SlamNode *>(node->id,node));
    slam_node_list.push_back(node);
    //also add node to slam //and maybe constraints 
    slam->add_node(node->pose2d_node);
    return node;
    //the constraints should be added to the node - then its cleaner 
}

/*SlamNode * SlamGraph::addSlamNode(NodeScan *pose, LabelDistribution *ld)
{
    //slam_nodes.push_back(node);
    //slam_nodes.insert(node);
    SlamNode *node = new SlamNode(pose, ld);
    //LabelDistribution *ld = new LabelDistribution();
    //node->labeldist = ld;
    slam_nodes.insert ( pair<int, SlamNode *>(node->id,node));
    slam_node_list.push_back(node);
    //also add node to slam //and maybe constraints 
    slam->add_node(node->pose2d_node);
    return node;
    //the constraints should be added to the node - then its cleaner 
    }*/

SlamNode * SlamGraph::addSlamNode(SlamNode *_node){
    SlamNode *node = new SlamNode(_node);
    slam_nodes.insert ( pair<int, SlamNode *>(node->id,node));
    slam_node_list.push_back(node);
    //also add node to slam //and maybe constraints 
    slam->add_node(node->pose2d_node);
    return node;
    //the constraints should be added to the node - then its cleaner 
 }
 
 MatrixXd SlamGraph::getCovariance(SlamNode *node){
    MatrixXd cov(3,3);
    for(int i=0; i < 9; i++){
        cov(i) = node->cov[i];
    }
    return cov; 
}

//this is expensive - dont call too often 
map<int,MatrixXd> SlamGraph::getCovariances(SlamNode *nd1, vector<SlamNode *> nd_list){
    const Covariances& covariances =  slam->covariances();

    Covariances::node_lists_t cv_node_lists;
    
    //fprintf(stderr, "List Size : %d\n", nd_list.size());

    list<Node*> nd_list_req;
    for(int i=0; i < nd_list.size(); i++){
        //Covariances::node_lists_t nd_cov_list;
        nd_list_req.push_back(nd1->pose2d_node);
        SlamNode *nd2 = nd_list.at(i);
        nd_list_req.push_back(nd2->pose2d_node);
        cv_node_lists.push_back(nd_list_req);
        nd_list_req.clear();
    }

    list<MatrixXd> cov_blocks = covariances.marginal(cv_node_lists);

    map<int,MatrixXd> cov_res;
    int nd_ind = 0;

    //fprintf(stderr, " Cov Size : %d - List Size : %d\n",   cov_blocks.size(), nd_list.size());
    for (list<MatrixXd>::iterator it = cov_blocks.begin(); it!=cov_blocks.end(); it++, nd_ind++) {
        SlamNode *node = nd_list.at(nd_ind);
        MatrixXd cv = *it;
        cov_res.insert(pair<int, MatrixXd>(node->id, cv));
    }
    //int64_t p_start = bot_timestamp_now();
    return cov_res;
}

//this is expensive - dont call too often 
MatrixXd SlamGraph::getCovariances(SlamNode *node1, SlamNode *node2){
    const Covariances& covariances =  slam->covariances();
    list<Node*> nd_list;
    //Covariances::node_lists_t nd_cov_list;
    nd_list.push_back(node1->pose2d_node);
    nd_list.push_back(node2->pose2d_node);
    //nd_cov_list.push_back(nd_list);
    
    //int64_t p_start = bot_timestamp_now();
    return covariances.marginal(nd_list);
}

//measurement probability calculation given the slam covariance between two nodes
//the means of the two nodes and the sm constraint
double SlamGraph::calculateProbability(SlamNode *node1, SlamNode *node2, Matrix3d cov_sm, Pose2d sm_constraint){
    MatrixXd cov = getCovariances(node1, node2);
    Pose2d value1 = node1->getPose();
    Pose2d value2 = node2->getPose();
    double x1 = value2.x(), y1 = value2.y(), t1 = value2.t();
    double x2 = value1.x(), y2 = value1.y(), t2 = value1.t();
    double c1 = cos(t1), s1 = sin(t1);
    
    //Jacobian 
    MatrixXd H(3,6); 
    
    int v = 0;
    
    H.setZero();
    H(0,0) = c1; H(0,1) = s1; H(0,2) = 0;
    H(0,3) = -c1; H(0,4) = -s1; H(0,5) = -(x2-x1)*s1 + (y2-y1)*c1;
    H(1,0) = -s1; H(1,1) = c1; H(1,2) = 0;
    H(1,3) = s1; H(1,4) = -c1; H(1,5) = -(x2-x1) * c1 - (y2-y1) *s1;
    H(2,2) = 1;
    H(2,5) = -1;
    
    double mean_x = (x2-x1)*c1 + (y2-y1)*s1;
    double mean_y = -(x2-x1)*s1 + (y2-y1)*c1;
    double mean_t = t2-t1;
    
    if(v){
        fprintf(stderr, "Mean from graph : %f,%f,%f => Mean from scanmatch : %f,%f,%f\n", 
                mean_x, mean_y, mean_t, sm_constraint.x(), 
                sm_constraint.y(),
                sm_constraint.t());

        cout << "H : " << endl << H << endl;
        cout << "Joint Covariance : " << endl << cov << endl;
    }
    Matrix3d result = H * cov * H.transpose() + cov_sm;
    if(v){
        cout << "Result  : " << endl << result << endl;
    }

    
    MatrixXd dx(3,1);
    
    //mean for the new random variable 
    dx(0) = (mean_x - sm_constraint.x());
    dx(1) = (mean_y - sm_constraint.y());
    dx(2) = (mean_t - sm_constraint.t());
    if(v)
        cout << "Distance : " << dx<<endl; 
    MatrixXd mh_dist = dx.transpose() * result.inverse() * dx;
    if(v)
        cout << "Mahalanobis Distance : " << mh_dist<<endl;
    double deter = result.determinant();
    if(v)
        cout << "Determinanant : " << deter << endl;

    //applying to gaussian probability 
    double prob = exp(-mh_dist(0)/2)/ (pow((2 * M_PI), 1.5) * pow(deter, 0.5));
    if(v)
        cout << "Probability : " << prob << endl;
    return prob;
}

void SlamGraph::updateSupernodeIDs(){

    //write this algorithm properly 
    int max_id = slam_node_list.size()-1;
    
    SlamNode *last_nd = getSlamNode(max_id);

    if(!last_nd->slam_pose->is_supernode)
        return;

    last_nd->parent_supernode = last_nd->id;
    int prev_supernode = getPreviousSupernodeID(max_id,SUPERNODE_SEARCH_DISTANCE);
    int next_supernode = getNextSupernodeID(max_id,SUPERNODE_SEARCH_DISTANCE);
    
    for(int i = max_id-1; i >prev_supernode; i--){ 
        SlamNode *nd = getSlamNode(i);

        if(nd->id <= last_nd->id){
            nd->parent_supernode = last_nd->id;
        }
        else{
            nd->parent_supernode = next_supernode;
        }

        /*if(fabs(nd->id - prev_supernode) < fabs(nd->id - last_nd->id)){
            nd->parent_supernode = prev_supernode;
        }
        else{
            nd->parent_supernode = last_nd->id;
            }*/
    }

    /*for(int i = max_id; i >= 0; i--){
        SlamNode *nd = getSlamNode(i);
        fprintf(stderr, "\t[%d] - Parent : %d\n", i, nd->parent_supernode);
        }*/
}

int SlamGraph::getPreviousSupernodeID(int ind, int search_distance){
    int start_super_ind = fmax(0, ind - search_distance);
    //search near the region - and find the closest two nodes - that belong to the relavent supernodes 
    int max_id = slam_node_list.size()-1;
    int prev_supernode = -1;
    for(int i = ind -1; i >= start_super_ind; i--){ 
        if(i < 0 || i > max_id)
            break;
        SlamNode *nd1 = getSlamNode(i);
        if(nd1 == NULL)
            continue;

        NodeScan *pose_1 = nd1->slam_pose;
        
        //fprintf(stderr, "\t\t%d : %d\n", i, pose_1->is_supernode);
        //if this node is not the current node and is also a supernode - break
        if(pose_1->is_supernode == true){
            prev_supernode = i;
            break;
        }
    }
    return prev_supernode;
}


int SlamGraph::getNextSupernodeID(int ind, int search_distance){

    //search near the region - and find the closest two nodes - that belong to the relavent supernodes 
    int max_id = slam_node_list.size()-1;
    int end_super_ind = fmin(max_id, ind + search_distance);
    int next_supernode = -1;
    for(int i = ind +1; i <= end_super_ind; i++){ 
        if(i < 0 || i > max_id)
            break;
        SlamNode *nd1 = getSlamNode(i);
        if(nd1 == NULL)
            continue;
        NodeScan *pose_1 = nd1->slam_pose;
        //if this node is not the current node and is also a supernode - break
        if(pose_1->is_supernode == true){
            next_supernode = i;
            break;
        }
    }
    return next_supernode;
}

int SlamGraph::belongsToCurrentSupernode(int ind, int curr_sn, int prev_sn, int next_sn){
    //nodes behind belong to the supernode 
    if(0){
        if(prev_sn < 0)
            if(ind <= curr_sn){
                return 1;
            }
        
        if(ind > prev_sn && ind <= curr_sn){
            return 1;
        }
        return 0;
    }
    if(0){
        if(next_sn < 0)
            if(ind >= curr_sn){
                return 1;
            }
        
        if(ind < next_sn && ind >= curr_sn){
            return 1;
        }
        return 0;
    }    
    else{
        int dist_to_prev = 100;
        int dist_to_next = 100;
        int dist_to_current = fabs(ind - curr_sn);
        if(prev_sn >=0){
            dist_to_prev = fabs(ind - prev_sn);
        }
        if(next_sn >=0){
            dist_to_next = fabs(ind - next_sn);
        }
        if(dist_to_current <= dist_to_prev && dist_to_current <= dist_to_next){
            return 1;
        }
        return 0;
    }
}


void SlamGraph::addOriginConstraint(SlamNode *node1, Pose2d transform, Noise noise){
    Pose2d_Node *current_node = node1->pose2d_node;
    origin_constraint = new Pose2d_Pose2d_Factor(origin_node, current_node, 
                                                 transform, 
                                                 noise);
    slam->add_factor(origin_constraint);
}

SlamConstraint* SlamGraph::addConstraint(SlamNode *node1,  SlamNode *node2, SlamNode *actualnode1, 
                                         SlamNode *actualnode2, Pose2d transform, Noise noise, 
                                         double hit_pct, int32_t type, int32_t status){
    int id = no_edges_added;
    SlamConstraint *constraint = addConstraint(id, node1,  node2, actualnode1, actualnode2, transform, noise, hit_pct, type, status);
    return constraint;
}


SlamConstraint* SlamGraph::addConstraint(int id, SlamNode *node1,  SlamNode *node2, SlamNode *actualnode1, 
                                         SlamNode *actualnode2, Pose2d transform, Noise noise, 
                                         double hit_pct, int32_t type, int32_t status)
{
    SlamConstraint *constraint = new SlamConstraint(id, node1, node2, actualnode1, actualnode2, transform, noise, hit_pct, 1, type, status);
    //lets add the relavent node as well 
    node1->constraints.insert(pair<int, SlamConstraint *>(node2->id,constraint));
    slam_constraints.insert(pair<int, SlamConstraint *>(constraint->id,constraint));
    slam->add_factor(constraint->ct_pose2d);
    no_edges_added++;

    // Bleed labels between nodes if i) they are both supernodes and ii) they are not sequential
    // iii) node that is bled from has language observation and iv) constraint type is not a language edge
    int node1_prev_supernode_id = getPreviousSupernodeID (node1->id, SUPERNODE_SEARCH_DISTANCE);
    int node2_prev_supernode_id = getPreviousSupernodeID (node2->id, SUPERNODE_SEARCH_DISTANCE);
    if (node1->slam_pose->is_supernode && node1->slam_pose->is_supernode && (node1->id != node2_prev_supernode_id)
        && (node2->id != node1_prev_supernode_id) && (type != SLAM_GRAPH_EDGE_T_TYPE_LANGUAGE) ) {
        
        LabelDistribution *ld1 = node1->labeldist;
        node2->labeldist->bleedLabels (ld1);

        LabelDistribution *ld2 = node2->labeldist;
        node1->labeldist->bleedLabels (ld2);
   }
    
    return constraint;
}

void SlamGraph::clearDummyConstraints(){
    map<int, SlamConstraint *>::iterator c_it;
    for ( c_it= failed_slam_constraints.begin() ; c_it != failed_slam_constraints.end(); c_it++ ){        
        delete c_it->second;
    }
    failed_slam_constraints.clear();
}

void SlamGraph::addDummyConstraint(SlamNode *node1,  SlamNode *node2, SlamNode *actualnode1, 
                                   SlamNode *actualnode2, Pose2d transform, Noise noise, 
                                   double hit_pct, int32_t type, int32_t status){
    int id = failed_slam_constraints.size();
    SlamConstraint *constraint = new SlamConstraint(id, node1, node2, actualnode1, actualnode2, 
                                                    transform, noise, hit_pct, 0, type, status);
    //node1->constraints.insert(pair<int, SlamConstraint *>(node2->id, constraint));
    failed_slam_constraints.insert(pair<int, SlamConstraint *>(id,constraint));
    //no_edges_added++;
}

void SlamGraph::addDummyConstraint(int id, SlamNode *node1,  SlamNode *node2, SlamNode *actualnode1, 
                                   SlamNode *actualnode2, Pose2d transform, Noise noise, 
                                   double hit_pct, int32_t type, int32_t status){
    SlamConstraint *constraint = new SlamConstraint(id, node1, node2, actualnode1, actualnode2, 
                                                    transform, noise, hit_pct, 0, type, status);
    //node1->constraints.insert(pair<int, SlamConstraint *>(node2->id, constraint));
    failed_slam_constraints.insert(pair<int, SlamConstraint *>(id,constraint));
    //no_edges_added++;
}

int SlamGraph::hasConstraint(int const_id){// id_1, int id_2){
    map<int, SlamConstraint *>::iterator it; 
    it = slam_constraints.find(const_id);//->second;
    
    if(it == slam_constraints.end())
        return 0;

    SlamConstraint *ct = it->second;

    if(ct==NULL) //this shouldnt be
        return 0;

    return 1; 
}

int SlamGraph::hasConstraint(int id_1, int id_2, int32_t type){
    SlamNode *node1 = getSlamNode(id_1);
    SlamNode *node2 = getSlamNode(id_2);

    map<int, SlamConstraint *>::iterator it; 
    it = node1->constraints.find(id_2);//->second;
    
    if(it == node1->constraints.end()){
        return 0;
    }

    SlamConstraint *ct = it->second;

    if(ct==NULL) //this shouldnt be
        return 0;
    
    if(ct->type != type)
        return 0;
    
    return 1; 
}

int SlamGraph::hasConstraint(int id_1, int id_2){
    SlamNode *node1 = getSlamNode(id_1);
    SlamNode *node2 = getSlamNode(id_2);

    map<int, SlamConstraint *>::iterator it; 
    it = node1->constraints.find(id_2);//->second;
    
    if(it == node1->constraints.end())
        return 0;

    SlamConstraint *ct = it->second;

    if(ct==NULL) //this shouldnt be
        return 0;
    
    return 1; 
}

int SlamGraph::getConstraintID(int id_1, int id_2, int32_t type){
    SlamNode *node1 = getSlamNode(id_1);
    SlamNode *node2 = getSlamNode(id_2);

    map<int, SlamConstraint *>::iterator it; 
    it = node1->constraints.find(id_2);//->second;
    
    if(it == node1->constraints.end())
        return -1;

    SlamConstraint *ct = it->second;

    if(ct==NULL) //this shouldnt be
        return -1;
    
    if(ct->type != type)
        return -1;
    
    return ct->id; 
}

int SlamGraph::removeConstraint(int id_1, int id_2, int32_t type){
    SlamNode *node1 = getSlamNode(id_1);
    SlamNode *node2 = getSlamNode(id_2);

    map<int, SlamConstraint *>::iterator it; 
    it = node1->constraints.find(id_2);//->second;
    
    if(it == node1->constraints.end())
        return 0;

    SlamConstraint *ct = it->second;

    if(ct==NULL) //this shouldnt be
        return 0;

    if(ct->type != type)
        return 0;

    node1->constraints.erase(it);

    it = slam_constraints.find(ct->id); 
    slam_constraints.erase(it);

    if(type == SLAM_GRAPH_EDGE_T_TYPE_LANGUAGE && ct->ct_pose2d != NULL){
        slam->remove_factor(ct->ct_pose2d);
    }
    else if(type == SLAM_GRAPH_EDGE_T_TYPE_SM_LC && ct->ct_pose2d != NULL){
        slam->remove_factor(ct->ct_pose2d);
    }

    delete ct; 

    return 0;
}

int SlamGraph::removeConstraint(int const_id){
    map<int, SlamConstraint *>::iterator it; 
    it = slam_constraints.find(const_id);
    
    if(it == slam_constraints.end())
        return 0;

    SlamConstraint *ct = it->second;

    if(ct==NULL){ //this shouldnt be
        return 0;
    }
    
    SlamNode *node1 = ct->node1;
    int id_2 = ct->node2->id;

    if (ct->ct_pose2d)
        slam->remove_factor(ct->ct_pose2d);

    slam_constraints.erase(it);

    it = node1->constraints.find(id_2);//->second;
    
    if(it == node1->constraints.end()){
        return 0;
    }

    node1->constraints.erase(it);
    
    delete ct; 

    return 0;
}

int SlamGraph::hasConstraint(int const_id, int32_t *type, int32_t *status){
    map<int, SlamConstraint *>::iterator it; 
    it = slam_constraints.find(const_id);
    
    if(it == slam_constraints.end()){
        *type = -1;
        *status = -1;
        return 0;
    }

    SlamConstraint *ct = it->second;

    if(ct==NULL){ //this shouldnt be
        *type = -1;
        *status = -1;
        return 0;
    }
    
    *type = ct->type;
    *status = ct->status;
    return 1;    
}

SlamConstraint * SlamGraph::getConstraint(int const_id){
    map<int, SlamConstraint *>::iterator it; 
    it = slam_constraints.find(const_id);

    if(it == slam_constraints.end()){
        return NULL;
    }

    return it->second;    
}
