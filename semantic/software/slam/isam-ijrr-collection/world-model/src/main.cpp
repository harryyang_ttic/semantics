#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <bot_core/bot_core.h>
#include <bot_frames/bot_frames.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>
#include <lcmtypes/slam_lcmtypes.h>

#include <vector>
using namespace std;

#define LASER_BUFFER_SIZE 500
#define CAMERA_BUFFER_SIZE 100
#define POSE_LIST_SIZE 10

typedef struct _laser_pose_t{
    bot_core_planar_lidar_t *laser;
    BotTrans pose; //to local frame
} laser_pose_t;

typedef struct _camera_pose_t{
    bot_core_image_t *image;
    BotTrans pose; //to local frame
} camera_pose_t;

laser_pose_t *copy_laser_pose(laser_pose_t *lp){
    laser_pose_t *new_lp = (laser_pose_t *) calloc(1,sizeof(laser_pose_t));
    new_lp->laser = bot_core_planar_lidar_t_copy(lp->laser);
    new_lp->pose = lp->pose;
    return new_lp;
}

camera_pose_t *copy_camera_pose(camera_pose_t *lp){
    camera_pose_t *new_lp = (camera_pose_t *) calloc(1,sizeof(camera_pose_t));
    new_lp->image = bot_core_image_t_copy(lp->image);
    new_lp->pose = lp->pose;
    return new_lp;
}

laser_pose_t *find_closest_laser_pose_from_utime(BotPtrCircular *laser_buffer, int64_t utime){
    double time_gap = 1000000.0;
    laser_pose_t *lp = NULL;
    int updated = 0;
    for (int i=0; i < bot_ptr_circular_size(laser_buffer); i++){
        laser_pose_t *cl = (laser_pose_t *) bot_ptr_circular_index(laser_buffer, i);
        double c_gap = fabs(utime - cl->laser->utime);
        if(time_gap > c_gap){
            time_gap = c_gap;
            lp = cl;
        }
        else if(updated){
            //we already have found the closest node - we can just dump the remaining buffer
            //because time gap is going up again 
            break;
        }
    }
    lp = copy_laser_pose(lp);
    bot_ptr_circular_clear(laser_buffer);
}

camera_pose_t *find_closest_camera_pose_from_utime(BotPtrCircular *camera_buffer, int64_t utime){
    double time_gap = 1000000.0;
    camera_pose_t *lp = NULL;
    int updated = 0;
    for (int i=0; i < bot_ptr_circular_size(camera_buffer); i++){
        camera_pose_t *cl = (camera_pose_t *) bot_ptr_circular_index(camera_buffer, i);
        double c_gap = fabs(utime - cl->image->utime);
        if(time_gap > c_gap){
            time_gap = c_gap;
            lp = cl;
        }
        else if(updated){
            //we already have found the closest node - we can just dump the remaining buffer
            //because time gap is going up again 
            break;
        }
    }
    lp = copy_camera_pose(lp);
    bot_ptr_circular_clear(camera_buffer);
}

class WorldModel
{

public:
    BotParam   *param;
    lcm_t      *lcm;
    BotFrames  *frames;
    int verbose; 

    //we keep the buffer of the last sequences here - and moved when a new node is added 
    BotPtrCircular *f_laser_cbuf;
    BotPtrCircular *r_laser_cbuf;
    BotPtrCircular *camera_cbuf;

    //we keep the matched ones here 
    vector<laser_pose_t *> f_laser_obs;
    vector<laser_pose_t *> r_laser_obs;
    vector<camera_pose_t *> camera_obs;
    vector<slam_graph_particle_list_t *> particle_history;
    vector<slam_laser_pose_t *> slam_laser_obs;

    WorldModel(){
        lcm = bot_lcm_get_global(NULL);
        param = bot_param_new_from_server(lcm, 0);
        frames = bot_frames_get_global(lcm, param);
        f_laser_cbuf = bot_ptr_circular_new(LASER_BUFFER_SIZE,destroy_laser , this);
        r_laser_cbuf = bot_ptr_circular_new(LASER_BUFFER_SIZE,destroy_laser , this);
        camera_cbuf = bot_ptr_circular_new(CAMERA_BUFFER_SIZE,destroy_camera , this);
    }

    static void destroy_laser(void *user, void *p){
        laser_pose_t *l = (laser_pose_t *)p;
        bot_core_planar_lidar_t_destroy(l->laser);
        free(l);
    }

    static void destroy_camera(void *user, void *p){
        camera_pose_t *c = (camera_pose_t *)p;
        bot_core_image_t_destroy(c->image);
        free(c);
    }

    

    laser_pose_t *get_laser_pose(const char *name, const bot_core_planar_lidar_t * msg){
        laser_pose_t *lp = (laser_pose_t *) calloc (1, sizeof(laser_pose_t));
        lp->laser = bot_core_planar_lidar_t_copy(msg);
        bot_frames_get_trans_with_utime(frames, name, "local", msg->utime, &lp->pose);
        return lp;
    }

    camera_pose_t *get_camera_pose(const char *name, const bot_core_image_t * msg){
        camera_pose_t *cp = (camera_pose_t *) calloc (1, sizeof(camera_pose_t));
        cp->image = bot_core_image_t_copy(msg);
        bot_frames_get_trans_with_utime(frames, name, "local", msg->utime, &cp->pose);
        return cp;
    }

    static void on_f_laser(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
                           const bot_core_planar_lidar_t * msg, void * user  __attribute__((unused))) {
        WorldModel *s = (WorldModel *) user; 
        fprintf(stderr, "Front Laser\n");
        bot_ptr_circular_add(s->f_laser_cbuf, s->get_laser_pose("SKIRT_FRONT", msg));//bot_core_planar_lidar_t_copy(msg));
        //s->f_laser_obs.push_back(bot_core_planar_lidar_t_copy(msg));
    }

    static void on_r_laser(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
                           const bot_core_planar_lidar_t * msg, void * user  __attribute__((unused))) {
        WorldModel *s = (WorldModel *) user; 
        fprintf(stderr, "Rear Laser\n");
        //s->r_laser_obs.push_back(bot_core_planar_lidar_t_copy(msg));
        //bot_ptr_circular_add(s->r_laser_cbuf, bot_core_planar_lidar_t_copy(msg));
        bot_ptr_circular_add(s->r_laser_cbuf, s->get_laser_pose("SKIRT_REAR", msg));
    }

    static void on_camera(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
                          const bot_core_image_t * msg, void * user  __attribute__((unused))) {
        WorldModel *s = (WorldModel *) user; 
        fprintf(stderr, "Front Laser\n");
        //bot_ptr_circular_add(s->camera_cbuf, bot_core_image_t_copy(msg));
        if(!strcmp(channel, "FRNT_IMAGE_BUMBLEBEE")){
            bot_ptr_circular_add(s->camera_cbuf, s->get_camera_pose("bumblebee", msg));
        }
        else if(!strcmp(channel, "DRAGONFLY_IMAGE")){
            bot_ptr_circular_add(s->camera_cbuf, s->get_camera_pose("dragonfly", msg));
        }
    }

    static void on_slam_laser(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
                              const slam_laser_pose_t * msg, void * user  __attribute__((unused))) {
        WorldModel *s = (WorldModel *) user; 
        fprintf(stderr, "Slam Laser\n");
        //check and add the closest front and rear lasers and camera to the queue
        s->slam_laser_obs.push_back(slam_laser_pose_t_copy(msg));

        laser_pose_t *front_laser_pose = find_closest_laser_pose_from_utime(s->f_laser_cbuf, msg->utime);
        s->f_laser_obs.push_back(front_laser_pose);
        laser_pose_t *rear_laser_pose = find_closest_laser_pose_from_utime(s->r_laser_cbuf, msg->utime);
        s->r_laser_obs.push_back(rear_laser_pose);
        camera_pose_t *camera_pose = find_closest_camera_pose_from_utime(s->camera_cbuf, msg->utime);
        s->camera_obs.push_back(camera_pose);
    }
};


//pthread
static void *track_work_thread(void *user)
{
    //    state_t *s = (state_t*) user;

    printf("obstacles: track_work_thread()\n");

    while(1){
	fprintf(stderr, " T - called ()\n");
	
	usleep(50000);
    }
}

//doesnt do anything right now - timeout function
gboolean heartbeat_cb (gpointer data)
{
    //state_t *s = (state_t *)data;
  
    //do the periodic stuff 
    //if(s->verbose){
    //	fprintf(stderr, "Callback - Timeout\n");
    //}
    //return true to keep running
    return TRUE;
}

void subscribe_to_channels(WorldModel *s)
{
    //bot_core_pose_t_subscribe(s->lcm, "POSE", on_pose ,s);
    bot_core_planar_lidar_t_subscribe(s->lcm, "SKIRT_FRONT", s->on_f_laser ,s);
    bot_core_planar_lidar_t_subscribe(s->lcm, "SKIRT_REAR", s->on_r_laser ,s);
    bot_core_image_t_subscribe(s->lcm, "FRNT_IMAGE_BUMBLEBEE", s->on_camera ,s);
    bot_core_image_t_subscribe(s->lcm, "DRAGONFLY_IMAGE", s->on_camera ,s);
    slam_laser_pose_t_subscribe(s->lcm, "SLAM_POSE_LASER_POINTS", s->on_slam_laser, s);
}  


static void usage(char * funcName)
{
    printf("Usage: %s [options]\n"
	   "options are:\n"
	   "--help,        -h    display this message \n"
	   "--publish_all, -a    publish robot laser messages for all channels \n"
	   "--verbose,     -v    be verbose", funcName);
    exit(1);

}

int 
main(int argc, char **argv)
{

    g_thread_init(NULL);
    setlinebuf (stdout);
    WorldModel *state  = new WorldModel();

    const char *optstring = "hav";
    struct option long_opts[] = { { "help", no_argument, 0, 'h' },
				  { "publish_all", no_argument, 0, 'a' }, 
				  { "verbose", no_argument, 0, 'v' }, 
				  { 0, 0, 0, 0 } };

    int c;
    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
	switch (c) {
	case 'h':
	    usage(argv[0]);
	    break;
	case 'a':
	    {
		fprintf(stderr,"Publishing all laser channels\n");
		break;
	    }
	case 'v':
	    {
		fprintf(stderr,"Verbose\n");
		state->verbose = 1;
		break;
	    }
	default:
	    {
		usage(argv[0]);
		break;
	    }
	}
    }

    //this does not attach the lcm to glib mainloop - so we need to do this mannully

    //pthread_create(&state->work_thread, NULL, track_work_thread, state);

    subscribe_to_channels(state);
  
    GMainLoop *mainloop = g_main_loop_new( NULL, FALSE );  
  
    if (!mainloop) {
	printf("Couldn't create main loop\n");
	return -1;
    }

    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);

    //read_parameters_from_conf(state);

    /* heart beat*/
    g_timeout_add_seconds (1, heartbeat_cb, state);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (mainloop);
    
    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(mainloop);
  
    bot_glib_mainloop_detach_lcm(state->lcm);
}


