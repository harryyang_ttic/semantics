//Subscribe to log_annotation_t and planar_lidar_t messages. Once receive an annotation, publish laser_annotation_t message
//Requires: er-simulate-360-laser
//Output: publish laser_annotation_t message


#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <bot_core/bot_core.h>
#include <bot_param/param_client.h>
#include <bot_frames/bot_frames.h>
#include <lcm/lcm.h>

#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>

#include <bot_param/param_client.h>//
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>
#include <lcmtypes/slam_particle_request_t.h>
#include <lcmtypes/slam_graph_region_particle_list_t.h>
#include <lcmtypes/slam_laser_pose_list_t.h>
#include <lcmtypes/slam_pixel_map_request_t.h>
#include <occ_map/PixelMap.hpp>
#include <map>

using namespace std;
using namespace occ_map;

#define PIXMAP_RESOLUTION 0.1
#define SENSOR_DECIMATION_DISTANCE 0.1
#define LIDAR_LIST_SIZE 4000

typedef struct
{
    lcm_t      *lcm;
    BotFrames *frames;
    GMainLoop *mainloop;
    slam_graph_region_particle_list_t *last_result;
    slam_laser_pose_list_t *last_laser_result;
    map<int, slam_laser_pose_t *> laser_map;
} state_t;


static void on_semantic_results (const lcm_recv_buf_t *rbuf, const char *channel,
                           const slam_particle_request_t *msg, void *user)
{
    state_t *s = (state_t *) user;

    fprintf(stderr, "SLAM requested");
    if(s->last_result){
        slam_graph_region_particle_list_t_publish(s->lcm, "REGION_PARTICLE_ISAM_RESULT", s->last_result);
        fprintf(stderr, "SLAM published\n");
    }
    if(s->last_laser_result){
        slam_laser_pose_list_t_publish(s->lcm, "ISAM_LASER_SCANS", s->last_laser_result);
        fprintf(stderr, "Laser scans published\n");
    }
}

static void on_pixelmap_request (const lcm_recv_buf_t *rbuf, const char *channel,
                           const slam_pixel_map_request_t *msg, void *user)
{
    state_t *s = (state_t *) user;

    //find the right particle 
    if(!s->last_result || !s->last_laser_result){
        fprintf(stderr, "Error - no slam result or lasers\n");
        return;
    }
    
    int requested_id = msg->particle_id;
    
    slam_graph_region_particle_t *p = NULL;
    
    for(int i=0; i < s->last_result->no_particles; i++){
        if(s->last_result->particle_list[i].id == requested_id){
            p = &s->last_result->particle_list[i];
        }
    }
    
    if(p==NULL){
        fprintf(stderr, "Requested particle not found\n");
        return;
    }
        
    double minx =0, miny =0, maxx =0, maxy =0;

    for(int i=0; i < p->no_regions; i++){
        slam_graph_region_t *region = &p->region_list[i];

        for(int j=0; j< region->count; j++){
            slam_graph_node_t node = region->nodes[j];  
            double x = node.xy[0];
            double y = node.xy[1];
            
            if(x < minx)
                minx = x;
            if(x > maxx)
                maxx = x;
            if(y < miny)
                miny = y;
            if(y > maxy)
                maxy = y;
        }
    }

    double xy0[] = { minx - 15, miny - 15};
    double xy1[] = { maxx + 15, maxy + 15};

    FloatPixelMap *pixmap = new FloatPixelMap (xy0, xy1, PIXMAP_RESOLUTION, 0);
    float ray_trace_clamp[2] = {-11.0, 6};
    
    map<int, slam_laser_pose_t *>::iterator it;
    for(int i=0; i < p->no_regions; i++){
        slam_graph_region_t *region = &p->region_list[i];

        for(int j=0; j< region->count; j++){
            slam_graph_node_t node = region->nodes[j];        
                        
            it = s->laser_map.find(node.node_id);
            if(it == s->laser_map.end())
                continue;

            slam_laser_pose_t *laser_pose = it->second;

            BotTrans bodyToLocal;
            bodyToLocal.trans_vec[0] = node.xy[0];
            bodyToLocal.trans_vec[1] = node.xy[1];
            bodyToLocal.trans_vec[2] = 0;
            double rpy[3] = { 0, 0, node.heading };
            bot_roll_pitch_yaw_to_quat(rpy, bodyToLocal.rot_quat);
            double pBody[3] = { 0, 0, 0 };
            double pLocal[3];

            double start[2] = {node.xy[0], node.xy[1]};

            for(int k=0; k < laser_pose->pl.no; k++){
                pBody[0] = laser_pose->pl.points[k].pos[0];
                pBody[1] = laser_pose->pl.points[k].pos[1];
                pBody[2] = 0;
                bot_trans_apply_vec(&bodyToLocal, pBody, pLocal);
                            
                double pXY[2] = {pLocal[0], pLocal[1]};
                if (pixmap->isInMap (pXY)){
                    pixmap->rayTrace(start, pXY, -1, 2, ray_trace_clamp);
                }
            }
        }
    }

    const occ_map_pixel_map_t *pixmap_msg = pixmap->get_pixel_map_t (bot_timestamp_now());
    occ_map_pixel_map_t_publish (s->lcm, "PIXMAP_MAX_PARTICLE", pixmap_msg);
    fprintf(stderr, "Published SLAM Map : %d\n", requested_id);
    delete pixmap;
}


void subscribe_to_channels(state_t *s)
{
    slam_particle_request_t_subscribe(s->lcm, "SEMANTIC_PARTICLE_RESULT_REQUEST", on_semantic_results, s);
    slam_pixel_map_request_t_subscribe(s->lcm, "PIXEL_MAP_REQUEST", on_pixelmap_request, s);
}  


static void usage(const char* progname)
{
    fprintf (stderr, "Usage: %s [options]\n"
             "\n"
             "Options:\n"
	     "  -l        Log filename (to load)\n"
	     "  -h        help\n",
             g_path_get_basename(progname));
    exit(1);
}

int 
main(int argc, char **argv)
{

    const char *optstring = "hl:";

    int c;
    int nodding = 0;
    char * log_path = NULL;
    char * channel = strdup("REGION_PARTICLE_ISAM_RESULT");

    char * laser_channel = strdup("ISAM_LASER_SCANS");

    state_t *state = new state_t();//(state_t*) calloc(1, sizeof(state_t));

    while ((c = getopt (argc, argv, optstring)) >= 0) {
        switch (c) {
        case 'h': //help
            usage(argv[0]);
            break;
	case 'l': //Log file
	    log_path = strdup(optarg);
	    break;
        default:
            usage(argv[0]);
            break;
        }
    }

    state->lcm =  bot_lcm_get_global(NULL);
    state->last_result = NULL;
    state->last_laser_result = NULL;
    BotParam *param = bot_param_new_from_server(state->lcm, 1);
    state->frames = bot_frames_get_global (state->lcm, param);
    
    if(log_path==NULL){
        usage(argv[0]);
        return -1;
    }
    
    if(log_path != NULL){
        fprintf(stderr, "System is loading the semantic map results\n");
        lcm_eventlog_t *read_log = lcm_eventlog_create(log_path, "r"); // problem here?
        fprintf(stderr, "Log open success.\n");

        //Adding to annotation circular buffer
        lcm_eventlog_event_t *event = (lcm_eventlog_event_t *)calloc(1, sizeof(lcm_eventlog_event_t));
        int count = 0;
        for(lcm_eventlog_event_t *event = lcm_eventlog_read_next_event(read_log); event != NULL; event = lcm_eventlog_read_next_event(read_log)){
            int decode_status = 0; 
            if(!strcmp(channel, event->channel)){
                    
                slam_graph_region_particle_list_t *msg = (slam_graph_region_particle_list_t *)calloc(1, sizeof(slam_graph_region_particle_list_t));
                decode_status = slam_graph_region_particle_list_t_decode(event->data, 0, event->datalen, msg);
                if(decode_status < 0){
                    fprintf(stderr, "Error decoding message.\n");
                }
                count++;
                if(count %10==0){
                    fprintf(stderr, ".");
                }
                if(state->last_result != NULL){
                    slam_graph_region_particle_list_t_destroy(state->last_result);
                }
                state->last_result = slam_graph_region_particle_list_t_copy(msg);
                slam_graph_region_particle_list_t_destroy(msg);
            }
            if(!strcmp(laser_channel, event->channel)){                    
                slam_laser_pose_list_t *msg = (slam_laser_pose_list_t *)calloc(1, sizeof(slam_laser_pose_list_t));
                decode_status = slam_laser_pose_list_t_decode(event->data, 0, event->datalen, msg);

                if(decode_status < 0){
                    fprintf(stderr, "Error decoding message.\n");
                }
                count++;
                if(count %10==0){
                    fprintf(stderr, "+");
                }
                if(state->last_laser_result != NULL){
                    slam_laser_pose_list_t_destroy(state->last_laser_result);
                }
                state->last_laser_result = slam_laser_pose_list_t_copy(msg);
                slam_laser_pose_list_t_destroy(msg);
            }
            lcm_eventlog_free_event(event);
        }
	fprintf(stderr, "\nMesages in log : %d\n", count);
    }
    else{
        return -1;
    }
    if(state->last_result == NULL){
        fprintf(stderr, "No results found\n");
        usage(argv[0]);
        return -1;
    }

    if(state->last_laser_result){
        fprintf(stderr, "Laser scans found : %d\n", state->last_laser_result->no_poses);
        //fprintf(stderr, "Inseting scan : 
        for(int i=0; i < state->last_laser_result->no_poses; i++){
            slam_laser_pose_t *n_lp = &state->last_laser_result->scans[i]; 
            state->laser_map.insert(make_pair(n_lp->id, n_lp));
        }
    }
    
    state->mainloop = g_main_loop_new( NULL, FALSE );

    subscribe_to_channels(state);
         
    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);

    //read_parameters_from_conf(state);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    //    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);

    bot_glib_mainloop_detach_lcm(state->lcm);
}


