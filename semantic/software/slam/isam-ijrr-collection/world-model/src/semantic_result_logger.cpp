//Subscribe to log_annotation_t and planar_lidar_t messages. Once receive an annotation, publish laser_annotation_t message
//Requires: er-simulate-360-laser
//Output: publish laser_annotation_t message


#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <bot_core/bot_core.h>
#include <bot_param/param_client.h>
#include <bot_frames/bot_frames.h>
#include <lcm/lcm.h>

#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>

#include <bot_param/param_client.h>//
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>
#include <lcmtypes/slam_graph_region_particle_list_t.h>
#include <lcmtypes/slam_pixel_map_request_t.h>
#include <lcmtypes/slam_status_t.h>
#include <lcmtypes/slam_laser_pose_t.h>
#include <lcmtypes/slam_laser_pose_list_t.h>
#include <lcmtypes/slam_complex_language_collection_list_t.h>
#include <map>
#include <occ_map/PixelMap.hpp>

using namespace std;
using namespace occ_map;

#define PIXMAP_RESOLUTION 0.1
#define SENSOR_DECIMATION_DISTANCE 0.1
#define LIDAR_LIST_SIZE 4000

typedef struct
{
    lcm_t      *lcm;
    BotFrames *frames;
    GMainLoop *mainloop;
    int save_latest;
    bool publish_map; 
    int requested_id; 
    //GHashTable *node_scans;
    map<int, slam_laser_pose_t *> node_scans;
    slam_graph_region_particle_list_t *last_graph;
    slam_complex_language_collection_list_t *lang;
    lcm_eventlog_event_t *last_log_event;
    lcm_eventlog_event_t *last_lang_log_event;
    lcm_eventlog_t *write_log;
    int done_writing;
    int done_writing_scans;
    int requested_scans;
} state_t;

static void publish_latest_map(state_t *s){    
    slam_graph_region_particle_t *p = NULL;
    
    for(int i=0; i < s->last_graph->no_particles; i++){
        if(s->last_graph->particle_list[i].id == s->requested_id){
            p = &s->last_graph->particle_list[i];
        }
    }
    
    if(p==NULL){
        fprintf(stderr, "Requested particle not found\n");
        return;
    }
        
    double minx =0, miny =0, maxx =0, maxy =0;

    for(int i=0; i < p->no_regions; i++){
        slam_graph_region_t *region = &p->region_list[i];

        for(int j=0; j< region->count; j++){
            slam_graph_node_t node = region->nodes[j];  
            double x = node.xy[0];
            double y = node.xy[1];
            
            if(x < minx)
                minx = x;
            if(x > maxx)
                maxx = x;
            if(y < miny)
                miny = y;
            if(y > maxy)
                maxy = y;
        }
    }

    double xy0[] = { minx - 15, miny - 15};
    double xy1[] = { maxx + 15, maxy + 15};

    FloatPixelMap *pixmap = new FloatPixelMap (xy0, xy1, PIXMAP_RESOLUTION, 0);
    float ray_trace_clamp[2] = {-11.0, 6};
    
    map<int, slam_laser_pose_t *>::iterator it;
    for(int i=0; i < p->no_regions; i++){
        slam_graph_region_t *region = &p->region_list[i];

        for(int j=0; j< region->count; j++){
            slam_graph_node_t node = region->nodes[j];        
                        
            it = s->node_scans.find(node.node_id);
            if(it == s->node_scans.end())
                continue;

            slam_laser_pose_t *laser_pose = it->second;

            BotTrans bodyToLocal;
            bodyToLocal.trans_vec[0] = node.xy[0];
            bodyToLocal.trans_vec[1] = node.xy[1];
            bodyToLocal.trans_vec[2] = 0;
            double rpy[3] = { 0, 0, node.heading };
            bot_roll_pitch_yaw_to_quat(rpy, bodyToLocal.rot_quat);
            double pBody[3] = { 0, 0, 0 };
            double pLocal[3];

            double start[2] = {node.xy[0], node.xy[1]};

            for(int k=0; k < laser_pose->pl.no; k++){
                pBody[0] = laser_pose->pl.points[k].pos[0];
                pBody[1] = laser_pose->pl.points[k].pos[1];
                pBody[2] = 0;
                bot_trans_apply_vec(&bodyToLocal, pBody, pLocal);
                            
                double pXY[2] = {pLocal[0], pLocal[1]};
                if (pixmap->isInMap (pXY)){
                    pixmap->rayTrace(start, pXY, -1, 2, ray_trace_clamp);
                }
            }
        }
    }

    const occ_map_pixel_map_t *pixmap_msg = pixmap->get_pixel_map_t (bot_timestamp_now());
    occ_map_pixel_map_t_publish (s->lcm, "PIXMAP_MAX_PARTICLE", pixmap_msg);
    fprintf(stderr, "Published SLAM Map : %d\n", s->requested_id);
    delete pixmap;
}

static void on_pixelmap_request (const lcm_recv_buf_t *rbuf, const char *channel,
                           const slam_pixel_map_request_t *msg, void *user)
{
    state_t *s = (state_t *) user;
    
    //find the right particle 
    if(!s->last_graph){
        fprintf(stderr, "Error - no slam result or lasers\n");
        return;
    }
    
    s->requested_id = msg->particle_id;
    publish_latest_map(s);
}

static void on_semantic_logger (const lcm_recv_buf_t *rbuf, const char *channel_orig,
                                const slam_graph_region_particle_list_t *msg, void *user)
{
    state_t *s = (state_t *) user;

    if(!s->write_log)
        return;
    fprintf(stderr, ".");

    //check if we have all the scanpoints 
    const char *channel = "REGION_PARTICLE_ISAM_RESULT";
    int channellen = strlen(channel);
    int64_t mem_sz = sizeof(lcm_eventlog_event_t) + channellen + 1 + rbuf->data_size;

    lcm_eventlog_event_t *le = (lcm_eventlog_event_t*) malloc(mem_sz);
    le = (lcm_eventlog_event_t*) malloc(mem_sz);
    memset(le, 0, mem_sz);

    le->timestamp = rbuf->recv_utime;
    le->channellen = channellen;
    le->datalen = rbuf->data_size;

    le->channel = ((char*)le) + sizeof(lcm_eventlog_event_t);
    strcpy(le->channel, channel);
    le->data = le->channel + channellen+1;

    assert((char*)le->data + rbuf->data_size == (char*)le + mem_sz);
    memcpy(le->data, rbuf->data, rbuf->data_size);
    
    s->done_writing = 1;

    if(0 != lcm_eventlog_write_event(s->write_log, le)){
	fprintf(stderr, "Error\n");
    }
    else{
	fprintf(stderr, "Saved to file\n");
    }    
    free(le);
}

static void on_logger_laser_scan_list (const lcm_recv_buf_t *rbuf, const char *channel_orig,
                           const slam_laser_pose_list_t *msg, void *user)
{
    state_t *s = (state_t *)user;
    fprintf(stderr, "Writing Laser scans\n");

    //check if we have all the scanpoints 
    const char *channel = "ISAM_LASER_SCANS";
    int channellen = strlen(channel);
    int64_t mem_sz = sizeof(lcm_eventlog_event_t) + channellen + 1 + rbuf->data_size;

    lcm_eventlog_event_t *le = (lcm_eventlog_event_t*) malloc(mem_sz);
    le = (lcm_eventlog_event_t*) malloc(mem_sz);
    memset(le, 0, mem_sz);

    le->timestamp = rbuf->recv_utime;
    le->channellen = channellen;
    le->datalen = rbuf->data_size;

    le->channel = ((char*)le) + sizeof(lcm_eventlog_event_t);
    strcpy(le->channel, channel);
    le->data = le->channel + channellen+1;

    assert((char*)le->data + rbuf->data_size == (char*)le + mem_sz);
    memcpy(le->data, rbuf->data, rbuf->data_size);
    
    s->done_writing_scans = 1;

    if(0 != lcm_eventlog_write_event(s->write_log, le)){
	fprintf(stderr, "Error\n");
    }
    else{
	fprintf(stderr, "Saved Laser scans to file\n");
    }    
    free(le);
}

void save_latest_event(state_t *state){
    if(state->save_latest){
        if(state->last_log_event){
            if(0 != lcm_eventlog_write_event(state->write_log, state->last_log_event)){
                fprintf(stderr, "Error\n");
            }
            else{
                fprintf(stderr, "Saved to file\n");
            }  
        }
        if(state->last_lang_log_event){
            if(0 != lcm_eventlog_write_event(state->write_log, state->last_lang_log_event)){
                fprintf(stderr, "Error\n");
            }
            else{
                fprintf(stderr, "Saved to file\n");
            }  
        }
    }
}

void publish_laser_scans(state_t *s){
    slam_laser_pose_list_t msg;
    
    map<int, slam_laser_pose_t *>::iterator it;
    msg.no_poses =  s->node_scans.size();//g_hash_table_size(s->node_scans);
    msg.scans = (slam_laser_pose_t *) calloc(msg.no_poses, sizeof(slam_laser_pose_t));
    int i = 0;
    fprintf(stderr, "Laser Poses : %d\n", msg.no_poses);
    for(it = s->node_scans.begin(); it != s->node_scans.end(); it++){
        slam_laser_pose_t *laser_pose = it->second;//(slam_laser_pose_t *) g_hash_table_lookup(s->node_scans, key);
        if(laser_pose){
            memcpy(&msg.scans[i], laser_pose, sizeof(slam_laser_pose_t));
            i++;
            fprintf(stderr, "Laser scan : %d\n", i);
        }   
        else{
            fprintf(stderr, "Laser scan NULL\n");
        }
    }
    fprintf(stderr, "Publishing Laser scans : %d\n", msg.no_poses);
    slam_laser_pose_list_t_publish(s->lcm, "SEMANTIC_LOGGER_SCANS", &msg);
    free(msg.scans);    
}


void save_latest(state_t *state){
    if(state->save_latest && state->last_graph){
        slam_graph_region_particle_list_t_subscribe(state->lcm, "SEMANTIC_LOGGER", on_semantic_logger, state);
        slam_graph_region_particle_list_t_publish(state->lcm, "SEMANTIC_LOGGER", state->last_graph);

        slam_laser_pose_list_t_subscribe(state->lcm, "SEMANTIC_LOGGER_SCANS", on_logger_laser_scan_list, state);
        publish_laser_scans(state);

        while(!(state->done_writing && state->done_writing_scans)){
            lcm_handle(state->lcm);
        }
        fprintf(stderr, "Done writing to file\n");        
    }
}

void request_laser_scans(state_t *self){
    self->requested_scans = 1;
    slam_pixel_map_request_t msg; 
    msg.utime = bot_timestamp_now();
    msg.particle_id = 0;
    msg.request = SLAM_PIXEL_MAP_REQUEST_T_REQ_SCAN_POINTS;
    slam_pixel_map_request_t_publish(self->lcm, "SLAM_SCAN_REQUEST", &msg);
}

void check_missing_scans(state_t *s){
    
    int missing = 0; 

    if(s->last_graph){
        map<int, slam_laser_pose_t *>::iterator it;
        //just check for the first particle 
        if(s->last_graph->no_particles > 0){
            slam_graph_region_particle_t *p = &s->last_graph->particle_list[0];
            
            for(int i=0; i < p->no_regions; i++){
                slam_graph_region_t *region = &p->region_list[i];
                for(int j=0; j< region->count; j++){
                    slam_graph_node_t node = region->nodes[j];        
                    it = s->node_scans.find(node.node_id);
                    if(it == s->node_scans.end()){
                        fprintf(stderr, "Laser pose not found -requesting Poses\n");
                        if(!s->requested_scans){
                            request_laser_scans(s);                            
                            return;
                        }
                    }
                }
            }
        }
    }
}


static void on_topo_graph (const lcm_recv_buf_t *rbuf, const char *channel,
                           const slam_graph_region_particle_list_t *msg, void *user)
{
    state_t *s = (state_t *) user;

    fprintf(stderr, ".");

    //check if we have all the scanpoints 
    
    if(s->last_graph){
        slam_graph_region_particle_list_t_destroy(s->last_graph);
    }
    s->last_graph = slam_graph_region_particle_list_t_copy(msg);

    check_missing_scans(s);
    
    if(s->publish_map){
        publish_latest_map(s);
    }
    
    if(s->save_latest){
        return;
    }

    if(s->write_log){
        //writing to file
        int channellen = strlen(channel);
        int64_t mem_sz = sizeof(lcm_eventlog_event_t) + channellen + 1 + rbuf->data_size;

        if(s->last_log_event){
            free(s->last_log_event);
        }

        s->last_log_event = (lcm_eventlog_event_t*) malloc(mem_sz);
        memset(s->last_log_event, 0, mem_sz);

        s->last_log_event->timestamp = rbuf->recv_utime;
        s->last_log_event->channellen = channellen;
        s->last_log_event->datalen = rbuf->data_size;

        s->last_log_event->channel = ((char*)s->last_log_event) + sizeof(lcm_eventlog_event_t);
        strcpy(s->last_log_event->channel, channel);
        s->last_log_event->data = s->last_log_event->channel + channellen+1;

        assert((char*)s->last_log_event->data + rbuf->data_size == (char*)s->last_log_event + mem_sz);
        memcpy(s->last_log_event->data, rbuf->data, rbuf->data_size);
        
        if(0 != lcm_eventlog_write_event(s->write_log, s->last_log_event)){
            fprintf(stderr, "Error\n");
        }
        else{
            fprintf(stderr, "Saved to file\n");
        }    
    }
    
}

static void on_language_update (const lcm_recv_buf_t *rbuf, const char *channel,
                                const slam_complex_language_collection_list_t *msg, void *user)
{
    state_t *s = (state_t *) user;

    //check if we have all the scanpoints 
    
    if(s->lang){
        slam_complex_language_collection_list_t_destroy(s->lang);
    }
    s->lang = slam_complex_language_collection_list_t_copy(msg);

    if(s->save_latest){
        return;
    }

    if(s->write_log){
        //writing to file
        int channellen = strlen(channel);
        int64_t mem_sz = sizeof(lcm_eventlog_event_t) + channellen + 1 + rbuf->data_size;

        if(s->last_lang_log_event){
            free(s->last_lang_log_event);
        }

        s->last_lang_log_event = (lcm_eventlog_event_t*) malloc(mem_sz);
        memset(s->last_lang_log_event, 0, mem_sz);

        s->last_lang_log_event->timestamp = rbuf->recv_utime;
        s->last_lang_log_event->channellen = channellen;
        s->last_lang_log_event->datalen = rbuf->data_size;

        s->last_lang_log_event->channel = ((char*)s->last_lang_log_event) + sizeof(lcm_eventlog_event_t);
        strcpy(s->last_lang_log_event->channel, channel);
        s->last_lang_log_event->data = s->last_lang_log_event->channel + channellen+1;

        assert((char*)s->last_lang_log_event->data + rbuf->data_size == (char*)s->last_lang_log_event + mem_sz);
        memcpy(s->last_lang_log_event->data, rbuf->data, rbuf->data_size);
        
        if(0 != lcm_eventlog_write_event(s->write_log, s->last_lang_log_event)){
            fprintf(stderr, "Error\n");
        }
        else{
            fprintf(stderr, "Saved to file\n");
        }    
    }
    
}

//we should also save the scan points 
//and request scanpoints if we dont have them 

static void on_laser_pose (const lcm_recv_buf_t *rbuf, const char *channel,
                           const slam_laser_pose_t *msg, void *user)
{
    state_t *self = (state_t *)user;
    fprintf(stderr, "s");
    slam_laser_pose_t *laser_pose = slam_laser_pose_t_copy(msg);
    self->node_scans.insert(make_pair(laser_pose->id, laser_pose));
}


static void on_laser_scan_list (const lcm_recv_buf_t *rbuf, const char *channel,
                           const slam_laser_pose_list_t *msg, void *user)
{
    state_t *self = (state_t *)user;
    fprintf(stderr, "S");
    map<int, slam_laser_pose_t *>::iterator it;
    for(int i=0; i < msg->no_poses; i++){
        slam_laser_pose_t *n_lp = &msg->scans[i]; 
        it = self->node_scans.find(n_lp->id);
       
        if(it == self->node_scans.end()){
            slam_laser_pose_t *laser_pose = slam_laser_pose_t_copy(n_lp);
            self->node_scans.insert(make_pair(laser_pose->id, laser_pose));
        }
    }
    self->requested_scans = 0;
}

static void on_slam_status (const lcm_recv_buf_t *rbuf, const char *channel,
                            const slam_status_t *msg, void *user)
{
    state_t *s = (state_t *) user; 
    fprintf(stderr, "Resetting\n");

    if(msg->status == SLAM_STATUS_T_RESET){
        map<int, slam_laser_pose_t *>::iterator it; 
        for(it = s->node_scans.begin(); it != s->node_scans.end(); it++){
            slam_laser_pose_t_destroy(it->second);
        }
        s->node_scans.clear();
        if(s->last_graph){
            slam_graph_region_particle_list_t_destroy(s->last_graph);
            s->last_graph = NULL;
        }
    }
}

void subscribe_to_channels(state_t *s)
{
    slam_graph_region_particle_list_t_subscribe(s->lcm, "REGION_PARTICLE_ISAM_RESULT", on_topo_graph, s);
    slam_laser_pose_t_subscribe(s->lcm, "SLAM_POSE_LASER_POINTS", on_laser_pose, s);
    slam_laser_pose_list_t_subscribe(s->lcm, "ISAM_LASER_SCANS", on_laser_scan_list, s);
    slam_complex_language_collection_list_t_subscribe(s->lcm, "COMPLEX_LANGUAGE_PATHS", on_language_update, s);
    slam_status_t_subscribe(s->lcm, "SLAM_STATUS", on_slam_status, s);
    if(s->publish_map){
        slam_pixel_map_request_t_subscribe(s->lcm, "PIXEL_MAP_REQUEST", on_pixelmap_request, s);
    }
}  


static void usage(const char* progname)
{
    fprintf (stderr, "Usage: %s [options]\n"
             "\n"
             "Options:\n"
	     "  -l        Log filename to write\n"
             "  -n        Save Latest Message\n" 
             "  -p        Publish Map\n" 
	     "  -h        help\n",
             g_path_get_basename(progname));
    exit(1);
}

int 
main(int argc, char **argv)
{

    const char *optstring = "hpl:n";

    int c;
    int nodding = 0;
    char * log_path = NULL;
    char * channel = strdup("REGION_PARTICLE_ISAM_RESULT");

    state_t *state = new state_t();//(state_t*) calloc(1, sizeof(state_t));
    state->save_latest = 0;
    while ((c = getopt (argc, argv, optstring)) >= 0) {
        switch (c) {
        case 'h': //help
            usage(argv[0]);
            break;
            //we should create this name the same way the logger does - unless given a name - so that we dont overwrite the log
	case 'l': //Log file
	    log_path = strdup(optarg);
	    break;
        case 'p': //Log file
            state->publish_map = true;
	    break;
        case 'n': //Log file
            state->save_latest = 1;
	    break;
        default:
            usage(argv[0]);
            break;
        }
    }
    //state->last_graph = NULL;
    state->last_log_event = NULL;
    state->last_lang_log_event = NULL;
    state->lcm =  bot_lcm_get_global(NULL);

    BotParam *param = bot_param_new_from_server(state->lcm, 1);
    state->frames = bot_frames_get_global (state->lcm, param);
    
    if(log_path==NULL){
        //usage(argv[0]);
        //return -1;
        state->save_latest = false;
    }
    
    if(log_path){
        state->write_log = lcm_eventlog_create(log_path, "w");
    }
    else{
        state->write_log = NULL;
    }
    
    state->requested_id = -1;

    state->mainloop = g_main_loop_new( NULL, FALSE );

    subscribe_to_channels(state);
         
    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);

    //read_parameters_from_conf(state);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    g_main_loop_run(state->mainloop);

    fprintf(stderr, "Exiting\n");
    save_latest(state);
    
    bot_glib_mainloop_detach_lcm(state->lcm);
}


