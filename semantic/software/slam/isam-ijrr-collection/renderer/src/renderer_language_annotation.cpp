#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <glib.h>
#include <string.h>
#include <assert.h>
#include <gdk/gdkkeysyms.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

#include <bot_vis/bot_vis.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <math.h>

#include <bot_core/bot_core.h>
#include <bot_vis/viewer.h>
#include <bot_vis/gtk_util.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>
#include <bot_core/math_util.h>
#include <geom_utils/geometry.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/slam_lcmtypes.h>

#include <boost/config.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <map>
using namespace std;
using namespace boost;

struct region_annotation_t {
    int region_id;
    set<int> node_ids;
    string annotation;
};


#define DTOR M_PI/180
#define RTOD 180/M_PI

#define PARTICLE_HISTORY_SIZE 10

#define PARAM_SELECT_LANGUAGE_FILE "Select Language Annotation File"
#define PARAM_SAVE_LANGUAGE "Save Annotation"

#define MAX_REFERSH_RATE_USEC 30000 // about 1/30 of a second

#define PARAM_REQUEST_RESULT "Request Result"
#define RENDERER_NAME "Language Annotation"
#define DATA_CIRC_SIZE 10
#define POSE_DATA_CIRC_SIZE 200 
#define PARAM_REQUEST_MAP_SAVE "Save Graph to File"
#define PARAM_REQUEST_MAP "Request map"

#define PARAM_SPATIAL_RELATION "SR:"
#define PARAM_SELECT_AGENT "Select Robot Location"
#define PARAM_SELECT_LANDMARK_REGION "Select Landmark Region"
#define PARAM_SELECT_FIGURE_REGION "Select Figure Region"
#define PARAM_ORDER_PARTICLES "Particle Order"
#define VALID_MAP_IND "Draw Map"
#define PARAM_DISP_PROB "Display prob."
#define PARAM_DRAW_LEGAND "Draw legend"
#define PARAM_DRAW_HEADING "Draw node with heading"
#define PARAM_DRAW_STATS "Draw stats"
#define PARAM_DRAW_ALL_GRAPHS "Draw all graphs"
#define PARAM_DRAW_COV "Draw covariance"
#define PARAM_DRAW_LABEL_PIE_CHART "Draw label pie chart"
#define PARAM_DRAW_LABEL_NAME "Draw name"
#define PARAM_DRAW_ALL_MAX_LABELS "Draw All Max Labels"
#define PARAM_DRAW_MAX_LABEL "Draw Max Label"
#define PARAM_DRAW_VAID_GRAPH "Draw Valid Graph"
#define PARAM_DRAW_NODE_ID "Draw Node IDs"
#define PARAM_DRAW_REGION_ID "Draw Region IDs"
#define PARAM_DRAW_SEGMENT_ID "Draw Segment IDs"
#define PARAM_DRAW_NODES "Draw Nodes"
#define PARAM_DRAW_REGIONS "Draw Regions"
#define PARAM_DRAW_REGION_SEMANTICS "Draw Region Semantic Dist"
#define PARAM_DRAW_REGION_SEMANTICS_MAX "Draw Region Semantic Max"
#define PARAM_DRAW_REGION_LABEL "Draw Region Label Dist"
#define PARAM_DRAW_REGION_LABEL_MAX "Draw Region Label Max"
#define PARAM_DRAW_LASER_CLASSIFICATION "Draw Laser Class"
#define PARAM_DRAW_LASER_CLASSIFICATION_MAX "Draw Laser Max-like"
#define PARAM_DRAW_IMAGE_CLASSIFICATION "Draw Image Class"
#define PARAM_DRAW_IMAGE_CLASSIFICATION_MAX "Draw Image Max-like"
#define PARAM_DRAW_CLASSIFICATION_TEXT "Draw Text"
#define PARAM_DRAW_REGIONS "Draw Regions"
#define PARAM_DRAW_REGION_CONNECTIONS "Draw Region Connections"
#define PARAM_DRAW_INTRA_REGION_EDGES "Draw Intra-region Edges"
#define PARAM_DRAW_INTER_REGION_EDGES "Draw Inter-region Edges"
#define PARAM_REMAP_REGIONS "Remap Region Colors"
#define PARAM_DRAW_LOG "Draw LOG Scale"
#define PARAM_DRAW_ODOM "Highlight Odometry"
#define PARAM_DRAW_DEAD_EDGES "Draw Dead Edges"
#define PARAM_DRAW_MAX_GRAPH "Draw Max Graph"
#define PARAM_COLOR_REGIONS "Color regions"
#define PARAM_EDGE_THICKNESS "Edge Thickness"
#define PARAM_DRAW_HEIGHT "Draw node height"
#define PARAM_NODE_RADIUS "Node Radius"
#define PARAM_DRAW_REGION_MEAN "Draw Region Mean"
#define PARAM_POINT_SIZE "Point Size" 
#define PARAM_POINT_ALPHA "Point Alpha" 
#define PARAM_DRAW_EQUAL_DISTANCE "Equal graph spacing"
#define PARAM_DISTANCE_SCALE "Graph Dist Scale"
#define PARAM_ADD_CONSTRAINT_LABEL "Add Constraint"

#define PARAM_CLEAR_SELECTION "Clear selection"
#define PARAM_BEGIN_ANNOTATION "Begin Ground Truth Annotation"
#define PARAM_SAVE_SELECTION "Save Ground Truth Selection"
#define PARAM_BEGIN_RESULT_ANNOTATION "Begin Result Annotation"
#define PARAM_SAVE_RESULT_SELECTION "Save Result Selection"
#define PARAM_NODE_ANNOTATION "Annotation: "
#define PARAM_CLEAR_ALL "Clear All Annotations"
#define PARAM_BOUNDING_BOXES "Bounding boxes"

#define NO_COLORS 8
#define DRAW_EDGES_TO_SUPERNODE "draw edges to supernode (versus closest nodes)"

//Boost stuff
typedef struct _Vertex
{
    int id;
} Vertex;

typedef adjacency_list < listS, vecS, undirectedS, Vertex, property < edge_weight_t, double > > graph_t;
typedef graph_traits < graph_t >::vertex_descriptor vertex_descriptor;
typedef graph_traits < graph_t >::edge_descriptor edge_descriptor;
typedef property < edge_weight_t, double >Weight;
typedef property < vertex_name_t, double >Name;

typedef pair<int, int> nodePairKey;

enum node_selection_t {
    SELECT_NONE, 
    SELECT_AGENT, 
    SELECT_LANDMARK, 
    SELECT_FIGURE
};

typedef struct _params_t{
    int draw_label_name;
    int draw_odom;
    int draw_height;
    int draw_max_map;
    int draw_prob;
    int draw_map_points;
    int draw_all_graphs;
    int draw_region_ids;
    int draw_segment_ids;
    int draw_dead_edges;
    int edge_thickness;
    int draw_valid_maps;
    int draw_cov;
    int draw_pie_chart;
    int draw_max_label;
    int draw_nodes;
    int draw_regions;
    int bounding_boxes;
    int color_regions;
    int remap_regions;
    int draw_diff;
    int draw_inter_region_edges;
    int draw_intra_region_edges;
    int log_scale;
    int draw_node_ids;
    int draw_region_connections;
    int draw_side_by_side;
    int g_id_1;
    int g_id_2;
    int particle_ordering_mode;
    int draw_region_mean;
    int distance_scale;
    int equal_dist;
    int draw_heading;
    double node_radius;
    int draw_semantic_class;
    int draw_semantic_class_max;
    int draw_semantic_label;
    int draw_semantic_label_max;
    int draw_laser_class; 
    int draw_laser_class_max;
    int draw_image_class;
    int draw_image_class_max; 
    int draw_class_text; 
    int draw_legand;
    int draw_all_max_labels;
    int draw_stats;
} params_t;

typedef struct _annotated_node_list_t{
    int count;
    int *ids;
    char *label;
} annotated_node_list_t;


//typedef struct _RendererLanguageAnnotation RendererLanguageAnnotation;
struct RendererLanguageAnnotation {
    BotRenderer renderer;
    BotEventHandler ehandler;
    
    lcm_t *lcm;
    BotParam *param;

    node_selection_t node_selection_mode;

    int have_data;
    GHashTable *node_scans;

    BotPtrCircular   *data_circ;

    GList *annotated_nodes;

    BotViewer         *viewer;
    BotGtkParamWidget *pw;   

    params_t params;

    GMutex *mutex;
    
    double *xy_first;
    double *xy_second;
    int no_particles; 
    int active_particle;
    int node_1;
    int node_2;
    
    GHashTable *selected_nodes;
    
    int current_ind;
    int active; //1 = add new constraint, 0 = inactive

    //insert to a sorted GList 
    BotPtrCircular *particle_history;

    GList *particle_list;
    slam_graph_region_particle_t *diff_particle;
    int64_t diff_utime;
    int diff_iter;

    int64_t last_pose_utime;

    gchar *lang_label_filename;
    gchar *annotation_filename;

    int robot_id; 
    int landmark_region_id;
    int landmark_node_id;
    int figure_region_id;
    int figure_node_id;
    vector<int> path_to_figure;
    set<int> path_to_figure_map;

    gchar *lang_filename;
    //FILE *lang_label_fp;
    graph_t *graph;
    unordered_map<int, slam_graph_node_t *> node_map;
    unordered_map<int, vertex_descriptor> vertex_map;
    unordered_map<int, int> parent_map;
    vector<string> spatial_relations;
    //double prob_bound;
    double footprint[8];

    double fp_length;
    double fp_width;

    double front_middle[2];

    int *sr_id;
    char **sr_description; 
    lcm_eventlog_t *write_log;

    RendererLanguageAnnotation(){
        graph = NULL;
        robot_id = -1; 
        landmark_region_id = -1;
        figure_region_id = -1;
        landmark_node_id = -1;
        figure_node_id = -1;
        write_log = NULL;

        spatial_relations.push_back("down");
        spatial_relations.push_back("through");
        spatial_relations.push_back("in_front_of");
        spatial_relations.push_back("to_the_right_of");
        spatial_relations.push_back("to_the_left_of");
        spatial_relations.push_back("behind");
        spatial_relations.push_back("next_to");
        spatial_relations.push_back("accross");

        sr_id = (int *) calloc(spatial_relations.size(), sizeof(int));
        sr_description = (char **) calloc(spatial_relations.size(), sizeof(char *));
        for(int i=0; i < spatial_relations.size(); i++){
            sr_id[i] = i;
            sr_description[i] = strdup(spatial_relations[i].c_str());
        }
    }
};

/*static void on_semantic_logger (const lcm_recv_buf_t *rbuf, const char *channel_orig,
                                const slam_graph_region_particle_list_t *msg, void *user)
{
    state_t *s = (state_t *) user;

    fprintf(stderr, ".");

    //check if we have all the scanpoints 
    const char *channel = "REGION_PARTICLE_ISAM_RESULT";
    int channellen = strlen(channel);
    int64_t mem_sz = sizeof(lcm_eventlog_event_t) + channellen + 1 + rbuf->data_size;

    lcm_eventlog_event_t *le = (lcm_eventlog_event_t*) malloc(mem_sz);
    le = (lcm_eventlog_event_t*) malloc(mem_sz);
    memset(le, 0, mem_sz);

    le->timestamp = rbuf->recv_utime;
    le->channellen = channellen;
    le->datalen = rbuf->data_size;

    le->channel = ((char*)le) + sizeof(lcm_eventlog_event_t);
    strcpy(le->channel, channel);
    le->data = le->channel + channellen+1;

    assert((char*)le->data + rbuf->data_size == (char*)le + mem_sz);
    memcpy(le->data, rbuf->data, rbuf->data_size);
    
    s->done_writing = 1;

    if(0 != lcm_eventlog_write_event(s->write_log, le)){
	fprintf(stderr, "Error\n");
    }
    else{
	fprintf(stderr, "Saved to file\n");
    }    
    free(le);
}*/

static void create_new_log(RendererLanguageAnnotation *self){
    if(self->write_log){
        fprintf(stderr, "Closing old log file\n");
        lcm_eventlog_destroy(self->write_log);
    }
    self->write_log = lcm_eventlog_create(self->lang_filename, "a");
}

static void send_map_request(RendererLanguageAnnotation *self, int id){
    slam_pixel_map_request_t msg; 
    msg.utime = bot_timestamp_now();
    msg.particle_id = id;
    msg.request = SLAM_PIXEL_MAP_REQUEST_T_REQ_PIXEL_MAP;
    slam_pixel_map_request_t_publish(self->lcm, "PIXEL_MAP_REQUEST", &msg);
}

static void clear_paths_and_parents(RendererLanguageAnnotation *self){
    self->robot_id = -1;
    self->landmark_region_id = -1;
    self->figure_region_id = -1;
    self->figure_node_id = -1;
    self->landmark_node_id = -1;
    self->path_to_figure.clear();
    self->path_to_figure_map.clear();
    self->parent_map.clear();
}

void set_node_empty(slam_graph_node_t *nd){
    nd->id = -1;
    nd->node_id = -1;
    nd->utime = 0;
    nd->xy[0] = 0;
    nd->xy[1] = 0;
    nd->heading = 0;
    nd->pofz = 0;
    nd->is_supernode = 0;
    nd->parent_supernode = -1;
    nd->segment_id = -1;
    memset(nd->cov, 0, 9 * sizeof(double));
    nd->labeldist.num_labels = 0;
    nd->labeldist.observation_frequency = NULL;
    nd->appearance_dist.count = 0;
    nd->appearance_dist.classes = NULL;
    nd->no_points = 0;
    nd->x_coords = NULL;
    nd->y_coords = NULL;
}

void deep_copy_node(slam_graph_node_t *src, slam_graph_node_t *dst){
    //technically not supposed to use these functions 
    __slam_graph_node_t_clone_array(src, dst, 1);
    //maybe use copy and then do a memcpy - and then free the coppied data (which should leave the array memory still available)
}

static int get_path_to_figure(unordered_map<int, int> &parent_map, int figure_node_id, vector<int> &path_to_figure, set<int> &path_to_figure_map){
    //fprintf(stderr, "Searching for path\n");
    path_to_figure.clear();
    path_to_figure_map.clear();
    unordered_map<int, int>::iterator it;
    it = parent_map.find(figure_node_id);
    
    if(it == parent_map.end()){
        return 0;
    }

    path_to_figure.push_back(figure_node_id);
    path_to_figure_map.insert(figure_node_id);
    
    while(it != parent_map.end()){
        path_to_figure.push_back(it->second);
        path_to_figure_map.insert(it->second);
        if(it->first == it->second)
            break;
        it = parent_map.find(it->second);
    }
    
    /*for(int i=0; i < path_to_figure.size(); i++){
        fprintf(stderr, "Path : %d\n", path_to_figure[i]);
      }*/

    return 1;
} 

static inline int remap_ind(int ind, int middle){
    //split them in half - based on even or odd 
    int rem = ind % 2;
    
    int add = 0;
    
    if(middle %2==0)
        add = 1;

    //even 
    if(rem ==0){
        if(ind <= middle)
            return ind;
        else
            return ind - (middle+add);
    }
    else{
        if(ind <= middle)
            return ind+(middle+add);
        else
            return ind;
    }
}

static gint compare_g_prob (gconstpointer a, gconstpointer b)
{
    slam_graph_region_particle_t *p1 = (slam_graph_region_particle_t *)a;
    slam_graph_region_particle_t *p2 = (slam_graph_region_particle_t *)b;

    double prob1 = exp(p1->weight);
    double prob2 = exp(p2->weight);
    if(prob1 > prob2)
        return 0;
    return 1;
}

static void destroy_g_particle(gpointer data){
    if(data != NULL){
        slam_graph_region_particle_t *p = (slam_graph_region_particle_t *)data;
        slam_graph_region_particle_t_destroy(p);
    }
}

static void destroy_annotated_list(gpointer data){
    if(data != NULL){
        annotated_node_list_t *l = (annotated_node_list_t *)data;
        free(l->ids);
        free(l->label);
        free (l);
    }
}

static gint compare_g_graph_id (gconstpointer a, gconstpointer b){
    slam_graph_region_particle_t *p1 = (slam_graph_region_particle_t *)a;
    slam_graph_region_particle_t *p2 = (slam_graph_region_particle_t *)b;

    if(p1->id < p2->id)
        return 0;
    return 1;
}

static gint compare_g_region_id (gconstpointer a, gconstpointer b){
    slam_graph_region_t *p1 = (slam_graph_region_t *)a;
    slam_graph_region_t *p2 = (slam_graph_region_t *)b;

    if(p1->id < p2->id)
        return 0;
    return 1;
}

static void destroy_g_int(gpointer data){
    if(data != NULL){
        int *p = (int *)data;
        free(p);
    }
}

void get_shortest_path_tree(graph_t &g, unordered_map<int, vertex_descriptor> &vertex_map, int start_node_id, unordered_map<int, int> &parent_map){
    parent_map.clear();
    property_map<graph_t, edge_weight_t>::type weightmap = get(edge_weight, g);
    std::vector<vertex_descriptor> p(num_vertices(g));
    std::vector<double> d(num_vertices(g));
    
    unordered_map<int, vertex_descriptor>::iterator it;

    it = vertex_map.find(start_node_id);
    
    if(it == vertex_map.end()){
        fprintf(stderr, "Node not found\n");
    }
    
    vertex_descriptor  b_vertex = it->second; 
    
    dijkstra_shortest_paths(g, b_vertex,
                            predecessor_map(boost::make_iterator_property_map(p.begin(), 
                                                                              get(boost::vertex_index, g))).
                            distance_map(boost::make_iterator_property_map(d.begin(), get(boost::vertex_index, g))));
        
    int64_t e_utime = bot_timestamp_now();

    graph_traits < graph_t >::vertex_iterator vi, vend;
    
    for (boost::tie(vi, vend) = vertices(g); vi != vend; ++vi) {        
        parent_map.insert(make_pair(g[*vi].id, g[p[*vi]].id));
    }
}

static void update_the_path(RendererLanguageAnnotation *self){
    if(self->robot_id < 0)
        return;
    //rebuild the shortest path tree
    get_shortest_path_tree(*self->graph, self->vertex_map, self->robot_id, self->parent_map);
    if(self->figure_node_id < 0)
        return;
    int found = get_path_to_figure(self->parent_map, self->figure_node_id, self->path_to_figure, self->path_to_figure_map);
    if(found == 0){
        fprintf(stderr, "Error - Path to figure not found\n");
    }
}

static void build_node_map(RendererLanguageAnnotation *self, slam_graph_region_particle_t *p){
    self->node_map.clear();
    for(int i=0; i < p->no_regions; i++){
        slam_graph_region_t *region = &p->region_list[i];
        for(int j=0; j< region->count; j++){
            slam_graph_node_t *node = &region->nodes[j];  
            self->node_map.insert(make_pair(node->id, node));
        }
    }
    
}

static void build_boost_graph(RendererLanguageAnnotation *self, slam_graph_region_particle_t *p, graph_t &g, unordered_map<int, vertex_descriptor> &vertex_map){
    vertex_map.clear();

    unordered_map<int, vertex_descriptor>::iterator it_v;

    for(int i=0; i < p->no_regions; i++){
        slam_graph_region_t *region = &p->region_list[i];
        for(int j=0; j< region->count; j++){
            slam_graph_node_t *node = &region->nodes[j];  
            vertex_descriptor v = add_vertex(g);
            g[v].id = node->id;        
            vertex_map.insert(make_pair(g[v].id, v));
        }
    }

    set<pair<int, int> > valid_edges;
    set<pair<int, int> >::iterator it_edge_set;
    unordered_map<int, int>::iterator it_i;
    unordered_map<int, int>::iterator it_j;

    unordered_map<int, slam_graph_node_t *>::iterator it_node;
    unordered_map<int, slam_graph_node_t *>::iterator it_node1;

    for(int i=0; i < p->no_edges; i++){            
        slam_graph_node_t *nd1, *nd2; 
        int nd1_id = p->edge_list[i].actual_scanned_node_id_1;
        it_node = self->node_map.find(nd1_id);
        
        if(it_node == self->node_map.end()){
            continue;
        }
        nd1 = it_node->second;
        
        int nd2_id = p->edge_list[i].actual_scanned_node_id_2;

        it_node = self->node_map.find(nd2_id);
        
        if(it_node == self->node_map.end()){
            continue;
        }
        nd2 = it_node->second;

        //check if we have this edge already 
        it_edge_set = valid_edges.find(make_pair(nd1_id, nd2_id)); 
        
        if(it_edge_set != valid_edges.end()){
            continue;
        }

        it_edge_set = valid_edges.find(make_pair(nd2_id, nd1_id)); 
        
        if(it_edge_set != valid_edges.end()){
            continue;
        }

        valid_edges.insert(make_pair(nd1_id, nd2_id)); 
        double dist = hypot(nd1->xy[0] - nd2->xy[0], nd1->xy[1] - nd2->xy[1]);
        
        it_v = vertex_map.find(nd1_id);
        if(it_v == vertex_map.end()){
            continue;
        }

        vertex_descriptor v_i = it_v->second;

        it_v = vertex_map.find(nd2_id);
        if(it_v == vertex_map.end()){
            continue;
        }

        vertex_descriptor v_j = it_v->second;

        add_edge(v_i, v_j, Weight(dist), g);
    }

    for(it_node = self->node_map.begin(); it_node != self->node_map.end(); it_node++){
        for(it_node1 = self->node_map.begin(); it_node1 != self->node_map.end(); it_node1++){
            slam_graph_node_t *nd1, *nd2; 
            nd1 = it_node->second;
            nd2 = it_node1->second;

            if(nd1->id == nd2->id){
                continue;
            }

            //check if we have this edge already 
            it_edge_set = valid_edges.find(make_pair(nd1->id, nd2->id)); 
            
            if(it_edge_set != valid_edges.end()){
                continue;
            }

            it_edge_set = valid_edges.find(make_pair(nd2->id, nd1->id)); 
        
            if(it_edge_set != valid_edges.end()){
                continue;
            }
            
            double dist = hypot(nd1->xy[0] - nd2->xy[0], nd1->xy[1] - nd2->xy[1]);

            if(dist < 1.0){
                //add edge 
                valid_edges.insert(make_pair(nd1->id, nd2->id));

                it_v = vertex_map.find(nd1->id);
                if(it_v == vertex_map.end()){
                    continue;
                }

                vertex_descriptor v_i = it_v->second;

                it_v = vertex_map.find(nd2->id);
                if(it_v == vertex_map.end()){
                    continue;
                }
                vertex_descriptor v_j = it_v->second;
                add_edge(v_i, v_j, Weight(dist), g);
            }
        }
    }

    fprintf(stderr, "Done building the graph\n");
    update_the_path(self);
}

static int remap_g_region_ind(int ind, int middle){
    //split them in half - based on even or odd 
    int rem = ind % 2;
    
    int add = 0;
    
    if(middle %2==0)
        add = 1;

    //even 
    if(rem ==0){
        if(ind <= middle)
            return ind;
        else
            return ind - (middle+add);
    }
    else{
        if(ind <= middle)
            return ind+(middle+add);
        else
            return ind;
    }
}


static void draw_g_triangle(RendererLanguageAnnotation *self, double pos[3], double theta, double color_ratio, double scale = 1.0){
    //double theta = 0;
        
    /*int count = fmax(2,ceil(2 *M_PI / resolution_radians));
      double delta =  2* M_PI/count;*/
    glColor3fv(bot_color_util_jet(color_ratio));

    glPushMatrix();
    glTranslated(pos[0], pos[1], pos[2]);
    glRotatef(theta * 180 / M_PI, 0, 0, 1);
    glScalef (scale, scale, scale);
    glPushAttrib (GL_ENABLE_BIT | GL_DEPTH_BUFFER_BIT);
    glDisable (GL_DEPTH_TEST);
    glLineWidth(2);
    glEnable(GL_POLYGON_OFFSET_FILL);
    glPolygonOffset (1, 1);

    /*double fp_length = self->footprint[0] - self->footprint[4];
    double fp_width = fabs (self->footprint[1] - self->footprint[3]);

    double front_middle[2] = {(self->footprint[0] + self->footprint[2])/2, (self->footprint[1] + self->footprint[3])/2};*/

    glBegin(GL_TRIANGLES);
    glVertex3f( self->front_middle[0], self->front_middle[1], 0);
    glVertex3f( self->footprint[6], self->footprint[7], 0); 
    glVertex3f(  self->footprint[4], self->footprint[5], 0);
    glEnd();
    glPopAttrib ();
    glPopMatrix();
    /*glBegin(GL_TRIANGLE_FAN);
    glVertex3d(pos[0], pos[1], pos[2]);
    glVertex3d(pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2]);
    for(int k=0; k< count; k++){
        theta += delta;
        glVertex3d( pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2]);
    }
    glEnd();*/
}

static void draw_g_circle(double pos[3], double color_ratio, double resolution_radians, double radius){
    double theta = 0;
        
    int count = fmax(2,ceil(2 *M_PI / resolution_radians));
    double delta =  2* M_PI/count;
    glColor3fv(bot_color_util_jet(color_ratio));

    glBegin(GL_TRIANGLE_FAN);
    glVertex3d(pos[0], pos[1], pos[2]);
    glVertex3d(pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2]);
    for(int k=0; k< count; k++){
        theta += delta;
        glVertex3d( pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2]);
    }
    glEnd();
}

static void on_lang_annotation (const lcm_recv_buf_t *rbuf, const char *channel,
                                const slam_language_annotation_t *msg, void *user)
{
    RendererLanguageAnnotation *self = (RendererLanguageAnnotation *)user;
    g_assert(self);

    g_assert(self->write_log);
    
    fprintf(stderr, "Received annotation\n");

    int channellen = strlen(channel);
    int64_t mem_sz = sizeof(lcm_eventlog_event_t) + channellen + 1 + rbuf->data_size;

    lcm_eventlog_event_t *le = (lcm_eventlog_event_t*) malloc(mem_sz);
    le = (lcm_eventlog_event_t*) malloc(mem_sz);
    memset(le, 0, mem_sz);

    le->timestamp = rbuf->recv_utime;
    le->channellen = channellen;
    le->datalen = rbuf->data_size;

    le->channel = ((char*)le) + sizeof(lcm_eventlog_event_t);
    strcpy(le->channel, channel);
    le->data = le->channel + channellen+1;

    assert((char*)le->data + rbuf->data_size == (char*)le + mem_sz);
    memcpy(le->data, rbuf->data, rbuf->data_size);
    
    if(0 != lcm_eventlog_write_event(self->write_log, le)){
	fprintf(stderr, "Error\n");
    }
    else{
	fprintf(stderr, "Saved Annotation to file\n");
    }    
    free(le);

}

char * get_spatial_relation(RendererLanguageAnnotation *self){
    
    int sr_ind = bot_gtk_param_widget_get_enum(self->pw, PARAM_SPATIAL_RELATION);

    if(sr_ind < self->spatial_relations.size()){
        fprintf(stderr, "Spatial Relation : %s\n", self->sr_description[sr_ind]);
        return strdup(self->sr_description[sr_ind]);
    }
    return NULL;
}

static void save_language_annotation(RendererLanguageAnnotation *self, slam_graph_region_particle_t *p){

    if(p == NULL){
        fprintf(stderr, "Error - No particle with ID found\n");
        return;
    }

    //check if we have valid values 

    //this can change based on the SR 
    //e.g. something is near something else doesn't require the agent 
    // or something is near the robot doesn't require a landmark 
    
    /*if(self->robot_id < 0 || self->landmark_region_id < 0 || self->figure_region_id < 0 || self->figure_node_id < 0 ||
       self->landmark_node_id < 0){
        //this can fail - this should depend on the spatial relation 
        //e.g. near/next to or right/left of 
        fprintf(stderr, "Error missing information\n");
        return;
        }*/

    if(self->figure_node_id < 0 || (self->robot_id < 0 && self->landmark_node_id < 0)){
        //this can fail - this should depend on the spatial relation 
        //e.g. near/next to or right/left of 
        fprintf(stderr, "Error missing information\n");
        return;
    }

    //if the figure is missing or if the landmark and the robot position is both missing then there isn't enough info

    char *spatial_relation = get_spatial_relation(self);
    
    if(spatial_relation == NULL){
        fprintf(stderr, "No valid spatial relation\n");
        return;
    }

    unordered_map<int, slam_graph_node_t *>::iterator it;

    slam_graph_node_t *agent_node = NULL;
    if(self->robot_id >= 0){
        it = self->node_map.find(self->robot_id);
        
        if(it == self->node_map.end()){
            fprintf(stderr, "Error Agent node not found - unable to save\n");
            return;
        }
        agent_node = it->second;
    }    

    slam_graph_node_t *landmark_node = NULL;
    if(self->landmark_node_id >=0){
        //search for the ladmark node 
        it = self->node_map.find(self->landmark_node_id);
        
        if(it == self->node_map.end()){
            fprintf(stderr, "Error - Landmark node not found - unable to save\n");
            //this is an issue - free the message
            return;
        }
        landmark_node = it->second;
    }

    slam_graph_node_t *figure_node = NULL;
    if(self->figure_node_id >=0){
        //search for the ladmark node 
        it = self->node_map.find(self->figure_node_id);
        
        if(it == self->node_map.end()){
            fprintf(stderr, "Error - Figure node not found - unable to save\n");
            //this is an issue - free the message
            return;
        }
        figure_node = it->second;
    }

    slam_language_annotation_t *msg = (slam_language_annotation_t *) calloc(1, sizeof(slam_language_annotation_t)); 
    
    //do we need to fill these?? - if so easier to do with a text box
    msg->utterence = strdup("test");
    msg->landmark_name = strdup("test");
    msg->figure_name = strdup("test");
    msg->correct = 1;
    msg->sr = spatial_relation;
    
    if(agent_node){
        msg->agent_valid = 1;
        msg->agent_pose.id = self->robot_id;
        msg->agent_pose.xy[0] = agent_node->xy[0];
        msg->agent_pose.xy[1] = agent_node->xy[1];
        msg->agent_pose.t = agent_node->heading;
    }
    else{
        //this SR doesn't require the agent's pose 
        msg->agent_valid = 0;
    }

    if(landmark_node){
        msg->landmark_valid = 1;
        deep_copy_node(landmark_node, &msg->landmark);
    }
    else{
        //we need to fill this with an empty data structure
        msg->landmark_valid = 0;
        set_node_empty(&msg->landmark);
    }

    if(figure_node){
        msg->figure_valid = 1;
        deep_copy_node(figure_node, &msg->figure);
    }
    else{
        msg->figure_valid = 0; //not sure if this would ever be the case 
        //we need to fill this with an empty data structure
        set_node_empty(&msg->figure);
    }
    
    //save the path 
    msg->path_count = self->path_to_figure.size();

    msg->path = (slam_node_pose_t *) calloc(msg->path_count, sizeof(slam_node_pose_t));
    for(int i=0; i < msg->path_count; i++){
        int id = self->path_to_figure[i];

        it = self->node_map.find(id);
        
        if(it == self->node_map.end()){
            fprintf(stderr, "Error Path node not found - unable to save\n");
            continue;
        }

        slam_graph_node_t *p_node = it->second;
        
        msg->path[i].id = p_node->id;
        msg->path[i].xy[0] = p_node->xy[0];
        msg->path[i].xy[1] = p_node->xy[1];
        msg->path[i].t = p_node->heading;        
    }

    slam_language_annotation_t_publish(self->lcm, "LANGUAGE_ANNOTATION", msg);
    slam_language_annotation_t_destroy(msg);    

    fprintf(stderr, "Landmark Region id : %d\n", self->landmark_region_id);
}

static void save_g_particle(RendererLanguageAnnotation *self, slam_graph_region_particle_t *p){
    char region_name[1000];
    sprintf(region_name, "%s.region", self->lang_filename);

    char node_name[1000];
    sprintf(node_name, "%s.node", self->lang_filename);

    FILE *fp = fopen(region_name, "w");
    
    for(int i=0; i < p->no_region_edges; i++){
        slam_graph_region_edge_t edge = p->region_edges[i];
        
        int rn_1_id = edge.region_1_id;
        int rn_2_id = edge.region_2_id;
        
        //for now add weight 1 
        //we could add spatial distance instead - which we can calculate based on the region mean - would take a bit more effort
        fprintf(fp,"%d,%d,%f\n", rn_1_id, rn_2_id, 1.0);
    }
    fclose (fp);

    fp = fopen(node_name, "w");
    
    for(int i=0; i < p->no_edges; i++){      
        int a = p->edge_list[i].actual_scanned_node_id_1;
        int b = p->edge_list[i].actual_scanned_node_id_2;
        //for now add weight 1 
        //we could add spatial distance instead - which we can calculate based on the region mean - would take a bit more effort
        fprintf(fp,"%d,%d,%f\n", a, b, 1.0);
    }
    fclose (fp);
}
      
static void draw_g_particle(RendererLanguageAnnotation *self, slam_graph_region_particle_t *p, int k, double min_prob, 
                     int active_particle, double scale, int num_particles){

    params_t params = self->params;

    char label[1042];
    int j = p->id;
    double prob = p->weight; 
    if(!params.log_scale)
        prob = exp(prob);
    double alpha = 1.0;
    double weight_color = (prob - min_prob) * scale;

    
    double weight = 0;

    if(self->params.draw_all_graphs && self->params.particle_ordering_mode == 0){
        weight = (prob - min_prob) * scale * params.distance_scale;
    }
    else if(self->params.draw_all_graphs && self->params.particle_ordering_mode == 1){
        weight = k / (double) num_particles * scale * params.distance_scale;
    }

    if(self->params.draw_all_graphs && params.equal_dist){
        //make the gaps equal
        weight = (num_particles - k) * params.distance_scale;
    }        

    GHashTable *slam_nodes = g_hash_table_new(g_int_hash, g_int_equal);

    GList * sorted_region_list = NULL;
    
    //we need to refer to the nodes for drawing edges 
    for(int i=0; i < p->no_regions; i++){
        slam_graph_region_t *region = &p->region_list[i];
        sorted_region_list = g_list_insert_sorted (sorted_region_list,region, compare_g_region_id);

        for(int j=0; j< region->count; j++){
            g_hash_table_insert(slam_nodes, &(region->nodes[j].id), &region->nodes[j]);
        }
    }

    //GHashTable *region_mapped_ind = g_hash_table_new(g_int_hash, g_int_equal);//
    GHashTable *region_mapped_ind = g_hash_table_new_full(g_int_hash, g_int_equal, NULL, destroy_g_int);

    //int *region_maped_ind = (int *) calloc(p->no_regions, sizeof(int));

    for(guint i=0; i <  g_list_length(sorted_region_list); i++){
        slam_graph_region_t *p = (slam_graph_region_t *) g_list_nth_data (sorted_region_list, i); 
        int *ind = (int *) calloc(1, sizeof(int)); 
        *ind = i;
        g_hash_table_insert(region_mapped_ind, &(p->id), ind);
        //region_maped_ind[p->id] = i;
    }

    g_list_free(sorted_region_list);

    double gap = params.distance_scale; 

    double inc = 0.0001;
    if(params.draw_height){
        inc = gap / p->no_nodes;
    }

    glPushAttrib (GL_DEPTH_BUFFER_BIT | GL_POINT_BIT | GL_CURRENT_BIT);
    glEnable (GL_DEPTH_TEST);
    glEnable(GL_BLEND);
        
    glColor3fv(bot_color_util_jet(weight_color));
        
    glEnable( GL_POINT_SMOOTH );
    glEnable( GL_BLEND );
    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    glPointSize( 8.0 );
                   
    if(params.draw_cov){
        for(int i=0; i < p->no_regions; i++){
            slam_graph_region_t *region = &p->region_list[i];
            
            for(int j=0; j< region->count; j++){
                slam_graph_node_t node = region->nodes[j]; 
                
                double node_cov_xy[] = { node.cov[0], node.cov[1], node.cov[3], node.cov[4] };
                
                gsl_matrix_view cov_xy = 
                    gsl_matrix_view_array (node_cov_xy, 2, 2);
                
                gsl_vector *eval = gsl_vector_alloc (2);
                gsl_matrix *evec = gsl_matrix_alloc (2, 2);
                
                gsl_eigen_symmv_workspace *w =
                    gsl_eigen_symmv_alloc (2);
                
                gsl_eigen_symmv (&cov_xy.matrix, eval, evec, w);
                
                gsl_eigen_symmv_free (w);
                
                double m_opengl[16] = {evec->data[0], evec->data[1], 0, 0,
                                       evec->data[2], evec->data[3], 0, 0,
                                       0, 0, 1, 0,
                                       0, 0, 0, 1};
                
                
                glPushMatrix();
                
                
                glTranslatef (node.xy[0], node.xy[1], weight);
                glMultMatrixd (m_opengl); 
                glScalef (sqrt(eval->data[0]), sqrt(eval->data[1]), 1);
                gsl_vector_free (eval);
                gsl_matrix_free (evec);
                
                glLineWidth(.8);
                bot_gl_draw_circle(1.0);
                glPopMatrix();
            }                       
        }
    }                 
        
                    
    if(params.bounding_boxes){
        for(int i=0; i < p->no_regions; i++){
            slam_graph_region_t *region = &p->region_list[i];
            
            for(int j=0; j< region->count; j++){
                slam_graph_node_t node = region->nodes[j]; 
                
                if(params.draw_regions && !node.is_supernode)
                    continue;

                glBegin(GL_LINE_LOOP);
                for(int k=0; k< node.no_points; k++){
                    glVertex3d( node.x_coords[k], node.y_coords[k] , weight);
                }
                glEnd();
            }      
        }                 
    }
        
    
        
    int div = p->no_regions;
    //make this even 
    if(div %2 == 1){
        div++;
    }
    int middle = (div / 2.0) -1;

    if(params.draw_nodes && !(params.draw_semantic_class || params.draw_semantic_class_max) 
       && !(params.draw_semantic_label || params.draw_semantic_label_max)){  
        double region_weight = 0;
        
        for(int i=0; i < p->no_regions; i++){
            slam_graph_region_t *region = &p->region_list[i];
            int r_id = *(int *) g_hash_table_lookup(region_mapped_ind, &(region->id));
            if(params.color_regions){
                if(params.remap_regions){
                    //int remapped_ind = remap_region_ind(region->id, middle);
                    int remapped_ind = remap_g_region_ind(r_id, middle);
                    region_weight = remapped_ind/((double) p->no_regions-1);
                }
                else{
                    region_weight =  region->id / ((double) p->no_regions-1);
                }     
            }
            
            int figure = 0;
            int landmark = 0;
            
            if(region->id == self->landmark_region_id){
                landmark = 1;
            }
            if(region->id == self->figure_region_id){
                figure = 1;
            }
            
            set<int>::iterator it;

            for(int j=0; j< region->count; j++){
                slam_graph_node_t node = region->nodes[j];  
                int selected = 0;
                if(node.id == self->robot_id){
                    selected = 1;
                }

                int path = 0;
                it = self->path_to_figure_map.find(node.id);
                if(it != self->path_to_figure_map.end()){
                    path = 1;
                }

                if(params.draw_regions && !params.draw_region_mean && node.is_supernode){
                    continue;
                }
                double c_weight = weight;
                //if(params.draw_height){
                c_weight += inc * node.id;
                //}
                        
                //skip drawing the node if the pie chart is drawn
                if((params.draw_pie_chart || self->params.draw_max_label) && node.is_supernode == 1){
                    slam_label_distribution_t ld = node.labeldist;
                    double prob = ld.observation_frequency[0];
                    int different = 0;
                    for(int k=0; k<ld.num_labels; k++){
                        if(ld.observation_frequency[k] != prob){
                            different = 1;
                            break;
                        }
                    }
                    if(different)
                        continue;
                }
                        
                int max_class = -1;
                double max_prob = 0;
                        
                
                double node_pos[3] = {node.xy[0], node.xy[1], c_weight + 0.05};
                

                //add the color back and make this general

                if(selected){
                    if(params.draw_heading)
                        draw_g_triangle(self, node_pos, node.heading, 1.0, 1.5);
                    else
                        draw_g_circle(node_pos, 1.0, bot_to_radians(10.0), params.node_radius * 1.5);   
                }
                else if(landmark){
                    if(params.draw_heading)
                        draw_g_triangle(self, node_pos, node.heading, 0.5, 1.5);
                    else
                        draw_g_circle(node_pos, 0.5, bot_to_radians(10.0), params.node_radius *1.5);   
                }
                else if(figure){
                    if(params.draw_heading)
                        draw_g_triangle(self, node_pos, node.heading, 0.2, 1.5);
                    else
                        draw_g_circle(node_pos, 0.2, bot_to_radians(10.0), params.node_radius *1.5);   
                }
                else if(path){
                    if(params.draw_heading)
                        draw_g_triangle(self, node_pos, node.heading, 0.7, 1.5);
                    else
                        draw_g_circle(node_pos, 0.7, bot_to_radians(10.0), params.node_radius *1.5);   
                }
                else{
                    if(params.draw_heading)
                        draw_g_triangle(self, node_pos, node.heading, region_weight);
                    else
                        draw_g_circle(node_pos, region_weight, bot_to_radians(10.0), params.node_radius);   
                }
            }
        }                    
            
    }

    if(params.draw_regions || params.draw_semantic_class || params.draw_semantic_class_max ||
       params.draw_semantic_label || params.draw_semantic_label_max){
        //draw the region mean 
        //glPointSize( 25.0 );
        //glBegin( GL_POINTS );
        
        double region_weight = 0;
        for(int i=0; i < p->no_regions; i++){
            slam_graph_region_t *region = &p->region_list[i];
            int r_id = *(int*) g_hash_table_lookup(region_mapped_ind, &(region->id));
            
            slam_probability_distribution_t *sem_classification =  &region->region_type_dist;
            slam_probability_distribution_t *sem_label =  &region->region_label_dist;

            if(params.color_regions){     
                if(params.remap_regions){
                    int remapped_ind = remap_g_region_ind(r_id, middle);//region->id, middle);
                    region_weight = remapped_ind/((double) p->no_regions-1);
                }
                else{
                    region_weight = region->id / ((double) p->no_regions-1);
                }   
            }
 
            //glColor3fv(bot_color_util_jet(region_weight));
            if(params.draw_region_mean){
                double c_weight = weight;
                //glVertex3d( region->mean_xy[0], region->mean_xy[1] , c_weight);  
                double node_pos[3] = {region->mean_xy[0], region->mean_xy[1], c_weight + 0.05};
                
                draw_g_circle(node_pos, region_weight, bot_to_radians(10.0), 2 * params.node_radius);   
                
            }
            else{
                for(int j=0; j< region->count; j++){
                    slam_graph_node_t node = region->nodes[j];  
                    double c_weight = weight;
                    //if(params.draw_height){
                    c_weight += inc * node.id;
                    //}
                    
                    if(node.is_supernode == 1){
                        double node_pos[3] = {node.xy[0], node.xy[1] , c_weight + 0.05};
                        draw_g_circle(node_pos, region_weight, bot_to_radians(10.0), 2 * params.node_radius);   
                        break;
                    }
                }
            }
        }        
    }
    glPopAttrib();

    glColor3fv(bot_color_util_jet(weight_color));
        
    if(params.draw_prob || params.draw_node_ids|| params.draw_region_ids || params.draw_segment_ids){
        glColor3f(1,1,1);
        for(int i=0; i < p->no_regions; i++){
            slam_graph_region_t *region = &p->region_list[i];
            if(params.draw_region_ids){
                slam_graph_node_t *node = (slam_graph_node_t *) g_hash_table_lookup(slam_nodes, &(region->center_ind));
                double c_weight = weight;
                if(params.draw_height){
                    c_weight += inc * node->id; //these heights are going to be messed up 
                }
                double textpos[3] = {node->xy[0] +0.4, node->xy[1] +0.4, c_weight};
                if(params.draw_segment_ids){
                    sprintf(label,"%d : %d", (int) region->id, (int) node->segment_id);
                }
                else{
                    sprintf(label,"%d", (int) region->id);
                }
                bot_gl_draw_text(textpos, GLUT_BITMAP_HELVETICA_12, label,
                                 BOT_GL_DRAW_TEXT_DROP_SHADOW);
            }
            else{
                for(int j=0; j< region->count; j++){
                    slam_graph_node_t node = region->nodes[j];     
                    if(node.is_supernode == 1 && params.draw_regions || params.draw_nodes){  
                        //draw the prob 
                        double c_weight = weight;
                        if(params.draw_height){
                            c_weight += inc * i;
                        }
                        double textpos[3] = {node.xy[0] +0.4, node.xy[1] +0.4, c_weight};
                
                        if(params.draw_prob){
                            sprintf(label,"%.3f", node.pofz);
                            bot_gl_draw_text(textpos, GLUT_BITMAP_HELVETICA_12, label,
                                             BOT_GL_DRAW_TEXT_DROP_SHADOW);
                        }
                        else{
                            if(params.draw_segment_ids && params.draw_node_ids){
                                sprintf(label,"(%d)=>%d", (int) node.segment_id, (int)node.id);
                            }
                            else if(params.draw_segment_ids){
                                sprintf(label,"(%d)", (int) node.segment_id);
                            }
                            else{
                                sprintf(label,"[%d]=>%d", (int) region->id, (int)node.id);
                            }
                            bot_gl_draw_text(textpos, GLUT_BITMAP_HELVETICA_12, label,
                                             BOT_GL_DRAW_TEXT_DROP_SHADOW);
                    
                        }                
                    }
                }
            }
        }
    }
        
    //draw the edges 
    glPushAttrib (GL_DEPTH_BUFFER_BIT | GL_POINT_BIT | GL_CURRENT_BIT);
    glEnable (GL_DEPTH_TEST);
    glEnable(GL_BLEND);
        
    if(params.draw_inter_region_edges || params.draw_intra_region_edges){//!params.draw_region_connections){
        for(int i=0; i < p->no_edges; i++){            
            int a;
            int b;
            
            a = p->edge_list[i].actual_scanned_node_id_1;
            b = p->edge_list[i].actual_scanned_node_id_2;

            slam_graph_node_t *nodea = (slam_graph_node_t *) g_hash_table_lookup(slam_nodes, &a);
            slam_graph_node_t *nodeb = (slam_graph_node_t *) g_hash_table_lookup(slam_nodes, &b);
            
            if(!params.draw_intra_region_edges){
                if(nodea->parent_supernode == nodeb->parent_supernode){
                    continue;
                }
            }

            if(!params.draw_inter_region_edges){
                if(nodea->parent_supernode != nodeb->parent_supernode){
                    continue;
                }
            }
            
            double c_weight_1 = weight;
            double c_weight_2 = weight;
            if(params.draw_height){
                c_weight_1 += inc * a;
                c_weight_2 += inc * b;
            }
            
            if(p->edge_list[i].status == SLAM_GRAPH_EDGE_T_STATUS_INITIALIZED){
                //we should prob skip them
                continue;
            }
            else{
                //this should be deprecated 
                if(p->edge_list[i].status == SLAM_GRAPH_EDGE_T_STATUS_FAILED){ 
                    if(!params.draw_dead_edges)
                        continue;
                    double width =  params.edge_thickness * p->edge_list[i].scanmatch_hit;
                    if(width == 0)
                        width = 0.1;
                    glLineWidth(width);
                    glColor3fv(bot_color_util_black);                            
                }
                else if(p->edge_list[i].status == SLAM_GRAPH_EDGE_T_STATUS_SUCCESS){
                    double width = params.edge_thickness * p->edge_list[i].scanmatch_hit;
                    if(width == 0)
                        width = 0.1;
                    glLineWidth(width);
                    
                    if(p->edge_list[i].type == SLAM_GRAPH_EDGE_T_TYPE_SM_LC){
                        float loop_color[3] = {1.0, 0, 0};
                        glColor3fv(bot_color_util_jet(1.0));
                    }
                    else if(p->edge_list[i].type == SLAM_GRAPH_EDGE_T_TYPE_LANGUAGE || p->edge_list[i].type == SLAM_GRAPH_EDGE_T_TYPE_LANGUAGE_FULL){
                        float loop_color[3] = {0, 1.0, 0};
                        glColor3fv(loop_color);
                    }
                    else if(p->edge_list[i].type == SLAM_GRAPH_EDGE_T_TYPE_ODOM_INC && params.draw_odom){
                        float loop_color[3] = {1.0, 0.8, 0.34};
                        glColor3fv(loop_color);
                    }
                    else if(p->edge_list[i].type == SLAM_GRAPH_EDGE_T_TYPE_SM_INFERRED){
                        float loop_color[3] = {1.0, 0.3, 1.0};
                        glColor3fv(loop_color);
                    }
                    else if(p->edge_list[i].type == SLAM_GRAPH_EDGE_T_TYPE_SM_NEW_INFO){
                        float loop_color[3] = {0.4, 0.1, 1.0};
                        glColor3fv(loop_color);
                    }
                    
                    else{
                        glColor3fv(bot_color_util_jet(weight_color));
                    }
                }                        
            }                    
            
            
            double alpha = 1.0;
            double scale = 0.05;
            
            glPointSize(4.0f);
            glBegin(GL_LINES);

            if(params.color_regions){
                double region_weight = 0;
                if(params.remap_regions){
                    int r_id = *(int*) g_hash_table_lookup(region_mapped_ind, &(nodeb->parent_supernode));//region->id));
                    int remapped_ind = remap_g_region_ind(r_id, middle);//nodeb->parent_supernode, middle);
                    region_weight = remapped_ind/((double) p->no_regions-1);
                }
                else{
                    region_weight = nodeb->parent_supernode / ((double) p->no_regions-1);
                }  
                
                glColor3fv(bot_color_util_jet(region_weight));
            }
            
            glVertex3d(nodea->xy[0],
                       nodea->xy[1],
                       c_weight_1);
            glVertex3d(nodeb->xy[0],
                       nodeb->xy[1],
                       c_weight_2);
            glEnd();
        }

        if(params.draw_dead_edges){
            for(int i=0; i < p->no_rejected_edges; i++){
                int a;
                int b;
                
                a = p->rejected_edge_list[i].actual_scanned_node_id_1;
                b = p->rejected_edge_list[i].actual_scanned_node_id_2;
                
                double c_weight_1 = weight;
                double c_weight_2 = weight;
                if(params.draw_height){
                    c_weight_1 += inc * a;
                    c_weight_2 += inc * b;
                }
                
                if(p->rejected_edge_list[i].status == SLAM_GRAPH_EDGE_T_STATUS_INITIALIZED){
                    //we should prob skip them
                    continue;
                }
                else{
                    if(p->rejected_edge_list[i].status == SLAM_GRAPH_EDGE_T_STATUS_FAILED){ 
                        double width =  params.edge_thickness * p->rejected_edge_list[i].scanmatch_hit;

                        if(width == 0)
                            width = 0.1;
                        glLineWidth(width);
                        glColor3fv(bot_color_util_black);                            
                    }                                     
                }                    
                slam_graph_node_t *nodea = (slam_graph_node_t *) g_hash_table_lookup(slam_nodes, &a);
                slam_graph_node_t *nodeb = (slam_graph_node_t *) g_hash_table_lookup(slam_nodes, &b);
                
                double alpha = 1.0;
                double scale = 0.05;
                
                glPointSize(4.0f);
                glBegin(GL_LINES);
                
                glVertex3d(nodea->xy[0],
                           nodea->xy[1],
                           c_weight_1);
                glVertex3d(nodeb->xy[0],
                           nodeb->xy[1],
                           c_weight_2);
                glEnd();
            }
        }        
    }

    if(self->path_to_figure.size()>1){
        glColor3fv(bot_color_util_red);      
        for(int i=1; i < self->path_to_figure.size(); i++){
            slam_graph_node_t *nd1 = (slam_graph_node_t *) g_hash_table_lookup(slam_nodes, &self->path_to_figure[i-1]);
            slam_graph_node_t *nd2 = (slam_graph_node_t *) g_hash_table_lookup(slam_nodes, &self->path_to_figure[i]);

            glLineWidth(params.edge_thickness*2);
            glBegin(GL_LINES);

            double c_weight_1 = weight;
            double c_weight_2 = weight;
                
            glVertex3d(nd1->xy[0],
                       nd1->xy[1],
                       c_weight_1);
            glVertex3d(nd2->xy[0],
                       nd2->xy[1],
                       c_weight_2);
            glEnd();            
        }
    }

    if(self->landmark_region_id >=0){
        double region_weight = 0;
        for(int i=0; i < p->no_regions; i++){
            slam_graph_region_t *region = &p->region_list[i];
            if(region->id != self->landmark_region_id){
                continue;
            }

            if(params.color_regions){
                int r_id = *(int *) g_hash_table_lookup(region_mapped_ind, &(region->id));
                region_weight =  region->id / ((double) p->no_regions-1);
                
                glColor3fv(bot_color_util_jet(region_weight));
            }

            glLineWidth(params.edge_thickness);
            
            for(int j=0; j< region->count; j++){
                slam_graph_node_t node = region->nodes[j]; 
                
                if(!node.is_supernode)
                    continue;

                glBegin(GL_LINE_LOOP);
                for(int k=0; k< node.no_points; k++){
                    glVertex3d( node.x_coords[k], node.y_coords[k] , weight);
                }
                glEnd();
            }      
        }                 
    }
    
    if(params.draw_region_connections){    
        glColor3f(0,0,1);
        glLineWidth(4);
        GHashTable *regions = g_hash_table_new(g_int_hash, g_int_equal);

        for(int i=0; i < p->no_regions; i++){
            slam_graph_region_t *rn = &p->region_list[i];
            g_hash_table_insert(regions, &(rn->id), rn);
        }
        
        for(int i=0; i < p->no_region_edges; i++){
            slam_graph_region_edge_t edge = p->region_edges[i];
            
            int rn_1_id = edge.region_1_id;
            int rn_2_id = edge.region_2_id;
            
            slam_graph_region_t *rn1 =  (slam_graph_region_t *) g_hash_table_lookup(regions, &rn_1_id);
            slam_graph_region_t *rn2 =  (slam_graph_region_t *) g_hash_table_lookup(regions, &rn_2_id);

            if(rn1 == NULL|| rn2 == NULL){
                fprintf(stderr, "Region %d - %p = %d = %p\n", rn_1_id, (void *) rn1, 
                        rn_2_id, (void *) rn2);
                continue;
            }

            double ra[2] = {0}, rb[2] = {0};
            
            if(params.draw_region_mean){
                ra[0] = rn1->mean_xy[0];
                ra[1] = rn1->mean_xy[1];
                rb[0] = rn2->mean_xy[0];
                rb[1] = rn2->mean_xy[1];
            }
            else{
                slam_graph_node_t *nodea = (slam_graph_node_t *) g_hash_table_lookup(slam_nodes, &(rn1->center_ind));
                ra[0] = nodea->xy[0];
                ra[1] = nodea->xy[1];
                slam_graph_node_t *nodeb = (slam_graph_node_t *) g_hash_table_lookup(slam_nodes, &(rn2->center_ind));
                rb[0] = nodeb->xy[0];
                rb[1] = nodeb->xy[1];
            }
            
            double c_weight_1 = weight;
            double c_weight_2 = weight;
            if(params.draw_height){
                c_weight_1 += inc * rn1->center_ind;
                c_weight_2 += inc * rn2->center_ind;
            }
            glPointSize(4.0f);
            
            glBegin(GL_LINES);
                
            glVertex3d(ra[0], ra[1], c_weight_1);
            glVertex3d(rb[0], rb[1], c_weight_2);
            glEnd();
        }
        //.region_1_id
        //region_2_id
        g_hash_table_destroy (regions);
    }

    glPopAttrib();
    //for(guint i=0; i <  g_list_length(region_mapped_ind); i++){
    //
    //}
    g_hash_table_destroy (region_mapped_ind);
    g_hash_table_destroy (slam_nodes);
}



static void on_topo_graph (const lcm_recv_buf_t *rbuf, const char *channel,
                           const slam_graph_region_particle_list_t *msg, void *user)
{
    RendererLanguageAnnotation *self = (RendererLanguageAnnotation *)user;
    g_assert(self);

    bot_ptr_circular_add(self->particle_history, slam_graph_region_particle_list_t_copy(msg));
    
    //destory the list 
    g_list_free_full (self->particle_list, destroy_g_particle);
    self->particle_list = NULL;
    
    //we should clear this 
    for(int i=0; i <  msg->no_particles; i++){
        slam_graph_region_particle_t *p = slam_graph_region_particle_t_copy(&msg->particle_list[i]);
        
        self->particle_list  = g_list_insert_sorted ( self->particle_list , p, compare_g_graph_id);
    }
    
    int last_valid_ind = bot_gtk_param_widget_get_int(self->pw, VALID_MAP_IND);

    if(msg->no_particles > 1){
        bot_gtk_param_widget_set_enabled (self->pw, VALID_MAP_IND, 1);
        bot_gtk_param_widget_modify_int(self->pw, VALID_MAP_IND, 0, msg->no_particles-1, 1, last_valid_ind);
    }
    else{
        self->active_particle = 0;
        bot_gtk_param_widget_set_enabled (self->pw, VALID_MAP_IND, 0);
    }

    self->no_particles = msg->no_particles;
    self->have_data = 1;    

    bot_viewer_request_redraw (self->viewer);
}

static void on_pose (const lcm_recv_buf_t *rbuf, const char *channel,
                     const bot_core_pose_t *msg, void *user)
{
    RendererLanguageAnnotation *self = (RendererLanguageAnnotation *)user;

    self->last_pose_utime = msg->utime;

    return;
}

static void on_slam_status (const lcm_recv_buf_t *rbuf, const char *channel,
                            const slam_status_t *msg, void *user)
{
    RendererLanguageAnnotation *self = (RendererLanguageAnnotation *)user;
    g_assert(self);
    //clear_g_points(self);
   
    fprintf(stderr, "Slam reset - clearing the old pose info - Finished\n");    
    bot_viewer_request_redraw (self->viewer);
}

static void
renderer_graph_annotation_destroy (BotRenderer *renderer)
{
    fprintf(stderr, "Called destroy\n");

    if (!renderer)
        return;

    RendererLanguageAnnotation *self = (RendererLanguageAnnotation *) renderer->user;

    if (!self)
        return;
    
    if(self->write_log){
        fprintf(stderr, "Closing old log file\n");
        lcm_eventlog_destroy(self->write_log);
    }    
    free (self);
}

static void 
renderer_graph_annotation_draw (BotViewer *viewer, BotRenderer *renderer)
{
    RendererLanguageAnnotation *self = (RendererLanguageAnnotation*)renderer->user;
    g_assert(self);

    if(g_list_length (self->particle_list) == 0)
        return;

    int draw_map_ind = 0;
    if(self->active_particle >=0){
        draw_map_ind = self->active_particle;
    }    
        
    int num_particles = g_list_length (self->particle_list);
        
    int max_prob_id = 0; 
    int min_prob_id = 0;
    int count = 0;
        
    char prob_status[100];
    char *full_status = (char *) calloc(num_particles *100 , sizeof(char));

    GList * sorted_list = NULL;

    if(self->params.particle_ordering_mode == 0){
        for(guint i=0; i <  g_list_length (self->particle_list); i++){
            slam_graph_region_particle_t *p = (slam_graph_region_particle_t *) g_list_nth_data (self->particle_list, i);
            sorted_list = g_list_insert_sorted (sorted_list, p, compare_g_prob);
        }
    }
    else if(self->params.particle_ordering_mode == 1){
        for(guint i=0; i <  g_list_length (self->particle_list); i++){
            slam_graph_region_particle_t *p = (slam_graph_region_particle_t *)  g_list_nth_data (self->particle_list, i);
            sorted_list  = g_list_insert_sorted (sorted_list , p, compare_g_graph_id);
        }
    }

    if(sorted_list == NULL){
        fprintf(stderr, "Error - No particles in sorted list\n");
        return;
    }
        
    for(guint i=0; i <  g_list_length (sorted_list); i++){
        slam_graph_region_particle_t *p = (slam_graph_region_particle_t *) g_list_nth_data (sorted_list, i);
        slam_graph_region_particle_t *p_max = (slam_graph_region_particle_t *) g_list_nth_data (sorted_list, max_prob_id);
        slam_graph_region_particle_t *p_min = (slam_graph_region_particle_t *) g_list_nth_data (sorted_list, min_prob_id);
            
        double prob = p->weight;
        double max_prob = p_max->weight;
        double min_prob = p_min->weight;
        if(!self->params.log_scale){
            prob = exp(prob);
            max_prob = exp(max_prob);
            min_prob = exp(min_prob);
        }
        count++;
        if(prob > max_prob)
            max_prob_id = i;
        else if(prob < min_prob)
            min_prob_id = i;
    }

    slam_graph_region_particle_t *valid_map = (slam_graph_region_particle_t *) g_list_nth_data (sorted_list, draw_map_ind);
          
    slam_graph_region_particle_t *p_max = (slam_graph_region_particle_t *) g_list_nth_data (sorted_list, max_prob_id);
    slam_graph_region_particle_t *p_min = (slam_graph_region_particle_t *) g_list_nth_data (sorted_list, min_prob_id);
        
    double min_prob = p_min->weight; 
    double max_prob = p_max->weight; 
        
    if(!self->params.log_scale){
        max_prob = exp(max_prob);
        min_prob = exp(min_prob);
    }

    if(count == 0){
        g_list_free (sorted_list);
        return;
    }
    double scale = 1.0;
        
    if(max_prob > min_prob){
        scale = 0.8/ (max_prob - min_prob);
    }

    int last_valid_ind = self->current_ind;

    if(self->params.draw_all_graphs || self->params.draw_max_map){
        if(self->current_ind != p_max->id){
            self->current_ind = p_max->id;
            send_map_request(self, self->current_ind);
            //rebuild the graph 
            fprintf(stderr, "Rebuilding the graph\n");
            if(self->graph)
                delete self->graph;
            self->graph = new graph_t();
            build_node_map(self, p_max);
            build_boost_graph(self, p_max, *self->graph, self->vertex_map);
        }
    }
    if(self->params.draw_valid_maps){
        if(valid_map->id != self->current_ind){
            self->current_ind = valid_map->id;
            send_map_request(self, self->current_ind);
            if(self->graph)
                delete self->graph;
            self->graph = new graph_t();
            build_node_map(self, valid_map);
            build_boost_graph(self, valid_map, *self->graph, self->vertex_map);
        }
    }

    sprintf(prob_status, "Current [%d]\n", (int) valid_map->id);
    strcat( full_status, prob_status);
    if(self->params.draw_stats){
        for(guint k=0; k <  g_list_length (sorted_list); k++){
            slam_graph_region_particle_t *p = (slam_graph_region_particle_t *) g_list_nth_data (sorted_list, k); 
            if(valid_map == p){
                sprintf(prob_status, "Cl:%d- S: %d \n-> F:%d \nA:%.3f (M:%d)\n", p->no_close_node_pairs, 
                        p->no_same_region_close_pairs, p->no_failed_close_node_pairs, p->average_distance_of_close_node_pairs, 
                        p->max_dist_of_close_node_pairs);
                strcat( full_status, prob_status);
                break;
            }
        }
       
    }
    for(guint i=0; i <  g_list_length (sorted_list); i++){
        slam_graph_region_particle_t *p = (slam_graph_region_particle_t *) g_list_nth_data (sorted_list, i);
        double prob = p->weight;
        if(!self->params.log_scale){
            prob = exp(prob);
        }
        sprintf(prob_status, "[%d] : %.3f\n", (int) p->id, prob);
        strcat( full_status, prob_status);
    }
    
    char label[1042];
    
    if(!self->params.draw_diff){//!bot_gtk_param_widget_get_bool(self->pw, PARAM_COMPARE_PARTICLE_HISTORY)){
        if(self->params.draw_all_graphs){
            for(guint k=0; k <  g_list_length (sorted_list); k++){
                //draw this particle if it's valid 
                slam_graph_region_particle_t *p = (slam_graph_region_particle_t *) g_list_nth_data (sorted_list, k);            
                draw_g_particle(self, p, k, min_prob, draw_map_ind, scale, num_particles);
            }
        }
        else if(self->params.draw_valid_maps){
            for(guint k=0; k <  g_list_length (sorted_list); k++){
                slam_graph_region_particle_t *p = (slam_graph_region_particle_t *) g_list_nth_data (sorted_list, k); 
                if(valid_map == p){
                    draw_g_particle(self, p, k, min_prob, draw_map_ind, scale, num_particles); 
                    break;
                }
            }
        }
        else if(self->params.draw_max_map){
            for(guint k=0; k <  g_list_length (sorted_list); k++){
                slam_graph_region_particle_t *p = (slam_graph_region_particle_t *) g_list_nth_data (sorted_list, k); 
                if(p_max == p){
                    draw_g_particle(self, p, k, min_prob, draw_map_ind, scale, num_particles); 
                    break;
                }
            }
        }
    }
    else{
        //fprintf(stderr, "Redrawing\n");
        if(self->diff_particle){
            //we need to do something funcky to change from the last particle to the next particle 
            draw_g_particle(self, self->diff_particle, 0, min_prob, draw_map_ind, scale, num_particles); 
        }
    }

    g_list_free (sorted_list);

    double class_xyz[] = {150, 90, 100};
    // Render the current robot status
    GLint viewport[4];
    glGetIntegerv (GL_VIEWPORT, viewport);
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluOrtho2D(0, viewport[2], 0, viewport[3]);

    glColor3f(1,1,1);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

   
    if(self->params.draw_legand){
        GHashTableIter iter;
        gpointer key, value;

        glColor3f(1,1,1);
        double state_xyz[] = {50, 90, 100};
        bot_gl_draw_text(state_xyz, NULL, full_status,
                         BOT_GL_DRAW_TEXT_JUSTIFY_CENTER |
                         BOT_GL_DRAW_TEXT_ANCHOR_VCENTER |
                         BOT_GL_DRAW_TEXT_ANCHOR_HCENTER |
                         BOT_GL_DRAW_TEXT_DROP_SHADOW);
        free(full_status);
    }
    glLoadIdentity();

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
}

static void activate(RendererLanguageAnnotation *self, int type)
{
    self->active = type;
    if(type==0){
        fprintf(stderr,"Reset.\n");
    }
    if(type==1){
        fprintf(stderr,"Ready for first click\n");
    }
    if(type==2){
        fprintf(stderr,"Ready for second click\n");
    }
    if(type==3){
        fprintf(stderr,"Ready to publish\n");
    }    
}


static void select_robot_node(BotViewer *viewer, BotEventHandler *ehandler, double xy[2])
{
    RendererLanguageAnnotation *self = (RendererLanguageAnnotation*) ehandler->user;
   
    double min_dist = HUGE;
    int node_id = -1;

    fprintf(stderr, "CUrrent Ind : %d\n", self->current_ind);

    if(self->current_ind == -1 || self->current_ind > g_list_length(self->particle_list)-1)
        return;
      
    slam_graph_region_particle_t *p = (slam_graph_region_particle_t *) g_list_nth_data (self->particle_list, self->current_ind);
      
    for(int i=0; i < p->no_regions; i++){
        slam_graph_region_t *region = &p->region_list[i];
        
        for(int j=0; j< region->count; j++){
            slam_graph_node_t node = region->nodes[j]; 
            double *temp = node.xy;
            double dist = sqrt(pow(temp[0] - xy[0],2)+pow(temp[1]-xy[1],2));
            if(dist < min_dist){
                min_dist = dist;
                node_id = node.id;
            }
        }
    }

    self->robot_id = node_id;

    fprintf(stderr, "--------------------------------------------------------\nParticle index %d (id %d)\n>First node: %d\n>Distance one: %f\n",(int)self->current_ind, (int)p->id, node_id, min_dist);
    
    if(node_id < 0){
        fprintf(stderr, "Node not found\n");
        return;
    }

    if(self->graph == NULL){
        fprintf(stderr, "No graph\n");
        //build graph from the active particle 
    }

    get_shortest_path_tree(*self->graph, self->vertex_map, node_id, self->parent_map);

    //unordered_map<int, int>::iterator it;
    
    /*for(it = parent_map.begin(); it != parent_map.end(); it++){
        fprintf(stderr, "Node : %d -> Parent : %d\n", it->first, it->second);
        }*/
}

static void select_node(BotViewer *viewer, BotEventHandler *ehandler, double *coords)
{
    RendererLanguageAnnotation *self = (RendererLanguageAnnotation*) ehandler->user;
    
    double *xy = coords;
    
    slam_graph_region_particle_t *p = (slam_graph_region_particle_t *) g_list_nth_data (self->particle_list, bot_gtk_param_widget_get_int(self->pw, VALID_MAP_IND));
    
    if(p == NULL){
        fprintf(stderr," Active Particle : %d = %p\n", self->current_ind, (void *) p);
        fprintf(stderr, "No valid particle\n");
        return;
    }

    double min_dist = 1.0;
    int found = 0;

    slam_graph_node_t node_temp;

    for(int i=0; i < p->no_regions; i++){
        slam_graph_region_t *region = &p->region_list[i];
        
        for(int j=0; j< region->count; j++){
            slam_graph_node_t node = region->nodes[j]; 

            double *temp = node.xy;
	
            double dist = sqrt(pow(temp[0] - xy[0],2) + pow(temp[1] - xy[1],2));
            
            if(dist < min_dist){
                min_dist = dist; 
                //node_temp = &node;
                node_temp = node;
                //fprintf(stderr, "Min Dist : %f => %d\n", min_dist, node_temp.id);
                found = 1;
            }
        }
    }

    if(found == 1){
        fprintf(stderr, "Min Dist : %f => %d\n", min_dist, (int) node_temp.id);
	slam_graph_node_t *node = slam_graph_node_t_copy(&node_temp);
        //fprintf(stderr, "Node found : %d\n", node->id);
	if(!g_hash_table_contains(self->selected_nodes, &(node->id))){
	    g_hash_table_insert(self->selected_nodes, &(node->id), node);
	}
    }
}

static void select_region(BotViewer *viewer, BotEventHandler *ehandler, double *coords, int figure)
{
    RendererLanguageAnnotation *self = (RendererLanguageAnnotation*) ehandler->user;
    
    double *xy = coords;
    
    slam_graph_region_particle_t *p = (slam_graph_region_particle_t *) g_list_nth_data (self->particle_list, bot_gtk_param_widget_get_int(self->pw, VALID_MAP_IND));
    
    if(p == NULL){
        fprintf(stderr," Active Particle : %d = %p\n", self->current_ind, (void *) p);
        fprintf(stderr, "No valid particle\n");
        return;
    }

    double min_dist = 5.0;
    int found = 0;

    //slam_graph_node_t node_temp;
    slam_graph_region_t result_region;

    for(int i=0; i < p->no_regions; i++){
        slam_graph_region_t *region = &p->region_list[i];
        
        for(int j=0; j< region->count; j++){
            slam_graph_node_t node = region->nodes[j]; 
            
            if(node.is_supernode == 0){
                continue;
            }

            double *temp = node.xy;
	
            double dist = sqrt(pow(temp[0] - xy[0],2) + pow(temp[1] - xy[1],2));
            
            if(dist < min_dist){
                min_dist = dist; 
                result_region = p->region_list[i];
                found = 1;
                break;
            }
        }
    }

    if(found == 1){
        if(figure){
            self->figure_region_id = result_region.id;
        }
        else{
            self->landmark_region_id = result_region.id;
        }
        for(int j=0; j< result_region.count; j++){
            slam_graph_node_t node = result_region.nodes[j]; 
            if(node.is_supernode){
                if(figure){
                    self->figure_node_id = node.id;
                }
                else{
                    self->landmark_node_id = node.id;
                }
            }
        }
    }
}

static int mouse_press (BotViewer *viewer, BotEventHandler *ehandler,
                        const double ray_start[3], const double ray_dir[3], 
                        const GdkEventButton *event)
{
    RendererLanguageAnnotation *self = (RendererLanguageAnnotation*) ehandler->user;

    
    double xy[2];
    int consumed = 0;

    if(self->node_selection_mode == SELECT_NONE)
        return consumed;
    

    geom_ray_z_plane_intersect_3d(POINT3D(ray_start), POINT3D(ray_dir), 
                                  0, POINT2D(xy));
    
    /*if(self->active == 1){
        activate(self,2);
        self->xy_first = xy;
        highlight_node(self->viewer, &self->ehandler, 1);
        }*/

    //add the node handling 
    if(self->node_selection_mode == SELECT_AGENT){
        self->xy_second = xy;
        //clear the landmark and figure regions 
        //should we do this on reset and/or save??
        self->robot_id = -1;
        /*self->landmark_region_id = -1;
        self->figure_region_id = -1;
        self->figure_node_id = -1;
        self->landmark_node_id = -1;
        */
        select_robot_node(self->viewer, &self->ehandler, xy);
        self->node_selection_mode = SELECT_NONE;    
        if(self->figure_node_id >=0){
            int found = get_path_to_figure(self->parent_map, self->figure_node_id, self->path_to_figure, self->path_to_figure_map);
            if(found == 0){
                fprintf(stderr, "Error - Path to figure not found\n");
            }
            else{
                fprintf(stderr, "Updated path\n");
            }
        }
    }
    else if(self->node_selection_mode == SELECT_LANDMARK){
        select_region(self->viewer, &self->ehandler,xy, 0);        
        self->node_selection_mode = SELECT_NONE;
    }
    else if(self->node_selection_mode == SELECT_FIGURE){
        select_region(self->viewer, &self->ehandler,xy, 1);   
        self->node_selection_mode = SELECT_NONE;
        //draw path
        int found = get_path_to_figure(self->parent_map, self->figure_node_id, self->path_to_figure, self->path_to_figure_map);
        if(found == 0){
            fprintf(stderr, "Error - Path to figure not found\n");
        }
    }

    //do we allow this to add the bounding box??
    
    bot_viewer_request_redraw(viewer);

    return consumed;
}

static void update_g_params(RendererLanguageAnnotation *self, BotGtkParamWidget *pw){
    self->params.draw_heading = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_HEADING);
    self->params.draw_all_max_labels = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_ALL_MAX_LABELS);
    self->params.draw_legand = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_LEGAND);
    self->params.draw_stats = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_STATS);
    self->params.draw_label_name = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_LABEL_NAME);
    self->params.draw_semantic_class = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_REGION_SEMANTICS);
    self->params.draw_semantic_class_max = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_REGION_SEMANTICS_MAX);
    self->params.draw_semantic_label = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_REGION_LABEL);
    self->params.draw_semantic_label_max = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_REGION_LABEL_MAX);
    self->params.node_radius = bot_gtk_param_widget_get_double( self->pw, PARAM_NODE_RADIUS);
    self->params.draw_odom = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_ODOM);
    self->params.draw_height = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_HEIGHT);
    self->params.draw_region_connections = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_REGION_CONNECTIONS);
    self->params.draw_inter_region_edges = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_INTER_REGION_EDGES);
    self->params.draw_intra_region_edges = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_INTRA_REGION_EDGES);
    self->params.draw_max_map = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_MAX_GRAPH);
    self->params.draw_prob = bot_gtk_param_widget_get_bool (self->pw, PARAM_DISP_PROB);
    self->params.draw_map_points = 0;
    self->params.draw_all_graphs = 0;
    self->params.draw_dead_edges = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_DEAD_EDGES);
    self->params.edge_thickness = bot_gtk_param_widget_get_int (self->pw, PARAM_EDGE_THICKNESS);
    self->params.draw_valid_maps = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_VAID_GRAPH);
    self->params.draw_cov = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_COV);
    //this should be set to draw the pie charts
    self->params.draw_pie_chart = 0;//bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_LABEL_PIE_CHART);
    self->params.draw_max_label = 0;//bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_MAX_LABEL);
    self->params.draw_nodes = bot_gtk_param_widget_get_bool(self->pw, PARAM_DRAW_NODES);
    self->params.draw_node_ids = bot_gtk_param_widget_get_bool(self->pw, PARAM_DRAW_NODE_ID);
    self->params.draw_region_ids = bot_gtk_param_widget_get_bool(self->pw, PARAM_DRAW_REGION_ID);
    self->params.draw_segment_ids = bot_gtk_param_widget_get_bool(self->pw, PARAM_DRAW_SEGMENT_ID);
    //this should be 1 to draw supernodes
    self->params.draw_regions = bot_gtk_param_widget_get_bool(self->pw, PARAM_DRAW_REGIONS);
    self->params.bounding_boxes = bot_gtk_param_widget_get_bool(self->pw, PARAM_BOUNDING_BOXES);
    self->params.color_regions = bot_gtk_param_widget_get_bool(self->pw, PARAM_COLOR_REGIONS);
    self->params.remap_regions = bot_gtk_param_widget_get_bool(self->pw, PARAM_REMAP_REGIONS);

    self->params.log_scale = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_LOG);

    //maybe turn the others off if this is true??
    self->params.draw_side_by_side = 0;
    self->params.g_id_1 = 0;
    //maybe turn the others off if this is true??
    self->params.g_id_2 = 0;

    self->params.particle_ordering_mode = bot_gtk_param_widget_get_enum(pw,PARAM_ORDER_PARTICLES);

    self->params.distance_scale = bot_gtk_param_widget_get_int(self->pw, PARAM_DISTANCE_SCALE);
    self->params.draw_region_mean = bot_gtk_param_widget_get_bool(self->pw, PARAM_DRAW_REGION_MEAN);
    self->params.equal_dist = bot_gtk_param_widget_get_bool(self->pw, PARAM_DRAW_EQUAL_DISTANCE);

    
    self->params.draw_laser_class = bot_gtk_param_widget_get_bool(self->pw, PARAM_DRAW_LASER_CLASSIFICATION);
    self->params.draw_laser_class_max = bot_gtk_param_widget_get_bool(self->pw, PARAM_DRAW_LASER_CLASSIFICATION_MAX);
    self->params.draw_image_class = bot_gtk_param_widget_get_bool(self->pw, PARAM_DRAW_IMAGE_CLASSIFICATION);
    self->params.draw_image_class_max = bot_gtk_param_widget_get_bool(self->pw, PARAM_DRAW_IMAGE_CLASSIFICATION_MAX);
    //self->params.draw_class_text = bot_gtk_param_widget_get_bool(self->pw, PARAM_DRAW_CLASSIFICATION_TEXT);
}

static void
on_param_widget_changed (BotGtkParamWidget *pw, const char *name, void *user)
{
    RendererLanguageAnnotation *self = (RendererLanguageAnnotation *) user;
    
    update_g_params(self, pw);

    if(!strcmp(name, VALID_MAP_IND)) {
        int raw_value = bot_gtk_param_widget_get_int(pw, VALID_MAP_IND);
        if(raw_value < self->no_particles){
            self->active_particle = raw_value;
            fprintf(stderr,"Changing valid map index: %d\n", raw_value);
        }
        else{
            self->active_particle =  self->no_particles -1;
            //fprintf(stderr, "Outside the max particle no\n");
        }
    }
    
    if(!strcmp(name, PARAM_REQUEST_MAP_SAVE)){
        int draw_map_ind = bot_gtk_param_widget_get_int(self->pw, VALID_MAP_IND);
        if(draw_map_ind < g_list_length (self->particle_list)){
            slam_graph_region_particle_t *p = (slam_graph_region_particle_t *) g_list_nth_data (self->particle_list, draw_map_ind);
            if(p == NULL)
                return;
            slam_particle_request_t msg; 
            msg.utime = bot_timestamp_now();
            msg.particle_id = p->id;
            msg.request = SLAM_PARTICLE_REQUEST_T_SAVE_PARTICLE;
            slam_particle_request_t_publish(self->lcm, "PARTICLE_SAVE_REQUEST", &msg);
        }
    }

    if(!strcmp(name, PARAM_REQUEST_RESULT)){
        slam_particle_request_t msg; 
        msg.utime = bot_timestamp_now();
        msg.particle_id = -1;
        msg.request = SLAM_PARTICLE_REQUEST_T_SAVE_PARTICLE;
        slam_particle_request_t_publish(self->lcm, "SEMANTIC_PARTICLE_RESULT_REQUEST", &msg);
    }

    if (!strcmp(name, PARAM_SELECT_LANGUAGE_FILE)) {

        GtkWidget *dialog;
        dialog = gtk_file_chooser_dialog_new("Add particle annotations to file", NULL,
                                             GTK_FILE_CHOOSER_ACTION_SAVE,
                                             GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                             GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
                                             NULL);
    
        if (self->lang_filename)
            gtk_file_chooser_set_filename (GTK_FILE_CHOOSER(dialog),
                                           self->lang_filename);
        
        if (gtk_dialog_run (GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT) {
            char *filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
            if (filename != NULL) {
                if (self->lang_filename)
                    g_free (self->lang_filename);
                self->lang_filename = g_strdup (filename);

                create_new_log(self);
                
                free (filename);
            }
        }

        gtk_widget_destroy (dialog);
    }

    if (!strcmp(name, PARAM_SAVE_LANGUAGE)){

        if(self->lang_filename == NULL){
            GtkWidget *dialog;
            dialog = gtk_file_chooser_dialog_new("Add particle annotations to file", NULL,
                                                 GTK_FILE_CHOOSER_ACTION_SAVE,
                                                 GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                                 GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
                                                 NULL);
    
            if (self->lang_filename)
                gtk_file_chooser_set_filename (GTK_FILE_CHOOSER(dialog),
                                               self->lang_filename);
        
            if (gtk_dialog_run (GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT) {
                char *filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
                if (filename != NULL) {
                    if (self->lang_filename)
                        g_free (self->lang_filename);
                    self->lang_filename = g_strdup (filename);
                    create_new_log(self);
                    free (filename);
                }
            }

            gtk_widget_destroy (dialog);
        }
      
        if(self->lang_filename != NULL){
            if(g_list_length (self->particle_list) == 0 || self->current_ind < 0){
                fprintf(stderr, "No particles to save");
                return;
            }

            slam_graph_region_particle_t *p = (slam_graph_region_particle_t *) g_list_nth_data (self->particle_list, self->current_ind);

            return save_language_annotation(self, p);
        }
    }  
    
    if (!strcmp(name, PARAM_SELECT_AGENT)){
        self->node_selection_mode = SELECT_AGENT;
    }

    if (!strcmp(name, PARAM_SELECT_LANDMARK_REGION)){
        self->node_selection_mode = SELECT_LANDMARK;
    }

    if (!strcmp(name, PARAM_SELECT_FIGURE_REGION)){
        self->node_selection_mode = SELECT_FIGURE;
    }    
    
    if (!strcmp(name, PARAM_SAVE_SELECTION)){
        if(self->annotation_filename == NULL){
            fprintf (stderr, "Please Select a file\n");
        }

        else if(g_hash_table_size(self->selected_nodes) == 0){
            fprintf(stderr, "Please select nodes\n");
        }

        else if(bot_gtk_param_widget_get_text_entry(pw, PARAM_NODE_ANNOTATION) == "Insert Particle Annotation Here"){
            fprintf(stderr, "Please input the node type into the text box.");
        }
      
        else{
	
	
            FILE *fp = fopen (self->annotation_filename, "a");
	    
            fprintf (fp, "groundtruth,%s", bot_gtk_param_widget_get_text_entry(pw, PARAM_NODE_ANNOTATION));

            //append the nodes to the annotated list
            annotated_node_list_t *new_annotation = (annotated_node_list_t *)calloc(1, sizeof(annotated_node_list_t));
            new_annotation->count = g_hash_table_size(self->selected_nodes);
            new_annotation->ids = (int*) calloc (new_annotation->count, sizeof(int));
            new_annotation->label = strdup(bot_gtk_param_widget_get_text_entry(pw, PARAM_NODE_ANNOTATION));  	  
            int n = 0;
            GHashTableIter iter;
            gpointer key, value;
            g_hash_table_iter_init (&iter, self->selected_nodes);
            while (g_hash_table_iter_next (&iter, &key, &value)){
                slam_graph_node_t *node = (slam_graph_node_t *) value;
                fprintf(fp, ",%d", (int) node->id);
                new_annotation->ids[n] = node->id;
                n ++;
            } 
	
            self->annotated_nodes = g_list_append(self->annotated_nodes,new_annotation);
	
            fprintf(fp, "\n");
	
            fclose (fp);
	
            //remove and delete the selected nodes from the hash table 
            g_hash_table_iter_init (&iter, self->selected_nodes);
            while (g_hash_table_iter_next (&iter, &key, &value)){
                g_hash_table_iter_remove(&iter);
                slam_graph_node_t *nd = (slam_graph_node_t *) value;
                slam_graph_node_t_destroy(nd);
            }
            //self->is_annotating_ground_truth = 0;
        }
    }  
    if (!strcmp(name, PARAM_SAVE_RESULT_SELECTION)){

        if(self->annotation_filename == NULL){
            fprintf (stderr, "Please Select a file\n");
        }

        else if(g_hash_table_size(self->selected_nodes) == 0){
            fprintf(stderr, "Please select nodes\n");
        }

        else if(bot_gtk_param_widget_get_text_entry(pw, PARAM_NODE_ANNOTATION) == "Insert Particle Annotation Here"){
            fprintf(stderr, "Please input the node type into the text box.");
        }
      
        else{
	
	
            FILE *fp = fopen (self->annotation_filename, "a");
	    
            fprintf (fp, "result,%s", bot_gtk_param_widget_get_text_entry(pw, PARAM_NODE_ANNOTATION));

            //append the nodes to the annotated list
            annotated_node_list_t *new_annotation = (annotated_node_list_t *)calloc(1, sizeof(annotated_node_list_t));
            new_annotation->count = g_hash_table_size(self->selected_nodes);
            new_annotation->ids = (int*) calloc (new_annotation->count, sizeof(int));
            new_annotation->label = strdup(bot_gtk_param_widget_get_text_entry(pw, PARAM_NODE_ANNOTATION));  	  
            int n = 0;
            GHashTableIter iter;
            gpointer key, value;
            g_hash_table_iter_init (&iter, self->selected_nodes);
            while (g_hash_table_iter_next (&iter, &key, &value)){
                slam_graph_node_t *node = (slam_graph_node_t *) value;
                fprintf(fp, ",%d", (int) node->id);
                new_annotation->ids[n] = node->id;
                n ++;
            } 
	
            self->annotated_nodes = g_list_append(self->annotated_nodes,new_annotation);
	
            fprintf(fp, "\n");
	
            fclose (fp);
	
            //remove and delete the selected nodes from the hash table 
            g_hash_table_iter_init (&iter, self->selected_nodes);
            while (g_hash_table_iter_next (&iter, &key, &value)){
                g_hash_table_iter_remove(&iter);
                slam_graph_node_t *nd = (slam_graph_node_t *) value;
                slam_graph_node_t_destroy(nd);
            }
            //self->is_annotating_result = 0;
        }
    }  

    if (!strcmp(name, PARAM_CLEAR_SELECTION)) {
        clear_paths_and_parents(self);
    }

    //fprintf(stderr, "Total number of annotations: %d\n",g_list_length(self->annotated_nodes));
    bot_viewer_request_redraw (self->viewer);
}
      
/*static void on_laser_scan_list (const lcm_recv_buf_t *rbuf, const char *channel,
  const slam_laser_pose_list_t *msg, void *user)
  {
  RendererRegionTopology *self = (RendererRegionTopology *)user;
  g_assert(self);
  for(int i=0; i < msg->no_poses; i++){
  slam_laser_pose_t *n_lp = &msg->scans[i]; 
  slam_laser_pose_t *laser_pose = (slam_laser_pose_t *) g_hash_table_lookup(self->node_scans, &n_lp->id);
  if(!laser_pose){
  laser_pose = slam_laser_pose_t_copy(n_lp);
  g_hash_table_insert(self->node_scans, &(laser_pose->id), laser_pose);
  }
  }
  self->requested_scans = 0;
  bot_viewer_request_redraw (self->viewer);
  }*/

static void
on_load_preferences (BotViewer *viewer, GKeyFile *keyfile, void *user_data)
{
    RendererLanguageAnnotation *self = (RendererLanguageAnnotation *) user_data;
    bot_gtk_param_widget_load_from_key_file (self->pw, keyfile, self->renderer.name);
}

static void
on_save_preferences (BotViewer *viewer, GKeyFile *keyfile, void *user_data)
{
    RendererLanguageAnnotation *self = (RendererLanguageAnnotation *) user_data;
    bot_gtk_param_widget_save_to_key_file (self->pw, keyfile, self->renderer.name);
}

static void particle_g_destroy(void *user, void *p)
{
    slam_graph_region_particle_list_t *part = (slam_graph_region_particle_list_t *) p;
    slam_graph_region_particle_list_t_destroy(part);
}

static RendererLanguageAnnotation *
renderer_language_annotation_new (BotViewer *viewer, int priority, BotParam * param)
{    
    RendererLanguageAnnotation *self = new RendererLanguageAnnotation();//(RendererLanguageAnnotation*) calloc (1, sizeof (*self));

    self->viewer = viewer;

    self->particle_history = bot_ptr_circular_new(PARTICLE_HISTORY_SIZE, particle_g_destroy, self);
    //self->is_annotating_ground_truth = 0;
    //self->is_annotating_result = 0;
    BotRenderer *renderer = &self->renderer;
    renderer->draw = renderer_graph_annotation_draw;
    
    //this doesn't get called on exit - as per the viewer 
    renderer->destroy = renderer_graph_annotation_destroy;
    renderer->widget = bot_gtk_param_widget_new();
    renderer->name = RENDERER_NAME;
    renderer->user = self;
    renderer->enabled = 1;
    
    self->lcm = bot_lcm_get_global (NULL);
    if (!self->lcm) {
        fprintf (stderr,"Error: setup_renderer_laser() failed to get global lcm object\n");
        renderer_graph_annotation_destroy (renderer);
        return NULL;
    }

    self->param = param;
    if (!self->param) {
        fprintf (stderr,"Error: setup_renderer_laser() failed to get BotParam instance\n");
        renderer_graph_annotation_destroy (renderer);
        return NULL;
    }

    self->mutex = g_mutex_new ();
     
    self->pw = BOT_GTK_PARAM_WIDGET (renderer->widget);
    self->particle_list = NULL;

    double *fp = self->footprint;

    bot_param_get_double_array_or_fail (self->param, "calibration.vehicle_bounds.front_left",
                                        fp, 2);
    bot_param_get_double_array_or_fail (self->param, "calibration.vehicle_bounds.front_right",
                                        fp+2, 2);
    bot_param_get_double_array_or_fail (self->param, "calibration.vehicle_bounds.rear_right",
                                        fp+4, 2);
    bot_param_get_double_array_or_fail (self->param, "calibration.vehicle_bounds.rear_left",
                                        fp+6, 2);

    self->fp_length = self->footprint[0] - self->footprint[4];
    self->fp_width = fabs (self->footprint[1] - self->footprint[3]);

    self->front_middle[0] = (self->footprint[0] + self->footprint[2])/2;
    self->front_middle[1] = (self->footprint[1] + self->footprint[3])/2;


    //bot_gtk_param_widget_add_double(self->pw, PROB_BOUND, 
    //                                BOT_GTK_PARAM_WIDGET_SLIDER, 0,.5 , .001, .3);
    
    gtk_widget_show_all (renderer->widget);
    g_signal_connect (G_OBJECT (self->pw), "changed", 
                      G_CALLBACK (on_param_widget_changed), self);
    g_signal_connect (G_OBJECT (viewer), "load-preferences", 
                      G_CALLBACK (on_load_preferences), self);
    g_signal_connect (G_OBJECT (viewer), "save-preferences",
                      G_CALLBACK (on_save_preferences), self);

    bot_gtk_param_widget_add_separator(self->pw, "Graphs to Render");
    
    bot_gtk_param_widget_add_buttons(self->pw, PARAM_REQUEST_RESULT, NULL);

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0,
                                       PARAM_DRAW_MAX_GRAPH, 0, NULL);

    
    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0,
                                       PARAM_DRAW_VAID_GRAPH, 0, NULL);

    bot_gtk_param_widget_add_int(self->pw, VALID_MAP_IND, 
                                 BOT_GTK_PARAM_WIDGET_SLIDER, 0,50 , 1, 0);

    bot_gtk_param_widget_add_enum(self->pw, PARAM_ORDER_PARTICLES, BOT_GTK_PARAM_WIDGET_MENU, 
                                  0, 
                                  "Prob",0,
                                  "ID",1,
                                  NULL);

    bot_gtk_param_widget_add_int(self->pw,PARAM_DISTANCE_SCALE, 
                                 BOT_GTK_PARAM_WIDGET_SLIDER, 0 , 40 , 1, 5);
    
    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0,
                                       PARAM_DRAW_EQUAL_DISTANCE, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0,
                                       PARAM_DRAW_HEIGHT, 0, NULL);

    bot_gtk_param_widget_add_separator(self->pw, "Graph Properties");

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0,
                                       PARAM_DRAW_REGIONS, 0, NULL);
    
    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0,
                                       PARAM_DRAW_NODES, 0, PARAM_DRAW_HEADING, 1, NULL);
       
    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0,
                                       PARAM_DRAW_REGION_CONNECTIONS, 
                                       0, NULL);
    
    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0,
                                       PARAM_DRAW_INTRA_REGION_EDGES, 
                                       0, NULL);
    
    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0,
                                       PARAM_DRAW_INTER_REGION_EDGES, 
                                       0, NULL);

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0,
                                       PARAM_DRAW_REGION_MEAN, 
                                       0, NULL);

    bot_gtk_param_widget_add_separator(self->pw, "Language Annotation");
    // BUTTON - Create new file to write language labels

    bot_gtk_param_widget_add_buttons (self->pw, PARAM_SELECT_LANGUAGE_FILE, NULL);  

    //add the buttons to 
    //select the current location of the robot
    bot_gtk_param_widget_add_buttons (self->pw, PARAM_SELECT_AGENT, NULL);  

    //do we draw the orientation as well?? - can this become messy when the robot revisits a region?? 

    //also do we want to provide the segmentation?? 

    //select the landmark node 
    bot_gtk_param_widget_add_buttons (self->pw, PARAM_SELECT_LANDMARK_REGION, NULL);  

    //select the figure node 
    bot_gtk_param_widget_add_buttons (self->pw, PARAM_SELECT_FIGURE_REGION, NULL);  

    // BUTTON - Clears Current Selection
    //bot_gtk_param_widget_add_buttons(self->pw, PARAM_CLEAR_SELECTION, NULL);

    //we should make this a drop down ?? 
    //bot_gtk_param_widget_add_text_entry(self->pw, PARAM_SPATIAL_RELATION, BOT_GTK_PARAM_WIDGET_ENTRY, "Insert Spatial Relation");
    bot_gtk_param_widget_add_enumv(self->pw, PARAM_SPATIAL_RELATION, BOT_GTK_PARAM_WIDGET_DEFAULTS, 0, self->spatial_relations.size(), (const char **) self->sr_description, self->sr_id);

    // Saves the current selection
    bot_gtk_param_widget_add_buttons (self->pw, PARAM_SAVE_LANGUAGE, PARAM_CLEAR_SELECTION, NULL);

    
                                  
    /*bot_gtk_param_widget_add_separator(self->pw, "Graph Annotation");
    // BUTTON - Create new file to write language labels

    bot_gtk_param_widget_add_text_entry(self->pw, PARAM_NODE_ANNOTATION, BOT_GTK_PARAM_WIDGET_ENTRY, "Insert Particle Annotation Here");

    bot_gtk_param_widget_add_separator(self->pw, "Ground Truth");
    // BUTTON - Begins annotation selections
    bot_gtk_param_widget_add_buttons (self->pw, PARAM_BEGIN_ANNOTATION, NULL);   

    // BUTTON - Saves annotations
    bot_gtk_param_widget_add_buttons (self->pw, PARAM_SAVE_SELECTION, NULL);

    bot_gtk_param_widget_add_separator(self->pw, "Result");

    // BUTTON - Begins annotation selections
    bot_gtk_param_widget_add_buttons (self->pw, PARAM_BEGIN_RESULT_ANNOTATION, NULL);   

    // BUTTON - Saves annotations
    bot_gtk_param_widget_add_buttons (self->pw, PARAM_SAVE_RESULT_SELECTION, NULL);

    bot_gtk_param_widget_add_separator(self->pw, "");
    // BUTTON - Clears Current Selection
    bot_gtk_param_widget_add_buttons(self->pw, PARAM_CLEAR_SELECTION, NULL);

    // BUTTON - Clears all annotations
    bot_gtk_param_widget_add_buttons(self->pw, PARAM_CLEAR_ALL, NULL);*/
    
    bot_gtk_param_widget_add_separator(self->pw, "Semantic Classifications");

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0, 
                                       PARAM_DRAW_LABEL_NAME, 0, NULL);
    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0, 
                                       PARAM_DRAW_ALL_MAX_LABELS, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0, 
                                       PARAM_DRAW_LEGAND, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0, 
                                       PARAM_DRAW_STATS, 0, NULL);


    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0,
                                       PARAM_DRAW_REGION_LABEL, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0,
                                       PARAM_DRAW_REGION_LABEL_MAX, 0, NULL);


    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0,
                                       PARAM_DRAW_REGION_SEMANTICS, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0,
                                       PARAM_DRAW_REGION_SEMANTICS_MAX, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0,
                                       PARAM_DRAW_LASER_CLASSIFICATION, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0,
                                       PARAM_DRAW_LASER_CLASSIFICATION_MAX, 0, NULL);
                                       
    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0,
                                       PARAM_DRAW_IMAGE_CLASSIFICATION, 0, NULL);
    
    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0,
                                       PARAM_DRAW_IMAGE_CLASSIFICATION_MAX, 0, NULL);
    
    /*bot_gtk_param_widget_add_booleans (self->pw, 
      0,
      PARAM_DRAW_CLASSIFICATION_TEXT, 0, NULL);*/
    
    bot_gtk_param_widget_add_separator(self->pw, "Graph Info");

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0,
                                       PARAM_DISP_PROB, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0,
                                       PARAM_DRAW_LOG, 0, NULL);
                                       
    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0,
                                       PARAM_BOUNDING_BOXES, 0, NULL);
    
    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0,
                                       PARAM_DRAW_COV, 0, NULL);
    
    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0,
                                       PARAM_DRAW_REGION_ID, 0, NULL);
    
    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0,
                                       PARAM_DRAW_SEGMENT_ID, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0,
                                       PARAM_DRAW_NODE_ID, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0,
                                       PARAM_DRAW_DEAD_EDGES, 0, NULL);

    
    bot_gtk_param_widget_add_booleans ( self->pw, (BotGtkParamWidgetUIHint) 0,
                                        PARAM_DRAW_ODOM, 1, NULL);

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0,
                                       PARAM_REMAP_REGIONS, 1, NULL);

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       (BotGtkParamWidgetUIHint) 0,
                                       PARAM_COLOR_REGIONS, 1, NULL);    

    bot_gtk_param_widget_add_int(self->pw,PARAM_EDGE_THICKNESS, 
                                 BOT_GTK_PARAM_WIDGET_SLIDER, 0 , 20 , 1, 4);
    
    bot_gtk_param_widget_add_double (self->pw, 
                                     PARAM_NODE_RADIUS,
                                     (BotGtkParamWidgetUIHint) 0, 0.1, 2.0, 0.05, 0.25);

    self->selected_nodes = g_hash_table_new(g_int_hash, g_int_equal);

    slam_graph_region_particle_list_t_subscribe(self->lcm, "REGION_PARTICLE_ISAM_RESULT", on_topo_graph, self);
    slam_language_annotation_t_subscribe(self->lcm, "LANGUAGE_ANNOTATION", on_lang_annotation, self);

    bot_core_pose_t_subscribe (self->lcm, "POSE", on_pose, self);

    self->ehandler.name = (char*)RENDERER_NAME;
    self->ehandler.enabled = 1;
    self->ehandler.mouse_press = mouse_press;
    self->ehandler.user = self;
    
    bot_viewer_add_event_handler(viewer, &self->ehandler, priority);
    //tells us when to dump the old buffer
    slam_status_t_subscribe(self->lcm, "SLAM_STATUS", on_slam_status, self);
    self->lang_filename = NULL;
    self->annotation_filename = NULL;
    self->node_scans = g_hash_table_new(g_int_hash, g_int_equal);
    self->active = 0;
    self->active_particle = -1;
    self->no_particles = 0;
    self->node_1 = -1;
    self->node_2 = -1;
    self->diff_particle = NULL;
    self->params.draw_diff = 0;
    self->current_ind = -1;
    self->diff_utime = 0;
    self->diff_iter = 0;

    self->node_selection_mode = SELECT_NONE;

    return self;
}

extern "C" void
setup_renderer_language_annotation (BotViewer *viewer, int priority, BotParam * param)
{
    RendererLanguageAnnotation *self = renderer_language_annotation_new (viewer, priority, param);
    bot_viewer_add_renderer (viewer, &self->renderer, priority);
}


