/**
 * @file main.cpp
 * @brief Multiple relative pose graphs with relative constraints.
 * @author Sachithra Hemachandra, Mathew Walter, Bianca Homeburg, Stefanie Tellex
 * @version 
 *
 * Copyright (C) 2009-2012 Massachusetts Institute of Technology.
 *
 */

#include "SlamParticle.hpp"
#include <iostream>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

#include <opencv/highgui.h>
#include <getopt.h>

#include <lcm/lcm.h>
#include <bot_core/bot_core.h>
//bot param stuff
#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_lcmgl_client/lcmgl.h>

#include <gsl/gsl_blas.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_sf_bessel.h>
#include <gsl/gsl_integration.h>
#include <math.h>

#include  <classify-semantic/classify_semantic.hpp>
#include <scanmatch/ScanMatcher.hpp>
#include <lcmtypes/sm_rigid_transform_2d_t.h>
#include <bot_frames/bot_frames.h>
#include <lcmtypes/er_lcmtypes.h>

#include <isam/isam.h>
#include <isam/Anchor.h>

#include <occ_map/PixelMap.hpp>
#include <lcmtypes/slam_lcmtypes.h>
#include <lcmtypes/perception_object_detection_list_t.h>
//#include "RegionSlam.h"
#include "NodeScan.h"
#include <lcmtypes/slam_lcmtypes.h>
#include "SlamGraph.hpp"


#include <door_detector_laser/door_detect_laser.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif 

using namespace scanmatch;
using namespace std;
using namespace isam;
using namespace Eigen;

//add a new node every this many meters
#define LINEAR_DIST_TO_ADD 1.0//1.0//0.5//1.0
#define ANGULAR_DIST_TO_ADD 1.0//M_PI / 180 * 60
#define MAP_PUBLISH_PERIOD 5.0 //s
#define LASER_DATA_CIRC_SIZE 100
#define IMAGE_DATA_CIRC_SIZE 80
#define MAX_LASERS_TO_COPY_FROM_QUEUE 3

typedef enum { print_node_info, print_particle_info, print_language_info} verbose_level_t;

typedef struct _laser_odom_t{
    //Pose2d *odom; 
    sm_rigid_transform_2d_t *odom;
    bot_core_planar_lidar_t *laser;
    bot_core_planar_lidar_t *rlaser;
    bot_core_planar_lidar_t *full_laser;
    bot_core_image_t *image;
    
    classification_result_t *laser_result;
    classification_result_t *image_result;

    int force_supernode;
} laser_odom_t; 

/*class ComplexLanguageEvent{
public:
    int language_event_id; 
    int64_t node_id;
    slam_language_label_t *label;
    int no_of_new_regions_added;    
    vector<int64_t> tested_nodes; 
    };*/

struct region_mapping_t {
    int particle_id; //id of the unique particle 
    map<int, int> region_map;
};


typedef struct _received_language_t{
    int64_t node_id;
    slam_language_label_t *label;
} received_language_t;    

struct app_t{
    lcm_t * lcm;
    //RegionSlam * regionslam;
    ScanMatcher *sm;
    ScanMatcher *sm_incremental;
    ScanMatcher *sm_loop;
    ScanMatcher *sm_lc_low; //low resolution scanmatcher to use to get a good startoff point

    int mode;
    BotFrames *frames;
    BotParam *param;
    bot_lcmgl_t * lcmgl_basic;
    bot_lcmgl_t * lcmgl_frame_test;
    bot_lcmgl_t * lcmgl_debug;
    bot_lcmgl_t * lcmgl_covariance;
    bot_lcmgl_t * lcmgl_particles;
    bot_lcmgl_t * lcmgl_sm_basic;
    bot_lcmgl_t * lcmgl_sm_graph;
    bot_lcmgl_t * lcmgl_sm_prior;
    bot_lcmgl_t * lcmgl_sm_result;
    bot_lcmgl_t * lcmgl_sm_result_low;
    bot_lcmgl_t * lcmgl_loop_closures;
    slam_graph_weight_list_t *weights;
    semantic_classifier_t *classifier;
    
    int requested_map_particle_id; 
    LabelInfo *label_info; 
    LabelInfo *label_info_particles; 
    
    full_laser_state_t *full_laser;
    //arguments for isam-region
    int ignoreLanguage;
    bool doSemanticClassification;
    bool useLowResPrior;
    bool finishSlam; //for now this will only force us to scanmatch ther current region 
    int useSMCov; 
    int scanmatchBeforeAdd;
    int probMode;
    int useCopy;
    bool useOnlyRegion;
    bool addDummyConstraints;
    bool useICP;
    bool copyNodeScans;
    int noScanTransformCheck;
    double odometryConfidence;
    slam_pixel_map_request_t *map_request; 
    slam_pixel_map_request_t *scan_request; 
    //bool useSpectralClustering;
    int segmentationMethod; // 0 - Fixed distance; 1 - RSS segmentation method; 2 - Fixed Spectral clustering 
    int publish_map;
    int useOdom; 
    char * chan;
    char * rchan;
    bool skipOutsideSM;
    int draw_scanmatch; 
    int draw_map;
    int keepDeadEdges;
    int publish_occupancy_maps;
    
    int complex_language_index; 

    vector<slam_language_answer_t *> slam_sr_answers; //prob shouldn't grow more than one 

    vector<complex_language_event_t *> outstanding_complex_language; 

    vector<complex_language_event_t *> failed_complex_language; 

    vector<complex_language_event_t *> grounded_language; 

    vector<received_language_t *> grounded_simple_language; 

    vector<RawDetection *> outstanding_object_detections; 

    vector<ObjectDetection *> object_detections; 
    map<int, ObjectDetection *> object_map; 

    map<int, region_mapping_t> particle_to_region_map;
    map<int, set<int> > unique_particle_to_duplicates;  //the mapping from unique particle to duplicates  
    //used for grounding language for the duplicate particles 
    
    int64_t last_node_utime; 
    int64_t last_graph_utime; //update from particle last_node_utime

    //parameters for the algorithm
    int64_t publish_occupancy_maps_last_utime;
    int64_t last_laser_reading_utime;
    double sm_acceptance_threshold;
    int max_prob_particle_id;

    object_door_list_t *door_list; 
    slam_init_node_t *node_init;
    int beam_skip; //downsample ranges by only taking 1 out of every beam_skip points
    double spatialDecimationThresh; //don't discard a point if its range is more than this many std devs from the mean range (end of hallway)
    double maxRange; //discard beams with reading further than this value
    double maxUsableRange; //only draw map out to this far for MaxRanges...
    float validBeamAngles[2]; //valid part of the field of view of the laser in radians, 0 is the center beam
    Scan * last_scan;
    set<pair<int, int> > *dist_lc, *lang_lc;
    sm_rigid_transform_2d_t * prev_odom; //odometry computed outside module
    sm_rigid_transform_2d_t * prev_added_odom; //odometry computed outside module
    sm_rigid_transform_2d_t * prev_sm_odom; //internally scan matched odometry
    int resample;
    int verbose;
    verbose_level_t verbose_level;
    int slave_mode;
    int use_doorways;
    vector<slam_region_transition_t *> transitions;

    BotPtrCircular   *front_laser_circ;
    BotPtrCircular   *rear_laser_circ;

    BotPtrCircular   *full_laser_circ;
    BotPtrCircular   *image_circ;
    
    //when a new init message is heard - a scan is added to the hash table - and also 
    //a scan match is done with the previous node 
    //the constraint is added to the relavent nodes - for reuse     
    GHashTable *slam_nodes;

    vector<laser_odom_t *> lasers_to_add;
    vector<NodeScan *> slam_nodes_to_add;

    vector<NodeScan *> all_slam_nodes;

    vector<NodeScan *> added_slam_nodes;

    vector<slam_language_edge_t *> language_edges_to_add;
    vector<slam_dirichlet_update_t *> dirichlet_updates_to_add;
    vector<slam_language_label_t *> language_labels_to_add;

    vector<slam_slu_parse_language_querry_t *> complex_language_labels_to_add;

    vector<perception_object_detection_list_t *> new_object_detections; 
    int got_label;

    vector<SlamParticle *> slam_particles;
    map<int, SlamParticle *> slam_particles_map;
    NodeScan * last_slampose;

    int waiting_for_slu; 
    //we should only get one?? 
    //this should be deprecated 
    vector<slam_slu_result_list_t *> slu_result; 
    //this is the new one 
    vector<slam_complex_language_result_list_t *> slu_complex_result; 
    int last_slampose_ind;
    int last_slampose_for_process_ind;
    int node_count;
    int bad_mode;

    int particle_id_to_train;

    SlamGraph *basic_graph; //a slam graph that doesn't have loop closure constraints - used for scan matching 

    bot_core_planar_lidar_t * r_laser_msg;
    bot_core_pose_t *last_deadreakon; 
    int64_t r_utime;

    int64_t last_node_sent_utime;

    pthread_t  scanmatch_thread;
    pthread_t  laser_process_thread;

    int no_particles;

    GMutex *mutex;
    GMutex *mutex_language;
    GMutex *mutex_particles;
    GMutex *mutex_laser_queue;
    GMutex *mutex_lasers;
    GMutex *mutex_objects;

    int64_t last_graph_processed_time;

    int use_frames; 
    int region;
    
    ConfigurationParams config_params;

    int next_id;
    int find_ground_truth;
    FloatPixelMap *local_px_map; // Local occupancy map used for measurement likelihood
};

//USED
//Finds the closest rear laser message to give utime (normally from the front laser)
bot_core_planar_lidar_t *get_closest_rear_laser(app_t *app, int64_t utime);

bot_core_image_t *get_closest_image(app_t *app, int64_t utime);

bot_core_planar_lidar_t *get_closest_full_laser(app_t *app, int64_t utime);

//USED
// Publishes occupancy grid maps for the most likely particle
//Done based on interaction from the viewer (so when we get a request for a particular map 
//publish that out
void publish_occupancy_maps (app_t *app) {

    // Determine most-likely particle id
    int max_particle_idx = 0;
    double weight_max = 0;

    for (int i=0; i < app->slam_particles.size(); i++) {
        SlamParticle *particle = app->slam_particles.at(i);

        if (particle->normalized_weight > weight_max) {
            weight_max = particle->normalized_weight;
            max_particle_idx = i;
        }
    }

    SlamParticle *particle = app->slam_particles.at(max_particle_idx);
    
    particle->publishOccupancyMap();
}

//this will return a list of unique particles and also update the mapping between the unique particle and 
//the duplicate (region ids) 
vector<SlamParticle *> get_unique_particles(app_t *app){
    vector<SlamParticle *> unique_particles;

    app->particle_to_region_map.clear();
    app->unique_particle_to_duplicates.clear();

    for(int i=0; i < app->slam_particles.size(); i++){
        SlamParticle *part1 = app->slam_particles.at(i);

        //bool found_match = false;
        SlamParticle *match = NULL;
        region_mapping_t r_map;

        for(int j=0; j < unique_particles.size(); j++){
             SlamParticle *part2 = unique_particles.at(j);
             
             //map<int, int> region_map;
             
             bool part_match = part2->compareToParticle(part1, r_map.region_map);

             //we should save the region map - for processing later 
             
             if(part_match){ //part1->compareToParticle(part2)){
                 match = part2;
                 //found_match = true;
                 r_map.particle_id = part2->graph_id;
                 //insert to the match 
                 break;
             }
             else{
                 r_map.region_map.clear();
             }
        }

        if(!match){
            unique_particles.push_back(part1);
            set<int> matches; 
            app->unique_particle_to_duplicates.insert(make_pair(part1->graph_id, matches));
        }
        else{
            //app->unique_particle_to_duplicates.in
            map<int, set<int> >::iterator it_matches;
            it_matches = app->unique_particle_to_duplicates.find(match->graph_id);
            assert(it_matches != app->unique_particle_to_duplicates.end());
            it_matches->second.insert(part1->graph_id);
            app->particle_to_region_map.insert(make_pair(part1->graph_id, r_map));
        }
    }
    
    fprintf(stderr, "\n\n============ Number of unique particles : %d\n",(int) unique_particles.size()); 

    return unique_particles;
}

void publish_slam_progress(app_t *self){
    slam_performance_t msg;
    msg.utime = bot_timestamp_now();
    msg.last_sensor_utime = self->last_laser_reading_utime; 
    msg.last_node_utime = self->last_node_utime; 
    msg.last_node_in_graph_utime = self->last_graph_utime; 

    slam_performance_t_publish(self->lcm, "SLAM_PROGRESS", &msg);
}

void print_region_to_region_mapping(region_mapping_t &mapping){
    map<int, int>::iterator it; 
    
    for(it = mapping.region_map.begin(); it != mapping.region_map.end(); it++){
        fprintf(stderr, "Region %d => %d\n", it->first, it->second);
    }    
}

void adjust_language_result_from_unique(slam_complex_language_result_t *result, region_mapping_t &mapping){
    //search thrugh the result and make the changes to region ids
    map<int, int>::iterator it;
    for(int i=0; i < result->count; i++){
        slam_complex_language_path_result_list_t *result_list = &result->language[i];
        for(int k=0; k < result_list->no_paths; k++){
            slam_complex_language_path_result_t *path = &result_list->results[k];
            it = mapping.region_map.find(path->region_id);
            if(it != mapping.region_map.end()){
                path->region_id = it->second;
            }
            else{
                fprintf(stderr, "Region mapping not found for region : %d\n", (int) path->region_id);

                //maybe print the found mapping?? 
                exit(-1);
            }
            for(int j=0; j < path->no_landmarks; j++){
                slam_slu_landmark_result_t *lm_result = &path->landmark_results[j];
            
                it = mapping.region_map.find(lm_result->landmark_id);
                if(it != mapping.region_map.end()){
                    fprintf(stderr, "Remapping LM : %d - %d\n", (int) lm_result->landmark_id, (int) it->second); 
                    lm_result->landmark_id = it->second;
                    
                }
                else{
                    fprintf(stderr, "Trying to remap particle : %d => Particle : %d\n", (int) result->particle_id, mapping.particle_id);
                    fprintf(stderr, "Region mapping not found : %d\n", (int) lm_result->landmark_id);
                    print_region_to_region_mapping(mapping);
                    exit(-1);
                }
            }
        }
    }
}

void count_different_particles(app_t *app){
    set<nodePairKey> different_particles;
    set<nodePairKey>::iterator it;
    int count = 0;
    for(int i=0; i < app->slam_particles.size(); i++){
        SlamParticle *part1 = app->slam_particles.at(i);
        for(int j=0; j < app->slam_particles.size(); j++){
            if(i == j)
                continue;
            SlamParticle *part2 = app->slam_particles.at(i);

            if( (different_particles.find(make_pair(part1->graph_id, part2->graph_id))== different_particles.end())
                && (different_particles.find(make_pair(part2->graph_id, part1->graph_id))== different_particles.end())){
                if(!part1->compareToParticle(part2)){
                    different_particles.insert(make_pair(part1->graph_id, part2->graph_id));
                }
            }
        }
    }

    fprintf(stderr, "\n\n============ Number of different particles : %d\n",(int) different_particles.size()); 
}

//this should be done in an intelligent manner - after querrying if we have any outstanding language 
int publish_outstanding_language_querries(app_t *app){
    //check if there is any outstanding language 
    
    bool outstanding = false;

    for(int i=0; i < app->slam_particles.size(); i++){
        SlamParticle *part = app->slam_particles.at(i);
        if(part->hasOutstandingLanguage()){
            outstanding = true;
            break;
        }
    }

    if(!outstanding){
        fprintf(stderr, "No outstanding language\n");
        return 0;    
    }

    vector<SlamParticle *> unique_particles = get_unique_particles(app);

    //we should send this out now and 
    //count_different_particles(app);

    fprintf(stderr, "+++++++++++ Sending Outstanding language querries ++++++++++++\n");

    slam_complex_language_collection_list_t *msg = (slam_complex_language_collection_list_t *) calloc(1, sizeof(slam_complex_language_collection_list_t));
    msg->utime = bot_timestamp_now();
    msg->no_particles = unique_particles.size();

    msg->collection = (slam_complex_language_collection_t *) calloc(msg->no_particles, sizeof(slam_complex_language_collection_t));
    
    for(int i=0; i < unique_particles.size(); i++){
        SlamParticle *part = unique_particles.at(i);
        part->fillOutstandingLanguageCollection(msg->collection[i]);
    }
    slam_complex_language_collection_list_t_publish(app->lcm, "SLU_COMPLEX_LANGUAGE_QUERRY", msg);
    slam_complex_language_collection_list_t_destroy(msg);
    
    return 1;
}

//this should be done in an intelligent manner - after querrying if we have any outstanding language 
int publish_outstanding_language_querries_all_particles(app_t *app){
    //check if there is any outstanding language 
    
    //bool outstanding = false;

    vector<SlamParticle *> outstanding_particles;
    //this should actually only send out querries for particles that have querries 
    for(int i=0; i < app->slam_particles.size(); i++){
        SlamParticle *part = app->slam_particles.at(i);
        if(part->hasOutstandingLanguage()){
            //outstanding = true;
            //break;
            outstanding_particles.push_back(part);
        }
    }

    if(outstanding_particles.size() == 0){//!outstanding){
        fprintf(stderr, "No outstanding language\n");
        return 0;    
    }

    //we should send this out now and 
    //count_different_particles(app);

    fprintf(stderr, "+++++++++++ Sending Outstanding language querries ++++++++++++\n");

    slam_complex_language_collection_list_t *msg = (slam_complex_language_collection_list_t *) calloc(1, sizeof(slam_complex_language_collection_list_t));
    msg->utime = bot_timestamp_now();
    msg->no_particles = (int) outstanding_particles.size();

    msg->collection = (slam_complex_language_collection_t *) calloc(msg->no_particles, sizeof(slam_complex_language_collection_t));
    
    for(int i=0; i < outstanding_particles.size(); i++){
        SlamParticle *part = outstanding_particles.at(i);
        part->fillOutstandingLanguageCollection(msg->collection[i]);
    }
    slam_complex_language_collection_list_t_publish(app->lcm, "SLU_COMPLEX_LANGUAGE_QUERRY", msg);
    slam_complex_language_collection_list_t_destroy(msg);
    
    return 1;
}

void publish_language_collection_list(app_t *app){
    bool has_language = false;

    for(int i=0; i < app->slam_particles.size(); i++){
        SlamParticle *part = app->slam_particles.at(i);
        if(part->hasLanguagePaths()){
            has_language = true;
            break;
        }
    }

    if(!has_language)
        return;    

    slam_complex_language_collection_list_t *msg = (slam_complex_language_collection_list_t *) calloc(1, sizeof(slam_complex_language_collection_list_t));
    msg->utime = bot_timestamp_now();
    msg->no_particles = app->slam_particles.size();
    msg->collection = (slam_complex_language_collection_t *) calloc(msg->no_particles, sizeof(slam_complex_language_collection_t));
    
    for(int i=0; i < app->slam_particles.size(); i++){
        SlamParticle *part = app->slam_particles.at(i);
        part->fillLanguageCollection(msg->collection[i]);
    }
    slam_complex_language_collection_list_t_publish(app->lcm, "COMPLEX_LANGUAGE_PATHS", msg);
    slam_complex_language_collection_list_t_destroy(msg);
}

//get the params for the RSS/IJRR work
ConfigurationParams getFixedRegionParams(app_t *app){
    ConfigurationParams param; 
    //----------- Active ------------//
    //---------- Semantic Information ------------//
    if(app->doSemanticClassification){
        param.useLaserForSemantic = true;
        param.useImageForSemantic = true;//false;
        param.useFactorGraphs = true;
        param.bleedLabelsToRegions = false;
    }
    else{
        param.useLaserForSemantic = false;
        param.useImageForSemantic = false;//false;
        param.useFactorGraphs = false;
        param.bleedLabelsToRegions = true;
    }

    //--------- Language grounding -------//
    param.sendUniqueParticles = true; 
    param.askQuestions = false;
    param.alwaysGround = false; //should be deprecated 
    param.gMode = FINISH_ON_GOOD;
    
    param.findShortestPathEveryTime = false;
    param.checkCurrentRegionForGrounding = false; //turn this to true if you want to have the current unformed region checked for grounding
    param.minRegionSizeBeforeLanguage = 4;
    //how the region polygon is extracted
    param.polygonMode = 4; //0 - use the mean node's laser points, 1 - use the region's convex hull, 2 - ray trace from the region and use those laser points

    //---------- Edge Proposals ------------//
    param.ignoreLargeScanmatchChanges = true;
    param.proposeSeperateSemanticEdges = true;
    param.noRegionEdgesToSample = 4; //3
    param.useSemanticsForDistanceEdge = false;
    //use 80 for the Killian court dataset - for large errors 
    param.regionLanguageIgnoreDist = 160;//80; //was using 20 - not sure which one to use 
    //seems like both of them are used at different places - check which one to keep 
    param.regionIgnoreDist = 10;//20; //was using 20 - not sure which one to use 
    param.maxDistForLC = 10.0;
    param.scanmatchBeforeAdd = app->scanmatchBeforeAdd;
    
    //---------- Scanmatching ----------------//
    param.noScanTransformCheck = false;
    param.useScanmatcherUncertainity = true; //for LC 
    param.useSMCov = false;
    param.smAcceptanceThreshold = 0.75;//0.75;//0.6 //0.7;//0.8; //0.7 - can create some bad scanmatches 
    param.incSMAcceptanceThreshold = 0.7;//0.7;//0.7;//0.5; 
    param.langSMAcceptanceThreshold = 0.55;
    param.smCloseAcceptanceThreshold = 0.5;
    param.scanmatchWithCurrentRegion = false; 
    param.minRegionSizeBeforeScanmatch = 2;

    //---------- Visualization-------------------//
    param.clearDeadEdges = !app->addDummyConstraints;
    param.drawScanmatch = app->draw_scanmatch;
    param.addDummyConstraints = true;

    //---------- Verbosity -------------//
    param.printDistanceScanmatch = true;
    param.printLanguageScanmatch = true;
    param.printRegionCreation = false;
    param.printGraphCreation = false;
    param.verbose = false;

    //-------- PofZ method ----//
    param.calculateRegionProb = false;//true;

    //-------- Region Merging ----------//
    param.mergeRegions = false;
    param.mergeRegionsOnOverlap = false;
    param.mergeRegionsOnCloseNodePairs = false;
    param.proposeEdgesFromCurrentRegion = false; //set this to true if you wish to propose edges from the current region 

    //-------- Segmentation--------------//
    param.segmentSplitThreshold = 0.5;
    param.fixedRegionDistanceThreshold = 6.0;
    //if the following are both false - it uses the fixedDistanceThreshold - should make this an eum type
    param.useSpectralFixed = false;
    param.spectralClustering = false;
    

    //---------- Unused ----------//
    param.segmentationMethod = 0;
    param.segmentBasedOnOverlap = true; //otherwise - will always segment - and then rely on the merging to merge regions 
    param.scanmatchOnSegmentEvent = true; //propose edges only when a basic segmentation (which creates a new RegionSegment) happens 
    param.countForBatchOptimize=1;
    param.useICP = app->useICP; 
    param.skipCloseNodes = true; 

    //----- Deprecated ------ //
    param.scanmatchOnlyRegion = app->useOnlyRegion;
    param.skipOutsideSM = app->skipOutsideSM; //skips regions deemed to be outside 
    param.doWideMatchOnFail = false;
    param.useLowResPrior = app->useLowResPrior;
    param.strictScanmatch = false;
    param.useBasicGraph = false;

    return param;
}

//get the params for the ICRA work
ConfigurationParams getSpectralRegionParamsICRA(app_t *app){
    ConfigurationParams param; 
    //----------- Active ------------//
    //---------- Semantic Information ------------//
    if(app->doSemanticClassification){
        param.useLaserForSemantic = true;
        param.useImageForSemantic = true;
        param.useFactorGraphs = true;
        param.bleedLabelsToRegions = false;
    }
    else{
        param.useLaserForSemantic = false;
        param.useImageForSemantic = false;//false;
        param.useFactorGraphs = false;
        param.bleedLabelsToRegions = true;
    }

    //how the region polygon is extracted
    param.polygonMode = 4; //0 - use the mean node's laser points, 1 - use the region's convex hull, 2 - ray trace from the region and use those laser points
    
    //--------- Language grounding -------//
    param.sendUniqueParticles = false; 
    param.askQuestions = false;
    param.alwaysGround = false;
    param.gMode = GROUND_ONCE;
    param.findShortestPathEveryTime = false;
    param.checkCurrentRegionForGrounding = false; //turn this to true if you want to have the current unformed region checked for grounding
    param.minRegionSizeBeforeLanguage = 4;
    //---------- Edge Proposals ------------//
    param.ignoreLargeScanmatchChanges = false;
    param.proposeSeperateSemanticEdges = false;
    param.noRegionEdgesToSample = 4; //3
    param.regionLanguageIgnoreDist = 80;//160; //was using 20 - not sure which one to use 
    //seems like both of them are used at different places - check which one to keep 
    param.regionIgnoreDist = 20; //was using 20 - not sure which one to use 
    param.maxDistForLC = 10.0;
    param.scanmatchBeforeAdd = app->scanmatchBeforeAdd;
    param.useSemanticsForDistanceEdge = true;
    //---------- Scanmatching ----------------//
    param.noScanTransformCheck = false;
    param.useScanmatcherUncertainity = true; //for LC 
    param.useSMCov = false;
    param.smAcceptanceThreshold = 0.75;//0.75;//0.6 //0.7;//0.8; //0.7 - can create some bad scanmatches 
    param.incSMAcceptanceThreshold = 0.7;//0.7;//0.7;//0.5; 
    param.langSMAcceptanceThreshold = 0.55;
    param.smCloseAcceptanceThreshold = 0.5;
    param.scanmatchWithCurrentRegion = false; 
    param.minRegionSizeBeforeScanmatch = 2;

    //---------- Visualization-------------------//
    param.clearDeadEdges = !app->addDummyConstraints;
    param.drawScanmatch = app->draw_scanmatch;
    param.addDummyConstraints = true;

    //---------- Verbosity -------------//
    param.printDistanceScanmatch = false;
    param.printLanguageScanmatch = false;
    param.printRegionCreation = false;
    param.printGraphCreation = false;
    param.verbose = false;

    //------ PofZ method -----//
    param.calculateRegionProb = false;

    //-------- Region Merging ----------//
    param.mergeRegions = true;
    param.mergeRegionsOnOverlap = false;
    param.mergeRegionsOnCloseNodePairs = true;
    param.proposeEdgesFromCurrentRegion = false; //set this to true if you wish to propose edges from the current region 

    //-------- Segmentation--------------//
    param.segmentSplitThreshold = 0.5;
    param.fixedRegionDistanceThreshold = 6.0;
    //if the following are both false - it uses the fixedDistanceThreshold - should make this an eum type
    param.useSpectralFixed = false;
    param.spectralClustering = true;
    

    //---------- Unused ----------//
    param.segmentationMethod = 0;
    param.segmentBasedOnOverlap = true; //otherwise - will always segment - and then rely on the merging to merge regions 
    param.scanmatchOnSegmentEvent = true; //propose edges only when a basic segmentation (which creates a new RegionSegment) happens 
    param.countForBatchOptimize=1;
    param.useICP = app->useICP; 
    param.skipCloseNodes = true; 

    //----- Deprecated ------ //
    param.scanmatchOnlyRegion = app->useOnlyRegion;
    param.skipOutsideSM = app->skipOutsideSM; //skips regions deemed to be outside 

    param.doWideMatchOnFail = false;
    param.useLowResPrior = app->useLowResPrior;
    param.strictScanmatch = false;
    param.useBasicGraph = false;
    

    return param;
}

//get the params for the post-icra work - with continuous grounding
ConfigurationParams getSpectralRegionParams(app_t *app){
    ConfigurationParams param; 
    //----------- Active ------------//
    //---------- Semantic Information ------------//
    if(app->doSemanticClassification){
        param.useLaserForSemantic = true;
        param.useImageForSemantic = true;
        param.useFactorGraphs = true;
        param.bleedLabelsToRegions = false;
    }
    else{
        param.useLaserForSemantic = false;
        param.useImageForSemantic = false;//false;
        param.useFactorGraphs = false;
        param.bleedLabelsToRegions = true;
    }

    //how the region polygon is extracted
    param.polygonMode = 4; //0 - use the mean node's laser points, 1 - use the region's convex hull, 2 - ray trace from the region and use those laser points
    
    //--------- Language grounding -------//
    param.sendUniqueParticles = false; 
    param.askQuestions = true;
    param.alwaysGround = true;
    param.gMode = ALWAYS_GROUND;
    param.findShortestPathEveryTime = true;
    param.checkCurrentRegionForGrounding = false; //turn this to true if you want to have the current unformed region checked for grounding
    param.minRegionSizeBeforeLanguage = 4;
    //---------- Edge Proposals ------------//
    param.ignoreLargeScanmatchChanges = false;
    param.proposeSeperateSemanticEdges = false;
    param.noRegionEdgesToSample = 4; //3
    param.regionLanguageIgnoreDist = 80;//160; //was using 20 - not sure which one to use 
    //seems like both of them are used at different places - check which one to keep 
    param.regionIgnoreDist = 20; //was using 20 - not sure which one to use 
    param.maxDistForLC = 10.0;
    param.scanmatchBeforeAdd = app->scanmatchBeforeAdd;
    param.useSemanticsForDistanceEdge = true;
    //---------- Scanmatching ----------------//
    param.noScanTransformCheck = false;
    param.useScanmatcherUncertainity = true; //for LC 
    param.useSMCov = false;
    param.smAcceptanceThreshold = 0.75;//0.75;//0.6 //0.7;//0.8; //0.7 - can create some bad scanmatches 
    param.incSMAcceptanceThreshold = 0.7;//0.7;//0.7;//0.5; 
    param.langSMAcceptanceThreshold = 0.55;
    param.smCloseAcceptanceThreshold = 0.5;
    param.scanmatchWithCurrentRegion = false; 
    param.minRegionSizeBeforeScanmatch = 2;

    //---------- Visualization-------------------//
    param.clearDeadEdges = !app->addDummyConstraints;
    param.drawScanmatch = app->draw_scanmatch;
    param.addDummyConstraints = true;

    //---------- Verbosity -------------//
    param.printDistanceScanmatch = false;
    param.printLanguageScanmatch = false;
    param.printRegionCreation = false;
    param.printGraphCreation = false;
    param.verbose = false;

    //------ PofZ method -----//
    param.calculateRegionProb = false;

    //-------- Region Merging ----------//
    param.mergeRegions = true;
    param.mergeRegionsOnOverlap = false;
    param.mergeRegionsOnCloseNodePairs = true;
    param.proposeEdgesFromCurrentRegion = false; //set this to true if you wish to propose edges from the current region 

    //-------- Segmentation--------------//
    param.segmentSplitThreshold = 0.5;
    param.fixedRegionDistanceThreshold = 6.0;
    //if the following are both false - it uses the fixedDistanceThreshold - should make this an eum type
    param.useSpectralFixed = false;
    param.spectralClustering = true;
    

    //---------- Unused ----------//
    param.segmentationMethod = 0;
    param.segmentBasedOnOverlap = true; //otherwise - will always segment - and then rely on the merging to merge regions 
    param.scanmatchOnSegmentEvent = true; //propose edges only when a basic segmentation (which creates a new RegionSegment) happens 
    param.countForBatchOptimize=1;
    param.useICP = app->useICP; 
    param.skipCloseNodes = true; 

    //----- Deprecated ------ //
    param.scanmatchOnlyRegion = app->useOnlyRegion;
    param.skipOutsideSM = app->skipOutsideSM; //skips regions deemed to be outside 

    param.doWideMatchOnFail = false;
    param.useLowResPrior = app->useLowResPrior;
    param.strictScanmatch = false;
    param.useBasicGraph = false;
    

    return param;
}

//get the params for the Fixed Spectral work
ConfigurationParams getFixedSpectralRegionParams(app_t *app){
    ConfigurationParams param; 
    //----------- Active ------------//
    //---------- Semantic Information ------------//
    if(app->doSemanticClassification){
        param.useLaserForSemantic = true;
        param.useImageForSemantic = true;//false;
        param.useFactorGraphs = true;
        param.bleedLabelsToRegions = false;
    }
    else{
        param.useLaserForSemantic = false;
        param.useImageForSemantic = false;//false;
        param.useFactorGraphs = false;
        param.bleedLabelsToRegions = true;
    }
    /*param.useLaserForSemantic = true;
    param.useImageForSemantic = true;//false;
    param.useFactorGraphs = true;
    param.bleedLabelsToRegions = false;*/
    //how the region polygon is extracted
    param.polygonMode = 4; //0 - use the mean node's laser points, 1 - use the region's convex hull, 2 - ray trace from the region and use those laser points
    
    //--------- Language grounding -------//
    param.sendUniqueParticles = false; 
    param.askQuestions = false;
    param.alwaysGround = false;
    param.gMode = GROUND_ONCE;
    param.findShortestPathEveryTime = true;
    param.checkCurrentRegionForGrounding = false; //turn this to true if you want to have the current unformed region checked for grounding
    param.minRegionSizeBeforeLanguage = 4;
    //---------- Edge Proposals ------------//
    param.ignoreLargeScanmatchChanges = false;
    param.proposeSeperateSemanticEdges = false;
    param.noRegionEdgesToSample = 4; //3
    param.regionLanguageIgnoreDist = 80;//160; //was using 20 - not sure which one to use 
    //seems like both of them are used at different places - check which one to keep 
    param.regionIgnoreDist = 20; //was using 20 - not sure which one to use 
    param.maxDistForLC = 10.0;
    param.scanmatchBeforeAdd = app->scanmatchBeforeAdd;
    param.useSemanticsForDistanceEdge = true;
    //---------- Scanmatching ----------------//
    param.noScanTransformCheck = false;
    param.useScanmatcherUncertainity = true; //for LC 
    param.useSMCov = false;
    param.smAcceptanceThreshold = 0.75;//0.75;//0.6 //0.7;//0.8; //0.7 - can create some bad scanmatches 
    param.incSMAcceptanceThreshold = 0.7;//0.7;//0.7;//0.5; 
    param.langSMAcceptanceThreshold = 0.55;
    param.smCloseAcceptanceThreshold = 0.5;
    param.scanmatchWithCurrentRegion = false; 
    param.minRegionSizeBeforeScanmatch = 2;

    //---------- Visualization-------------------//
    param.clearDeadEdges = !app->addDummyConstraints;
    param.drawScanmatch = app->draw_scanmatch;
    param.addDummyConstraints = true;

    //---------- Verbosity -------------//
    param.printDistanceScanmatch = true;
    param.printLanguageScanmatch = false;
    param.printRegionCreation = false;
    param.printGraphCreation = false;
    param.verbose = false;

    //------ PofZ method -----//
    param.calculateRegionProb = false;

    //-------- Region Merging ----------//
    param.mergeRegions = true;
    param.mergeRegionsOnOverlap = false;
    param.mergeRegionsOnCloseNodePairs = true;
    param.proposeEdgesFromCurrentRegion = false; //set this to true if you wish to propose edges from the current region 

    //-------- Segmentation--------------//
    param.segmentSplitThreshold = 0.5;
    param.fixedRegionDistanceThreshold = 6.0;
    //if the following are both false - it uses the fixedDistanceThreshold - should make this an eum type
    param.useSpectralFixed = true;
    param.spectralClustering = false;
    

    //---------- Unused ----------//
    param.segmentationMethod = 0;
    param.segmentBasedOnOverlap = true; //otherwise - will always segment - and then rely on the merging to merge regions 
    param.scanmatchOnSegmentEvent = true; //propose edges only when a basic segmentation (which creates a new RegionSegment) happens 
    param.countForBatchOptimize=1;
    param.useICP = app->useICP; 
    param.skipCloseNodes = true; 

    //----- Deprecated ------ //
    param.scanmatchOnlyRegion = app->useOnlyRegion;
    param.skipOutsideSM = app->skipOutsideSM; //skips regions deemed to be outside 
    param.doWideMatchOnFail = false;
    param.useLowResPrior = app->useLowResPrior;
    param.strictScanmatch = false;
    param.useBasicGraph = false;

    return param;
}

//USED
//construct a particle message from the individual particles
slam_graph_region_particle_list_t *get_slam_region_particle_msg(app_t *app){
    slam_graph_region_particle_list_t *msg = (slam_graph_region_particle_list_t *) calloc(1, sizeof(slam_graph_region_particle_list_t));
    msg->utime = bot_timestamp_now();
    msg->no_particles = app->slam_particles.size();
    
    msg->particle_list = (slam_graph_region_particle_t *) calloc(msg->no_particles, sizeof(slam_graph_region_particle_t));
    msg->status = SLAM_GRAPH_REGION_PARTICLE_LIST_T_STATUS_PROCESSED;

    msg->no_failed = (int) app->failed_complex_language.size();
    msg->annotations_failed = (slam_graph_language_annotation_t *) calloc(msg->no_failed, sizeof(slam_graph_language_annotation_t)); 
    for(int  i=0; i < app->failed_complex_language.size(); i++){
        msg->annotations_failed[i].node_id = app->failed_complex_language[i]->node_id; 
        msg->annotations_failed[i].label = strdup(app->failed_complex_language[i]->parse_result->utterance);
    }

    msg->no_outstanding = (int) app->outstanding_complex_language.size();
    msg->annotations_outstanding = (slam_graph_language_annotation_t *) calloc(msg->no_outstanding, sizeof(slam_graph_language_annotation_t)); 

    for(int  i=0; i < app->outstanding_complex_language.size(); i++){
        msg->annotations_outstanding[i].node_id = app->outstanding_complex_language[i]->node_id; 
        msg->annotations_outstanding[i].label = strdup(app->outstanding_complex_language[i]->parse_result->utterance);
    }

    msg->no_grounded = (int) app->grounded_language.size() + (int) app->grounded_simple_language.size();
    msg->annotations_grounded = (slam_graph_language_annotation_t *) calloc(msg->no_grounded, sizeof(slam_graph_language_annotation_t)); 

    for(int  i=0; i < app->grounded_language.size(); i++){
        msg->annotations_grounded[i].node_id = app->grounded_language[i]->node_id; 
        msg->annotations_grounded[i].label = strdup(app->grounded_language[i]->parse_result->utterance);
    }

    for(int  i=0; i < app->grounded_simple_language.size(); i++){
        int j = app->grounded_language.size() + i;
        msg->annotations_grounded[j].node_id = app->grounded_simple_language[i]->node_id; 
        msg->annotations_grounded[j].label = strdup(app->grounded_simple_language[i]->label->update);
    }
    
    if(app->doSemanticClassification){
        app->label_info->getTypeLcmMessage(&msg->type_info);
        app->label_info->getApperenceLcmMessage(&msg->appearance_info);
        app->label_info->getLabelLcmMessage(&msg->label_info);
    }
    else{
        app->label_info->getTypeLcmMessage(&msg->type_info);
        app->label_info->getApperenceLcmMessage(&msg->appearance_info);
        LabelDistribution ld;
        ld.getLabelLcmMessage(&msg->label_info);
    }

    for(int i=0; i < msg->no_particles; i++){
        SlamParticle *part = app->slam_particles.at(i);
        part->getLCMRegionMessageFromGraph(&msg->particle_list[i]);
    }

    //add the node classification stuff to the particle - since its the same for all the particles 
    msg->no_nodes  = (int) app->added_slam_nodes.size();
    msg->node_classifications = (slam_graph_node_classification_t *) calloc(msg->no_nodes, sizeof(slam_graph_node_classification_t));

    map<int, double>::iterator it;
    for(int i=0; i < app->added_slam_nodes.size(); i++){
        msg->node_classifications[i].no_laser_classes = 0;
        msg->node_classifications[i].no_image_classes = 0;
        msg->node_classifications[i].laser_classification = NULL;
        msg->node_classifications[i].image_classification = NULL;

        NodeScan *node = app->added_slam_nodes[i];
        msg->node_classifications[i].node_id = node->node_id;

        if(node->laser_classification){
            msg->node_classifications[i].no_laser_classes = node->laser_classification->count;
            msg->node_classifications[i].laser_classification = (slam_probability_element_t *) 
                calloc(msg->node_classifications[i].no_laser_classes, sizeof(slam_probability_element_t));
            int j=0;
            for(it = node->laser_classification_obs.begin(); it != node->laser_classification_obs.end(); it++, j++){
                msg->node_classifications[i].laser_classification[j].type = it->first;
                msg->node_classifications[i].laser_classification[j].probability = it->second;
            }
        }
                
        if(node->image_classification){
            msg->node_classifications[i].no_image_classes = node->image_classification->count;
            msg->node_classifications[i].image_classification = (slam_probability_element_t *) 
                calloc(msg->node_classifications[i].no_image_classes, sizeof(slam_probability_element_t));
            int j=0;
            for(it = node->image_classification_obs.begin(); it != node->image_classification_obs.end(); it++, j++){
                msg->node_classifications[i].image_classification[j].type = it->first;
                msg->node_classifications[i].image_classification[j].probability = it->second;
            }
        }
    }

    return msg;
}

//USED 
//publishes the resulting particles - used by the renderer to render the latest results 
//need to add the set of actions taken during this cycle - to better diagnose 
void publish_slam_region_particles(app_t *app){
    slam_graph_region_particle_list_t *msg = get_slam_region_particle_msg(app);

    slam_graph_region_particle_list_t_publish(app->lcm, "REGION_PARTICLE_ISAM_RESULT", msg);
    slam_graph_region_particle_list_t_destroy(msg);

    if(app->weights != NULL){
        slam_graph_weight_list_t_publish(app->lcm, "PARTICLE_ISAM_WEIGHTS", app->weights);
    }
}

//USED 
//publish the local to global transforms for each particle 
//the renderer will pick the valid one and publish the transform 
void publish_slam_transforms(app_t *app){
    erlcm_rigid_transform_list_t msg;
    msg.utime = bot_timestamp_now();
    msg.num = app->slam_particles.size();
    msg.list = (erlcm_rigid_transform_t *) calloc(msg.num, sizeof(erlcm_rigid_transform_t));
    bot_lcmgl_t *lcmgl = app->lcmgl_frame_test;
    bot_lcmgl_point_size(lcmgl, 10);
    bot_lcmgl_color3f(lcmgl, 1.0, 0, 0);
    bot_lcmgl_begin(lcmgl, GL_POINTS);

    for(int i=0; i < app->slam_particles.size(); i++){
        SlamParticle *part = app->slam_particles.at(i);

        BotTrans global_to_local = part->getBotTransForCurrentNode();
        msg.list[i].id = part->graph_id;
        bot_core_rigid_transform_t *bt_transform = &msg.list[i].transform;
        memcpy (bt_transform->trans, global_to_local.trans_vec, 3*sizeof(double));
        memcpy (bt_transform->quat, global_to_local.rot_quat, 4*sizeof(double));
        //lets lcmgl it??
        Pose2d last_pose = part->getLastPose();
        double pos[3] = {last_pose.x(),last_pose.y(),0};
        double l_pos[3];
        bot_trans_apply_vec(&global_to_local, pos, l_pos);
        bot_lcmgl_vertex3f(lcmgl, pos[0], pos[1], pos[2]);          
        //bot_lcmgl_vertex3f(lcmgl, l_pos[0], l_pos[1], l_pos[2]);          
    }

    bot_lcmgl_end(lcmgl);
    bot_lcmgl_switch_buffer(lcmgl);
     
    //erlcm_rigid_transform_t_publish(app->lcm, "SLAM_TRANSFORMS", NULL);
    erlcm_rigid_transform_list_t_publish(app->lcm, "SLAM_TRANSFORMS", &msg);
    //erlcm_position_t_publish(app->lcm, "TEST", NULL);
    free(msg.list);
}

//USED - Partial functionality
//Helper function to create new NodeScans 
//Currently the NodeScan copy doesn't fully copy its information
//Right now this is used with copyNodeScans function - which is not supported 
void copy_slam_nodes(vector<NodeScan *>nodes, vector<NodeScan *> &node_list){
    for(int i=0; i < nodes.size(); i++){
        NodeScan *nd = nodes[i];
        node_list.push_back(new NodeScan(nd));
    }
}

//USED 
//go through the vector - and pop the old stuff up to the last supernode
//if there are no new nodes - don't update 
int get_new_slam_nodes(app_t *app, vector<NodeScan *> &new_node_list){
    int max_new_nodes = 5;
    int min_nodes = 3;

    bool has_sn = false;

    /*for(int i = app->slam_nodes_to_add.size() -1; i >=0; i--){
        NodeScan *pose = app->slam_nodes_to_add[i];
        if(pose != NULL){
            if(pose->is_supernode){
                has_sn = true;
                break;
            }
        }
    }
    
    if(!has_sn && app->slam_nodes_to_add.size() < min_nodes){
        return 0; 
        }*/

    if(app->slam_nodes_to_add.size() == 0)
        return 0;

    int has_supernode = 0;
    int added_new_node = 0;
    //go through the new pose queue - and copy over nodes upto the first supernode 
    while (!app->slam_nodes_to_add.empty()){
        NodeScan *pose = app->slam_nodes_to_add.back();
        app->slam_nodes_to_add.pop_back();
        if (app->verbose)
            fprintf(stderr, "Slam node id : %d => Super : %d\n", pose->node_id, pose->is_supernode);
        if(pose != NULL){
            g_hash_table_insert(app->slam_nodes, &(pose->node_id), pose);
            new_node_list.push_back(pose);
            added_new_node = 1;
            app->last_slampose_for_process_ind = pose->node_id; //this is not used - so we should take out at some point 
            if(pose->is_supernode){
                has_supernode = 1;
                break;
            }
        }
        if(new_node_list.size() >= max_new_nodes)
            break;
    }

    //now go through the app transition list - see if there are any nodes that occured after a transition 
    //for(size_t i=0; i < app->transitions.size(); i++){
    if(app->transitions.size() > 0){
        std::vector<slam_region_transition_t *>::iterator it = app->transitions.begin();
    
        while(it != app->transitions.end()){
            int64_t transition_utime = (*it)->utime;
            int found_node = 0;

            for(size_t j=0; j < new_node_list.size(); j++){
                if(new_node_list.at(j)->utime > transition_utime){
                    fprintf(stderr, "\n\n\n+++++++++++++++++Found transition node - breaking+++++++++++++++++\n");
                    new_node_list.at(j)->transition_node = true;
                    //this is no longer set - so it will take out the entire set of nodes 
                    new_node_list.at(j)->is_supernode = true;
                    found_node = 1;
                    break;
                }            
            }
            if(found_node){ //remove the transition - so that it doesn't label others 
                slam_region_transition_t_destroy(*it);
                app->transitions.erase(it);
            }
            else{
                ++it;
            }
        }
    }

    if(has_supernode)
        return 2;
    else if(added_new_node)
        return 1;
    else
        return 0;
}

//USED 
void free_laser_odom(laser_odom_t *l_odom){
     sm_rigid_transform_2d_t_destroy(l_odom->odom);
     bot_core_planar_lidar_t_destroy(l_odom->laser);
     if(l_odom->rlaser){
         bot_core_planar_lidar_t_destroy(l_odom->rlaser);
     }
     if(l_odom->full_laser){
         bot_core_planar_lidar_t_destroy(l_odom->full_laser);
     }
     if(l_odom->image){
         bot_core_image_t_destroy(l_odom->image);
     }
     if(l_odom->image_result){
         classification_result_t_destroy(l_odom->image_result);
     }
     if(l_odom->laser_result){
         classification_result_t_destroy(l_odom->laser_result);
     }
     free(l_odom);
}

//USED 
/*
 * Publish the laser scan points out - for rendering 
 */


void get_slampose_message(NodeScan *node, slam_laser_pose_t &msg){
    Scan *scan = node->scan;
    msg.utime = node->utime;
    msg.id = node->node_id;
    msg.rp[0] = 0;
    msg.rp[1] = 0;
    msg.pl.no = scan->numPoints;
    msg.pl.points = (slam_scan_point_t *) calloc(scan->numPoints, sizeof(slam_scan_point_t));
    for (unsigned i = 0; i < scan->numPoints; i++) {  
        msg.pl.points[i].pos[0] =  scan->points[i].x;
        msg.pl.points[i].pos[1] =  scan->points[i].y;
    }
}

void publish_slampose(app_t *app, Scan *scan, int64_t utime, int64_t id){
    slam_laser_pose_t msg;
    msg.utime = utime;
    msg.id = id;
    msg.rp[0] = 0;
    msg.rp[1] = 0;
    msg.pl.no = scan->numPoints;
    msg.pl.points = (slam_scan_point_t *) calloc(scan->numPoints, sizeof(slam_scan_point_t));
    for (unsigned i = 0; i < scan->numPoints; i++) {  
        msg.pl.points[i].pos[0] =  scan->points[i].x;
        msg.pl.points[i].pos[1] =  scan->points[i].y;
    }
    slam_laser_pose_t_publish(app->lcm, "SLAM_POSE_LASER_POINTS", &msg);
    free(msg.pl.points);
}

void publish_slampose_list(app_t *app){
    slam_laser_pose_list_t *msg = (slam_laser_pose_list_t  *) calloc(1,sizeof(slam_laser_pose_list_t));
    msg->utime = bot_timestamp_now();
    msg->no_poses = (int) app->added_slam_nodes.size();
    msg->scans = (slam_laser_pose_t *) calloc(msg->no_poses, sizeof(slam_laser_pose_t));
    for(int i=0; i < app->added_slam_nodes.size(); i++){
        get_slampose_message(app->added_slam_nodes[i], msg->scans[i]);
    }

    slam_laser_pose_list_t_publish(app->lcm, "ISAM_LASER_SCANS", msg);
    slam_laser_pose_list_t_destroy(msg);
}


//signal handling to exit program 
sig_atomic_t still_groovy = 1;

static void sig_action(int signal, siginfo_t *s, void *user)
{
    still_groovy = 0;
}

//USED
//Only add laser scans to the queue if the scan is over a specific distance from the last one in the queue 
static void add_to_queue(const bot_core_planar_lidar_t * laser_msg, const sm_rigid_transform_2d_t * odom,
                         int64_t utime, app_t * app, int force_supernode = 0)
{
    static int first_scan = 1;
    static int first_laser_scan = 1;
    
    //compute the distance between this scan and the last one that got added.
    double dist[2];
    sm_vector_sub_2d(odom->pos, app->prev_odom->pos, dist);

    double ld = sm_norm(SMPOINT(dist));
    double ad = sm_angle_subtract(odom->theta, app->prev_odom->theta);
    bool addScan = false;

    if(!app->slave_mode){
        //this should be taken off when node creation is automatic
        if (fabs(ld) > LINEAR_DIST_TO_ADD) 
            addScan = true;

        if (fabs(ad) > ANGULAR_DIST_TO_ADD) 
            addScan = true;
        
        if(first_scan){
            addScan = true;
            first_scan = 0;
        }       
    }

    //if in slave mode - we add all new requests 
    if(app->slave_mode && app->node_init != NULL)
        addScan = true; 

    if(!addScan)
        return;
    
   

    bot_core_planar_lidar_t *rlaser = get_closest_rear_laser(app, laser_msg->utime);
    bot_core_planar_lidar_t *full_laser = NULL;
    bot_core_image_t *image = NULL;

    if(app->doSemanticClassification){
        full_laser = get_closest_full_laser(app, laser_msg->utime);
        image = get_closest_image(app, laser_msg->utime);
        
        if(full_laser == NULL){
            fprintf(stderr, "Doing semantic classification and no close laser found\n");
            return;
        }
        if(image == NULL){
            fprintf(stderr, "Doing semantic classification and no close image found\n");
            return;
        }
    }

    //get the odometry delta
    Pose2d curr_pose(odom->pos[0], odom->pos[1], odom->theta);
    Pose2d prev_pose(app->prev_odom->pos[0], app->prev_odom->pos[1], app->prev_odom->theta);
    Pose2d prev_curr_tranf = curr_pose.ominus(prev_pose);

    if (app->prev_odom != NULL)
        sm_rigid_transform_2d_t_destroy(app->prev_odom);
    app->prev_odom = sm_rigid_transform_2d_t_copy(odom);

    laser_odom_t *l_odom = (laser_odom_t *) calloc(1, sizeof(laser_odom_t));

    l_odom->laser = bot_core_planar_lidar_t_copy(laser_msg);
    l_odom->rlaser = NULL;
    l_odom->image = NULL;
    l_odom->full_laser = NULL;
    
    if(rlaser != NULL){
        l_odom->rlaser = bot_core_planar_lidar_t_copy(rlaser);
    }
    //get the closest full laser and image - which will be used for classification 
    int64_t s_utime = bot_timestamp_now();

    fprintf(stderr, RED);
    if(full_laser != NULL){
        //fprintf(stderr, YELLOW "\n\n+++++++++++++++++ Found Full laser (%f) +++++++++++++++++\n\n" RESET_COLOR, (full_laser->utime - laser_msg->utime)/1.0e6);
        l_odom->full_laser = bot_core_planar_lidar_t_copy(full_laser);
        l_odom->laser_result = get_classification_laser(app->classifier, l_odom->full_laser);

        print_classification_result_t(l_odom->laser_result);
        for(int i=0; i < l_odom->laser_result->count; i++){
            semantic_class_t *s_class = &l_odom->laser_result->classes[i];
            int old_class = s_class->type;
            s_class->type = app->label_info->getIndexFromClassifierClassId(s_class->type);
            fprintf(stderr, "Class : Old : %d => New : %d\n", old_class,  s_class->type);
        }
        //we should remap this here 
        //
    }
    else{
        l_odom->laser_result = NULL;
    }
    
    if(image != NULL){
        //fprintf(stderr, RED "\n\n+++++++++++++++++ Found Image (%f) +++++++++++++++++\n\n" RESET_COLOR, (image->utime - laser_msg->utime)/1.0e6);
        l_odom->image = bot_core_image_t_copy(image);
        l_odom->image_result = get_classification_image(app->classifier, l_odom->image);

        //we should remap this here 
        print_classification_result_t(l_odom->image_result);
        for(int i=0; i < l_odom->image_result->count; i++){
            semantic_class_t *s_class = &l_odom->image_result->classes[i];
            int old_class = s_class->type;
            s_class->type = app->label_info->getIndexFromClassifierClassId(s_class->type);
            fprintf(stderr, "Class : Old : %d => New : %d\n", old_class,  s_class->type);
        }
        //
    }
    else{
        l_odom->image_result = NULL;
    }

    int64_t e_utime = bot_timestamp_now();
    
    //fprintf(stderr, RED "\n\n+++++++++++++++++ Classifying semantics (time) : %.4f (s) +++++++++++++++++\n\n" RESET_COLOR, (e_utime - s_utime)/1.0e6);


    l_odom->odom =  sm_rigid_transform_2d_t_copy(odom);//new Pose2d(odom);//(prev_curr_tranf);
    l_odom->force_supernode = force_supernode;

    //lock a mutex
    //fprintf(stderr, YELLOW "Waiting to add laser to Queue\n", RESET_COLOR);
    g_mutex_lock(app->mutex_laser_queue);
    app->lasers_to_add.push_back(l_odom);
    g_mutex_unlock(app->mutex_laser_queue);
    //fprintf(stderr, YELLOW "Added laser to Queue\n", RESET_COLOR);
}


//NOT USED 
smPoint *merge_front_and_back_laser(smPoint *f_laser, int f_no, smPoint *r_laser, int r_no, int *total){

    int no_of_points = f_no + r_no;
    smPoint *full_points =  (smPoint *) calloc(no_of_points, sizeof(smPoint));
    memcpy(full_points, f_laser, sizeof(smPoint) * f_no);
    fprintf(stderr, "Front Laser Count : %d + Rear Laser Count : %d\n", f_no, r_no);
    //memcpy(&full_points[numValidPoints], r_points, sizeof(smPoint) * r_numValidPoints);

    ANNkd_tree *kd_tree;

    ann_t *kd_points = (ann_t *) calloc(1,sizeof(ann_t));
    kd_points->dataPts = annAllocPts(f_no, 2);
    kd_points->no_points = f_no;
    for (unsigned i = 0; i < f_no; i++) {     
        kd_points->dataPts[i][0] = f_laser[i].x;
        kd_points->dataPts[i][1] = f_laser[i].y;
    }

    kd_tree = new ANNkd_tree(kd_points->dataPts, 
                             kd_points->no_points,
                             2);    
    
    ANNpoint qp = annAllocPt(2);
    double radius_bound = 0.2;//0.05;//0.3; //was 0.05 
    int query_size = 1;
    double epsilon =  0.05;//0.1;
    ANNidxArray  nnIdx = new ANNidx[query_size];
    ANNdistArray dists = new ANNdist[query_size];

    int not_matched = 0;

    for(unsigned i = 0; i < r_no; i++){
        qp[0] = r_laser[i].x;
        qp[1] = r_laser[i].y;
        
        kd_tree->annkFRSearch (qp,                    // query point
                               ANN_POW(radius_bound), // squared radius search bound
                               query_size,            // number of near neighbors
                               nnIdx,                 // nearest neighbors (returned)
                               dists,                 // distance (returned)
                               epsilon);
        
        for(int l=0;l< query_size ;l++){
            if(nnIdx[l] < 0){//because we have reached the end of the valid neighbours
                full_points[f_no+not_matched].x = r_laser[i].x;
                full_points[f_no+not_matched].y = r_laser[i].y;
                not_matched++;
                break; 
            }
            break;
        }
    }
    fprintf(stderr, "Not Matched : %d => New Total Points : %d / %d\n", not_matched, not_matched + f_no, no_of_points);
    no_of_points = not_matched + f_no;
    full_points = (smPoint*) realloc(full_points,  no_of_points * sizeof(smPoint));
    *total = no_of_points;
    
    //cleanup memory 
    delete kd_tree;
    annDeallocPts(kd_points->dataPts);
    annDeallocPt(qp);
    free(kd_points);
    delete [] nnIdx;
    delete [] dists;

    return full_points;
}

//USED 
//get the rear laser - compensated to be in the front laser's frame 
smPoint *get_scan_points_from_laser_compensated(bot_core_planar_lidar_t *laser_msg, app_t *app, char *frame, int *noPoints, int64_t utime){
    smPoint * points = (smPoint *) calloc(laser_msg->nranges, sizeof(smPoint));
    int numValidPoints = sm_projectRangesAndDecimate(app->beam_skip, app->spatialDecimationThresh, laser_msg->ranges,
                                                     laser_msg->nranges, laser_msg->rad0, laser_msg->radstep, points, 
                                                     app->maxRange, 
                                                     //bot_to_radians(-135), bot_to_radians(135)); 
                                                     //app->validBeamAngles[0], app->validBeamAngles[1]);
                                                     -M_PI/4, M_PI/4); 

    //fprintf(stderr, MAKE_GREEN "NO of valid points : %d\n" RESET_COLOR, numValidPoints);
    
    if (numValidPoints < 30) {
        //fprintf(stderr, "WARNING! NOT ENOUGH VALID POINTS! numValid=%d\n", numValidPoints);
        free(points);
        *noPoints = 0;
        return NULL;
    }
    else {
        points = (smPoint *) realloc(points, numValidPoints * sizeof(smPoint));//crop memory down to size
        *noPoints = numValidPoints;
    }
    //adding laser offset
    BotTrans sensor_to_local_sensor_time;
    BotTrans local_to_body_comp_time;

    int status_1 = bot_frames_get_trans_with_utime(app->frames, frame, 
                                    "local",
                                    laser_msg->utime, 
                                    &sensor_to_local_sensor_time);

    int status_2 = bot_frames_get_trans_with_utime(app->frames, "local", 
                                    "body",
                                    utime, 
                                    &local_to_body_comp_time);

    if(!status_1 || !status_2){
        fprintf (stderr, "Error getting bot_frames transformation from %s to body!\n", app->chan);
        free (points);
        *noPoints = 0;
        return NULL;
    }
    
    //transform base map to particle's map frame - according to nd2 
    BotTrans sensor_to_body_frame;
    bot_trans_apply_trans_to(&local_to_body_comp_time, &sensor_to_local_sensor_time, &sensor_to_body_frame);
    
    double pos_s[3] = {0}, pos_b[3];
    for(int i=0; i < numValidPoints; i++){
        pos_s[0] = points[i].x;
        pos_s[1] = points[i].y;
        bot_trans_apply_vec(&sensor_to_body_frame, pos_s , pos_b);
        points[i].x = pos_b[0];
        points[i].y = pos_b[1];
    }

    //transform to body pose
    
    return points;
}

//USED - To get points from laser
smPoint *get_scan_points_from_laser(bot_core_planar_lidar_t *laser_msg, app_t *app, char *frame, int *noPoints){
    smPoint * points = (smPoint *) calloc(laser_msg->nranges, sizeof(smPoint));
    int numValidPoints = sm_projectRangesAndDecimate(app->beam_skip, app->spatialDecimationThresh, laser_msg->ranges,
                                                         laser_msg->nranges, laser_msg->rad0, laser_msg->radstep, points, 
                                                         app->maxRange, app->validBeamAngles[0], app->validBeamAngles[1]);
    *noPoints = numValidPoints;
           
    if (numValidPoints < 30) {
        fprintf(stderr, "WARNING! NOT ENOUGH VALID POINTS! numValid=%d\n", numValidPoints);
        free(points);
        *noPoints = 0;
        return NULL;
    }
    else {
        points = (smPoint *) realloc(points, numValidPoints * sizeof(smPoint));//crop memory down to size
    }
    //adding laser offset
    double sensor_to_body[12];
    if (!bot_frames_get_trans_mat_3x4 (app->frames, frame, 
                                       "body",
                                       sensor_to_body)) {
        fprintf (stderr, "Error getting bot_frames transformation from %s to body!\n", app->chan);
        free (points);
        *noPoints = 0;
        return NULL;
    }
    //transform to body pose
    double pos_s[3] = {0}, pos_b[3];
    for(int i=0; i < numValidPoints; i++){
        pos_s[0] = points[i].x;
        pos_s[1] = points[i].y;
        bot_vector_affine_transform_3x4_3d (sensor_to_body, pos_s , pos_b);
        points[i].x = pos_b[0];
        points[i].y = pos_b[1];
    }
    return points;
}


//USED
//process the laser observations 
void createNodeScan(app_t * app, laser_odom_t *l_odom){
    static int first_scan = 1; 
    static int first_laser_scan = 1;

    //rotate the cov to body frame
    double Rcov[9];
    Matrix3d cv(Rcov);
    
    bot_core_planar_lidar_t * laser_msg = l_odom->laser;
    bot_core_planar_lidar_t * r_laser_msg = l_odom->rlaser;
    int64_t utime = laser_msg->utime;
    int force_supernode = l_odom->force_supernode;
    static double dist_traveled = 0;
    static double angle_turned = 0;
    sm_rigid_transform_2d_t *odom = l_odom->odom;

    //get the odometry delta
    Pose2d curr_pose(odom->pos[0], odom->pos[1], odom->theta);
    Pose2d prev_pose(app->prev_added_odom->pos[0], app->prev_added_odom->pos[1], app->prev_added_odom->theta);
    
    //check if there is a door detected between these two points 
    bool transition_node = false;
    
    if(app->use_doorways && app->door_list){
        doorway_t *doorway = NULL;
        double pos_1[2] = {curr_pose.x(), curr_pose.y()};
        double pos_2[2] = {prev_pose.x(), prev_pose.y()};
        doorway = detect_doorway_between_pos(app->door_list, pos_1, pos_2);
        if(doorway != NULL){
            fprintf(stderr, CYAN "\n\n\n Door Detected \n\n\n" RESET_COLOR);
            transition_node = true;
            destroy_door(doorway);
        }    
    }
    
    Pose2d prev_curr_tranf = curr_pose.ominus(prev_pose);

    double dist[2];
    sm_vector_sub_2d(odom->pos, app->prev_added_odom->pos, dist);

    double ld = sm_norm(SMPOINT(dist));
    double ad = sm_angle_subtract(odom->theta, app->prev_odom->theta);
    
    if (app->prev_added_odom != NULL)
        sm_rigid_transform_2d_t_destroy(app->prev_added_odom);
    app->prev_added_odom = sm_rigid_transform_2d_t_copy(odom);
    
    dist_traveled += fabs(ld);
    angle_turned += fabs(prev_curr_tranf.t());
    
    if(app->verbose){
        fprintf(stderr, "Scan is %f meters from last add, adding scan\n", ld);
        fprintf(stderr, "Scan is %f degrees from last add, adding scan\n", prev_curr_tranf.t()* 180.0/M_PI);
    }

    int numValidPoints = 0;
    smPoint * points = get_scan_points_from_laser(laser_msg, app, app->chan , &numValidPoints);

    if(points == NULL)
        return;
    
    int r_numValidPoints = 0;
    smPoint * r_points = NULL;
    if(r_laser_msg != NULL){
        //fprintf(stderr, MAKE_GREEN "Found rear laser\n" RESET_COLOR);
        r_points =  get_scan_points_from_laser_compensated(r_laser_msg, app, app->rchan , &r_numValidPoints, laser_msg->utime);
    }

    ScanTransform T;
    //just use the zero transform... it'll get updated the first time its needed
    memset(&T, 0, sizeof(T));

    Scan * scan;

    int no_of_points = numValidPoints + r_numValidPoints;
    smPoint *full_points =  (smPoint *) calloc(no_of_points, sizeof(smPoint));
    memcpy(full_points, points, sizeof(smPoint) * numValidPoints);
    memcpy(&full_points[numValidPoints], r_points, sizeof(smPoint) * r_numValidPoints);

    //fprintf(stderr, "Total no of Points : %d\n", no_of_points);

    //scan = new Scan(numValidPoints, points, T, SM_HOKUYO_UTM, utime, true);
    scan = new Scan(no_of_points, full_points, T, SM_HOKUYO_UTM, utime, true);
  
    //first node wont have a constraint with another node 
    //add locks here somewhere also - if needed
    
    int node_id = app->node_count;
    if(app->slave_mode){
        if (app->verbose)
            fprintf(stderr, "New node id : %d\n", (int) app->node_init->id);
        node_id = app->node_init->id;            
    }
    
    publish_slampose(app, scan, utime, node_id);
    
    //compute the distance between this scan and the last one that got added.

    double dr = hypot(prev_curr_tranf.x(), prev_curr_tranf.y());
    double alpha1 = atan2(prev_curr_tranf.y(), prev_curr_tranf.x());
    double alpha2 = prev_curr_tranf.t() - alpha1;
    
    //double lambda1 = 0.0001*0.0001, lambda2 = 0.00001, lambda3 = 0.00001, lambda4 = 0.0001*0.0001; 
    //these values were used before and seem too low 
    //double lambda1 = 0.001*0.001, lambda2 = 0.00001, lambda3 = 0.00001, lambda4 = 0.001*0.001; 

    double lambda1 = 0.005*0.005, lambda2 = 0.00005, lambda3 = 0.00005, lambda4 = 0.005*0.005; 
    
    Matrix3d V;
    V << -dr * sin(alpha1), dr, 0, dr * cos(alpha1), dr ,0, 1, 0, 1;

    Matrix3d M;
    
    M << lambda1 * pow(alpha1,2) + lambda2 * pow(dr,2), 0, 0, 
        0, lambda3 * pow(dr,2) + lambda4 * pow(alpha1,2) + lambda4 * pow(alpha2,2), 0, 
        0, 0, (lambda1 * pow(alpha2,2) + lambda2 * pow(dr,2));
    
    Matrix3d motion_cov = V * M * V.transpose();
    
    Noise *cov;
    if(first_laser_scan){
        cov =  new SqrtInformation(10000. * eye(3));
    }
    else{
        cov = new Covariance(motion_cov);
    }

    //cout << motion_cov << endl;

    //double cov_hardcode_odom[9] = { .00002, 0, 0, 0, .00002, 0, 0, 0, .00001 };//{ .0002, 0, 0, 0, .0002, 0, 0, 0, .0001 };
    //Matrix3d cv_hardcode_odom(cov_hardcode_odom);
    //cov = new Covariance(cv_hardcode_odom);
    
    //************* Do the incremental scan match **************//
    
    //do incremental scan matching to refine the odometry estimate
    Pose2d prev_sm_pose(app->prev_sm_odom->pos[0], app->prev_sm_odom->pos[1], app->prev_sm_odom->theta);
    Pose2d curr_sm_pose = prev_sm_pose.oplus(prev_curr_tranf);
    ScanTransform sm_prior;
    memset(&sm_prior, 0, sizeof(sm_prior));
    sm_prior.x = curr_sm_pose.x();
    sm_prior.y = curr_sm_pose.y();
    sm_prior.theta = curr_sm_pose.t();
    sm_prior.score = app->odometryConfidence; //wide prior
    
    int64_t s_utime = bot_timestamp_now();

    ScanTransform r = app->sm->matchSuccessive(points, numValidPoints, SM_HOKUYO_UTM, sm_get_utime(), false, &sm_prior);

    double sxx = r.sigma[0];
    double sxy = r.sigma[1];
    double syy = r.sigma[4];
    double stt = r.sigma[8];
    
    /*fprintf(stderr, BLUE "Scanmatch Score : %f Uncertainity :=> sxx=%f,sxy=%f,syy=%f,stt=%f\n" RESET_COLOR, r.hits / (double) numValidPoints, 
      sxx, sxy, syy, stt);*/

    int64_t e_utime = bot_timestamp_now();

    //we should check how far this is from the odometry - we should reject bad scanmatches that propose significant deviations 


    curr_sm_pose.set(r.x, r.y, r.theta);
    sm_rigid_transform_2d_t sm_odom;
    memset(&sm_odom, 0, sizeof(sm_odom));
    sm_odom.utime = laser_msg->utime;
    sm_odom.pos[0] = r.x;
    sm_odom.pos[1] = r.y;
    sm_odom.theta = r.theta;
    memcpy(sm_odom.cov, r.sigma, 9 * sizeof(double));
    if (app->prev_sm_odom != NULL)
        sm_rigid_transform_2d_t_destroy(app->prev_sm_odom);
    app->prev_sm_odom = sm_rigid_transform_2d_t_copy(&sm_odom);
        
    Pose2d prev_curr_sm_tranf = curr_sm_pose.ominus(prev_sm_pose);

    double dx = prev_curr_sm_tranf.x() - prev_curr_tranf.x();
    double dy = prev_curr_sm_tranf.y() - prev_curr_tranf.y();
    double dt = bot_mod2pi(prev_curr_sm_tranf.t() - prev_curr_tranf.t());

    fprintf(stderr, GREEN "\n\n Delta (SM and Odom) %f, %f, %f \n\n" RESET_COLOR, dx, dy, bot_to_degrees(dt));
    
    int sm_error = 0;
    int sm_high_cov = 0;
    //this fixes things too well - need to set this back on after the paper 
    if(fabs(dx) > 0.2 || fabs(dy) > 0.2 || fabs(dt) > bot_to_radians(5.0)){
        sm_high_cov = 1;
        //sm_error = 1;
    }
    if(fabs(dx) > 0.2 || fabs(dy) > 0.2 || fabs(dt) > bot_to_radians(5.0)){
        fprintf(stderr, RED "SM in error - using odom\n");
        sm_error = 1;
    }
    /*
    if(fabs(dx) > 0.5 || fabs(dy) > 0.5 || fabs(dt) > bot_to_radians(15.0)){
        fprintf(stderr, RED "SM in error - using odom\n");
        sm_error = 1;
        }*/
    
    //we should not do a scanmatch if the no of points are low
    //if(no_of_points < 30){
    //sm_error = 1;
    //}

    double Rcov_sm[9];
    sm_rotateCov2D(r.sigma, -r.theta, Rcov_sm);
    Matrix3d cv_sm(Rcov_sm); 

    //Bianca - Covariance - previous to current constraint 
    Noise *cov_sm;
    if(app->useSMCov){
        if(first_laser_scan){
            cov_sm =  new SqrtInformation(10000. * eye(3));
        }
        else{
            cov_sm = new Covariance(cv_sm);
        }
    }
    else{
        if(first_laser_scan){
            cov_sm =  new SqrtInformation(10000. * eye(3));
        }
        else{
            //double cov_hardcode[9] = { .00002, 0, 0, 0, .00002, 0, 0, 0, .00001 };
            //used values earlier - { .0002, 0, 0, 0, .0002, 0, 0, 0, .0001 };
            if(sm_high_cov){
                double cov_hardcode[9] = { .002, 0, 0, 0, .002, 0, 0, 0, .001 };
                Matrix3d cv_hardcode(cov_hardcode);
                cov_sm = new Covariance(cv_hardcode);
            }
            else{
                double cov_hardcode[9] = { .00005, 0, 0, 0, .00005, 0, 0, 0, .00002 };
                Matrix3d cv_hardcode(cov_hardcode);
                cov_sm = new Covariance(cv_hardcode);
            }
            
        }
    } 

    bot_lcmgl_t *lcmgl = app->lcmgl_debug;
    if(0){
        if(app->last_scan != NULL){
            bot_lcmgl_t *lcmgl = app->lcmgl_debug;
            bot_lcmgl_point_size(lcmgl, 2);
            bot_lcmgl_begin(lcmgl, GL_POINTS);
            double pBody[3] = { 0, 0, 0 };
            double pLocal[3];
            bot_lcmgl_color3f(lcmgl, 1.0, 0.5, 0);
            for (unsigned i = 0; i < app->last_scan->numPoints; i++) {        
                pBody[0] = app->last_scan->points[i].x;
                pBody[1] = app->last_scan->points[i].y;
                bot_lcmgl_vertex3f(lcmgl, pBody[0], pBody[1], pBody[2]);
            }
            bot_lcmgl_end(lcmgl);
            
            BotTrans bodyToLocal;
            bodyToLocal.trans_vec[0] = prev_curr_sm_tranf.x();
            bodyToLocal.trans_vec[1] = prev_curr_sm_tranf.y();
            bodyToLocal.trans_vec[2] = 0;
            double rpy[3] = { 0, 0, prev_curr_sm_tranf.t()};
            bot_roll_pitch_yaw_to_quat(rpy, bodyToLocal.rot_quat);
            
            bot_lcmgl_point_size(lcmgl, 2);
            bot_lcmgl_begin(lcmgl, GL_POINTS);
            bot_lcmgl_color3f(lcmgl, 0, 0.5, 0.5);
            for (unsigned i = 0; i < scan->numPoints; i++) {        
                pBody[0] = scan->points[i].x;
                pBody[1] = scan->points[i].y;
                bot_trans_apply_vec(&bodyToLocal, pBody, pLocal);
                bot_lcmgl_vertex3f(lcmgl, pLocal[0], pLocal[1], pLocal[2]);                
            }
            bot_lcmgl_end(lcmgl);
            bot_lcmgl_switch_buffer(lcmgl);
        }
        delete app->last_scan;
    
        app->last_scan = new Scan(numValidPoints, points, T, SM_HOKUYO_UTM, utime, true);
    }

    // We are now done with points
    free (points);
    free (r_points);
    free (full_points);

    publish_slampose(app, scan, utime, node_id);
    
    double hitpct = r.hits / (double) numValidPoints;//scan->numPoints;
        
    if(sm_error){
        hitpct = 0.0;
    } 
    else{
        //if(hitpct > 0.5){
        //hitpct = 0.9;
        //}
    }


    if (app->verbose)
        fprintf(stderr, "Node added => Node id : %d\n",node_id);

    //get the BotTrans - for local to body 
    BotTrans body_to_local;
    if(!bot_frames_get_trans_with_utime(app->frames, "body", "local", laser_msg->utime, &body_to_local)){
        fprintf(stderr, "Error Getting Bot Trans\n");
    }
    
    if(l_odom->full_laser && l_odom->image){
        /*int64_t s_utime = bot_timestamp_now();
        get_classification(app->classifier, l_odom->full_laser, l_odom->image);
        int64_t e_utime = bot_timestamp_now();
        fprintf(stderr, RED "\n\n+++++++++++++++++ Classifying semantics (time) : %.4f (s) +++++++++++++++++\n\n" RESET_COLOR, (e_utime - s_utime)/1.0e6);*/
    }
    //add a new Slam node 
    NodeScan * slampose = new NodeScan(utime, node_id, scan, false, curr_pose, l_odom->laser_result, l_odom->image_result);
    
    app->last_node_utime = utime;

    slampose->transition_node = transition_node;

    if(first_laser_scan){
        slampose->transition_node = true;
        slampose->is_supernode = false;
    }

    first_laser_scan = 0;


    slampose->laser = bot_core_planar_lidar_t_copy(laser_msg);
    slampose->body_to_local = body_to_local;
    app->node_count++;         
    PoseToPoseTransform *constraint_sm = new PoseToPoseTransform(&prev_curr_sm_tranf, slampose, app->last_slampose, 
                                                                 SLAM_GRAPH_EDGE_T_TYPE_SM_INC, 
                                                                 cov_sm, hitpct, ld, ad, 1);//cov_hc);
    delete cov_sm;
    
    slampose->inc_constraint_sm = constraint_sm; 

    //odom constraint added 
    PoseToPoseTransform *constraint_odom = new PoseToPoseTransform(&prev_curr_tranf, slampose, app->last_slampose, 
                                                                   SLAM_GRAPH_EDGE_T_TYPE_ODOM_INC, cov, 1.0, ld, ad,  1);
    delete cov;
    //constraint is added to the current node (transform is respective to - i.e. in the previous node's pov)
    slampose->inc_constraint_odom = constraint_odom; 
    
    //this can get blocked 
    g_mutex_lock(app->mutex);
    //fprintf(stderr, RED "Waiting on MUTEX\n" RESET_COLOR); 
    app->last_slampose = slampose;
    app->last_slampose_ind = slampose->node_id;
    if(app->slave_mode){
        vector<NodeScan *>::iterator it = app->slam_nodes_to_add.begin();
        app->slam_nodes_to_add.insert(it, slampose);    
        it = app->all_slam_nodes.begin();
        app->all_slam_nodes.insert(it, slampose);
    }
    else{
        if (app->verbose)
            fprintf (stdout, "----------------------adding slam node\n");
        vector<NodeScan *>::iterator it = app->slam_nodes_to_add.begin();
        app->slam_nodes_to_add.insert(it, slampose);
        it = app->all_slam_nodes.begin();
        app->all_slam_nodes.insert(it, slampose);
    }
    //fprintf(stderr, RED "Done waiting on MUTEX\n" RESET_COLOR); 
    g_mutex_unlock(app->mutex);

    //destroy the node init - after its handled
    if(app->node_init != NULL){
        slam_init_node_t_destroy(app->node_init);
        app->node_init = NULL;
    }
    
    return;
}

//Clearly being stuck on something

//USED 
int process_laser_queue(app_t *app){
    //go through the queue 
    int to_add = 0;
    //fprintf(stderr, RED "Waiting on Laser Queue\n" RESET_COLOR);
    g_mutex_lock(app->mutex_laser_queue);    
    if(app->lasers_to_add.size() > 0){
        //fprintf(stderr, BLUE "+++Laser Queue Size : %d\n" RESET_COLOR, (int) app->lasers_to_add.size());
        to_add = 1;
    }
    g_mutex_unlock(app->mutex_laser_queue);
    //fprintf(stderr, RED "Done checking Laser Queue\n" RESET_COLOR);

    if(to_add ==0){
        return 0;
    }
    
    int count = 0; 
    //otherwise because of resampling everything gets blocked 
    //maybe this createNodeScan can be broken up - so that another thread 
    //already does the scanmatching - while the particles are being processed
    while(to_add && count < MAX_LASERS_TO_COPY_FROM_QUEUE){
        //fprintf(stderr, BLUE "Waiting on Laser Queue\n", RESET_COLOR);
        g_mutex_lock(app->mutex_laser_queue);
        //fprintf(stderr, BLUE "Have Lock\n", RESET_COLOR);
        //fprintf(stderr, "Processing Laser in queue\n");
        laser_odom_t *l_odom = app->lasers_to_add.at(0);

        app->lasers_to_add.erase(app->lasers_to_add.begin());

        if(app->lasers_to_add.size() > 0)
            to_add =1;
        else
            to_add = 0;
        //fprintf(stderr, BLUE "Unlocking Laser Queue\n", RESET_COLOR);
        g_mutex_unlock(app->mutex_laser_queue);
        count++;
        createNodeScan(app, l_odom);        

        //free the data 
        /*sm_rigid_transform_2d_t_destroy(l_odom->odom);
        bot_core_planar_lidar_t_destroy(l_odom->laser);
        free(l_odom);*/
        free_laser_odom(l_odom);
    }
    return 1;
}


void app_destroy(app_t *app)
{
    //TODO: there is a double free mess cuz scan matcher free's scans that are held onto elsewhere :-/
    //LEAK everything for now!!!

    if (app->mutex)
        g_mutex_free (app->mutex);

    if (app->mutex_particles)
        g_mutex_free (app->mutex_particles);

    if (app->mutex_lasers)
        g_mutex_free (app->mutex_lasers);

    // dump timing stats
    sm_tictoc(NULL);
    exit (1);

}


//free function to clean up the circular buffer data 
void
circ_free_lidar_data(void *user, void *p) {
    bot_core_planar_lidar_t *np = (bot_core_planar_lidar_t *) p; 
    bot_core_planar_lidar_t_destroy(np);
}

//free function to clean up the circular buffer data 
void
circ_free_image_data(void *user, void *p) {
    bot_core_image_t *np = (bot_core_image_t *) p; 
    bot_core_image_t_destroy(np);
}


//USED 
//process the laser scan - for odometry
void process_laser(bot_core_planar_lidar_t *flaser, app_t *app, int64_t utime, int force_supernode = 0){
    if(app->useOdom){
        sm_rigid_transform_2d_t odom;
        memset(&odom, 0, sizeof(odom));

        double rpy[3];
        
        //this covariance is ignored 
        double cov[9] = { 0 };
        cov[0] = 0.0005;
        cov[1] = 0;
        cov[3] = 0;
        cov[4] = 0.0005; //about 2 cm of std dev 
        cov[8] = 0.0002;
        
        if(!app->use_frames){
            if(app->last_deadreakon != NULL){
                //use the odom msg
                memcpy(odom.pos, app->last_deadreakon->pos, 2 * sizeof(double));
                bot_quat_to_roll_pitch_yaw(app->last_deadreakon->orientation, rpy);
            }
            else
                return;
        }
        else{ 
            int64_t frame_utime;
            //this can be fine as long as its been updated 
            int status = bot_frames_get_latest_timestamp(app->frames, "body",
                                                         "local",&frame_utime);

            if(!status || fabs(frame_utime - utime) / 1.0e6 > 10.0){
                fprintf(stderr, "Frames hasn't been updated yet - retruning %f - %f => Gap : %f\n", frame_utime/1.0e6, utime /1.0e6, fabs(frame_utime - utime) / 1.0e6);
                return;
            }

            double body_to_local[12];
            if (!bot_frames_get_trans_mat_3x4_with_utime (app->frames, "body",
                                                          "local",  flaser->utime,
                                                          body_to_local)) {
                fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
                return;        
            }
            double pose_b[3] = {.0}, pose_l[3];
            bot_vector_affine_transform_3x4_3d (body_to_local, 
                                                pose_b, 
                                                pose_l);
            odom.pos[0] = pose_l[0];
            odom.pos[1] = pose_l[1];

            BotTrans body_to_local_t;

            if (!bot_frames_get_trans_with_utime (app->frames, "body", "local", flaser->utime, 
                                                  &body_to_local_t)){
                fprintf (stderr, "frame error\n");
                return;
            }
            
            double rpy_b[3] = {0};
            double quat_b[4], quat_l[4];
            bot_roll_pitch_yaw_to_quat (rpy_b, quat_b);
            
            bot_quat_mult (quat_l, body_to_local_t.rot_quat, quat_b);
            bot_quat_to_roll_pitch_yaw (quat_l, rpy);
        }

        odom.theta = rpy[2];
        //cov was published as body frame already... rotate to global for now
        double Rcov[9];
        sm_rotateCov2D(cov, odom.theta, odom.cov);

        static int64_t utime_prev = flaser->utime;

        sm_tictoc("aligned_laser_handler");
        add_to_queue(flaser, &odom, flaser->utime, app, force_supernode);
        sm_tictoc("aligned_laser_handler");
    }
    else{
        //if we dont have odom - use scanmatching 
        smPoint * points = (smPoint *) calloc(flaser->nranges, sizeof(smPoint));
        int numValidPoints = sm_projectRangesAndDecimate(app->beam_skip, app->spatialDecimationThresh, flaser->ranges,
                                                         flaser->nranges, flaser->rad0, flaser->radstep, points, 
                                                         app->maxRange, app->validBeamAngles[0], app->validBeamAngles[1]);
        if (numValidPoints < 30) {
            fprintf(stderr, "WARNING! NOT ENOUGH VALID POINTS! numValid=%d\n", numValidPoints);
            free (points);
            return;
        }

        double sensor_to_body[12];
        if (!bot_frames_get_trans_mat_3x4 (app->frames, app->chan,
                                           "body",
                                           sensor_to_body)) {
            fprintf (stderr, "Error getting bot_frames transformation from %s to body!\n", app->chan);
            free (points);
            return;
        }

        //transform to body pose
        double pos_s[3] = {0};
        double pos_b[3] = {0};
        for(int i=0; i < numValidPoints; i++){
            pos_s[0] = points[i].x;
            pos_s[1] = points[i].y;
            bot_vector_affine_transform_3x4_3d (sensor_to_body, pos_s , pos_b);
            points[i].x = pos_b[0];
            points[i].y = pos_b[1];
        }
        
        ////////////////////////////////////////////////////////////////////
        //Actually do the matching
        ////////////////////////////////////////////////////////////////////
        //don't have a better estimate than prev, so just set prior to NULL
        ScanTransform r = app->sm_incremental->matchSuccessive(points, numValidPoints, 
                                                               SM_HOKUYO_UTM, sm_get_utime(), NULL); 

        sm_rigid_transform_2d_t odom;
        memset(&odom, 0, sizeof(odom));
        odom.utime = flaser->utime;
        odom.pos[0] = r.x;
        odom.pos[1] = r.y;
        odom.theta = r.theta;
        memcpy(odom.cov, r.sigma, 9 * sizeof(double));

        //  app->sm_incremental->drawGUI(points, numValidPoints, r, NULL,"ScanMatcher");
        
        free(points);
        add_to_queue(flaser, &odom, flaser->utime, app, force_supernode);
    }
}

//USED - Deprecated 
static void slam_command_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                                 const char * channel __attribute__((unused)), const slam_command_t * msg,
                                 void * user  __attribute__((unused)))
{
    app_t * app = (app_t *) user;
    if(msg->command == SLAM_COMMAND_T_FINISH_SLAM){
        app->finishSlam = true;
    }
    //not supported 
    else if(msg->command == SLAM_COMMAND_T_RUN_SM_TRAINING){
           app->particle_id_to_train = msg->particle_ind;
    }
    //if we get this - set flag to stop the slam and then do a proper scanmatch (also use its own local region)
}

//USED
//helper function
// Compare language_label_t by utime
bool compare_by_utime (slam_language_label_t *first, slam_language_label_t *second)
{
    if (first->utime <= second->utime)
        return true;
    else
        return false;
}

bool is_valid_id(app_t *app, int64_t id){
    for (int i=0; i < app->slam_particles.size(); i++) {
        SlamParticle *particle = app->slam_particles.at(i);
        if(id == particle->graph_id){
            return true;
        }
    }    
    return false;
}

//USED 
//publishes occupancy map 
void publish_map_id(app_t *app, int64_t id){
    for (int i=0; i < app->slam_particles.size(); i++) {
        SlamParticle *particle = app->slam_particles.at(i);
        if(id == particle->graph_id){
            int64_t s_utime = bot_timestamp_now();
            particle->printParticleInformation();
            particle->publishOccupancyMap();
            int64_t e_utime = bot_timestamp_now();
            fprintf(stderr, "Publishing Occ-map for particle : %d (%.4f)\n", (int) id, (e_utime - s_utime)/ 1.0e6);
            break;
        }
    }    
}

//------------------ LCM Handlers ---------------------------//
static void pixel_map_request_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                                      const char * channel __attribute__((unused)), 
                                      const slam_pixel_map_request_t * msg, void * user  __attribute__((unused))){
    // Currently only handle simple language
    app_t *app = (app_t *) user;
    
    //add this to the state
    g_mutex_lock(app->mutex);
    if(app->map_request != NULL){
        slam_pixel_map_request_t_destroy(app->map_request);
    }
    app->map_request = slam_pixel_map_request_t_copy(msg);
    g_mutex_unlock(app->mutex);
}

static void parsed_complex_language_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                                            const char * channel __attribute__((unused)), 
                                            const slam_slu_parse_language_querry_t * msg, void * user  __attribute__((unused))){
    
    app_t *app = (app_t *) user;
    fprintf(stderr, RED "\n\n++++++++ Complex Language Message received (%s) +++++\n" RESET_COLOR, msg->utterance);

    fprintf(stderr, "Figure : %s, Landmark : %s => Spatial Relation : %s\n", 
            msg->figure, msg->landmark, msg->sr);

    app->got_label = 1;
    // Add the label to the list and sort by utime
    g_mutex_lock (app->mutex);
    //we only add simple language here 
    app->complex_language_labels_to_add.push_back (slam_slu_parse_language_querry_t_copy (msg));
    g_mutex_unlock (app->mutex);   

    return;
}

static void slam_scan_request_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                                      const char * channel __attribute__((unused)), 
                                      const slam_pixel_map_request_t * msg, void * user  __attribute__((unused))){
    // Currently only handle simple language
    app_t *app = (app_t *) user;
    
    //add this to the state
    g_mutex_lock(app->mutex);
    if(app->scan_request != NULL){
        slam_pixel_map_request_t_destroy(app->scan_request);
    }
    app->scan_request = slam_pixel_map_request_t_copy(msg);
    g_mutex_unlock(app->mutex);
}

static void language_label_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                                   const char * channel __attribute__((unused)), 
                                   const slam_language_label_t * msg, void * user  __attribute__((unused)))
{
    app_t *app = (app_t *) user;
    fprintf(stderr, "\n\n++++++++ Language Message received (%s) +++++\n", msg->update);
    if(msg->constraint_type == SLAM_LANGUAGE_LABEL_T_TYPE_SIMPLE && msg->label == -1){
        fprintf(stderr, "Invalid Label ID\n");
        return;
    }

    if(msg->constraint_type == SLAM_LANGUAGE_LABEL_T_TYPE_COMPLEX){
        //send this querry over to SLU - to get the parsed output 
        slam_language_label_t *comp_lang = slam_language_label_t_copy(msg);
        slam_language_label_t_publish(app->lcm, "ISAM_REGION_SLU_LANGUAGE_PARSE", comp_lang);
        slam_language_label_t_destroy(comp_lang);
    }
    else{
        app->got_label = 1;
        // Add the label to the list and sort by utime
        g_mutex_lock (app->mutex);
        //we only add simple language here 
        app->language_labels_to_add.push_back (slam_language_label_t_copy (msg));
        g_mutex_unlock (app->mutex);   
    }

    return;
}

static void slu_complex_language_result_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                               const char * channel __attribute__((unused)), const slam_complex_language_result_list_t * msg,
                               void * user  __attribute__((unused)))
{
fprintf (stderr, "SLU result received\n");
    app_t * app = (app_t *) user;

    //if we get this - set flag to stop the slam and then do a proper scanmatch (also use its own local region)
    g_mutex_lock(app->mutex);
    if(app->waiting_for_slu == 1){
        fprintf(stderr, "Received outstanding SLU result\n");
        app->slu_complex_result.push_back(slam_complex_language_result_list_t_copy(msg));        
    }
    g_mutex_unlock(app->mutex);
}


static void slu_result_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                               const char * channel __attribute__((unused)), const slam_slu_result_list_t * msg,
                               void * user  __attribute__((unused)))
{
    fprintf (stderr, "SLU result received\n");
    app_t * app = (app_t *) user;

    //if we get this - set flag to stop the slam and then do a proper scanmatch (also use its own local region)
    g_mutex_lock(app->mutex);
    if(app->waiting_for_slu == 1){
        fprintf(stderr, "Received outstanding SLU result\n");
        //lets add it to the 
        //if(app->slu_result != NULL){
        //fprintf(stderr, "Error : we have an unprocessed result\n");
        //slam_slu_result_list_t_destroy(app->slu_result);
        //}
        app->slu_result.push_back(slam_slu_result_list_t_copy(msg));        
    }
    g_mutex_unlock(app->mutex);
}

static void slam_sr_answer_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                         const char * channel __attribute__((unused)), 
                         const slam_language_answer_t * msg,
                         void * user  __attribute__((unused)))
{
    app_t * app = (app_t *) user;

    slam_language_answer_t *answer = slam_language_answer_t_copy(msg);
    g_mutex_lock(app->mutex);
    fprintf(stderr, "Received answer\n");
    app->slam_sr_answers.push_back(answer);
    g_mutex_unlock(app->mutex);
}

static void language_edge_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                                  const char * channel __attribute__((unused)), const slam_language_edge_t * msg,
                                  void * user  __attribute__((unused)))
{
    fprintf (stderr, "ERROR: Should not be in language_edge_handler\n");
    app_t * app = (app_t *) user;

    //if we get this - set flag to stop the slam and then do a proper scanmatch (also use its own local region)
    g_mutex_lock(app->mutex);
    app->language_edges_to_add.push_back(slam_language_edge_t_copy(msg));
    g_mutex_unlock(app->mutex);
}

static void dirichlet_update_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), const slam_dirichlet_update_t * msg,
                                     void * user  __attribute__((unused)))
{
    app_t * app = (app_t *) user;

    g_mutex_lock(app->mutex);
    app->dirichlet_updates_to_add.push_back(slam_dirichlet_update_t_copy(msg));
    g_mutex_unlock(app->mutex);
}

//USED - Deprecated - not sure if we plan to use the door detector 
static void door_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                         const char * channel __attribute__((unused)), 
                         const object_door_list_t * msg,
                         void * user  __attribute__((unused)))
{
    app_t * app = (app_t *) user;
    
    //if we get this - set flag to stop the slam and then do a proper scanmatch (also use its own local region)
    //g_mutex_lock(app->mutex);
    if(app->door_list != NULL)
        object_door_list_t_destroy(app->door_list);
    
    app->door_list = object_door_list_t_copy(msg);
}



//USED - Deprecated - the region segmentation i checked in the particle 
static void region_transition_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                         const char * channel __attribute__((unused)), 
                         const slam_region_transition_t * msg,
                         void * user  __attribute__((unused)))
{
    app_t * app = (app_t *) user;

    slam_region_transition_t *trans = slam_region_transition_t_copy(msg);
    if(trans->type == SLAM_REGION_TRANSITION_T_TYPE_VIEWER){
        fprintf(stderr, "In playback mode - using the last laser reading\n");
        trans->utime = app->last_laser_reading_utime;
    }
    app->transitions.push_back(trans);
}

void checkObjectToClosestNode(app_t * app){
    //if the object was detected before the last NodeScan - then it should be added to the node with the 
    //closest time 

    //otherwise - it should be kept in the outstanding list 
    //search backwards 
    //should we just keep the closest detection with time?? - as this is the one that should have the 
    //least drift??
    
    //vector<perception_object_detection_t *> remaining_outstanding;
    vector<RawDetection *> remaining_outstanding;

    int break_ind = -1;

    //fprintf(stderr, "No of Outstanding Objects : %d\n", (int) app->outstanding_object_detections.size());

    //if we want to be efficient 
    //have a map<NodeScan *, RawDetection *> node_close_observation;
    map<ObjectDetection *, map<NodeScan *, RawDetection *> > node_object_observations;
    map<ObjectDetection *, map<NodeScan *, RawDetection *> >::iterator it_object;
    map<NodeScan *, RawDetection *>::iterator it_obs;
        
    for(int j=0; j < app->outstanding_object_detections.size(); j++){
        perception_object_detection_t *detection = app->outstanding_object_detections[j]->observation; 
        NodeScan *matching_node = NULL;
        
        //if the object was detected before the last node - then find the closest node in terms of time 
        //and add the object to that node scan 
        double time_gap = 1000;
        //this is in reverse order - latest first 
        
        if(app->all_slam_nodes.size() > 0 && app->all_slam_nodes[0]->utime < detection->utime_last_seen){
            //fprintf(stderr, "Nodes are all older than the current detection - breaking\n");
            break_ind = j;
            break;                
        }

        for(int i= 0; i < app->all_slam_nodes.size(); i++){
            NodeScan *nd = app->all_slam_nodes[i];
            
            double deltat = (nd->utime - detection->utime_last_seen)/1.0e6;
            
            if(deltat < 0){ //object was created after this node was created - don't search earlier than this 
                if(fabs(time_gap) > fabs(deltat)){
                    //this is the node we want - set it 
                    matching_node = nd;
                    break;
                }
            }
            else{
                time_gap = deltat;
            }
        }
        
        if(matching_node){
            //fprintf(stderr, "Found matching node : %d => Time gap : %f\n", matching_node->node_id, time_gap);
            //add this to the object node map 
            it_object = node_object_observations.find(app->outstanding_object_detections[j]->object); 
            
            if(it_object == node_object_observations.end()){
                map<NodeScan *, RawDetection *> nd_obs_map;
                nd_obs_map.insert(make_pair(matching_node, app->outstanding_object_detections[j]));
                node_object_observations.insert(make_pair(app->outstanding_object_detections[j]->object, nd_obs_map));
                fprintf(stderr, "No Entry found for Object : %d - Adding for node : %d\n",app->outstanding_object_detections[j]->object->id,  matching_node->node_id);
            }
            else{
                it_obs = it_object->second.find(matching_node);
                if(it_obs == it_object->second.end()){
                    it_object->second.insert(make_pair(matching_node, app->outstanding_object_detections[j]));
                    fprintf(stderr, "No Entry found for %d node for Object : %d - Adding\n", matching_node->node_id, app->outstanding_object_detections[j]->object->id);
                }
                else{
                    RawDetection *old_det = it_obs->second; 
                    double delta1 = fabs(old_det->observation->utime_last_seen - matching_node->utime)/1.0e6;
                    double delta2 = fabs(app->outstanding_object_detections[j]->observation->utime_last_seen - matching_node->utime)/1.0e6;
                    if(delta2 < delta1){
                        //replace 
                        it_obs->second = app->outstanding_object_detections[j];
                        fprintf(stderr, "Found closer observation (%f) for %d node for Object : %d - Adding\n", delta2, matching_node->node_id, app->outstanding_object_detections[j]->object->id);
                        delete old_det;
                    }
                    else{
                        delete app->outstanding_object_detections[j];
                    }                    
                    //found an old match - check if the new one is closer - replace if it is 
                }
            }
                

            //app->outstanding_object_detections[j]->object->updateDetection(app->outstanding_object_detections[j]->observation, matching_node);
            //maybe we should wait till everything is associated with a node - then we can just put the closest node-object pair 
            //based on time 
            //delete app->outstanding_object_detections[j];
            //we should add this to the node scan - its up to the node scan to update its current list of objects 
            //if it needs to 
            //if there is an observation from this node already - check which one is closest in time 
            //add if this is the closest 
            
        }
        else{
            remaining_outstanding.push_back(app->outstanding_object_detections[j]);
        }
    }

    //update the nodes 
    for(it_object = node_object_observations.begin(); it_object != node_object_observations.end(); it_object++){
        ObjectDetection *obj = it_object->first;
        for(it_obs = it_object->second.begin(); it_obs != it_object->second.end(); it_obs++){
            obj->updateDetection(it_obs->second->observation, it_obs->first);
            delete it_obs->second->observation;
        }
    }
        

        /*
          if(matching_node){
          //fprintf(stderr, "Found matching node : %d => Time gap : %f\n", matching_node->node_id, time_gap);
          //add this to the object node map 
          map<ObjectDetection *, map<NodeScan *, RawDetection *> >::iterator it_object = node_object_observations.find(app->outstanding_object_detections[j]->object); 
            
          if(it_object == node_object_observations.end()){
          node_object_observations.insert(make_pair(app->outstanding_object_detections[j]->object, make_pair(matching_node, app->outstanding_object_detections[j]);
          }
            

          app->outstanding_object_detections[j]->object->updateDetection(app->outstanding_object_detections[j]->observation, matching_node);
          //maybe we should wait till everything is associated with a node - then we can just put the closest node-object pair 
          //based on time 
          delete app->outstanding_object_detections[j];
          //we should add this to the node scan - its up to the node scan to update its current list of objects 
          //if it needs to 
          //if there is an observation from this node already - check which one is closest in time 
          //add if this is the closest 
            
          }
          else{
          remaining_outstanding.push_back(app->outstanding_object_detections[j]);
          }
        */

    //fprintf(stderr, "Done searching\n");

    if(break_ind >=0){
        for(int i=break_ind; i < app->outstanding_object_detections.size(); i++){
            remaining_outstanding.push_back(app->outstanding_object_detections[i]);
        }        
    }

    app->outstanding_object_detections = remaining_outstanding; 
    //for(int i=
    
}


//we don't want to collect this list - either match or update 
static void object_list_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                         const char * channel __attribute__((unused)), 
                         const perception_object_detection_list_t * msg,
                         void * user  __attribute__((unused)))
{
    app_t * app = (app_t *) user;
    //we should match to any object we have in the representation already??
    g_mutex_lock(app->mutex_objects);

    fprintf(stderr, CYAN"No of Objects received : %d\n" RESET_COLOR, msg->count);
    for(int i=0; i < msg->count; i++){
        map<int, ObjectDetection *>::iterator it= app->object_map.find(msg->objects[i].id);
        
        ObjectDetection *object = NULL;
        if(it != app->object_map.end()){
            object = it->second;
        }
        else{
            object = new ObjectDetection(msg->objects[i].id, msg->objects[i].type);
            app->object_map.insert(make_pair(msg->objects[i].id, object));
        }
        
        app->outstanding_object_detections.push_back(new RawDetection(object, perception_object_detection_t_copy(&msg->objects[i])));
    }
    //app->new_object_detections.push_back(perception_object_detection_list_t_copy(msg));

    //maybe we should be publishing observations - and they will have the unique id 
    //then it can be matched to the closest 
    //for each object we observe - we should match it to the closest node at which it was observed from 
    
    g_mutex_unlock(app->mutex_objects);
}



////////////////////////////////////////////////////////////////////
//where the laser is put to the circular buffer 
////////////////////////////////////////////////////////////////////
bot_core_planar_lidar_t *get_closest_rear_laser(app_t *app, int64_t utime){
    double min_utime = 0.5;//we dont consider if the closest laser is 0.5 s apart
    int close_ind = -1;
    bot_core_planar_lidar_t *rlaser = NULL;

    //fprintf(stderr , "Rear Laser Queue Size : %d\n", bot_ptr_circular_size(app->rear_laser_circ));

    for (int i=0; i<(bot_ptr_circular_size(app->rear_laser_circ)); i++) {
        bot_core_planar_lidar_t *laser = (bot_core_planar_lidar_t *) bot_ptr_circular_index(app->rear_laser_circ, i);
        double time_diff = fabs(laser->utime - utime)  / 1.0e6;

        if(time_diff< min_utime){
            min_utime = time_diff;
            close_ind = i;
        }
    }

    if(close_ind == -1){
        if(app->verbose){
            fprintf(stderr, "No matching rear laser scan found\n");
        }
        return NULL;
    }
  
    rlaser = (bot_core_planar_lidar_t *) bot_ptr_circular_index(app->rear_laser_circ, close_ind);
    return rlaser;
}

bot_core_planar_lidar_t *get_closest_full_laser(app_t *app, int64_t utime){
    double min_utime = 0.5;//we dont consider if the closest laser is 0.5 s apart
    int close_ind = -1;
    bot_core_planar_lidar_t *full_laser = NULL;

    //fprintf(stderr , "Rear Laser Queue Size : %d\n", bot_ptr_circular_size(app->rear_laser_circ));

    for (int i=0; i<(bot_ptr_circular_size(app->full_laser_circ)); i++) {
        bot_core_planar_lidar_t *laser = (bot_core_planar_lidar_t *) bot_ptr_circular_index(app->full_laser_circ, i);
        double time_diff = fabs(laser->utime - utime)  / 1.0e6;

        if(time_diff< min_utime){
            min_utime = time_diff;
            close_ind = i;
        }
    }

    if(close_ind == -1){
        if(app->verbose){
            fprintf(stderr, "No matching full laser scan found\n");
        }
        return NULL;
    }
  
    full_laser = (bot_core_planar_lidar_t *) bot_ptr_circular_index(app->full_laser_circ, close_ind);
    return full_laser;
}

bot_core_image_t *get_closest_image(app_t *app, int64_t utime){
    double min_utime = 5.0;//0.5;//we dont consider if the closest laser is 0.5 s apart
    int close_ind = -1;
    bot_core_image_t *image = NULL;

    for (int i=0; i<(bot_ptr_circular_size(app->image_circ)); i++) {
        bot_core_image_t *image = (bot_core_image_t *) bot_ptr_circular_index(app->image_circ, i);
        double time_diff = fabs(image->utime - utime)  / 1.0e6;

        if(time_diff< min_utime){
            min_utime = time_diff;
            close_ind = i;
        }
    }

    if(close_ind == -1){
        if(app->verbose){
            fprintf(stderr, "No matching image found\n");
        }
        return NULL;
    }
  
    image = (bot_core_image_t *) bot_ptr_circular_index(app->image_circ, close_ind);
    fprintf(stderr, "Closest Image Diff : %.3f\n", (image->utime - utime)  / 1.0e6);

    
    return image;
}

static void full_laser_handler(int64_t utime, bot_core_planar_lidar_t * msg,
                            void * user)
{
    app_t * app = (app_t *) user;
    bot_ptr_circular_add (app->full_laser_circ, bot_core_planar_lidar_t_copy(msg));
}

static void image_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), const bot_core_image_t * msg,
                            void * user  __attribute__((unused)))
{
    app_t * app = (app_t *) user;
    bot_ptr_circular_add (app->image_circ, bot_core_image_t_copy(msg));
}

static void r_laser_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), const bot_core_planar_lidar_t * msg,
                            void * user  __attribute__((unused)))
{
    app_t * app = (app_t *) user;
    
    bot_ptr_circular_add (app->rear_laser_circ, bot_core_planar_lidar_t_copy(msg));

}

static void laser_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), const bot_core_planar_lidar_t * msg,
                          void * user  __attribute__((unused)))
{
    app_t * app = (app_t *) user;

    app->last_laser_reading_utime = msg->utime;
    
    if(app->slave_mode){
        bot_ptr_circular_add (app->front_laser_circ,  bot_core_planar_lidar_t_copy(msg));
        //if there is an unhandled node init msg - check if this is it 

        if(app->node_init != NULL){
            double min_utime = 0.5;//we dont consider if the closest laser is 0.5 s apart
            int close_ind = -1;
            bot_core_planar_lidar_t *flaser = NULL;

            for (int i=0; i<(bot_ptr_circular_size(app->front_laser_circ)); i++) {
                bot_core_planar_lidar_t *laser = (bot_core_planar_lidar_t *) bot_ptr_circular_index(app->front_laser_circ, i);
                double time_diff = fabs(laser->utime - msg->utime)  / 1.0e6;

                if(time_diff< min_utime){
                    min_utime = time_diff;
                    close_ind = i;
                }
            }
            
            if(close_ind == -1){
                fprintf(stderr, "No matching laser scan found\n");
                return;
            }
            flaser = (bot_core_planar_lidar_t *) bot_ptr_circular_index(app->front_laser_circ, close_ind);
            
            process_laser(flaser,app, msg->utime);

            if(app->node_init != NULL){
                slam_init_node_t_destroy(app->node_init);
                app->node_init = NULL;
            }
        }
    }
    else{
        /*if(app->use_doorways){
            //adding laser offset
            BotTrans body_to_local;
            if (!bot_frames_get_trans_with_utime (app->frames, "body",
                                                  "local", msg->utime, 
                                                  &body_to_local)) {
                fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
                return;
            }
            
            double pos_l[2] = {body_to_local.trans_vec[0], body_to_local.trans_vec[1]};
            
            double rpy[3];
            bot_quat_to_roll_pitch_yaw(body_to_local.rot_quat, rpy);
            
            doorway_t *doorway = NULL;
            if(app->door_list)
                doorway = detect_doorway_at_pos(app->door_list, pos_l, rpy[2], -0.4, 0.8);
            
            int found_door = 0;
            if(doorway != NULL){
                found_door = 1;
            }        
            else{
            
            }

            //check the id and skip if its the same door that was added last 
            
            process_laser( (bot_core_planar_lidar_t *) msg,app, msg->utime, found_door);
            destroy_door(doorway);
        }
        else{*/
        //fprintf(stderr, CYAN "+" RESET_COLOR);
        process_laser( (bot_core_planar_lidar_t *) msg,app, msg->utime);
        //}
    }
    
    static int64_t utime_prev = msg->utime;
}


gboolean check_updated  (gpointer key, gpointer value, gpointer user_data){
    SlamGraph *graph = (SlamGraph *) value;

    if(graph->updated == 0){
        return TRUE;
    }
    return FALSE;
}


void destroy_slam_graph(gpointer data){
    SlamGraph *graph = (SlamGraph *) data;

    delete graph;
}//addSlamNode

//USED - Depricated 
//Should not be used anymore 
//basic graph that doesnt have the loop closures - used mainly for scanmatching 
int slam_on_basic_graph(app_t *app, int64_t utime){
    
    if(g_hash_table_size(app->slam_nodes) < 2){
        if(app->verbose)
            fprintf(stderr, "Not enough nodes in the graph\n");
        return -1;
    }

    int64_t start = bot_timestamp_now();
    
    //get the graph 
    int new_graph = 0;
    if(app->basic_graph == NULL){
        new_graph = 1;
        fprintf(stderr, "Graph Not found - creating new graph\n");
        app->basic_graph = new SlamGraph(app->label_info, utime);//&app->label_info, utime);
    }

    SlamGraph *graph = app->basic_graph;

    map<int, SlamNode *>::iterator it;

    GHashTableIter iter;
    gpointer key, value;
    
    g_hash_table_iter_init (&iter, app->slam_nodes);

    vector<NodeScan *> new_nodes;

    //add new nodes to the graph
    //the hash table messes the order up 
    int node_added = 0;
    
    for(int i=0; i < g_hash_table_size(app->slam_nodes); i++){
        NodeScan *pose = (NodeScan *) g_hash_table_lookup(app->slam_nodes, &i);
        if(!pose)
            continue;
        
        int node_ind = pose->node_id;
        
        it = graph->slam_nodes.find(node_ind);
        
        //check if this is valid 
        if(it != graph->slam_nodes.end()){
            SlamNode *node = it->second;
            if(app->verbose)
                fprintf(stderr, "Basic Graph Node [%d] found in Slamgraph - skipping => Slam Pose Pointer %p\n", node_ind, (void *) node->slam_pose); 
            continue;
        }

        if(app->verbose)
            fprintf(stderr, "Basic Graph Node [%d] not found - adding node\n", node_ind);
        graph->addSlamNode(pose);
        node_added =1; 
        new_nodes.push_back(pose);
    }

    if(node_added == 0)
        return -1;

    if(app->verbose){
        fprintf(stderr, "\n============================================================\n");
        fprintf(stderr, "Basic Slam graph\n");
        fprintf(stderr,"Adding basic constraints\n");
    }

    int64_t end_node = bot_timestamp_now();
    if(app->verbose)
        fprintf(stderr, "Time to add node : %f\n", (end_node - start)/1.0e6);

    //add the constraint to the origin node
    if(new_graph){
        if (app->verbose)
            fprintf(stderr, "Adding constraint to the origin\n");
        //the constraint to the prior is not going to be in the edge list // need to add that before 
        int first_node_id = 0;

        SlamNode *node = graph->slam_nodes.find(first_node_id)->second;
        
        if(node == NULL){
            fprintf(stderr, "First node not found - this should not have happened\n");
        }
        else{
            if (app->verbose)
                fprintf(stderr, "First node found in Slam graph - adding constraint\n"); 
            
            //what do we use to build the local map??
            if (node->slam_pose->inc_constraint_sm->hitpct > .7) {
                PoseToPoseTransform *p2p_constraint = node->slam_pose->inc_constraint_sm; 
                graph->addOriginConstraint(node, *p2p_constraint->transform, *p2p_constraint->noise);                
            }
            else {
                PoseToPoseTransform *p2p_constraint = node->slam_pose->inc_constraint_odom; 
                graph->addOriginConstraint(node, *p2p_constraint->transform, *p2p_constraint->noise);                     
            }
        }
    }

    if(app->verbose){
        fprintf(stderr, "Done\n");
    }
    
    //add the current to previous constraints 
    for(int i=0; i < g_hash_table_size(app->slam_nodes); i++){
        NodeScan *pose = (NodeScan *) g_hash_table_lookup(app->slam_nodes, &i);
        if(pose == NULL)
            continue;
        if(pose->inc_constraint_sm == NULL){
            fprintf(stderr, "There is no incremental constraint\n");
        }

        PoseToPoseTransform *p2p_constraint = pose->inc_constraint_sm;
        Pose2d_Pose2d_Factor* constraint;

        if(p2p_constraint->node_relative_to == NULL){
            continue;
        }

        SlamNode *node1 = graph->slam_nodes.find(pose->node_id)->second;
        SlamNode *node2 = graph->slam_nodes.find(p2p_constraint->node_relative_to->node_id)->second;

        if(node1 == NULL || node2 == NULL){
            fprintf(stderr, "Error - one or more nodes not found\n");
            continue;
        }
        
        //check if the constraint exists - and if so add the constraint 
        //node id is the constraint respective to - i.e. current_node (id_1) respective to (id_2)
        //so find the constraint if it exists 
        int has_constraint = graph->hasConstraint(node1->id);//, node2->id);
        
        if(has_constraint == -1){
            fprintf(stderr, "Crap - node not added - im going to segfault now : %d %d\n", 
                    (int) node1->id, (int) node2->id);
            continue;
        }
        if(has_constraint == 1){
            if(app->verbose){
                fprintf(stderr, "Found added constraint - skipping\n");
            }
            continue;                
        }
        
        if(app->verbose){
            fprintf(stderr, "Sucess - found the constraint :) \n");
        }
        
        graph->addConstraint(node1->id, node1, node2, node1, node2, *p2p_constraint->transform, *p2p_constraint->noise, p2p_constraint->hitpct, SLAM_GRAPH_EDGE_T_TYPE_SM_INC,  SLAM_GRAPH_EDGE_T_STATUS_SUCCESS);
        
        if(app->verbose){
            fprintf(stderr, "\tAdding new constraints - %d %d\n", (int) node1->id, (int) node2->id);
        } 
    }

    int64_t end_constraint = bot_timestamp_now();

    //run optimization 
    graph->runSlam();


    int64_t end = bot_timestamp_now();
    
    if(app->verbose)
        fprintf(stderr, "Basic Time taken : %f\n", (end - start)/1.0e6);

    //go through the constraints again and do scan matching here again
    bot_lcmgl_t *lcmgl = app->lcmgl_basic;
    //draw the map 

    if(0){//app->draw_map){
        
        bot_lcmgl_color3f(lcmgl, 0.0, 1.0, 0);

        for ( it= graph->slam_nodes.begin() ; it != graph->slam_nodes.end(); it++ ){
            SlamNode *node = it->second;
            NodeScan *pose = node->slam_pose;
            if(pose == NULL)
                continue;
            Pose2d value = node->getPose();
            double pose_l[3] = {value.x(), value.y(), 0};
            bot_lcmgl_circle(lcmgl, pose_l, .3);        

            BotTrans bodyToLocal;
            bodyToLocal.trans_vec[0] = value.x();
            bodyToLocal.trans_vec[1] = value.y();
            bodyToLocal.trans_vec[2] = 0.0;
            double rpy[3] = { 0, 0, value.t() };
            bot_roll_pitch_yaw_to_quat(rpy, bodyToLocal.rot_quat);
            double pBody[3] = { 0, 0, 0 };
            double pLocal[3];
            Scan *scan = pose->scan;

            bot_lcmgl_point_size(lcmgl, 2);
            bot_lcmgl_begin(lcmgl, GL_POINTS);
        
            for (unsigned i = 0; i < scan->numPoints; i++) {        
                pBody[0] = scan->points[i].x;
                pBody[1] = scan->points[i].y;
                bot_trans_apply_vec(&bodyToLocal, pBody, pLocal);
                bot_lcmgl_vertex3f(lcmgl, pLocal[0], pLocal[1], pLocal[2]);
            }
            bot_lcmgl_end(lcmgl);
        }

        //might be worth drawing the constraints        
        bot_lcmgl_switch_buffer(lcmgl);
    }
}

//USED 
//Renormalizes the particle weights 
int renormalize_particles(app_t *app){

    //normalize weights
    if (app->verbose)
        fprintf(stderr, "Before renormalizing\n");
    double sum_weights = 0.0;
    for(int i=0; i < app->slam_particles.size(); i++){
        SlamParticle *p = app->slam_particles.at(i);
        sum_weights += exp(p->weight);
    }
    if(sum_weights == 0){
        fprintf(stderr, "Error! Sum of weights is 0! - assigning equal weights");
        for(int i=0; i < app->slam_particles.size(); i++){
            SlamParticle *p = app->slam_particles.at(i);
            p->normalized_weight = log(1.0/(double) app->slam_particles.size());
            p->weight = p->normalized_weight;
        } 
        return 1; 
    }

    //renormalizing the weights - otherwise things can get too small and mess up 
    
    if (app->verbose)
        fprintf(stderr, "After renormalizing\n");
    for(int i=0; i < app->slam_particles.size(); i++){
        SlamParticle *p = app->slam_particles.at(i);
        p->normalized_weight = p->weight - log(sum_weights);
        p->weight = p->normalized_weight;
    } 
   
    return 1; //succeeded at renormalizing
}

//USED - not sure what this fuction is supposed to do 
void update_weights(app_t *app){
    if(app->weights != NULL){
        slam_graph_weight_list_t_destroy(app->weights);
    }
    
    app->weights = (slam_graph_weight_list_t *) calloc(1,sizeof(slam_graph_weight_list_t));
    //we reset the time when we publish along with the particles 
    app->weights->utime = bot_timestamp_now(); 
    app->weights->no_particles = app->slam_particles.size();
    app->weights->weights_before = (slam_graph_weight_t *) calloc(app->weights->no_particles, sizeof(slam_graph_weight_t));
    app->weights->weights_after = (slam_graph_weight_t *) calloc(app->weights->no_particles, sizeof(slam_graph_weight_t));

    set<pair<int, int> > curr_dist_lc, curr_lang_lc;

     //calculate N_eff
    double sum_sq_weights = 0.0;

    for(int i=0; i < app->slam_particles.size(); i++){
        SlamParticle *p = app->slam_particles.at(i);
        sum_sq_weights += pow(exp(p->normalized_weight),2);
        //p->addLoopClosuresSet(&dist_lc, &lang_lc);
        p->addLoopClosuresSet(app->dist_lc, app->lang_lc);
        p->addLoopClosuresSet(&curr_dist_lc, &curr_lang_lc);
        pair<int,int> lc_counts = p->getNoOfLoopClosures();
        app->weights->weights_before[i].id = p->graph_id;
        app->weights->weights_before[i].no_distance_lc = lc_counts.first;
        app->weights->weights_before[i].no_lang_lc = lc_counts.second;
        app->weights->weights_before[i].weight = p->normalized_weight;
        app->weights->weights_before[i].pofz = 0; //not set for now
    }

    double n_eff = 1.0 / sum_sq_weights;
    double threshold  = app->slam_particles.size() / 2.0;//app->slam_particles.size()/2.0; 
    app->weights->status = SLAM_GRAPH_WEIGHT_LIST_T_STATUS_NO_RESAMPLE;
    app->weights->n_effective = n_eff;

    for(int i=0; i < app->slam_particles.size(); i++){
        SlamParticle *p = app->slam_particles.at(i);
        p->weight = log(1/(double) app->slam_particles.size());
        app->weights->weights_after[i].id = p->graph_id;
        pair<int,int> lc_counts = p->getNoOfLoopClosures();
        app->weights->weights_after[i].no_distance_lc = lc_counts.first;
        app->weights->weights_after[i].no_lang_lc = lc_counts.second;
        app->weights->weights_after[i].weight = p->normalized_weight;
        app->weights->weights_after[i].pofz = 0; //not set for now
    }
}

//USED - This is the function used right now - but is probably broken 
int resample_particles(app_t *app, int64_t utime){

    //fprintf(stderr, MAKE_GREEN "this will show up green" RESET_COLOR "\n");
    //does this kill the diversity??

    renormalize_particles(app);

    if(app->weights != NULL){
        slam_graph_weight_list_t_destroy(app->weights);
    }
    
    app->weights = (slam_graph_weight_list_t *) calloc(1,sizeof(slam_graph_weight_list_t));
    //we reset the time when we publish along with the particles 
    app->weights->utime = bot_timestamp_now(); 
    app->weights->no_particles = app->slam_particles.size();
    app->weights->weights_before = (slam_graph_weight_t *) calloc(app->weights->no_particles, sizeof(slam_graph_weight_t));
    app->weights->weights_after = (slam_graph_weight_t *) calloc(app->weights->no_particles, sizeof(slam_graph_weight_t));


    //calculate N_eff
    double sum_sq_weights = 0.0;
    //lc's in the current particles (ignores ones that were cut out in resampling
    set<pair<int, int> > curr_dist_lc, curr_lang_lc;
    for(int i=0; i < app->slam_particles.size(); i++){
        SlamParticle *p = app->slam_particles.at(i);
        sum_sq_weights += pow(exp(p->normalized_weight),2);
        //p->addLoopClosuresSet(&dist_lc, &lang_lc);
        p->addLoopClosuresSet(app->dist_lc, app->lang_lc);
        p->addLoopClosuresSet(&curr_dist_lc, &curr_lang_lc);
        pair<int,int> lc_counts = p->getNoOfLoopClosures();
        app->weights->weights_before[i].id = p->graph_id;
        app->weights->weights_before[i].no_distance_lc = lc_counts.first;
        app->weights->weights_before[i].no_lang_lc = lc_counts.second;
        app->weights->weights_before[i].weight = p->normalized_weight;
        app->weights->weights_before[i].pofz = 0; //not set for now
    }
    
    app->weights->no_dist_lc = app->dist_lc->size();
    app->weights->no_lang_lc = app->lang_lc->size();

    app->weights->curr_no_dist_lc = curr_dist_lc.size();
    app->weights->curr_no_lang_lc = curr_lang_lc.size();
    //we need to also give a measure of how many lc's there were in total - which we can 
    //only do by checking the lc node pairs 

    double n_eff = 1.0 / sum_sq_weights;
    double threshold  = app->slam_particles.size()/ 2.0;//app->slam_particles.size()/2.0; 
    app->weights->status = SLAM_GRAPH_WEIGHT_LIST_T_STATUS_NO_RESAMPLE;
    app->weights->n_effective = n_eff;
    //if n_eff value greater than critical value
    if(n_eff <= threshold){
        app->weights->status = SLAM_GRAPH_WEIGHT_LIST_T_STATUS_RESAMPLE;
        fprintf(stderr, "\n\n+++++++++++++Resampling+++++++++++++++++++\n");
        //create a new vector
        vector<SlamParticle *> new_slam_particles;
        //sample from particles
        double *weights = new double[app->slam_particles.size()];
        for(int i=0; i<app->slam_particles.size(); i++){
            if(i==0)
                weights[i] = exp(app->slam_particles.at(i)->normalized_weight);
            else    
                weights[i] = weights[i-1]+ exp(app->slam_particles.at(i)->normalized_weight);
        }

        for(int i=0; i<app->slam_particles.size(); i++){
            fprintf(stderr, "Particle id : %d => Cumalative weight : %f\n",  i, weights[i]);
        }
        
        int *par_ids = new int[app->slam_particles.size()];
        for(int i=0; i<app->slam_particles.size(); i++)
            par_ids[i] = 0;

        for(int i=0; i<app->slam_particles.size(); i++){
            double r = rand() / (double) RAND_MAX;
            for(int j=0; j<app->slam_particles.size(); j++){
                if(j==0){
                    if(r < weights[j])
                        par_ids[j]++;
                }
                else{
                    if(r > weights[j-1] && r < weights[j])
                        par_ids[j]++;
                }                
            }
        }

        vector<SlamParticle *> to_adjust_particles;

        for(int i=0; i<app->slam_particles.size(); i++){
            int n = par_ids[i];

            if(n ==0){
                to_adjust_particles.push_back(app->slam_particles.at(i));
                //add to a list to be adjusted 
            }
        }

        for(int i=0; i<app->slam_particles.size(); i++){
            int n = par_ids[i];
            fprintf(stderr, "New particles : %d -> %d => weight : %f\n", i, n, app->slam_particles.at(i)->weight);
            if(n >0){
                //keep the old particle 
                new_slam_particles.push_back(app->slam_particles.at(i));
                       
                if(n > 1){              
                    //create copies of the same particle and add to the list
                    map<int, SlamParticle *>::iterator p_it;
                    for(int j=0; j<n-1; j++){
                        //remove from the map 
                        int p_id = to_adjust_particles.at(to_adjust_particles.size()-1)->graph_id;
                        p_it = app->slam_particles_map.find(p_id);
                        assert(p_it != app->slam_particles_map.end()); //we should be able to find it 
                        app->slam_particles_map.erase(p_it);
                        to_adjust_particles.at(to_adjust_particles.size()-1)->adjust(app->next_id, utime, app->slam_particles.at(i));
                        //insert back to map 
                        app->slam_particles_map.insert(make_pair(to_adjust_particles.at(to_adjust_particles.size()-1)->graph_id, to_adjust_particles.at(to_adjust_particles.size()-1))); 
                        to_adjust_particles.pop_back();
                        app->next_id++;
                    }
                }
            }
        }
        
        for(int i=0; i < app->slam_particles.size(); i++){
            SlamParticle *p = app->slam_particles.at(i);
            p->weight = log(1/(double) app->slam_particles.size());
            app->weights->weights_after[i].id = p->graph_id;
            pair<int,int> lc_counts = p->getNoOfLoopClosures();
            app->weights->weights_after[i].no_distance_lc = lc_counts.first;
            app->weights->weights_after[i].no_lang_lc = lc_counts.second;
            app->weights->weights_after[i].weight = p->normalized_weight;
            app->weights->weights_after[i].pofz = 0; //not set for now
        }

        fprintf(stderr, "=========== No of samples : %d - weight : %f\n", (int) app->slam_particles.size(), 
                log(1/(double) app->slam_particles.size()));

        delete par_ids;
        delete weights;

        return 1; //did resample
    }
    
    for(int i=0; i < app->slam_particles.size(); i++){
        SlamParticle *p = app->slam_particles.at(i);
        app->weights->weights_after[i].id = p->graph_id;
        app->weights->weights_after[i].weight = p->normalized_weight;
        app->weights->weights_after[i].pofz = 0; //not set for now
    }

    return 0; //didn't resample
}

//USED - Not functional 
//Uses Copy of the SLAM Particles 
int resample_particles_copy(app_t *app, int64_t utime){
    //fprintf(stderr, MAKE_GREEN "this will show up green" RESET_COLOR "\n");
    //does this kill the diversity??

    renormalize_particles(app);
    
    //calculate N_eff
    double sum_sq_weights = 0.0;
    for(int i=0; i < app->slam_particles.size(); i++){
        SlamParticle *p = app->slam_particles.at(i);
        sum_sq_weights += pow(exp(p->normalized_weight),2);
    }

    double n_eff = 1.0 / sum_sq_weights;
    double threshold  = app->slam_particles.size() / 2.0;
    //if n_eff value greater than critical value
    if(n_eff <= threshold){
        fprintf(stderr, "\n\n+++++++++++++Resampling+++++++++++++++++++\n");
        //create a new vector
        vector<SlamParticle *> new_slam_particles;
        //sample from particles
        double *weights = new double[app->slam_particles.size()];
        for(int i=0; i<app->slam_particles.size(); i++){
            if(i==0)
                weights[i] = exp(app->slam_particles.at(i)->normalized_weight);
            else    
                weights[i] = weights[i-1]+ exp(app->slam_particles.at(i)->normalized_weight);
        }

        for(int i=0; i<app->slam_particles.size(); i++){
            fprintf(stderr, "Particle id : %d => Cumalative weight : %f\n",  i, weights[i]);
        }
        
        int *par_ids = new int[app->slam_particles.size()];
        for(int i=0; i<app->slam_particles.size(); i++)
            par_ids[i] = 0;

        for(int i=0; i<app->slam_particles.size(); i++){
            double r = rand() / (double) RAND_MAX;
            for(int j=0; j<app->slam_particles.size(); j++){
                if(j==0){
                    if(r < weights[j])
                        par_ids[j]++;
                }
                else{
                    if(r > weights[j-1] && r < weights[j])
                        par_ids[j]++;
                }                
            }
        }

        //return new particles
        for(int i=0; i<app->slam_particles.size(); i++){
            int n = par_ids[i];
            fprintf(stderr, "New particles : %d -> %d => weight : %f\n", i, n, app->slam_particles.at(i)->weight);
            if(n >0){
                //keep the old particle 
                new_slam_particles.push_back(app->slam_particles.at(i));
                       
                if(n > 1){              
                    //create copies of the same particle and add to the list
                    for(int j=0; j<n-1; j++){
                        SlamParticle *part = app->slam_particles.at(i)->copy(utime, app->next_id);
                        new_slam_particles.push_back(part);
                        app->next_id++;
                    }
                }
            }
            else{
                delete app->slam_particles.at(i);
            }
        }

        app->slam_particles = new_slam_particles;
        
        for(int i=0; i < app->slam_particles.size(); i++){
            SlamParticle *p = app->slam_particles.at(i);
            p->weight = log(1/(double) app->slam_particles.size());
        }

        fprintf(stderr, "=========== No of samples : %d - weight : %f\n", (int) app->slam_particles.size(), 
                log(1/(double) app->slam_particles.size()));

        delete par_ids;
        delete weights;

        return 1; //did resample
    }
    return 0; //didn't resample
}

void clear_dead_edges(app_t *app){
    for(int i=0; i < app->slam_particles.size(); i++){
        SlamParticle *part = app->slam_particles.at(i);
        part->clearDummyConstraints();
    }
}

//USED - Needs re-implementation for updated SLAM particle 
int process_for_nodes_and_language(app_t *app, int64_t utime, vector<NodeScan *> new_nodes, 
                                   map<int, slam_language_label_t *> new_language_labels, 
                                   map<int, complex_language_event_t *> complex_language, 
                                   vector<slam_language_answer_t *> sr_answers){

    //get the graph 
    //update the slam particles by adding the new nodes and then adding edges 
    BotTrans body_to_laser;
    bot_frames_get_trans(app->frames, "body", app->chan, &body_to_laser);

    fprintf(stderr, "\nProcessing Slam Particles\n");
    if(app->slam_particles.size() == 0){
        for(int i=0; i < app->no_particles; i++){
            SlamParticle *part = new SlamParticle(utime, app->next_id, 
                                                  app->lcm,
                                                  app->sm_loop, 
                                                  app->sm_lc_low, 
                                                  body_to_laser, 
                                                  app->label_info_particles, 
                                                  app->config_params, 
                                                  app->local_px_map,
                                                  app->lcmgl_sm_basic, 
                                                  app->lcmgl_sm_graph, 
                                                  app->lcmgl_sm_prior, 
                                                  app->lcmgl_sm_result, 
                                                  app->lcmgl_sm_result_low, 
                                                  app->lcmgl_loop_closures);
            
            app->next_id++;
            
            app->slam_particles.push_back(part);
            app->slam_particles_map.insert(make_pair(part->graph_id, part));
        }
    }

    // Update the Dirichlet according to language_label_t
    fprintf(stderr, MAKE_GREEN "Num of new language labels: %d" RESET_COLOR "\n", (int) new_language_labels.size());

    double max_prob_particle = 0.0;
    
    int new_region_created = 0;
    // add language labels 
    int64_t s_utime = bot_timestamp_now();
    for(int i=0; i < app->slam_particles.size(); i++){
        double m_prob = 0; 

        SlamParticle *part = app->slam_particles.at(i);
        AddNodesAndEdgesResult result_info;
        int added_new_region = 0;
        //fprintf(stderr, "------------------------------ Particle [%d] --------------------------\n", part->graph_id);
        if(app->copyNodeScans){
            vector<NodeScan *> nd_list_copy;
            copy_slam_nodes(new_nodes, nd_list_copy); 
            added_new_region = part->addNodesAndEdges(nd_list_copy, new_language_labels, complex_language, app->probMode, app->find_ground_truth, &m_prob, app->ignoreLanguage, result_info, app->object_map, app->finishSlam);
        }
        else{
            added_new_region = part->addNodesAndEdges(new_nodes, new_language_labels, complex_language, app->probMode, app->find_ground_truth, &m_prob, app->ignoreLanguage, result_info, app->object_map, app->finishSlam);
        }

        if(added_new_region)
            new_region_created = 1;
        //fprintf(stderr, "--------------------------------------------------------------------\n");

        if(max_prob_particle < m_prob)
            max_prob_particle = m_prob;
        
    }
    int64_t e_utime_1 = bot_timestamp_now();
    if(new_nodes.size() > 0){
        app->last_graph_utime = new_nodes.at(new_nodes.size() - 1)->utime;
    }

    app->finishSlam = false;
    //dont update the weights for this - this should be done when slu has returned 
    int send_complex_language = 0;
    
    for(int i=0; i < new_nodes.size(); i++){
        NodeScan *nd = new_nodes[i];
        app->added_slam_nodes.push_back(nd);
    }

    //update the answer - there should be only one answer
    if(sr_answers.size()>0){
        fprintf(stderr, "Recieved language answers : %d\n", (int) sr_answers.size());
        //
        map<int, SlamParticle *>::iterator it_p;
        for(int k=0; k < sr_answers.size(); k++){
            //find the right particle 
            fprintf(stderr, "Answer for question id : %d Particle: %d - answer : %s\n", 
                    (int) sr_answers[k]->id, (int) sr_answers[k]->particle_id, sr_answers[k]->answer);
            it_p = app->slam_particles_map.find(sr_answers[k]->particle_id);
            if(it_p == app->slam_particles_map.end()){
                fprintf(stderr, RED "Error - No matching particle found for answer\n" RESET_COLOR);
            }
            else{
                SlamParticle *a_part = it_p->second;
                a_part->updateAnswer(sr_answers[k]->id, sr_answers[k]->answer);
            }
            slam_language_answer_t_destroy(sr_answers[k]);
        }    
    }

    if(app->outstanding_complex_language.size() > 0){
        fprintf(stderr, "Outstanding Language\n");
    }
    
    int sent = 0;
    if(app->config_params.sendUniqueParticles){
        sent = publish_outstanding_language_querries(app);
    }
    else{
        sent = publish_outstanding_language_querries_all_particles(app);
    }

    if(sent){
        g_mutex_lock(app->mutex);
        app->waiting_for_slu = 1;
        g_mutex_unlock(app->mutex);
    }
    
    if(new_region_created){
        //we should change how this is sent out 

        /*if(app->outstanding_complex_language.size() > 0){
        //lets send off the first one for now - and pop this when we get the language
        complex_language_event_t *comp_lang = app->outstanding_complex_language[0];
        //next time we send this - it should ignore any node created before this
        comp_lang->last_checked_node_id = -1; //skip any node with an id below or equal to this 

        fprintf(stderr, "Evaluating New Complex language : %s\n", comp_lang->parse_result->utterance);
        send_complex_language = 1;
        //create a querry message and send to SLU
        slam_graph_region_particle_list_t *p_msg = get_slam_region_particle_msg(app);
        slam_region_slu_querry_t *msg = (slam_region_slu_querry_t *) calloc(1, sizeof(slam_region_slu_querry_t));
        msg->utime = bot_timestamp_now();
        msg->language_event = comp_lang->language_event_id; 
        msg->last_checked_node_id = comp_lang->last_checked_node_id;
        //we need to use this uttrance node id when grounding the language 
        msg->uttrance_node_id = comp_lang->node_id;
        msg->plist = *p_msg;
        msg->utterance = strdup(comp_lang->parse_result->utterance);
        slam_region_slu_querry_t_publish(app->lcm, "ISAM_REGION_SLU_LANGUAGE", msg);
        slam_region_slu_querry_t_destroy(msg);
        //always check - since the landmark might have been missing 
        //comp_lang->last_checked_node_id = app->slam_particles.at(0)->graph->getLastSlamNode()->id;
        //need to add a lock 
        g_mutex_lock(app->mutex);
        app->waiting_for_slu = 1;
        g_mutex_unlock(app->mutex);
        }

        if(app->failed_complex_language.size() > 0){
            
        //also check if enough new regions 
        //increment the regions added count on these - a very crude measure 
        //we should also check if enough valid regions have been created near the annotated region
        for(int i=0; i < app->failed_complex_language.size(); i++){
        complex_language_event_t *comp_lang = app->failed_complex_language[i];
        comp_lang->no_of_new_regions_added += 1;
        }
        }*/
        
        /*int sent = publish_outstanding_language_querries(app);
          if(sent){
          g_mutex_lock(app->mutex);
          app->waiting_for_slu = 1;
          g_mutex_unlock(app->mutex);
          }*/
    }

    /*int outstanding_complex_lang = 0;
      g_mutex_lock(app->mutex);
      outstanding_complex_lang = app->waiting_for_slu;
      g_mutex_unlock(app->mutex);

      if(app->failed_complex_language.size() > 0 && outstanding_complex_lang == 0){     
      fprintf(stderr, "No of failed complex language : %d\n", (int) app->failed_complex_language.size());
      complex_language_event_t *max_lang_event = NULL;
      int max_no_regions_added = 0;
      for(int i=0; i < app->failed_complex_language.size(); i++){
      complex_language_event_t *comp_lang = app->failed_complex_language[i];
      if(max_no_regions_added < comp_lang->no_of_new_regions_added){
      max_no_regions_added = comp_lang->no_of_new_regions_added;
      max_lang_event = comp_lang; 
      }
      }
      //change this condition to be something else - i.e. how many new regions are within the valid boundary
      if(max_no_regions_added > 10){
      max_lang_event->no_of_new_regions_added = 0; 
      fprintf(stderr, "Re-evaluating Complex language : %s - No of regions added : %d\n", max_lang_event->parse_result->utterance, max_no_regions_added);
      slam_graph_region_particle_list_t *p_msg = get_slam_region_particle_msg(app);
      slam_region_slu_querry_t *msg = (slam_region_slu_querry_t *) calloc(1, sizeof(slam_region_slu_querry_t));
      msg->utime = bot_timestamp_now();
      msg->language_event = max_lang_event->language_event_id; 
      //we need to use this uttrance node id when grounding the language 
      msg->uttrance_node_id = max_lang_event->node_id;
      msg->last_checked_node_id = max_lang_event->last_checked_node_id;
      msg->plist = *p_msg;
      msg->utterance = strdup(max_lang_event->parse_result->utterance);
      slam_region_slu_querry_t_publish(app->lcm, "ISAM_REGION_SLU_LANGUAGE", msg);
      slam_region_slu_querry_t_destroy(msg);
      //always check - since the landmark might have been missing 
      //max_lang_event->last_checked_node_id = app->slam_particles.at(0)->graph->getLastSlamNode()->id;

      //need to add a lock 
      g_mutex_lock(app->mutex);
      app->waiting_for_slu = 1;
      g_mutex_unlock(app->mutex);
      }
      }*/

    if(!app->waiting_for_slu){
        int64_t e_utime = bot_timestamp_now();
        fprintf(stderr, RED "++++++++ Time to add edges : %.3f / Total Time : %.3f(nodes : %d) ++++++++++++\n" RESET_COLOR, (e_utime_1 - s_utime) / 1.0e6, (e_utime - s_utime) / 1.0e6, (int) new_nodes.size());

        if(max_prob_particle == 0)
            max_prob_particle = 0.1;
    
        double max_weight = -1e10;
        SlamParticle *max_particle = NULL;
        for(int i=0; i < app->slam_particles.size(); i++){
            SlamParticle *part = app->slam_particles.at(i);
            if(max_weight < part->weight){
                max_weight = part->weight;
                app->max_prob_particle_id = part->graph_id;
                max_particle = part;
            }
        }
        
        if(max_particle){
            if(app->config_params.askQuestions){
                slam_language_question_t *msg = max_particle->getQuestion();
                if(msg){
                    fprintf(stderr, "\n\n++++Asking question++++\n\n");
                    slam_language_question_t_publish(app->lcm, "SLAM_SR_QUESTION", msg);
                    slam_language_question_t_destroy(msg);
                }
            }
        }
        
    
        //use this to reweight/resample
        if(app->resample){
            if(app->useCopy)
                resample_particles_copy(app, utime);
            else
                resample_particles(app, utime);
        }
        else{
            renormalize_particles(app);
            update_weights(app);
        }

        if(app->publish_map){
            if(app->requested_map_particle_id > 0 && is_valid_id(app, app->requested_map_particle_id)){
                publish_map_id(app, app->requested_map_particle_id);
            }
            else{
                if(app->max_prob_particle_id >= 0){
                    publish_map_id(app, app->max_prob_particle_id);
                }
            }
        }
    }
    
    return 1;
}


//USED 
//returns 0 if no particles or no new message
int process_graph_particles(app_t *app){
    
    //copy over the remaining nodes - before processing the particles 
    GHashTableIter iter;
    gpointer key, value;


    vector<slam_language_answer_t *> sr_answers; 
    
    g_mutex_lock(app->mutex);
    
    
    //check if there are any requests for a map 
    if(app->map_request != NULL){
        app->requested_map_particle_id = app->map_request->particle_id;
        publish_map_id(app, app->map_request->particle_id);
        slam_pixel_map_request_t_destroy(app->map_request);
        app->map_request = NULL;
    }
    if(app->scan_request != NULL){
        publish_slampose_list(app);
        slam_pixel_map_request_t_destroy(app->scan_request);
        app->scan_request = NULL;
    }
    
    //check for any language results we were waiting for 
    if(app->waiting_for_slu == 1){
        if(app->slu_complex_result.size() > 0){
            //this should only be of size 1 
            fprintf(stderr, GREEN "------------SLU results received : %d--------------------\n" RESET_COLOR, (int) app->slu_complex_result.size());
            
                       
            for(int k=0; k < app->slu_complex_result.size(); k++){
                slam_complex_language_result_list_t *slu_result = app->slu_complex_result[k];
               
                fprintf(stderr, "Found the SLU result - processing\n");

                //landmark ids are region ids 
            
                for(int j = 0; j < slu_result->no_particles; j++){
                    slam_complex_language_result_t *lang_result = &slu_result->results[j]; 
                    //this could be valid for multiple particles now 
                    for(int i=0; i < app->slam_particles.size(); i++){
                        SlamParticle *part = app->slam_particles.at(i);                                       
                        
                        slam_complex_language_result_t *adjusted_result = NULL;
                        if(part->graph_id == lang_result->particle_id){
                            fprintf(stderr, "Found unique particle %d for language\n", part->graph_id);
                            adjusted_result = lang_result;
                        }
                        
                        bool to_adjust = false;

                        map<int, set<int> >::iterator it_part;
                        set<int>::iterator it_match;
                        if(app->config_params.sendUniqueParticles){
                            it_part = app->unique_particle_to_duplicates.find(lang_result->particle_id);
                            if(it_part != app->unique_particle_to_duplicates.end()){
                                it_match = it_part->second.find(part->graph_id);
                                if(it_match != it_part->second.end()){
                                    fprintf(stderr, "Found duplicate particle %d for language\n", part->graph_id);
                                    to_adjust = true;
                                }
                            }
                        }
                        
                        if(to_adjust){
                            adjusted_result = slam_complex_language_result_t_copy(lang_result);
                            adjusted_result->particle_id = part->graph_id;
                            map<int, region_mapping_t>::iterator it_mapping;
                            it_mapping = app->particle_to_region_map.find(part->graph_id);
                            if(it_mapping != app->particle_to_region_map.end()){
                                adjust_language_result_from_unique(adjusted_result, it_mapping->second);
                            }
                            else{
                                fprintf(stderr, "Error - Region mapping not found => Particle %d - (Mapping from)  %d\n", part->graph_id, (int) lang_result->particle_id);
                                exit(-1);
                            }                            
                        }
                        
                        if(adjusted_result){
                            int lang_grounded = part->addComplexLanguageUpdates(lang_result);
                        }
                        else{
                            fprintf(stderr, "Matching language result not found\n");
                        }
                    
                        if(to_adjust){
                            //destroy the duplicated message
                            slam_complex_language_result_t_destroy(adjusted_result);
                        }
                    }
                }

                //we need to change this model                 
                /*for(int i=0; i < app->slam_particles.size(); i++){
                    //this could be valid for multiple particles now 
                    SlamParticle *part = app->slam_particles.at(i);                                       
                    
                    slam_complex_language_result_t *lang_result = NULL;
                    for(int j = 0; j < slu_result->no_particles; j++){
                        if(slu_result->results[j].particle_id == part->graph_id){
                            lang_result = &slu_result->results[j];
                            break;
                        }
                    }

                    if(lang_result){
                        int lang_grounded = part->addComplexLanguageUpdates(lang_result);
                    }
                    
                    else{
                        fprintf(stderr, "Error : Have not found matching result => Result for particle\n");
                        for(int j = 0; j < slu_result->no_particles; j++){
                            fprintf(stderr, "Resulting Particle IDs : %d\n", (int) slu_result->results[j].particle_id);
                        }
                    }
                    }*/
                
                //should we not resample here as well???     
                app->slu_complex_result.erase(app->slu_complex_result.begin() + k);
                slam_complex_language_result_list_t_destroy(slu_result);            
            }
            fprintf(stderr, "====== Done processing SLU results ======\n");
            app->waiting_for_slu = 0;
        }
        else{
            fprintf(stderr, ".");
            g_mutex_unlock(app->mutex);
            return 2;
        }        
    }

    sr_answers = app->slam_sr_answers; 

    int print = 0;
    if(app->slam_sr_answers.size() > 0)
        print = 1;
    app->slam_sr_answers.clear();

    if(print)
        fprintf(stderr, "Size of answers : %d - %d\n", (int) sr_answers.size(), (int) app->slam_sr_answers.size());    
   
    int64_t s_utime = bot_timestamp_now();
    vector<NodeScan *> new_nodes;
            
    int added = get_new_slam_nodes(app, new_nodes);//update_slam_nodes(app);

    static int last_node_added = 0;
  
    if (added > 0){
        checkObjectToClosestNode(app);
        last_node_added = new_nodes[new_nodes.size() - 1]->node_id;
        if(app->verbose){
            fprintf(stderr, "Processing Slam poses - To Add : %d\n", added);
            fprintf (stdout, "--------- added = %d\n", added);
            fprintf(stderr, "Last node added : %d\n", last_node_added);
        }
    }

    vector<slam_language_label_t *>::iterator itr;

    //we should keep the complex language in an outstanding queue 
       
    map<int, slam_language_label_t*> node_to_label_map;

    vector<slam_language_label_t *> unhandled_language;

    int count = 0;
    for (itr = app->language_labels_to_add.begin(); itr != app->language_labels_to_add.end(); itr++) {
        slam_language_label_t *label = (slam_language_label_t *) *itr;    

        int remove = 0;

        for (int j=0; j < new_nodes.size(); j++) {
            NodeScan *node_scan = new_nodes.at(j);
            fprintf (stdout, "Checking new node with delta t = %.2f\n",
                     ((double)(node_scan->utime - label->utime))/1E6);

            if (node_scan->utime >= label->utime) {
                if(label->constraint_type == SLAM_LANGUAGE_LABEL_T_TYPE_SIMPLE){
                    //add this to the NodeScan ??  - there should be only one of these vs complex language labels
                    received_language_t *new_lang = (received_language_t *) calloc(1,sizeof(received_language_t));
                    new_lang->node_id = node_scan->node_id; 
                    new_lang->label = slam_language_label_t_copy(label);
                    app->grounded_simple_language.push_back(new_lang);
                    node_to_label_map.insert(make_pair(node_scan->node_id, label));
                }
                else{
                    fprintf(stderr, "+++++ Error - Complex language should not be here +++++\n");
                }
                remove = 1;
                fprintf (stdout, "Found new node with delta t = %.2f\n",
                         ((double)(node_scan->utime - label->utime))/1E6);
                break;
            }
        }

        //fprintf(stderr, "Done - Found : %d\n", remove);
        if (remove == 0)
            unhandled_language.push_back(label);
        count++;
        //fprintf(stderr, "Count : %d\n", count++);
    }

    app->language_labels_to_add = unhandled_language;

    vector<slam_slu_parse_language_querry_t *> new_complex_language_labels;
    vector<slam_slu_parse_language_querry_t *> unhandled_complex_language;
    vector<slam_slu_parse_language_querry_t *>::iterator itr_c;

    //handle the complex language 
    map<int, complex_language_event_t *> new_complex_language;
    int complex_language_found = 0;
    for (itr_c = app->complex_language_labels_to_add.begin(); itr_c != app->complex_language_labels_to_add.end(); itr_c++) {
        slam_slu_parse_language_querry_t *label = (slam_slu_parse_language_querry_t *) *itr_c;    

        int remove = 0;
       
        for (int j=0; j < new_nodes.size(); j++) {
            NodeScan *node_scan = new_nodes.at(j);
            fprintf (stdout, "Checking new node with delta t = %.2f\n",
                     ((double)(node_scan->utime - label->utime))/1E6);

            if (node_scan->utime >= label->utime) {
                fprintf(stderr, "+++++ Adding complex language +++++\n");
                complex_language_found++;
                //lets not add this to this map 
                complex_language_event_t *complex_lang_pair = (complex_language_event_t *) calloc(1, sizeof(complex_language_event_t));
                complex_lang_pair->node_id = node_scan->node_id;
                complex_lang_pair->parse_result = label;
                complex_lang_pair->language_event_id = app->complex_language_index;
                if(app->ignoreLanguage){
                    app->grounded_language.push_back(complex_lang_pair);
                }
                else{
                    app->outstanding_complex_language.push_back(complex_lang_pair);
                    new_complex_language.insert(make_pair(node_scan->node_id, complex_lang_pair));
                }
                app->complex_language_index++;
            
                remove = 1;
                fprintf (stdout, "Found new node with delta t = %.2f\n",
                         ((double)(node_scan->utime - label->utime))/1E6);
                break;
            }
        }

        //fprintf(stderr, "Done - Found : %d\n", remove);
        if (remove == 0)
            unhandled_complex_language.push_back(label);
    }

    app->complex_language_labels_to_add = unhandled_complex_language;
    int64_t e_utime = bot_timestamp_now();
    g_mutex_unlock(app->mutex);

    if(added)
        fprintf(stderr, YELLOW "============ Time to Add nodes %.6f ========\n" RESET_COLOR, (e_utime - s_utime) / 1.0e6);
    
    if(added == 0 && node_to_label_map.size() == 0 && complex_language_found == 0 && !app->finishSlam && sr_answers.size() == 0) {
        //no new nodes - not processing
        //fprintf(stderr, "Nothing to Do - returning\n");
        return 0; 
    }
    
    int64_t now = bot_timestamp_now();
    app->last_node_sent_utime = now;
    if(app->verbose)
        fprintf(stderr, "----------- First node set ---------- \n");
    
    // if we have any matched complex language - process it differently 
    // This will add nodes, process the simple language and distance based edges and then 
    // query SLU for the complex language 
    // After the SLU result - we will update the graph with the new langauge labels 
    // then propose new edges - and then calculate the measurements
    
    //fprintf(stderr, "New nodes to add : %d\n", new_nodes.size());

    process_for_nodes_and_language(app, app->last_node_sent_utime, new_nodes, 
                                   node_to_label_map, new_complex_language, sr_answers);

    map<int, slam_language_label_t*>::iterator it_label_map;
    for(it_label_map = node_to_label_map.begin(); it_label_map != node_to_label_map.end(); it_label_map++){
        slam_language_label_t_destroy(it_label_map->second);
    }
    
    //destroy the grounded language

    fprintf(stderr, "Added Nodes and Edges : Waiting for language : %d\n", app->waiting_for_slu);
    
    //if(!app->waiting_for_slu){
    //for drawing the loop closures 
    if(added == 2)
        bot_lcmgl_switch_buffer(app->lcmgl_loop_closures);
        
    publish_slam_region_particles(app);
    publish_slam_progress(app);
    publish_language_collection_list(app);
    //calculate the transforms ??
    publish_slam_transforms(app);
        
    if(app->keepDeadEdges ==0){
        clear_dead_edges(app);
    }
        
    // publish occupancy grid maps
    if (app->publish_occupancy_maps) {
        double dt = (now - app->publish_occupancy_maps_last_utime)/1000000.0;
        if (dt > MAP_PUBLISH_PERIOD) {
            //fprintf(stderr, "Publishing Max Map\n");
            publish_occupancy_maps (app);
            app->publish_occupancy_maps_last_utime = now;
        }
    }
    //}

    fprintf(stderr, "Done\n");

    return 1;
}

//USED 
//Laser processing thread 
static void *laser_process_thread(void *user)
{
    app_t * app = (app_t *) user;

    printf("isam-particles: laser_process_thread()\n");
    int status = 0;
    while(1){
        //sleep a bit if we have no new graph messages
        status = process_laser_queue(app);
        if(status==0)
            usleep(5000);
        else if(status == 1)
            usleep(500);
    }
}

//particle processing thread
static void *particles_thread(void *user)
{
    app_t * app = (app_t *) user;

    printf("isam-particles: loop_closure_thread()\n");
    int status = 0;
    while(1){
        status = process_graph_particles(app);
        //sleep a bit if we have no new graph messages
        if(status==0)
            usleep(5000);

        if(status==2){
            usleep(50000);
        }
    }
}

static void usage(const char *name)
{
    //please update this 
    fprintf(stderr, "usage: %s [options]\n"
            "\n"
            "  -h, --help                      Shows this help text and exits\n"
            "  -a,  --aligned                  Subscribe to aligned_laser (ROBOT_LASER) msgs\n"
            "  -c, --chan <LCM CHANNEL>        Input lcm channel default:\"LASER\" or \"ROBOT_LASER\"\n"
            "  -s, --scanmatch                 Run Incremental scan matcher every time a node gets added to map\n"
            "  -d, --draw                      Show window with scan matches \n"
            "  -p, --publish_pose              publish POSE messages\n"
            "  -v, --verbose                   Be verbose\n"
            "  -m, --mode  \"HOKUYO_UTM\"|\"SICK\" configures low-level options.\n"
            "\n"
            "Low-level laser options:\n"
            "  -A, --mask <min,max>            Mask min max angles in (radians)\n"
            "  -B, --beamskip <n>              Skip every n beams \n"
            "  -D, --decimation <value>        Spatial decimation threshold (meters?)\n"
            "  -F                              Use frames to align laser scans\n"
            "  -R                              Use regions\n"
            "  -S                              Use Slave mode - pose additions will be triggered by external source\n"
            "  -M, --range <range>             Maximum range (meters)\n", name);
}

int main(int argc, char *argv[])
{
    setlinebuf(stdout);
    app_t state; 

    //don't calloc this - we have maps and vectors - who need the constructor called to work properly
    app_t *app = &state; //(app_t *) calloc(1, sizeof(app_t));

    app->prev_odom = (sm_rigid_transform_2d_t *) calloc(1, sizeof(sm_rigid_transform_2d_t)); //set initial prev_odom to zero...
    app->prev_sm_odom = (sm_rigid_transform_2d_t *) calloc(1, sizeof(sm_rigid_transform_2d_t)); //set initial prev_odom to zero...

    app->prev_added_odom = (sm_rigid_transform_2d_t *) calloc(1, sizeof(sm_rigid_transform_2d_t)); //set initial prev_odom to zero...
    app->finishSlam = false;
    memset(app->prev_odom, 0, sizeof(sm_rigid_transform_2d_t));
    memset(app->prev_sm_odom, 0, sizeof(sm_rigid_transform_2d_t));
    app->particle_id_to_train = -1;
    app->last_laser_reading_utime = 0;
    app->complex_language_index = 0;
    app->waiting_for_slu = 0;
    int draw_scanmatch = 0;
    bool alignLaser = false;
    app->requested_map_particle_id = -1;
    app->chan = NULL;
    app->rchan = NULL;
    app->copyNodeScans = false; //this does not work propoerly yet - need to copy the other data structures in NodeScan  
    app->skipOutsideSM = false;
    app->useICP = true;
    app->doSemanticClassification = true;
    app->weights = NULL;
    app->probMode = 1;
    app->verbose = 0;
    app->verbose_level = print_node_info;
    app->publish_map = 0;
    app->map_request = NULL;
    app->scan_request = NULL;
    app->noScanTransformCheck = 0;
    app->useLowResPrior = true;
    app->door_list = NULL;
    app->dist_lc = new set<pair<int, int> >();
    app->lang_lc = new set<pair<int, int> >();
    //making these default
    app->scanmatchBeforeAdd = 1;
    app->use_frames = 1;
    app->odometryConfidence = LINEAR_DIST_TO_ADD;
    alignLaser = true;
    app->useOdom = 1;
    app->slave_mode = 0;
    app->no_particles = 2;
    // set to default values
    app->validBeamAngles[0] = -100;
    app->validBeamAngles[1] = +100;
    app->beam_skip = 3;
    app->spatialDecimationThresh = .2;
    app->maxRange = 29.7; //20.0
    app->maxUsableRange = 8;
    app->find_ground_truth = 0;
    app->useCopy = 0;

    app->sm_acceptance_threshold = 0.8;
    
    app->last_slampose_ind = -1;
    app->last_slampose_for_process_ind = -1;
    app->last_scan = NULL;

    app->node_init = NULL;
    //app->basic_graph = NULL;
    app->useOnlyRegion = true;
    app->addDummyConstraints = true;
    app->resample = 1;
    app->use_doorways = 1;
    app->keepDeadEdges = 0;
    app->publish_occupancy_maps = 1;
    app->useSMCov = 0;
    app->segmentationMethod = 1;
    /* LCM */
    app->lcm = lcm_create(NULL);
    if (!app->lcm) {
        fprintf(stderr, "ERROR: lcm_create() failed\n");
        return 1;
    }   

    Pose2d p1(1, 1, bot_to_radians(45));
    Pose2d p2(2, 2, bot_to_radians(45));
    
    Pose2d delta = p2.ominus(p1); //converted to p1 frame 

    cout << delta << endl;

    app->ignoreLanguage = 0;
   
    const char *optstring = "t:hruc:dafvmMP:wLboNODM:pn:s::ISRgC:";
    char c;
    struct option long_opts[] = { { "help", no_argument, 0, 'h' },
                                  {  "clustering_mode", required_argument, 0, 'C' },
                                  {  "icp", no_argument, 0, 'I' }, //needed
                                  { "chan", required_argument, 0, 'c' },
                                  { "no_language", required_argument, 0, 'N' },
                                  { "draw_scanmatch", no_argument, 0,'S' },
                                  { "use_doorways", no_argument, 0,'d' },
                                  { "use_fixed_cov", no_argument, 0, 'f'},
                                  { "no_low_res", no_argument, 0, 'L'},
                                  { "bad_mode", no_argument, 0,'b' },
                                  { "draw_map", no_argument, 0, 'm' },
                                  { "skip_outside_region", no_argument, 0, 'O' },
                                  { "prob_mode", required_argument, 0, 'P' },
                                  { "publish_occupancy_maps", required_argument, 0, 'M' },
                                  { "no_resample", required_argument, 0,'R' },
                                  { "strict_scanmatch", no_argument, 0,'T' },
                                  { "dummy_constraints", no_argument, 0, 'D' },    
                                  { "publish_gridmap", no_argument, 0, 'p' },
                                  { "no_particles", required_argument,   0, 'n' },
                                  { "use_odom", required_argument,   0, 'o' },
                                  { "alternate_scanmatch", no_argument, 0, 'a' },
                                  { "scanmatch", optional_argument, 0, 's' },
                                  { "wide_scan", no_argument, 0, 'w' },
                                  { "use_rear_laser", no_argument, 0, 'r' },
                                  { "verbose", no_argument, 0,'v' },
                                  { "scanmatch_threashold", required_argument, 0,'t' },
                                  { "ground_truth", no_argument, 0,'g' },
                                  { 0, 0, 0, 0 } };

    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
        switch (c) {
        case 'N':
            app->ignoreLanguage = 1;
            break;
        case 'r':
            app->rchan = strdup("SKIRT_REAR");
            break;
        case 'd':
            app->doSemanticClassification = false;
            break;
        case 'I':
            app->useICP = 1;
            break;
        case 'c':
            free(app->chan);
            app->chan = strdup(optarg);
            fprintf(stderr,"Main Laser Channel : %s\n", app->chan);
            break;
        case 'C':
            //app->useSpectralClustering = false;
            app->segmentationMethod = atoi(optarg);
            if(app->segmentationMethod == 0){
                fprintf(stderr, "Segmentation Method : Fixed Distance\n");
            }
            else if(app->segmentationMethod == 1){
                fprintf(stderr, "Segmentation Method : Sampling Spectral Segmentation\n");
            }
            else if(app->segmentationMethod == 2){
                fprintf(stderr, "Segmentation Method : Fixed Spectral Clustering\n");
            }
            else if(app->segmentationMethod == 3){
                fprintf(stderr, "Segmentation Method : Sampling Spectral Clustering - Use for ICRA results\n");
            }
            else{
                fprintf(stderr, "Segmentation Method : Unknown\n");
                exit(-1);
            }
            break;
        case 'f':
            app->useSMCov = 0;
            break;
        case 'b':
            app->bad_mode = 1;
            break;
        case 'n':
            app->no_particles = atoi(optarg);
        case 't':
            app->mode = 1;
            break;
        case 'o':
            app->scanmatchBeforeAdd = 0;
            break;
        case 'R':
            app->resample = 0;
            break;
        case 'L':
            app->useLowResPrior = false;
            break;
        case 'S':
            app->draw_scanmatch = 1;
            break;
        case 'O':
            app->skipOutsideSM = true;
            break;
        case 'w':
            app->useOnlyRegion = false;
            break;
        case 'm':
            app->sm_acceptance_threshold = atof(optarg);
            break;
        case 'v':
            app->verbose = 1;
            break;
        case 'p':
            app->publish_map = 1;
            break;
        case 's':
            app->scanmatchBeforeAdd = 1;
            if (optarg != NULL) {
                app->odometryConfidence = atof(optarg);
            }
            break;
        case 'M':
            app->publish_occupancy_maps = 1;
            break;
        case 'P':
            app->probMode = atoi(optarg);
            break;
        case 'D':
            app->keepDeadEdges = 1;
            break;
        case 'g':
            app->find_ground_truth = 1;
            app->no_particles = 1;
            break;
        case 'h':
        default:
            usage(argv[0]);
            return 1;
        }
    }

    fprintf(stderr, "Setting strict Scanmatch\n");
    
    app->classifier = semantic_classifier_create(false);
    app->param = bot_param_new_from_server(app->lcm, 1);
    app->max_prob_particle_id = -1;
    app->frames = bot_frames_get_global (app->lcm, app->param);
    app->lcmgl_basic = bot_lcmgl_init(app->lcm, "slam-particle-nodes_basic");
    app->lcmgl_frame_test = bot_lcmgl_init(app->lcm, "slam-particle-farmes_test");

    app->lcmgl_debug = bot_lcmgl_init(app->lcm, "slam-particle-nodes_debug");
    //for the basic map - transfomed 
    app->lcmgl_sm_basic = bot_lcmgl_init(app->lcm, "slam-particle-nodes_sm_basic");
    //for the actual graph - that is being scanmatched for 
    app->lcmgl_sm_graph = bot_lcmgl_init(app->lcm, "slam-particle-nodes_sm_graph");
    //original scan position 
    app->lcmgl_sm_prior = bot_lcmgl_init(app->lcm, "slam-particle-nodes_sm_prior");
    //scanmatched result
    app->lcmgl_sm_result = bot_lcmgl_init(app->lcm, "slam-particle-nodes_sm_result");
    app->lcmgl_sm_result_low = bot_lcmgl_init(app->lcm, "slam-particle-nodes_sm_result_low");

    app->lcmgl_particles = bot_lcmgl_init(app->lcm, "slam-particle-nodes_particle");
    
    app->lcmgl_covariance = bot_lcmgl_init(app->lcm, "slam-particle-nodes_covariance");

    app->lcmgl_loop_closures = bot_lcmgl_init(app->lcm, "slam-particle-nodes_loop_closures");
    app->node_count = 0;
    app->last_slampose = NULL;

    if (app->chan == NULL) {
        if (alignLaser)
            app->chan = strdup("SKIRT_FRONT");
        else
            app->chan = strdup("LASER");
    }

    if(app->segmentationMethod == 0){
        app->config_params = getFixedRegionParams(app);
    }
    else if(app->segmentationMethod == 1){
        app->config_params = getSpectralRegionParams(app);
    }
    else if(app->segmentationMethod == 2){
        app->config_params = getFixedSpectralRegionParams(app);
    }
    else if(app->segmentationMethod == 3){
        app->config_params = getSpectralRegionParamsICRA(app);
    }

    if (app->verbose) {
        if (alignLaser)
            printf("INFO: Listening for robot_laser msgs on %s\n", app->chan);
        else
            printf("INFO: Listening for laser msgs on %s\n", app->chan);

        printf("INFO: publish_map:%d\n", app->publish_map);
        printf("INFO: Max Range:%lf\n", app->maxRange);
        printf("INFO: SpatialDecimationThresh:%lf\n", app->spatialDecimationThresh);
        printf("INFO: Beam Skip:%d\n", app->beam_skip);
        printf("INFO: validRange:%f,%f\n", app->validBeamAngles[0], app->validBeamAngles[1]);
    }

    //fill_label_index_map(app);
    //print_class_ad_labels(app);
    app->label_info = new LabelInfo();
    app->label_info_particles = new LabelInfo();
    app->label_info->print();
   


    //hardcoded loop closing scan matcher params
    double metersPerPixel_lc = 0.02; //translational resolution for the brute force search
    double thetaResolution_lc = 0.01; //angular step size for the brute force search
   
    int useGradientAscentPolish_lc = 1; //use gradient descent to improve estimate after brute force search

    //Sachi - try this with resulution 5 
    int useMultires_lc = 5; // low resolution will have resolution metersPerPixel * 2^useMultiRes
    bool useThreads_lc = true;

    //hardcoded scan matcher params
    double metersPerPixel = 0.02; //translational resolution for the brute force search
    double thetaResolution = 0.02; //angular step size for the brute force search
    
    if(app->bad_mode){
        metersPerPixel_lc = 0.1;
        thetaResolution_lc = 0.05;
        metersPerPixel = 0.1;
        thetaResolution = 0.1;
    }

    sm_incremental_matching_modes_t matchingMode = SM_GRID_COORD; //use gradient descent to improve estimate after brute force search
    int useMultires = 3; // low resolution will have resolution metersPerPixel * 2^useMultiRes

    double initialSearchRangeXY = .15; //nominal range that will be searched over
    double initialSearchRangeTheta = .1;

    //SHOULD be set greater than the initialSearchRange
    double maxSearchRangeXY = .3; //if a good match isn't found I'll expand and try again up to this size...
    double maxSearchRangeTheta = .2; //if a good match isn't found I'll expand and try again up to this size...

    int maxNumScans = 1;//30; //keep around this many scans in the history
    double addScanHitThresh = .90; //add a new scan to the map when the number of "hits" drops below this

    int useThreads = 1;

    //create the actual scan matcher object
    /*app->regionslam = new RegionSlam(app->lcm, metersPerPixel_lc, 
      thetaResolution_lc, useGradientAscentPolish_lc, 
      useMultires_lc,
      draw_scanmatch , useThreads_lc, app->frames);*/
  

    //create the incremental scan matcher object
    app->sm_incremental = new ScanMatcher(metersPerPixel, thetaResolution, useMultires, useThreads, false);
    //set the scan matcher to start at pi/2...
    ScanTransform startPose;
    memset(&startPose, 0, sizeof(startPose));
    app->sm_incremental->initSuccessiveMatchingParams(maxNumScans, initialSearchRangeXY, maxSearchRangeXY,
                                                      initialSearchRangeTheta, maxSearchRangeTheta, matchingMode, addScanHitThresh, false, .3, &startPose);

    
    app->sm_loop = new ScanMatcher(metersPerPixel_lc, thetaResolution_lc, useMultires_lc, useThreads_lc, false);

    //create the SLAM scan matcher object
    app->sm = new ScanMatcher(metersPerPixel, thetaResolution, useMultires, useThreads, false);

    //set the scan matcher to start at pi/2...
    app->sm->initSuccessiveMatchingParams(maxNumScans, LINEAR_DIST_TO_ADD, LINEAR_DIST_TO_ADD, ANGULAR_DIST_TO_ADD,
                                          ANGULAR_DIST_TO_ADD, matchingMode, addScanHitThresh, false, .3, &startPose);

    //set the scan matcher to start at pi/2... cuz it looks better
    memset(&app->sm->currentPose, 0, sizeof(app->sm->currentPose));
    app->sm->currentPose.theta = M_PI / 2;
    app->sm->prevPose = app->sm->currentPose;
    app->r_laser_msg = NULL;
    app->r_utime = 0;

    //hardcoded loop closing scan matcher params
    double metersPerPixel_low_res = 0.4; //translational resolution for the brute force search
    double thetaResolution_low_res = 0.2; //angular step size for the brute force search
   
    int useGradientAscentPolish_low_res = 0; //use gradient descent to improve estimate after brute force search

    //Sachi - try this with resulution 5 
    int useMultires_low_res = 3; // low resolution will have resolution metersPerPixel * 2^useMultiRes
    bool useThreads_low_res = true;//false;

    //create the SLAM scan matcher object
    app->sm_lc_low = new ScanMatcher(metersPerPixel_low_res, thetaResolution_low_res, useMultires_low_res, useThreads_low_res, false);

    app->slam_nodes = g_hash_table_new(g_int_hash, g_int_equal);

    app->front_laser_circ = bot_ptr_circular_new (LASER_DATA_CIRC_SIZE,
                                                  circ_free_lidar_data, app);
    
    app->rear_laser_circ = bot_ptr_circular_new (LASER_DATA_CIRC_SIZE,
                                                 circ_free_lidar_data, app);

    app->full_laser_circ = bot_ptr_circular_new (LASER_DATA_CIRC_SIZE,
                                                 circ_free_lidar_data, app);

    app->image_circ = bot_ptr_circular_new (IMAGE_DATA_CIRC_SIZE,
                                                 circ_free_image_data, app);

       
    // Allocate the pixel map for the local occupancy grid
    double xy0[2] = {-15.0, -15.0};
    double xy1[2] = {15.0, 15.0};
    double res = 0.1;
    
    //passed to the particles - used for calculating pofz - check if its better to use a local copy
    app->local_px_map = new FloatPixelMap(xy0, xy1, res, 0, true, true);

    app->next_id = 0;

    if (app->sm->isUsingIPP())
        fprintf(stderr, "Using IPP\n");
    else
        fprintf(stderr, "NOT using IPP\n");

    bot_core_planar_lidar_t_subscribe(app->lcm, app->chan, laser_handler, app);

    bot_core_image_t_subscribe(app->lcm, "DRAGONFLY_IMAGE", image_handler, app);

    app->full_laser = full_laser_init(app->lcm, 360, &full_laser_handler, app);

    if(app->rchan != NULL){
        bot_core_planar_lidar_t_subscribe(app->lcm, app->rchan, r_laser_handler, app);
    }

    slam_command_t_subscribe(app->lcm, "SLAM_COMMAND", slam_command_handler, app);
    
    if(app->slave_mode){
        fprintf(stderr,"Slave mode : Subscribing to external trigger\n");
    }
    
    slam_language_label_t_subscribe (app->lcm, "LANGUAGE_LABEL", language_label_handler, app);

    slam_slu_parse_language_querry_t_subscribe (app->lcm, "SLU_LANG_PARSE_RESULT", parsed_complex_language_handler, app);

    if(app->ignoreLanguage == 1)
        fprintf(stderr, "Ignoring language\n");

    slam_pixel_map_request_t_subscribe (app->lcm, "PIXEL_MAP_REQUEST", pixel_map_request_handler, app);

    slam_pixel_map_request_t_subscribe (app->lcm, "SLAM_SCAN_REQUEST", slam_scan_request_handler, app);
    
    slam_slu_result_list_t_subscribe(app->lcm, "SLU_LANG_RESULT", slu_result_handler, app);
    
    if(app->use_doorways)
        object_door_list_t_subscribe(app->lcm, "DOOR_LIST", door_handler, app);

    slam_region_transition_t_subscribe(app->lcm, "REGION_TRANSITION", region_transition_handler, app);
    slam_language_answer_t_subscribe(app->lcm, "SLAM_SR_ANSWER", slam_sr_answer_handler, app);
    perception_object_detection_list_t_subscribe(app->lcm, "OBJECT_DETECTION_LIST", object_list_handler, app);
    slam_complex_language_result_list_t_subscribe(app->lcm, "SLU_COMPLEX_LANGUAGE_RESULT", slu_complex_language_result_handler, app);
    app->mutex = g_mutex_new();
    app->mutex_language = g_mutex_new();
    app->mutex_particles = g_mutex_new();
    app->mutex_lasers = g_mutex_new();
    app->mutex_laser_queue = g_mutex_new();
    app->mutex_objects = g_mutex_new();

    //reset the viewer 
    slam_status_t r_msg;
    r_msg.utime = bot_timestamp_now();
    r_msg.status = SLAM_STATUS_T_RESET;
    slam_status_t_publish(app->lcm, "SLAM_STATUS", &r_msg);

    //process the particles 
    pthread_create(&app->scanmatch_thread , NULL, particles_thread, app);

    //process the laser queue
    pthread_create(&app->laser_process_thread , NULL, laser_process_thread, app);

    struct sigaction new_action;
    new_action.sa_sigaction = sig_action;
    sigemptyset(&new_action.sa_mask);
    new_action.sa_flags = 0;

    sigaction(SIGINT, &new_action, NULL);
    sigaction(SIGTERM, &new_action, NULL);
    sigaction(SIGKILL, &new_action, NULL);
    sigaction(SIGHUP, &new_action, NULL);

    
    /* sit and wait for messages */
    while (still_groovy)
        lcm_handle(app->lcm);

    app_destroy(app);

    return 0;
}
