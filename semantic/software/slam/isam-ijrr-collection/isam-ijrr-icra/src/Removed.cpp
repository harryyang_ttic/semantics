 void addNodesAndEdgesSelectiveBasic(vector<NodeScan *> new_nodes, 
                                             vector<slam_language_label_t *> new_language_labels, int probMode,
                                                      int find_ground_truth, double *maximum_probability, int ignoreLanguageForEdges);
    

//adds nodes selectively and creates edges 
void addNodesAndEdgesSelective(vector<NodeScan *> new_nodes, vector<slam_language_label_t *> new_laguage_labels, 
                               int probMode, int find_ground_truth, double *maximum_probability, int ignoreLanguageForEdges);

//this method is deprecated - but it has code that can decimate nodes - which might be useful 
//for an improved region based method 
//this one relies on a signal at the time a node is created - telling it to assign it to a new region 
void SlamParticle::addNodesAndEdgesSelective(vector<NodeScan *> new_nodes, 
                                             vector<slam_language_label_t *> new_language_labels, int probMode,
                                             int find_ground_truth, double *maximum_probability, int ignoreLanguageForEdges){
    map<int, SlamNode *>::iterator it;

    SlamGraph *basic_graph = graph;
    if(useBasicGraph){
        basic_graph = sparse_graph;
    }

    // Steps:
    //  1) Add new nodes to graph (includes bleeding from previous supernode)
    //  2) Add new loop closure edges (includes bleeding between supernodes)
    //  3) Apply simple language:
    //      a) Update Dirichlet for preceding supernode
    //      b) Bleed to neighboring nodes
    //  4) Propose language edges

    int max_node_ind = -1;

    map<int, SlamNode *> lang_updated_nodes;

    int node_added = 0;

    //fprintf (stdout, "-------------------------- new_language_labels.size() = %d\n", new_language_labels.size());

    //vector<SlamNode *> nodes_to_process; 

    int edge_added = 0;

    //check through the transitions - see if any slam poses fall after it - then add a new region and then
    //hmm - can't remove it right now -0 it will mess up the other particles)

    for(int i=0; i < new_nodes.size(); i++){
        vector<SlamNode *> nodes_to_process; 
        NodeScan *pose = (NodeScan *) new_nodes.at(i);
        SlamNode *node = NULL;
        if(!pose) {
            fprintf (stdout, "addNodesAndEdges: new_node pose is NULL!\n");
            continue;
        }
        
        int node_ind = pose->node_id;

        //this should never happen 
 
        fprintf(stderr, "\n\n------------------------------ Checking to add Node : %d\n", node_ind);
        
        int add_type = 0; //type 0 - skip adding node, 1 - add normally - because the last node was also added, 2 do some special stuff - because the transforms and uncertainity are different         

        //routine 

        //First node - add by default - create new region 

        //===============================================================

        //if region transition detected - with some p prob.

        //---Propose new region for the new node 

        //---(1) New region (based on some region model)
        
        //---(2) Revisiting an old region (based on distance or appearance simmilarity)
        //---------- Scanmatch with the existing region's map 
        //----------------(if) Sucess : If new space (far from any existing node) - create new node - add to the proposed region - and add constraints to both the 
        //------------------------------previous region and the region it belongs to now (will create an inter region constraint and an intra region constraint
        //----------------------------- If old space (close to existing node in the existing region) - create a region constraint (between the existing and the
        //------------------------------previous region (will only create an inter region constraint) 
       
        //----------------(if) Failed : One option would be to treat it as a new region (and assume that maybe a new scanmatch will suceed in creating an edge) 
        //------------------------------or treat this as the old region (prob option 1 is better)
        //------------------------------Create node (esp if we trat this as a new region) 
        
        //---(3) Same as the old region (maybe we did not actually transition to a new region)
        //-----------Scanmatch with the old region (hmm - maybe these scanmatches should exclude the last node - reusing the old scans??) 
        //--------------- (if) Sucess : If new space add new node 
        //----------------(if) Failed : Add new constraint between existing nodes 
        
        //------------------------------------------------------------------------------------------------------------------//

        //if no transition detected 
        
        //--- Propose new region to merge the current region : 
        //-----------with prob p1 propose a new region - to scanmatch 
        //-----------maybe based on distance to close node which might be in a new region 
        //-----------hmm - this might be pretty intensive if this proposal happens at every node 
        //-----------(in the old one - only when there is a supernode is this region proposal considered 
        //-----------we could just make the prob smaller also - so that the new region is proposed less often 
        //-----------maybe based on distance or overlap 
        
        //If not new region - scanmatch with the current region - and either add new node or new constraint 
        
        //we need to infer the pose for this potential node - before we can check the min dist
        PoseToPoseTransform *pose_transform = NULL;

        //transform from active node 
        PoseToPoseTransform *transform_from_active_node = NULL;

        if ((scanmatchBeforeAdd) && 
            (pose->inc_constraint_sm->hitpct > inc_sm_acceptance_threshold)) {
            pose_transform = pose->inc_constraint_sm;
        }
        else{
            pose_transform = pose->inc_constraint_odom;
        }

        //get the transform from the active node
        Pose2d current_node_to_active_node = *pose_transform->transform; 
        NodeScan *last_nd = pose_transform->node_relative_to;

        fprintf(stderr, "Current constraint : %f, %f, %f\n", current_node_to_active_node.x(), current_node_to_active_node.y(), 
                current_node_to_active_node.t());
                
        //Sigma = Sigma_0 + J * Sigma_node * J'

        MatrixXd sigma_new = pose_transform->noise->sqrtinf().inverse();

        if(tfFromActiveNode){
            current_node_to_active_node = tfFromActiveNode->oplus(current_node_to_active_node);
            fprintf(stderr, "Active Transform constraint : %f, %f, %f\n", tfFromActiveNode->x(), tfFromActiveNode->y(), 
                    tfFromActiveNode->t());
        }
        if(noiseFromActiveNode){
            Matrix3d J;
            double c = cos(tfFromActiveNode->t());
            double s = sin(tfFromActiveNode->t());
            J << c , -s , 0, 
                s , c, 0, 
                0 , 0, 1;
                    
            MatrixXd sigma_c = noiseFromActiveNode->sqrtinf().inverse();
            sigma_new = sigma_c + J * sigma_new * J.transpose();
        }

        if(activeNode == NULL){
            fprintf(stderr, "First Slam Node - adding\n");
            //add to the first region 
            //we should add this node to the graph by default 
            node = addNodeToNewRegion(pose, pose_transform, NULL);
            
            activeNode = pose; 
            clearActiveNodeParams();            
        }
        else{
            SlamNode *active_node = graph->getSlamNodeFromID(activeNode->node_id);
            Pose2d active_pose = active_node->getPose();
            Pose2d current_node_pose = active_pose.oplus(current_node_to_active_node);
            Covariance noise_new(sigma_new);

            int type = SLAM_GRAPH_EDGE_T_TYPE_SM_INFERRED;
            if(pose->node_id == (activeNode->node_id+1)){
                //consecutive 
                type = SLAM_GRAPH_EDGE_T_TYPE_SM_INC;
            }
            
            PoseToPoseTransform *transform_from_active_node = new PoseToPoseTransform(&current_node_to_active_node, 
                                                                                      pose, active_node->slam_pose, 
                                                                                      //SLAM_GRAPH_EDGE_T_TYPE_SM_INFERRED, 
                                                                                      type, 
                                                                                      &noise_new, 1.0, 1.0, 1.0, 1);
            
            RegionNode *region_to_add = NULL;
            
            if(pose->transition_node){
                //propose new region 
                // Draw a uniform random number
                double u_rand = gsl_rng_uniform (rng);
                
                //these thresholds should update - based on the current map 
                
                //for now every transition creates a new region 
                double p_new = 0.3;
                double p_old = 0.5;
                double p_same = 0.2;

                //maybe this should change 
                
                if(u_rand < p_new){
                    fprintf(stderr, "Proposing New region\n");
                    
                    //create new region and add relavent constraints
                    //this is simple cause new regions will always get created - vs old proposed regions might get rejected
                    //we should add this node to the graph by default - new regions will have the node added 
                    
                    node = addNodeToNewRegion(pose, transform_from_active_node, active_node);
                
                    activeNode = pose; 
                    clearActiveNodeParams();
                    //do we still try to scanmatch?? - because does this mean a new merge?? (also we would need to be a bit more selective about scanmatches as well now 
                }
                else if(u_rand < (p_new + p_old)){
                    fprintf(stderr, "\n\n++++++++++Proposing revisiting old region++++++\n");

                    //propose an old region - do a search with the closest regions 
                    double min_dist = 1000000;

                    SlamNode *closest_slamnode = proposeOldRegion(current_node_pose, active_node->region_node, &min_dist);

                    //graph->getClosestDistanceToRegion(current_node_pose, active_node->region_node, &min_dist);
                    
                    //for now we will use the closest region - but we should probabilistically descide which region 

                    if(min_dist < 10000){
                        fprintf(stderr, "Closest Region Dist : %f - SlamNode : %p\n", min_dist, (void *)closest_slamnode);
                        
                        fprintf(stderr, "Closest Node id : %d\n", closest_slamnode->id);
                    }

                    if(min_dist < 5){
                        //lets do a scanmatch also - to see if this region is the same                         
                        ScanTransform lc_r; 
                            
                        double hitpct = 0;
                        int accept = 0;
                            
                        //a dummy cov - this should be filled by SM 
                        double cov_dummy[9] = { .02, 0, 0, 0, .02, 0, 0, 0, .01 };
                        Matrix3d cv_temp(cov_dummy);
                        Covariance cov_temp(cv_temp);
                            
                        //set the original tf to where it is infered 
                        Pose2d transf(current_node_pose);
                            
                        draw_scanmatch = true;
                                                        
                        lc_r = doScanMatchWithRegion(pose, closest_slamnode, 
                                                     &transf, &cov_temp, &accept, 
                                                     &hitpct);

                        //for now just add the previous region 
                        if(accept){
                            node = addNodeToRegion(pose, transform_from_active_node, active_node, 
                                                   closest_slamnode->region_node);
                            //add the scanmatch result to the graph also 
                            graph->addConstraint(closest_slamnode, node, closest_slamnode, node,
                                                 transf, cov_temp, 
                                                 hitpct, SLAM_GRAPH_EDGE_T_TYPE_SM_LC, 
                                                 SLAM_GRAPH_EDGE_T_STATUS_SUCCESS);
                            edge_added = 1;
                            
                            activeNode = pose; 
                            clearActiveNodeParams();
                        }
                        else{
                            //add to new region 
                            node = addNodeToNewRegion(pose, transform_from_active_node, active_node);
                            fprintf(stderr, "Adding additional constraint\n");
                            
                            activeNode = pose; 
                            clearActiveNodeParams();
                        }
                    }
                    else{
                        node = addNodeToNewRegion(pose, transform_from_active_node, active_node);
                        activeNode = pose; 
                        clearActiveNodeParams();
                    }

                    //if scanmatch with the proposed region fails - we will assume that this is a new region (least until something causes us to merge with an old region 
                    //get an old region
                    
                    //propose a new region - then try scanmatching with the closest node in the region 
                    //if it suceeds - then - add constraint and/or node to the new region 
                    
                    //if it fails add this to a new region 
                }
                
                else{
                    fprintf(stderr, "Proposing the same as the current region\n");
                    node = addNodeToRegion(pose, transform_from_active_node, active_node, active_node->region_node);
                    activeNode = pose; 
                    clearActiveNodeParams();
                    //check if there are close nodes in the same region and if so update the constraints/track 
                    //otherwise add a new node

                    //this would result in the same process as a no merge behaviour in the no transition case 
                
                }
            }
            else{
                
                //randomly propose a new region - to merge (otherwise do not merge

                //if proposed for merger - find the closest node in the region and do a scanmatch to the node 
            
                //if sucessful - merge the two regions - also add edge and/or node to the region 

                //if scanmatch fails - or if a merge region was not proposed - then track node and add if outside the region 
                //we are not proposing any new regions for now to merge 
                double u_rand = gsl_rng_uniform (rng);
                
                //these thresholds should update - based on the current map 
                
                //for now every transition creates a new region 
                double p_same = 0.7; //0.8
                double p_merge = 0.3;//1.0;

                int merge = 1;
                if(u_rand < p_same){
                    fprintf(stderr, "Not proposing to merge region\n");
                                   
                    //now check if we want to create a new node or not
                    double min_dist = 10000000;
                    SlamNode *closest_node = active_node->region_node->getClosestNode(current_node_pose, &min_dist);
                    int skip_closest_node = 0;
                
                    if(closest_node->id == (pose->node_id -1)){
                        fprintf(stderr, "Closest node is also the last node - should not include that in the scan\n");
                        skip_closest_node = 1;
                    }

                    //check how far we are from the active node interms of time ?? - this will fail if we scanmatched 
                    //to a new node - how about distance??

                    double dist_from_active = hypot(current_node_to_active_node.x(), 
                                                    current_node_to_active_node.y());

                    if(min_dist < NODE_SKIP_DISTANCE){
                        //we have a really close node to the current node - lets not add this one

                        //if we are far away from the active node - lets try to scanmatch this to the active node region 
                        //if we get a match - we will add a new constraint 
                        //if we fail - we will add it to the graph 

                        //now lets see how far the closest node is 
                        //************
                        //lets do a scanmatch also - to see if this region is the same   
                        if(dist_from_active > NODE_SKIP_SCANMATCH_DIST){
                            fprintf(stderr, "Doing scanmatch to add new constraint\n");
                      
                            ScanTransform lc_r; 
                    
                            double hitpct = 0;
                            int accept = 0;
                    
                            //a dummy cov - this should be filled by SM 
                            double cov_dummy[9] = { .02, 0, 0, 0, .02, 0, 0, 0, .01 };
                            Matrix3d cv_temp(cov_dummy);
                            Covariance cov_temp(cv_temp);
                    
                            //set the original tf to where it is infered 
                            Pose2d transf(current_node_pose);
                    
                            draw_scanmatch = true;
                    
                            if(skip_closest_node){
                                lc_r = doScanMatchWithRegionQuick(pose, closest_node, 
                                                                  &transf, &cov_temp, &accept, 
                                                                  &hitpct, closest_node);
                            }
                            else{
                                lc_r = doScanMatchWithRegionQuick(pose, closest_node, 
                                                                  &transf, &cov_temp, &accept, 
                                                                  &hitpct);
                            }
                    
                            //for now just add the previous region 
                            if(accept){
                                //what is this is the active node??? - should we ask to skip the active node???
                                if(closest_node != active_node){
                                    fprintf(stderr, "Node is too close to current node - not adding to slam graph\n");
                                    //we should still scanmatch it to the region 
                                    //minus ?? the active node??
                                    //things will get messy if the closest node is the active node 
                            
                                    //reverse the Scanmatch result 
                                    Pose2d origin(0,0,0);
                                    //Pose2d node_closest_to_current_node = origin.ominus(transf);
                                    //Pose2d node_closest_to_active_node = current_node_to_active_node.oplus(node_closest_to_current_node);
                                    Pose2d current_node_to_node_closest = origin.ominus(transf);
                                    Pose2d node_closest_to_active_node = current_node_to_active_node.oplus(transf);

                                    MatrixXd sigma_c = sigma_new; 
                            
                                    double cov_high[9] = { .02, 0, 0, 0, .02, 0, 0, 0, .01 };
                                    Matrix3d cv_high(cov_high);
                                    Covariance cov_hc(cv_high);
                            
                                    Matrix3d J;
                                    double c = cos(current_node_to_active_node.t());
                                    double s = sin(current_node_to_active_node.t());
                                    J << c , -s , 0, 
                                        s , c, 0, 
                                        0 , 0, 1; 
                                
                                    sigma_new = sigma_c + J * cv_high * J.transpose();
                                
                                    Covariance noiseNew(sigma_new);
                                
                                    graph->addConstraint(closest_node, active_node, closest_node, active_node,
                                                         node_closest_to_active_node, noiseNew, 
                                                         hitpct, SLAM_GRAPH_EDGE_T_TYPE_SM_NEW_INFO, SLAM_GRAPH_EDGE_T_STATUS_SUCCESS);
                                    edge_added = 1;
                                    activeNode = closest_node->slam_pose;
                                    tfFromActiveNode = new Pose2d(current_node_to_node_closest);
                                    noiseFromActiveNode = new Covariance(cov_hc);
                                }
                                else{
                                    fprintf(stderr, "We scanmatched with respect to the active node - because it was closest - not adding constraint\n");

                                    //we should update the transform - but not add it to the graph - bad things will happen if we do 
                                    clearActiveNodeParams();
                                    double cov_high[9] = { .02, 0, 0, 0, .02, 0, 0, 0, .01 };
                                    Matrix3d cv_high(cov_high);

                                    Pose2d origin(0,0,0);
                                    Pose2d node_closest_to_current_node = origin.ominus(transf);
                                
                                    tfFromActiveNode = new Pose2d(node_closest_to_current_node);
                                    noiseFromActiveNode = new Covariance(cv_high);                               
                                }
                            }
                            else{
                                //scanmatch failed - we are adding the node 
                                node = addNodeToRegion(pose, transform_from_active_node, active_node, active_node->region_node);
                                activeNode = pose; 
                                clearActiveNodeParams();
                            }
                        }
                        else{
                            //dont do anything to the active node 
                            //but update the transform from active and the noise 
                            tfFromActiveNode = new Pose2d(current_node_to_active_node);
                            noiseFromActiveNode = new Covariance(sigma_new);
                        }                        
                    }
                    else{
                        node = addNodeToRegion(pose, transform_from_active_node, active_node, active_node->region_node);
                        activeNode = pose; 
                        clearActiveNodeParams();
                    }
                }

                else if(u_rand < (p_same + p_merge)){
                    fprintf(stderr, "Proposing a region to merge with\n");
                    
                    //we should not always add this node 
                    /*node = addNodeToRegion(pose, transform_from_active_node, active_node, active_node->region_node);
                      activeNode = pose; 
                      clearActiveNodeParams();*/

                    //propose a scanmatch between this scan and another region 
                    //if sucessfull 
                    //maybe merge probailistically?
                    //or if the distance is too far - then add an inter region constraint
                    //propose an old region - do a search with the closest regions 
                    double min_dist = 1000000;

                    SlamNode *closest_slamnode = proposeOldRegion(current_node_pose, active_node->region_node, &min_dist);

                    //SlamNode *closest_slamnode = graph->getClosestDistanceToRegion(current_node_pose, active_node->region_node, &min_dist);
                    
                    //for now we will use the closest region - but we should probabilistically descide which region 

                    if(min_dist < 10000){
                        fprintf(stderr, "Closest Region Dist : %f - SlamNode : %p\n", min_dist, (void *)closest_slamnode);
                        
                        fprintf(stderr, "Closest Node id : %d\n", closest_slamnode->id);
                    }

                    if(min_dist < 10){
                        //can we do a cheaper test ?? - to see whether there is enough overlap between the regions to 
                        //do a scanmatch??

                        //lets do a scanmatch also - to see if this region is the same                         
                        ScanTransform lc_r; 
                        
                        double hitpct = 0;
                        int accept = 0;
                            
                        //a dummy cov - this should be filled by SM 
                        double cov_dummy[9] = { .02, 0, 0, 0, .02, 0, 0, 0, .01 };
                        Matrix3d cv_temp(cov_dummy);
                        Covariance cov_temp(cv_temp);
                            
                        //set the original tf to where it is infered 
                        Pose2d transf(current_node_pose);
                            
                        draw_scanmatch = true;
                                                        
                        lc_r = doScanMatchWithRegion(pose, closest_slamnode, 
                                                     &transf, &cov_temp, &accept, 
                                                     &hitpct);

                        //for now just add the previous region 
                        if(accept){
                            if(min_dist < NODE_SKIP_DISTANCE){
                                Pose2d origin(0,0,0);
                                Pose2d current_node_to_node_closest = origin.ominus(transf);
                                Pose2d node_closest_to_active_node = current_node_to_active_node.oplus(transf);

                                MatrixXd sigma_c = sigma_new; 
                            
                                double cov_high[9] = { .02, 0, 0, 0, .02, 0, 0, 0, .01 };
                                Matrix3d cv_high(cov_high);
                                Covariance cov_hc(cv_high);
                            
                                Matrix3d J;
                                double c = cos(current_node_to_active_node.t());
                                double s = sin(current_node_to_active_node.t());
                                J << c , -s , 0, 
                                    s , c, 0, 
                                    0 , 0, 1; 
                                
                                sigma_new = sigma_c + J * cv_high * J.transpose();
                                
                                Covariance noiseNew(sigma_new);
                                
                                graph->addConstraint(closest_slamnode, active_node, closest_slamnode, active_node,
                                                     node_closest_to_active_node, noiseNew, 
                                                     hitpct, SLAM_GRAPH_EDGE_T_TYPE_SM_NEW_INFO, SLAM_GRAPH_EDGE_T_STATUS_SUCCESS);
                                edge_added = 1;
                                graph->mergeRegions(closest_slamnode->region_node, active_node->region_node);
                                
                                activeNode = closest_slamnode->slam_pose;
                                tfFromActiveNode = new Pose2d(current_node_to_node_closest);
                                noiseFromActiveNode = new Covariance(cov_hc);
                            }
                            else{
                                //we should not always add this node 
                                node = addNodeToRegion(pose, transform_from_active_node, active_node, active_node->region_node);
                                activeNode = pose; 
                                clearActiveNodeParams();
                                //add the scanmatch result to the graph also 
                                graph->addConstraint(closest_slamnode, node, closest_slamnode, node,
                                                     transf, cov_temp, 
                                                     hitpct, SLAM_GRAPH_EDGE_T_TYPE_SM_LC, 
                                                     SLAM_GRAPH_EDGE_T_STATUS_SUCCESS);
                                edge_added = 1;
                                
                                double p_merge = get_prob_to_merge(min_dist);
                                
                                //merge the regions 
                                //do this probabilistically 
                                double u_rand_merge = gsl_rng_uniform (rng);
                                
                                //these thresholds should update - based on the current map 
                                
                                //for now every transition creates a new region 
                                if(u_rand_merge < p_merge){
                                    fprintf(stderr, "Merging regions - %d = %d\n", 
                                            closest_slamnode->region_node->region_id, 
                                            node->region_node->region_id);
                                    
                                    graph->mergeRegions(closest_slamnode->region_node, node->region_node);
                                }
                                else{
                                    fprintf(stderr, "Adding inter region constraint - but not merging\n");
                                }
                            }                                
                        }
                        else{
                            //either way - it might be worth mergin prob 
                            
                            node = addNodeToRegion(pose, transform_from_active_node, active_node, active_node->region_node);
                            activeNode = pose; 
                            clearActiveNodeParams();
                            fprintf(stderr, "Scanmatch failed - adding node to current region\n");
                        }
                    }
                    else{
                        node = addNodeToRegion(pose, transform_from_active_node, active_node, active_node->region_node);
                        activeNode = pose; 
                        clearActiveNodeParams();
                        fprintf(stderr, "Too far - not scanmatching - not doing anything\n");
                    }
                }            
            }            
        }

        fprintf(stderr, "Node Add Type = %d\n", add_type);
        
        //run optimization - basic part of the graph has changed
        if(add_type >0 || edge_added || node!= NULL){
            if(useBasicGraph){
                basic_graph->runSlam();
            }
            graph->runSlam();
        }
        if(graph->slam_node_list.size() == 1 || node == NULL){
            continue;
        }

        if(!node->slam_pose->is_supernode){
            node->processed = 1;
            if(probMode != 0){
                unprocessed_slam_nodes.push_back(node);
            }
        }
        
        else{
            nodes_to_process.push_back(node);
            unprocessed_slam_nodes.push_back(node);
        }

        //need a datastructure that reasons about region connectivity 

        if(node->slam_pose->is_supernode){
            int bled = bleedTemporal (node);
            
            if(bled){
                if (verbose)
                    fprintf(stderr, "Bleeding Temporal : %d\n", (int) node->id);
                lang_updated_nodes.insert(pair<int, SlamNode *> (node->id, node));//.push_back(node);
            }
        }

        if(verbose){
            fprintf(stderr, "\n============================================================\n");
            fprintf(stderr, "Particle : %d - New nodes to add : %d\n", 
                    graph_id, (int) new_nodes.size());
            fprintf(stderr,"Adding basic constraints\n");
        }

        // 2) Add the loop closures. When non language constraints are added between supernodes,
        //    their Dirichlet's are bled.
        //fprintf(stderr, "\n\n Remember to turn LC on \n\n");
        //addLoopClosures(basic_graph, nodes_to_process, find_ground_truth);
            
        //run slam 
        graph->status = 0;
        graph->runSlam();        
    }        

    graph->updateRegionConnections();
    graph->updateRegionMeans();
    
    //graph->printRegionConnections();

    if(verbose){
        fprintf(stderr, "Done\n");
    }
    
    //if(verbose)
    //  fprintf(stderr, "No Nodes to process : %d\n", (int) nodes_to_process.size());
    

    int64_t end_constraint = bot_timestamp_now();

    

    // 3) Process the new_language_labels by updating the Dirichlets    
    
    for (int i=0; i < new_language_labels.size(); i++) {
        slam_language_label_t *label = new_language_labels.at(i);

        int node_id = -1;
        // Look for the most recent supernode with more recent utime

        //finds the SN nodes to which the language belongs to 
        vector<SlamNode *>::reverse_iterator rit;
        for (rit = graph->slam_node_list.rbegin(); rit < graph->slam_node_list.rend(); ++rit) {
            SlamNode *node = (SlamNode *) *rit;
            if (node->slam_pose->is_supernode) {
                if (node->slam_pose->utime > label->utime)
                    node_id = node->id;
                else{ 
                    break;
                }
            }
        }
        
        if (node_id != -1) {            
            SlamNode *ref_node = (SlamNode *) graph->getSlamNodeFromID (node_id);
            if (verbose)
                fprintf(stderr, "Adding Current Language : %d\n", (int) ref_node->id);

            lang_updated_nodes.insert(pair<int, SlamNode *> (ref_node->id, ref_node));
            // Update the Dirichlet for this node

            ref_node->labeldist->addDirectObservation (label->label, LABEL_INCREMENT, language_event_count);
            if (verbose)
                fprintf (stdout, "== Incorporating direct label to update node %d\n", node_id);
            
            // Bleed to supernodes that are either directly connected (except via language constraint)
            // or connected temporally via minor nodes

            int prev_supernode_id = graph->getPreviousSupernodeID (node_id, SUPERNODE_SEARCH_DISTANCE);

            map<int, SlamConstraint *>::iterator c_it;
            for ( c_it = ref_node->constraints.begin() ; c_it != ref_node->constraints.end(); c_it++){        
                SlamConstraint *ct = c_it->second;
                
                if (ct->type == SLAM_GRAPH_EDGE_T_TYPE_LANGUAGE)
                    continue;

                int adjacent_node_id;
                SlamNode *adjacent_node;
                if (ct->node1->id == node_id)
                    adjacent_node = ct->node2;
                else
                    adjacent_node = ct->node1;

                if(prev_supernode_id == adjacent_node->id){
                    //previous one will be bled to down below
                    continue;
                }

                // If the node is a supernode, bleed to it, otherwise bleed to it's parent supernode
                // if the edge is temporal
                if (adjacent_node->slam_pose->is_supernode) {
                    lang_updated_nodes.insert(pair<int, SlamNode *> (adjacent_node->id, adjacent_node));

                    if (verbose)
                        fprintf(stderr, "Adding Connected Language : %d\n", (int) adjacent_node->id);
                    adjacent_node->labeldist->addObservation (label->label, LABEL_BLEED_INCREMENT, language_event_count);
                    if (verbose)
                        fprintf (stdout, "== Bleeding to adjacent supernode %d\n", adjacent_node->id);
                }
            }

            if (prev_supernode_id != -1) {
                SlamNode *prev_supernode = graph->getSlamNodeFromID(prev_supernode_id);
                lang_updated_nodes.insert(pair<int, SlamNode *> (prev_supernode->id, prev_supernode));

                if (verbose)
                    fprintf(stderr, "Adding Previous Language : %d\n", (int) prev_supernode->id);
                prev_supernode->labeldist->addObservation (label->label, LABEL_BLEED_INCREMENT, language_event_count);

                if (verbose)
                    fprintf (stdout, "== Bleeding to previous supernode %d\n", prev_supernode_id);
            }

            int next_supernode_id = graph->getNextSupernodeID (node_id, SUPERNODE_SEARCH_DISTANCE);
            if (next_supernode_id != -1) {
                SlamNode *next_supernode = graph->getSlamNodeFromID (next_supernode_id);
                lang_updated_nodes.insert(pair<int, SlamNode *> (next_supernode->id, next_supernode));

                if (verbose)
                    fprintf(stderr, "Adding Next Language : %d\n", (int) next_supernode->id);
                next_supernode->labeldist->addObservation (label->label, LABEL_BLEED_INCREMENT, language_event_count);

                if (verbose)
                    fprintf (stdout, "== Bleeding to next supernode %d\n", next_supernode_id);
            }
        }        
        language_event_count++;
    }

    if (verbose)
        fprintf(stderr, "++++ Nodes updated with Lang : %d\n", (int) lang_updated_nodes.size());

    // 4) Propose new language edges
    if(ignoreLanguageForEdges == 0){
        int added_lang = addLanguageEdges(basic_graph, lang_updated_nodes);
        
        if(added_lang){
            graph->status = 0;
            graph->runSlam();
        }
    }
} 


void SlamParticle::addNodesAndEdgesSelectiveBasic(vector<NodeScan *> new_nodes, 
                                                  vector<slam_language_label_t *> new_language_labels, int probMode,
                                                  int find_ground_truth, double *maximum_probability, int ignoreLanguageForEdges){
    map<int, SlamNode *>::iterator it;

    SlamGraph *basic_graph = graph;
    if(useBasicGraph){
        basic_graph = sparse_graph;
    }

    // Steps:
    //  1) Add new nodes to graph (includes bleeding from previous supernode)
    //  2) Add new loop closure edges (includes bleeding between supernodes)
    //  3) Apply simple language:
    //      a) Update Dirichlet for preceding supernode
    //      b) Bleed to neighboring nodes
    //  4) Propose language edges

    int max_node_ind = -1;

    map<int, SlamNode *> lang_updated_nodes;

    int node_added = 0;

    //fprintf (stdout, "-------------------------- new_language_labels.size() = %d\n", new_language_labels.size());

    //vector<SlamNode *> nodes_to_process; 

    int edge_added = 0;

    //check through the transitions - see if any slam poses fall after it - then add a new region and then
    //hmm - can't remove it right now -0 it will mess up the other particles)

    for(int i=0; i < new_nodes.size(); i++){
        vector<SlamNode *> nodes_to_process; 
        NodeScan *pose = (NodeScan *) new_nodes.at(i);
        SlamNode *node = NULL;
        if(!pose) {
            fprintf (stdout, "addNodesAndEdges: new_node pose is NULL!\n");
            continue;
        }
        
        int node_ind = pose->node_id;

        //this should never happen 
 
        fprintf(stderr, "\n\n------------------------------ Checking to add Node : %d\n", node_ind);
        
        int add_type = 0; //type 0 - skip adding node, 1 - add normally - because the last node was also added, 2 do some special stuff - because the transforms and uncertainity are different         

        //routine 

        //First node - add by default - create new region 

        //===============================================================

        //if region transition detected - with some p prob.

        //---Propose new region for the new node 

        //---(1) New region (based on some region model)
        
        //---(2) Revisiting an old region (based on distance or appearance simmilarity)
        //---------- Scanmatch with the existing region's map 
        //----------------(if) Sucess : If new space (far from any existing node) - create new node - add to the proposed region - and add constraints to both the 
        //------------------------------previous region and the region it belongs to now (will create an inter region constraint and an intra region constraint
        //----------------------------- If old space (close to existing node in the existing region) - create a region constraint (between the existing and the
        //------------------------------previous region (will only create an inter region constraint) 
       
        //----------------(if) Failed : One option would be to treat it as a new region (and assume that maybe a new scanmatch will suceed in creating an edge) 
        //------------------------------or treat this as the old region (prob option 1 is better)
        //------------------------------Create node (esp if we trat this as a new region) 
        
        //---(3) Same as the old region (maybe we did not actually transition to a new region)
        //-----------Scanmatch with the old region (hmm - maybe these scanmatches should exclude the last node - reusing the old scans??) 
        //--------------- (if) Sucess : If new space add new node 
        //----------------(if) Failed : Add new constraint between existing nodes 
        
        //------------------------------------------------------------------------------------------------------------------//

        //if no transition detected 
        
        //--- Propose new region to merge the current region : 
        //-----------with prob p1 propose a new region - to scanmatch 
        //-----------maybe based on distance to close node which might be in a new region 
        //-----------hmm - this might be pretty intensive if this proposal happens at every node 
        //-----------(in the old one - only when there is a supernode is this region proposal considered 
        //-----------we could just make the prob smaller also - so that the new region is proposed less often 
        //-----------maybe based on distance or overlap 
        
        //If not new region - scanmatch with the current region - and either add new node or new constraint 
        
        //we need to infer the pose for this potential node - before we can check the min dist
        PoseToPoseTransform *pose_transform = NULL;

        if(pose_transform->node_relative_to == NULL){
            fprintf(stderr, "First Scan - Adding Node and Prior\n");
            add_type = 1;
            node = addNodeToSlamGraph(pose);         
            //no need to add origin constraint 
            activeNode = pose; 
            clearActiveNodeParams();
        }
        //not the first node 
        else{
            //we should check until we find a valid SlamPose - because that is the active pose - or 
            //do we just go from the last added pose??
            SlamNode *active_node = graph->getSlamNodeFromID(activeNode->node_id);//graph->last_added_node;
            int current_id = pose_transform->node_relative_to->node_id;

            fprintf(stderr, "Node to Add : %d - Active Node id : %d\n", 
                    node_ind, active_node->id);
            
            if(active_node == NULL){
                fprintf(stderr, "First Scan - this should not happen\n");
                add_type = 1;
                node = addNodeToSlamGraph(pose);
                activeNode = pose; 
                clearActiveNodeParams();
                //break;
            }
            else{
                Pose2d active_pose = active_node->getPose();
                int active_id = active_node->id;
            
                Pose2d current_node_to_active_node = *pose_transform->transform; 
                NodeScan *last_nd = pose_transform->node_relative_to;

                fprintf(stderr, "Current constraint : %f, %f, %f\n", current_node_to_active_node.x(), current_node_to_active_node.y(), 
                        current_node_to_active_node.t());
                
                //Sigma = Sigma_0 + J * Sigma_node * J'

                MatrixXd sigma_new = pose_transform->noise->sqrtinf().inverse();

                if(tfFromActiveNode){
                    current_node_to_active_node = tfFromActiveNode->oplus(current_node_to_active_node);
                    fprintf(stderr, "Active Transform constraint : %f, %f, %f\n", tfFromActiveNode->x(), tfFromActiveNode->y(), 
                            tfFromActiveNode->t());
                }
                if(noiseFromActiveNode){
                    Matrix3d J;
                    double c = cos(tfFromActiveNode->t());
                    double s = sin(tfFromActiveNode->t());
                    J << c , -s , 0, 
                        s , c, 0, 
                        0 , 0, 1;
                    
                    MatrixXd sigma_c = noiseFromActiveNode->sqrtinf().inverse();
                    sigma_new = sigma_c + J * sigma_new * J.transpose();
                }

                Pose2d current_node_pose = active_pose.oplus(current_node_to_active_node);

                SlamNode *closest_node = NULL;
                double min_dist = 100000000;//NODE_SKIP_DISTANCE;

                //now search for the closest node 
                for(int i=0; i< graph->slam_node_list.size(); i++){
                    SlamNode *t_node = graph->getSlamNodeFromPosition(i);

                    //if(active_node->node_id == t_node->id)
                    //continue;

                    Pose2d t_pose = t_node->getPose();

                    Pose2d delta = t_pose.ominus(current_node_pose);
                    double t_dist = hypot(delta.x(), delta.y());
                    if(min_dist > t_dist){
                        min_dist = t_dist;
                        closest_node = t_node;
                    }
                }


                if(closest_node != NULL){
                    fprintf(stderr, "Closest Node found (%d) dist - %f\n", 
                            closest_node->id, min_dist);

                    //we need to do a Scanmatch with the close node or more

                    double ref_dist = hypot(current_node_pose.x(), current_node_pose.y());
                    double ref_angle = current_node_pose.t();

                    if(min_dist < NODE_SKIP_DISTANCE){
                            
                        //this should not be done to incrmental sm
                        if(closest_node->id == pose_transform->node_relative_to->node_id){
                            add_type = 0;
                            fprintf(stderr, "Closest Node is the Last node - not adding node or constraint\n");
                            //we need to update the transforms - otherwise shit will break next time 
                            clearActiveNodeParams();
                            tfFromActiveNode = new Pose2d(current_node_to_active_node);
                            noiseFromActiveNode = new Covariance(sigma_new);
                        }
                        //else 
                        else{
                            fprintf(stderr, "Closest Node does not have an inc constraint from this node - Doing SM\n");
                            //try to do a scanmatch with the closest node 
                            ScanTransform lc_r; 
                            
                            double hitpct = 0;
                            int accept = 0;
                            
                            //a dummy cov - this should be filled by SM 
                            double cov_dummy[9] = { .02, 0, 0, 0, .02, 0, 0, 0, .01 };
                            Matrix3d cv_temp(cov_dummy);
                            Covariance cov_temp(cv_temp);
                            
                            //set the original tf to where it is infered 
                            Pose2d transf(current_node_pose);
                            
                            draw_scanmatch = true;
                                                        
                            lc_r = doScanMatch(basic_graph, pose, closest_node->id,
                                               &transf, &cov_temp, &accept, 
                                               &hitpct,0);
                            draw_scanmatch = false;
                            
                            if(closest_node->id == active_node->id){
                                //Don't create these as edges - we will end up adding edges from the same node to itself 
                                add_type = 0;

                                if(accept == 0){
                                    fprintf(stderr, "Closest Node is the Last node - not adding node or constraint\n");
                                    //we need to update the transforms - otherwise shit will break next time 
                                    clearActiveNodeParams();
                                    tfFromActiveNode = new Pose2d(current_node_to_active_node);
                                    noiseFromActiveNode = new Covariance(sigma_new);
                                }
                                else{
                                    clearActiveNodeParams();
                                    //according to Hordur's stuff this will make the estimate over confident (because we already incorporated this to the map 
                                    //leaving it for now
                                    double cov_high[9] = { .02, 0, 0, 0, .02, 0, 0, 0, .01 };
                                    Matrix3d cv_high(cov_high);

                                    tfFromActiveNode = new Pose2d(transf);
                                    noiseFromActiveNode = new Covariance(cv_high);
                                }
                            }
                            else{
                                if(accept == 0){
                                    //we failed scanmatch - for now we will add the node to SLAM graph 
                                    fprintf(stderr, "Scanmatch Failed to closest node - Adding node - and infered transform \n");
                                    //this should not happen here 
                                    //this is the next node from the inc node - add the incremental constraint                                
                                    //this is not an incremental node - need to infer the constraint - and add that node 
                                    Covariance noise_new(sigma_new);
                                    fprintf(stderr, "Infered Pose Constraint : %f, %f, %f\n", current_node_to_active_node.x(), 
                                            current_node_to_active_node.y(), current_node_to_active_node.t());
                                
                                    PoseToPoseTransform * incremental_pose_transform = new PoseToPoseTransform(&current_node_to_active_node, 
                                                                                                               pose, active_node->slam_pose, 
                                                                                                               //we prob need a composite type 
                                                                                                               SLAM_GRAPH_EDGE_T_TYPE_SM_INFERRED, 
                                                                                                               &noise_new, 1.0, 1.0, 1.0, 1);
                                
                                    add_type = 2;
                                    node = addNodeToSlamGraph(pose);
                                    addConstraintToGraph(incremental_pose_transform);
                                    edge_added = 1;

                                    activeNode = pose; 
                                    clearActiveNodeParams();
                                    delete incremental_pose_transform;
                                }                        
                                //we suceed in SM the current node with a close node (which should not be the active node)
                                //we should add a constraint between the active node and close node 
                                else{                                
                                    SlamNode *active_slam_node = graph->getSlamNodeFromID(activeNode->node_id);

                                    fprintf(stderr, "Found Constraint from Active node to Closest Node - Adding constraint\n");
                                    //reverse the Scanmatch result 
                                    Pose2d origin(0,0,0);
                                    Pose2d node_closest_to_current_node = origin.ominus(transf);
                                    Pose2d node_closest_to_active_node = current_node_to_active_node.oplus(node_closest_to_current_node);
                                    MatrixXd sigma_c = sigma_new; //noiseFromActiveNode->inverse();
                                
                                    double cov_high[9] = { .02, 0, 0, 0, .02, 0, 0, 0, .01 };
                                    Matrix3d cv_high(cov_high);
                                    Covariance cov_hc(cv_high);
                                
                                    Matrix3d J;
                                    double c = cos(node_closest_to_current_node.t());
                                    double s = sin(node_closest_to_current_node.t());
                                    J << c , -s , 0, 
                                        s , c, 0, 
                                        0 , 0, 1; 
                                
                                    sigma_new = sigma_c + J * cv_high * J.transpose();
                                
                                    Covariance noiseNew(sigma_new);
                                
                                    //adding constraint between two already existing nodes 
                                    //this needs a different color 
                                    graph->addConstraint(closest_node, active_slam_node, closest_node, active_slam_node,
                                                         node_closest_to_active_node, noiseNew, 
                                                         hitpct, SLAM_GRAPH_EDGE_T_TYPE_SM_NEW_INFO, SLAM_GRAPH_EDGE_T_STATUS_SUCCESS);
                                    edge_added = 1;
                                    activeNode = closest_node->slam_pose; 
                                    clearActiveNodeParams();
                                    //according to Hordur's stuff this will make the estimate over confident (because we already incorporated this to the map 
                                    //leaving it for now
                                    tfFromActiveNode = new Pose2d(transf);
                                    noiseFromActiveNode = new Covariance(cov_hc);
                                }
                            }
                        }
                    }
                    else{
                        //this means that the active node is the previous node - and this node needs to be added to the 
                        //pose graph 
                        fprintf(stderr, "Node is outside the closest radius - Adding this node\n");
                        if(active_node->id == pose_transform->node_relative_to->node_id){
                            //actually this should just stop the search with one - this shouldn't stop checking 
                            fprintf(stderr, "Active Node is the last node - no fancy stuff needed Active : %d Current : %d\n", active_node->id, 
                                    pose_transform->node_relative_to->node_id);
                            add_type = 1;
                            node = addNodeToSlamGraph(pose);
                            if(!addIncrementalConstraintToGraph(node)){
                                fprintf(stderr, "Error adding inc constraint\n");
                            }
                            activeNode = pose; 
                            clearActiveNodeParams();
                        }
                        //the active node is not the previous node - we need to scanmatch  - if it fails - then use the infered transform 
                        //if it suceeds - then 
                        else{
                            //create a new pose transform
                            //the active node is not the last node - and this node needs to be added to the PG 
                            if(active_node->id == closest_node->id){
                                fprintf(stderr, "Actually this should check which one has the better sigma\n");
                            }
                            
                            activeNode = pose;                            
                            add_type = 2;
                            
                            //try to do a scanmatch with the closest node 
                            ScanTransform lc_r; 
                                
                            double hitpct = 0;
                            int accept = 0;
                                
                            //a dummy cov - this should be filled by SM 
                            double cov_dummy[9] = { .02, 0, 0, 0, .02, 0, 0, 0, .01 };
                            Matrix3d cv_temp(cov_dummy);
                            Covariance cov_temp(cv_temp);
                                
                            //set the original tf to where it is infered 
                            Pose2d transf(current_node_pose);
                                
                            draw_scanmatch = true;

                            lc_r = doScanMatch(basic_graph, pose, closest_node->id,
                                               &transf, &cov_temp, &accept, 
                                               &hitpct, 0);
                            draw_scanmatch = false;

                            
                            //you can't add the node before doing scanmatch 
                            node = addNodeToSlamGraph(pose);

                            if(accept){
                                vector<PoseToPoseTransform *> *v = NULL;
                                    
                                map<int, vector<PoseToPoseTransform *> *>::iterator c_it;
                                c_it = pose->sm_full_constraints.find(closest_node->slam_pose->node_id);
                                    
                                //this should not happen in the new node - the vector should be NULL 
                                if(c_it != pose->sm_full_constraints.end()){
                                    v = c_it->second;
                                }
                                    
                                double cov_high[9] = { .02, 0, 0, 0, .02, 0, 0, 0, .01 };
                                Matrix3d cv_high(cov_high);
                                Covariance cov_hc(cv_high);
                                
                                PoseToPoseTransform *constraint_tf = new PoseToPoseTransform(&transf, pose, closest_node->slam_pose, 
                                                                                             SLAM_GRAPH_EDGE_T_TYPE_SM_LC, &cov_hc, 
                                                                                             hitpct, ref_dist, ref_angle, 1);

                                fprintf(stderr, "Scanmatch Suceeded - adding node and SM constraint\n");
                                addConstraintToGraph(constraint_tf);
                                edge_added = 1;

                                activeNode = pose;
                                clearActiveNodeParams();
                                
                                if(v != NULL){
                                    //add this to the vector
                                    v->push_back(constraint_tf);
                                }
                                else{
                                    v = new vector<PoseToPoseTransform *>();
                                    v->push_back(constraint_tf);
                                    pose->sm_full_constraints.insert ( pair<int, vector<PoseToPoseTransform *> *>(closest_node->slam_pose->node_id, v));
                                }
                            }
                            else{
                                fprintf(stderr, "Scanmatch Failed - adding node and infered constraint\n");
                                Covariance noise_new(sigma_new);        

                                //this one seems to work ok
                                fprintf(stderr, "Infered Pose Constraint : %f, %f, %f\n", current_node_to_active_node.x(), 
                                        current_node_to_active_node.y(), current_node_to_active_node.t());

                                PoseToPoseTransform *incremental_pose_transform = new PoseToPoseTransform(&current_node_to_active_node, //new Pose2d(current_node_to_active_node), 
                                                                                                          pose, active_node->slam_pose, 
                                                                                                          //we prob need a composite type 
                                                                                                          SLAM_GRAPH_EDGE_T_TYPE_SM_INFERRED, 
                                                                                                          &noise_new, 1.0, 1.0, 1.0, 1);
                                
                                
                                addConstraintToGraph(incremental_pose_transform);
                                edge_added = 1;
                                activeNode = pose;
                                clearActiveNodeParams();
                                delete incremental_pose_transform;
                            }                                
                        }
                    }
                }
                else{
                    fprintf(stderr, "Error - No close node found - should not have happened\n");
                }
            }
        }

        fprintf(stderr, "Node Add Type = %d\n", add_type);

        
        
        //run optimization - basic part of the graph has changed
        if(add_type >0 || edge_added || node_added){
            if(useBasicGraph){
                basic_graph->runSlam();
            }
            graph->runSlam();
        }
        if(graph->slam_node_list.size() == 1){
            continue;
        }

        if (node == NULL){
            continue;
        }
        if(!node->slam_pose->is_supernode){
            node->processed = 1;
            if(probMode != 0){
                unprocessed_slam_nodes.push_back(node);
            }
        }
        
        else{
            nodes_to_process.push_back(node);
            unprocessed_slam_nodes.push_back(node);
        }

        if(node->slam_pose->is_supernode){
            int bled = bleedTemporal (node);
            
            if(bled){
                if (verbose)
                    fprintf(stderr, "Bleeding Temporal : %d\n", (int) node->id);
                lang_updated_nodes.insert(pair<int, SlamNode *> (node->id, node));//.push_back(node);
            }
        }

        if(verbose){
            fprintf(stderr, "\n============================================================\n");
            fprintf(stderr, "Particle : %d - New nodes to add : %d\n", 
                    graph_id, (int) new_nodes.size());
            fprintf(stderr,"Adding basic constraints\n");
        }

        // 2) Add the loop closures. When non language constraints are added between supernodes,
        //    their Dirichlet's are bled.
        //fprintf(stderr, "\n\n Remember to turn LC on \n\n");
        addLoopClosures(basic_graph, nodes_to_process, find_ground_truth);
            
        //run slam 
        graph->status = 0;
        graph->runSlam();
        
    }        

    if(verbose){
        fprintf(stderr, "Done\n");
    }
    
    //if(verbose)
    //  fprintf(stderr, "No Nodes to process : %d\n", (int) nodes_to_process.size());
    

    int64_t end_constraint = bot_timestamp_now();

    

    // 3) Process the new_language_labels by updating the Dirichlets    
    
    for (int i=0; i < new_language_labels.size(); i++) {
        slam_language_label_t *label = new_language_labels.at(i);

        int node_id = -1;
        // Look for the most recent supernode with more recent utime

        //finds the SN nodes to which the language belongs to 
        vector<SlamNode *>::reverse_iterator rit;
        for (rit = graph->slam_node_list.rbegin(); rit < graph->slam_node_list.rend(); ++rit) {
            SlamNode *node = (SlamNode *) *rit;
            if (node->slam_pose->is_supernode) {
                if (node->slam_pose->utime > label->utime)
                    node_id = node->id;
                else{ 
                    break;
                }
            }
        }
        
        if (node_id != -1) {            
            SlamNode *ref_node = (SlamNode *) graph->getSlamNodeFromID (node_id);
            if (verbose)
                fprintf(stderr, "Adding Current Language : %d\n", (int) ref_node->id);

            lang_updated_nodes.insert(pair<int, SlamNode *> (ref_node->id, ref_node));
            // Update the Dirichlet for this node

            ref_node->labeldist->addDirectObservation (label->label, LABEL_INCREMENT, language_event_count);
            if (verbose)
                fprintf (stdout, "== Incorporating direct label to update node %d\n", node_id);
            
            // Bleed to supernodes that are either directly connected (except via language constraint)
            // or connected temporally via minor nodes

            int prev_supernode_id = graph->getPreviousSupernodeID (node_id, SUPERNODE_SEARCH_DISTANCE);

            map<int, SlamConstraint *>::iterator c_it;
            for ( c_it = ref_node->constraints.begin() ; c_it != ref_node->constraints.end(); c_it++){        
                SlamConstraint *ct = c_it->second;
                
                if (ct->type == SLAM_GRAPH_EDGE_T_TYPE_LANGUAGE)
                    continue;

                int adjacent_node_id;
                SlamNode *adjacent_node;
                if (ct->node1->id == node_id)
                    adjacent_node = ct->node2;
                else
                    adjacent_node = ct->node1;

                if(prev_supernode_id == adjacent_node->id){
                    //previous one will be bled to down below
                    continue;
                }

                // If the node is a supernode, bleed to it, otherwise bleed to it's parent supernode
                // if the edge is temporal
                if (adjacent_node->slam_pose->is_supernode) {
                    lang_updated_nodes.insert(pair<int, SlamNode *> (adjacent_node->id, adjacent_node));

                    if (verbose)
                        fprintf(stderr, "Adding Connected Language : %d\n", (int) adjacent_node->id);
                    adjacent_node->labeldist->addObservation (label->label, LABEL_BLEED_INCREMENT, language_event_count);
                    if (verbose)
                        fprintf (stdout, "== Bleeding to adjacent supernode %d\n", adjacent_node->id);
                }
            }

            if (prev_supernode_id != -1) {
                SlamNode *prev_supernode = graph->getSlamNodeFromID(prev_supernode_id);
                lang_updated_nodes.insert(pair<int, SlamNode *> (prev_supernode->id, prev_supernode));

                if (verbose)
                    fprintf(stderr, "Adding Previous Language : %d\n", (int) prev_supernode->id);
                prev_supernode->labeldist->addObservation (label->label, LABEL_BLEED_INCREMENT, language_event_count);

                if (verbose)
                    fprintf (stdout, "== Bleeding to previous supernode %d\n", prev_supernode_id);
            }

            int next_supernode_id = graph->getNextSupernodeID (node_id, SUPERNODE_SEARCH_DISTANCE);
            if (next_supernode_id != -1) {
                SlamNode *next_supernode = graph->getSlamNodeFromID (next_supernode_id);
                lang_updated_nodes.insert(pair<int, SlamNode *> (next_supernode->id, next_supernode));

                if (verbose)
                    fprintf(stderr, "Adding Next Language : %d\n", (int) next_supernode->id);
                next_supernode->labeldist->addObservation (label->label, LABEL_BLEED_INCREMENT, language_event_count);

                if (verbose)
                    fprintf (stdout, "== Bleeding to next supernode %d\n", next_supernode_id);
            }
        }        
        language_event_count++;
    }

    if (verbose)
        fprintf(stderr, "++++ Nodes updated with Lang : %d\n", (int) lang_updated_nodes.size());

    // 4) Propose new language edges
    if(ignoreLanguageForEdges == 0){
        int added_lang = addLanguageEdges(basic_graph, lang_updated_nodes);
        
        if(added_lang){
            graph->status = 0;
            graph->runSlam();
        }
    }
} 
