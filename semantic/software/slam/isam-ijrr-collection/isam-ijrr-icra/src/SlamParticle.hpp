#ifndef SLAMPARTICLE_H_
#define SLAMPARTICLE_H_

#include <mrpt_sm_utils/mrpt_scanmatcher.hpp>
#include <spectral-cluster-util/spectral_cluster.hpp>
#include "SlamGraph.hpp"
#include <scanmatch/ScanMatcher.hpp>
#include <lcm/lcm.h>
#include <bot_core/bot_core.h>
#include <bot_lcmgl_client/lcmgl.h>
#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif 

#include "Utils.hpp"
#include <laser_features/laser_feature.h>
#include <lcmtypes/slam_language_edge_t.h>
#include <lcmtypes/slam_language_label_t.h>
#include <lcmtypes/slam_graph_edge_t.h>
#include <lcmtypes/slam_slu_result_list_t.h>
#include <lcmtypes/slam_pixel_map_request_t.h>
#include <lcmtypes/slam_region_transition_t.h>
#include <lcmtypes/slam_graph_region_particle_t.h>
#include <lcmtypes/slam_slu_language_querry_list_t.h>

#include "ExplorationUtils.hpp"
#include <gsl/gsl_blas.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_sf_bessel.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_randist.h>
#include <math.h>
#include <occ_map/PixelMap.hpp>
#include <laser_utils/LaserSim2D.hpp>

#define NO_OF_LANG_EDGES 4

using namespace occ_map;
using namespace laser_util;
using namespace std;
using namespace scanmatch;

typedef pair<int,double> pointDist;

class PointCorrespondence{
public:
    BotTrans transform;
    int id_1;
    int id_2;
    int matches;     

    vector<pointDist> match_list;

    PointCorrespondence(BotTrans transform_, int id_1_, int id_2_, int matches_, vector<pointDist> match_list_){
        transform = transform_;
        id_1 = id_1_;
        id_2 = id_2_;
        matches = matches_;
        match_list = match_list_;
    }
    PointCorrespondence(BotTrans transform_, int id_1_, int id_2_, int matches_){
        transform = transform_;
        id_1 = id_1_;
        id_2 = id_2_;
        matches = matches_;
    }
};

enum groundingMode {ALWAYS_GROUND = 0, GROUND_ONCE = 1, FINISH_ON_GOOD = 2};

struct ConfigurationParams{
    bool skipOutsideSM;
    bool findShortestPathEveryTime;
    bool useLaserForSemantic;
    bool useImageForSemantic; 
    bool useFactorGraphs; 
    double fixedRegionDistanceThreshold; 
    bool verbose; 
    bool bleedLabelsToRegions; 
    bool useSpectralFixed;
    bool useSemanticsForDistanceEdge; 
    bool proposeSeperateSemanticEdges;
    bool ignoreLargeScanmatchChanges;
    int polygonMode; //0 - use the mean node's laser points, 1 - use the region's convex hull, 2 - ray trace from the region and use those laser points 
    bool printDistanceScanmatch;
    bool printLanguageScanmatch;
    bool printRegionCreation;
    bool printGraphCreation;
    bool calculateRegionProb;
    bool mergeRegions;
    bool mergeRegionsOnOverlap;
    bool mergeRegionsOnCloseNodePairs;
    bool proposeEdgesFromCurrentRegion;
    bool segmentSplitThreshold;
    int segmentationMethod;
    bool checkCurrentRegionForGrounding;  //checks the current (unsegmented region) for grounding
    int minRegionSizeBeforeLanguage;
    bool segmentBasedOnOverlap;
    bool scanmatchOnSegmentEvent;
    bool scanmatchBeforeAdd;
    bool scanmatchOnlyRegion;
    bool noScanTransformCheck;
    bool scanmatchWithCurrentRegion;
    bool clearDeadEdges; 
    bool addDummyConstraints;
    bool drawScanmatch;
    bool doWideMatchOnFail;
    double smAcceptanceThreshold;
    double smCloseAcceptanceThreshold;
    double incSMAcceptanceThreshold;
    double langSMAcceptanceThreshold;
    bool askQuestions;
    groundingMode gMode; // 0 - always ground, 1 - ground once, 2 - finish when properly grounded
    bool alwaysGround;
    bool useICP;
    int noRegionEdgesToSample;
    double regionIgnoreDist;
    double regionLanguageIgnoreDist;
    bool strictScanmatch;
    bool useSMCov;
    bool useLowResPrior;
    double maxDistForLC;
    int countForBatchOptimize;
    bool skipCloseNodes;
    bool useBasicGraph;
    bool useScanmatcherUncertainity;
    bool spectralClustering; 
    int minRegionSizeBeforeScanmatch;
    bool sendUniqueParticles;
};

//make it hierachical??
struct AddNodesAndEdgesResult{
    int no_regions_created;
    int no_edges_sampled;
    int no_edges_added;
    double time_to_add_edges;
    int no_of_regions_merged;
};


struct ParticleOperationResult{
    string type;
    string comment;
    double time_taken;
    vector<ParticleOperationResult> results;
};

class SlamParticle {
public:
    SlamParticle(int64_t _utime, int _graph_id, 
                 lcm_t *_lcm,
                 ScanMatcher *_sm, 
                 ScanMatcher *_sm_low_res,
                 BotTrans _body_to_laser, 
                 LabelInfo *_label_info, 
                 ConfigurationParams params,
                 FloatPixelMap *_local_px_map,                           
                 bot_lcmgl_t *_lcmgl_sm_basic,
                 bot_lcmgl_t *_lcmgl_sm_graph,
                 bot_lcmgl_t *_lcmgl_sm_prior,
                 bot_lcmgl_t *_lcmgl_sm_result,
                 bot_lcmgl_t *_lcmgl_sm_result_low,
                 bot_lcmgl_t *_lcmgl_loop_closures);
    
    ~SlamParticle();

    bool compareToParticle(SlamParticle *part);
    
    bool compareToParticle(SlamParticle *part, map<int, int> &region_map);

    int getCorrespondance(BotTrans node_match_to_target, SlamNode *target, SlamNode *match, vector<pointDist> *match_list);
    
    double getPointOverlap(vector<SlamNode *> list_i, vector<SlamNode *> list_j);
    
    double getPointOverlap(RegionNode *r_i, RegionNode *r_j);

    vector<regionDist> getOverlapWithRegions(RegionNode *current_region, vector<SlamNode *> nodes, double max_dist);

    SlamParticle *copy(int64_t utime, int new_id);

    //take the given particle and adjust the graph to match the given particle - doesn't look like its being used 
    void adjust(int id, int64_t utime, SlamParticle *part);
    
    ScanTransform doScanMatchLanguageICP(SlamGraph *basic_graph, int64_t node_id_1, int64_t node_id_2, 
                                         Pose2d *transf, Noise *cov_hc, int *accepted, 
                                         int64_t *used_ind_1, int64_t *used_ind_2, 
                                         double *hit_percentage, int use_prior);

    //does scanmatch between two nodes
    void drawBasicMapInParticleFrame(SlamGraph *basic_graph, int valid_supernode1, int valid_supernode2, int start_ind, int end_ind, BotTrans base_frame_to_particle_frame);
    //draw the prior
    void drawScan(bot_lcmgl_t *lcmgl, NodeScan *scan, ScanTransform T, double color[3]);

    Pose2d getLastPose();
    BotTrans getBotTransForCurrentNode();

    void sampleSemanticEdgesFromRegion(RegionNode *new_region, vector<RegionNode *> regions_to_check_for_semantic);
    
    int scanmatchRegions(SlamNode *c_nd, SlamNode *p_nd, bool add_dummy_regions, bool skip_odometry=false, bool use_cache=false, bool ignore_large_diff=false);
    
    int scanmatchRegionsExtensive(SlamNode *c_nd, SlamNode *p_nd, bool add_dummy_regions, int *large_change, bool skip_odometry=false);
    
    void drawPoints(bot_lcmgl_t *lcmgl, pointlist2d_t *list, ScanTransform *T, double color[3], int transform); 

    double getUpdatedCorrespondenceScore(SlamNode *nd1, SlamNode *nd2);
    
    PointCorrespondence *getUpdatedCorrespondence(SlamNode *nd_i, SlamNode *nd_j);
    
    void replaceCorrespondenceScore(SlamNode *nd_i, SlamNode *nd_j, int matched);

    int addScansToScanMatcher(SlamGraph *basic_graph, ScanMatcher *sm, int start_ind, 
                              int end_ind, int region_supernode_id, 
                              int match_supernode_id, 
                              BotTrans base_frame_to_particle_frame);
    
    int getClosestNodes(int sn_id_1, int sn_id_2, int *cl_ind_1, int *cl_ind_2, double *dist);//SlamNode *cl_node1, SlamNode *cl_node2);

    ScanTransform doScanMatch(SlamGraph *basic_graph, int64_t node_id_1, int64_t node_id_2,
                              Pose2d *transf, 
                              Noise *cov_hc, int *accepted, int64_t *used_ind_1, int64_t *used_ind_2, double *hit_percentage, int exhaustive_search);

    ScanTransform doScanMatch(SlamGraph *basic_graph, NodeScan *node_to_check, int64_t target_node_region, 
                              Pose2d *transf, Noise *cov_hc, int *accepted, 
                              double *hit_percentage, int exhaustive_scan);

    void printScanSizes();

    int findExplorationFrontiers(RegionNode *region);

    ScanTransform doScanMatchWithRegion(NodeScan *pose_to_match, 
                                        SlamNode *target_node, Pose2d *transf, 
                                        Noise *cov_hc, int *accepted, 
                                        double *hit_percentage, SlamNode *node_to_skip);
    
    int addLanguageEdges(SlamGraph *basic_graph, map<int, SlamNode *> lang_updated_nodes);
    
    int addComplexLanguageUpdates(slam_complex_language_result_t *slu_result); 
    int addComplexLanguageUpdates(slam_slu_result_t *slu_msg); 

    //adds nodes and creates edges 
    int addNodesAndEdges(vector<NodeScan *> new_nodes, map<int, slam_language_label_t *> new_laguage_labels, 
                         map<int, complex_language_event_t *> complex_language, int probMode, 
                         int find_ground_truth, double *maximum_probability, int ignoreLanguageForEdges, 
                         AddNodesAndEdgesResult &result_info, map<int, ObjectDetection *> &objects, bool finishSlam=false);

    int addNodesAndEdgesIJRRMethod(vector<NodeScan *> new_nodes, 
                                   map<int, slam_language_label_t *> new_language_labels, 
                                   map<int, complex_language_event_t *> complex_language, int probMode,
                                   int find_ground_truth, double *maximum_probability, 
                                   int ignoreLanguageForEdges, AddNodesAndEdgesResult &result_info, 
                                   map<int, ObjectDetection *> &objects, bool finishSlam);
    
    int addNodesAndEdgesUniversalMethod(vector<NodeScan *> new_nodes,  map<int, slam_language_label_t *> new_language_labels, 
                                        map<int, complex_language_event_t *> complex_language, int probMode,
                                        int find_ground_truth, double *maximum_probability, int ignoreLanguageForEdges, 
                                        AddNodesAndEdgesResult &result_info, map<int, ObjectDetection *> &objects, bool finishSlam);

    void addNodesAndEdgesSelectiveSpectralProposeOldRegion(vector<NodeScan *> new_nodes, 
                                                           vector<slam_language_label_t *> new_language_labels, int probMode,
                                                           int find_ground_truth, double *maximum_probability, int ignoreLanguageForEdges);

    void printParticleInformation();
    
    vector<nodePairDist> proposeEdgesToOtherSegments(RegionSegment *segment, int no_edges_to_sample, bool use_closest_dist, bool ignore_connected);

   
    vector<nodePairDist> proposeOldRegionListFromRegionSemantics(RegionNode *region, int no_edges_to_sample);
    
    //void addNodesAndEdges(vector<NodeScan *> new_nodes, SlamGraph *basic_graph, vector<slam_language_edge_t *> new_laguage_edges, int probMode, int find_ground_truth, double *maximum_probability);



    void doScanMatchCheck(int64_t node_id_1, int64_t node_id_2, 
                          int *accepted, int64_t *used_ind_1, int64_t *used_ind_2);
    
    BotTrans getTransform(SlamNode *basic_nd, SlamNode *graphNode);
    BotTrans getTransformPrior(SlamNode *basic_nd, SlamNode *graph_nd_actual, SlamNode *graph_nd_proposed);
    void updateWeight(int probMode, vector<SlamNode *> nodes_to_process);
    
    double compareScans(const bot_core_planar_lidar_t *sim_laser, bot_core_planar_lidar_t *actual_laser);

    double pHit (double z_star, double z);
    double pShort (double z_star, double z);
    double scanLikelihood(const bot_core_planar_lidar_t *sim_laser, bot_core_planar_lidar_t *actual_laser);
    
    void processMeasurementLikelihood(int probMode);

    void addLoopClosures(SlamGraph *basic_graph, vector<SlamNode *> nodes_to_process, int find_ground_truth);
    
    double getProbOfMeasurementLikelihoodSMRegion(RegionNode *node, RegionNode *skip_node, double *max_probability);
    double getProbOfMeasurement(SlamNode *last_nd);
    double getProbOfMeasurementSM(SlamNode *last_nd);
    double getProbOfMeasurementLikelihood(SlamNode *last_nd, double *max_prob);
    double getProbOfMeasurementLikelihoodFixed(SlamNode *last_nd, double *maximum_probability);
    double getProbOfMeasurementLikelihoodScanMatchNode(SlamNode *node, double *max_probability);
    //converts the SlamGraph to a slam_graph_particle_t msg
    void getLCMMessageFromGraph(slam_graph_particle_t *particle);
    void getLCMRegionMessageFromGraph(slam_graph_region_particle_t *particle);
    void getLCMMessageFromBasicGraph(slam_graph_particle_t *particle);
    void publishOccupancyMap ();
    MatrixXd getObservationCovarianceInParticleFrame(double pBody[3], SlamNode *nd);
    double getObservationProbability(MatrixXd cov_match_in_particle_frame, MatrixXd cov_querry_in_particle_frame, 
                                     double pMatch[2], double pQuery[2]);

    double getObservationProbabilityOld(MatrixXd cov_match_in_particle_frame, MatrixXd cov_querry_in_particle_frame, 
                                        double pMatch[2], double pQuery[2]);

    double getObservationProbabilityFixed(double pMatch[2], double pQuery[2]);

    void calculateObservationProbability(int probMode,  double *maximum_probability);
    
    void calculateObservationProbabilityOfRegion(RegionNode *region, double *maximum_probability);

    void calculateObservationProbabilityOfNode(SlamNode *node, double *maximum_probability);

    double getProbOfMeasurementLikelihoodNode(SlamNode *node, double *max_probability);

    double getProbOfMeasurementLikelihoodCount(SlamNode *last_nd, double *maximum_probability);

    double getProbOfMeasurementLikelihoodRegion(SlamNode *nd, double *max_probability);
    double getDistanceBetweenNodes(SlamNode *nd1, SlamNode *nd2);
    double getProbOfMeasurementLikelihoodComplex(SlamNode *last_nd, double *maximum_probability);

    vector <slam_language_edge_t *> proposeLanguageEdges (map<nodePairKey, double> edge_similarities, int no_edges);

    MatrixXd getLaserCov(double pBody[3]);

    void clearActiveNodeParams();
    SlamNode *addNodeToSlamGraph(NodeScan *pose);

    SlamNode *addNodeToRegion(NodeScan *pose, PoseToPoseTransform * p2p_constraint, SlamNode *relative_node, 
                              RegionNode *region);

    SlamNode *addNodeToNewRegion(NodeScan *pose, PoseToPoseTransform * p2p_constraint, SlamNode *relative_node);
    
    double getObservationProbabilityComplex(MatrixXd qPoseMPoseCov, MatrixXd pQuerryCov, MatrixXd pMatchCov, 
                                            double qLaserPos[2], double mLaserPos[2], 
                                            Pose2d qPose, Pose2d mPose);
    
    pointlist2d_t *getPointsForRegion(RegionNode *region);
    
    pointlist2d_t *getPointsForRegion(SlamNode *node, double radius, bool in_node_frame);

    pointlist2d_t * getPointsForSegment(SlamNode *node, double radius, bool in_node_frame);

    double calculateSimilarity (LabelDistribution *ldist1, LabelDistribution *ldist2);

    vector<RegionNode *> bleedLabels(vector<pair<RegionNode*, RegionNode *> > connected_regions);
    vector<RegionNode *> bleedLabelsWithSkip(RegionNode *skip_region);
    vector<RegionNode *> bleedLabels();
    vector<RegionNode *> bleedLabels(RegionNode *region);

    int bleedTemporal (SlamNode *node);
    int bleedRegions();
    pair<int,int> getNoOfLoopClosures();
    void addLoopClosuresSet(set<pair<int, int> > *dist_lc, set<pair<int, int> > *lang_lc);
    void printEdges();

    RegionNode* segmentRegionGeneral(RegionNode *current_region);
    RegionNode* mergeRegions(RegionNode *current_region, RegionNode *new_region, int *merged);
    vector<RegionNode *> sampleEdgesFromRegion(RegionNode *current_region, RegionNode *new_region, vector<RegionNode *> regions_to_check_for_semantic, bool ignoreLanguageForEdges);
    void clearDummyConstraints();

    pointlist2d_t *getRegionPointsFromTransformSuperNode(SlamGraph *basic_graph, int start_ind, int end_ind, int region_supernode_id, int use_only_region);

    Scan *getRegionFromTransform(SlamGraph *basic_graph, int start_ind, int end_ind, int region_supernode_id, BotTrans base_frame_to_particle_frame, int use_only_region);

    pointlist2d_t * getRegionPointsFromTransform(SlamGraph *basic_graph, int start_ind, int end_ind, int region_supernode_id, BotTrans base_frame_to_particle_frame, int use_only_region);

    void doScanMatchWithinRegion(RegionNode *region, SlamNode *node);

    int addConstraintToGraph(SlamNode* node1, SlamNode *node2, PoseToPoseTransform *constraint);
    int addConstraintToGraph(PoseToPoseTransform *constraint);
    int addIncrementalConstraintToGraph(SlamNode* node1);

    int addRegionToScanMatcher(scanmatch::ScanMatcher*, RegionNode*);
    int addNodesToScanMatcher(ScanMatcher *_sm, vector<SlamNode *> nodes);

    slam_language_question_t* getQuestion();
    int updateAnswer(int id, string answer);
    PointCorrespondence *getCachedCorrespondence(int id_1, int id_2);
    void insertCachedCorrespondence(int id_1, int id_2, PointCorrespondence *corr);

    //bool compareDistance (nodeDist node1, nodeDist node2);
    int addRegionToScanMatcher(ScanMatcher *_sm, Pose2d target_pose, SlamNode *node, RegionNode *region, SlamNode *node_to_skip, int max_size);
    
    int addRegionToScanMatcher(ScanMatcher *_sm, Pose2d current_pose, SlamNode *node, RegionNode *region, vector<SlamNode *>nodes_to_skip, int max_size);

    ScanTransform doScanMatchWithRegionQuick(NodeScan *pose_to_match, 
                                             SlamNode *target_node, 
                                             Pose2d *transf, 
                                             Noise *cov_hc, int *accepted, 
                                             double *hit_percentage, 
                                             SlamNode *node_to_skip);

    vector< vector<SlamNode *> > segmentRegion(RegionNode *region, double *n_cut);

    vector< vector<SlamNode *> > segmentNodes(vector<SlamNode *> nodes, double *n_cut);
    
    vector< vector<SlamNode *> > checkToMergeRegions(RegionNode *region1, RegionNode *region2, double *n_cut);

    //BotTrans getTransformFromSlamNodes(SlamNode *target, SlamNode *match);

    SlamNode *proposeOldRegion(Pose2d current_pose, RegionNode *last_region, double *min_dist);
    SlamNode *proposeOldRegionFromNode(SlamNode *c_node, double *minimum_dist);
    RegionNode *proposeOldRegionFromRegion(RegionNode *region, double *minimum_dist, int *c_node_querry, int *c_node_propose);
    vector<nodePairDist> proposeOldRegionListFromCurrentNode(SlamNode *node, int no_edges_to_sample, bool use_closest_dist = false, bool ignore_connected=false);
    
    vector<nodePairDist> proposeOldRegionListFromRegion(RegionNode *region, int no_edges_to_sample, bool use_semantics = true);

    void updateRegionRayTrace(RegionNode *region);
    pointlist2d_t *getRegionRayTrace(RegionNode* region);

    pointlist2d_t *getRegionBoundingbox(RegionNode* region);

    bool hasOutstandingLanguage();
    
    bool hasLanguagePaths();


    void fillLanguageCollection(slam_complex_language_collection_t &msg);
    void fillOutstandingLanguageCollection(slam_complex_language_collection_t &msg);

    LabelInfo *label_info; //passed through the main  
    vector<SlamNode *> unsegmented_nodes;
    lcm_t *lcm;
    int64_t utime; //time the graph was created 
    int graph_id; //graph id
    SlamGraph *graph; //slam graph 
    //SlamGraph *basic_graph; //slam basic graph 
    SlamGraph *sparse_graph; //slam basic graph 
    ScanMatcher *sm; //scanmatch object for loopclosing
    ScanMatcher *sm_low_res;
    
    bot_lcmgl_t *lcmgl_sm_basic;
    bot_lcmgl_t *lcmgl_sm_graph;
    bot_lcmgl_t *lcmgl_sm_prior;
    bot_lcmgl_t *lcmgl_sm_result;
    bot_lcmgl_t *lcmgl_sm_result_low;
    bot_lcmgl_t *lcmgl_loop_closures;
    BotTrans body_to_laser;
    BotTrans laser_to_body;
    FloatPixelMap *local_px_map;
    
    gsl_rng *rng;

    int64_t last_node_utime;
    
    //these can be particle specific
    NodeScan *activeNode;
    Pose2d *tfFromActiveNode; 
    Noise *noiseFromActiveNode; 
    
    map <nodePairKey, PoseToPoseTransform *> languageSMCache;
    map<nodePairKey, PointCorrespondence*> correspondenceCache;
    
    vector<SlamNode *> unprocessed_slam_nodes; 
    
    RegionNode *active_region;

    ConfigurationParams param; 

    double normalized_weight; 
    double weight; 
    double distFromLastRegionNode; 
    int language_event_count;

    vector<ParticleOperationResult> operation_results;

};

#endif
