#include "SlamGraph.hpp"
#include "Utils.hpp"
using namespace Utils;

ObjectDetection::ObjectDetection(int _id, int _type){
    id = _id;
    type = _type;
    closest_node_pair = NULL;
    //set to very high 
    pose_to_close_node = Pose2d(1000,1000,0);
}

int ObjectDetection::updateDetection(perception_object_detection_t *detection, NodeScan *nd){
    map<NodeScan *, objectNodePair *>::iterator it;
    
    it = observation_map.find(nd);
    
    int update_pose = 0;
    objectNodePair *obj_pair = NULL;
    if(it == observation_map.end()){
        //create a new pair and add it to the map 
        obj_pair = new objectNodePair();
        obj_pair->nd = nd;
        obj_pair->object_utime = detection->utime_last_seen;
        update_pose = 1;
        fprintf(stderr, "Adding new object-node pair => Object %d (Type : %d) - Node %d\n", id, type, nd->node_id);
        observation_map.insert(make_pair(nd, obj_pair));
    }
    else{
        //update the pair 
        obj_pair = it->second; 
        
        //this obeservation is closer in time than the current one
        if(fabs(nd->utime - obj_pair->object_utime) > fabs(nd->utime -detection->utime_last_seen)){
            fprintf(stderr, "Updating object-node pair %f -> %f => Object %d (Type : %d) - Node %d\n", 
                    fabs(nd->utime - obj_pair->object_utime)/1.0e6, fabs(nd->utime -detection->utime_last_seen)/1.0e6, 
                    id, type, nd->node_id);
                    
            obj_pair->object_utime = detection->utime_last_seen;
            update_pose = 1;
        }
    }
    if(update_pose){
        BotTrans object_to_local = getBotTransQuat(detection->pos, detection->quat); 
            
        BotTrans local_to_node = getBotTransFromPose(nd->odom_pose);
        
        bot_trans_invert(&local_to_node);
        
        BotTrans object_to_node; 
        bot_trans_apply_trans_to(&local_to_node, &object_to_local, &object_to_node);
        
        obj_pair->pose_to_node = getPoseFromBotTrans(object_to_node);
        fprintf(stderr, "Updated Pose : %f,%f,%f\n", obj_pair->pose_to_node.x(), obj_pair->pose_to_node.y(), obj_pair->pose_to_node.t());

        double c_dist = hypot(pose_to_close_node.x(), pose_to_close_node.y());

        double n_dist = hypot(obj_pair->pose_to_node.x(), obj_pair->pose_to_node.y());

        if(c_dist > n_dist){
            pose_to_close_node = obj_pair->pose_to_node;
            //closest_node = nd;
            closest_node_pair = obj_pair;
        }
        //also check each node and find the closest node - and set that as the valid node 
    }    
}
