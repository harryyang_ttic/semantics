#include "SlamGraph.hpp"
#include "Utils.hpp"
using namespace Utils;

ComplexLanguageEvent::ComplexLanguageEvent(complex_language_event_t *event, LabelInfo *_info, bool _use_factor_graph):
    outstanding_language(0),event_id(event->language_event_id), 
    node_id( event->node_id), info(_info), use_factor_graph(_use_factor_graph){

    landmark = string(event->parse_result->landmark);
    figure = string(event->parse_result->figure);

    figure_id = info->getIndexForLabel(figure);
    utterance = string(event->parse_result->utterance);
    sr = string(event->parse_result->sr);
    complete =false;
    failed_grounding = false;
    number_of_regions_added_since_failure = 0;
    handled_internally = false;
    last_checked_id = -1;
    last_added_id = -1;
    id_at_failed_time = -1;
}

ComplexLanguageEvent::ComplexLanguageEvent(complex_language_event_t *event, SlamNode *node, LabelInfo *_info, bool _use_factor_graph):
    outstanding_language(0), event_id(event->language_event_id), info(_info), use_factor_graph(_use_factor_graph)
{
    landmark = string(event->parse_result->landmark);
    figure = string(event->parse_result->figure);
    utterance = string(event->parse_result->utterance);
    sr = string(event->parse_result->sr);
    figure_id = info->getIndexForLabel(figure);
    setNode(node);
    complete = false;
    fprintf(stderr, "Complex Event : %d\n", event_id);
    failed_grounding = false;
    number_of_regions_added_since_failure = 0;
    handled_internally = false;
}

void ComplexLanguageEvent::setNode(SlamNode *node){
    utterence_node = node;
    node_id = node->id;    
}

void ComplexLanguageEvent::updateLastCheckedGrounding(int64_t id){
    last_checked_id = id;
}

void ComplexLanguageEvent::updateFailedGrounding(){
    id_at_failed_time = last_checked_id;
}

int ComplexLanguageEvent::getIDsAddedSinceFailure(){
    return fmax(0, last_checked_id - id_at_failed_time); 
}

int ComplexLanguageEvent::getOutstandingCount(){
    //return outstanding_language;
    int outstanding_count = 0;
    map<RegionNode *, RegionPath*>::iterator it; 

    for(it = node_paths.begin(); it != node_paths.end(); it++){
        fprintf(stderr, "Landmarks for region path : %d\n", (int) it->second->landmark_prob.size());
        if(!it->second->isUpdated())
            outstanding_count++;
    }
    
    fprintf(stderr, "Region Path count : %d / %d\n", outstanding_count, (int) node_paths.size());
    return outstanding_count; 
}

int ComplexLanguageEvent::getNoPaths(){
    return node_paths.size();
}

vector<regionPathProb> ComplexLanguageEvent::getNormalizePaths(int max_size){
    vector<regionPathProb> result;
    //we can normalize this if we need to 
    map<RegionNode *, RegionPath *>::iterator it;
 
    
    double min_prob = 0.1; //maybe this falls too low and then gets rejected
    for(it = node_paths.begin(); it != node_paths.end(); it++){
        double prob = it->second->getProbability();
        if(prob > min_prob){
            result.push_back(make_pair(it->second, prob));
        }        
        else{
            fprintf(stderr, "Skipping Figure region : %d - %f < %f\n", it->first->region_id, prob, min_prob);
        }
    }    
    
    if(result.size() == 0)
        return result;

    sort(result.begin(), result.end(), compareRegionPathProbs);

    //get top N 
    if(max_size >=0){
        int size = fmin(max_size, result.size());        
        result.resize(size);
    }

    double sum = 0;
    vector<regionPathProb>::iterator it_v;
    for(it_v = result.begin(); it_v != result.end(); it_v++){
        sum += it_v->second;
    }

    assert(sum >0);

    fprintf(stderr, "Normalized Figure Paths\n");
    for(it_v = result.begin(); it_v != result.end(); it_v++){
        it_v->second = it_v->second / sum;
        fprintf(stderr, "Figure : %d => Prob : %f\n", it_v->first->region->region_id, it_v->second);
    }

    return result;
}

//double ComplexLanguageEvent::

vector<regionProb> ComplexLanguageEvent::getLandmarkDistribution(){
    vector<regionProb> landmark_prob; 

    fprintf(stderr, "Figure Marginalization : %d\n",  (int) valid_landmarks.size());
    
    //margialize the path likelihoods for each landmark 
    map<int, regionLandmark>::iterator it_l; 

    double sum = 0;
    for(it_l = valid_landmarks.begin(); it_l != valid_landmarks.end(); it_l++){   
        //for each region path - get the path likelihood 
        RegionNode *lm_nd = it_l->second.first;
        map<RegionNode *, RegionPath *>::iterator it;
        double p_prob = 0;
        for(it = node_paths.begin(); it != node_paths.end(); it++){
            p_prob += it->second->getPathProbability(lm_nd);
        }
        landmark_prob.push_back(make_pair(lm_nd, p_prob));
        sum += p_prob;
    }

    sort(landmark_prob.begin(), landmark_prob.end(), compareRegionProb);


    for(int i=0; i < landmark_prob.size(); i++){    
        //should we normalize 
        if(sum > 0)
            landmark_prob[i].second /= sum;
        fprintf(stderr, "Landmark : %d - Probability : %f\n", landmark_prob[i].first->region_id, landmark_prob[i].second);
    }

    return landmark_prob;
}
  
void ComplexLanguageEvent::addNodePath(SlamNode *node, NodePath path){
    //these are the figure node and the path to the figure from the location of the utterence
    //we should do this for region and not node (since mean nodes can change - and then paths won't get flushed 
    map<RegionNode *, RegionPath *>::iterator it; 

    it = node_paths.find(node->region_node);
    
    if(it != node_paths.end()){
        //remove 
        bool new_path = it->second->updatePath(path);
        fprintf(stderr, "Already have path - checking\n");
    }
    else{
        //the region path should contain how likeli the figure node is to be the given label 
        //if we wish to normalize the likelihoods over the figures, then it needs to happen at the 
        //complex language level 
        RegionPath *r_path = new RegionPath(node->region_node, path, info, figure_id, use_factor_graph);
        
        number_of_regions_added_since_failure++;
        
        //add the landmarks for this path 
        map<int, regionLandmark>::iterator it_l; 
        for(it_l = valid_landmarks.begin(); it_l != valid_landmarks.end(); it_l++){   
            r_path->addLandmark(it_l->second);
        }
        node_paths.insert(make_pair(node->region_node, r_path));
        fprintf(stderr, "\tAdding Path\n");
        //exit(-1);

        last_added_id = last_checked_id;
        
        /*spatialRelation srType = getSpatialRelationType(sr);

        
        if(isSRHandled(srType)){
            double prob = 0;
            
            fprintf(stderr, "SR Type : %s handled internally - Grounding\n", sr.c_str());
            
            Pose2d robot_pose = utterence_node->getPose();
            
            for(it_l = valid_landmarks.begin(); it_l != valid_landmarks.end(); it_l++){ 
                //would this have valid landmarks??
                RegionNode *lm = it_l->second.first;
                prob = getSRLikelihoodGeneral(srType, robot_pose, node->getPose(), lm->mean_node->getPose(), true);

                fprintf(stderr, "\tProb : %f\n", prob);
                r_path->updateProbability(lm, prob);
            }
            //for(all landmarks){
            //r_path->updateProbability(landmark_region, prob);
            //}
            }*/ 
        
        //updateProbability(landmark_region, p_result->landmark_results[j].path_probability);

        //should we try to ground the hand coded spatial relations?? 
    }    
}

/*bool ComplexLanguageEvent::evaluateRegionPaths(groundingMode mode){
    spatialRelation srType = getSpatialRelationType(sr);

    fprintf(stderr, "Evaluating the Spatial relation likelihood\n");

    fprintf(stderr, "\tSpatial Relation : %s -> %s\n", sr.c_str(), getSR(srType).c_str());
    
    if(isSRHandled(srType)){
        fprintf(stderr, "\tInternally Handled Spatial Relation\n");
        
        map<RegionNode *, RegionPath *>::iterator it;

        RegionNode 

        if(use_factor_graph)
            landmark_id = info->getIndexForLabel(landmark);
        else
            landmark_id = info->getIndexForLabel(landmark);

        fprintf(stderr, "Landmark : %s -> landmark id : %d\n", landmark->c_str(), landmark_id);

        bool use_robot = true;
        
        if(landmark_id >= 0){
            use_robot = false;
        }

        Pose2d robot_pose = utterence_node->getPose();        

        if(mode == ){
            double max_prob = 0; 
                        
            if(!use_robot){
                map<RegionPath *, vector<RegionNode *, double> > grounding_result; 

                for(it = node_paths.begin(); it != node_paths.end(); it++){
                    RegionPath *r_path = it->second;
            
                    vector<RegionNode *, double> region_path_groundings;

                    double prob = 0;

                    Pose2d fig_pose = it->first->mean_node->getPose();

                    //this has a valid landmark attached - don't complete this unless we have landmarks for this path 
                    if(valid_landmarks.size() == 0){
                        fprintf(stderr, "Valid landmark - but no landmarks found yet - waiting for landmarks\n");
                    }
                    else{
                        fprintf(stderr, "Have valid landmarks - grounding now\n");

                        map<int, regionLandmark>::iterator it_l; 
                        for(it_l = valid_landmarks.begin(); it_l != valid_landmarks.end(); it_l++){ 
                            //would this have valid landmarks??
                            RegionNode *lm = it_l->second.first;
                            prob = getSRLikelihoodGeneral(srType, robot_pose, fig_pose, lm->mean_node->getPose(), use_robot);
                        
                            region_path_groundings.push_back(make_pair(lm, prob));
                            if(max_prob < prob){
                                max_prob = prob;
                            }
                            fprintf(stderr, "\tFigure region : %d => Prob of grounding : %f\n", it->first->region_id, prob);              
                        }   
                    }               
                }

                if(max_prob > 0.2){
                    fprintf(stderr, "Grounding high enough - Grounding now\n");
                    
                    map<RegionPath *, vector<RegionNode *, double> >::iterator it_fig;

                    for(it_fig = grounding_result.begin(); it_fig != grounding_result.end(); it_fig++){
                        node_paths.find(it_fig)
                    }
                        
                    complete = true;  
                }
            }
            else{
                vector<RegionNode *, double> grounding_result; 
                for(it = node_paths.begin(); it != node_paths.end(); it++){
                    RegionPath *r_path = it->second;
            
                    double prob = 0;

                    Pose2d fig_pose = it->first->mean_node->getPose();

                    Pose2d lm_pose(0,0,0);
                    //some of these will not have landmarks 
                    double prob = getSRLikelihoodGeneral(srType, robot_pose, fig_pose, lm_pose, use_robot);

                    grounding_result.push_back(make_pair(lm, prob));
                    
                    if(max_prob < prob){
                        max_prob = prob;
                    }
                    
                    fprintf(stderr, "\tFigure region : %d => Prob of grounding : %f\n", it->first->region_id, prob);              
                }
                if(max_prob > 0.2){
                    fprintf(stderr, "Grounding high enough - Grounding now\n");
                    
                    
                    
                    complete = true;  
                }
            }
            
        }
        else{
            for(it = node_paths.begin(); it != node_paths.end(); it++){
                RegionPath *r_path = it->second;
            
                vector<RegionNode *, double> region_path_groundings;

                double prob = 0;

                Pose2d fig_pose = it->first->mean_node->getPose();

                //this has a valid landmark attached - don't complete this unless we have landmarks for this path 
                if(!use_robot){
                    if(valid_landmarks.size() == 0){
                        fprintf(stderr, "Valid landmark - but no landmarks found yet - waiting for landmarks\n");
                    }
                    else{
                        fprintf(stderr, "Have valid landmarks - grounding now\n");

                        map<int, regionLandmark>::iterator it_l; 
                        for(it_l = valid_landmarks.begin(); it_l != valid_landmarks.end(); it_l++){ 
                            //would this have valid landmarks??
                            RegionNode *lm = it_l->second.first;
                            prob = getSRLikelihoodGeneral(srType, robot_pose, fig_pose, lm->mean_node->getPose(), use_robot);
                        
                            fprintf(stderr, "\tProb : %f\n", prob);
                            r_path->updateProbability(lm, prob);
                            
                            fprintf(stderr, "\tFigure region : %d => Prob of grounding : %f\n", it->first->region_id, prob);              
                        }   
                    }
                }
                else{                
                    Pose2d lm_pose(0,0,0);
                    //some of these will not have landmarks 
                    double prob = getSRLikelihoodGeneral(srType, robot_pose, fig_pose, lm_pose, use_robot);
                    
                    r_path->updateProbability(NULL, prob);
                    fprintf(stderr, "\tFigure region : %d => Prob of grounding : %f\n", it->first->region_id, prob);              
                }
            }
        }
    }
    else{
        fprintf(stderr, "Spatial relation not handled\n");
    }
    }*/

bool ComplexLanguageEvent::evaluateRegionPaths(bool ground_only_if_high, vector<RegionNode *> &grounded_regions, double ground_threshold, int max_count){
    spatialRelation srType = getSpatialRelationType(sr);

    fprintf(stderr, "Evaluating the Spatial relation likelihood\n");

    fprintf(stderr, "\tSpatial Relation : %s -> %s\n", sr.c_str(), getSR(srType).c_str());
    
    //double ground_threshold = 0.2;
    
    if(isSRHandled(srType)){
        fprintf(stderr, "\tInternally Handled Spatial Relation\n");
        handled_internally = true;

        if(node_paths.size() == 0){
            failed_grounding = true;
            updateFailedGrounding();
            number_of_regions_added_since_failure = 0;            
            return false; 
        }
        
        map<RegionNode *, RegionPath *>::iterator it;

        it = node_paths.begin();
        assert(it != node_paths.end());

        RegionNode *fig = it->first;

        int landmark_id = -1; 
        int figure_id = -1;

        if(use_factor_graph){
            landmark_id = info->getIndexForLabel(landmark);
            figure_id = info->getIndexForLabel(figure);
        }
        else{
            landmark_id = fig->labeldist->getLabelID(landmark);
            figure_id = fig->labeldist->getLabelID(figure);
        }

        fprintf(stderr, "Landmark : %s -> landmark id : %d\n", landmark.c_str(), landmark_id);

        bool use_robot = true;
        
        if(landmark_id >= 0){
            use_robot = false;
        }

        Pose2d robot_pose = utterence_node->getPose();        

        if(ground_only_if_high){
            double max_prob = 0; 
                        
            if(!use_robot){
                map<RegionPath *, map<RegionNode *, double> > grounding_result; 
                
                fprintf(stderr, "====== Checking SR from landmark\n");

                for(it = node_paths.begin(); it != node_paths.end(); it++){
                    RegionPath *r_path = it->second;
            
                    map<RegionNode *, double> region_path_groundings;

                    double prob = 0;

                    Pose2d fig_pose = it->first->mean_node->getPose();

                    //this has a valid landmark attached - don't complete this unless we have landmarks for this path 
                    if(valid_landmarks.size() == 0){
                        fprintf(stderr, "Valid landmark - but no landmarks found yet - waiting for landmarks\n");
                    }
                    else{
                        fprintf(stderr, "Have valid landmarks - grounding now\n");

                        map<int, regionLandmark>::iterator it_l; 
                        for(it_l = valid_landmarks.begin(); it_l != valid_landmarks.end(); it_l++){ 
                            //would this have valid landmarks??
                            RegionNode *lm = it_l->second.first;
                            prob = getSRLikelihoodGeneral(srType, robot_pose, fig_pose, lm->mean_node->getPose(), use_robot);
                        
                            region_path_groundings.insert(make_pair(lm, prob));
                            if(max_prob < prob){
                                max_prob = prob;
                            }
                            fprintf(stderr, "\tFigure region : %d => Prob of grounding : %f\n", it->first->region_id, prob);              
                        }
                        grounding_result.insert(make_pair(r_path, region_path_groundings)); 
                    }               
                }

                if(max_prob > 0.2){
                    fprintf(stderr, "Grounding high enough - Grounding now : %d\n", (int) grounding_result.size());
                    
                    map<RegionPath *, map<RegionNode *, double> >::iterator it_fig;

                    //sort this and pick the top N 
                    
                    vector<regionProb> figure_grounding;

                    for(it_fig = grounding_result.begin(); it_fig != grounding_result.end(); it_fig++){
                        //it = node_paths.find(it_fig->first);
                        fprintf(stderr, "Grounding Region Path : %d\n", it_fig->first->region->region_id);
                        //if(it == node_paths.end())
                        //   continue;

                        RegionPath *r_path = it_fig->first;                        
                        RegionNode *figure_region = r_path->region;                        
                        
                        if(use_factor_graph){
                            fprintf(stderr, "Using Factor graph\n");

                            map<RegionNode *, double> landmark_grounding = it_fig->second;
                            map<RegionNode *, double>::iterator lm_iter;
                            for(lm_iter = landmark_grounding.begin(); lm_iter != landmark_grounding.end(); lm_iter++){
                                double prob = lm_iter->second;
                                RegionNode *lm = lm_iter->first;
                                r_path->updateProbability(lm, prob);                                
                            }
                        }
                        else{
                            map<int, double> landmark_likelihoods = getLandmarkLikelihoodsForFigure(figure_region);

                            double ground_prob = 0;
                            map<RegionNode *, double> landmark_grounding = it_fig->second;
                            map<RegionNode *, double>::iterator lm_iter;
                            map<int, double>::iterator lm_like_it;

                            fprintf(stderr, "Landmarks : %d\n", (int) landmark_grounding.size());

                            for(lm_iter = landmark_grounding.begin(); lm_iter != landmark_grounding.end(); lm_iter++){
                                double prob = lm_iter->second;
                                RegionNode *lm = lm_iter->first;

                                lm_like_it = landmark_likelihoods.find(lm->region_id);
                                if(lm_like_it == landmark_likelihoods.end())
                                    continue;

                                ground_prob += prob * lm_like_it->second;

                                fprintf(stderr, " -> LM : %d -> %f * %f\n", lm->region_id, prob, lm_like_it->second);
                            }

                            figure_grounding.push_back(make_pair(figure_region, ground_prob)); 
                        }
                    }

                    sort(figure_grounding.begin(), figure_grounding.end(), compareRegionProb);

                    for(int i=0; i < figure_grounding.size(); i++){
                        RegionNode *figure_region = figure_grounding[i].first;
                        double ground_prob = figure_grounding[i].second;
                         
                        if(i > max_count){
                            break;
                        }

                        //we should do the top N here also 
                        if(ground_prob > ground_threshold){
                            figure_region->labeldist->addObservation(figure_id, ground_prob, 1000 + event_id);
                            grounded_regions.push_back(figure_region);
                        }
                        else{
                            break;
                        }

                        fprintf(stderr, "Adding likelihood to region : %d -> %f\n", figure_region->region_id, ground_prob);
                    }
                        
                    complete = true;  
                }
                else{
                    failed_grounding = true;
                    updateFailedGrounding();
                    number_of_regions_added_since_failure = 0;
                }
            }
            else{
                map<RegionNode *, double> grounding_result; 
                vector<regionProb> figure_grounding;

                for(it = node_paths.begin(); it != node_paths.end(); it++){
                    RegionPath *r_path = it->second;
            
                    Pose2d fig_pose = it->first->mean_node->getPose();

                    Pose2d lm_pose(0,0,0);
                    //some of these will not have landmarks 
                    double prob = getSRLikelihoodGeneral(srType, robot_pose, fig_pose, lm_pose, use_robot);

                    grounding_result.insert(make_pair(r_path->region, prob));
                    
                    if(max_prob < prob){
                        max_prob = prob;
                    }
                    
                    fprintf(stderr, "\tFigure region : %d => Prob of grounding : %f\n", it->first->region_id, prob);              
                }
                if(max_prob > 0.2){
                    fprintf(stderr, "Grounding high enough - Grounding now\n");
                    
                    map<RegionNode *, double>::iterator it_fig;

                    for(it_fig = grounding_result.begin(); it_fig != grounding_result.end(); it_fig++){
                        it = node_paths.find(it_fig->first);

                        if(it == node_paths.end())
                            continue;

                        RegionNode *figure_region = it->first;
                        RegionPath *r_path = it->second;                        
                        
                        double prob = it_fig->second;

                        if(use_factor_graph){                            
                            r_path->updateProbability(NULL, prob);
                        }
                        else{
                            if(prob > ground_threshold){
                                figure_grounding.push_back(make_pair(figure_region, prob)); 
                                //figure_region->labeldist->addObservation(figure_id, prob, 1000 + event_id);
                                //grounded_regions.push_back(figure_region);
                            }
                        }                            
                    }                    
                    complete = true;  
                    sort(figure_grounding.begin(), figure_grounding.end(), compareRegionProb);
                    for(int i=0; i < figure_grounding.size(); i++){
                        RegionNode *figure_region = figure_grounding[i].first;
                        double ground_prob = figure_grounding[i].second;
                         
                        if(i > max_count){
                            break;
                        }

                        //we should do the top N here also 
                        if(ground_prob > ground_threshold){
                            figure_region->labeldist->addObservation(figure_id, ground_prob, 1000 + event_id);
                            grounded_regions.push_back(figure_region);
                        }
                        else{
                            break;
                        }
                        
                        fprintf(stderr, "Adding likelihood to region : %d -> %f\n", figure_region->region_id, ground_prob);
                    }
                     
                }
                else{
                    failed_grounding = true;
                    number_of_regions_added_since_failure = 0;
                }
            }
            
        }
        else{
            for(it = node_paths.begin(); it != node_paths.end(); it++){
                RegionPath *r_path = it->second;
            
                double prob = 0;

                Pose2d fig_pose = it->first->mean_node->getPose();

                //this has a valid landmark attached - don't complete this unless we have landmarks for this path 
                if(!use_robot){
                    if(valid_landmarks.size() == 0){
                        fprintf(stderr, "Valid landmark - but no landmarks found yet - waiting for landmarks\n");
                    }
                    else{
                        fprintf(stderr, "Have valid landmarks - grounding now\n");

                        map<int, regionLandmark>::iterator it_l; 
                        for(it_l = valid_landmarks.begin(); it_l != valid_landmarks.end(); it_l++){ 
                            //would this have valid landmarks??
                            RegionNode *lm = it_l->second.first;
                            prob = getSRLikelihoodGeneral(srType, robot_pose, fig_pose, lm->mean_node->getPose(), use_robot);
                        
                            fprintf(stderr, "\tProb : %f\n", prob);
                            r_path->updateProbability(lm, prob);
                            
                            fprintf(stderr, "\tFigure region : %d => Prob of grounding : %f\n", it->first->region_id, prob);              
                        }   
                    }
                }
                else{                
                    Pose2d lm_pose(0,0,0);
                    //some of these will not have landmarks 
                    double prob = getSRLikelihoodGeneral(srType, robot_pose, fig_pose, lm_pose, use_robot);
                    
                    r_path->updateProbability(NULL, prob);
                    fprintf(stderr, "\tFigure region : %d => Prob of grounding : %f\n", it->first->region_id, prob);              
                }
            }
        }
    }
    else{
        fprintf(stderr, "Spatial relation not handled\n");
        if(node_paths.size() == 0){
            return false; 
        }
    }
}

vector<RegionPath *> ComplexLanguageEvent::getRegionPaths(){
    vector<RegionPath *> region_paths; 
    map<RegionNode *, RegionPath *>::iterator it;
    for(it = node_paths.begin(); it != node_paths.end(); it++){
        region_paths.push_back(it->second);
    }
    return region_paths;
}

//the node path, the slam node and the grounding prob for each valid landmark 
void ComplexLanguageEvent::addNewRegionPath(RegionNode *region_node, NodePath path, map<int, double> grounding_prob, double prob){
    RegionPath *r_path = new RegionPath(region_node, path, info, figure_id, use_factor_graph);
    map<int, regionLandmark>::iterator it_l; 
    for(it_l = valid_landmarks.begin(); it_l != valid_landmarks.end(); it_l++){   
        r_path->addLandmark(it_l->second);
    }
    //for(int i=0; i < grounding_prob.size(); i++){
    map<int, double>::iterator it;
    for(it = grounding_prob.begin(); it != grounding_prob.end(); it++){
        it_l = valid_landmarks.find(it->first);
        if(it_l != valid_landmarks.end()){
            r_path->updateProbability(it_l->second.first, it->second);
        }
    }
    //this should copy over the existing prob
    if(valid_landmarks.size() == 0){
        r_path->updateProbability(NULL, prob);
    }
    node_paths.insert(make_pair(region_node, r_path));
}

void ComplexLanguageEvent::removeNodePath(RegionNode *region){    
    map<RegionNode *, RegionPath *>::iterator it; 
    it = node_paths.find(region);    
    if(it != node_paths.end()){
        //remove         
        node_paths.erase(it);
        delete it->second;
    }
}

void ComplexLanguageEvent::printPath(SlamNode *node){
    map<RegionNode *, RegionPath *>::iterator it; 

    it = node_paths.find(node->region_node);
    
    if(it == node_paths.end()){
        fprintf(stderr, GREEN "Language Event : %d Node %d => No Path found to Node : %d\n" RESET_COLOR, 
                (int) event_id, (int) node_id, (int) node->id);
    }
    else{
        fprintf(stderr, GREEN "Language Event : %d Node %d => Path to Node : %d\n\t" RESET_COLOR, 
                (int) event_id, (int) node_id,(int) node->id);
        RegionPath *path = it->second;
        path->print();
    }
}

void ComplexLanguageEvent::clearNodePaths(){
    //this needs to delete all the contained paths 
    map<RegionNode *, RegionPath *>::iterator it; 

    for(it = node_paths.begin(); it != node_paths.end(); it++){
        delete it->second;
    }
    node_paths.clear();
}

void ComplexLanguageEvent::fillNodePaths(slam_complex_language_path_list_t &msg){
    msg.event_id = event_id;
    msg.utterence = strdup(utterance.c_str());
    msg.landmark = strdup(landmark.c_str());
    msg.figure = strdup(figure.c_str());
    msg.sr = strdup(sr.c_str());
    
    msg.valid_landmark_count = valid_landmarks.size();
    msg.valid_landmarks = (int64_t *) calloc(msg.valid_landmark_count, sizeof(int64_t));

    int i = 0;
    map<int, regionLandmark>::iterator it_l; 
    for(it_l = valid_landmarks.begin(); it_l != valid_landmarks.end(); it_l++, i++){        
        msg.valid_landmarks[i] = it_l->second.first->mean_node->id;
    }

    msg.no_paths = (int) node_paths.size();
    msg.path = (slam_node_path_t *) calloc(msg.no_paths, sizeof(slam_node_path_t));
    
    map<RegionNode *, RegionPath *>::iterator it; 
    i = 0;
    for(it = node_paths.begin(); it != node_paths.end(); it++, i++){
        NodePath path = it->second->path; 
        it->second->isUpdated()? msg.path[i].updated = 1: msg.path[i].updated =0;
        msg.path[i].prob = it->second->getProbability();
        msg.path[i].count = (int) path.size();
        msg.path[i].node_id = it->first->mean_node->id;
        msg.path[i].nodes = (int64_t *) calloc(msg.path[i].count, sizeof(int64_t));
        for(int j=0; j < path.size(); j++){
            msg.path[i].nodes[j] = path[j]->id;
        }
        msg.path[i].no_landmarks = 0;
        msg.path[i].landmark_ids = NULL;
    }
}

//send only the outstanding paths 
void ComplexLanguageEvent::fillOutstandingNodePaths(slam_complex_language_path_list_t &msg){
    msg.event_id = event_id;
    msg.utterence = strdup(utterance.c_str());
    msg.landmark = strdup(landmark.c_str());
    msg.figure = strdup(figure.c_str());
    msg.sr = strdup(sr.c_str());
    msg.utterence_node = utterence_node->id;
    msg.utterence_region = utterence_node->region_node->region_id;

    map<RegionNode *, RegionPath *>::iterator it; 
    int count = 0;
    for(it = node_paths.begin(); it != node_paths.end(); it++){
        NodePath path = it->second->path; 
        if(!it->second->isUpdated())
            count++;
    }
    
    if(count == 0){
        msg.no_paths = 0;
        msg.path = NULL;
        return;
    }
    
    msg.valid_landmark_count = valid_landmarks.size();
    msg.valid_landmarks = (int64_t *) calloc(msg.valid_landmark_count, sizeof(int64_t));

    int i = 0;
    map<int, regionLandmark>::iterator it_l; 
    for(it_l = valid_landmarks.begin(); it_l != valid_landmarks.end(); it_l++, i++){        
        msg.valid_landmarks[i] = it_l->second.first->mean_node->id;
    }
    
    msg.no_paths = count;
    msg.path = (slam_node_path_t *) calloc(msg.no_paths, sizeof(slam_node_path_t));
        
    i = 0;
    for(it = node_paths.begin(); it != node_paths.end(); it++){
        NodePath path = it->second->path; 
        if(it->second->isUpdated())
            continue;

        it->second->isUpdated()? msg.path[i].updated = 1: msg.path[i].updated =0;
        msg.path[i].prob = it->second->getProbability();
        msg.path[i].count = (int) path.size();
        msg.path[i].node_id = it->first->mean_node->id;
        msg.path[i].nodes = (int64_t *) calloc(msg.path[i].count, sizeof(int64_t));
        for(int j=0; j < path.size(); j++){
            msg.path[i].nodes[j] = path[j]->id;
        }
        map<RegionNode *, GroundingProb>::iterator it_lm; 

        int lm_count = 0;
        for(it_lm = it->second->landmark_prob.begin(); it_lm != it->second->landmark_prob.end(); it_lm++){
            if(!it_lm->second.evaluated)
                lm_count++;
        }

        msg.path[i].no_landmarks = lm_count;
        msg.path[i].landmark_ids = (int64_t *) calloc(msg.path[i].no_landmarks, sizeof(int64_t));
        int k=0;
        for(it_lm = it->second->landmark_prob.begin(); it_lm != it->second->landmark_prob.end(); it_lm++){
            if(!it_lm->second.evaluated){
                msg.path[i].landmark_ids[k] = it_lm->first->mean_node->id;
                k++;
            }
        }

        i++;
    }
}

vector<regionProb> ComplexLanguageEvent::getGroundedRegions(){
    vector<regionProb> result;
    //we can normalize this if we need to 
    map<RegionNode *, RegionPath *>::iterator it; 
    for(it = node_paths.begin(); it != node_paths.end(); it++){
        double prob = it->second->getProbability();
        if(prob > 0){
            result.push_back(make_pair(it->first, prob));
        }        
    }    
    sort(result.begin(), result.end(), compareRegionProb);
    return result;
}

vector<RegionPath *> ComplexLanguageEvent::getGroundedRegionPaths(){
    vector<RegionPath *> result;
    //we can normalize this if we need to 
    map<RegionNode *, RegionPath *>::iterator it; 
    for(it = node_paths.begin(); it != node_paths.end(); it++){
        double prob = it->second->getProbability();
        if(prob > 0){
            result.push_back(it->second);
        }        
    }    
    sort(result.begin(), result.end(), compareRegionPaths);
    return result;
}

map<int, double> ComplexLanguageEvent::getLandmarkLikelihoodsForFigure(RegionNode *fig_region){
     map<RegionNode *, RegionPath *>::iterator it; 
     it = node_paths.find(fig_region);
     map<int, double> figure_landmark_prob; 
     if(it != node_paths.end()){
         fprintf(stderr, "Region found : %d\n", fig_region->region_id);
         return it->second->getNormalizedLandmarkDistribution();
     }
     return figure_landmark_prob; 
}

int ComplexLanguageEvent::updateResult(vector<pair<RegionNode *, slam_complex_language_path_result_t*> > &path_results){
    //search through each path and update the result 
    //the path should have likelihoods for each landmark region ?? (as this is how its sent back) 
    map<int, regionLandmark>::iterator it_l; 
    
    map<RegionNode *, RegionPath *>::iterator it; 
    for(int i=0; i < path_results.size(); i++){
        //reduce the outstanding language count 
        RegionNode *region = path_results[i].first;
        slam_complex_language_path_result_t *p_result = path_results[i].second;
        it = node_paths.find(region);
        if(it != node_paths.end()){
            //update the path likelihoods 
            for(int j=0; j < p_result->no_landmarks; j++){
                //find the landmark region and then updated the probability
                it_l = valid_landmarks.find(p_result->landmark_results[j].landmark_id);
                if(it_l == valid_landmarks.end()){
                    fprintf(stderr, "Region Not found - this should not have happend\n");
                    exit(-1);
                }
                else{
                    RegionNode *landmark_region = it_l->second.first;
                    //this updates the path probability 
                    it->second->updateProbability(landmark_region, p_result->landmark_results[j].path_probability);
                }
            }
        }
    }
    fprintf(stderr, "Oustanding Results : %d\n", outstanding_language);
    if(outstanding_language < 0)
        exit(-1);
}

void ComplexLanguageEvent::clearLandmarks(){
    valid_landmarks.clear();
}

vector<regionProb> ComplexLanguageEvent::getFigureDistribution(){
    map<RegionNode *, RegionPath *>::iterator it; 

    fprintf(stderr, "Complex Langage Event : %d => Utterence : %s\n", event_id, utterance.c_str());

    vector<regionProb> figure_results; 
    
    double sum = 0;
    for(it = node_paths.begin(); it != node_paths.end(); it++){
        RegionPath *path = it->second;            
        figure_results.push_back(make_pair(path->region, path->getProbability()));
        sum += path->getProbability();
    }
        
    sort(figure_results.begin(), figure_results.end(), compareRegionProb);
        
    fprintf(stderr, "\n\n");
    for(int i=0; i < figure_results.size(); i++){
        if(sum > 0)
            figure_results[i].second /= sum;

        fprintf(stderr, "Figure Region : %d - %f\n", figure_results[i].first->region_id, figure_results[i].second);
    }
    return figure_results;
}

int ComplexLanguageEvent::getNoAskedQuestions(){
    return asked_questions.size();
}

void ComplexLanguageEvent::updateAskedQuestions(spatialRelation sr){
    //update the number of asked questions 
    asked_questions.push_back(sr);
}

spatialResults ComplexLanguageEvent::evalSRQuestionsFromCurrentLocation(SlamNode *nd, double info_gain_threshold, int *landmark, vector<indexProb> &max_info_prob){
    //evaluate the Spatial relations from the current location 
    const spatialRelation lst[] = {LEFT_OF, RIGHT_OF, IN_FRONT_OF, BEHIND, NEAR};
    vector<spatialRelation> sr_list(lst, lst + sizeof(lst)/sizeof(spatialRelation));

    vector<regionProb> figure_results = getFigureDistribution();    

    double figure_entropy = calculateEntropy(figure_results);

    vector<regionProb> landmark_results = getLandmarkDistribution();

    double landmark_entropy = calculateEntropy(landmark_results);
    
    fprintf(stderr, "Figure Entropy : %f\n", figure_entropy);
    fprintf(stderr, "Landmark Entropy : %f\n", landmark_entropy);

    double original_entropy = 0;
    vector<regionProb> check_regions; 
    if(figure_entropy == 0 && landmark_entropy == 0){
        fprintf(stderr, "No entropy over figure or landmarks\n");
        return make_pair(INVALID,0); 
        //return 
    }
    
    
    if(figure_entropy > landmark_entropy){
        check_regions = figure_results;
        *landmark = 0;
        original_entropy = figure_entropy;
    }
    else{
        //note that if we resolve uncertanity about the landmark - this might reduce the uncertainity over 
        //the figures as well - which is not considered right now - in the yes/no entropy calculations 
        check_regions = landmark_results;
        *landmark = 1;
        original_entropy = landmark_entropy;
    }

    if(check_regions.size() == 0){
        fprintf(stderr, "Size of regions to check for questions is zero\n");
        return make_pair(INVALID,0);
    }

    fprintf(stderr, "Current Robot Node : %d\n", nd->id);
    //map<spatialRelation, vector<regionProb> > sr_results;
    vector<spatialResults> sr_info_gain;

    //if likelihood is below this threshold - set to 0 - so we don't end up using low likelihood differences 
    double min_yes_threshold = 0.2;
    
    double max_info_gain = 0;
    spatialRelation max_sr = INVALID;
    
    //it might not be advisable to keep this as RegionNode* as this might change by the time the answer is incorporated??
    vector<indexProb> max_sr_likelihood; 
    //we should calculate the entroy for when the answer is yes and no 
    for(int i=0; i < sr_list.size(); i++){
        //vector<regionProb> question_prob;
        vector<regionProb> yes_likelihood;
        vector<regionProb> no_likelihood;

        double p_yes = 0;
        fprintf(stderr, "Spatial Relation : %s\n", getSR(sr_list[i]).c_str());
        vector<indexProb> sr_likelihood; 

        for(int j=0; j < check_regions.size(); j++){
            SlamNode *region_node = check_regions[j].first->mean_node;
            assert(region_node);

            //what if none of the regions are not high-likely at the given SR 
            double prob_yes = getSRLikelihood(region_node->getPose(), sr_list[i], nd->getPose());
            
            //can this mess things up for the entropy calculation 
            if(prob_yes < min_yes_threshold)
                prob_yes = 0;

            fprintf(stderr, "\tRegion : %d (%f) - SR Prob : %f\n", check_regions[j].first->region_id, check_regions[j].second, prob_yes);

            sr_likelihood.push_back(make_pair(check_regions[j].first->region_id, prob_yes));

            double prob_no = 1 - prob_yes;

            double new_likelihood_yes = check_regions[j].second * prob_yes; 
            yes_likelihood.push_back(make_pair(check_regions[j].first, new_likelihood_yes));

            p_yes += new_likelihood_yes; 
            double new_likelihood_no = check_regions[j].second * prob_no; 
            no_likelihood.push_back(make_pair(check_regions[j].first, new_likelihood_no));
            //calculate the entroy of yes and entropy of a no 
            //question_prob.push_back(make_pair(region_node, prob));
        }

        double yes_entropy = calculateEntropy(yes_likelihood);
        double no_entropy = calculateEntropy(no_likelihood);

        //this should not be uniform - this won't capture how likely you are to get a positive/negative answer 
        //i.e. if there is no entitiy SR from the current location - then P_yes should be very small 
        //double p_yes = 0.5;
        double p_no = 1 - p_yes;//0.5;
        
        double expected_entropy = yes_entropy * p_yes + no_entropy * p_no;

        double information_gain = original_entropy - expected_entropy;
        fprintf(stderr, "Information gain : %f Yes (%f) : %f No (%f): %f - Original : %f Expected : %f\n", information_gain, 
                p_yes, yes_entropy, p_no, no_entropy, original_entropy, expected_entropy);
        sr_info_gain.push_back(make_pair(sr_list[i], information_gain));

        if(max_info_gain < information_gain){
            max_info_gain = information_gain;
            max_sr = sr_list[i];  
            max_sr_likelihood = sr_likelihood;
        }
    }
    //dont even need this sorting - if we just need the max 
    sort(sr_info_gain.begin(), sr_info_gain.end(), compareSRProbs);

    if(sr_info_gain.size() == 0)
        return make_pair(INVALID,0);

    //information gain threshold 
    
    
    //seems like its giving decent answers 
    for(int i=0; i < sr_info_gain.size(); i++){
        fprintf(stderr, "SR : %s - Expected Information Gain : %f\n", getSR(sr_info_gain[i].first).c_str(), sr_info_gain[i].second);
    }
    //print the expeced information gain 

    if(sr_info_gain[0].second > info_gain_threshold){
        return sr_info_gain[0];
    }                                           
    else{
        return make_pair(INVALID,0);
    }
}

double ComplexLanguageEvent::evalQuestionsAboutRegion(RegionNode *region, double info_gain_threshold, int *landmark){
    //evaluate the Spatial relations from the current location 
    vector<regionProb> figure_results = getFigureDistribution();    

    double figure_entropy = calculateEntropy(figure_results);

    vector<regionProb> landmark_results = getLandmarkDistribution();

    double landmark_entropy = calculateEntropy(landmark_results);
    
    fprintf(stderr, "Figure Entropy : %f\n", figure_entropy);
    fprintf(stderr, "Landmark Entropy : %f\n", landmark_entropy);

    double original_entropy = 0;
    vector<regionProb> check_regions; 
    if(figure_entropy == 0 && landmark_entropy == 0){
        fprintf(stderr, "No entropy over figure or landmarks\n");
        return 0;
    }    
    
    //we should check if the current region is part of the landmark or figure list 
    int region_is_figure = 0;
    int region_is_landmark = 0;

    //check how much the entropy gets reduced - this should be zero if the region isnt a landmark or figure 

    vector<regionProb> figure_new_dist_yes;
    vector<regionProb> figure_new_dist_no;
    double remainder_sum = 0;
    double prob_yes = 0;
    for(int i=0; i < figure_results.size(); i++){
        if(figure_results[i].first == region){
            region_is_figure = 1;
            figure_new_dist_yes.push_back(make_pair(figure_results[i].first, 1.0));
            figure_new_dist_no.push_back(make_pair(figure_results[i].first, 0.0));
            prob_yes = figure_results[i].second; 
        }
        else{
            remainder_sum += figure_results[i].second;
            figure_new_dist_yes.push_back(make_pair(figure_results[i].first, 0.0));
            figure_new_dist_no.push_back(make_pair(figure_results[i].first, figure_results[i].second));
        }        
    }

    double expected_figure_entropy_gain = 0;
    
    if(region_is_figure){
    //normalize the remainder 
        if(remainder_sum > 0){
            for(int i=0; i < figure_new_dist_no.size(); i++){
                figure_new_dist_no[i].second = figure_new_dist_no[i].second / remainder_sum; 
            }
        }

        double figure_new_entropy_yes = calculateEntropy(figure_new_dist_yes);
        double figure_new_entropy_no = calculateEntropy(figure_new_dist_no);
    
        double new_expected_entropy = figure_new_entropy_yes * prob_yes + (1-prob_yes) * figure_new_entropy_no;
        expected_figure_entropy_gain = figure_entropy - new_expected_entropy;
        //this is the expected entropy gain if we ask whether the current region is the figure 
        //and get a yes/no answer 
    }

    vector<regionProb> landmark_new_dist_yes;
    vector<regionProb> landmark_new_dist_no;
    remainder_sum = 0;
    prob_yes = 0;
    for(int i=0; i < landmark_results.size(); i++){
        if(landmark_results[i].first == region){
            region_is_landmark = 1;
            landmark_new_dist_yes.push_back(make_pair(landmark_results[i].first, 1.0));
            landmark_new_dist_no.push_back(make_pair(landmark_results[i].first, 0.0));
            prob_yes = landmark_results[i].second; 
        }
        else{
            remainder_sum += landmark_results[i].second;
            landmark_new_dist_yes.push_back(make_pair(landmark_results[i].first, 0.0));
            landmark_new_dist_no.push_back(make_pair(landmark_results[i].first, landmark_results[i].second));
        }        
    }

    double expected_landmark_entropy_gain = 0;
    
    if(region_is_landmark){
    //normalize the remainder 
        if(remainder_sum > 0){
            for(int i=0; i < landmark_new_dist_no.size(); i++){
                landmark_new_dist_no[i].second = landmark_new_dist_no[i].second / remainder_sum; 
            }
        }

        double landmark_new_entropy_yes = calculateEntropy(landmark_new_dist_yes);
        double landmark_new_entropy_no = calculateEntropy(landmark_new_dist_no);
    
        double new_expected_entropy = landmark_new_entropy_yes * prob_yes + (1-prob_yes) * landmark_new_entropy_no;
        expected_landmark_entropy_gain = landmark_entropy - new_expected_entropy;
        //this is the expected entropy gain if we ask whether the current region is the landmark
        //and get a yes/no answer 
    }
  
    fprintf(stderr, "Expected landmark gain in entropy : %f\n Figure gain in entropy : %f\n", 
            expected_landmark_entropy_gain, expected_figure_entropy_gain);    
    
    if(!region_is_figure && !region_is_landmark){
        fprintf(stderr, "Region is not a landmark of figure - skipping asking question\n");
        return 0;
    }

    if(expected_landmark_entropy_gain > expected_figure_entropy_gain){
        *landmark = 1;
        return expected_landmark_entropy_gain;
    }
    else{
        *landmark = 0;
        return expected_figure_entropy_gain;
    }    
}

void ComplexLanguageEvent::print(bool print_path){
    map<RegionNode *, RegionPath *>::iterator it; 
    if(print_path && node_paths.size() > 0){
        //we should sort this before printing 
        vector<RegionPath *> result_paths;//node_paths.size()); 

        for(it = node_paths.begin(); it != node_paths.end(); it++){
            RegionPath *path = it->second;            
            result_paths.push_back(path);
        }
        
        sort(result_paths.begin(), result_paths.end(), compareRegionPaths);

        //do we want to print the top N?? 
        for(int i=0; i< result_paths.size(); i++){
            result_paths[i]->printProbability();            
        }
    }

    vector<regionProb> figure_results = getFigureDistribution();    

    double figure_entropy = calculateEntropy(figure_results);

    vector<regionProb> landmark_results = getLandmarkDistribution();

    double landmark_entropy = calculateEntropy(landmark_results);
    
    fprintf(stderr, "Figure Entropy : %f\n", figure_entropy);
    fprintf(stderr, "Landmark Entropy : %f\n", landmark_entropy);
}


void ComplexLanguageEvent::updateLandmarks(vector<RegionNode *> landmarks){
    //this can do some pruning on the landmarks as well - since these are just the physically close regions 
    
    map<int, regionLandmark>::iterator it; 
    map<int, regionLandmark>::iterator it_invalid; 
    for(int i=0; i < landmarks.size(); i++){
        it = valid_landmarks.find(landmarks[i]->region_id);
        it_invalid = invalid_landmarks.find(landmarks[i]->region_id);
        if(it == valid_landmarks.end() && it_invalid == invalid_landmarks.end()){
            addLandmark(landmarks[i]);
        }
    }    
}

vector<RegionNode *> ComplexLanguageEvent::getInvalidLandmarks(){
    map<int, regionLandmark>::iterator it;
    vector<RegionNode *> landmarks;
    for(it = invalid_landmarks.begin(); it != invalid_landmarks.end(); it++){
        landmarks.push_back(it->second.first);
    }
    return landmarks;
}

vector<RegionNode *> ComplexLanguageEvent::getValidLandmarks(){
    map<int, regionLandmark>::iterator it;
    vector<RegionNode *> landmarks;
    for(it = valid_landmarks.begin(); it != valid_landmarks.end(); it++){
        landmarks.push_back(it->second.first);
    }
    return landmarks;
}

void ComplexLanguageEvent::addLandmark(RegionNode *landmark_node, int type){
    //check if the Region is a valid landmark - otherwise skip 
    //check if the likelihood of being landmark is above some threshold 
    if(type == 0){
        double threshold = 0.4;
        if(!use_factor_graph){
            //lets lower this threshold - HACK 
            threshold = 0.2;
        }

        //update this method to work with synonyms 
        double p_landmark = landmark_node->getProbabilityOfLabel(landmark);

        cout << "Landmark : " << landmark << " : " <<  landmark_node->region_id << " Prob : " << p_landmark << endl;

        if(p_landmark < threshold){
            fprintf(stderr, "Skiping for landmark (%d) : prob %.3f is too low\n", landmark_node->region_id, p_landmark);
            //we should add this to an invlid landmark pile - so that we don't keep checking them 
            //this likelihood can change - when nodes are bled to 
            invalid_landmarks.insert(make_pair(landmark_node->region_id, make_pair(landmark_node, p_landmark)));
            return;
        }

        valid_landmarks.insert(make_pair(landmark_node->region_id, make_pair(landmark_node, p_landmark)));
        //add the landmark to the region paths (it should skip its own self
        map<RegionNode *, RegionPath *>::iterator it; 
        for(it = node_paths.begin(); it != node_paths.end(); it++){
            it->second->addLandmark(make_pair(landmark_node, p_landmark));
        }
    }
    else if(type == 1){
        double p_landmark = landmark_node->getProbabilityOfLabel(landmark);
        valid_landmarks.insert(make_pair(landmark_node->region_id, make_pair(landmark_node, p_landmark)));
        fprintf(stderr, "Valid landmark : %d - %f\n", landmark_node->region_id, p_landmark);
        //add the landmark to the region paths (it should skip its own self
        map<RegionNode *, RegionPath *>::iterator it; 
        for(it = node_paths.begin(); it != node_paths.end(); it++){
            it->second->addLandmark(make_pair(landmark_node, p_landmark));
        }
    }
    else if(type == 2){
        double p_landmark = landmark_node->getProbabilityOfLabel(landmark);
        fprintf(stderr, "Invalid landmark : %d - %f\n", landmark_node->region_id, p_landmark);
        invalid_landmarks.insert(make_pair(landmark_node->region_id, make_pair(landmark_node, p_landmark)));
    }
}

void ComplexLanguageEvent::removeLandmark(RegionNode *region){
    map<int, regionLandmark>::iterator it; 
    it = valid_landmarks.find(region->region_id);

    //remove the landmark from the event and the paths 
    if(it != valid_landmarks.end()){
        valid_landmarks.erase(it);
        map<RegionNode *, RegionPath *>::iterator it_p; 
        for(it_p = node_paths.begin(); it_p != node_paths.end(); it_p++){
            it_p->second->removeLandmark(region);
        }
    }

    it = invalid_landmarks.find(region->region_id);

    //remove the landmark from the event and the paths 
    if(it != invalid_landmarks.end()){
        invalid_landmarks.erase(it);        
    }
}
