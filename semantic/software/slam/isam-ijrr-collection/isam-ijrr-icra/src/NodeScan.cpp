/*
 * NodeScan.cpp
 *
 *  Created on: Oct 22, 2009
 *      Author: abachrac
 */

#include "NodeScan.h"
using namespace scanmatch;

NodeScan::NodeScan(int64_t _utime, int _node_id, Scan * _scan, bool _is_supernode, Pose2d _odom_pose, classification_result_t *_laser_classification, classification_result_t *_image_classification):
    utime(_utime), node_id(_node_id), scan(_scan)
{
    allScans.push_back(scan);
    pose2d_node = NULL;
    inc_constraint_sm = NULL;
    inc_constraint_odom = NULL;
    laser_classification = NULL;
    image_classification = NULL;
    laser = NULL;
    odom_pose = _odom_pose;

    if(_laser_classification){
        laser_classification = classification_result_t_copy(_laser_classification);
        double buffer = 0.2;
        //we should add and normalize this 
        double sum = 1+ buffer * laser_classification->count;
        for(int i=0; i < laser_classification->count; i++){
            laser_classification_obs.insert(make_pair(laser_classification->classes[i].type, (laser_classification->classes[i].prob + buffer)/sum));
        }
    }
    if(_image_classification){
        image_classification = classification_result_t_copy(_image_classification);
        double buffer = 0.2;
        double sum = 1+ buffer * image_classification->count;
        for(int i=0; i < image_classification->count; i++){
            image_classification_obs.insert(make_pair(image_classification->classes[i].type, (image_classification->classes[i].prob + buffer)/sum));
        }
    }

    is_supernode = _is_supernode;
    transition_node = false;
    
    //create a kd tree 
    kd_points = (ann_t *) calloc(1,sizeof(ann_t));
    //add the scans 
    kd_points->dataPts = annAllocPts(scan->numPoints, 2);
    kd_points->no_points = scan->numPoints;
    //put the points in the data structure 
    for (unsigned i = 0; i < scan->numPoints; i++) {  
           
        kd_points->dataPts[i][0] = scan->points[i].x;
        kd_points->dataPts[i][1] = scan->points[i].y;
    }

    kd_tree = new ANNkd_tree(kd_points->dataPts, 
                             kd_points->no_points,
                             2);
    max_prob = 0;
}

NodeScan::NodeScan(NodeScan *node)
{
    Scan *new_scan = new Scan(node->scan->numPoints, node->scan->points, node->scan->T, node->scan->laser_type, 
                           node->scan->utime);

    NodeScan(node->utime, node->node_id, new_scan, node->is_supernode, odom_pose, node->laser_classification, node->image_classification);
    //need to copy the other data structures as well 
}

/*double NodeScan::getObsProbLaser(int class_id){
    double buffer = 0.05;
    
    if(laser_classification_obs.size() > 0){
        map<int, double>::iterator it;
        double max_prob = 0;
        for(it = laser_classification_obs.begin(); it != laser_classification_obs.end(); it++){
            if(it->second > max_prob){
                max_prob = it->second;
            }
        }
        it = laser_classification_obs.find(class_id);
        if(it == laser_classification_obs.end()){
            return buffer;
        }
        else if(it->second == max_prob){
            return 1;
        }
        else{
            return buffer;
        }
        //return it->second + buffer;
    }
    else
        return buffer;
}

double NodeScan::getObsProbImage(int class_id){
    double buffer = 0.05;
    
    if(image_classification_obs.size() > 0){
        map<int, double>::iterator it;
        double max_prob = 0;
        for(it = image_classification_obs.begin(); it != image_classification_obs.end(); it++){
            if(it->second > max_prob){
                max_prob = it->second;
            }
        }
        it = image_classification_obs.find(class_id);
        if(it == image_classification_obs.end()){
            return buffer;
        }
        else if(it->second == max_prob){
            return 1;
        }
        else{
            return buffer;
        }
        //return it->second + buffer;
    }
    else
        return buffer;
        }*/

double NodeScan::getObsProbLaser(int class_id){
    double buffer = 0.05;
    
    if(laser_classification_obs.size() > 0){
        map<int, double>::iterator it;
        it = laser_classification_obs.find(class_id);
        if(it == laser_classification_obs.end()){
            return buffer;
        }
        return it->second + buffer;
    }
    else
        return buffer;
}

double NodeScan::getObsProbImage(int class_id){
    double buffer = 0.05;
    if(image_classification_obs.size() > 0){
        map<int, double>::iterator it;
        it = image_classification_obs.find(class_id);
        if(it == image_classification_obs.end()){
            return buffer;
        }
        return it->second + buffer;
    }
    else
        return buffer;

}

NodeScan::~NodeScan()
{
    delete scan;
    delete inc_constraint_sm;
    delete inc_constraint_odom;
    if(laser)
        bot_core_planar_lidar_t_destroy(laser);
}

int NodeScan::updateScan()
{
    fprintf (stdout, "NodeScan::updateScan() - Nothing Happens!!!\n");
    return 0;
}
