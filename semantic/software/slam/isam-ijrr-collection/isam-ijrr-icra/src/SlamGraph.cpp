#include "SlamGraph.hpp"
#include "Utils.hpp"

#define LANGUAGE_TOP_N_GROUNDINGS 5
#define MIN_LANG_GROUNDING_PROB 0.1
#define NO_NODES_ADDED_AFTER_FAILURE 30


using namespace Utils;

int SlamGraph::asked_question_id = 0;

SlamGraph::SlamGraph(LabelInfo *_label_info, int64_t _utime, bool _use_factor_graphs, double _ignore_landmark_dist, double _ignore_figure_dist, bool _use_laser_for_semantic, bool _use_image_for_semantic):
    ignore_landmark_dist(_ignore_landmark_dist), ignore_figure_dist(_ignore_figure_dist), last_region_id(0), 
    last_segment_id(0), no_segments_in_region(0), no_edges_added(0), status(0), last_added_node(NULL), 
    last_created_region(NULL), last_segment(NULL), factor_graph(NULL), 
    use_laser_for_semantic(_use_laser_for_semantic), use_image_for_semantic(_use_image_for_semantic), 
    utime(_utime), updated(1), outstanding_question(NULL), asked_question(NULL), use_factor_graphs(_use_factor_graphs)
{
    //label_info = NULL;
    slam = new Slam();
    label_info = _label_info;
    //add the origin node 
    origin_node = new Pose2d_Node();
    slam->add_node(origin_node);
    //add prior for the origin 
    Pose2d prior_origin(0., 0., 0.);
    Noise noise = SqrtInformation(10000. * eye(3));
    prior = new Pose2d_Factor(origin_node, prior_origin, noise);
    slam->add_factor(prior);
    new_regions_added_threshold = 5;
}

 SlamGraph::~SlamGraph()
{
    delete slam;
    //might want to look at this 
    delete prior;
    delete origin_node;
    map<int, SlamNode *>::iterator it;
    for ( it= slam_nodes.begin() ; it != slam_nodes.end(); it++ )
        delete it->second;
    slam_nodes.clear();
    map<int, SlamConstraint *>::iterator c_it;
    for ( c_it= slam_constraints.begin() ; c_it != slam_constraints.end(); c_it++ ){        
        delete c_it->second;
    }
    
    slam_constraints.clear();
    for ( c_it= failed_slam_constraints.begin() ; c_it != failed_slam_constraints.end(); c_it++ ){        
        delete c_it->second;
    }
    failed_slam_constraints.clear();    
    
    delete factor_graph; 
    delete outstanding_question;
    delete asked_question;
}

void SlamGraph::updateObjectPoses(){
    map<int, SlamObject *>::iterator it_o;

    fprintf(stderr, "MAJENTA \n\n================= Objects [%d] ===============\n\n" RESET_COLOR, (int) object_map.size());

    for(it_o = object_map.begin(); it_o != object_map.end(); it_o++){
        SlamObject *object = it_o->second;
        object->updatePose();
        //object->print();
    }
}

void SlamGraph::updateObjectSlamNode(SlamObject *object){
    //search through the detection - find the node that is closest to the observation 
    //for now lets use only the nodes from which it was observed from 
    ObjectDetection *det = object->objectDetection;
    map<NodeScan *, objectNodePair *>::iterator it;

    objectNodePair *closest_node_pair = det->closest_node_pair;
        
    if(closest_node_pair){
        NodeScan *nd_scan = closest_node_pair->nd;
        SlamNode *node = getSlamNodeFromID(nd_scan->node_id);
        object->updateAssignedNode(node, label_info);
    }
}

void SlamGraph::updateObjects(){
    if(object_map.size() == 0)
        return;
    map<int, SlamObject *>::iterator it_o;
    for(it_o = object_map.begin(); it_o != object_map.end(); it_o++){
        SlamObject *object = it_o->second;
        //we can probably skip this if we set the closest node scan when its updated 
        updateObjectSlamNode(object);
    }
    updateObjectPoses();
}

void SlamGraph::updateObjects(map<int, ObjectDetection *> &objects){
    fprintf(stderr, "\n\n Update Objects Called\n\n");

    if(objects.size() == 0)
        return;
    map<int, ObjectDetection *>::iterator it_d;
    map<int, SlamObject *>::iterator it_o;
    for(it_d = objects.begin(); it_d != objects.end(); it_d++){
        ObjectDetection *det = it_d->second;
        
        it_o = object_map.find(it_d->first);
        SlamObject *object = NULL;
        if(it_o == object_map.end()){
            fprintf(stderr, "Object Not found in particle - Creating new object\n");
            object = new SlamObject(det, label_info);            
            object_map.insert(make_pair(object->id, object));
        }
        else{
            //we don't need to do this since its already pointing to the right one
            object = it_o->second;
        }
        //we can probably skip this if we set the closest node scan when its updated 
        updateObjectSlamNode(object);
    }
    updateObjectPoses();
}

void SlamGraph::updateComplexLanguage(map<int, ComplexLanguageEvent *> &language_to_match){
    //clear the existing complex language groundings - and recreate them from the given set of groundings
    //the regions and nodes in this complex language event map are from a different particle 
    
    //for each complex language get each grounded figure 
    //and add factors 
    map<int, ComplexLanguageEvent *>::iterator it_lang; 
    map<int, ComplexLanguageEvent *>::iterator it;
    for(it_lang = language_to_match.begin(); it_lang != language_to_match.end(); it_lang++){
        //two data structures maintain the complex language - but neither map should need to change 
        ComplexLanguageEvent *target_event = it_lang->second; //this is the target - we want the old event to match the target event 
        it = complex_language_events.find(it_lang->first);
        assert(it != complex_language_events.end());
            
        ComplexLanguageEvent *old_event = it->second;
        
        //remove the old landmarks
        old_event->clearLandmarks();
        old_event->clearNodePaths(); //we need to compy over the paths from the target 
        fprintf(stderr, "Cleared landmarks and figures\n");

        //now lets fill these from the other particle 
        vector<RegionNode *> valid_lm = target_event->getValidLandmarks();
        vector<RegionNode *> invalid_lm = target_event->getInvalidLandmarks();

        for(int i=0; i < valid_lm.size(); i++){
            RegionNode *nd = valid_lm[i];
            RegionNode *c_nd = getRegionFromID(nd->region_id);
            assert(c_nd);
            old_event->addLandmark(c_nd, 1); //adding as valid landmark
            fprintf(stderr, "Adding Valid Landmark : %d\n", c_nd->region_id);
        }

        for(int i=0; i < invalid_lm.size(); i++){
            RegionNode *nd = invalid_lm[i];
            RegionNode *c_nd = getRegionFromID(nd->region_id);
            assert(c_nd);
            fprintf(stderr, "Adding Invalid Landmark : %d\n", c_nd->region_id);
            old_event->addLandmark(c_nd, 2); //adding as invalid landmark
        }

        //add the node paths 
        vector<RegionPath *> region_paths = target_event->getRegionPaths();
        for(int i=0; i < region_paths.size(); i++){
            RegionPath *rp = region_paths[i];
            RegionNode *c_nd = getRegionFromID(rp->region->region_id);
            old_event->addNewRegionPath(c_nd, rp->path, rp->getPathProbs(), rp->prob);
        }
        
        old_event->complete = target_event->complete;
        old_event->handled_internally = target_event->handled_internally;
        old_event->failed_grounding = target_event->failed_grounding;
        old_event->number_of_regions_added_since_failure = target_event->number_of_regions_added_since_failure;
    }
    //this is being done by the particle 
    //runBP();
}

void SlamGraph::runBP(bool ignore_language){
    if(factor_graph){
        //hopefully this wont segfault when factors are removed when regions get killed off 
        delete factor_graph; 
    }
        
    fprintf(stderr, "-------- Running BP ---------\n");

    int64_t s_utime = bot_timestamp_now();
    
    //lets rebuild this here 
    vector<SemFactor> semantic_factors; 

     vector<SemFactor> semantic_factors_no_lang; 

    bool print_factors = false;

    ///map<int, RegionNode *> valid_region_labels; 
    for(int i=0; i < region_node_list.size(); i++){
        RegionNode *region = region_node_list[i];
        
        semantic_factors.push_back(*region->label_prior);
        semantic_factors_no_lang.push_back(*region->label_prior);
        if(print_factors){
            cout << "Region ID : " << region->region_id << endl;
            cout << "Region Label Prior: " << *region->label_prior << endl;
        }

        //the region label distribution has an effect on the region type distribution 
        if(region->region_type_to_label){
            semantic_factors.push_back(*region->region_type_to_label);
            semantic_factors_no_lang.push_back(*region->region_type_to_label);
            if(print_factors)
                cout << "Region Type to Label : " << *region->region_type_to_label << endl;
        }

        if(region->region_type_given_appearance){
            semantic_factors.push_back(*region->region_type_given_appearance);
            semantic_factors_no_lang.push_back(*region->region_type_given_appearance);
            if(print_factors)
                cout << "Region Type to Appearence : " << *region->region_type_to_label << endl;
        }

        map<nodePairKey, SemFactor *>::iterator it;
        //add p_type_given_observation
        for(it = region->prob_type_given_observation.begin(); it != region->prob_type_given_observation.end(); it++){
            //cout << "Adding factor : " << *it->second <<endl;
            semantic_factors.push_back(*it->second); //*region->region_type);
            semantic_factors_no_lang.push_back(*it->second);
            if(print_factors)
                cout << "Obs Factor Image : " << *it->second << endl;
        }

        //add p_obs
        for(int j=0; j < region->nodes.size(); j++){
            SlamNode *node = region->nodes[j];
            /*if(use_laser_for_semantic){
                semantic_factors.push_back(*node->sem_type_laser_obs);
                semantic_factors.push_back(*node->node_type_to_laser_obs);
            }
            if(use_image_for_semantic){
                semantic_factors.push_back(*node->sem_type_image_obs);
                semantic_factors.push_back(*node->node_type_to_image_obs);
                }*/

            semantic_factors.push_back(*node->node_appearance_factor);
            semantic_factors_no_lang.push_back(*node->node_appearance_factor);
            
            if(print_factors){
                cout << "Adding factor : " << *node->sem_type_laser_obs;
                cout << "Adding factor : " << *node->sem_type_image_obs;
            }

            for(int k=0; k < node->language_observations.size(); k++){
                //valid_region_labels.insert(make_pair(region->region_id, )
                LanguageObservation *lang_obs =  node->language_observations[k];
                semantic_factors.push_back(*lang_obs->phi_obs_factor);
                semantic_factors.push_back(*lang_obs->label_obs_factor);
                if(lang_obs->label_phi_region){
                    semantic_factors.push_back(*lang_obs->label_phi_region);
                    if(print_factors)
                        cout << "Language Phi To Label To Region Type " << *lang_obs->label_phi_region << endl; 
                }
                else{
                    fprintf(stderr, "Region Label to Node label factor has not been created - creating factor\n");
                    lang_obs->label_phi_region = label_info->getRegionLabelFactor(lang_obs->label_obs_node, lang_obs->phi, region->region_label);
                    semantic_factors.push_back(*lang_obs->label_phi_region);
                    if(print_factors)
                        cout << "Language Phi To Label To Region Type " << *lang_obs->label_phi_region << endl; 
                }
            }
            //cout << "\nObs Factor Laser : " << *node->sem_type_laser_obs << endl;
            //cout << "Obs Factor Image : " << *node->sem_type_image_obs << endl;
        }
    }

    //add objects 
    map<int, SlamObject *>::iterator it_o;
    for(it_o = object_map.begin(); it_o != object_map.end(); it_o++){
        SlamObject *object = it_o->second;
        //fprintf(stderr, "Object : %d => Type : %d\n", object->id, object->type);
        semantic_factors.push_back(*object->object_type_observation);
        semantic_factors_no_lang.push_back(*object->object_type_observation);
        //this can cause issues when resampling is done - the object to region factors need to be trashed 

        if(object->object_to_region_type_factor){
            //cout << *object->object_to_region_type_factor << endl;
            semantic_factors.push_back(*object->object_to_region_type_factor);
            semantic_factors_no_lang.push_back(*object->object_to_region_type_factor);
        }
    }

    if(1){
        //add this to the languageless factor graph 
        SemFactorGraph *factor_graph_no_lang = new SemFactorGraph(semantic_factors_no_lang);
        fprintf(stderr, BLUE "Running Belief propagation\n" RESET_COLOR);
 
        // Store the constants in a PropertySet object
        dai::PropertySet opts;

        // Set some constants
        size_t maxiter = 10000;
        dai::Real   tol = 1e-9;
        size_t verb = 1;
    
        opts.set("maxiter", maxiter);  // Maximum number of iterations
        opts.set("tol", tol);          // Tolerance for convergence
        opts.set("verbose",verb);     // Verbosity (amount of output generated)
        //opts.set("logdomain",true); 
    
        dai::BP bp_no_lang(*factor_graph_no_lang, opts("updates",string("SEQRND"))("logdomain",true));
        // Initialize belief propagation algorithm
        bp_no_lang.init();
        // Run belief propagation algorithm
        bp_no_lang.run();
    
        int v = 0;
        for(int i =0; i < region_node_list.size(); i++){
            RegionNode *region = region_node_list[i];
        
            SemFactor r_label_fac = bp_no_lang.belief(*region->region_label);
            r_label_fac.normalize();
            if(v)
                cout << "\nRegion (" << region->region_id << ") : Label Factor => " << r_label_fac << endl;  

            region->region_label_dist_no_lang.clear();
            //cout << bp.belief(*region->region_type) << endl; // display the belief of bp for the variables in that factor
            if(v) 
                fprintf(stderr, BLUE "=== Region : %d (Label) =====\n" RESET_COLOR, region->region_id);

            for(int j=0; j < region->region_label->states(); j++){
                //dai::Real denom = P.marginal (*(region->region_type))[j];
                dai::Real denom = r_label_fac[j];
                region->region_label_dist_no_lang.insert(make_pair(j, denom));
                if(v){
                    if(denom < 0.01)
                        continue;
                    fprintf(stderr, BLUE "\t%s = %.3f\n" RESET_COLOR, label_info->getLabelNameFromID(j).c_str(),denom);
                }
            }

            if(v)
                fprintf(stderr, BLUE " ==== Region : %d (Type) =====\n" RESET_COLOR, region->region_id);

            // Report factor marginals for fg, calculated by the belief propagation algorithm
            //cout << "Approximate (loopy belief propagation) factor marginals:" << endl;
            //cout << bp.belief(fg.factor(I).vars()) << endl; // display the belief of bp for the variables in that factor
            SemFactor r_fac = bp_no_lang.belief(*region->region_type);
            r_fac.normalize();
            region->region_type_dist_no_lang.clear();
            //cout << bp.belief(*region->region_type) << endl; // display the belief of bp for the variables in that factor
            for(int j=0; j < region->region_type->states(); j++){
                //dai::Real denom = P.marginal (*(region->region_type))[j];
                dai::Real denom = r_fac[j];
                region->region_type_dist_no_lang.insert(make_pair(j, denom));
                if(v){
                    if(denom < 0.01)
                        continue;
                    fprintf(stderr, BLUE "\t%s = %.3f\n" RESET_COLOR, label_info->getTypeNameFromID(j).c_str(),denom);
                }
            }
        }
        delete factor_graph_no_lang;
    }

    //for each complex language get each grounded figure 
    //and add factors 
    if(!ignore_language){
        if(0){
            map<int, ComplexLanguageEvent *>::iterator it_lang; 
            for(it_lang = complex_language_events.begin(); it_lang != complex_language_events.end(); it_lang++){
                vector<RegionPath *> grounded_regions = it_lang->second->getGroundedRegionPaths();
                //should we keep them in language? 
                for(int l=0; l < grounded_regions.size(); l++){
                    //hard coded for now - replace 
                    if(grounded_regions[l]->getProbability() < 0.2){
                        fprintf(stderr, "Region %d Grounding Threshold %f too low - skipping\n", grounded_regions[l]->region->region_id, 
                                grounded_regions[l]->getProbability());
                        break;
                    }
                    //create a language observation factor 
                    //should prob add it to the region 

                    else{
                        LanguageObservation *lang_obs = grounded_regions[l]->lang_obs;
                        if(lang_obs){
                            semantic_factors.push_back(*lang_obs->phi_obs_factor);
                            semantic_factors.push_back(*lang_obs->label_obs_factor);
                            semantic_factors.push_back(*lang_obs->label_phi_region);
                            fprintf(stderr, "Adding Language factor\n");
                        }
                        else{
                            fprintf(stderr, "No language factor found for complex language event - Error - Should not call runBP\n");
                            exit(-1);
                        }
                    }
                }
            }
        }
        else{
            //thos one normalizes the grounding probs 
            map<int, ComplexLanguageEvent *>::iterator it_lang; 
        
            int count = 0;

            for(it_lang = complex_language_events.begin(); it_lang != complex_language_events.end(); it_lang++){
                vector<regionPathProb> grounded_regions = it_lang->second->getNormalizePaths(LANGUAGE_TOP_N_GROUNDINGS);
                //should we keep them in language? 
                it_lang->second->print(false);
                fprintf(stderr, "========== No of grounded region paths : %d\n", (int) grounded_regions.size());
                //exit(-1);
                for(int l=0; l < grounded_regions.size(); l++){
                    //hard coded for now - replace 
                    //fprintf(stderr, "\t=>Region %d : Prob: %f\n", grounded_regions[l].first->region->region_id, 
                    //grounded_regions[l].second);
                    count++;
                    LanguageObservation *lang_obs = grounded_regions[l].first->lang_obs;
                    if(lang_obs){
                        semantic_factors.push_back(*lang_obs->phi_obs_factor);
                        semantic_factors.push_back(*lang_obs->label_obs_factor);
                        //update the phi value?? 
                        lang_obs->setPhi(grounded_regions[l].second);
                        semantic_factors.push_back(*lang_obs->label_phi_region);         
                    }
                    else{
                        fprintf(stderr, "No language factor found for complex language event - Error - Should not call runBP\n");
                        //exit(-1);
                    }
                }
            }
        }
    }    

    //now add factors for the objects 
    
    factor_graph = new SemFactorGraph(semantic_factors);
    fprintf(stderr, BLUE "Running Belief propagation\n" RESET_COLOR);
     
    // Store the constants in a PropertySet object
    dai::PropertySet opts;

    // Set some constants
    size_t maxiter = 10000;
    dai::Real   tol = 1e-9;
    size_t verb = 1;
    
    opts.set("maxiter", maxiter);  // Maximum number of iterations
    opts.set("tol", tol);          // Tolerance for convergence
    opts.set("verbose",verb);     // Verbosity (amount of output generated)
    //opts.set("logdomain",true); 
    
    dai::BP bp(*factor_graph, opts("updates",string("SEQRND"))("logdomain",true));
    // Initialize belief propagation algorithm
    bp.init();
    // Run belief propagation algorithm
    bp.run();
    
    int v = 0;
    
    for(int i =0; i < region_node_list.size(); i++){
        RegionNode *region = region_node_list[i];
        
        SemFactor r_label_fac = bp.belief(*region->region_label);
        r_label_fac.normalize();
        if(v)
            cout << "\nRegion (" << region->region_id << ") : Label Factor => " << r_label_fac << endl;  

        region->region_label_dist.clear();
        //cout << bp.belief(*region->region_type) << endl; // display the belief of bp for the variables in that factor
        if(v) 
            fprintf(stderr, BLUE "=== Region : %d (Label) =====\n" RESET_COLOR, region->region_id);

        for(int j=0; j < region->region_label->states(); j++){
            //dai::Real denom = P.marginal (*(region->region_type))[j];
            dai::Real denom = r_label_fac[j];
            region->region_label_dist.insert(make_pair(j, denom));
            if(v){
                if(denom < 0.01)
                    continue;
                fprintf(stderr, BLUE "\t%s = %.3f\n" RESET_COLOR, label_info->getLabelNameFromID(j).c_str(),denom);
            }
        }

        if(v)
            fprintf(stderr, BLUE " ==== Region : %d (Type) =====\n" RESET_COLOR, region->region_id);

        // Report factor marginals for fg, calculated by the belief propagation algorithm
        //cout << "Approximate (loopy belief propagation) factor marginals:" << endl;
        //cout << bp.belief(fg.factor(I).vars()) << endl; // display the belief of bp for the variables in that factor
        SemFactor r_fac = bp.belief(*region->region_type);
        r_fac.normalize();
        region->region_type_dist.clear();
        //cout << bp.belief(*region->region_type) << endl; // display the belief of bp for the variables in that factor
        for(int j=0; j < region->region_type->states(); j++){
            //dai::Real denom = P.marginal (*(region->region_type))[j];
            dai::Real denom = r_fac[j];
            region->region_type_dist.insert(make_pair(j, denom));
            if(v){
                if(denom < 0.01)
                    continue;
                fprintf(stderr, BLUE "\t%s = %.3f\n" RESET_COLOR, label_info->getTypeNameFromID(j).c_str(),denom);
            }
        }
    }

    int64_t e_utime = bot_timestamp_now();
    
    fprintf(stderr, "Total Time for BP : %f\n", (e_utime - s_utime) / 1.0e6);
}

void SlamGraph::runBP(RegionNode *region){
    if(factor_graph){
        //hopefully this wont segfault when factors are removed when regions get killed off 
        delete factor_graph; 
    }
        
    int64_t s_utime = bot_timestamp_now();
    
    //lets rebuild this here 
    vector<SemFactor> semantic_factors; 
    vector<SemFactor> semantic_factors_no_lang; 

    bool print_factors = false;

           
    semantic_factors.push_back(*region->label_prior);
    semantic_factors_no_lang.push_back(*region->label_prior);
    if(print_factors){
        cout << "Region ID : " << region->region_id << endl;
        cout << "Region Label Prior: " << *region->label_prior << endl;
    }
    
    //the region label distribution has an effect on the region type distribution 
    if(region->region_type_to_label){
        semantic_factors.push_back(*region->region_type_to_label);
        semantic_factors_no_lang.push_back(*region->region_type_to_label);
        if(print_factors)
            cout << "Region Type to Label : " << *region->region_type_to_label << endl;
    }
    
    if(region->region_type_given_appearance){
        semantic_factors.push_back(*region->region_type_given_appearance);
        semantic_factors_no_lang.push_back(*region->region_type_given_appearance);
        if(print_factors)
            cout << "Region Type to Appearence : " << *region->region_type_to_label << endl;
    }
    
    map<nodePairKey, SemFactor *>::iterator it;
    //add p_type_given_observation
    for(it = region->prob_type_given_observation.begin(); it != region->prob_type_given_observation.end(); it++){
        //cout << "Adding factor : " << *it->second <<endl;
        semantic_factors.push_back(*it->second); //*region->region_type);
        semantic_factors_no_lang.push_back(*it->second); //*region->region_type);
        if(print_factors)
            cout << "Obs Factor Image : " << *it->second << endl;
    }
    
    //add p_obs
    for(int j=0; j < region->nodes.size(); j++){
        SlamNode *node = region->nodes[j];
        /*if(use_laser_for_semantic){
          semantic_factors.push_back(*node->sem_type_laser_obs);
          semantic_factors.push_back(*node->node_type_to_laser_obs);
          }
          if(use_image_for_semantic){
          semantic_factors.push_back(*node->sem_type_image_obs);
          semantic_factors.push_back(*node->node_type_to_image_obs);
          }*/
        
        semantic_factors.push_back(*node->node_appearance_factor);
        semantic_factors_no_lang.push_back(*node->node_appearance_factor);
        
        if(print_factors){
            cout << "Adding factor : " << *node->sem_type_laser_obs;
            cout << "Adding factor : " << *node->sem_type_image_obs;
        }
        
        for(int k=0; k < node->language_observations.size(); k++){
            //valid_region_labels.insert(make_pair(region->region_id, )
            LanguageObservation *lang_obs =  node->language_observations[k];
            semantic_factors.push_back(*lang_obs->phi_obs_factor);
            semantic_factors.push_back(*lang_obs->label_obs_factor);
            if(lang_obs->label_phi_region){
                semantic_factors.push_back(*lang_obs->label_phi_region);
                if(print_factors)
                    cout << "Language Phi To Label To Region Type " << *lang_obs->label_phi_region << endl; 
            }
            else{
                fprintf(stderr, "Region Label to Node label factor has not been created - creating factor\n");
                lang_obs->label_phi_region = label_info->getRegionLabelFactor(lang_obs->label_obs_node, lang_obs->phi, region->region_label);
                semantic_factors.push_back(*lang_obs->label_phi_region);
                if(print_factors)
                    cout << "Language Phi To Label To Region Type " << *lang_obs->label_phi_region << endl; 
            }
        }
        //cout << "\nObs Factor Laser : " << *node->sem_type_laser_obs << endl;
        //cout << "Obs Factor Image : " << *node->sem_type_image_obs << endl;
    }

    map<int, SlamObject *>::iterator it_o;
    for(it_o = region->observed_objects.begin(); it_o != region->observed_objects.end(); it_o++){
        SlamObject *object = it_o->second;
        //fprintf(stderr, "Object : %d => Type : %d\n", object->id, object->type);
        semantic_factors.push_back(*object->object_type_observation);
        semantic_factors_no_lang.push_back(*object->object_type_observation);
        
        //this can cause issues when resampling is done - the object to region factors need to be trashed 
        
        if(object->object_to_region_type_factor){
            //cout << *object->object_to_region_type_factor << endl;
            semantic_factors.push_back(*object->object_to_region_type_factor);
            semantic_factors_no_lang.push_back(*object->object_to_region_type_factor);
        }
    }
    
    //add this to the languageless factor graph 
    if(1){
        SemFactorGraph *factor_graph_no_lang = new SemFactorGraph(semantic_factors_no_lang);
        fprintf(stderr, BLUE "Running Belief propagation\n" RESET_COLOR);
 
        // Store the constants in a PropertySet object
        dai::PropertySet opts;

        // Set some constants
        size_t maxiter = 10000;
        dai::Real   tol = 1e-9;
        size_t verb = 1;
    
        opts.set("maxiter", maxiter);  // Maximum number of iterations
        opts.set("tol", tol);          // Tolerance for convergence
        opts.set("verbose",verb);     // Verbosity (amount of output generated)
        //opts.set("logdomain",true); 
    
        dai::BP bp_no_lang(*factor_graph_no_lang, opts("updates",string("SEQRND"))("logdomain",true));
        // Initialize belief propagation algorithm
        bp_no_lang.init();
        // Run belief propagation algorithm
        bp_no_lang.run();
    
        int v = 0;
           
        SemFactor r_label_fac = bp_no_lang.belief(*region->region_label);
        r_label_fac.normalize();
        if(v)
            cout << "\nRegion (" << region->region_id << ") : Label Factor => " << r_label_fac << endl;  

        region->region_label_dist_no_lang.clear();
        //cout << bp.belief(*region->region_type) << endl; // display the belief of bp for the variables in that factor
        if(v) 
            fprintf(stderr, BLUE "=== Region : %d (Label) =====\n" RESET_COLOR, region->region_id);

        for(int j=0; j < region->region_label->states(); j++){
            //dai::Real denom = P.marginal (*(region->region_type))[j];
            dai::Real denom = r_label_fac[j];
            region->region_label_dist_no_lang.insert(make_pair(j, denom));
            if(v){
                if(denom < 0.01)
                    continue;
                fprintf(stderr, BLUE "\t%s = %.3f\n" RESET_COLOR, label_info->getLabelNameFromID(j).c_str(),denom);
            }
        }

        if(v)
            fprintf(stderr, BLUE " ==== Region : %d (Type) =====\n" RESET_COLOR, region->region_id);

        // Report factor marginals for fg, calculated by the belief propagation algorithm
        //cout << "Approximate (loopy belief propagation) factor marginals:" << endl;
        //cout << bp.belief(fg.factor(I).vars()) << endl; // display the belief of bp for the variables in that factor
        SemFactor r_fac = bp_no_lang.belief(*region->region_type);
        r_fac.normalize();
        region->region_type_dist_no_lang.clear();
        //cout << bp.belief(*region->region_type) << endl; // display the belief of bp for the variables in that factor
        for(int j=0; j < region->region_type->states(); j++){
            //dai::Real denom = P.marginal (*(region->region_type))[j];
            dai::Real denom = r_fac[j];
            region->region_type_dist_no_lang.insert(make_pair(j, denom));
            if(v){
                if(denom < 0.01)
                    continue;
                fprintf(stderr, BLUE "\t%s = %.3f\n" RESET_COLOR, label_info->getTypeNameFromID(j).c_str(),denom);
            }
        }

        delete factor_graph_no_lang;
    }

    if(0){
        map<int, ComplexLanguageEvent *>::iterator it_lang; 
        for(it_lang = complex_language_events.begin(); it_lang != complex_language_events.end(); it_lang++){
            vector<RegionPath *> grounded_regions = it_lang->second->getGroundedRegionPaths();
            //should we keep them in language? 
            for(int l=0; l < grounded_regions.size(); l++){
                if(grounded_regions[l]->region != region)
                    continue;
                //hard coded for now - replace 
                if(grounded_regions[l]->getProbability() < 0.2){
                    fprintf(stderr, "Region %d Grounding Threshold %f too low - skipping\n", grounded_regions[l]->region->region_id, 
                            grounded_regions[l]->getProbability());
                    break;
                }
                //create a language observation factor 
                //should prob add it to the region 

                else{
                    //there should be a better way to get this 
                    if(grounded_regions[l]->region != region)
                        continue;
                    LanguageObservation *lang_obs = grounded_regions[l]->lang_obs;
                    if(lang_obs){
                        semantic_factors.push_back(*lang_obs->phi_obs_factor);
                        semantic_factors.push_back(*lang_obs->label_obs_factor);
                        semantic_factors.push_back(*lang_obs->label_phi_region);
                        fprintf(stderr, "Adding Language factor\n");
                    }
                    else{
                        fprintf(stderr, "No language factor found for complex language event - Error - Should not call runBP\n");
                        exit(-1);
                    }
                }
            }
        }
    }
    else{
        map<int, ComplexLanguageEvent *>::iterator it_lang; 
        for(it_lang = complex_language_events.begin(); it_lang != complex_language_events.end(); it_lang++){
            vector<regionPathProb> grounded_regions = it_lang->second->getNormalizePaths(LANGUAGE_TOP_N_GROUNDINGS);
            //should we keep them in language? 
            it_lang->second->print(false);

            for(int l=0; l < grounded_regions.size(); l++){
                if(grounded_regions[l].first->region != region)
                    continue;
                
                //hard coded for now - replace 
                fprintf(stderr, "\tRegion %d Grounding Threshold %f\n", 
                        grounded_regions[l].first->region->region_id, 
                        grounded_regions[l].second);
            
                LanguageObservation *lang_obs = grounded_regions[l].first->lang_obs;
                if(lang_obs){
                    semantic_factors.push_back(*lang_obs->phi_obs_factor);
                    semantic_factors.push_back(*lang_obs->label_obs_factor);
                    //update the phi value?? 
                    lang_obs->setPhi(grounded_regions[l].second);
                    semantic_factors.push_back(*lang_obs->label_phi_region);                
                }
                else{
                    fprintf(stderr, "No language factor found for complex language event - Error - Should not call runBP\n");
                    exit(-1);
                }
            }
            fprintf(stderr, "Updating belief\n");            
        }
    }
    //now add factors for the objects 
        
    factor_graph = new SemFactorGraph(semantic_factors);
    fprintf(stderr, BLUE "Running Belief propagation\n" RESET_COLOR);

    // Store the constants in a PropertySet object
    dai::PropertySet opts;

    // Set some constants
    size_t maxiter = 10000;
    dai::Real   tol = 1e-9;
    size_t verb = 1;
    
    opts.set("maxiter", maxiter);  // Maximum number of iterations
    opts.set("tol", tol);          // Tolerance for convergence
    opts.set("verbose",verb);     // Verbosity (amount of output generated)
    //opts.set("logdomain",true); 
    
        
    dai::BP bp(*factor_graph, opts("updates",string("SEQRND"))("logdomain",true));
    // Initialize belief propagation algorithm
    bp.init();
    // Run belief propagation algorithm
    bp.run();

    int v = 0;
    SemFactor r_label_fac = bp.belief(*region->region_label);
    r_label_fac.normalize();
    if(v)
        cout << "\nRegion (" << region->region_id << ") : Label Factor => " << r_label_fac << endl;  
    
    region->region_label_dist.clear();
    //cout << bp.belief(*region->region_type) << endl; // display the belief of bp for the variables in that factor
    if(v) 
        fprintf(stderr, BLUE "=== Region : %d (Label) =====\n" RESET_COLOR, region->region_id);
    
    for(int j=0; j < region->region_label->states(); j++){
        //dai::Real denom = P.marginal (*(region->region_type))[j];
        dai::Real denom = r_label_fac[j];
        region->region_label_dist.insert(make_pair(j, denom));
        if(v){
            if(denom < 0.01)
                continue;
            fprintf(stderr, BLUE "\t%s = %.3f\n" RESET_COLOR, label_info->getLabelNameFromID(j).c_str(),denom);
        }
    }
    
    if(v)
        fprintf(stderr, BLUE " ==== Region : %d (Type) =====\n" RESET_COLOR, region->region_id);
    
    // Report factor marginals for fg, calculated by the belief propagation algorithm
    //cout << "Approximate (loopy belief propagation) factor marginals:" << endl;
    //cout << bp.belief(fg.factor(I).vars()) << endl; // display the belief of bp for the variables in that factor
    SemFactor r_fac = bp.belief(*region->region_type);
    r_fac.normalize();
    region->region_type_dist.clear();
    //cout << bp.belief(*region->region_type) << endl; // display the belief of bp for the variables in that factor
    for(int j=0; j < region->region_type->states(); j++){
        //dai::Real denom = P.marginal (*(region->region_type))[j];
        dai::Real denom = r_fac[j];
        region->region_type_dist.insert(make_pair(j, denom));
        if(v){
            if(denom < 0.01)
                continue;
            fprintf(stderr, BLUE "\t%s = %.3f\n" RESET_COLOR, label_info->getTypeNameFromID(j).c_str(),denom);
        }
    }

    int64_t e_utime = bot_timestamp_now();
    
    fprintf(stderr, "Total Time for BP : %f\n", (e_utime - s_utime) / 1.0e6);
}

void SlamGraph::runSlam()
{
    if(!status){
        status = 1; 
        fprintf (stdout, "Performing batch optimiziation\n");
        slam->batch_optimization();
    }
    else
        slam->update();
    
    //update the covariances 
    const Covariances& covariances =  slam->covariances().clone();

    int64_t cov_stime = bot_timestamp_now();

    //technically the first node shouldn't have a covariance - but we have a start node and then the first node
    // for(int i=0; i < slam_node_list.size(); i++){
    //     Covariances::node_lists_t node_lists;
    //     list<Node*> nodes;
    //     SlamNode *node = slam_node_list.at(i);
    //     if(node != NULL){
    //         fprintf (stdout, "++ Recovering cov for node id = %d\n", node->id);
    //         nodes.push_back(node->pose2d_node);
    //         node_lists.push_back(nodes);
    //         nodes.clear();
    //     }

    //     list<MatrixXd> cov_blocks = covariances.marginal(node_lists);
    //     int64_t cov_etime = bot_timestamp_now();
    
    //     int nd_ind = 0;
    //     for (list<MatrixXd>::iterator it = cov_blocks.begin(); it!=cov_blocks.end(); it++, nd_ind++) {
    //         MatrixXd cv = *it;
    //         for(int k=0; k < 9; k++)
    //             node->cov[k] = cv(k);
    //     }
    // }   
    


    Covariances::node_lists_t node_lists;
    list<Node*> nodes;
    //technically the first node shouldn't have a covariance - but we have a start node and then the first node
    for(int i=0; i < slam_node_list.size(); i++){
        SlamNode *node = slam_node_list.at(i);
        if(node != NULL){
            nodes.push_back(node->pose2d_node);
            node_lists.push_back(nodes);
            nodes.clear();
        }
    }

    list<MatrixXd> cov_blocks = covariances.marginal(node_lists);
    int64_t cov_etime = bot_timestamp_now();
    
    int nd_ind = 0;
    for (list<MatrixXd>::iterator it = cov_blocks.begin(); it!=cov_blocks.end(); it++, nd_ind++) {
        SlamNode *node = slam_node_list.at(nd_ind);
        MatrixXd cv = *it;
        if(node != NULL){
            for(int k=0; k < 9; k++){
                node->cov[k] = cv(k);
            }
        }
        else{
            fprintf(stderr, "Error - Covariance - something funcky going on\n");
        }
    }    
}

void SlamGraph::updateBoundingBoxes(){
    for(int i=0; i < slam_node_list.size(); i++){
        SlamNode *node = slam_node_list.at(i);
        node->updateBoundingBox();
    }
}

int SlamGraph::getSlamNodePosition(int64_t id){
    SlamNode *nd = getSlamNodeFromID(id);
    if(nd == NULL){
        return -1;
    }
    
    return nd->position;
}

SlamNode * SlamGraph::getSlamNodeFromPosition(int64_t id){
    if(id >= slam_node_list.size())
        return NULL;
        
    return slam_node_list[id];
}

SlamNode * SlamGraph::getSlamNodeFromID(int64_t id){
    map<int, SlamNode *>::iterator it;
    it = slam_nodes.find(id);
    if(it == slam_nodes.end()){
        return NULL;
    }
    return it->second;
}

int SlamGraph::removeConstraint(SlamConstraint *constraint){
    removeConstraint(constraint->id);
}

RegionNode *SlamGraph::createNewRegion(){
    int id = 0;

    id = last_region_id++;
    
    RegionNode *r_node;
    r_node = new RegionNode(id, use_factor_graphs, label_info);

    last_created_region = r_node;
    region_nodes.insert(pair<int, RegionNode *>(r_node->region_id, r_node));
    region_node_list.push_back(r_node);
    //add these to the map and vector 
    
    return last_created_region;
}

RegionNode *SlamGraph::createNewRegion(int id){
    RegionNode *r_node;
    r_node = new RegionNode(id, use_factor_graphs, label_info);
    
    last_region_id = fmax(id, last_region_id)+1;
    last_created_region = r_node;
    region_nodes.insert(pair<int, RegionNode *>(r_node->region_id, r_node));
    region_node_list.push_back(r_node);
    //add these to the map and vector 
    
    return last_created_region;
}

void SlamGraph::pruneRegionConnections(){
    //fprintf(stderr, "===== Region Connectivity =====\n");
    for(int i=0; i < region_node_list.size(); i++){
        RegionNode *region = region_node_list[i];
        region->pruneRegionEdges(false);
    }
    //fprintf(stderr, "================================\n");
}

vector<RegionNode*> SlamGraph::getConnectedRegions(RegionNode *current_region){
    vector<RegionNode*> connected_regions;
    for(int i=0; i < region_node_list.size(); i++){
        RegionNode *region1 = region_node_list[i];
        if(region1==current_region) 
            continue;
        if(region1->isConnected(current_region)){
            connected_regions.push_back(region1);
        }
    }
    return connected_regions;
}

vector<regionPair> SlamGraph::getConnectedRegionPairs(RegionNode *skip){
    vector<regionPair> region_pairs;
    for(int i=0; i < region_node_list.size(); i++){
        RegionNode *region1 = region_node_list[i];
        if(region1 == skip)
            continue;
        for(int j=0; j < region_node_list.size(); j++){
            if(i==j){
                continue;
            }
            RegionNode *region2 = region_node_list[j];
            if(region2 == skip)
                continue;
            if(region1->isConnected(region2)){
                region_pairs.push_back(make_pair(region1, region2));
            }
        }
    }
    return region_pairs;
}

void SlamGraph::printRegionConnections(bool print_edges){
    fprintf(stderr, " No of Regions : %d - No of Nodes : %d No of Edges : %d\n", (int) region_node_list.size(), (int) slam_nodes.size(), (int) slam_constraints.size());

    fprintf(stderr, "===== Region Connectivity =====\n");
    for(int i=0; i < region_node_list.size(); i++){
        RegionNode *region = region_node_list[i];
        fprintf(stderr, "\t [%d] No of Nodes : %d\n", region->region_id, (int) region->nodes.size());
        if(print_edges){
            region->printRegionEdges();
        }
    }
    fprintf(stderr, "================================\n");
}

void SlamGraph::calculateRegionDistance(){
    fprintf(stderr, "===== Region Efficiency =====\n");
    
    updateRegionDistances();

    int total_count = 0; 
    int total_distance = 0;
    int no_of_failed_edges = 0;
    int max_distance = 0;

    int same_region = 0;
    int different_region = 0;

    map<pair<int, int>, int>::iterator it_1_2;
    for(int i=0; i < slam_nodes.size(); i++){
        for(int j=0; j < slam_nodes.size(); j++){
            if(i == j)
                continue; 
            
            Pose2d delta = slam_nodes[i]->getPose().ominus(slam_nodes[j]->getPose());
            if(hypot(delta.x(), delta.y()) > 1.0)
                continue;

            if(slam_nodes[i]->region_node == slam_nodes[j]->region_node){
                same_region++; 
            }                  
            else{
                different_region++;
            }

            if(slam_nodes[i]->region_node == slam_nodes[j]->region_node){
                //same_region++; 
                total_count++;
            }  
            else{
                it_1_2 = topo_distance.find(make_pair(slam_nodes[i]->region_node->region_id, slam_nodes[j]->region_node->region_id));
                int distance = it_1_2->second;
                
                //fprintf(stderr, "Region : %d - %d => Distance : %d\n", slam_nodes[i]->region_node->region_id, slam_nodes[j]->region_node->region_id, distance); 
                if(distance > 1){
                    total_distance += (distance - 1);
                    if((distance - 1) > max_distance){
                        max_distance = distance - 1;
                    }
                    no_of_failed_edges++;
                }
                total_count++;
            }            
        }
    }    

    double average_distance = 0;
    if(total_count > 0){
        average_distance = total_distance / (double) total_count;
    }

    no_close_node_pairs = total_count;
    no_same_region_close_pairs = same_region;
    no_failed_close_node_pairs = no_of_failed_edges;
    max_dist_of_close_node_pairs = max_distance;
    average_distance_of_close_node_pairs = average_distance;

    fprintf(stderr, "===================Avg Topo Distance====================");
    fprintf(stderr, " No of Regions : %d - No of Nodes : %d No of close pairs %d\n", (int) region_node_list.size(), (int) slam_nodes.size(), total_count);
    fprintf(stderr, "No of Same Regions      : %d\n", same_region);
    fprintf(stderr, "No of Different Regions : %d\n", different_region);
    fprintf(stderr, " No of failed edges : %d - Max distance : %d\n", no_of_failed_edges, max_distance);
    fprintf(stderr, "Average topological distance of close node pairs : %.3f\n", average_distance);

    fprintf(stderr, "================================\n");
}

void SlamGraph::calculateRegionEfficiency(){
    fprintf(stderr, "===== Region Efficiency =====\n");
    
    int same_region = 0;
    int different_region = 0;
    
    for(int i=0; i < slam_nodes.size(); i++){
        for(int j=0; j < slam_nodes.size(); j++){
            if(i == j)
                continue; 
            
            Pose2d delta = slam_nodes[i]->getPose().ominus(slam_nodes[j]->getPose());
            if(hypot(delta.x(), delta.y()) > 1.0)
                continue;

            if(slam_nodes[i]->region_node == slam_nodes[j]->region_node){
                same_region++; 
            }                  
            else{
                different_region++;
            }
        }
    }    

    fprintf(stderr, " No of Regions : %d - No of Nodes : %d No of Edges : %d\n", (int) region_node_list.size(), (int) slam_nodes.size(), (int) slam_constraints.size());
        
    fprintf(stderr, "No of Same Regions      : %d\n", same_region);
    
    fprintf(stderr, "No of Different Regions : %d\n", different_region);

    fprintf(stderr, "================================\n");
}

void SlamGraph::updateRegionMeans(){
    //fprintf(stderr, "===== Region Connectivity =====\n");
    for(int i=0; i < region_node_list.size(); i++){
        RegionNode *region = region_node_list[i];
        region->updateMean();
    }
    //fprintf(stderr, "================================\n");
}

void SlamGraph::updateRegionDistances(){
    //ideally this can be done when a new edge is created between two regions 
    //then we wont need to go through each of these guys 
    //go through the contraint list 
    //and add edges for between each region with connected edges 
    
    //reset all the current edges
    //map<pair<int, int>, int> edge_weights;
    topo_distance.clear();
    for(int i=0; i < region_node_list.size(); i++){
        RegionNode *r1 = region_node_list[i];
        for(int j=0; j < region_node_list.size(); j++){
            RegionNode *r2 = region_node_list[j];
            if(r1->isConnected(r2)){
                topo_distance.insert(make_pair(make_pair(r1->region_id, r2->region_id), 1));
            }
            else{
                topo_distance.insert(make_pair(make_pair(r1->region_id, r2->region_id), 1000));
            }
        }
    }

    map<pair<int, int>, int>::iterator it_1_3;
    map<pair<int, int>, int>::iterator it_3_2;
    map<pair<int, int>, int>::iterator it_1_2;
    map<pair<int, int>, int>::iterator it_2_1;
        
    for(int k=0; k < region_node_list.size(); k++){
        RegionNode *r3 = region_node_list[k];
        for(int i=0; i < region_node_list.size(); i++){
            RegionNode *r1 = region_node_list[i];
            for(int j=0; j < region_node_list.size(); j++){
                RegionNode *r2 = region_node_list[j];
                it_1_3 = topo_distance.find(make_pair(r1->region_id, r3->region_id));
                int dist_1_3 = it_1_3->second;
                it_3_2 = topo_distance.find(make_pair(r2->region_id, r3->region_id));
                int dist_3_2 = it_3_2->second;
                it_1_2 = topo_distance.find(make_pair(r1->region_id, r2->region_id));
                int dist_1_2 = it_1_2->second;

                if((dist_1_3 + dist_3_2) < dist_1_2){
                    topo_distance.erase(it_1_2);
                    topo_distance.insert(make_pair(make_pair(r1->region_id, r2->region_id), dist_1_3 + dist_3_2));
                    it_2_1 = topo_distance.find(make_pair(r2->region_id, r1->region_id));
                    topo_distance.erase(it_2_1);
                    topo_distance.insert(make_pair(make_pair(r2->region_id, r1->region_id), dist_1_3 + dist_3_2));
                }
            }
        }
    }
}

void SlamGraph::updateRegionConnections(){
    //ideally this can be done when a new edge is created between two regions 
    //then we wont need to go through each of these guys 
    //go through the contraint list 
    //and add edges for between each region with connected edges 
    
    //reset all the current edges
    pruneRegionConnections();
    
    map<int, SlamConstraint *>::iterator c_it;
    for ( c_it= slam_constraints.begin() ; c_it != slam_constraints.end(); c_it++ ){        
        SlamConstraint *ct = c_it->second;
        RegionNode *region1 = ct->node1->region_node;
        RegionNode *region2 = ct->node2->region_node;

        //we need to look at the edge type also 
        //otherwise it can be a region to region constraint - but not a region connectivity 

        if(region1 == region2)
            continue;

        /*RegionEdge *edge1 = region1->getEdge(region2);
        RegionEdge *edge2 = region2->getEdge(region1);
        
        if(edge1 == NULL && edge2 == NULL){
            fprintf(stderr, "No edge exists - adding edge\n");
            //we need to add both regions - otherwise its harder to remove edges 
            region1->addEdge(region2);
            region2->addEdge(region1);
            continue;
            }*/
        region1->addEdge(region2, ct);
        region2->addEdge(region1, ct);
        //if we are here - this means that an edge existed already - so no worries 
    }    
    
    //printRegionConnections();
}

map<RegionNode*, RegionNode *> SlamGraph::getShortestPathRegionTree(RegionNode *node)
{
    graph_t g; 

    map<int, vertex_descriptor> vertex_map;
    map<int, vertex_descriptor>::iterator it_v;

    vertex_descriptor *v_node = NULL;

    for(int i=0; i < region_node_list.size(); i++){
        //for (int i=0; i < slam_node_list.size(); i++){
        vertex_descriptor v = add_vertex(g);
        RegionNode *region = region_node_list[i];
        g[v].id = region->region_id;
        if(region->region_id == node->region_id){
            v_node = &v;
        }
        vertex_map.insert(make_pair(g[v].id, v));
    }

    map<int, SlamConstraint *>::iterator it;
    set<pair<int, int> > valid_edges;
    set<pair<int, int> >::iterator it_edge_set;
    map<int, int>::iterator it_i;
    map<int, int>::iterator it_j;

    for(int i=0; i < region_node_list.size(); i++){
         RegionNode *region1 = region_node_list[i];
         vector<RegionNode *> connected_regions = region1->getConnectedRegions();
         for(int j=0; j < connected_regions.size(); j++){
             RegionNode *region2 = connected_regions[j];
             
             it_edge_set = valid_edges.find(make_pair(region1->region_id, region2->region_id));
             if(it_edge_set != valid_edges.end())
                 continue;
             it_edge_set = valid_edges.find(make_pair(region2->region_id, region1->region_id));
             if(it_edge_set != valid_edges.end())
                 continue;
             valid_edges.insert(make_pair(region1->region_id, region2->region_id));
         }
    }
    
    for(it_edge_set = valid_edges.begin(); it_edge_set != valid_edges.end(); it_edge_set++){
        nodePairKey edge = *it_edge_set; 
        it_v = vertex_map.find(edge.first);
        if(it_v == vertex_map.end()){
            //fprintf(stderr, "Error - No vertex descriptor found for node : %d\n", it_i->first);
            continue;
        }

        vertex_descriptor v_i = it_v->second;
        it_v = vertex_map.find(edge.second);
        if(it_v == vertex_map.end()){
            //fprintf(stderr, "Error - No vertex descriptor found for node : %d\n", it_i->second);
            continue;
        }
        vertex_descriptor v_j = it_v->second;
        add_edge(v_i, v_j, Weight(1.0), g);
    }

    property_map<graph_t, edge_weight_t>::type weightmap = get(edge_weight, g);
    std::vector<vertex_descriptor> p(num_vertices(g));
    std::vector<double> d(num_vertices(g));
    
    //vertex_descriptor b_vertex;
    
    dijkstra_shortest_paths(g, *v_node,
                            predecessor_map(boost::make_iterator_property_map(p.begin(), 
                                                                              get(boost::vertex_index, g))).
                            distance_map(boost::make_iterator_property_map(d.begin(), get(boost::vertex_index, g))));
        
    int64_t e_utime = bot_timestamp_now();
    //fprintf(stderr, CYAN "Time to find Shortest Path : %f\n" RESET_COLOR, (e_utime - s_utime) / 1.0e6);
    //fprintf(stderr, CYAN "Distances from %d and parents\n" RESET_COLOR, node->id);
    graph_traits < graph_t >::vertex_iterator vi, vend;
    //from this we should build the minimum spanning tree for the graph - from the last node 

    map<RegionNode*, RegionNode*> parent_map; 
    set<RegionNode *> node_set;
    map<RegionNode*, set<RegionNode *> > neighbour_map; 

    map<RegionNode*, set<RegionNode *> >::iterator it_neighbour_map;
    
    //this is not building a minimum spanning tree right now 
    //this just tells which edges are there (which is effected by the way its included) 
    
    for (boost::tie(vi, vend) = vertices(g); vi != vend; ++vi) {        
        RegionNode *nd_i = getRegionFromID(g[*vi].id);
        RegionNode *nd_j = getRegionFromID(g[p[*vi]].id);

        parent_map.insert(make_pair(nd_i, nd_j));
    }
    return parent_map;    
}

map<RegionNode*, int> SlamGraph::getRegionDistance(RegionNode *region, vector<RegionNode *> c_region_list, int max_dist){
    map<RegionNode*, int> result;
    int64_t utime_s = bot_timestamp_now();
    
    map<RegionNode*, RegionNode *> region_result = getShortestPathRegionTree(region);
    
    int64_t utime_e = bot_timestamp_now();
    
    fprintf(stderr, YELLOW "Time to build graph : %f\n" RESET_COLOR, (utime_e - utime_s) /1.0e6);
    
    map<RegionNode*, RegionNode *>::iterator it;

    fprintf(stderr, "Current region : %d\n", (int) region->region_id);

    //for(it = region_result.begin(); it != region_result.end(); it++){
        //fprintf(stderr, "Region : %d -> %d\n", it->first->region_id, it->second->region_id);
    //}    
    
    for(int i=0; i < c_region_list.size(); i++){
        int count = 0;
        RegionNode *region2 = c_region_list[i];
        RegionNode *last_region = region2;

        fprintf(stderr, "Looking for path to region : %d\n", (int) region2->region_id);

        it = region_result.find(last_region);
        while(it != region_result.end()){
            count++;
            if(last_region == it->second){
                fprintf(stderr, "Next is same as current - setting high\n");
                count += max_dist;
                break;
            }
            last_region = it->second;
            //fprintf(stderr, "\tR : %d\n", last_region->region_id);
            if(last_region == region){
                break;
            }
            it = region_result.find(last_region);
            if(count > max_dist){
                break; 
            }
        }        
        fprintf(stderr, "Distance from region %d - Region : %d = %d\n", region->region_id, region2->region_id, count);
        result.insert(make_pair(region2,count));
    }

    return result;
}

void SlamGraph::removeComplexLanguageNodePaths(RegionNode *region){
    map<int, ComplexLanguageEvent *>::iterator it_lang; 
    for(it_lang = complex_language_events.begin(); it_lang != complex_language_events.end(); it_lang++){
        ComplexLanguageEvent *current_event = it_lang->second;
        current_event->removeNodePath(region);
        current_event->removeLandmark(region);
    }
}

void SlamGraph::removeRegion(RegionNode *r_node){
    //fprintf(stderr, "==== Removing Region %d ====\n", r_node->region_id);
    region_nodes.erase(r_node->region_id);
    
    /*fprintf(stderr, CYAN "Regions before removal : %d\n" RESET_COLOR, region_node_list.size());
    for(size_t i=0; i < region_node_list.size(); i++){
        fprintf(stderr, CYAN "Region : %d - %d\n", region_node_list[i]->region_id, (int) region_node_list[i]->nodes.size());
        }*/

    for(size_t i=0; i < region_node_list.size(); i++){
        if(region_node_list[i]->region_id == r_node->region_id){
            //fprintf(stderr, "Found Region - removing from list\n");
            region_node_list.erase(region_node_list.begin()+i);
            break;
        }
    }

    /*fprintf(stderr, CYAN "Regions after removal : %d\n" RESET_COLOR, region_node_list.size());
    for(size_t i=0; i < region_node_list.size(); i++){
        fprintf(stderr, CYAN "Region : %d\n", region_node_list[i]->region_id);
        }*/

    map<int, RegionEdge *>::iterator it;
    for(it = r_node->edges.begin(); it!= r_node->edges.end(); it++){
        //remove the edges from both regions
        RegionNode *region_2 = it->second->node2;//getRegionFromID(it->second->node2->region_id);//it->second->node2;     

        if(region_2 != NULL){
            //fprintf(stderr, CYAN "Removing region edge with Region : %d\n" RESET_COLOR, region_2->region_id);
            region_2->deleteEdge(r_node);
        }
        delete it->second;        
    }
    
    removeComplexLanguageNodePaths(r_node);

    //remove the edges here     
    delete r_node;
}

int SlamGraph::addNodeToRegion(SlamNode *node, int id){
    RegionNode *region = getRegionFromID(id);
    if(region == NULL){
        fprintf(stderr, "Error : Specified region not found\n");
        return -1;
    }
    return region->addNode(node);
}

int SlamGraph::addNodeToRegion(SlamNode *node, RegionNode *region){
    if(region == NULL){
        fprintf(stderr, "Error : Specified region not found\n");
        return -1;
    }
    return region->addNode(node);
}

RegionNode* SlamGraph::mergeRegions(RegionNode *region1, RegionNode *region2){
    //printRegionConnections();
    //merging will remove region2 
    RegionNode *surviving_region = NULL;
    RegionNode *removed_region = NULL;
    if(region1->region_id < region2->region_id){
        surviving_region = region1;
        removed_region = region2;
    }
    else{
        surviving_region = region2;
        removed_region = region1;
    }
    
    removed_region->addCurrentNodesToOtherRegion(surviving_region);
    
    //removed_region->printRegionEdges();
    //we could transfer the regions out also 
    fprintf(stderr, "Merge regions %d - %d\n", region1->region_id, region2->region_id);
    removeRegion(removed_region);
    //printRegionConnections();
    updateRegionConnections();
    return surviving_region;
}

RegionNode * SlamGraph::getRegionFromID(int region_id){    
    map<int, RegionNode *>::iterator it;
    it = region_nodes.find(region_id);
    if(it == region_nodes.end()){
        return NULL;
    }
    return it->second;
}

RegionSegment *SlamGraph::createNewSegment(){
    RegionSegment *segment = new RegionSegment(last_segment_id++);
    region_segment_list.push_back(segment);
    region_segment_map.insert(make_pair(segment->id, segment));
    last_segment = segment;
    return segment;
}

RegionSegment *SlamGraph::createNewSegment(vector<SlamNode*> nodes){
    RegionSegment *segment = new RegionSegment(last_segment_id++);
    segment->addNodes(nodes);
    region_segment_list.push_back(segment);
    region_segment_map.insert(make_pair(segment->id, segment));
    last_segment = segment;
    return segment;
}


SlamNode *SlamGraph::getClosestDistanceToRegion(Pose2d pose, RegionNode *current_region, double *distance){
    //search through the nodes in the graph and find the closest node from the given location to the 
    //region - make sure that the position is not from a node in the region - as otherwise it will be 0
    //also return the closest node 
    //negative distance indicates failure (also the closest node will be null

    double min_dist = 10000000;

    SlamNode *res_node = NULL;

    for(int i=0; i < region_node_list.size(); i++){
        RegionNode *region = region_node_list[i];
        if(region == current_region){
            //the current region - we skip this 
            continue;
        }
        
        for(int j=0; j < region->nodes.size(); j++){
            SlamNode *c_node = region->nodes[j];
            Pose2d delta = c_node->getPose().ominus(pose);
            double dist = hypot(delta.x(), delta.y());

            if(dist < min_dist){
                min_dist = dist;
                //closest_node = c_node;
                res_node = c_node;
            }
        }
    }
    *distance = min_dist;
    return res_node;
}

void SlamGraph::getDistanceToNodesInRegion(Pose2d pose, RegionNode *region, 
                                           std::vector<nodeDist> *region_dist_list){
    //search through the nodes in the graph and find the closest node from the given location to the 
    //region - make sure that the position is not from a node in the region - as otherwise it will be 0
    //also return the closest node 
    //negative distance indicates failure (also the closest node will be null

    for(int j=0; j < region->nodes.size(); j++){
        SlamNode *c_node = region->nodes[j];
        Pose2d delta = c_node->getPose().ominus(pose);
        double dist = hypot(delta.x(), delta.y());
        
        region_dist_list->push_back(make_pair(c_node, dist));
    }
}

void SlamGraph::getDistanceToRegions(Pose2d pose, RegionNode *current_region, 
                                          std::vector<nodeDist> *region_dist_list){
    //search through the nodes in the graph and find the closest node from the given location to the 
    //region - make sure that the position is not from a node in the region - as otherwise it will be 0
    //also return the closest node 
    //negative distance indicates failure (also the closest node will be null

    for(int i=0; i < region_node_list.size(); i++){
        RegionNode *region = region_node_list[i];
        if(region == current_region){
            //the current region - we skip this 
            continue;
        }
        
        double min_dist = 100000000;
        SlamNode *res_node = region->getClosestNode(pose, &min_dist);
        if(min_dist < 1000 && res_node != NULL){
            region_dist_list->push_back(make_pair(res_node, min_dist));
        }
    }
}

double SlamGraph::getClosestDistanceToRegion(SlamNode *node_to_check, RegionNode *region, 
                                               SlamNode *closestNode){            
    double min_dist = 100000000;
    closestNode = region->getClosestNode(node_to_check->getPose(), &min_dist);
    return min_dist;
}

double SlamGraph::findClosestRegion(RegionNode *querry_region, RegionNode *closest_region, SlamNode *node1, SlamNode *node2){
    //find the cloest region to the given region 
    //also return the closest region and the nodes that were closest 
    double min_dist = 10000000; 
    node1 = NULL;
    node2 = NULL;
    closest_region = NULL;
    for(int i=0; i < region_node_list.size(); i++){
        RegionNode *region = region_node_list[i];
        //skip if its the querry region 
        if(region == querry_region)
            continue;
        SlamNode *nd1, *nd2;
        double dist = querry_region->getClosestDistanceToRegion(region, nd1, nd2);
        if(dist < min_dist){
            min_dist = dist;
            node1 = nd1;
            node2 = nd2;
            closest_region = region;
        }
    }
    return min_dist;
}

double SlamGraph::findDistanceToRegionMeans(RegionNode *querry_region, std::vector<nodePairDist> *region_dist_list, double add_threshold){
    int64_t s_utime = bot_timestamp_now();
    //find the cloest region to the given region 
    //also return the closest region and the nodes that were closest 
    
    //querry_region->updateMean();
    
    double min_dist = add_threshold; //10000000; 

    double mean_utime = 0;

    for(int i=0; i < region_node_list.size(); i++){
        RegionNode *region = region_node_list[i];
        //skip if its the querry region 
        if(region == querry_region)
            continue;
        int nd1_id, nd2_id;
        
        //region->updateMean();

        vector<pair <SlamNode *, SlamNode *> > close_node_pairs; 
        //this is very expensive - and stupid - we should find the actual closest nodes only once we figured out the 
        //closest pairs 
        int64_t utime_s = bot_timestamp_now();
        double dist = querry_region->getMeanDistanceToRegion(region, close_node_pairs, &nd1_id, &nd2_id);               
        int64_t utime_e = bot_timestamp_now();

        mean_utime += (utime_e - utime_s) / 1.0e6;

        
        /*SlamNode *q_mean_node = querry_region->mean_node;
        SlamNode *r_mean_node = region->mean_node;
              
        Pose2d delta = q_mean_node->getPose().ominus(r_mean_node->getPose());

        double dist = hypot(delta.x(), delta.y()); */

        if(dist > add_threshold)
            continue;
        //double mean_dist = hypot(querry_region->mean.x() - region->mean.x(), querry_region->mean.y() - region->mean.y());

        SlamNode *nd1 = getSlamNodeFromID(nd1_id);
        SlamNode *nd2 = getSlamNodeFromID(nd2_id);

        region_dist_list->push_back(make_pair(make_pair(nd1,nd2), dist));
        
        if(dist < min_dist){
            min_dist = dist;
        }
    }
    
    int64_t e_utime = bot_timestamp_now();
    fprintf(stderr, RED "\n\n\nTime for Distance : %f - Dist Cal : %f\n\n\n" RESET_COLOR, (e_utime - s_utime) / 1.0e6, mean_utime);
    return min_dist;
}

double SlamGraph::findMeanDistanceToRegions(RegionNode *querry_region, std::vector<nodePairDist> *region_dist_list){
    int64_t s_utime = bot_timestamp_now();
    //find the cloest region to the given region 
    //also return the closest region and the nodes that were closest 
    double min_dist = 10000000; 
    for(int i=0; i < region_node_list.size(); i++){
        RegionNode *region = region_node_list[i];
        //skip if its the querry region 
        if(region == querry_region)
            continue;
        int nd1_id, nd2_id;
        
        vector<pair <SlamNode *, SlamNode *> > close_node_pairs; 
        double dist = querry_region->getMeanDistanceToRegion(region, close_node_pairs, &nd1_id, &nd2_id);
        
        //if(0 && dist < 15){
        if(0 && dist < 15){
            for(int i=0; i < close_node_pairs.size(); i++){
                //this could be cached once??
                MatrixXd cov = getCovariances(close_node_pairs[i].first, close_node_pairs[i].second);
                //cout << cov << endl;
                //sometimes getting a segfault - need to figure out why - but doesnt seem to take a long time 
                fprintf(stderr, "Covariance between %d - %d\n", close_node_pairs[i].first->id, close_node_pairs[i].second->id);
            }
        }

        SlamNode *nd1 = getSlamNodeFromID(nd1_id);
        SlamNode *nd2 = getSlamNodeFromID(nd2_id);
        //fprintf(stderr, "Node %d (%d) - %d (%d) - %f\n", nd1->id, nd2->id, nd1->region_node->region_id, nd2->region_node->region_id, dist);
        region_dist_list->push_back(make_pair(make_pair(nd1,nd2), dist));
        
        if(dist < min_dist){
            min_dist = dist;
        }
    }
    int64_t e_utime = bot_timestamp_now();
    //fprintf(stderr, RED "\n\n\nTime for cov : %f\n\n\n" RESET_COLOR, (e_utime - s_utime) / 1.0e6);
    return min_dist;
}

/*double SlamGraph::findDistanceToRegions(vector<SlamNode *> segment_nodes, std::vector<nodePairDist> &region_dist_list, double threshold){
    for(int i=0; i < region_node_list.size(); i++){
        RegionNode *region = region_node_list[i];
        if(region == q_segment
        double min_dist = threshold; 
        SlamNode *m_nd1 = NULL;
        SlamNode *m_nd2 = NULL;
        for(int j=0; j < q_segment->nodes.size(); j++){
            double dist = 0;
            SlamNode *nd1 = q_segment->nodes[j];
            SlamNode *nd2 = c_segment->getClosestNode(nd1, &dist);
            if(dist < min_dist){
                m_nd1 = nd1;
                m_nd2 = nd2;
                min_dist = dist;
            }            
        }
        if(min_dist < threshold){
            segment_dist_list.push_back(make_pair(make_pair(m_nd1,m_nd2), min_dist));
        }
        
    }    
    }*/

double SlamGraph::findDistanceToSegments(RegionSegment *q_segment, std::vector<nodePairDist> &segment_dist_list, double threshold){
    for(int i=0; i < region_segment_list.size(); i++){
        RegionSegment *c_segment = region_segment_list[i];
        if(c_segment == q_segment)
            continue;
        double min_dist = threshold; 
        SlamNode *m_nd1 = NULL;
        SlamNode *m_nd2 = NULL;
        for(int j=0; j < q_segment->nodes.size(); j++){
            double dist = 0;
            SlamNode *nd1 = q_segment->nodes[j];
            SlamNode *nd2 = c_segment->getClosestNode(nd1, &dist);
            if(dist < min_dist){
                m_nd1 = nd1;
                m_nd2 = nd2;
                min_dist = dist;
            }            
        }
        if(min_dist < threshold){
            segment_dist_list.push_back(make_pair(make_pair(m_nd1,m_nd2), min_dist));
        }
        
    }    
}

double SlamGraph::findDistanceToRegions(SlamNode *querry_node, std::vector<nodePairDist> *region_dist_list){
    double min_dist = 10000000; 
    for(int i=0; i < region_node_list.size(); i++){
        RegionNode *region = region_node_list[i];
        //skip if its the querry region 
        if(region == querry_node->region_node)
            continue;
        int c_nd_id;
        
        double dist;// = region->getMeanDistanceFromNode(querry_node, &c_nd_id);
        Pose2d current_pose = querry_node->getPose();
        SlamNode *nd2 = region->getClosestNode(current_pose, &dist);//getSlamNodeFromID(c_nd_id);

        region_dist_list->push_back(make_pair(make_pair(querry_node,nd2), dist));
        
        if(dist < min_dist){
            min_dist = dist;
        }
    }
    return min_dist;
}

double SlamGraph::findMeanDistanceToRegions(SlamNode *querry_node, std::vector<nodePairDist> *region_dist_list){
    //find the cloest region to the given region 
    //also return the closest region and the nodes that were closest 
    double min_dist = 10000000; 
    for(int i=0; i < region_node_list.size(); i++){
        RegionNode *region = region_node_list[i];
        //skip if its the querry region 
        if(region == querry_node->region_node)
            continue;
        int c_nd_id;
        double dist = region->getMeanDistanceFromNode(querry_node, &c_nd_id);

        SlamNode *nd2 = getSlamNodeFromID(c_nd_id);

        region_dist_list->push_back(make_pair(make_pair(querry_node,nd2), dist));
        
        if(dist < min_dist){
            min_dist = dist;
        }
    }
    return min_dist;
}

double SlamGraph::findDistanceToRegions(RegionNode *querry_region, std::vector<nodePairDist> *region_dist_list){
    //find the cloest region to the given region 
    //also return the closest region and the nodes that were closest 
    double min_dist = 10000000; 
    for(int i=0; i < region_node_list.size(); i++){
        RegionNode *region = region_node_list[i];
        //skip if its the querry region 
        if(region == querry_region)
            continue;
        int nd1_id, nd2_id;
        double dist = querry_region->getClosestDistanceToRegion(region, &nd1_id, &nd2_id);
        

        /*SlamNode *nd1, *nd2;
        double dist = querry_region->getClosestDistanceToRegion(region, nd1, nd2);
        
        fprintf(stderr, "Dist : %f\n", dist);
        fprintf(stderr, "Node %d (%d) - %d (%d) - %f\n", nd1->id, nd2->id, nd1->region_node->region_id, nd2->region_node->region_id, 
        dist);*/
        SlamNode *nd1 = getSlamNodeFromID(nd1_id);
        SlamNode *nd2 = getSlamNodeFromID(nd2_id);
        //fprintf(stderr, "Node %d (%d) - %d (%d) - %f\n", nd1->id, nd2->id, nd1->region_node->region_id, nd2->region_node->region_id, dist);
        region_dist_list->push_back(make_pair(make_pair(nd1,nd2), dist));
        
        if(dist < min_dist){
            min_dist = dist;
        }
    }
    return min_dist;
}



int SlamGraph::removeSlamNode(int node_id)
{
    //slam->add_node(node->pose2d_node);
    SlamNode *node = getSlamNodeFromID(node_id);
    if(node == NULL)
        return -1;
    
    slam_nodes.erase(node_id);
    slam->remove_node(node->pose2d_node);

    for(int i = 0; i< slam_node_list.size(); i++){
        if(slam_node_list[i]->id == node_id){
            slam_node_list.erase(slam_node_list.begin()+ i);

            //update the rest of the nodes with the new position
            if(i < slam_node_list.size()){ //if the removed node wasn't the last node
                for(int j=i; j < slam_node_list.size(); j++){
                    slam_node_list[i]->position = j;
                }
            }

            break;
        }
    }

    //remove the node from the node map 
    //what do we do with the constraints ?? - maybe whatever calls this should handle that
    SlamNode *p_node = node->prev_node;
    SlamNode *n_node = node->next_node;

    int reassign = 0;
    SlamConstraint *const_p_to_c  = node->inc_constraint;
    SlamConstraint *const_c_to_n = NULL;

    Pose2d orig_transform = const_p_to_c->transform;
    MatrixXd orig_noise = const_p_to_c->noise.sqrtinf().inverse();

    Pose2d new_transform;
    Pose2d new_transform_inv;
    MatrixXd new_noise;

    //cout << orig_noise << endl;
    //Matrix3d orig_noise_mat; 
    /*orig_noise_mat<< orig_noise[0] , orig_noise[1], orig_noise[2], 
        orig_noise[3] , orig_noise[4], orig_noise[5], 
        orig_noise[6] , orig_noise[7], orig_noise[8];*/
        

    if(n_node != NULL){
        const_c_to_n = n_node->inc_constraint;
        new_transform = const_c_to_n->transform;
        new_noise = const_c_to_n->noise.sqrtinf().inverse();
        Pose2d center(0,0,0);
        new_transform_inv = center.ominus(new_transform);
        reassign = 1;        
    }
    else{
        fprintf(stderr, "No next node found - ignoring incremental constraint\n");
    }

    //need to fix both constraints coming in to the slam node 
    //and constraints going out from the slam node 
    map<int, SlamConstraint *>::iterator it; 
    for (it= node->constraints_from.begin(); it!= node->constraints_from.end(); ++it){
        SlamConstraint *constraint = it->second;
        if(reassign){
            //transform the constraints to the new node and then add them to that node 
            //also remove and add a new inc constraint for the new node - coming from the previous node
            Pose2d tf = constraint->transform;
            //Noise new_noise = constraint->noise;
            MatrixXd ns = constraint->noise.sqrtinf().inverse();
            //Pose2d new_transform = orig_transform.oplus(new_to_orig);
            Pose2d updated_tf = tf.oplus(new_transform);
            
            Matrix3d J;

            double c = cos(tf.t());
            double s = sin(tf.t());
            J << c , -s , 0, 
                c , s, 0, 
                0 , 0, 1; 

            MatrixXd updated_noise_trans = ns + J * new_noise * J.transpose();
            //Noise *new_noise_n = new Covariance(new_noise_trans);
            Covariance updated_noise_n(updated_noise_trans);
            addConstraint(n_node, constraint->node2, n_node, constraint->node2, updated_tf,
                          updated_noise_n, constraint->hitpct, constraint->type, constraint->status);
            //MatrixXd new_noise_trans = orig_noise; //_mat; 
        }
        removeConstraint(constraint);
    }

    for (it= node->constraints.begin(); it!= node->constraints.end(); ++it){
        SlamConstraint *constraint = it->second;
        if(reassign){
            //transform the constraints to the new node and then add them to that node 
            //also remove and add a new inc constraint for the new node - coming from the previous node
            
            //this needs to be inverted 

            Pose2d tf = constraint->transform;
            //Noise new_noise = constraint->noise;
            MatrixXd ns = constraint->noise.sqrtinf().inverse();
            Pose2d updated_tf = new_transform_inv.oplus(tf);
            
            Matrix3d J;

            double c = cos(new_transform_inv.t());
            double s = sin(new_transform_inv.t());
            J << c , -s , 0, 
                c , s, 0, 
                0 , 0, 1; 

            MatrixXd updated_noise_trans = new_noise + J * ns * J.transpose();
            //Noise *new_noise_n = new Covariance(new_noise_trans);
            Covariance updated_noise_n(updated_noise_trans);
            addConstraint(constraint->node1, n_node, constraint->node1, n_node, updated_tf, 
                          updated_noise_n, constraint->hitpct, constraint->type, constraint->status);
        }
        removeConstraint(constraint);
    }
    fprintf(stderr, "Removing Node and constraint - someone should be adding a new one between the previous and next\n");
    delete node;

    return 0;
    //the constraints should be added to the node - then its cleaner 
}

int SlamGraph::removeSlamNodeBasic(int node_id)
{
    //slam->add_node(node->pose2d_node);
    SlamNode *node = getSlamNodeFromID(node_id);
    if(node == NULL)
        return -1;
    
    slam_nodes.erase(node_id);
    slam->remove_node(node->pose2d_node);
    for(int i = 0; i< slam_node_list.size(); i++){
        if(slam_node_list[i]->id == node_id){
            slam_node_list.erase(slam_node_list.begin()+i);
            
            //update the rest of the nodes with the new position
            if(i < slam_node_list.size()){ //if the removed node wasn't the last node
                for(int j=i; j < slam_node_list.size(); j++){
                    slam_node_list[i]->position = j;
                }
            }

            break;
        }
       
    }
    //remove the node from the node map 
    //what do we do with the constraints ?? - maybe whatever calls this should handle that
    //this will mess up the other guy's inc constraint

    map<int, SlamConstraint *>::iterator it; 
    for (it= node->constraints.begin(); it!= node->constraints.end(); ++it){
        SlamConstraint *constraint = it->second;
        removeConstraint(constraint);
    }
    fprintf(stderr, "Removing Node and constraint - someone should be adding a new one between the previous and next\n");
    delete node;

    return 0;
    //the constraints should be added to the node - then its cleaner 
}

SlamNode * SlamGraph::addSlamNode(NodeScan *pose)
{
    //slam_nodes.push_back(node);
    //slam_nodes.insert(node);
    SlamNode *node = new SlamNode(pose, label_info, b_graph);

    //update the nodes so that they point to the previous and next nodes 
    node->prev_node = last_added_node;
    if(last_added_node != NULL){
        last_added_node->next_node = node;
    }
    last_added_node = node;

    slam_nodes.insert ( pair<int, SlamNode *>(node->id,node));
    slam_node_list.push_back(node);
    node->position = slam_node_list.size()-1;
    //also add node to slam //and maybe constraints 
    slam->add_node(node->pose2d_node);
    return node;
    //the constraints should be added to the node - then its cleaner 
}



SlamNode * SlamGraph::addSlamNode(SlamNode *_node){
    SlamNode *node = new SlamNode(_node, b_graph);//, label_info);
    node->prev_node = last_added_node;
    if(last_added_node != NULL){
        last_added_node->next_node = node;
    }
    last_added_node = node;
    slam_nodes.insert ( pair<int, SlamNode *>(node->id,node));
    slam_node_list.push_back(node);
    node->position = slam_node_list.size()-1;
    //also add node to slam //and maybe constraints 
    slam->add_node(node->pose2d_node);
    return node;
    //the constraints should be added to the node - then its cleaner 
 }
 
 MatrixXd SlamGraph::getCovariance(SlamNode *node){
    MatrixXd cov(3,3);
    for(int i=0; i < 9; i++){
        cov(i) = node->cov[i];
    }
    return cov; 
}

//this is expensive - dont call too often 
map<int,MatrixXd> SlamGraph::getCovariances(SlamNode *nd1, vector<SlamNode *> nd_list){
    const Covariances& covariances =  slam->covariances();

    Covariances::node_lists_t cv_node_lists;
    
    //fprintf(stderr, "List Size : %d\n", nd_list.size());

    list<Node*> nd_list_req;
    for(int i=0; i < nd_list.size(); i++){
        //Covariances::node_lists_t nd_cov_list;
        nd_list_req.push_back(nd1->pose2d_node);
        SlamNode *nd2 = nd_list.at(i);
        nd_list_req.push_back(nd2->pose2d_node);
        cv_node_lists.push_back(nd_list_req);
        nd_list_req.clear();
    }

    list<MatrixXd> cov_blocks = covariances.marginal(cv_node_lists);

    map<int,MatrixXd> cov_res;
    int nd_ind = 0;

    //fprintf(stderr, " Cov Size : %d - List Size : %d\n",   cov_blocks.size(), nd_list.size());
    for (list<MatrixXd>::iterator it = cov_blocks.begin(); it!=cov_blocks.end(); it++, nd_ind++) {
        SlamNode *node = nd_list.at(nd_ind);
        MatrixXd cv = *it;
        cov_res.insert(pair<int, MatrixXd>(node->id, cv));
    }
    //int64_t p_start = bot_timestamp_now();
    return cov_res;
}

//this is expensive - dont call too often 
MatrixXd SlamGraph::getCovariances(SlamNode *node1, SlamNode *node2){
    const Covariances& covariances =  slam->covariances();
    list<Node*> nd_list;
    //Covariances::node_lists_t nd_cov_list;
    nd_list.push_back(node1->pose2d_node);
    nd_list.push_back(node2->pose2d_node);
    //nd_cov_list.push_back(nd_list);
    
    //int64_t p_start = bot_timestamp_now();
    return covariances.marginal(nd_list);
}

//measurement probability calculation given the slam covariance between two nodes
//the means of the two nodes and the sm constraint

//hmm - this seems wrong - we should prob integrate this somehow 

double SlamGraph::calculateProbability(SlamNode *node1, SlamNode *node2, Matrix3d cov_sm, Pose2d sm_constraint){
    MatrixXd cov = getCovariances(node2, node1);
    Pose2d value1 = node1->getPose();
    Pose2d value2 = node2->getPose();
    double x1 = value2.x(), y1 = value2.y(), t1 = value2.t();
    double x2 = value1.x(), y2 = value1.y(), t2 = value1.t();
    double c1 = cos(t1), s1 = sin(t1);
    
    //Jacobian 
    MatrixXd H(3,6); 
    
    int v = 0;
    
    //jacobian is correct 
    H.setZero();
    H(0,0) = c1; H(0,1) = s1; H(0,2) = 0;
    H(0,3) = -c1; H(0,4) = -s1; H(0,5) = -(x2-x1)*s1 + (y2-y1)*c1;
    H(1,0) = -s1; H(1,1) = c1; H(1,2) = 0;
    H(1,3) = s1; H(1,4) = -c1; H(1,5) = -(x2-x1) * c1 - (y2-y1) *s1;
    H(2,2) = 1;
    H(2,5) = -1;
    
    double mean_x = (x2-x1)*c1 + (y2-y1)*s1;
    double mean_y = -(x2-x1)*s1 + (y2-y1)*c1;
    double mean_t = bot_mod2pi(t2-t1);
    
    if(v){
        fprintf(stderr, "Mean from graph : %f,%f,%f => Mean from scanmatch : %f,%f,%f\n", 
                mean_x, mean_y, mean_t, sm_constraint.x(), 
                sm_constraint.y(),
                sm_constraint.t());

        cout << "H : " << endl << H << endl;
        cout << "Joint Covariance : " << endl << cov << endl;
    }
    Matrix3d result = H * cov * H.transpose() + cov_sm;
    if(v){
        cout << "Result  : " << endl << result << endl;
    }
    
    MatrixXd dx(3,1);
    
    //mean for the new random variable 
    dx(0) = (mean_x - sm_constraint.x());
    dx(1) = (mean_y - sm_constraint.y());
    dx(2) = bot_mod2pi(mean_t - sm_constraint.t());
    if(v)
        cout << "Distance : " << dx<<endl; 
    MatrixXd mh_dist = dx.transpose() * result.inverse() * dx;
    if(v)
        cout << "Mahalanobis Distance : " << mh_dist<<endl;
    double deter = result.determinant();
    if(v)
        cout << "Determinanant : " << deter << endl;

    //applying to gaussian probability 
    double prob = exp(-mh_dist(0)/2)/ (pow((2 * M_PI), 1.5) * pow(deter, 0.5));
    if(v)
        cout << "Probability : " << prob << endl;
    return prob;
}

SlamNode * SlamGraph::getLastSlamNode(){
    return getSlamNodeFromPosition(slam_node_list.size()-1);
}

void SlamGraph::updateSupernodeIDs(){

    //write this algorithm properly 
    int max_id = slam_node_list.size()-1;
    
    SlamNode *last_nd = getSlamNodeFromPosition(max_id);

    if(!last_nd->slam_pose->is_supernode)
        return;

    last_nd->parent_supernode = last_nd->id;
    int prev_supernode = getPreviousSupernodeID(last_nd->id,SUPERNODE_SEARCH_DISTANCE);
    int next_supernode = getNextSupernodeID(last_nd->id,SUPERNODE_SEARCH_DISTANCE);
    
    for(int i = max_id-1; i >getSlamNodePosition(prev_supernode); i--){ 
        SlamNode *nd = getSlamNodeFromPosition(i);

        if(nd->id <= last_nd->id){
            nd->parent_supernode = last_nd->id;
        }
        else{
            nd->parent_supernode = next_supernode;
        }

        /*if(fabs(nd->id - prev_supernode) < fabs(nd->id - last_nd->id)){
            nd->parent_supernode = prev_supernode;
        }
        else{
            nd->parent_supernode = last_nd->id;
            }*/
    }

    /*for(int i = max_id; i >= 0; i--){
        SlamNode *nd = getSlamNode(i);
        fprintf(stderr, "\t[%d] - Parent : %d\n", i, nd->parent_supernode);
        }*/
}

int SlamGraph::getPreviousSupernodeID(int ind, int search_distance){
    int start_super_ind = fmax(0, getSlamNodePosition(ind) - search_distance);
    //search near the region - and find the closest two nodes - that belong to the relavent supernodes 
    int max_id = slam_node_list.size()-1;
    int prev_supernode = -1;
    for(int i = getSlamNodePosition(ind) -1; i >= start_super_ind; i--){ 
        if(i < 0 || i > max_id)
            break;
        SlamNode *nd1 = getSlamNodeFromPosition(i);
        if(nd1 == NULL)
            continue;

        NodeScan *pose_1 = nd1->slam_pose;
        
        //fprintf(stderr, "\t\t%d : %d\n", i, pose_1->is_supernode);
        //if this node is not the current node and is also a supernode - break
        if(pose_1->is_supernode == true){
            prev_supernode = nd1->id;
            break;
        }
    }
    return prev_supernode;
}


int SlamGraph::getNextSupernodeID(int ind, int search_distance){

    //search near the region - and find the closest two nodes - that belong to the relavent supernodes 
    int max_id = slam_node_list.size()-1;
    int end_super_ind = fmin(max_id, getSlamNodePosition(ind) + search_distance);
    int next_supernode = -1;
    for(int i = getSlamNodePosition(ind) +1; i <= end_super_ind; i++){ 
        if(i < 0 || i > max_id)
            break;
        SlamNode *nd1 = getSlamNodeFromPosition(i);
        if(nd1 == NULL)
            continue;
        NodeScan *pose_1 = nd1->slam_pose;
        //if this node is not the current node and is also a supernode - break
        if(pose_1->is_supernode == true){
            next_supernode = nd1->id;
            break;
        }
    }
    return next_supernode;
}

int SlamGraph::belongsToCurrentSupernode(int ind, int curr_sn, int prev_sn, int next_sn){
    //nodes behind belong to the supernode 
    if(0){
        if(prev_sn < 0)
            if(ind <= curr_sn){
                return 1;
            }
        
        if(ind > prev_sn && ind <= curr_sn){
            return 1;
        }
        return 0;
    }
    if(0){
        if(next_sn < 0)
            if(ind >= curr_sn){
                return 1;
            }
        
        if(ind < next_sn && ind >= curr_sn){
            return 1;
        }
        return 0;
    }    
    else{
        int dist_to_prev = 100;
        int dist_to_next = 100;
        int dist_to_current = fabs(ind - curr_sn);
        if(prev_sn >=0){
            dist_to_prev = fabs(ind - prev_sn);
        }
        if(next_sn >=0){
            dist_to_next = fabs(ind - next_sn);
        }
        if(dist_to_current <= dist_to_prev && dist_to_current <= dist_to_next){
            return 1;
        }
        return 0;
    }
}


void SlamGraph::addOriginConstraint(SlamNode *node1, Pose2d transform, Noise noise){
    Pose2d_Node *current_node = node1->pose2d_node;
    origin_constraint = new Pose2d_Pose2d_Factor(origin_node, current_node, 
                                                 transform, 
                                                 noise);
    slam->add_factor(origin_constraint);
}

SlamConstraint* SlamGraph::addConstraint(SlamNode *node1,  SlamNode *node2, SlamNode *actualnode1, 
                                         SlamNode *actualnode2, Pose2d transform, Noise noise, 
                                         double hit_pct, int32_t type, int32_t status){
    int id = no_edges_added;
    SlamConstraint *constraint = addConstraint(id, node1,  node2, actualnode1, actualnode2, transform, noise, hit_pct, type, status);
    return constraint;
}

//find shortest path from the given node to other nodes 
void SlamGraph::findShortestPathRebuild(SlamNode *node)
{
    //this creates a graph 
    //also adds some additional edges 
    //we assume that any nodes within n meters to be traversable 
    
    if(complex_language_events.size() == 0){
        fprintf(stderr, GREEN "No Complex Language events - Skipping Finding Shortest Path \n\n" RESET_COLOR);
        return; 
    }

    int64_t s_utime = bot_timestamp_now();
    graph_t g; 

    map<int, vertex_descriptor> vertex_map;
    map<int, vertex_descriptor>::iterator it_v;
    for (int i=0; i < slam_node_list.size(); i++){
        vertex_descriptor v = add_vertex(g);
        g[v].id = slam_node_list[i]->id;        
        vertex_map.insert(make_pair(g[v].id, v));
    }

    map<int, SlamConstraint *>::iterator it;
    map<int, int> valid_edges;
    map<int, int>::iterator it_i;
    map<int, int>::iterator it_j;

    for (int i=0; i < slam_node_list.size(); i++){
        SlamNode *nd_i = slam_node_list[i];
        //fprintf(stderr, "Node : %d\n", nd_i->id);
        for (int j=0; j < slam_node_list.size(); j++){
            SlamNode *nd_j = slam_node_list[j];
            //fprintf(stderr, "\tNode : %d\n", nd_j->id);
            if(nd_i == nd_j){
                //fprintf(stderr, "\t\tSame node - continuing\n");
                continue;
            }
            //skip if we already have an edge
            it_i = valid_edges.find(nd_i->id);
            it_j = valid_edges.find(nd_j->id);

            if((it_i != valid_edges.end() && it_i->second == nd_j->id) || (it_j != valid_edges.end() && it_j->second == nd_i->id)){
                continue;
            }

            it = nd_i->constraints.find(nd_j->id);
            Pose2d delta = nd_i->getPose().ominus(nd_j->getPose());
            if(it != nd_i->constraints.end()){
                //fprintf(stderr, "\t\tConstraint Found between : %d -> %d\n", nd_i->id, nd_j->id);
            }
            else{
                //fprintf(stderr, "\t\tNo constraint Found between : %d -> %d\n", nd_i->id, nd_j->id);
            }
            if(hypot(delta.x(), delta.y()) < 1.0){
                //fprintf(stderr, "\t\tConstraint adding because nodes are close\n");
            }

            if(it != nd_i->constraints.end() || hypot(delta.x(), delta.y()) < 1.0){
                valid_edges.insert(make_pair(nd_i->id, nd_j->id));
            }
        }
    }
        
    for(it_i = valid_edges.begin(); it_i != valid_edges.end(); it_i++){
        it_v = vertex_map.find(it_i->first);
        if(it_v == vertex_map.end()){
            //fprintf(stderr, "Error - No vertex descriptor found for node : %d\n", it_i->first);
            continue;
        }

        vertex_descriptor v_i = it_v->second;
        it_v = vertex_map.find(it_i->second);
        if(it_v == vertex_map.end()){
            //fprintf(stderr, "Error - No vertex descriptor found for node : %d\n", it_i->second);
            continue;
        }
        vertex_descriptor v_j = it_v->second;
        SlamNode *nd_i = getSlamNodeFromID(it_i->first);
        SlamNode *nd_j = getSlamNodeFromID(it_i->second);
        Pose2d delta = nd_i->getPose().ominus(nd_j->getPose());
        double weight = hypot(delta.x(), delta.y());
        //fprintf(stderr, "Adding Edge : %d - %d : %f\n", nd_i->id, nd_j->id, weight);
        add_edge(v_i, v_j, Weight(weight), g);
    }

    property_map<graph_t, edge_weight_t>::type weightmap = get(edge_weight, g);
    std::vector<vertex_descriptor> p(num_vertices(g));
    std::vector<double> d(num_vertices(g));
    
    dijkstra_shortest_paths(g, node->b_vertex,
                            predecessor_map(boost::make_iterator_property_map(p.begin(), 
                                                                              get(boost::vertex_index, g))).
                            distance_map(boost::make_iterator_property_map(d.begin(), get(boost::vertex_index, g))));
        
    int64_t e_utime = bot_timestamp_now();
    //fprintf(stderr, CYAN "Time to find Shortest Path : %f\n" RESET_COLOR, (e_utime - s_utime) / 1.0e6);
    //fprintf(stderr, CYAN "Distances from %d and parents\n" RESET_COLOR, node->id);
    graph_traits < graph_t >::vertex_iterator vi, vend;
    //from this we should build the minimum spanning tree for the graph - from the last node 

    map<SlamNode*, SlamNode *> parent_map; 

    
    for (boost::tie(vi, vend) = vertices(g); vi != vend; ++vi) {        
        SlamNode *nd_i = getSlamNodeFromID(g[*vi].id);
        SlamNode *nd_j = getSlamNodeFromID(g[p[*vi]].id);
        //fprintf(stderr, CYAN "Region %d [%d] -> Distance : %.3f - Parent : %d\n" RESET_COLOR, 
        //nd_i->region_node->region_id, nd_i->id, d[*vi], g[p[*vi]].id);
        parent_map.insert(make_pair(nd_i, nd_j));
        //want to interate trough this - to either build the paths or to build the Minimum spanning tree 
        /*graph_traits < graph_t >::vertex_iterator vp = g[p[*vi]];
          while(vp != */
    }
    //std::cout << std::endl;
    //}

    //if the current node has a complex language event - 
    //save the paths from this node to other region nodes - to evaluate this complex language expression 
    
    //if there are complex language nodes already in the graph 
    

    fprintf(stderr, "\n\n");

    ComplexLanguageEvent *current_event = getComplexLanguageEvent(node);


    if(current_event){
        for(int i=0; i < region_node_list.size(); i++){
            RegionNode *region = region_node_list[i];
            if(region == node->region_node)
                continue;
            
            vector<SlamNode *> path = getPath(parent_map, region->mean_node);
            std::reverse(path.begin(), path.end());
            current_event->addNodePath(region->mean_node, path);
        }
    }
    
    if(node->region_node->mean_node == node){
        //this was a region mean also - make sure this doesn't trigger for incomplete regions)
        map<int, ComplexLanguageEvent *>::iterator it; 
        for(it = complex_language_events.begin(); it != complex_language_events.end(); it++){
            
        }
    }
    
    /*for (boost::tie(vi, vend) = vertices(g); vi != vend; ++vi) {        
        SlamNode *nd_i = getSlamNodeFromID(g[*vi].id);
        if(nd_i == node)
            continue;
        
            if(nd_i->region_node->mean_node == nd_i){
                //SlamNode *nd_j = getSlamNodeFromID(g[p[*vi]].id);
                fprintf(stderr, CYAN "Region %d [%d] -> Distance : %.3f - Path : \n" RESET_COLOR, nd_i->region_node->region_id, nd_i->id, d[*vi]);
                vector<SlamNode *> path = getPath(parent_map, nd_i); 
            
                //path seems fine 
                for(int i=0; i < path.size(); i++){                    
                    fprintf(stderr, CYAN "\t %d [%d]\n" RESET_COLOR, path[i]->id, path[i]->region_node->region_id);
                }     
                std::reverse(path.begin(), path.end());
                //add this path to the event 
            }
        }
        }
        std::cout << std::endl;*/


    fprintf(stderr, GREEN "Done Finding Shortest Path \n\n" RESET_COLOR);    
}

void SlamGraph::fillLanguageCollection(slam_complex_language_collection_t &msg){
    msg.count = (int) complex_language_events.size();
    msg.language = (slam_complex_language_path_list_t *) calloc(msg.count, sizeof(slam_complex_language_path_list_t));
       
    map<int, ComplexLanguageEvent *>::iterator it_lang; 
    int i=0;
    vector<ComplexLanguageEvent *> valid_complex_language; 
    for(it_lang = complex_language_events.begin(); it_lang != complex_language_events.end(); it_lang++, i++){
        ComplexLanguageEvent *event = it_lang->second;
        event->fillNodePaths(msg.language[i]);
    }    
}

void SlamGraph::fillOutstandingLanguageCollection(slam_complex_language_collection_t &msg){
    msg.count = (int) complex_language_events.size();
    msg.language = (slam_complex_language_path_list_t *) calloc(msg.count, sizeof(slam_complex_language_path_list_t));
        
    map<int, ComplexLanguageEvent *>::iterator it_lang; 
    int i=0;
    vector<ComplexLanguageEvent *> valid_complex_language; 
    for(it_lang = complex_language_events.begin(); it_lang != complex_language_events.end(); it_lang++){
        ComplexLanguageEvent *event = it_lang->second;
        
        bool to_send = true; 

        if(event->complete){
            to_send = false;
        }
        if(event->handled_internally){
            to_send = false;
        }
        if(event->failed_grounding && ((event->number_of_regions_added_since_failure < new_regions_added_threshold) && (event->getIDsAddedSinceFailure() < NO_NODES_ADDED_AFTER_FAILURE))){
            to_send = false;
        }

        if(to_send){ 
            //!event->complete){                                                                
            event->fillOutstandingNodePaths(msg.language[i]);
            i++;
        }
    }    
    //reallocate to the new language size
    fprintf(stderr, "Outstanding Querry count : %d\n", i);
    msg.count = i;
    msg.language = (slam_complex_language_path_list_t *) realloc(msg.language, msg.count * sizeof(slam_complex_language_path_list_t));
}

int SlamGraph::addComplexLanguageUpdates(slam_complex_language_result_t *slu_msg, vector<RegionNode *> &grounded_regions, bool finish_if_good_grounding, bool finish_always){
    double grounding_threshold = 0.1; //dont add anything below this 
    double finish_grounding_threshold = 0.2; //was 0.2

    map<int, ComplexLanguageEvent *>::iterator it_lang; 
    for(int i=0; i< slu_msg->count; i++){
        slam_complex_language_path_result_list_t *event_result = &slu_msg->language[i];
        it_lang = complex_language_events.find(event_result->event_id);
        assert(it_lang != complex_language_events.end());
        ComplexLanguageEvent *event = it_lang->second;           

        if(finish_always){
            event->complete = true;
        }
        
        bool integrate_paths = true; 
        
        if(finish_if_good_grounding){
            //check the max path likelihood 
            double max_path_prob = 0; 
            
            for(int i=0; i < event_result->no_paths; i++){
                slam_complex_language_path_result_t *p_result = &event_result->results[i];

                for(int j=0; j < p_result->no_landmarks; j++){
                    //not sure if we should use this - or some overall prob measure 
                    if(max_path_prob < p_result->landmark_results[j].path_probability){
                        max_path_prob = p_result->landmark_results[j].path_probability;
                    }
                }
            }

            fprintf(stderr, "Max Path prob %f\n", max_path_prob);
            if(max_path_prob < finish_grounding_threshold){
                fprintf(stderr, "Path prob %f is not high enough - adding to reground list \n", max_path_prob);
                integrate_paths = false;                
            }            
        }
        
        if(integrate_paths){
            vector<pair<RegionNode *, slam_complex_language_path_result_t*> > path_results; 
            if(use_factor_graphs){
                for(int i=0; i < event_result->no_paths; i++){
                    slam_complex_language_path_result_t *p_result = &event_result->results[i];
                    SlamNode *nd = getSlamNodeFromID(p_result->node_id);
                    path_results.push_back(make_pair(nd->region_node, p_result));
                }                   
                
                event->updateResult(path_results);
            }
            else{               
                vector<regionProb> figure_groundings; 

                for(int i=0; i < event_result->no_paths; i++){
                    slam_complex_language_path_result_t *p_result = &event_result->results[i];

                    SlamNode *nd = getSlamNodeFromID(p_result->node_id);
                    RegionNode *figure_region = nd->region_node;
                    
                    map<int, double> landmark_likelihoods = event->getLandmarkLikelihoodsForFigure(figure_region);

                    map<int, double>::iterator it_like; 

                    /*fprintf(stderr, "==== Landmark likelihood for figure : %d  (No of landmarks : %d)=====\n", figure_region->region_id, landmark_likelihoods.size());
                    for(it_like = landmark_likelihoods.begin(); it_like != landmark_likelihoods.end(); it_like++){
                        fprintf(stderr, "\t[%d] - %f\n", it_like->first, it_like->second);
                        }*/

                    double ground_prob = 0;
                    for(int j=0; j < p_result->no_landmarks; j++){
                        int landmark_id = p_result->landmark_results[j].landmark_id;
                        
                        double lm_prob = 0;
                        it_like = landmark_likelihoods.find(landmark_id);

                        if(it_like != landmark_likelihoods.end()){
                            lm_prob = it_like->second;
                        }

                        //fprintf(stderr, "Landmark : %d -> %f\n", landmark_id, lm_prob);
                        
                        ground_prob += p_result->landmark_results[j].path_probability * lm_prob; 
                        fprintf(stderr, "\tPath Prob : %f - Landmark (%d) Prob : %f => Overall : %f\n", p_result->landmark_results[j].path_probability, 
                                landmark_id, lm_prob, p_result->landmark_results[j].path_probability * lm_prob);
                    }
                    
                    //should the ground prob be normalized?? 
                    figure_groundings.push_back(make_pair(figure_region, ground_prob));
                }

                sort(figure_groundings.begin(), figure_groundings.end(), compareRegionProb);
                                
                for(int i=0; i < figure_groundings.size(); i++){
                    RegionNode *figure_region = figure_groundings[i].first;
                    double ground_prob = figure_groundings[i].second;
                    
                    int figure_id = figure_region->labeldist->getLabelID(event->figure);
                    
                    if(i>= LANGUAGE_TOP_N_GROUNDINGS){
                        fprintf(stderr, "Done grounding the top %d\n", LANGUAGE_TOP_N_GROUNDINGS);
                        break;
                    }
                    //this event id is wrong 
                    if(ground_prob > grounding_threshold){
                        figure_region->labeldist->addObservation(figure_id, ground_prob, 1000 + event->event_id);
                        grounded_regions.push_back(figure_region);
                        fprintf(stderr, "Grounding Prob for region : %d Adding => %f\n", (int) figure_region->region_id, ground_prob);
                    }
                    else{
                        fprintf(stderr, "Grounding Prob for region : %d Too low=> %f\n", (int) figure_region->region_id, ground_prob);
                        break;
                    }
                } 
            }           
            
            //decide this based on the mode - i.e. we might not want to ground this till we get a good likelihood score 
            if(finish_if_good_grounding){
                fprintf(stderr, "Finishing integration\n");
                event->complete = true;
                
                //we need to send up the regions that got grounded - so that loop closures can be performed if desired 
                
                

                /*
                  int label_id = current_region->labeldist->getLabelID(string(annotation->update));
                    
                    if(label_id>=0){
                        current_region->labeldist->addDirectObservation(label_id, prob_correspondance, language_event_count);
                    }

                 */

                //we should integrate the information immediately to the label distributions if we are not using factor graphs 
            }
        }
        else if(finish_if_good_grounding){
            //we need a way to tell this to not try to ground immediately 
            //HACK 
            fprintf(stderr, "Failed to ground language -> adding to ungrounded language - for checking groundings later\n");
            //exit(-1);
            event->failed_grounding = true;
            event->number_of_regions_added_since_failure = 0;
            event->updateFailedGrounding();
        }
    }
}

void SlamGraph::buildBoostGraph(graph_t &g){
    map<int, vertex_descriptor> vertex_map;
    map<int, vertex_descriptor>::iterator it_v;
    for (int i=0; i < slam_node_list.size(); i++){
        vertex_descriptor v = add_vertex(g);
        g[v].id = slam_node_list[i]->id;        
        vertex_map.insert(make_pair(g[v].id, v));
    }

    map<int, SlamConstraint *>::iterator it;
    set<pair<int, int> > valid_edges;
    set<pair<int, int> >::iterator it_edge_set;
    map<int, int>::iterator it_i;
    map<int, int>::iterator it_j;

    for (int i=0; i < slam_node_list.size(); i++){
        SlamNode *nd_i = slam_node_list[i];

        for (int j=0; j < slam_node_list.size(); j++){
            SlamNode *nd_j = slam_node_list[j];

            if(nd_i == nd_j){
                continue;
            }

            it = nd_i->constraints.find(nd_j->id);
            Pose2d delta = nd_i->getPose().ominus(nd_j->getPose());

            if(it != nd_i->constraints.end() || hypot(delta.x(), delta.y()) < 1.0){
                it_edge_set = valid_edges.find(make_pair(nd_i->id, nd_j->id));
                if(it_edge_set != valid_edges.end())
                    continue;
                it_edge_set = valid_edges.find(make_pair(nd_j->id, nd_i->id));
                if(it_edge_set != valid_edges.end())
                    continue;
                valid_edges.insert(make_pair(nd_i->id, nd_j->id));
            }
        }
    }
        
    for(it_edge_set = valid_edges.begin(); it_edge_set != valid_edges.end(); it_edge_set++){
        nodePairKey edge = *it_edge_set; 
        it_v = vertex_map.find(edge.first);
        if(it_v == vertex_map.end()){
            //fprintf(stderr, "Error - No vertex descriptor found for node : %d\n", it_i->first);
            continue;
        }

        vertex_descriptor v_i = it_v->second;
        it_v = vertex_map.find(edge.second);
        if(it_v == vertex_map.end()){
            //fprintf(stderr, "Error - No vertex descriptor found for node : %d\n", it_i->second);
            continue;
        }
        vertex_descriptor v_j = it_v->second;
        SlamNode *nd_i = getSlamNodeFromID(edge.first);
        SlamNode *nd_j = getSlamNodeFromID(edge.second);
        Pose2d delta = nd_i->getPose().ominus(nd_j->getPose());
        double weight = hypot(delta.x(), delta.y());
        add_edge(v_i, v_j, Weight(weight), g);
    }
}

map<SlamNode*, SlamNode *> SlamGraph::getShortestPathTree(SlamNode *node, graph_t &g)
{
    property_map<graph_t, edge_weight_t>::type weightmap = get(edge_weight, g);
    std::vector<vertex_descriptor> p(num_vertices(g));
    std::vector<double> d(num_vertices(g));
    
    dijkstra_shortest_paths(g, node->b_vertex,
                            predecessor_map(boost::make_iterator_property_map(p.begin(), 
                                                                              get(boost::vertex_index, g))).
                            distance_map(boost::make_iterator_property_map(d.begin(), get(boost::vertex_index, g))));
        
    int64_t e_utime = bot_timestamp_now();
    //fprintf(stderr, CYAN "Time to find Shortest Path : %f\n" RESET_COLOR, (e_utime - s_utime) / 1.0e6);
    //fprintf(stderr, CYAN "Distances from %d and parents\n" RESET_COLOR, node->id);
    graph_traits < graph_t >::vertex_iterator vi, vend;
    //from this we should build the minimum spanning tree for the graph - from the last node 

    map<SlamNode*, SlamNode *> parent_map; 
    set<SlamNode *> node_set;
    map<SlamNode*, set<SlamNode *> > neighbour_map; 

    map<SlamNode*, set<SlamNode *> >::iterator it_neighbour_map;
    
    //this is not building a minimum spanning tree right now 
    //this just tells which edges are there (which is effected by the way its included) 
    
    for (boost::tie(vi, vend) = vertices(g); vi != vend; ++vi) {        
        SlamNode *nd_i = getSlamNodeFromID(g[*vi].id);
        SlamNode *nd_j = getSlamNodeFromID(g[p[*vi]].id);

        parent_map.insert(make_pair(nd_i, nd_j));
    }
        
    return parent_map; 
}

void SlamGraph::getShortestPathTree(SlamNode *node, graph_t &g, map<SlamNode*, SlamNode *> &parent_map, map<SlamNode *, double> &dist_map)
{
    property_map<graph_t, edge_weight_t>::type weightmap = get(edge_weight, g);
    std::vector<vertex_descriptor> p(num_vertices(g));
    std::vector<double> d(num_vertices(g));
    
    dijkstra_shortest_paths(g, node->b_vertex,
                            predecessor_map(boost::make_iterator_property_map(p.begin(), 
                                                                              get(boost::vertex_index, g))).
                            distance_map(boost::make_iterator_property_map(d.begin(), get(boost::vertex_index, g))));
        
    int64_t e_utime = bot_timestamp_now();
    //fprintf(stderr, CYAN "Time to find Shortest Path : %f\n" RESET_COLOR, (e_utime - s_utime) / 1.0e6);
    //fprintf(stderr, CYAN "Distances from %d and parents\n" RESET_COLOR, node->id);
    graph_traits < graph_t >::vertex_iterator vi, vend;
    //from this we should build the minimum spanning tree for the graph - from the last node 

    set<SlamNode *> node_set;
    map<SlamNode*, set<SlamNode *> > neighbour_map; 

    map<SlamNode*, set<SlamNode *> >::iterator it_neighbour_map;
    
    //this is not building a minimum spanning tree right now 
    //this just tells which edges are there (which is effected by the way its included) 
    
    for (boost::tie(vi, vend) = vertices(g); vi != vend; ++vi) {        
        SlamNode *nd_i = getSlamNodeFromID(g[*vi].id);
        SlamNode *nd_j = getSlamNodeFromID(g[p[*vi]].id);

        parent_map.insert(make_pair(nd_i, nd_j));
        dist_map.insert(make_pair(nd_i, d[*vi]));
    }
}

map<SlamNode*, SlamNode *> SlamGraph::getShortestPathTree(SlamNode *node)
{
    graph_t g; 
    
    buildBoostGraph(g);
    return getShortestPathTree(node, g);
}

//other option is to update this every time the graph is updated?? - with some heuristic 
//then the wrong paths can get updated 

void SlamGraph::checkCurrentRegionForInfoGain(RegionNode *current_region){
    //this should be general 
    //check the reduction in entropy if the robot asks about the current region 
    if(complex_language_events.size() == 0){
        return;
    }
    vector<SRQuestion> language_questions; 
    //find the max question to ask here 
    for(int i=0; i < complex_language_events.size(); i++){
        ComplexLanguageEvent *event = complex_language_events[i];
        int landmark = 0;
        //maybe should check with topologically close locations??
        double info_gain = event->evalQuestionsAboutRegion(current_region, 0.3, &landmark);        
    }    
}

slam_language_question_t* SlamGraph::getOutstandingQuestion(int64_t p_id){
    //the SR should be given an ID and moved to the asked question queue?? 
    slam_language_question_t *msg = NULL;
    if(outstanding_question){
        if(asked_question){
            //should we ever get to this state ?? 
            fprintf(stderr, "Oustanding question asked - unable to ask this question\n");
            return NULL;
        }
        else{
            msg = (slam_language_question_t *) calloc(1, sizeof(slam_language_question_t));
            msg->id = outstanding_question->id;
            msg->particle_id = p_id;
            msg->utime = bot_timestamp_now();
            string question_string = askSRQuestion(*outstanding_question);
            msg->question = strdup(question_string.c_str());
            asked_question = outstanding_question;
            outstanding_question = NULL;
        }
    }
    return msg;     
}

int SlamGraph::updateAnswer(int id, string answer){
    if(asked_question){
        if(asked_question->id == id){
            fprintf(stderr, "Question matches answer ID\n");
            fprintf(stderr, "Answer to question : %s - %s\n", askSRQuestion(*asked_question).c_str(), 
                    answer.c_str());
            //add routine to update the complex language with the answer 

            //TO-DO//
            //if the answer is that this is the place - then we should set the complex language as complete 

            //integrate the received answer to the complex language 
            //if(!strcmp(answer, "yes")){
            if(!answer.compare("yes")){
                fprintf(stderr, "Received a yes answer\n");
                //integrate yes answer 
                //asked_question->event->updateSRQuestionAnswer(asked_question, answer);
                
            } 
            else if(!answer.compare("no")){//!strcmp(answer, "no")){
                fprintf(stderr, "Received a no answer\n");
                //asked_question->event->updateSRQuestionAnswer(asked_question, answer);
            }

            delete asked_question; 
            asked_question = NULL;
            return 0;
        }
        else{
            fprintf(stderr, "Asked question is not the same as the answered question - this should not happen\n");
            return -1;
        }
    }
    else{
        fprintf(stderr, "No question has been asked\n");
        return -1;
    }
}

int SlamGraph::getNextQuestionID(){
    return SlamGraph::asked_question_id++;
}

string SlamGraph::askSRQuestion(SRQuestion &question){
    
    //add this to a queue maybe and call from main - since main is the only place where the lcm publishing happens 
    //right now 
    string question_label; 
    if(question.landmark){
        question_label = question.event->landmark;
    }
    else{
        question_label = question.event->figure;
    }
    string question_str = createQuestion(question_label, question.sr_result.first);
    fprintf(stderr, "Asking question :\n\t%s\n", question_str.c_str());
    return question_str;
}

void SlamGraph::updateOutstandingQuestion(SRQuestion &question, vector<indexProb> &info_prob){
    if(outstanding_question){
        fprintf(stderr, "Already have outstanding question - which hasn't been cleared - should we skip adding this question??\n");
        return;
    }
    //delete outstanding_question; 
    outstanding_question = new SRQuestion(question);
    //we should set this SR result temp in the complex language (and possibly the SR question attached to it) 
    //for future information?? 
    outstanding_question->updateEventCount();
    outstanding_question->updateInfoProb(info_prob);
    outstanding_question->setID(getNextQuestionID());
}

void SlamGraph::checkSRQuestionInfoGain(SlamNode *node){
    if(complex_language_events.size() == 0 || asked_question){ //if we have an asked question we will not ask another till we hear the answer - or timeout?? 
        return;
    }
    int64_t s_utime = bot_timestamp_now();
    //we should look at which one is the best question to ask about which complex language 
    //also need to know whether we are asking a question about the landmark/figure 

    int skip_question_threshold = 3;

    //find the max 
    double max_info_gain = 0; 
    vector<indexProb> max_info_prob; 
        
    vector<SRQuestion> language_questions; 
    for(int i=0; i < complex_language_events.size(); i++){
        ComplexLanguageEvent *event = complex_language_events[i];
        fprintf(stderr, "No of asked questions : %d\n", event->getNoAskedQuestions());
        if(event->getNoAskedQuestions() >= skip_question_threshold){
            fprintf(stderr, "Asked enough questions - skipping this language event\n");
            continue;
        }

        int landmark = 0;
        //maybe should check with topologically close locations??
        //maybe we should get the full information for this calculation?? 
        vector<indexProb> info_prob; 
        spatialResults sr_question = event->evalSRQuestionsFromCurrentLocation(node, 0.3, &landmark, info_prob);

        fprintf(stderr, "SR likelihoods\n");
        for(int k=0; k < info_prob.size(); k++){
            fprintf(stderr, "\t%d - %f\n", info_prob[k].first, info_prob[k].second);
        }

        //maybe there should be a penalty for how many questions/when the last question was asked ??
        //so that we do not keep asking about the same thing??
        //also a way to not ask the same question?? 
        if(sr_question.first != INVALID && sr_question.second > 0){
            fprintf(stderr, "Adding SR : %s\n", getSR(sr_question.first).c_str()); 
            SRQuestion question(event, landmark, sr_question); 
            //question.event = event;
            //question.landmark = landmark;
            //question.sr_result = sr_question;
            language_questions.push_back(question);

            if(max_info_gain < sr_question.second){
                max_info_gain = sr_question.second;
                max_info_prob = info_prob;
            }
        }
    }
    //lets sort these 
    if(language_questions.size() > 0){
        sort(language_questions.begin(), language_questions.end(), compareSRQuestions);

        fprintf(stderr, "Max SR likelihoods\n");
        for(int k=0; k < max_info_prob.size(); k++){
            fprintf(stderr, "\t%d - %f\n", max_info_prob[k].first, max_info_prob[k].second);
        }
        updateOutstandingQuestion(language_questions[0], max_info_prob);
        askSRQuestion(language_questions[0]);
    }
    int64_t e_utime = bot_timestamp_now();
    fprintf(stderr, GREEN "Time taken : %.3f\n", (e_utime - s_utime) / 1.0e6);
}

vector<RegionNode *> SlamGraph::findShortestPathFromOutstandingComplexLanguage(RegionNode *current_region, bool complete_event, bool complete_if_valid){
    vector<RegionNode *> grounded_regions;
    map<int, ComplexLanguageEvent *>::iterator it_lang; 

    vector<ComplexLanguageEvent *> valid_complex_language; 
    
    //we can control which language events are considered for this region - by setting the language events as complete 
    for(it_lang = complex_language_events.begin(); it_lang != complex_language_events.end(); it_lang++){
        if(it_lang->second->utterence_node->region_node != current_region && !it_lang->second->complete){
        //if(1){//!it_lang->second->complete){
            fprintf(stderr, "Complex Language valid - adding\n");
            valid_complex_language.push_back(it_lang->second);
        }
    }

    //this valid should not be too much 
    fprintf(stderr, "\n\n\n\nTotal Complex Language : %d -> Valid Complex language : %d\n\n\n\n", (int) complex_language_events.size(), 
            (int) valid_complex_language.size());
    
    if(valid_complex_language.size() ==0){
        fprintf(stderr, GREEN "No Complex Language events - Skipping Finding Shortest Path \n\n" RESET_COLOR);
        return grounded_regions; 
    }
    
    int64_t s_utime = bot_timestamp_now();
    
    fprintf(stderr, "Current Region : %d\n", (int) current_region->region_id);
    
    graph_t g;
    buildBoostGraph(g);


    for(int i=0; i < valid_complex_language.size(); i++){
        ComplexLanguageEvent *event = valid_complex_language[i];        

        event->updateLastCheckedGrounding(getLastNodeID());
        
        event->complete = complete_event;

        SlamNode *node = event->utterence_node; 

        map<SlamNode*, SlamNode *> parent_map;
        map<SlamNode *, double> dist_map;
        getShortestPathTree(node, g, parent_map, dist_map);

        int added_count = 0;

        vector<RegionNode *> landmark_regions;
        fprintf(stderr, "Complex Language associatd with region : %d\n", (int) node->region_node->region_id);

        for(int i=0; i < region_node_list.size(); i++){
            RegionNode *region = region_node_list[i];
            //skip the region in which the utterance was heard and the current unformed region

            if(region == current_region){
                continue;
            }
                       
            double dist = getPathLength(dist_map, region->mean_node); 
            fprintf(stderr, "\tRegion : %d => Distance : %f\n", region->region_id, dist);

            if(dist < ignore_landmark_dist){
                fprintf(stderr, "\tAdding Landmark : %d\n", region->region_id);
                landmark_regions.push_back(region);
            }
            //if(region->mean_node->getDistanceToNode(current_event->utterence_node) > ignore_figure_dist){
            if(region == node->region_node || dist > ignore_figure_dist){
                continue;
            }
            
            added_count++;
            vector<SlamNode *> path = getPath(parent_map, region->mean_node);
            std::reverse(path.begin(), path.end());
            event->addNodePath(region->mean_node, path);            
        }
       

        /**/
        event->updateLandmarks(landmark_regions);
        fprintf(stderr, GREEN "Done Finding Shortest Path From Language Event %d => Added %d Paths \n\n" RESET_COLOR, event->event_id, added_count);         
        if(event->failed_grounding){
            fprintf(stderr, "Failed event - New Regions added : %d\n", event->number_of_regions_added_since_failure);
            if(event->number_of_regions_added_since_failure > new_regions_added_threshold || event->getIDsAddedSinceFailure() > NO_NODES_ADDED_AFTER_FAILURE){              
                fprintf(stderr, "Checking Internally\n");
                event->evaluateRegionPaths(complete_if_valid, grounded_regions, MIN_LANG_GROUNDING_PROB, LANGUAGE_TOP_N_GROUNDINGS);
            }
        }
        else{
            event->evaluateRegionPaths(complete_if_valid, grounded_regions, MIN_LANG_GROUNDING_PROB, LANGUAGE_TOP_N_GROUNDINGS);
        }
        /*if(added_count > 0){
            exit(-1);
            }*/
    }
    return grounded_regions; 
}

int64_t SlamGraph::getLastNodeID(){
    SlamNode *last_node = getLastSlamNode();
    if(last_node){
        return last_node->id;
    }
    return -1; 
}


//find shortest path from the given node (which has complex language) to other nodes 
//actually this should only be called when the region is split off - otherwise more likely that the path is wrong 
void SlamGraph::findShortestPathFromComplexLanguage(SlamNode *node)
{
    ComplexLanguageEvent *current_event = getComplexLanguageEvent(node);
    if(complex_language_events.size() == 0 || current_event == NULL){
        fprintf(stderr, GREEN "No Complex Language events for the current node - Skipping Finding Shortest Path \n\n" RESET_COLOR);
        return; 
    }

    int64_t s_utime = bot_timestamp_now();
    
    graph_t g;
    buildBoostGraph(g);
    map<SlamNode*, SlamNode *> parent_map;
    map<SlamNode *, double> dist_map;
    getShortestPathTree(node, g, parent_map, dist_map);

    for(int i=0; i < region_node_list.size(); i++){
        RegionNode *region = region_node_list[i];
        if(region == node->region_node){
            continue;
        }
        double dist = getPathLength(dist_map, region->mean_node); 
        
        //if(region->mean_node->getDistanceToNode(current_event->utterence_node) > ignore_figure_dist){
        if(dist > ignore_figure_dist){
            continue;
        }
        vector<SlamNode *> path = getPath(parent_map, region->mean_node);
        std::reverse(path.begin(), path.end());
        current_event->addNodePath(region->mean_node, path);
        //current_event->printPath(region->mean_node);        
    }
    
    fprintf(stderr, GREEN "Done Finding Shortest Path From Language Event %d \n\n" RESET_COLOR, current_event->event_id);    
}

void SlamGraph::printComplexLanguageEvents()
{
    //update the paths for all complex language - get the shortest paths from each complex language node to other regions 
    //and update the regions 
    if(complex_language_events.size() == 0)
        return;

    map<int, ComplexLanguageEvent *>::iterator it_lang; 
    for(it_lang = complex_language_events.begin(); it_lang != complex_language_events.end(); it_lang++){
        //for efficiency - this should only be called upon if the graph changed significantly or 
        //if nodes were added nearby 

        ComplexLanguageEvent *current_event = it_lang->second;
        current_event->print();
    }
}

void SlamGraph::findShortestPathFromAllComplexLanguage(RegionNode *current_region)
{
    //update the paths for all complex language - get the shortest paths from each complex language node to other regions 
    //and update the regions 
    if(complex_language_events.size() == 0)
        return;

    int64_t s_utime = bot_timestamp_now();
    
    graph_t g;
    buildBoostGraph(g);

    map<int, ComplexLanguageEvent *>::iterator it_lang; 
    for(it_lang = complex_language_events.begin(); it_lang != complex_language_events.end(); it_lang++){
        //for efficiency - this should only be called upon if the graph changed significantly or 
        //if nodes were added nearby 

        ComplexLanguageEvent *current_event = it_lang->second;
        
        //current_event->clearNodePaths();
        map<SlamNode*, SlamNode *> parent_map;
        map<SlamNode *, double> dist_map;
        getShortestPathTree(current_event->utterence_node, g, parent_map, dist_map);

        vector<RegionNode *> landmark_regions;

        for(int i=0; i < region_node_list.size(); i++){
            RegionNode *region = region_node_list[i];
            if(region == current_region){
                continue;
            }
            double dist = getPathLength(dist_map, region->mean_node); 
            //double dist = region->mean_node->getDistanceToNode(current_event->utterence_node);
            //fprintf(stderr, "Distance to region %d : %f\n", region->region_id, dist);

            if(dist < ignore_landmark_dist){
                landmark_regions.push_back(region);
            }

            if(dist > ignore_figure_dist){
                //fprintf(stderr, "skipping\n");
                continue;
            }
            vector<SlamNode *> path = getPath(parent_map, region->mean_node);
            current_event->addNodePath(region->mean_node, path);
        }
        current_event->updateLandmarks(landmark_regions);
        fprintf(stderr, GREEN "Done Finding Shortest Path From Language Event %d \n\n" RESET_COLOR, current_event->event_id);    
    }

    int64_t e_utime = bot_timestamp_now();
    //seems pretty short time to do this 
    fprintf(stderr, GREEN "Done Finding Shortest Path From Complex Language to completed region : %.5f\n\n" RESET_COLOR, (e_utime - s_utime) / 1.0e6);
}

//this is for the partially formed region - so the eval should be provisional - to be flushed when the region is fully formed 
void SlamGraph::findShortestPathFromAllComplexLanguageToCurrentRegion(RegionNode *region)
{
    //update the paths for all complex language - get the shortest paths from each complex language node to other regions 
    //and update the regions 
    if(complex_language_events.size() == 0)
        return;

    int64_t s_utime = bot_timestamp_now();
    
    graph_t g;
    buildBoostGraph(g);

    map<int, ComplexLanguageEvent *>::iterator it_lang; 
    for(it_lang = complex_language_events.begin(); it_lang != complex_language_events.end(); it_lang++){
        //for efficiency - this should only be called upon if the graph changed significantly or 
        //if nodes were added nearby 

        ComplexLanguageEvent *current_event = it_lang->second;
        
        //current_event->clearNodePaths();
        map<SlamNode*, SlamNode *> parent_map;
        map<SlamNode *, double> dist_map;
        getShortestPathTree(current_event->utterence_node, g, parent_map, dist_map);

        vector<RegionNode *> landmark_regions;

        
        double dist = getPathLength(dist_map, region->mean_node); 
        //double dist = region->mean_node->getDistanceToNode(current_event->utterence_node);
        //fprintf(stderr, "Distance to region %d : %f\n", region->region_id, dist);
        
        if(dist > ignore_figure_dist){
            //fprintf(stderr, "skipping\n");
            continue;
        }
        vector<SlamNode *> path = getPath(parent_map, region->mean_node);
        //we should update this - so that it is added to a provisional path 
        current_event->addNodePath(region->mean_node, path);
        
        fprintf(stderr, GREEN "Done Finding Shortest Path From Language Event %d \n\n" RESET_COLOR, current_event->event_id);    
    }

    int64_t e_utime = bot_timestamp_now();
    //seems pretty short time to do this 
    fprintf(stderr, GREEN "Done Finding Shortest Path From Complex Language to completed region : %.5f\n\n" RESET_COLOR, (e_utime - s_utime) / 1.0e6);
}

int SlamGraph::getOutstandingLanguageCount(){
    int count = 0; 
    map<int, ComplexLanguageEvent *>::iterator it_lang; 
    for(it_lang = complex_language_events.begin(); it_lang != complex_language_events.end(); it_lang++){
        ComplexLanguageEvent *current_event = it_lang->second;

        bool to_send = true; 

        if(current_event->complete){
            fprintf(stderr, "Event : %d - Complete\n", current_event->event_id);
            to_send = false;
        }
        if(current_event->handled_internally){
            fprintf(stderr, "Event : %d - Handled Internally\n", current_event->event_id);
            to_send = false;
        }
        if(current_event->failed_grounding && ((current_event->number_of_regions_added_since_failure < new_regions_added_threshold) && (current_event->getIDsAddedSinceFailure() < NO_NODES_ADDED_AFTER_FAILURE))) {
            fprintf(stderr, "Event failed [%d] - New regions added : %d - Setting false\n", current_event->event_id, current_event->number_of_regions_added_since_failure);
            to_send = false;
        }

        if(to_send){ //!current_event->complete)
            count += current_event->getOutstandingCount();
            fprintf(stderr, "Event : %d - Adding to check Grounding : %d\n", current_event->event_id, current_event->getOutstandingCount());
        }
    }        

    fprintf(stderr, RED "\n\n Outstanding Language : %d\n", count);
    return count;
}

int SlamGraph::getLanguagePathCount(){
    int count = 0; 
    map<int, ComplexLanguageEvent *>::iterator it_lang; 
    for(it_lang = complex_language_events.begin(); it_lang != complex_language_events.end(); it_lang++){
        ComplexLanguageEvent *current_event = it_lang->second;
        count += current_event->getNoPaths();
    }
    
    fprintf(stderr, RED "\n\n Complex Language : %d\n", count);
    return count;
}

//find shortest path from the given node (which has complex language) to other nodes 
void SlamGraph::findShortestPathForRegionToComplexLanguage(RegionNode *complete_region, RegionNode *current_region)
{
    map<int, ComplexLanguageEvent *>::iterator it_lang; 

    vector<ComplexLanguageEvent *> valid_complex_language; 
    
    //we can control which language events are considered for this region - by setting the language events as complete 
    for(it_lang = complex_language_events.begin(); it_lang != complex_language_events.end(); it_lang++){
        if(it_lang->second->utterence_node->region_node != complete_region && it_lang->second->utterence_node->region_node != current_region){ 
            //&& it_lang->second->complete){ -- need to add this back 
            valid_complex_language.push_back(it_lang->second);
        }
    }

    /*fprintf(stderr, "\n\n\n\nTotal Complex Language : %d -> Valid Complex language : %d\n\n\n\n", (int) complex_language_events.size(), 
            (int) valid_complex_language.size());
    
    if(valid_complex_language.size() > 0){
        exit(-1);
        }*/

    if(valid_complex_language.size() ==0){
        fprintf(stderr, GREEN "No Complex Language events - Skipping Finding Shortest Path \n\n" RESET_COLOR);
        return; 
    }

    int64_t s_utime = bot_timestamp_now();
    
    SlamNode *node = complete_region->mean_node;    
        
    graph_t g;
    buildBoostGraph(g);

    map<SlamNode*, SlamNode *> parent_map;
    map<SlamNode *, double> dist_map;
    getShortestPathTree(node, g, parent_map, dist_map);

    for(int i=0; i < valid_complex_language.size(); i++){
        ComplexLanguageEvent *event = valid_complex_language[i];        
        //this is the path from the node at which the complex language was heard to the newly added region mean node 
        double dist = getPathLength(dist_map, event->utterence_node); 
        //if(complete_region->mean_node->getDistanceToNode(event->utterence_node) > ignore_figure_dist){
        if(dist > ignore_figure_dist){
            continue;
        }
        vector<SlamNode *> path = getPath(parent_map, event->utterence_node);
        //this will update the path 
        event->addNodePath(node, path);
        //event->printPath(node);        
    }
    int64_t e_utime = bot_timestamp_now();
    //seems pretty short time to do this 
    fprintf(stderr, GREEN "Done Finding Shortest Path From Complex Language to completed region : %.5f\n\n" RESET_COLOR, (e_utime - s_utime) / 1.0e6);   
}

ComplexLanguageEvent * SlamGraph::getComplexLanguageEvent(SlamNode *node){
    map<int, ComplexLanguageEvent *>::iterator it_node;
        
    it_node = complex_language_events_by_node.find(node->id);

    if(it_node != complex_language_events_by_node.end())
        return it_node->second; 
    return NULL;
}

void SlamGraph::addComplexLanguageEvent(complex_language_event_t *complex_lang){
    map<int, ComplexLanguageEvent *>::iterator it = complex_language_events.find(complex_lang->language_event_id);
    if(it == complex_language_events.end()){
        ComplexLanguageEvent *complex_event = new ComplexLanguageEvent(complex_lang, getSlamNodeFromID(complex_lang->node_id), label_info, use_factor_graphs);
        complex_language_events.insert(make_pair(complex_lang->language_event_id, complex_event));
        map<int, ComplexLanguageEvent *>::iterator it_node;
        
        it_node = complex_language_events_by_node.find(complex_lang->node_id);
        if(it_node == complex_language_events_by_node.end()){
            complex_language_events_by_node.insert(make_pair(complex_lang->node_id, complex_event));
        }
        else{ //unlikely that there will be more than one complex language event per node 
            fprintf(stderr, "Complex event already found for node - this should not have happened (or least not supported)\n");
            assert(false); 
        }
    }
    else{
        fprintf(stderr, "Duplicate language id\n");
        assert(false);
    }
}


//find shortest path from the given node to other nodes 
void SlamGraph::findShortestPath(SlamNode *node)
{
    fprintf(stderr, GREEN "Finding Shortest Path \n\n" RESET_COLOR);
    property_map<graph_t, edge_weight_t>::type weightmap = get(edge_weight, b_graph);
    std::vector<vertex_descriptor> p(num_vertices(b_graph));
    std::vector<double> d(num_vertices(b_graph));
    int64_t s_utime = bot_timestamp_now();
    dijkstra_shortest_paths(b_graph, node->b_vertex,
                            predecessor_map(boost::make_iterator_property_map(p.begin(), 
                                                                              get(boost::vertex_index, b_graph))).
                            distance_map(boost::make_iterator_property_map(d.begin(), get(boost::vertex_index, b_graph))));
        
    int64_t e_utime = bot_timestamp_now();
    fprintf(stderr, CYAN "Time to find Shortest Path : %f\n" RESET_COLOR, (e_utime - s_utime) / 1.0e6);
    fprintf(stderr, CYAN "Distances from %d and parents\n" RESET_COLOR, node->id);
    graph_traits < graph_t >::vertex_iterator vi, vend;
    for (boost::tie(vi, vend) = vertices(b_graph); vi != vend; ++vi) {        
        std::cout << "Node : " << b_graph[*vi].id <<  " distance = " << d[*vi] << " => Parent : " << b_graph[p[*vi]].id << endl;
    }
    std::cout << std::endl;

    fprintf(stderr, GREEN "Done Finding Shortest Path \n\n" RESET_COLOR);    
}

SlamConstraint* SlamGraph::addConstraint(int id, SlamNode *node1,  SlamNode *node2, SlamNode *actualnode1, 
                                         SlamNode *actualnode2, Pose2d transform, Noise noise, 
                                         double hit_pct, int32_t type, int32_t status)
{
    SlamConstraint *constraint = new SlamConstraint(id, node1, node2, actualnode1, actualnode2, transform, noise, hit_pct, 1, type, status);
    //lets add the relavent node as well 
    if(type == SLAM_GRAPH_EDGE_T_TYPE_ODOM_INC || type == SLAM_GRAPH_EDGE_T_TYPE_SM_INC){
        //this is an incremental constraint 
        node1->inc_constraint = constraint;
    }

    node1->constraints.insert(pair<int, SlamConstraint *>(node2->id,constraint));
    node2->constraints_from.insert(pair<int, SlamConstraint *>(node1->id,constraint));
    slam_constraints.insert(pair<int, SlamConstraint *>(constraint->id,constraint));
    slam->add_factor(constraint->ct_pose2d);
    no_edges_added++;

    //add_edge(node1->b_vertex, node2->b_vertex, b_graph);
    add_edge(node1->b_vertex, node2->b_vertex, Weight(1.0), b_graph);//, Weight(1));
    // Bleed labels between nodes if i) they are both supernodes and ii) they are not sequential
    // iii) node that is bled from has language observation and iv) constraint type is not a language edge
    /*int node1_prev_supernode_id = getPreviousSupernodeID (node1->id, SUPERNODE_SEARCH_DISTANCE);
    int node2_prev_supernode_id = getPreviousSupernodeID (node2->id, SUPERNODE_SEARCH_DISTANCE);
    if (node1->slam_pose->is_supernode && node1->slam_pose->is_supernode && (node1->id != node2_prev_supernode_id)
        && (node2->id != node1_prev_supernode_id) && (type != SLAM_GRAPH_EDGE_T_TYPE_LANGUAGE) ) {
        
        LabelDistribution *ld1 = node1->labeldist;
        node2->labeldist->bleedLabels (ld1);

        LabelDistribution *ld2 = node2->labeldist;
        node1->labeldist->bleedLabels (ld2);
        }*/
    
    return constraint;
}

void SlamGraph::clearDummyConstraints(){
    map<int, SlamConstraint *>::iterator c_it;
    for ( c_it= failed_slam_constraints.begin() ; c_it != failed_slam_constraints.end(); c_it++ ){        
        delete c_it->second;
    }
    failed_slam_constraints.clear();
}

void SlamGraph::addDummyConstraint(SlamNode *node1,  SlamNode *node2, SlamNode *actualnode1, 
                                   SlamNode *actualnode2, Pose2d transform, Noise noise, 
                                   double hit_pct, int32_t type, int32_t status){
    int id = failed_slam_constraints.size();
    SlamConstraint *constraint = new SlamConstraint(id, node1, node2, actualnode1, actualnode2, 
                                                    transform, noise, hit_pct, 0, type, status);
    //node1->constraints.insert(pair<int, SlamConstraint *>(node2->id, constraint));
    failed_slam_constraints.insert(pair<int, SlamConstraint *>(id,constraint));
    //no_edges_added++;
}

void SlamGraph::addDummyConstraint(int id, SlamNode *node1,  SlamNode *node2, SlamNode *actualnode1, 
                                   SlamNode *actualnode2, Pose2d transform, Noise noise, 
                                   double hit_pct, int32_t type, int32_t status){
    SlamConstraint *constraint = new SlamConstraint(id, node1, node2, actualnode1, actualnode2, 
                                                    transform, noise, hit_pct, 0, type, status);
    //node1->constraints.insert(pair<int, SlamConstraint *>(node2->id, constraint));
    failed_slam_constraints.insert(pair<int, SlamConstraint *>(id,constraint));
    //no_edges_added++;
}

int SlamGraph::hasConstraint(int const_id){// id_1, int id_2){
    map<int, SlamConstraint *>::iterator it; 
    it = slam_constraints.find(const_id);//->second;
    
    if(it == slam_constraints.end())
        return 0;

    SlamConstraint *ct = it->second;

    if(ct==NULL) //this shouldnt be
        return 0;

    return 1; 
}

int SlamGraph::hasConstraint(int id_1, int id_2, int32_t type){
    SlamNode *node1 = getSlamNodeFromID(id_1);
    SlamNode *node2 = getSlamNodeFromID(id_2);

    map<int, SlamConstraint *>::iterator it; 
    it = node1->constraints.find(id_2);//->second;
    
    if(it == node1->constraints.end()){
        return 0;
    }

    SlamConstraint *ct = it->second;

    if(ct==NULL) //this shouldnt be
        return 0;
    
    if(ct->type != type)
        return 0;
    
    return 1; 
}

int SlamGraph::hasConstraint(int id_1, int id_2){
    SlamNode *node1 = getSlamNodeFromID(id_1);
    SlamNode *node2 = getSlamNodeFromID(id_2);

    map<int, SlamConstraint *>::iterator it; 
    it = node1->constraints.find(id_2);//->second;
    
    if(it == node1->constraints.end())
        return 0;

    SlamConstraint *ct = it->second;

    if(ct==NULL) //this shouldnt be
        return 0;
    
    return 1; 
}

int SlamGraph::getConstraintID(int id_1, int id_2, int32_t type){
    SlamNode *node1 = getSlamNodeFromID(id_1);
    SlamNode *node2 = getSlamNodeFromID(id_2);

    map<int, SlamConstraint *>::iterator it; 
    it = node1->constraints.find(id_2);//->second;
    
    if(it == node1->constraints.end())
        return -1;

    SlamConstraint *ct = it->second;

    if(ct==NULL) //this shouldnt be
        return -1;
    
    if(ct->type != type)
        return -1;
    
    return ct->id; 
}

int SlamGraph::removeConstraint(int id_1, int id_2, int32_t type){
    SlamNode *node1 = getSlamNodeFromID(id_1);
    SlamNode *node2 = getSlamNodeFromID(id_2);

    map<int, SlamConstraint *>::iterator it; 
    it = node1->constraints.find(id_2);//->second;
    
    if(it == node1->constraints.end())
        return 0;

    
    SlamConstraint *ct = it->second;

    if(ct==NULL) //this shouldnt be
        return 0;

    if(ct->type != type)
        return 0;

    node1->constraints.erase(it);

    it = node2->constraints_from.find(id_1);
    node2->constraints_from.erase(it);

    it = slam_constraints.find(ct->id); 
    slam_constraints.erase(it);

    if(type == SLAM_GRAPH_EDGE_T_TYPE_LANGUAGE && ct->ct_pose2d != NULL){
        slam->remove_factor(ct->ct_pose2d);
    }
    else if(type == SLAM_GRAPH_EDGE_T_TYPE_SM_LC && ct->ct_pose2d != NULL){
        slam->remove_factor(ct->ct_pose2d);
    }

    delete ct; 

    return 0;
}

int SlamGraph::removeConstraint(int const_id){
    map<int, SlamConstraint *>::iterator it; 
    it = slam_constraints.find(const_id);
    
    if(it == slam_constraints.end())
        return 0;

    SlamConstraint *ct = it->second;

    if(ct==NULL){ //this shouldnt be
        return 0;
    }
    
    SlamNode *node1 = ct->node1;                
    int id_1 = node1->id;
    int id_2 = ct->node2->id;

    if (ct->ct_pose2d)
        slam->remove_factor(ct->ct_pose2d);

    slam_constraints.erase(it);

    it = node1->constraints.find(id_2);//->second;
    
    if(it == node1->constraints.end()){
        return 0;
    }

    node1->constraints.erase(it);
    
    it = ct->node2->constraints_from.find(id_1);
    ct->node2->constraints_from.erase(it);

    
    
    delete ct; 

    return 0;
}

int SlamGraph::hasConstraint(int const_id, int32_t *type, int32_t *status){
    map<int, SlamConstraint *>::iterator it; 
    it = slam_constraints.find(const_id);
    
    if(it == slam_constraints.end()){
        *type = -1;
        *status = -1;
        return 0;
    }

    SlamConstraint *ct = it->second;

    if(ct==NULL){ //this shouldnt be
        *type = -1;
        *status = -1;
        return 0;
    }
    
    *type = ct->type;
    *status = ct->status;
    return 1;    
}

SlamConstraint * SlamGraph::getConstraint(int const_id){
    map<int, SlamConstraint *>::iterator it; 
    it = slam_constraints.find(const_id);

    if(it == slam_constraints.end()){
        return NULL;
    }

    return it->second;    
}
