#include "Utils.hpp"
#include <algorithm>
//#include <prob-utils/prob.hpp>

bool Utils::compareID (SemVar v1, SemVar v2) { 
    if(v1.label() < v2.label())
        return true;
    return false;
}

bool Utils::compareRegionProb(regionProb p1, regionProb p2){
    if(p1.second > p2.second)
        return true;
    return false;
}

bool Utils::compareRegionPaths(RegionPath *p1, RegionPath *p2){
    if(p1->getProbability() > p2->getProbability()){
        return true;
    }
    return false;
}

bool Utils::compareRegionPathProbs(regionPathProb p1, regionPathProb p2){
    if(p1.second > p2.second){
        return true;
    }
    return false;
}

bool Utils::compareSRQuestions(SRQuestion p1, SRQuestion p2){
    if(p1.sr_result.second > p2.sr_result.second){
        return true;
    }
    return false;
}

bool Utils::compareSRProbs(spatialResults p1, spatialResults p2){
    if(p1.second > p2.second){
        return true;
    }
    return false;
}

double Utils::get_sigmoid_value(double val, double mean, double threshold, double sigma){
    double delta = fabs(val - mean);
    
    double dev = (threshold - delta) / sigma;

    double max_prob = 1 / (1 + exp(-threshold /sigma));

    return 1/ (1+ exp(-dev)) / max_prob;
}

string Utils::getSR(spatialRelation sr){
    switch(sr){
    case LEFT_OF:
        return string("left of");
        break;
    case RIGHT_OF:
        return string("right of");
        break;
    case IN_FRONT_OF:
        return string("infrontof");
        break;
    case BEHIND:
        return string("behind");
        break;
    case NEAR:
        return string("near");
        break;
    case INVALID:
        return string("invalid");
        break;
    case DOWN_FROM:
        return string("down");
        break;
    case ACROSS:
        return string("across");
        break;
    case THROUGH:
        return string("through");
        break;
    case AWAY:
        return string("away");
        break;
    default:
        return string("unknown");
        break;
    }
}

vector<spatialRelation> Utils::getSRList(){
    vector<spatialRelation> vec;
    vec.push_back(LEFT_OF);
    vec.push_back(RIGHT_OF);
    vec.push_back(IN_FRONT_OF);
    vec.push_back(BEHIND);
    vec.push_back(NEAR);
    vec.push_back(AWAY);
    vec.push_back(DOWN_FROM);
    vec.push_back(THROUGH);
    vec.push_back(ACROSS);
    
    return vec;
}

spatialRelation Utils::getSpatialRelationType(string name){
    vector<spatialRelation> sr_list = Utils::getSRList();
    for(int i=0; i < sr_list.size(); i++){
        spatialRelation sr = sr_list[i];
        if(!name.compare(getSR(sr))){
            return sr;
        }
    }
    return INVALID;
}

string Utils::createQuestion(string label, spatialRelation sr){
    string question("Is the ");
    question += label; 
    question += string(" ");
 
    switch(sr){
    case LEFT_OF:
        return question + string("on my left?");
        break;
    case RIGHT_OF:
        return question + string("on my right?");//string("right of");
        break;
    case IN_FRONT_OF:
        return question + string("in front of me?");
        break;
    case BEHIND:
        return question + string("behind me?");
        break;
    case NEAR:
        return question + string("near me?");
        break;
    default:
        return string("unknown");
        break;
    }
}

bool Utils::isSRHandled(spatialRelation sr){
    if(sr == NEAR || sr == AWAY || sr == LEFT_OF || sr == RIGHT_OF || sr == IN_FRONT_OF || sr == BEHIND){
        return true;
    }
    return false;
}

double Utils::getSRLikelihoodGeneral(spatialRelation sr, Pose2d robot_pose, Pose2d figure, Pose2d landmark, bool use_robot){
    if(use_robot){
        return Utils::getSRLikelihoodNew(figure, sr, robot_pose);
    }
    else{
        return Utils::getSRLikelihoodNew(figure, sr, landmark);
    }
}

double Utils::getSRLikelihoodNew(Pose2d figure, spatialRelation sr, Pose2d landmark){
    Pose2d delta = figure.ominus(landmark);
    double theta = atan2(delta.y(), delta.x());
    double dist = hypot(delta.x(), delta.y());

    bool v = false;

    if(sr == NEAR){
        double near_threshold = 10.0;

        double dx = near_threshold - dist;

        double prob = 1/ (1+ exp(-dx));
        if(v)
            fprintf(stderr, "Near Prob : %f - dx : %f, Distance between nodes : %f\n", prob, dx, dist);
        return prob;             
    }
    else if(sr == AWAY){
        double near_threshold = 10.0;

        double dx = near_threshold - dist;

        double prob = 1 - 1/ (1+ exp(-dx));
        if(v)
            fprintf(stderr, "Away Prob : %f - dx : %f, Distance between nodes : %f\n", prob, dx, dist);
        return prob;             
    }
    else if(sr == LEFT_OF){
        //p1 is to the left of p2 
        //get p1 in p2 co-ords and then check which quadrant it is in 

        //ideally a wrapped normal?? 
        double dist_mean = 10.0; //mean distance 
        double dist_threshold = 3.0;
        double dist_sigma = 2.0; 

        double prob_x = Utils::get_sigmoid_value(dist, dist_mean, dist_threshold, dist_sigma);
        
        double t_mean = bot_to_radians(90);
        double t_threshold = bot_to_radians(20);
        double t_sigma = bot_to_radians(40);

        double prob_t = Utils::get_sigmoid_value(theta, t_mean, t_threshold, t_sigma);
        
        double prob = prob_x * prob_t; 
        
        return prob;
        //use von mises distribution 
    }
    else if(sr == RIGHT_OF){
        //ideally a wrapped normal?? 
        double dist_mean = 10.0; //mean distance 
        double dist_threshold = 3.0;
        double dist_sigma = 2.0;

        double prob_x = Utils::get_sigmoid_value(dist, dist_mean, dist_threshold, dist_sigma);
        
        double t_mean = bot_to_radians(-90);
        double t_threshold = bot_to_radians(20);
        double t_sigma = bot_to_radians(40);
        
        double prob_t = Utils::get_sigmoid_value(theta, t_mean, t_threshold, t_sigma);

        double prob = prob_x * prob_t; 
        
        return prob;
        //use von mises distribution 
    }
    else if(sr == IN_FRONT_OF){
        //ideally a wrapped normal?? 
        double dist_mean = 10.0; //mean distance 
        double dist_threshold = 3.0;
        double dist_sigma = 2.0; 

        double prob_x = Utils::get_sigmoid_value(dist, dist_mean, dist_threshold, dist_sigma);

        double t_mean = bot_to_radians(0);
        double t_threshold = bot_to_radians(20);
        double t_sigma = bot_to_radians(40);
        
        double prob_t = Utils::get_sigmoid_value(theta, t_mean, t_threshold, t_sigma);

        double prob = prob_x * prob_t; 
        
        return prob;
        //use von mises distribution 
    }
    else if(sr == BEHIND){
        //ideally a wrapped normal?? 
        double dist_mean = 10.0; //mean distance 
        double dist_threshold = 3.0;
        double dist_sigma = 2.0;

        double prob_x = Utils::get_sigmoid_value(dist, dist_mean, dist_threshold, dist_sigma);

        double t_mean = bot_to_radians(180);
        double t_threshold = bot_to_radians(20);
        double t_sigma = bot_to_radians(40);
        
        double prob_t = Utils::get_sigmoid_value(theta, t_mean, t_threshold, t_sigma);
        
        double prob = prob_x * prob_t; 

        return prob;
        //use von mises distribution 
    }
    else{
        fprintf(stderr, "+++++++++++ Error - Unknown SR - Unable to calculate prob +++++++++++\n");
        return 0;
    }
}

//hack - spatial relations for some simple relations
double Utils::getSRLikelihood(Pose2d p1, spatialRelation sr, Pose2d p2){
    Pose2d delta = p1.ominus(p2);
    double theta = atan2(delta.y(), delta.x());
    double dist = hypot(delta.x(), delta.y());

    bool v = false;

    if(sr == NEAR){
        double near_threshold = 10.0;

        double dx = near_threshold - dist;

        double prob = 1/ (1+ exp(-dx));
        if(v)
            fprintf(stderr, "Near Prob : %f - dx : %f, Distance between nodes : %f\n", prob, dx, dist);
        return prob;             
    }
    else if(sr == LEFT_OF){
        //p1 is to the left of p2 
        //get p1 in p2 co-ords and then check which quadrant it is in 
        double sigma = 2.0;
        //for now a simple hack - put a center on the left (0,10) and see 
        Pose2d left(0, 10, 0);
        Pose2d rel_pose = left.ominus(delta);
        double left_threshold = 5.0;
        double dx = (left_threshold - hypot(rel_pose.x(), rel_pose.y()))/sigma;
        //check if its in the quadrant 
        //double k = 0.5;
        //double mean = M_PI/4;
        double prob = 1/ (1+ exp(-dx));
        if(v)
            fprintf(stderr, "Left Prob : %f - dx : %f Relative Pose : %f, %f, %f\n", prob, dx, delta.x(), delta.y(), bot_to_degrees(delta.t()));
        return prob;
        //use von mises distribution 
    }
    else if(sr == RIGHT_OF){
        //p1 is to the left of p2 
        //get p1 in p2 co-ords and then check which quadrant it is in 
        double sigma = 2.0;
        //for now a simple hack - put a center on the left (0,10) and see 
        Pose2d right(0, -10, 0);
        Pose2d rel_pose = right.ominus(delta);
        double right_threshold = 5.0;
        double dx = (right_threshold - hypot(rel_pose.x(), rel_pose.y()))/sigma;
        //check if its in the quadrant 
        //double k = 0.5;
        //double mean = M_PI/4;
        double prob = 1/ (1+ exp(-dx));
        if(v)
            fprintf(stderr, "Right Prob : %f - dx : %f, Relative Pose : %f, %f, %f\n", prob, dx, delta.x(), delta.y(), bot_to_degrees(delta.t()));
        return prob;
        //use von mises distribution 
    }
    else if(sr == IN_FRONT_OF){
        //p1 is to the left of p2 
        //get p1 in p2 co-ords and then check which quadrant it is in 
        double sigma = 2.0;
        //for now a simple hack - put a center on the left (0,10) and see 
        Pose2d front(10, 0, 0);
        Pose2d rel_pose = front.ominus(delta);
        double front_threshold = 5.0;
        double dx = (front_threshold - hypot(rel_pose.x(), rel_pose.y()))/sigma;
        //check if its in the quadrant 
        //double k = 0.5;
        //double mean = M_PI/4;
        double prob = 1/ (1+ exp(-dx));
        if(v)
            fprintf(stderr, "Front Prob : %f - dx : %f, Relative Pose : %f, %f, %f\n", prob, dx, delta.x(), delta.y(), bot_to_degrees(delta.t()));
        return prob;
        //use von mises distribution 
    }
    else if(sr == BEHIND){
        //p1 is to the left of p2 
        //get p1 in p2 co-ords and then check which quadrant it is in 
        double sigma = 2.0;
        //for now a simple hack - put a center on the left (0,10) and see 
        Pose2d behind(-10, 0, 0);
        Pose2d rel_pose = behind.ominus(delta);
        double behind_threshold = 5.0;
        double dx = (behind_threshold - hypot(rel_pose.x(), rel_pose.y()))/sigma;
        //check if its in the quadrant 
        //double k = 0.5;
        //double mean = M_PI/4;
        double prob = 1/ (1+ exp(-dx));
        if(v)
            fprintf(stderr, "Behind Prob : %f - dx : %f, Relative Pose : %f, %f, %f\n", prob, dx, delta.x(), delta.y(), bot_to_degrees(delta.t()));
        return prob;
        //use von mises distribution 
    }
}

double Utils::calculateEntropy(const vector<regionProb> &dist){
    if(dist.size() == 0){
        fprintf(stderr, "No elements in distribution\n");
        return 0;
    }

    double sum = 0;
    double p_sum = 0;
    for(int i=0; i < dist.size(); i++){
        if(dist[i].second > 0){
            p_sum += dist[i].second;
            sum += -log(dist[i].second) * dist[i].second;
        }
        // p* ln(p) for p->0 approaches 0 
    }

    //this can all be zero if all likelihoods are zero 
    //assert(fabs(p_sum -1.0) < 0.0001); //check if the distribution is normalized 
    return sum;
}

BotTrans Utils::getBotTransQuat(double pos[3], double quat[4]){
    BotTrans trans;
    trans.trans_vec[0] = pos[0];
    trans.trans_vec[1] = pos[1];
    trans.trans_vec[2] = pos[2];
    
    trans.rot_quat[0] = quat[0];
    trans.rot_quat[1] = quat[1];
    trans.rot_quat[2] = quat[2];
    trans.rot_quat[3] = quat[3];

    return trans; 
}

double Utils::getPathLength(map<SlamNode *, double> &dist_map, SlamNode *goal){
    map<SlamNode *, double>::iterator it = dist_map.find(goal);
    if(it == dist_map.end())
        return 100000;
    return it->second;
}

vector<SlamNode *> Utils::getPath(map<SlamNode*, SlamNode *> &parent_map, SlamNode *goal){
    //follow the way up to the root 
    //one of these will either not have a parent or the parent will be the node itself 
    vector<SlamNode *> path; 
    map<SlamNode*, SlamNode *>::iterator it; 
    SlamNode *parent = goal;
    path.push_back(parent);
    while(true){
        it = parent_map.find(parent);
        if(it == parent_map.end() || it->second == parent){
            break;
        }
        parent = it->second;
        path.push_back(parent);
    }
    return path;
}

//These should prob be in the Utils 
Pose2d Utils::getPoseFromBotTrans(BotTrans tf){
    double rpy[3];
    bot_quat_to_roll_pitch_yaw(tf.rot_quat, rpy);
    Pose2d pose(tf.trans_vec[0],tf.trans_vec[1],rpy[2]);
    return pose;
}

BotTrans Utils::getBotTransRPY(double pos[3], double rpy[3]){
    BotTrans trans;
    trans.trans_vec[0] = pos[0];
    trans.trans_vec[1] = pos[1];
    trans.trans_vec[2] = pos[2];
    
    bot_roll_pitch_yaw_to_quat(rpy, trans.rot_quat);

    return trans; 
}

BotTrans Utils::getBotTransFromPose(Pose2d pose){
    BotTrans trans;
    trans.trans_vec[0] = pose.x(); 
    trans.trans_vec[1] = pose.y(); 
    trans.trans_vec[2] = 0.0;

    double rpy[3] = { 0, 0, pose.t() };
    bot_roll_pitch_yaw_to_quat(rpy, trans.rot_quat);
    
    return trans; 
}

BotTrans Utils::getTransformFromSlamNodes(SlamNode *match, SlamNode *target){
    BotTrans tf_match_to_target; 
    BotTrans target_to_local = getBotTransFromPose(target->getPose());
    BotTrans match_to_local = getBotTransFromPose(match->getPose());
    
    BotTrans local_to_target = target_to_local;
    bot_trans_invert(&local_to_target);

    bot_trans_apply_trans_to(&local_to_target, &match_to_local, &tf_match_to_target);
    return tf_match_to_target;
}

double Utils::calculateCloseNodeRatio(vector<SlamNode *> &nodes, vector<SlamNode *> &nodes1, vector<nodePairDist> &close_node_pairs){
    //for each node in this region calculate the closest node
    //calculate the ratio of close node pairs to total pairs
    
    double close_threshold = 1.0;
    int close_count = 0; 
    for(size_t i=0; i < nodes.size(); i++){
        SlamNode *nd_i = nodes[i];
        SlamNode *nd_c = NULL;
        double closestDist = 10000000; 
        for(size_t j=0; j < nodes1.size(); j++){
            SlamNode *nd_j = nodes1[j]; 
            double dist = nd_i->getDistanceToNode(nd_j);
            if(closestDist > dist){
                closestDist = dist; 
                nd_c = nd_j;
            }
        }
        if(closestDist < close_threshold)
            close_count++;
        if(nd_c != NULL){
            close_node_pairs.push_back(make_pair(make_pair(nd_i, nd_c), closestDist));
        }
    }
    return close_count/ (double) nodes.size();
}

//USED - To get points from laser
smPoint* Utils::get_scan_points_from_laser(const bot_core_planar_lidar_t *laser_msg, int beam_skip,  double spatialDecimationThresh, 
                                           double maxRange, float validBeamAngles[2], int *noPoints, BotTrans *trans){
    smPoint * points = (smPoint *) calloc(laser_msg->nranges, sizeof(smPoint));
    int numValidPoints = sm_projectRangesAndDecimate(beam_skip, spatialDecimationThresh, laser_msg->ranges,
                                                         laser_msg->nranges, laser_msg->rad0, laser_msg->radstep, points, 
                                                         maxRange, validBeamAngles[0], validBeamAngles[1]);
    *noPoints = numValidPoints;
           
    if (numValidPoints < 30) {
        fprintf(stderr, "WARNING! NOT ENOUGH VALID POINTS! numValid=%d\n", numValidPoints);
        free(points);
        *noPoints = 0;
        return NULL;
    }
    else {
        points = (smPoint *) realloc(points, numValidPoints * sizeof(smPoint));//crop memory down to size
    }
    
    if(trans){
        //transform to body pose
        double pos_s[3] = {0}, pos_b[3];
        for(int i=0; i < numValidPoints; i++){
            pos_s[0] = points[i].x;
            pos_s[1] = points[i].y;
            bot_trans_apply_vec(trans, pos_s, pos_b);
            //bot_vector_affine_transform_3x4_3d (sensor_to_body, pos_s , pos_b);
            points[i].x = pos_b[0];
            points[i].y = pos_b[1];
        }
    }
    return points;
}

slam_language_label_t* Utils::getAnnotatedLanguage(map<int, slam_language_label_t *> &lang_labels, int node_id){
    map<int, slam_language_label_t *>::iterator it; 
    it = lang_labels.find(node_id);
    if(it == lang_labels.end())
        return NULL;
    return it->second;
}

/*int Utils::getObsIndFromLabel(LabelInfo *info, string label){
    //ugly hack 
    std::transform(label.begin(), label.end(), label.begin(), ::tolower);
    //label.erase(std::remove_if(label.begin(), label.end(), isspace), label.end());
    cout << "\n\n\n\n\nLower case string : " << label << endl;

    return info->getIndexForLabel(label);
    }*/

double Utils::get_prob_to_merge(double closest_dist, double alpha){    
    return 1 / (1.0 + alpha*pow(closest_dist,2));    
}

double Utils::get_prob_to_segment_from_ncut(double ncut){
    //n_cut of 0 should be highest
    //n_cut of 2 should have 0 likelihood 
    double prob = 1/(1+36 * pow(ncut,3)); //was 36 - which seems too low 
    return prob;
}

double Utils::get_prob_to_segment_from_ncut_model_1(double ncut){
    //n_cut of 0 should be highest
    //n_cut of 2 should have 0 likelihood 
    double prob = 1/(1+ 4 * pow(ncut,2)); //was 36 - which seems too low 
    return prob;
}

double Utils::rice_dist_probability (double x, void * p) {
    dist_params * params = (dist_params *)p;
    double mu = (params->mu);
    double sigma = (params->sigma);
    
    double temp = x/ pow(sigma, 2) * exp( -(pow(x,2) + pow(mu,2)) / (2 * pow(sigma,2)));
    double temp2 = temp * gsl_sf_bessel_I0(x*mu/ pow(sigma,2));
    double rtrn = temp2 / (1.0 + 0.8*pow(mu,2)); //was 0.2
    return  rtrn;
}

double Utils::get_prob_to_segment_region_size(int no_segments){
    if(no_segments == 0)
        return 0;
    return (1 - 1/(2.0 * no_segments));///fmin(1.0, (1- 1/ (1 + 0.4 * pow((double) no_nodes, 0.5)))); 
}

double  Utils::get_prob_of_split_overlap(double max_overlap){
    double a = 0.7;
    double b = 0.9;
    if(max_overlap < 0.5)
        return a;
    else
        return a + (b - a) / 0.5 * (max_overlap - 0.5);
}


bool Utils::compareDistance (nodeDist node1, nodeDist node2) { 
    if(node1.second < node2.second)
        return true;
    return false;
}

bool Utils::compareNodePairDistance (nodePairDist node1, nodePairDist node2) { 
    if(node1.second < node2.second)
        return true;
    return false;
}

bool Utils::compareRegionOverlap (regionDist node1, regionDist node2){
     if(node1.second > node2.second)
         return true;
     return false;
}

double Utils::folded_gaussian_probability_for_merging_regions(double x, void *p){
    dist_params * params = (dist_params *)p;
    double mu = (params->mu);
    double sigma = (params->sigma);

    if(x >= 0){
        double prob1 = 1/ (sigma * pow(2 * M_PI, 0.5)) * (exp (-pow((-x - mu),2)/(2 * pow(sigma,2))) + exp (-pow((x - mu),2)/(2 * pow(sigma,2))));
        double prob = prob1 * get_prob_to_merge(x,0.3);//1/ (1.0 + 0.3*pow(x,2));
        return prob; 
    }
    else{
        return 0;
    } 
}

double Utils::folded_gaussian_probability_for_dist(double x, void *p){
    dist_params * params = (dist_params *)p;
    double mu = (params->mu);
    double sigma = (params->sigma);

    if(x >= 0){
        double prob1 = 1/ (sigma * pow(2 * M_PI, 0.5)) * (exp (-pow((-x - mu),2)/(2 * pow(sigma,2))) + exp (-pow((x - mu),2)/(2 * pow(sigma,2))));
        double prob = prob1 / (1.0 + 0.4*pow(x,2));
        return prob; 
    }
    else{
        return 0;
    } 
}

double Utils::folded_gaussian_probability(double x, void *p){
    dist_params * params = (dist_params *)p;
    double mu = (params->mu);
    double sigma = (params->sigma);

    //not sure which one we should use
    double sigma1 = 0.4;

    if(x >= 0){
        double prob1 = 1/ (sigma * pow(2 * M_PI, 0.5)) * (exp (-pow((-x - mu),2)/(2 * pow(sigma,2))) + exp (-pow((x - mu),2)/(2 * pow(sigma,2))));
        //double prob = prob1 * 4 / (sigma1 * pow(2 * M_PI, 0.5)) * exp (-pow(x,2)/(2 * pow(sigma1,2))) ;
        if(x < 0.5) 
            return prob1; 
        else if(x < 1.0)
            prob1 *= (1- 2 * (x - 0.5)); 
        else 
            prob1 = 0; 
        return prob1;
    }
    else{
        return 0;
    } 
}

double Utils::get_observation_probability(double mu, double sigma){
    gsl_function F;
    dist_params params = {mu, sigma};
        
    //needs a function it can querry 
    F.function = &folded_gaussian_probability;
    F.params = &params;

    int64_t s_time = bot_timestamp_now();

    double result = 1.0;
    double abserr = 1.0;
    double epsabs = .001;
    double epsrel = .001;
    size_t limit = 20000;
    gsl_integration_workspace * workspace = gsl_integration_workspace_alloc (limit);
       
    int error = gsl_integration_qags (&F, 0, 1.0, epsabs, epsrel, limit,  workspace, &result, &abserr);

    int64_t e_time = bot_timestamp_now();

    //fprintf(stderr, "\n\n\n========== ++++++++++ Time taken for integration : %f\n", (e_time - s_time)/1.0e6);
    //fprintf(stderr, "Result : %f\n", result);
    
    gsl_integration_workspace_free (workspace);

    //i think we dont need to do this 
    double min_prob = 0.05; 
    if(result < min_prob)
        result = min_prob; 

    return result;
}

//Bianca - to fill 
int Utils::check_to_add(SlamNode *node1, SlamNode *node2, int find_ground_truth){
    gsl_set_error_handler_off();
    Pose2d p1 = node1->getPose();
    Pose2d p2 = node2->getPose();
    
    double dist = hypot(p1.x() - p2.x(), p1.y() - p2.y());
    //fprintf(stderr, "\t\t Distance: %f\n", dist);
    
    if(find_ground_truth){
      if(dist < 10)
         return 1;
      else return 0;
    }
    
    double sigma = node1->cov[0] + node1->cov[4] + node2->cov[0] + node2->cov[4];
    sigma = sigma/2;
    
    //fprintf(stderr, "\t\t Sigma: %f\n", sigma);
    
    //if (dist < 8){
       

    double threshold = 0;
    //fprintf(stderr, "\t\t Threshold: %f\n", threshold);
       
    gsl_function F;
    dist_params params = {dist, .3};
        
    F.function = &rice_dist_probability;
    F.params = &params;
              
    double x = 1.0;
    double y = 1.0;
       
    double * result = &x;
    double * abserr = &y;
    double epsabs = .001;
    double epsrel = .001;
    size_t limit = 2000000;
    gsl_integration_workspace * workspace = gsl_integration_workspace_alloc (limit);
    limit = 2000000;  
    //fprintf(stderr, "Made workspace\n"); 
       
    int error = gsl_integration_qags (&F, 0.0001, 8.0, epsabs, epsrel, limit,  workspace, result, abserr);
    //fprintf(stderr, "Integrated\n");
    if (error != 0 && dist > 20)
        threshold = 0;
    else if (error !=0)
        threshold = 1 / (pow(dist,2) + 1);
    else threshold = *result;
       

   
    gsl_integration_workspace_free (workspace);
       
       
    //this should be a function of the distance, cov etc 

    double rand_val = rand() / (double) RAND_MAX;
       
    //fprintf(stderr, "got result and random value\n");
    /*fprintf(stderr, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");
      fprintf(stderr, "\tEdge information - Node %d to Node %d\n", node1->id, node2->id);
      fprintf(stderr, "\tDistance: %f; Threshold: %f; Random value: %f\n", dist, threshold, rand_val);
      fprintf(stderr, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");
    */

    if(rand_val < threshold)
        return 1;
    return 0;
    //}
    //if(0){
    //    fprintf(stderr, "\t\t %d - %d => %f => threshold : %f - rand : %f\n",
    //            node1->id, node2->id, 
    //            dist, threshold, rand_val);
    //}

    return 0;
}

//Bianca - to fill 
int Utils::get_prob_for_edge(SlamNode *node1, SlamNode *node2, int find_ground_truth){
    gsl_set_error_handler_off();
    Pose2d p1 = node1->getPose();
    Pose2d p2 = node2->getPose();
    
    double dist = hypot(p1.x() - p2.x(), p1.y() - p2.y());
    //fprintf(stderr, "\t\t Distance: %f\n", dist);
    
    if(find_ground_truth){
      if(dist < 10)
         return 1;
      else return 0;
    }
    
    double sigma = node1->cov[0] + node1->cov[4] + node2->cov[0] + node2->cov[4];
    sigma = sigma/2;
    
    //fprintf(stderr, "\t\t Sigma: %f\n", sigma);
    
    //if (dist < 8){
       

    double threshold = 0;
    //fprintf(stderr, "\t\t Threshold: %f\n", threshold);
       
    gsl_function F;
    dist_params params = {dist, .3};
        
    F.function = &rice_dist_probability;
    F.params = &params;
              
    double x = 1.0;
    double y = 1.0;
       
    double * result = &x;
    double * abserr = &y;
    double epsabs = .001;
    double epsrel = .001;
    size_t limit = 2000000;
    gsl_integration_workspace * workspace = gsl_integration_workspace_alloc (limit);
    limit = 2000000;  
    //fprintf(stderr, "Made workspace\n"); 
       
    int error = gsl_integration_qags (&F, 0.0001, 8.0, epsabs, epsrel, limit,  workspace, result, abserr);
    //fprintf(stderr, "Integrated\n");
    if (error != 0 && dist > 20)
        threshold = 0;
    else if (error !=0)
        threshold = 1 / (pow(dist,2) + 1);
    else threshold = *result;
       

   
    gsl_integration_workspace_free (workspace);
       
       
    //this should be a function of the distance, cov etc 

    double rand_val = rand() / (double) RAND_MAX;
       
    //fprintf(stderr, "got result and random value\n");
    /*fprintf(stderr, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");
      fprintf(stderr, "\tEdge information - Node %d to Node %d\n", node1->id, node2->id);
      fprintf(stderr, "\tDistance: %f; Threshold: %f; Random value: %f\n", dist, threshold, rand_val);
      fprintf(stderr, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");
    */

    if(rand_val < threshold)
        return 1;
    return 0;
    //}
    //if(0){
    //    fprintf(stderr, "\t\t %d - %d => %f => threshold : %f - rand : %f\n",
    //            node1->id, node2->id, 
    //            dist, threshold, rand_val);
    //}

    return 0;
}

double Utils::getCosineSimilarity(map<int, double> dist_i, map<int, double> dist_j){
    map<int, double>::iterator it_i;
    map<int, double>::iterator it_j;
    
    double p_sum = 0;
    for(it_i = dist_i.begin(); it_i != dist_i.end(); it_i++){
        int class_id = it_i->first;
        double p_i = it_i->second;
        it_j = dist_j.find(class_id);
        double p_j = 0;
        if(it_j != dist_j.end())
            p_j = it_j->second;
        
        //there might be an edge liklihood per class - which should be included here 
        //maybe we can encode this in the LabelInfo as well
        double p_edge_for_class = 1.0;  //right now assumed to be the same 
        
        //fprintf(stderr, GREEN "Class : %d -> %f, %f\n" RESET_COLOR, class_id, p_i, p_j);
        p_sum += p_i * p_j * p_edge_for_class;
    }
    //fprintf(stderr, GREEN "=== Sum : %f\n" RESET_COLOR, p_sum);
    return p_sum;
}

double Utils::getLikelihoodOfEdge(map<int, double> likelihood, map<int, double> dist_i, map<int, double> dist_j){
    map<int, double>::iterator it_i;
    map<int, double>::iterator it_p;
    map<int, double>::iterator it_j;
    
    double p_sum = 0;
    for(it_i = dist_i.begin(); it_i != dist_i.end(); it_i++){
        int class_id = it_i->first;
        double p_i = it_i->second;
        it_j = dist_j.find(class_id);
        double p_j = 0;
        if(it_j != dist_j.end())
            p_j = it_j->second;

        
        it_p = likelihood.find(it_i->first);
        double p_edge_for_class = 1.0;  //right now assumed to be the same 
        if(it_p != likelihood.end()){
            p_edge_for_class = it_p->second;
        }
        //there might be an edge liklihood per class - which should be included here 
        //maybe we can encode this in the LabelInfo as well
        
        
        //fprintf(stderr, GREEN "Class : %d -> %f, %f\n" RESET_COLOR, class_id, p_i, p_j);
        p_sum += p_i * p_j * p_edge_for_class;
    }
    //fprintf(stderr, GREEN "=== Sum : %f\n" RESET_COLOR, p_sum);
    return p_sum;
}

double Utils::get_prob_to_propose_regions(SlamNode *node1, SlamNode *node2, MatrixXd cov, double dist_threshold){
    gsl_set_error_handler_off();
    Pose2d p1 = node1->getPose();
    Pose2d p2 = node2->getPose();
    
    double dist = hypot(p1.x() - p2.x(), p1.y() - p2.y());
    //fprintf(stderr, "\t\t Distance: %f\n", dist);
    
    if(dist > dist_threshold)
        return 0;
    
    MatrixXd H(1,6);
    H(0,0) = (p1.x() - p2.x()) / dist;
    H(0,1) = (p1.y() - p2.y()) / dist;
    H(0,2) = 0;
    H(0,3) = -(p1.x() - p2.x()) / dist;
    H(0,4) = -(p1.y() - p2.y()) / dist;
    H(0,5) = 0;

    MatrixXd cov_r = H * cov * H.transpose();

    //cout << "Result Distribution => Mean : " <<  dist << "Cov " << cov_r(0,0) << endl;

    //cout << "[%d] - [%d] Result Distribution (1) => Mean : " <<  dist << "Cov " << cov_r(0,0) << endl;

    double threshold = 0;
    double sigma = pow(cov_r(0,0),0.5);
  
    if(0)
        fprintf(stderr, "[%d] - [%d] => mu (%f) sigma (%f)\t", node1->id, node2->id, 
                dist, sigma);
    
    double a = fmax(0,dist - 2 * sigma);
    double b = dist + 2 * sigma;
    gsl_function F;
    dist_params params = {dist, sigma};
        
    F.function = &folded_gaussian_probability_for_merging_regions; //folded_gaussian_probability_for_dist;
    F.params = &params;
              
    double result = 1.0;
    double abserr = 1.0;
    double epsabs = .001;
    double epsrel = .001;
    size_t limit = 20000;

    gsl_integration_workspace * workspace = gsl_integration_workspace_alloc (limit);

    //fprintf(stderr, "Made workspace\n"); 
       
    int error = gsl_integration_qags (&F, a, b, epsabs, epsrel, limit,  workspace, &result, &abserr);
    
    if(0)
        fprintf(stderr, "Result : %f\n", result);

    if (error != 0 && dist > dist_threshold)
        threshold = 0;
    else if (error !=0)
        threshold = 1 / (pow(dist,2) + 1);
    else threshold = result;
   
    gsl_integration_workspace_free (workspace);
       
    return threshold;
    //if(0){
    //    fprintf(stderr, "\t\t %d - %d => %f => threshold : %f - rand : %f\n",
    //            node1->id, node2->id, 
    //            dist, threshold, rand_val);
    //}

}

double Utils::get_prob_to_propose_regions_no_cov(SlamNode *node1, SlamNode *node2, double dist_threshold){
    gsl_set_error_handler_off();
    Pose2d p1 = node1->getPose();
    Pose2d p2 = node2->getPose();
    
    double dist = hypot(p1.x() - p2.x(), p1.y() - p2.y());
    //fprintf(stderr, "\t\t Distance: %f\n", dist);
    
    if(dist > dist_threshold)
        return 0.1;

    return fmax(0.1,get_prob_to_merge(dist,0.3));
}

int Utils::check_to_add_basic(SlamNode *node1, SlamNode *node2, int find_ground_truth, MatrixXd cov){
    //fprintf(stderr, "+++++++++ Checking to add\n");

    gsl_set_error_handler_off();
    Pose2d p1 = node1->getPose();
    Pose2d p2 = node2->getPose();
    
    double dist = hypot(p1.x() - p2.x(), p1.y() - p2.y());
    //fprintf(stderr, "\t\t Distance: %f\n", dist);
    
    if(find_ground_truth){
      if(dist < 10)
         return 1;
      else return 0;
    }

    if(dist > 20)
        return 0;
    
    //double sigma = node1->cov[0] + node1->cov[4] + node2->cov[0] + node2->cov[4];

    MatrixXd H(1,6);
    H(0,0) = (p1.x() - p2.x()) / dist;
    H(0,1) = (p1.y() - p2.y()) / dist;
    H(0,2) = 0;
    H(0,3) = -(p1.x() - p2.x()) / dist;
    H(0,4) = -(p1.y() - p2.y()) / dist;
    H(0,5) = 0;

    MatrixXd cov_r = H * cov * H.transpose();

    //cout << "Result Distribution => Mean : " <<  dist << "Cov " << cov_r(0,0) << endl;

    //cout << "[%d] - [%d] Result Distribution (1) => Mean : " <<  dist << "Cov " << cov_r(0,0) << endl;

    double threshold = 0;
    double sigma = pow(cov_r(0,0),0.5);
  
    if(0)
        fprintf(stderr, "[%d] - [%d] => mu (%f) sigma (%f)\t", node1->id, node2->id, 
                dist, sigma);
    
    double a = fmax(0,dist - 2 * sigma);
    double b = dist + 2 * sigma;
    gsl_function F;
    dist_params params = {dist, sigma};
        
    F.function = &folded_gaussian_probability_for_dist;
    F.params = &params;
              
    double result = 1.0;
    double abserr = 1.0;
    double epsabs = .001;
    double epsrel = .001;
    size_t limit = 20000;

    gsl_integration_workspace * workspace = gsl_integration_workspace_alloc (limit);

    //fprintf(stderr, "Made workspace\n"); 
       
    int error = gsl_integration_qags (&F, a, b, epsabs, epsrel, limit,  workspace, &result, &abserr);
    
    if(0)
        fprintf(stderr, "Result : %f\n", result);

    if (error != 0 && dist > 20)
        threshold = 0;
    else if (error !=0)
        threshold = 1 / (pow(dist,2) + 1);
    else threshold = result;
   
    gsl_integration_workspace_free (workspace);
       
       
    //this should be a function of the distance, cov etc 

    double rand_val = rand() / (double) RAND_MAX;
       
    //fprintf(stderr, "got result and random value\n");
    //fprintf(stderr, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");
    // fprintf(stderr, "\tEdge information - Node %d to Node %d\n", node1->id, node2->id);
    //fprintf(stderr, "\tDistance: %f; Threshold: %f; Random value: %f\n", dist, threshold, rand_val);
    // fprintf(stderr, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");
    
    if(rand_val < threshold){
        //fprintf(stderr, " - Proposed\n");
        return 1;
    }
    //fprintf(stderr, " - Rejected\n");
    return 0;
    //}
    //if(0){
    //    fprintf(stderr, "\t\t %d - %d => %f => threshold : %f - rand : %f\n",
    //            node1->id, node2->id, 
    //            dist, threshold, rand_val);
    //}

}
