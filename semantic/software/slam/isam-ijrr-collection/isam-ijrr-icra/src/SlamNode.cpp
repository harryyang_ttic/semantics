#include "SlamGraph.hpp"
#include "Utils.hpp" 
using namespace Utils;
//int SlamNode::phi_last_id = PHI_ID_INCREMENT;
//int SlamNode::language_last_id = LANG_OBS_ID_INCREMENT;

#define MAX_SCAN_DISTANCE 10.0

SlamNode::SlamNode(NodeScan *_slam_pose, LabelInfo *label_info, graph_t &graph){
    id = _slam_pose->node_id;
    
    b_vertex = add_vertex(graph); 
    graph[b_vertex].id = id;

    segment = NULL;
    slam_pose = _slam_pose;
    pose2d_node = new Pose2d_Node();
    processed = 0;
    pofz = 0;
    position = -1;
    parent_supernode = -1;
    labeldist = new LabelDistribution();
    prob_used = 0;
    prob_of_scan = (double *) calloc(slam_pose->scan->numPoints, sizeof(double));
    max_scan_prob = 0; 
    prev_node = NULL;
    next_node = NULL;
    region_node = NULL;
    //do the bounding box here - and in the update do the transform 
    int no_valid_points = 0;
    for(int i=0; i<slam_pose->scan->numPoints; i++){//iterate over smpoints
        if(hypot(slam_pose->scan->points[i].x, slam_pose->scan->points[i].y) < MAX_SCAN_DISTANCE)
            no_valid_points++;
    } 

    ptr = pointlist2d_new (no_valid_points);//slam_pose->scan->numPoints);
    ptr_global = pointlist2d_new (no_valid_points);
    //do we filter these points???
    int k =0;
    for(int i=0; i<slam_pose->scan->numPoints; i++){//iterate over smpoints
        if(hypot(slam_pose->scan->points[i].x, slam_pose->scan->points[i].y) < MAX_SCAN_DISTANCE){
            ptr->points[k].x = slam_pose->scan->points[i].x; 
            ptr->points[k].y = slam_pose->scan->points[i].y; 
            k++;
        }
    } 

    //fprintf(stderr, "No points : %d / %d\n", no_valid_points, slam_pose->scan->numPoints);
    if(no_valid_points > 2){
        bounding_box_bot_frame = convexhull_graham_scan_2d(ptr);
        bounding_box_global = pointlist2d_new(bounding_box_bot_frame->npoints);
        //pointlist2d_free(ptr);
    }
    else{
        bounding_box_bot_frame = pointlist2d_new_copy(ptr);
        bounding_box_global = pointlist2d_new(bounding_box_bot_frame->npoints);
    }

    if(label_info){
        node_type = label_info->getAppearenceVar(id);//new SemVar(id, (int) label_info->classifier_label_to_index.size());
        //create the variable 
        type_obs_laser = label_info->getLaserAppearenceVar(id + NODE_LASER_TYPE_INCREMENT);//new SemVar(id + NODE_LASER_TYPE_INCREMENT, (int) label_info->classifier_label_to_index.size());
        type_obs_image = label_info->getImageAppearenceVar(id + NODE_IMAGE_TYPE_INCREMENT);//new SemVar(id + NODE_IMAGE_TYPE_INCREMENT, (int) label_info->classifier_label_to_index.size()); //change the IDs
        
        node_type_to_laser_obs = label_info->getLaserConfusionMatrix(node_type, type_obs_laser);
        node_type_to_image_obs = label_info->getImageConfusionMatrix(node_type, type_obs_image);

        //maybe this should be done the in the label_info also??
        sem_type_laser_obs = new SemFactor(*type_obs_laser);
        sem_type_image_obs = new SemFactor(*type_obs_image);
        
        for(int i=0; i < type_obs_laser->states(); i++){ 
             sem_type_laser_obs->set(i, slam_pose->getObsProbLaser(i));
        }
        sem_type_laser_obs->normalize();

        for(int i=0; i < type_obs_laser->states(); i++){
            sem_type_image_obs->set(i, slam_pose->getObsProbImage(i));
        }
        sem_type_image_obs->normalize();

        //this should be done in the node scan?? - since this won't change for each particle 
        vector<SemFactor> semantic_factors; 
        semantic_factors.push_back(*node_type_to_laser_obs);
        semantic_factors.push_back(*node_type_to_image_obs);
        semantic_factors.push_back(*sem_type_laser_obs);
        semantic_factors.push_back(*sem_type_image_obs);

        SemFactorGraph fg(semantic_factors);
        // Store the constants in a PropertySet object
        dai::PropertySet opts;

        // Set some constants
        size_t maxiter = 10000;
        dai::Real   tol = 1e-9;
        size_t verb = 0;
    
        opts.set("maxiter", maxiter);  // Maximum number of iterations
        opts.set("tol", tol);          // Tolerance for convergence
        opts.set("verbose",verb);     // Verbosity (amount of output generated)
    
        dai::BP bp(fg, opts("updates",string("SEQRND"))("logdomain",false));
        // Initialize belief propagation algorithm
        bp.init();
        // Run belief propagation algorithm
        bp.run();
        
        SemFactor app_fac = bp.belief(*node_type);
        node_appearance_factor = new SemFactor(*node_type);
        
        for(int k=0; k < node_type->states(); k++){
            dai::Real denom = app_fac[k];
            node_appearance_dist.insert(make_pair(k, denom));
            node_appearance_factor->set(k, denom);
        }
    }
    else{
        fprintf(stderr, "No Label Info given");
        exit(-1);
    }
}

SlamNode::SlamNode(SlamNode *_node, graph_t &graph){
    id = _node->slam_pose->node_id;

    b_vertex = add_vertex(graph); 
    graph[b_vertex].id = id;

    slam_pose = _node->slam_pose;
    pose2d_node = new Pose2d_Node();

    region_node = NULL;

    segment = NULL;
    processed = 0;
    pofz = 0;
    position = _node->position;
    parent_supernode = _node->parent_supernode;
    labeldist = _node->labeldist->copy();
    prob_used = 0;
    prob_of_scan = (double *) calloc(slam_pose->scan->numPoints, sizeof(double));
    max_scan_prob = 0; 

    int no_valid_points = 0;
    for(int i=0; i<slam_pose->scan->numPoints; i++){//iterate over smpoints
        if(hypot(slam_pose->scan->points[i].x, slam_pose->scan->points[i].y) < MAX_SCAN_DISTANCE)
            no_valid_points++;
    } 

    ptr = pointlist2d_new (no_valid_points);
    ptr_global = pointlist2d_new (no_valid_points);
    //do we filter these points???
    int k =0;
    for(int i=0; i<slam_pose->scan->numPoints; i++){//iterate over smpoints
        if(hypot(slam_pose->scan->points[i].x, slam_pose->scan->points[i].y) < MAX_SCAN_DISTANCE){
            ptr->points[k].x = slam_pose->scan->points[i].x; 
            ptr->points[k].y = slam_pose->scan->points[i].y; 
            k++;
        }
    } 

    bounding_box_bot_frame = convexhull_graham_scan_2d(ptr);
    bounding_box_global = pointlist2d_new(bounding_box_bot_frame->npoints);

}

void SlamNode::printID(){
    fprintf(stderr, "Slam Node : %d\n", id);
}

/*int SlamNode::getNextPhiID(){
    fprintf(stderr, "New Phi ID : %d\n", SlamNode::phi_last_id);
    return SlamNode::phi_last_id++;
}*/

double SlamNode::getDistanceToNode(SlamNode *nd){
    Pose2d delta = getPose().ominus(nd->getPose());
    return hypot(delta.x(), delta.y());
}

/*int SlamNode::getNextLangID(){
    fprintf(stderr, "New Lang ID : %d\n", SlamNode::language_last_id);
    return SlamNode::language_last_id++;
    }*/

void SlamNode::addLanguageAnnotation(LabelInfo *info, double prob_correspondance, slam_language_label_t *annotation){
    if(annotation == NULL){
        fprintf(stderr, RED "No valid annotation received\n");
        return; 
    }
    int label_id = info->getIndexForLabel(string(annotation->update));

    fprintf(stderr, GREEN "Label : %s -> ID : %d\n", annotation->update, label_id);
    addLanguageAnnotation(info, prob_correspondance, label_id);    
}

void SlamNode::addLanguageAnnotation(LabelInfo *info, double prob_correspondance, int label_id){

    if(label_id == -1){
        fprintf(stderr, "Error - Not a valid language phrase - returning\n");
        return; 
    }
    
    LanguageObservation *lang_obs = new LanguageObservation(info);
    lang_obs->setPhi(prob_correspondance);
    lang_obs->setLabelObservation(label_id);
    /*lang_obs->label_obs_node = info->getNodeLabelObsVar(getNextLangID());//new SemVar(getNextLangID(), (int) info->label_to_index.size());//NO_OF_LABELS);
    lang_obs->phi = info->getNodePhiVar(getNextPhiID());//new SemVar(getNextPhiID(), 2);//NO_OF_LABELS);
    //move these guys to the label info??
    lang_obs->label_obs_factor = new SemFactor(dai::VarSet(*lang_obs->label_obs_node));
    lang_obs->phi_obs_factor = new SemFactor(dai::VarSet(*lang_obs->phi));
    */
    /*for(int i=0; i < lang_obs->phi->states(); i++){
        if(i==0){
            lang_obs->phi_obs_factor->set(i, (1 - prob_correspondance));
        }
        else{
            lang_obs->phi_obs_factor->set(i, prob_correspondance);
        }
        }*/
    
    /*for(int i=0; i < lang_obs->label_obs_node->states(); i++){
        if(label_id == i)
            lang_obs->label_obs_factor->set(i, 1.0);
        else
            lang_obs->label_obs_factor->set(i, .0);
            }*/
    
    //cout << "Label Obs Factor : " << *lang_obs->label_obs_factor << endl; 
    //cout << "Phi Obs Factor : " << *lang_obs->phi_obs_factor << endl; 
    language_observations.push_back(lang_obs);

    updateLanguageFactors();
}

void SlamNode::updateBoundingBox(){
    BotTrans bodyToLocal;
    Pose2d pose = getPose();
    bodyToLocal.trans_vec[0] = pose.x();
    bodyToLocal.trans_vec[1] = pose.y();
    bodyToLocal.trans_vec[2] = 0.0;
    double rpyQueryBody[3] = {0.0, 0.0, pose.t()};
    bot_roll_pitch_yaw_to_quat (rpyQueryBody, bodyToLocal.rot_quat);

    double pBody[3] =  {.0}, pLocal[3];

    if(bounding_box_global == NULL){
        bounding_box_global = pointlist2d_new(bounding_box_bot_frame->npoints);
    }
    
    for(int i=0; i< bounding_box_bot_frame->npoints; i++){//iterate over smpoints
        pBody[0] = bounding_box_bot_frame->points[i].x; 
        pBody[1] = bounding_box_bot_frame->points[i].y; 
        bot_trans_apply_vec ( &bodyToLocal, pBody, pLocal);
      
        bounding_box_global->points[i].x = pLocal[0];
        bounding_box_global->points[i].y = pLocal[1];
    } 
}

void SlamNode::updateScanPoints(){
    BotTrans bodyToLocal;
    Pose2d pose = getPose();
    bodyToLocal.trans_vec[0] = pose.x();
    bodyToLocal.trans_vec[1] = pose.y();
    bodyToLocal.trans_vec[2] = 0.0;
    double rpyQueryBody[3] = {0.0, 0.0, pose.t()};
    bot_roll_pitch_yaw_to_quat (rpyQueryBody, bodyToLocal.rot_quat);

    double pBody[3] =  {.0}, pLocal[3];

    if(ptr_global == NULL){
        ptr_global = pointlist2d_new(ptr->npoints);
    }
    
    for(int i=0; i< ptr->npoints; i++){//iterate over smpoints
        pBody[0] = ptr->points[i].x; 
        pBody[1] = ptr->points[i].y; 
        bot_trans_apply_vec ( &bodyToLocal, pBody, pLocal);
      
        ptr_global->points[i].x = pLocal[0];
        ptr_global->points[i].y = pLocal[1];
    } 
}

void SlamNode::updateLanguageFactors(){
    for(int i=0; i < language_observations.size(); i++){
        LanguageObservation *lang_obs = language_observations[i];
        lang_obs->updateRegionFactor(region_node->label_info, region_node);
        /*if(lang_obs->label_phi_region)
            delete lang_obs->label_phi_region;
            lang_obs->label_phi_region = region_node->label_info->getRegionLabelFactor(lang_obs->label_obs_node, lang_obs->phi, region_node->region_label);*/
    }
}

void SlamNode::updateRegion(RegionNode *region){
    region_node = region;
    
    //update the region language factors - if there were any 
    //when the node is created - the language observations are not set - so this won't have happened 
    updateLanguageFactors();

    if(segment) 
        segment->region = region;
}
RegionNode *SlamNode::getRegion(){
    return region_node;
}

SlamNode::~SlamNode(){
    delete pose2d_node;
    delete labeldist;
    map<int, SlamConstraint *>::iterator s_it;
    free(prob_of_scan);
    if(bounding_box_bot_frame != NULL){
        pointlist2d_free(bounding_box_bot_frame);
    }
    if(ptr != NULL){
        pointlist2d_free(ptr);
    }
    if(ptr_global != NULL){
        pointlist2d_free(ptr_global);
    }
    if(bounding_box_global != NULL){
        pointlist2d_free(bounding_box_global);
    }
}

void SlamNode::resetProbability(){
    max_scan_prob = 0; 
    memset(prob_of_scan, 0, slam_pose->scan->numPoints * sizeof(double));
}

Pose2d SlamNode::getPose(){
    return pose2d_node->value();
}
