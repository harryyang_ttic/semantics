cmake_minimum_required(VERSION 2.6.0)

# pull in the pods macros. See cmake/pods.cmake for documentation
set(POD_NAME isam-region)
include(cmake/pods.cmake)


#find_package(PCL 1.2 REQUIRED)
#include_directories(
#  ${PCL_COMMON_INCLUDE_DIRS}
#)

# create an executable, and make it public
add_subdirectory(src)
