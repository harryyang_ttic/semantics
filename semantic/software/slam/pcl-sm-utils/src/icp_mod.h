/*
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2010, Willow Garage, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of Willow Garage, Inc. nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id: icp.h 4367 2012-02-10 01:31:51Z rusu $
 *
 */

#ifndef PCL_ICP_MOD_H_
#define PCL_ICP_MOD_H_

// PCL includes
#include <pcl/sample_consensus/ransac.h>
#include <pcl/sample_consensus/sac_model_registration.h>
#include <pcl/registration/registration.h>
#include <pcl/registration/icp.h>

#include <pcl/registration/transformation_estimation_svd.h>

namespace pcl
{
    /** \brief @b IterativeClosestPoint provides a base implementation of the Iterative Closest Point algorithm. 
     * The transformation is estimated based on Singular Value Decomposition (SVD).
     *
     * The algorithm has several termination criteria:
     *
     * <ol>
     * <li>Number of iterations has reached the maximum user imposed number of iterations (via \ref setMaximumIterations)</li>
     * <li>The epsilon (difference) between the previous transformation and the current estimated transformation is smaller than an user imposed value (via \ref setTransformationEpsilon)</li>
     * <li>The sum of Euclidean squared errors is smaller than a user defined threshold (via \ref setEuclideanFitnessEpsilon)</li>
     * </ol>
     *
     *
     * Usage example:
     * \code
     * IterativeClosestPoint<PointXYZ, PointXYZ> icp;
     * // Set the input source and target
     * icp.setInputCloud (cloud_source);
     * icp.setInputTarget (cloud_target);
     *
     * // Set the max correspondence distance to 5cm (e.g., correspondences with higher distances will be ignored)
     * icp.setMaxCorrespondenceDistance (0.05);
     * // Set the maximum number of iterations (criterion 1)
     * icp.setMaximumIterations (50);
     * // Set the transformation epsilon (criterion 2)
     * icp.setTransformationEpsilon (1e-8);
     * // Set the euclidean distance difference epsilon (criterion 3)
     * icp.setEuclideanFitnessEpsilon (1);
     *
     * // Perform the alignment
     * icp.align (cloud_source_registered);
     *
     * // Obtain the transformation that aligned cloud_source to cloud_source_registered
     * Eigen::Matrix4f transformation = icp.getFinalTransformation ();
     * \endcode
     *
     * \author Radu Bogdan Rusu, Michael Dixon
     * \ingroup registration
     */
    template <typename PointSource, typename PointTarget>
        class IterativeClosestPointMod : public IterativeClosestPoint<PointSource, PointTarget>
    {
        typedef typename Registration<PointSource, PointTarget>::PointCloudSource PointCloudSource;
        typedef typename PointCloudSource::Ptr PointCloudSourcePtr;
        typedef typename PointCloudSource::ConstPtr PointCloudSourceConstPtr;

        typedef typename Registration<PointSource, PointTarget>::PointCloudTarget PointCloudTarget;

        typedef PointIndices::Ptr PointIndicesPtr;
        typedef PointIndices::ConstPtr PointIndicesConstPtr;

    public:
        /** \brief Empty constructor. */
        IterativeClosestPointMod () 
        {
            reg_name_ = "IterativeClosestPointMod";
            transformation_estimation_.reset (new pcl::registration::TransformationEstimationSVD<PointSource, PointTarget>);
        };

        inline double 
            getFitnessScoreWithCount (double max_range = std::numeric_limits<double>::max (), int *matches = NULL);

        /** \brief Rigid transformation computation method  with initial guess.
         * \param output the transformed input point cloud dataset using the rigid transformation found
         * \param guess the initial guess of the transformation to compute
         */
    protected:
        void 
            computeTransformation (PointCloudSource &output, const Eigen::Matrix4f &guess);

        using Registration<PointSource, PointTarget>::reg_name_;
        using Registration<PointSource, PointTarget>::getClassName;
        using Registration<PointSource, PointTarget>::input_;
        using Registration<PointSource, PointTarget>::indices_;
        using Registration<PointSource, PointTarget>::target_;
        using Registration<PointSource, PointTarget>::tree_;
        using Registration<PointSource, PointTarget>::nr_iterations_;
        using Registration<PointSource, PointTarget>::max_iterations_;
        using Registration<PointSource, PointTarget>::ransac_iterations_;
        using Registration<PointSource, PointTarget>::previous_transformation_;
        using Registration<PointSource, PointTarget>::final_transformation_;
        using Registration<PointSource, PointTarget>::transformation_;
        using Registration<PointSource, PointTarget>::transformation_epsilon_;
        using Registration<PointSource, PointTarget>::converged_;
        using Registration<PointSource, PointTarget>::corr_dist_threshold_;
        using Registration<PointSource, PointTarget>::inlier_threshold_;
        using Registration<PointSource, PointTarget>::min_number_correspondences_;
        using Registration<PointSource, PointTarget>::update_visualizer_;
        using Registration<PointSource, PointTarget>::correspondence_distances_;
        using Registration<PointSource, PointTarget>::euclidean_fitness_epsilon_;
        using Registration<PointSource, PointTarget>::transformation_estimation_;
    };
}

#include <boost/unordered_map.hpp>

//////////////////////////////////////////////////////////////////////////////////////////////
template <typename PointSource, typename PointTarget> inline double
pcl::IterativeClosestPointMod<PointSource, PointTarget>::getFitnessScoreWithCount (double max_range, int *matches)
{
  double fitness_score = 0.0;
  fprintf(stderr, "++++ Called modified function ++++ \n");

  PointCloudSource input_transformed;
  //Eigen::Matrix4f Ti = Eigen::Matrix4f::Identity ();
  //Ti = transformation_;

  //transformPointCloud (*input_, input_transformed, Ti);

  transformPointCloud (*input_, input_transformed, final_transformation_);

  //std::cout << "Transform : \n" << Ti << std::endl;
  std::vector<int> nn_indices (1);
  std::vector<float> nn_dists (1);

  fprintf(stderr, "\n");
  // For each point in the source dataset
  int nr = 0;
  for (size_t i = 0; i < input_transformed.points.size (); ++i)
  {
    Eigen::Vector4f p1 = Eigen::Vector4f (input_transformed.points[i].x,
                                          input_transformed.points[i].y,
                                          input_transformed.points[i].z, 0);
    // Find its nearest neighbor in the target
    tree_->nearestKSearch (input_transformed.points[i], 1, nn_indices, nn_dists);
    
    // Deal with occlusions (incomplete targets)
    //fprintf(stderr, "%d - %f", nn_indices[0], nn_dists[0]);
    
    if (nn_dists[0] > max_range){
        //fprintf(stderr, "\tFailed\n");
        continue;
    }
    
    //fprintf(stderr, "\t Succeed\n");

    Eigen::Vector4f p2 = Eigen::Vector4f (target_->points[nn_indices[0]].x,
                                          target_->points[nn_indices[0]].y,
                                          target_->points[nn_indices[0]].z, 0);
    // Calculate the fitness score
    fitness_score += fabs ((p1-p2).squaredNorm ());
    nr++;
  }

  if(matches != NULL){
      *matches = nr;
  }

  fprintf(stderr, "No of matches : %d\n", nr);
  

  if (nr > 0)
    return (fitness_score / nr);
  else
    return (std::numeric_limits<double>::max ());
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template <typename PointSource, typename PointTarget> void
pcl::IterativeClosestPointMod<PointSource, PointTarget>::computeTransformation (PointCloudSource &output, const Eigen::Matrix4f &guess)
{
    //fprintf(stderr, "++++ Called modified Convergence function ++++ \n");
    // Allocate enough space to hold the results
    std::vector<int> nn_indices (1);
    std::vector<float> nn_dists (1);

    // Point cloud containing the correspondences of each point in <input, indices>
    PointCloudTarget input_corresp;
    input_corresp.points.resize (indices_->size ());

    nr_iterations_ = 0;
    converged_ = false;
    double dist_threshold = corr_dist_threshold_ * corr_dist_threshold_;

    //corr_dist_threshold_ -= 0.4;

    // If the guessed transformation is non identity
    if (guess != Eigen::Matrix4f::Identity ())
    {
        // Initialise final transformation to the guessed one
        final_transformation_ = guess;
        // Apply guessed transformation prior to search for neighbours
        transformPointCloud (output, output, guess);
    }

    // Resize the vector of distances between correspondences 
    std::vector<float> previous_correspondence_distances (indices_->size ());
    correspondence_distances_.resize (indices_->size ());

    //fprintf(stderr, "\n++++ Min no of correspondance : %d\n", min_number_correspondences_);

    while (!converged_)           // repeat until convergence
    {
        // Save the previously estimated transformation
        previous_transformation_ = transformation_;
        // And the previous set of distances
        previous_correspondence_distances = correspondence_distances_;

        int cnt = 0;
        std::vector<int> source_indices (indices_->size ());
        std::vector<int> target_indices (indices_->size ());

        // Iterating over the entire index vector and  find all correspondences
        for (size_t idx = 0; idx < indices_->size (); ++idx)
        {
            if (!this->searchForNeighbors (output, (*indices_)[idx], nn_indices, nn_dists))
            {                
                PCL_ERROR ("[pcl::%s::computeTransformation] Unable to find a nearest neighbor in the target dataset for point %d in the source!\n", getClassName ().c_str (), (*indices_)[idx]);
                return;
            }

            // Check if the distance to the nearest neighbor is smaller than the user imposed threshold
            if (nn_dists[0] < dist_threshold)
            {
                source_indices[cnt] = (*indices_)[idx];
                target_indices[cnt] = nn_indices[0];
                cnt++;
            }

            // Save the nn_dists[0] to a global vector of distances
            correspondence_distances_[(*indices_)[idx]] = std::min (nn_dists[0], (float)dist_threshold);
        }

        //fprintf(stderr, "Inlier threshold : %f\n", inlier_threshold_);
        
        int basic_cnt  = cnt;

        if (cnt < min_number_correspondences_)
        {
            //fprintf(stderr, "Current Corresponding count : %d  < %d\n", cnt, min_number_correspondences_);

            PCL_ERROR ("[pcl::%s::computeTransformation] Not enough correspondences found. Relax your threshold parameters.\n", getClassName ().c_str ());
            converged_ = false;
            return;
        }

        // Resize to the actual number of valid correspondences
        source_indices.resize (cnt); target_indices.resize (cnt);

        std::vector<int> source_indices_good;
        std::vector<int> target_indices_good;
        {
            // From the set of correspondences found, attempt to remove outliers
            // Create the registration model
            typedef typename SampleConsensusModelRegistration<PointSource>::Ptr SampleConsensusModelRegistrationPtr;
            SampleConsensusModelRegistrationPtr model;
            model.reset (new SampleConsensusModelRegistration<PointSource> (output.makeShared (), source_indices));
            // Pass the target_indices
            model->setInputTarget (target_, target_indices);
            // Create a RANSAC model
            RandomSampleConsensus<PointSource> sac (model, inlier_threshold_);
            sac.setMaxIterations (ransac_iterations_);

            // Compute the set of inliers
            if (!sac.computeModel ())
            {
                source_indices_good = source_indices;
                target_indices_good = target_indices;
            }
            else
            {
                std::vector<int> inliers;
                // Get the inliers
                sac.getInliers (inliers);
                source_indices_good.resize (inliers.size ());
                target_indices_good.resize (inliers.size ());

                boost::unordered_map<int, int> source_to_target;
                for (unsigned int i = 0; i < source_indices.size(); ++i)
                    source_to_target[source_indices[i]] = target_indices[i];

                // Copy just the inliers
                std::copy(inliers.begin(), inliers.end(), source_indices_good.begin());
                for (size_t i = 0; i < inliers.size (); ++i)
                    target_indices_good[i] = source_to_target[inliers[i]];
            }
        }

        // Check whether we have enough correspondences
        cnt = (int)source_indices_good.size ();
        if (cnt < min_number_correspondences_)
        {
            //fprintf(stderr, "Current Good Corresponding count : %d  < %d\n", cnt, min_number_correspondences_);

            PCL_ERROR ("[pcl::%s::computeTransformation] Not enough correspondences found. Relax your threshold parameters.\n", getClassName ().c_str ());
            converged_ = false;
            return;
        }

        //PCL_ERROR ("[pcl::%s::computeTransformation] Basic : %d Number of correspondences %d [%f%%] out of %lu points [100.0%%], RANSAC rejected: %lu [%f%%].\n", getClassName ().c_str (), basic_cnt, cnt, (cnt * 100.0) / indices_->size (), (unsigned long)indices_->size (), (unsigned long)source_indices.size () - cnt, (source_indices.size () - cnt) * 100.0 / source_indices.size ());
  
        // Estimate the transform
        //rigid_transformation_estimation_(output, source_indices_good, *target_, target_indices_good, transformation_);
        transformation_estimation_->estimateRigidTransformation (output, source_indices_good, *target_, target_indices_good, transformation_);

        // Tranform the data
        transformPointCloud (output, output, transformation_);

        // Obtain the final transformation    
        final_transformation_ = transformation_ * final_transformation_;

        nr_iterations_++;

        // Update the vizualization of icp convergence
        if (update_visualizer_ != 0)
            update_visualizer_(output, source_indices_good, *target_, target_indices_good );

        // Various/Different convergence termination criteria
        // 1. Number of iterations has reached the maximum user imposed number of iterations (via 
        //    setMaximumIterations)
        // 2. The epsilon (difference) between the previous transformation and the current estimated transformation 
        //    is smaller than an user imposed value (via setTransformationEpsilon)
        // 3. The sum of Euclidean squared errors is smaller than a user defined threshold (via 
        //    setEuclideanFitnessEpsilon)

        if (nr_iterations_ >= max_iterations_ ||
            fabs ((transformation_ - previous_transformation_).sum ()) < transformation_epsilon_ ||
            //this one might be changed - to consider only the relavent distances???
            fabs (this->getFitnessScore (correspondence_distances_, previous_correspondence_distances)) <= euclidean_fitness_epsilon_
            )
        {
            /*fprintf(stderr, "Converged\n");

            fprintf(stderr, "Curr Iteration : %d - Max : %d\n", nr_iterations_, max_iterations_);

            fprintf(stderr, "Transform epsilon : %f => Target : %f\n", fabs ((transformation_ - previous_transformation_).sum ()), 
                    transformation_epsilon_);
            fprintf(stderr, "Fitness score decrease : %f => Target : %f\n", fabs (this->getFitnessScore (correspondence_distances_, previous_correspondence_distances)), 
            euclidean_fitness_epsilon_);*/
            

            converged_ = true;
            PCL_DEBUG ("[pcl::%s::computeTransformation] Convergence reached. Number of iterations: %d out of %d. Transformation difference: %f\n",
                       getClassName ().c_str (), nr_iterations_, max_iterations_, fabs ((transformation_ - previous_transformation_).sum ()));

            PCL_DEBUG ("nr_iterations_ (%d) >= max_iterations_ (%d)\n", nr_iterations_, max_iterations_);
            PCL_DEBUG ("fabs ((transformation_ - previous_transformation_).sum ()) (%f) < transformation_epsilon_ (%f)\n",
                       fabs ((transformation_ - previous_transformation_).sum ()), transformation_epsilon_);
            PCL_DEBUG ("fabs (getFitnessScore (correspondence_distances_, previous_correspondence_distances)) (%f) <= euclidean_fitness_epsilon_ (%f)\n",
                       fabs (this->getFitnessScore (correspondence_distances_, previous_correspondence_distances)),
                       euclidean_fitness_epsilon_);

        }
    }
}
//#include <pcl/registration/impl/icp.hpp>
//#include "icp_mod.cpp"

#endif  //#ifndef PCL_ICP_H_
