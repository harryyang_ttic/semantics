#ifndef PCL_SM_UTIL_H
#define PCL_SM_UTIL_H

#include <iostream>
//#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/registration/icp.h>
#include <pcl/registration/icp_nl.h>
#include <pcl/registration/transformation_estimation_lm.h>
#include <pcl/registration/warp_point_rigid_3d.h>
#include "pcl/common/pca.h"
//#include <point_types/point_types.h>
#include <bot_core/bot_core.h>
#include <lcm/lcm.h>
#include <geom_utils/geometry.h>
#include <bot_lcmgl_client/lcmgl.h>
#include <GL/gl.h>
/*#ifdef __cplusplus
extern "C" {
#endif*/

typedef struct _icp_result_t{
    double angle;
    //Eigen::Matrix4f Tf;
    BotTrans Tf; 
    double score; 
    int n_match;
    int converged; 
} icp_result_t;

//int doPCLScanMatch(pointlist2d_t *target, pointlist2d_t *match, double transform[12], int *matches);

int doPCLScanMatch(pointlist2d_t *target, pointlist2d_t *match, double transform[12], 
                   double *fitness_score, int *matches, double max_corr_dist);
//pointlist2d_t * doPCLScanMatchPointsMod(pointlist2d_t *target, pointlist2d_t *match, double transform[12]);

pointlist2d_t * doPCLScanMatchPointsOrig(pointlist2d_t *target, pointlist2d_t *match, double transform[12], 
                                         double *f_score, 
                                         int *matches, 
                                         double max_corr_dist);

//get the transformed points out as well 
pointlist2d_t * doPCLScanMatchPoints(pointlist2d_t *target, pointlist2d_t *match, 
                                     double transform[12], double *fitness_score, int *matches, 
                                     bot_lcmgl_t *lcmgl_target, 
                                     bot_lcmgl_t *lcmgl_match, 
                                     bot_lcmgl_t *lcmgl_result, 
                                     double max_corr_dist);

pointlist2d_t * doPCLScanMatchPointsWide(pointlist2d_t *target, pointlist2d_t *match, double transform[12], double *fitness_score, int *matches, double max_corr_dist); 

//int doPCLScanMatchIncremental(pointlist2d_t *target, pointlist2d_t *match, double transform[12]);

//int doPCLScanMatchNL(pointlist2d_t *target, pointlist2d_t *match, double transform[12]); 

pointlist2d_t* doPCLScanMatchWithTransform(pointlist2d_t *target, pointlist2d_t *match, double transform[12]); 
/*#ifdef __cplusplus
}
#endif*/

#endif
