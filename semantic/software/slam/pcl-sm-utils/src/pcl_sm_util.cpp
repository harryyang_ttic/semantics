#include "pcl_sm_util.hpp"
#include "icp_mod.h"

int doPCLScanMatch(pointlist2d_t *target, pointlist2d_t *match, double transform[12], 
                   double *f_score, int *matches, double max_corr_dist = 0.4){
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_target (new pcl::PointCloud<pcl::PointXYZ>); //target scan 
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_match (new pcl::PointCloud<pcl::PointXYZ>); //the one that should be transformed to match the target scan 
    
    cloud_target->width    = target->npoints; 
    cloud_target->height   = 1;
    cloud_target->is_dense = false;
    cloud_target->points.resize (cloud_target->width * cloud_target->height);
    for (size_t i = 0; i < cloud_target->points.size (); ++i)
        {
            cloud_target->points[i].x = target->points[i].x;
            cloud_target->points[i].y = target->points[i].y;
            cloud_target->points[i].z = 0;
        }

    cloud_match->width    = match->npoints;
    cloud_match->height   = 1;
    cloud_match->is_dense = false;
    cloud_match->points.resize (cloud_match->width * cloud_match->height);
    for (size_t i = 0; i < cloud_match->points.size (); ++i)
        {
            cloud_match->points[i].x = match->points[i].x;
            cloud_match->points[i].y = match->points[i].y;
            cloud_match->points[i].z = 0;
        }

    pcl::IterativeClosestPointMod<pcl::PointXYZ, pcl::PointXYZ> icp;
    
    //double rans = 0.5; //was 0.05
    //double dist = 5.0;

    std::cout << "Outlier rejection threshold : " << icp.getRANSACOutlierRejectionThreshold () << " Max Corresponding distance : " << icp.getMaxCorrespondenceDistance () << std::endl;

    //too small and this doesn't converge - i think because it doesnt have a good signal as to which way to turn 
    icp.setMaxCorrespondenceDistance (10.0);

    icp.setRANSACIterations(10);
    // Set the maximum number of iterations (criterion 1)
    icp.setMaximumIterations (20);
    // Set the transformation epsilon (criterion 2)
    //icp.setTransformationEpsilon (1);
    // Set the euclidean distance difference epsilon (criterion 3)
    //icp.setEuclideanFitnessEpsilon (1);
    //icp.setMaxCorrespondenceDistance (dist);
    //icp.setRANSACOutlierRejectionThreshold (rans);

    Eigen::Matrix4f Ti = Eigen::Matrix4f::Identity (); 
    icp.setInputTarget(cloud_target);
    icp.setInputCloud(cloud_match);
    pcl::PointCloud<pcl::PointXYZ>::Ptr orig_match = cloud_match;;
    pcl::PointCloud<pcl::PointXYZ>::Ptr final = cloud_match;
    //call icp
    icp.align(*final);
    Ti = icp.getFinalTransformation () * Ti;
    
    int nr = 0;
    double max_dist = max_corr_dist;

    double fitness_score = icp.getFitnessScoreWithCount(max_dist, &nr);
    *f_score = fitness_score;
    *matches = nr;

    std::cout << "Rough iteration has converged:" << icp.hasConverged() << " score: " <<
         fitness_score << std::endl;
    
    fprintf(stderr, "Found Matches : %d out of : %d\n", nr , (int) final->points.size ()); 

    Eigen::Matrix4f tf = Ti;

    for(int i=0; i < 3; i++){
        fprintf(stderr, "\n");
        for(int j=0; j<4; j++){
            transform[i*4 + j] = tf(i,j);
            fprintf(stderr, "%f\t", transform[i*4 + j]);
        }
    }

    double theta = atan2(-transform[1], transform[0]);
    fprintf(stderr, "\nRotation : %f = Deg : %f\n", theta, bot_to_degrees(theta)); 
    
    //if overlap is less than 50% of if the count is less than 3 * no of scans (assumed to be 250 points) 

    int valid_overlap = 0;
    
    if(nr / (double) match->npoints > 0.5  && nr > 3 * 250)
        valid_overlap = 1;

    if( icp.hasConverged()  == 0){// || fitness_score > 0.05 || valid_overlap == 0){
        cloud_target->points.resize(0);
        cloud_match->points.resize(0);
        orig_match->points.resize(0);
        return 0; 
    }

    std::cout << Ti << std::endl;//icp.getFinalTransformation() << std::endl;

    /*pointlist2d_t *result = pointlist2d_new(final->points.size());

    for (size_t i = 0; i < final->points.size (); ++i){
        result->points[i].x = final->points[i].x;
        result->points[i].y = final->points[i].y;
        }*/
    
    cloud_target->points.resize(0);
    cloud_match->points.resize(0);
    orig_match->points.resize(0);
    
    return 1;
}


pointlist2d_t * doPCLScanMatchPoints(pointlist2d_t *target, pointlist2d_t *match, double transform[12], 
                                     double *f_score, 
                                     int *matches, bot_lcmgl_t *lcmgl_target, 
                                     bot_lcmgl_t *lcmgl_match, 
                                     bot_lcmgl_t *lcmgl_result, 
                                     double max_corr_dist = 0.4){

    int use_pca = 1;
    //use PCA to align the points (rotate as well - as move)

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_target (new pcl::PointCloud<pcl::PointXYZ>); //target scan 
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_match (new pcl::PointCloud<pcl::PointXYZ>); //the one that should be transformed to match the target scan 

    //align the points better???

    //find the center of the second point cloud and align them properly??
    
    cloud_target->width    = target->npoints; 
    cloud_target->height   = 1;
    cloud_target->is_dense = false;
    cloud_target->points.resize (cloud_target->width * cloud_target->height);

    double mx_t = 0, my_t = 0;
    for (size_t i = 0; i < cloud_target->points.size (); ++i)
        {
            mx_t += target->points[i].x;
            my_t += target->points[i].y;
        }

    mx_t /= (double) cloud_target->points.size ();
    my_t /= (double) cloud_target->points.size ();

    for (size_t i = 0; i < target->npoints; ++i){            
        cloud_target->points[i].x = target->points[i].x;
        cloud_target->points[i].y = target->points[i].y;
        cloud_target->points[i].z = 0;
    }

    // fprintf(stderr, "Tf : %f, %f\n", mx_t, my_t);

    pcl::PCA<pcl::PointXYZ>pca;//(segment_points);
    pca.setInputCloud(cloud_target->makeShared());
    
    Eigen::Vector4f xyz = pca.getMean();
    Eigen::MatrixXf eigen_vectors = pca.getEigenVectors();
    Eigen::VectorXf eigen_values = pca.getEigenValues();
    std::cout << "PCA Mean : " << xyz << std::endl;
    //angle 
    double target_tf_angle = atan2(eigen_vectors(1,0), eigen_vectors(0,0));//atan2(eigen_vectors(0,1), eigen_vectors(0,0));
    //fprintf(stderr, "Eigen Transform angle : %f\n", bot_to_degrees(target_tf_angle));
    //std::cout << "Eigen Vectors : " << eigen_vectors << std::endl;
    //std::cout << "Eigen Values : " << eigen_values << std::endl;


    BotTrans target_to_tf_target1;
    
    target_to_tf_target1.trans_vec[0] = -mx_t;
    target_to_tf_target1.trans_vec[1] = -my_t;
    target_to_tf_target1.trans_vec[2] = 0;

    double rpy_target_to_tf_target1[3] = {.0,.0, 0};
    bot_roll_pitch_yaw_to_quat (rpy_target_to_tf_target1,  target_to_tf_target1.rot_quat);   

    BotTrans tf_target1_to_tf_target;
    
    tf_target1_to_tf_target.trans_vec[0] = 0;
    tf_target1_to_tf_target.trans_vec[1] = 0;
    tf_target1_to_tf_target.trans_vec[2] = 0;

    double rpy_target_to_tf_target[3] = {.0,.0, 0};
    if(use_pca){
        rpy_target_to_tf_target[2] = -target_tf_angle;
    }
    bot_roll_pitch_yaw_to_quat (rpy_target_to_tf_target,  tf_target1_to_tf_target.rot_quat);   

    BotTrans target_to_tf_target;
     
    bot_trans_apply_trans_to(&tf_target1_to_tf_target, &target_to_tf_target1, &target_to_tf_target);

    double pos_1[3] = {0}, trans_pos_1[3];
    for (size_t i = 0; i < target->npoints; ++i)
        {
            pos_1[0] = target->points[i].x;
            pos_1[1] = target->points[i].y;
            bot_trans_apply_vec(&target_to_tf_target, pos_1, trans_pos_1);
            
            cloud_target->points[i].x = trans_pos_1[0];
            cloud_target->points[i].y = trans_pos_1[1];
            cloud_target->points[i].z = 0;
        }

    cloud_match->width    = match->npoints;
    cloud_match->height   = 1;
    cloud_match->is_dense = false;
    cloud_match->points.resize (cloud_match->width * cloud_match->height);

    

    double mx_m = 0, my_m = 0;
    //maybe we can just do the alignment here ?? 
    for (size_t i = 0; i < cloud_match->points.size (); ++i){
        mx_m += match->points[i].x;
        my_m += match->points[i].y;
    }

    mx_m /= (double) cloud_match->points.size ();
    my_m /= (double) cloud_match->points.size ();

    //i think we need to bring them both to zero and then rotate 
    
    double dx = 0;// - mx_m;//mx_t - mx_m;
    double dy = 0;//- my_m; //mx_t - my_m;
    double w = 0;
    
    pcl::IterativeClosestPointMod<pcl::PointXYZ, pcl::PointXYZ> icp;
    
    pcl::PointCloud<pcl::PointXYZ>::Ptr orig_match = cloud_match;;
    pcl::PointCloud<pcl::PointXYZ> final; // = cloud_match;

    int draw_act = 1;

    //add a way to decide which is the best solution - and then finally - draw that solution 

    int angle_size = 8;
    std::vector<icp_result_t> results;

    int nr = 0;
    double max_dist = max_corr_dist;

    for (size_t i = 0; i < match->npoints; ++i){
        cloud_match->points[i].x = match->points[i].x;
        cloud_match->points[i].y = match->points[i].y;
        cloud_match->points[i].z = 0;
    }

    pcl::PCA<pcl::PointXYZ>pca1;//(segment_points);
    pca1.setInputCloud(cloud_match->makeShared());
    
    Eigen::Vector4f xyz_m = pca1.getMean();
    Eigen::MatrixXf eigen_vectors_m = pca1.getEigenVectors();
    Eigen::VectorXf eigen_values_m = pca1.getEigenValues();
        
    double match_tf_angle = atan2(eigen_vectors_m(1,0), eigen_vectors_m(0,0));
    //fprintf(stderr, "Match Eigen Transform angle : %f\n", bot_to_degrees(match_tf_angle));
    //std::cout << "PCA Mean : " << xyz_m << std::endl;
    //std::cout << "Eigen Vectors : " << eigen_vectors_m << std::endl;
    //std::cout << "Eigen Values : " << eigen_values_m << std::endl;

    BotTrans match_to_tf_match_m;
    
    match_to_tf_match_m.trans_vec[0] = -mx_m;
    match_to_tf_match_m.trans_vec[1] = -my_m;
    match_to_tf_match_m.trans_vec[2] = 0;

    double rpy_match_to_tf_match_m[3] = {.0,.0,.0};
    bot_roll_pitch_yaw_to_quat (rpy_match_to_tf_match_m,  match_to_tf_match_m.rot_quat);

    BotTrans tf_match_m_to_tf_match_1;
    
    tf_match_m_to_tf_match_1.trans_vec[0] = 0;
    tf_match_m_to_tf_match_1.trans_vec[1] = 0;
    tf_match_m_to_tf_match_1.trans_vec[2] = 0;

    double rpy_match_to_tf_match_1[3] = {.0,.0, 0};
    if(use_pca){
        rpy_match_to_tf_match_1[2] = -match_tf_angle;
    }

    bot_roll_pitch_yaw_to_quat (rpy_match_to_tf_match_1,  tf_match_m_to_tf_match_1.rot_quat);

    BotTrans match_to_tf_match_1;

    bot_trans_apply_trans_to(&tf_match_m_to_tf_match_1, &match_to_tf_match_m, &match_to_tf_match_1);

    if(lcmgl_target){
        if(draw_act){
            bot_lcmgl_color3f(lcmgl_target, 0, .0, .5);
            bot_lcmgl_point_size(lcmgl_target, 2);
            bot_lcmgl_begin(lcmgl_target, GL_POINTS);
            
            for (size_t i = 0; i < target->npoints; ++i){
                bot_lcmgl_vertex3f(lcmgl_target, target->points[i].x, target->points[i].y, 0);
            }
            
            bot_lcmgl_end(lcmgl_target);
        }
        else{
            bot_lcmgl_color3f(lcmgl_target, 0, .0, 1.0);
            bot_lcmgl_point_size(lcmgl_target, 2);
            bot_lcmgl_begin(lcmgl_target, GL_POINTS);
            
            for (size_t i = 0; i < cloud_target->points.size (); ++i){
                bot_lcmgl_vertex3f(lcmgl_target, cloud_target->points[i].x, cloud_target->points[i].y, 0);
            }
            
            bot_lcmgl_end(lcmgl_target);
        }
        bot_lcmgl_switch_buffer(lcmgl_target);
    }
    
    for(int k = 0; k < angle_size; k++){        

        Eigen::Matrix4f Ti = Eigen::Matrix4f::Identity ();

        w = 2 * M_PI /angle_size * k;

        BotTrans tf_match_1_to_tf_match;
    
        tf_match_1_to_tf_match.trans_vec[0] = 0;
        tf_match_1_to_tf_match.trans_vec[1] = 0;
        tf_match_1_to_tf_match.trans_vec[2] = 0;
        
        double rpy_tf_match_1_to_tf_match[3] = {.0,.0, w};
        bot_roll_pitch_yaw_to_quat (rpy_tf_match_1_to_tf_match,  tf_match_1_to_tf_match.rot_quat);

        BotTrans match_to_tf_match;

        bot_trans_apply_trans_to(&tf_match_1_to_tf_match, &match_to_tf_match_1, &match_to_tf_match);
                
        //fprintf(stderr, "\n\n Iteration : %d Rotating match scan with : %f\n", k, bot_to_degrees(w));
        
        icp_result_t res; 
        res.angle = w;

        double pos_2[3] = {0}, trans_pos_2[3];
        for (size_t i = 0; i < match->npoints; ++i){
            pos_2[0] = match->points[i].x;
            pos_2[1] = match->points[i].y;
            bot_trans_apply_vec(&match_to_tf_match, pos_2, trans_pos_2);
            
            cloud_match->points[i].x = trans_pos_2[0];
            cloud_match->points[i].y = trans_pos_2[1];
            cloud_match->points[i].z = 0;
        }

          
        if(lcmgl_match){
            if(draw_act){
                double pos[3] = {0}, trans_pos[3];

                bot_lcmgl_color3f(lcmgl_match, 0.5, .0, .0);
                bot_lcmgl_point_size(lcmgl_match, 2);
                bot_lcmgl_begin(lcmgl_match, GL_POINTS);
                BotTrans tf = match_to_tf_match;
                bot_trans_invert(&tf);
                for (size_t i = 0; i < cloud_match->points.size (); ++i)
                    {
                        pos[0] = cloud_match->points[i].x;
                        pos[1] = cloud_match->points[i].y;
                        bot_trans_apply_vec(&tf, pos, trans_pos);

                        bot_lcmgl_vertex3f(lcmgl_match, trans_pos[0], trans_pos[1], 0);
                     }
                bot_lcmgl_end(lcmgl_match);
            }
            else{
                bot_lcmgl_color3f(lcmgl_match, 1.0, .0, .0);
                bot_lcmgl_point_size(lcmgl_match, 2);
                bot_lcmgl_begin(lcmgl_match, GL_POINTS);
                
                for (size_t i = 0; i < cloud_match->points.size (); ++i)
                    {
                        bot_lcmgl_vertex3f(lcmgl_match, cloud_match->points[i].x, cloud_match->points[i].y, 0);
                    }
                bot_lcmgl_end(lcmgl_match);
            }

            bot_lcmgl_switch_buffer(lcmgl_match);
        }
        //too small and this doesn't converge - i think because it doesnt have a good signal as to which way to turn 
        //
        //seemed to work with 3.0
        double max_c_dist = 3.0;//3.0;//10.0;
        icp.setRANSACIterations(200); //was at 10
        icp.setMaxCorrespondenceDistance (max_c_dist);//(10.0);
        // Set the maximum number of iterations (criterion 1)
        //icp.setMaximumIterations (40); //was at 20
    
        icp.setTransformationEpsilon(1e-8);
        icp.setEuclideanFitnessEpsilon (1e-5);
        Eigen::Matrix4f prev;
        double ransac_threshold = 2.0;//2.0;//0.5; 
    
        icp.setRANSACOutlierRejectionThreshold (ransac_threshold);
        
        //std::cout << "Outlier rejection threshold : " << icp.getRANSACOutlierRejectionThreshold () << " Max Corresponding distance : " << icp.getMaxCorrespondenceDistance () << std::endl;

        icp.setInputTarget(cloud_target);
        icp.setInputCloud(cloud_match);
        
        //call icp
        icp.setMaximumIterations (40); //was at 20
        int nr_basic = 0;
        
        int v = 0;

        //cloud_match = final;
        icp.setInputCloud(cloud_match);
        icp.align(final);
        Ti = icp.getFinalTransformation () * Ti;
        
        //std::cout << "Final Transform given from ICP " << icp.getFinalTransformation () << std::endl;

        if(icp.hasConverged()){
            res.converged = 1;
        }

        std::cout << "Final Transform : \n" << Ti << std::endl; 

        //can we just get the match_sm_to_target
        BotTrans tf_target_to_target = target_to_tf_target;;
        bot_trans_invert(&tf_target_to_target);
        
        BotTrans tf_match_to_tf_target_sm;
        tf_match_to_tf_target_sm.trans_vec[0] = Ti(0,3);
        tf_match_to_tf_target_sm.trans_vec[1] = Ti(1,3);
        tf_match_to_tf_target_sm.trans_vec[2] = 0;

        double angle = atan2(-Ti(0,1), Ti(0,0));

        double rpy1[3] = {0,0, angle};
        bot_roll_pitch_yaw_to_quat (rpy1, tf_match_to_tf_target_sm.rot_quat);

        BotTrans match_sm_to_tf_target;

        bot_trans_apply_trans_to(&tf_match_to_tf_target_sm, &match_to_tf_match, &match_sm_to_tf_target);
        
        BotTrans match_sm_to_target;  //this is what we want 

        bot_trans_apply_trans_to(&tf_target_to_target, &match_sm_to_tf_target, &match_sm_to_target);

        BotTrans tf_match_sm_to_tf_target;
        tf_match_sm_to_tf_target.trans_vec[0] = Ti(0,3);
        tf_match_sm_to_tf_target.trans_vec[1] = Ti(1,3);
        tf_match_sm_to_tf_target.trans_vec[2] = 0;

        //double angle = atan2(-Ti(0,1), Ti(0,0));

        //double rpy1[3] = {0,0, angle};
        //bot_roll_pitch_yaw_to_quat (rpy1, tf_match_sm_to_tf_target.rot_quat);

        //BotTrans tf_target_to_target= target_to_tf_target;
        //bot_trans_invert(&tf_target_to_target);

        BotTrans tf_match_sm_to_target;
        bot_trans_apply_trans_to( &tf_target_to_target, &tf_match_sm_to_tf_target, &tf_match_sm_to_target);

        BotTrans match_sm_to_target_1;
        //match_to_tf_match and match_sm_to_tf_match_sm is the same 
        BotTrans match_sm_to_tf_match_sm = match_to_tf_match;
        bot_trans_apply_trans_to(&tf_match_sm_to_target, &match_sm_to_tf_match_sm, &match_sm_to_target_1);
        
        //0.4 gives matches that are wrong 
        
        double fitness_score = icp.getFitnessScoreWithCount( 0.2, &nr);

        BotTrans target_to_act = match_sm_to_target; 
        bot_trans_invert(&target_to_act);

        //lets do a count 
        double pos[3] = {0}, trans_pos[3];
        int match_count = 0;

        //fprintf(stderr, "Actual Matched No of Points : %d => Reported : %d\n", match_count, nr);

        double rpy_res[3]; 
        bot_quat_to_roll_pitch_yaw (target_to_act.rot_quat, rpy_res);

        //fprintf(stderr, " [%d] => Result : %f, %f, %f\n", k, target_to_act.trans_vec[0], 
        //       target_to_act.trans_vec[1], bot_to_degrees(rpy_res[2]));

        res.Tf = match_sm_to_target;//target_to_act;
        res.score = fitness_score;
        res.n_match = nr; 

        results.push_back(res);

        if(lcmgl_result){

            if(draw_act){
                bot_lcmgl_color3f(lcmgl_result, 0.5, 0.5, .0);
                bot_lcmgl_point_size(lcmgl_result, 5);
                bot_lcmgl_begin(lcmgl_result, GL_POINTS);
                
                double pos[3] = {0}, trans_pos[3];
                BotTrans act_to_target_res = target_to_act;
                bot_trans_invert(&act_to_target_res);
                
                for(int i = 0; i < match->npoints ; i++){
                    pos[0] = match->points[i].x;
                    pos[1] = match->points[i].y;
                    bot_trans_apply_vec(&act_to_target_res, pos, trans_pos);
                    bot_lcmgl_vertex3f(lcmgl_result, trans_pos[0], trans_pos[1], 0);
                }
                bot_lcmgl_end(lcmgl_result);
                char match_info[512];

                pos[0] = match->points[0].x;
                pos[1] = match->points[0].y;
                
                bot_trans_apply_vec(&act_to_target_res, pos, trans_pos);

                
                sprintf(match_info,"Match : %d Score : %f", nr, fitness_score);
                double text_pos[3] = {trans_pos[0] + 5, trans_pos[1] + 5,0};
                bot_lcmgl_text(lcmgl_result, text_pos, match_info);
            }

            else{
                bot_lcmgl_color3f(lcmgl_result, 0.5, .0, .5);
                bot_lcmgl_point_size(lcmgl_result, 5);
                bot_lcmgl_begin(lcmgl_result, GL_POINTS);
                
                for (size_t i = 0; i < final.points.size (); ++i){
                    bot_lcmgl_vertex3f(lcmgl_result, final.points[i].x, final.points[i].y, 0);
                }
                bot_lcmgl_end(lcmgl_result);
                
                char match_info[512];
                sprintf(match_info,"Match : %d Score : %f", nr, fitness_score);
                double text_pos[3] = {final.points[0].x, final.points[0].y + 5,0};
                bot_lcmgl_text(lcmgl_result, text_pos, match_info);
            }
            bot_lcmgl_switch_buffer(lcmgl_result);
        }
       
        if(0){
            std::cout << "Sleeping \n" ;
            sleep(2);
        }
    }

    double fitness_score = 0; 
    int max_matched = 0;
    int matched_id = -1;
 
    for(int i=0; i < results.size(); i++){
        icp_result_t r = results.at(i);
        fprintf(stderr, "Angle : %f -> Score : %f Matched : %d Converged : %d\n", 
                bot_to_degrees(r.angle), 
                r.score, 
                r.n_match, 
                r.converged);
        
        if( r.converged && max_matched < r.n_match){
            matched_id = i;
            max_matched = r.n_match; 
            fitness_score = r.score;
        }

        double rpy_act[3]; 
        bot_quat_to_roll_pitch_yaw (r.Tf.rot_quat, rpy_act);

        fprintf(stderr, "Transform : %f, %f - %f\n", r.Tf.trans_vec[0], r.Tf.trans_vec[1], bot_to_degrees(rpy_act[2]));
    }

    *f_score = fitness_score;
    *matches = max_matched; //nr;

    std::cout << "Rough iteration has converged:" << icp.hasConverged() << " score: " <<
         fitness_score << std::endl;
        
    fprintf(stderr, "Found Matches : %d out of : %d\n", max_matched , (int) final.points.size ()); 
    
    if(matched_id >=0){
        fprintf(stderr, "Found Valid Match\n");

        BotTrans result = results.at(matched_id).Tf;; 
        double rpy_result[3]; 
        bot_quat_to_roll_pitch_yaw (result.rot_quat, rpy_result);

        fprintf(stderr, "Transform : %f, %f - %f\n", result.trans_vec[0], result.trans_vec[1], bot_to_degrees(rpy_result[2]));
        for(int i=0; i < 3; i++){
            fprintf(stderr, "\n");
            for(int j=0; j<4; j++){
                transform[i*4 + j] = 0;//tf(i,j);
            }
        }

        if(lcmgl_result){
            if(max_matched > 700 || max_matched / (double)  match->npoints > 0.5 ){
                bot_lcmgl_color3f(lcmgl_result, 0.5, 1.0, .0);
            }
            else{
                bot_lcmgl_color3f(lcmgl_result, 1.0, 0.0, .0);
            }

            bot_lcmgl_point_size(lcmgl_result, 5);
            bot_lcmgl_begin(lcmgl_result, GL_POINTS);
            
            fprintf(stderr, "ICP - (PCL) Drawing result +++++ \n");
            
            /*double pos[3] = {0}, trans_pos[3];
            BotTrans act_to_target_res = result; 
            bot_trans_invert(&act_to_target_res);
            
            for(int i = 0; i < match->npoints ; i++){
                pos[0] = match->points[i].x;
                pos[1] = match->points[i].y;
                bot_trans_apply_vec(&act_to_target_res, pos, trans_pos);
                bot_lcmgl_vertex3f(lcmgl_result, trans_pos[0], trans_pos[1], 0);
                }*/

            double pos[3] = {0}, trans_pos[3];
            BotTrans act_to_target_res = result; 
            //bot_trans_invert(&act_to_target_res);
            
            for(int i = 0; i < match->npoints ; i++){
                pos[0] = match->points[i].x;
                pos[1] = match->points[i].y;
                bot_trans_apply_vec(&act_to_target_res, pos, trans_pos);
                bot_lcmgl_vertex3f(lcmgl_result, trans_pos[0], trans_pos[1], 0);
            }

            bot_lcmgl_end(lcmgl_result);

            pos[0] = match->points[0].x;
            pos[1] = match->points[0].y;
            
            bot_trans_apply_vec(&act_to_target_res, pos, trans_pos);

            char match_info[512];
            sprintf(match_info,"Match : %d Score : %f", results.at(matched_id).n_match, results.at(matched_id).score);
            double text_pos[3] = {trans_pos[0]+5.0, trans_pos[1] + 5,0};
            bot_lcmgl_text(lcmgl_result, text_pos, match_info);
            
            bot_lcmgl_switch_buffer(lcmgl_result);
        }

        transform[3] = result.trans_vec[0];
        transform[7] = result.trans_vec[1];
        transform[0] = cos(rpy_result[2]);
        transform[1] = -sin(rpy_result[2]);
        transform[4] = sin(rpy_result[2]);
        transform[5] = cos(rpy_result[2]);

        double theta = atan2(-transform[1], transform[0]);
        fprintf(stderr, "\nRotation : %f = Deg : %f\n", theta, bot_to_degrees(theta)); 
    }
    else{
        fprintf(stderr, "Failed to find match\n");
    }

    //if overlap is less than 50% of if the count is less than 3 * no of scans (assumed to be 250 points) 

    int valid_overlap = 0;
    
    if((max_matched / (double) match->npoints) > 0.5  && max_matched > 500)
        valid_overlap = 1;

    if(matched_id == -1){// || fitness_score > 0.05 || valid_overlap == 0){
        cloud_target->points.resize(0);
        cloud_match->points.resize(0);
        orig_match->points.resize(0);
        return NULL; 
    }

    //std::cout << Ti << std::endl;//icp.getFinalTransformation() << std::endl;

    pointlist2d_t *result = pointlist2d_new(final.points.size());
    
    //this probably needs to be fixed properly 

    for (size_t i = 0; i < final.points.size (); ++i){
        result->points[i].x = final.points[i].x;
        result->points[i].y = final.points[i].y;
    }

    
    cloud_target->points.resize(0);
    cloud_match->points.resize(0);
    orig_match->points.resize(0);
    
    return result;
}

/*
  pointlist2d_t * doPCLScanMatchPoints(pointlist2d_t *target, pointlist2d_t *match, double transform[12], 
  double *f_score, 
  int *matches, bot_lcmgl_t *lcmgl, 
  double max_corr_dist = 0.4){
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_target (new pcl::PointCloud<pcl::PointXYZ>); //target scan 
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_match (new pcl::PointCloud<pcl::PointXYZ>); //the one that should be transformed to match the target scan 

  //find the center of the second point cloud and align them properly??
    
  cloud_target->width    = target->npoints; 
  cloud_target->height   = 1;
  cloud_target->is_dense = false;
  cloud_target->points.resize (cloud_target->width * cloud_target->height);

  double mx_t = 0, my_t = 0;
  for (size_t i = 0; i < cloud_target->points.size (); ++i)
  {
  cloud_target->points[i].x = target->points[i].x;
  cloud_target->points[i].y = target->points[i].y;
  cloud_target->points[i].z = 0;
  mx_t += target->points[i].x;
  my_t += target->points[i].y;
  }

  mx_t /= (double) cloud_target->points.size ();
  my_t /= (double) cloud_target->points.size ();

  cloud_match->width    = match->npoints;
  cloud_match->height   = 1;
  cloud_match->is_dense = false;
  cloud_match->points.resize (cloud_match->width * cloud_match->height);


  double mx_m = 0, my_m = 0;
  //maybe we can just do the alignment here ?? 
  for (size_t i = 0; i < cloud_match->points.size (); ++i){
  mx_m += match->points[i].x;
  my_m += match->points[i].y;
  }

  mx_m /= (double) cloud_match->points.size ();
  my_m /= (double) cloud_match->points.size ();


  //maybe we can just do the alignment here?? 
  for (size_t i = 0; i < cloud_match->points.size (); ++i){
  cloud_match->points[i].x = match->points[i].x;
  cloud_match->points[i].y = match->points[i].y;
  cloud_match->points[i].z = 0;
  }

  pcl::IterativeClosestPointMod<pcl::PointXYZ, pcl::PointXYZ> icp;

  std::cout << "Outlier rejection threshold : " << icp.getRANSACOutlierRejectionThreshold () << " Max Corresponding distance : " << icp.getMaxCorrespondenceDistance () << std::endl;

  //too small and this doesn't converge - i think because it doesnt have a good signal as to which way to turn 
  //
  //seemed to work with 3.0
  double max_c_dist = 3.0;//10.0;
  icp.setRANSACIterations(200); //was at 10
  icp.setMaxCorrespondenceDistance (max_c_dist);//(10.0);
  // Set the maximum number of iterations (criterion 1)
  //icp.setMaximumIterations (40); //was at 20
    
  icp.setTransformationEpsilon(1e-8);
  icp.setEuclideanFitnessEpsilon (1e-5);
  Eigen::Matrix4f prev;
  // Set the transformation epsilon (criterion 2)
  //icp.setTransformationEpsilon (1);
  // Set the euclidean distance difference epsilon (criterion 3)
  //icp.setEuclideanFitnessEpsilon (1);
  //icp.setMaxCorrespondenceDistance (dist);
  double ransac_threshold = 2.0;//2.0;//0.5; 
    
  icp.setRANSACOutlierRejectionThreshold (ransac_threshold);

  Eigen::Matrix4f Ti = Eigen::Matrix4f::Identity (); 
  icp.setInputTarget(cloud_target);
  icp.setInputCloud(cloud_match);
  pcl::PointCloud<pcl::PointXYZ>::Ptr orig_match = cloud_match;;
  pcl::PointCloud<pcl::PointXYZ>::Ptr final = cloud_match;
  //call icp
  icp.setMaximumIterations (20); //was at 20
  int nr_basic = 0;
        
  for (int i = 0; i < 10; ++i){
  PCL_INFO ("\nIteration Nr. %d.\n", i);
  cloud_match = final;
  icp.setInputCloud(cloud_match);
  //call icp
  icp.align(*final);
        
  Eigen::Matrix4f current_transform = icp.getFinalTransformation ();  
  std::cout << "Current Transform : \n" << current_transform << std::endl;
  double jump = hypot(current_transform(0,3), current_transform(1,3));
        
  Ti = icp.getFinalTransformation () * Ti;
  prev = icp.getLastIncrementalTransformation ();

  double fitness_score1 = icp.getFitnessScore(0.3, &nr_basic);

  //if the algo converged - but the solution is bad - then we should try reducing the convergence distance 
  if(icp.hasConverged()){
  fprintf(stderr, "Converged\n");
  if(nr_basic < 0.5 * (int) final->points.size ()){
  fprintf(stderr, "Haven't found enough good correspondances - reducing threshold\n");
  max_c_dist *= 0.5;                    
  }
  else{
  fprintf(stderr, "Converged with good match\n");
  break;
  }
  }
  else{
  max_c_dist *= 1.5;
  //might be worth giving a random bump 
  //PointCloudSource input_transformed;
  //transformPointCloud (*input_, input_transformed, final_transformation_);
  }


  if(lcmgl){
  bot_lcmgl_color3f(lcmgl, 0.5, 1.0, .0);
  bot_lcmgl_point_size(lcmgl, 2);
  bot_lcmgl_begin(lcmgl, GL_POINTS);
            
  for (size_t i = 0; i < final->points.size (); ++i)
  {
  bot_lcmgl_vertex3f(lcmgl, final->points[i].x, final->points[i].y, 0);
  }
  bot_lcmgl_end(lcmgl);
  bot_lcmgl_switch_buffer(lcmgl);
            
  icp.setMaxCorrespondenceDistance (max_c_dist);
  }
  if(0){
  std::cout << "Sleeping \n" ;
  sleep(1);
  }

  fprintf(stderr, "New converged distance : %f\n", max_c_dist);

  std::cout << "has converged:" << icp.hasConverged() << " score: " <<
  icp.getFitnessScore() << std::endl;
     
  std::cout << "Overall Transform " << std::endl <<Ti <<std::endl;//icp.getFinalTransformation() << std::endl;
  }


  
    
  int nr = 0;
  double max_dist = max_corr_dist;



  double fitness_score = icp.getFitnessScoreWithCount(max_dist, &nr);
  *f_score = fitness_score;
  *matches = nr;

  std::cout << "Rough iteration has converged:" << icp.hasConverged() << " score: " <<
  fitness_score << std::endl;
        
  fprintf(stderr, "Found Matches : %d out of : %d\n", nr , (int) final->points.size ()); 
    
  Eigen::Matrix4f tf = Ti;

  for(int i=0; i < 3; i++){
  fprintf(stderr, "\n");
  for(int j=0; j<4; j++){
  transform[i*4 + j] = tf(i,j);
  fprintf(stderr, "%f\t", transform[i*4 + j]);
  }
  }

  double theta = atan2(-transform[1], transform[0]);
  fprintf(stderr, "\nRotation : %f = Deg : %f\n", theta, bot_to_degrees(theta)); 
    
  //if overlap is less than 50% of if the count is less than 3 * no of scans (assumed to be 250 points) 

  int valid_overlap = 0;
    
  if(nr / (double) match->npoints > 0.5  && nr > 3 * 250)
  valid_overlap = 1;

  if(icp.hasConverged()  == 0){// || fitness_score > 0.05 || valid_overlap == 0){
  cloud_target->points.resize(0);
  cloud_match->points.resize(0);
  orig_match->points.resize(0);
  return NULL; 
  }

  std::cout << Ti << std::endl;//icp.getFinalTransformation() << std::endl;

  pointlist2d_t *result = pointlist2d_new(final->points.size());

  for (size_t i = 0; i < final->points.size (); ++i){
  result->points[i].x = final->points[i].x;
  result->points[i].y = final->points[i].y;
  }

    
  cloud_target->points.resize(0);
  cloud_match->points.resize(0);
  orig_match->points.resize(0);
    
  return result;
  }
*/

pointlist2d_t * doPCLScanMatchPointsOrig(pointlist2d_t *target, pointlist2d_t *match, double transform[12], 
                                         double *f_score, 
                                         int *matches, 
                                         double max_corr_dist = 0.4){
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_target (new pcl::PointCloud<pcl::PointXYZ>); //target scan 
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_match (new pcl::PointCloud<pcl::PointXYZ>); //the one that should be transformed to match the target scan 

    //find the center of the second point cloud and align them properly??
    
    cloud_target->width    = target->npoints; 
    cloud_target->height   = 1;
    cloud_target->is_dense = false;
    cloud_target->points.resize (cloud_target->width * cloud_target->height);
    for (size_t i = 0; i < cloud_target->points.size (); ++i)
        {
            cloud_target->points[i].x = target->points[i].x;
            cloud_target->points[i].y = target->points[i].y;
            cloud_target->points[i].z = 0;
        }

    cloud_match->width    = match->npoints;
    cloud_match->height   = 1;
    cloud_match->is_dense = false;
    cloud_match->points.resize (cloud_match->width * cloud_match->height);
    for (size_t i = 0; i < cloud_match->points.size (); ++i)
        {
            cloud_match->points[i].x = match->points[i].x;
            cloud_match->points[i].y = match->points[i].y;
            cloud_match->points[i].z = 0;
        }

    pcl::IterativeClosestPointMod<pcl::PointXYZ, pcl::PointXYZ> icp;
    
    //double rans = 0.5; //was 0.05
    //double dist = 5.0;

    std::cout << "Outlier rejection threshold : " << icp.getRANSACOutlierRejectionThreshold () << " Max Corresponding distance : " << icp.getMaxCorrespondenceDistance () << std::endl;

    //too small and this doesn't converge - i think because it doesnt have a good signal as to which way to turn 
    //
    //seemed to work with 3.0
    double max_c_dist = 3.0;//10.0;
    icp.setRANSACIterations(200); //was at 10
    icp.setMaxCorrespondenceDistance (max_c_dist);//(10.0);
    // Set the maximum number of iterations (criterion 1)
    //icp.setMaximumIterations (40); //was at 20
    
    icp.setTransformationEpsilon(1e-8);
    icp.setEuclideanFitnessEpsilon (1e-5);
    Eigen::Matrix4f prev;
    // Set the transformation epsilon (criterion 2)
    //icp.setTransformationEpsilon (1);
    // Set the euclidean distance difference epsilon (criterion 3)
    //icp.setEuclideanFitnessEpsilon (1);
    //icp.setMaxCorrespondenceDistance (dist);
    double ransac_threshold = 2.0;//2.0;//0.5; 
    
    icp.setRANSACOutlierRejectionThreshold (ransac_threshold);

    Eigen::Matrix4f Ti = Eigen::Matrix4f::Identity (); 
    icp.setInputTarget(cloud_target);
    icp.setInputCloud(cloud_match);
    pcl::PointCloud<pcl::PointXYZ>::Ptr orig_match = cloud_match;;
    pcl::PointCloud<pcl::PointXYZ> final;
    //call icp
    icp.setMaximumIterations (20); 
    int nr_basic = 0;
        
    icp.align(final);
    
    Ti = icp.getFinalTransformation () * Ti;
    prev = icp.getLastIncrementalTransformation ();

    int nr = 0;
    double max_dist = max_corr_dist;

    double fitness_score = icp.getFitnessScoreWithCount(max_dist, &nr);
    *f_score = fitness_score;
    *matches = nr;

    std::cout << "Rough iteration has converged:" << icp.hasConverged() << " score: " <<
         fitness_score << std::endl;
        
    fprintf(stderr, "Found Matches : %d out of : %d\n", nr , (int) final.points.size ()); 
    
    Eigen::Matrix4f tf = Ti;

    for(int i=0; i < 3; i++){
        fprintf(stderr, "\n");
        for(int j=0; j<4; j++){
            transform[i*4 + j] = tf(i,j);
            fprintf(stderr, "%f\t", transform[i*4 + j]);
        }
    }

    double theta = atan2(-transform[1], transform[0]);
    fprintf(stderr, "\nRotation : %f = Deg : %f\n", theta, bot_to_degrees(theta)); 
    
    //if overlap is less than 50% of if the count is less than 3 * no of scans (assumed to be 250 points) 

    int valid_overlap = 0;
    
    if(nr / (double) match->npoints > 0.5  && nr > 3 * 250)
        valid_overlap = 1;

    if(icp.hasConverged()  == 0){// || fitness_score > 0.05 || valid_overlap == 0){
        cloud_target->points.resize(0);
        cloud_match->points.resize(0);
        orig_match->points.resize(0);
        return NULL; 
    }

    std::cout << Ti << std::endl;//icp.getFinalTransformation() << std::endl;

    pointlist2d_t *result = pointlist2d_new(final.points.size());

    for (size_t i = 0; i < final.points.size (); ++i){
        result->points[i].x = final.points[i].x;
        result->points[i].y = final.points[i].y;
    }

    
    cloud_target->points.resize(0);
    cloud_match->points.resize(0);
    orig_match->points.resize(0);
    
    return result;
}

pointlist2d_t * doPCLScanMatchPointsWide(pointlist2d_t *target, pointlist2d_t *match, double transform[12], 
                                     double *f_score, 
                                     int *matches, double max_corr_dist = 0.4){
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_target (new pcl::PointCloud<pcl::PointXYZ>); //target scan 
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_match (new pcl::PointCloud<pcl::PointXYZ>); //the one that should be transformed to match the target scan 
    
    cloud_target->width    = target->npoints; 
    cloud_target->height   = 1;
    cloud_target->is_dense = false;
    cloud_target->points.resize (cloud_target->width * cloud_target->height);
    for (size_t i = 0; i < cloud_target->points.size (); ++i)
        {
            cloud_target->points[i].x = target->points[i].x;
            cloud_target->points[i].y = target->points[i].y;
            cloud_target->points[i].z = 0;
        }

    cloud_match->width    = match->npoints;
    cloud_match->height   = 1;
    cloud_match->is_dense = false;
    cloud_match->points.resize (cloud_match->width * cloud_match->height);
    for (size_t i = 0; i < cloud_match->points.size (); ++i)
        {
            cloud_match->points[i].x = match->points[i].x;
            cloud_match->points[i].y = match->points[i].y;
            cloud_match->points[i].z = 0;
        }

    pcl::IterativeClosestPointMod<pcl::PointXYZ, pcl::PointXYZ> icp;
    
    //double rans = 0.5; //was 0.05
    //double dist = 5.0;

    std::cout << "Outlier rejection threshold : " << icp.getRANSACOutlierRejectionThreshold () << " Max Corresponding distance : " << icp.getMaxCorrespondenceDistance () << std::endl;

    //too small and this doesn't converge - i think because it doesnt have a good signal as to which way to turn 

    //icp.setMaxCorrespondenceDistance (10.0);
    //icp.setRANSACIterations(50); //was at 10
    // Set the maximum number of iterations (criterion 1)
    //    icp.setMaximumIterations (60); //was at 20
    

    // Set the transformation epsilon (criterion 2)
    //icp.setTransformationEpsilon (1);
    // Set the euclidean distance difference epsilon (criterion 3)
    //icp.setEuclideanFitnessEpsilon (1);
    //icp.setMaxCorrespondenceDistance (dist);
    //icp.setRANSACOutlierRejectionThreshold (rans);

    Eigen::Matrix4f Ti = Eigen::Matrix4f::Identity (); 
    icp.setInputTarget(cloud_target);
    icp.setInputCloud(cloud_match);
    pcl::PointCloud<pcl::PointXYZ>::Ptr orig_match = cloud_match;;
    pcl::PointCloud<pcl::PointXYZ>::Ptr final = cloud_match;
    //call icp
    icp.align(*final);
    Ti = icp.getFinalTransformation () * Ti;
    
    int nr = 0;
    double max_dist = max_corr_dist;

    int nr_basic = 0;

    /*double fitness_score1 = icp.getFitnessScore(max_dist, &nr_basic);

      fprintf(stderr, "Original Fitness Score : %f => Nr : %d\n", fitness_score1, nr_basic);   */

    double fitness_score = icp.getFitnessScoreWithCount(max_dist, &nr);
    *f_score = fitness_score;
    *matches = nr;

    std::cout << "Rough iteration has converged:" << icp.hasConverged() << " score: " <<
         fitness_score << std::endl;
        
    fprintf(stderr, "Found Matches : %d out of : %d\n", nr , (int) final->points.size ()); 
    
    Eigen::Matrix4f tf = Ti;

    for(int i=0; i < 3; i++){
        fprintf(stderr, "\n");
        for(int j=0; j<4; j++){
            transform[i*4 + j] = tf(i,j);
            fprintf(stderr, "%f\t", transform[i*4 + j]);
        }
    }

    double theta = atan2(-transform[1], transform[0]);
    fprintf(stderr, "\nRotation : %f = Deg : %f\n", theta, bot_to_degrees(theta)); 
    
    //if overlap is less than 50% of if the count is less than 3 * no of scans (assumed to be 250 points) 

    int valid_overlap = 0;
    
    if(nr / (double) match->npoints > 0.5  && nr > 3 * 250)
        valid_overlap = 1;

    if(icp.hasConverged()  == 0){// || fitness_score > 0.05 || valid_overlap == 0){
        cloud_target->points.resize(0);
        cloud_match->points.resize(0);
        orig_match->points.resize(0);
        return NULL; 
    }

    std::cout << Ti << std::endl;//icp.getFinalTransformation() << std::endl;

    pointlist2d_t *result = pointlist2d_new(final->points.size());

    for (size_t i = 0; i < final->points.size (); ++i){
        result->points[i].x = final->points[i].x;
        result->points[i].y = final->points[i].y;
    }

    
    cloud_target->points.resize(0);
    cloud_match->points.resize(0);
    orig_match->points.resize(0);
    
    return result;
}

/*pointlist2d_t * doPCLScanMatchPointsMod(pointlist2d_t *target, pointlist2d_t *match, double transform[12]){
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_target (new pcl::PointCloud<pcl::PointXYZ>); //target scan 
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_match (new pcl::PointCloud<pcl::PointXYZ>); //the one that should be transformed to match the target scan 
    
    cloud_target->width    = target->npoints; 
    cloud_target->height   = 1;
    cloud_target->is_dense = false;
    cloud_target->points.resize (cloud_target->width * cloud_target->height);
    for (size_t i = 0; i < cloud_target->points.size (); ++i)
        {
            cloud_target->points[i].x = target->points[i].x;
            cloud_target->points[i].y = target->points[i].y;
            cloud_target->points[i].z = 0;
        }

    cloud_match->width    = match->npoints;
    cloud_match->height   = 1;
    cloud_match->is_dense = false;
    cloud_match->points.resize (cloud_match->width * cloud_match->height);
    for (size_t i = 0; i < cloud_match->points.size (); ++i)
        {
            cloud_match->points[i].x = match->points[i].x;
            cloud_match->points[i].y = match->points[i].y;
            cloud_match->points[i].z = 0;
        }

    pcl::IterativeClosestPointMod<pcl::PointXYZ, pcl::PointXYZ> icp;
    
    //double rans = 0.5; //was 0.05
    //double dist = 5.0;

    std::cout << "Outlier rejection threshold : " << icp.getRANSACOutlierRejectionThreshold () << " Max Corresponding distance : " << icp.getMaxCorrespondenceDistance () << std::endl;

    //too small and this doesn't converge - i think because it doesnt have a good signal as to which way to turn 
    icp.setMaxCorrespondenceDistance (10.0);

    icp.setRANSACIterations(10);
    // Set the maximum number of iterations (criterion 1)
    icp.setMaximumIterations (20);
    // Set the transformation epsilon (criterion 2)
    //icp.setTransformationEpsilon (1);
    // Set the euclidean distance difference epsilon (criterion 3)
    //icp.setEuclideanFitnessEpsilon (1);
    //icp.setMaxCorrespondenceDistance (dist);
    //icp.setRANSACOutlierRejectionThreshold (rans);

    Eigen::Matrix4f Ti = Eigen::Matrix4f::Identity (); 
    icp.setInputTarget(cloud_target);
    icp.setInputCloud(cloud_match);
    pcl::PointCloud<pcl::PointXYZ>::Ptr orig_match = cloud_match;;
    pcl::PointCloud<pcl::PointXYZ>::Ptr final = cloud_match;
    //call icp
    icp.align(*final);
    Ti = icp.getFinalTransformation () * Ti;
    
    int nr = 0;
    int max_dist = 0.4;

    //double fitness_score = icp.getFitnessScore(0.4, &nr);
    double fitness_score = icp.getFitnessScoreWithCount(0.4, &nr);

    std::cout << "Rough iteration has converged:" << icp.hasConverged() << " score: " <<
         fitness_score << std::endl;

    
    if(0){
        cloud_match = final;
        icp.setInputCloud(cloud_match);
        
        
        icp.setMaxCorrespondenceDistance (.4);
        // Set the maximum number of iterations (criterion 1)
        icp.setMaximumIterations (10);
        icp.align(*final);
    
        Ti = icp.getFinalTransformation () * Ti;
    }

    //fprintf(stderr, "No of Correspondances : %d\n", no_correspondance);
        
    fprintf(stderr, "Found Matches : %d out of : %d\n", nr , (int) final->points.size ()); 
    

    Eigen::Matrix4f tf = Ti;

    for(int i=0; i < 3; i++){
        fprintf(stderr, "\n");
        for(int j=0; j<4; j++){
            transform[i*4 + j] = tf(i,j);
            fprintf(stderr, "%f\t", transform[i*4 + j]);
        }
    }

    double theta = atan2(-transform[1], transform[0]);
    fprintf(stderr, "\nRotation : %f = Deg : %f\n", theta, bot_to_degrees(theta)); 
    
    //if overlap is less than 50% of if the count is less than 3 * no of scans (assumed to be 250 points) 

    int valid_overlap = 0;
    
    if(nr / (double) match->npoints > 0.5  && nr > 3 * 250)
        valid_overlap = 1;

    if( icp.hasConverged()  == 0 || fitness_score > 0.05 || valid_overlap == 0)
        return NULL; 

    std::cout << Ti << std::endl;//icp.getFinalTransformation() << std::endl;

    pointlist2d_t *result = pointlist2d_new(final->points.size());

    for (size_t i = 0; i < final->points.size (); ++i){
        result->points[i].x = final->points[i].x;
        result->points[i].y = final->points[i].y;
    }

    
    cloud_target->points.resize(0);
    cloud_match->points.resize(0);
    orig_match->points.resize(0);
    
    return result;
    }*/


/*int doPCLScanMatchIncremental(pointlist2d_t *target, pointlist2d_t *match, double transform[12]){
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_target (new pcl::PointCloud<pcl::PointXYZ>); //target scan 
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_match (new pcl::PointCloud<pcl::PointXYZ>); //the one that should be transformed to match the target scan 
    
    cloud_target->width    = target->npoints; 
    cloud_target->height   = 1;
    cloud_target->is_dense = false;
    cloud_target->points.resize (cloud_target->width * cloud_target->height);
    for (size_t i = 0; i < cloud_target->points.size (); ++i)
        {
            cloud_target->points[i].x = target->points[i].x;
            cloud_target->points[i].y = target->points[i].y;
            cloud_target->points[i].z = 0;
        }

    cloud_match->width    = match->npoints;
    cloud_match->height   = 1;
    cloud_match->is_dense = false;
    cloud_match->points.resize (cloud_match->width * cloud_match->height);
    for (size_t i = 0; i < cloud_match->points.size (); ++i)
        {
            cloud_match->points[i].x = match->points[i].x;
            cloud_match->points[i].y = match->points[i].y;
            cloud_match->points[i].z = 0;
        }

    pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;
    
    //too small and this doesn't converge - i think because it doesnt have a good signal as to which way to turn 
    icp.setMaxCorrespondenceDistance (10.0);
    // Set the maximum number of iterations (criterion 1)
    //icp.setMaximumIterations (50);
    // Set the transformation epsilon (criterion 2)
    //icp.setTransformationEpsilon (1);
    // Set the euclidean distance difference epsilon (criterion 3)
    //icp.setEuclideanFitnessEpsilon (1);
    //icp.setMaxCorrespondenceDistance (dist);
    //icp.setRANSACOutlierRejectionThreshold (rans);

    icp.setInputTarget(cloud_target);
    icp.setInputCloud(cloud_match);
    
    pcl::PointCloud<pcl::PointXYZ>::Ptr orig_match =  cloud_match;

    icp.setMaximumIterations (2);
    Eigen::Matrix4f Ti = Eigen::Matrix4f::Identity (), prev, targetToSource;
    pcl::PointCloud<pcl::PointXYZ>::Ptr reg_result = cloud_match;

    std::cout << "Outlier rejection threshold : " << icp.getRANSACOutlierRejectionThreshold () << " Max Corresponding distance : " << icp.getMaxCorrespondenceDistance () << std::endl;

    //    pcl::PointCloud<pcl::PointXYZ> final;
    //call icp
    for (int i = 0; i < 30; ++i){
        PCL_INFO ("Iteration Nr. %d.\n", i);
        cloud_match = reg_result;
        icp.setInputCloud(cloud_match);
        //call icp
        icp.align(*reg_result);

        Ti = icp.getFinalTransformation () * Ti;
        
        prev = icp.getLastIncrementalTransformation ();

        std::cout << "has converged:" << icp.hasConverged() << " score: " <<
            icp.getFitnessScore() << std::endl;
        
        std::cout << icp.getFinalTransformation() << std::endl;
    }

    std::cout << "Final Transform : " << Ti << std::endl;

    Eigen::Matrix4f tf = Ti; //icp.getFinalTransformation(); 

    for(int i=0; i < 3; i++){
        fprintf(stderr, "\n");
        for(int j=0; j<4; j++){
            transform[i*4 + j] = tf(i,j);
            fprintf(stderr, "%f\t", transform[i*4 + j]);
        }
    }

    double theta = atan2(-transform[1], transform[0]);
    fprintf(stderr, "\nRotation : %f = Deg : %f\n", theta, bot_to_degrees(theta)); 
    

    if( icp.hasConverged()  == 0)
        return 0; 
    std::cout << "has converged:" << icp.hasConverged() << " score: " <<
        icp.getFitnessScore() << std::endl;
    std::cout << icp.getFinalTransformation() << std::endl;
    
    cloud_target->points.resize(0);
    cloud_match->points.resize(0);
    orig_match->points.resize(0);
    
    return 1;
    //delete cloud_target;
    //delete cloud_match;
}


int doPCLScanMatchNL(pointlist2d_t *target, pointlist2d_t *match, double transform[12]){
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_target (new pcl::PointCloud<pcl::PointXYZ>); //target scan 
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_match (new pcl::PointCloud<pcl::PointXYZ>); //the one that should be transformed to match the target scan 
    
    cloud_target->width    = target->npoints; 
    cloud_target->height   = 1;
    cloud_target->is_dense = false;
    cloud_target->points.resize (cloud_target->width * cloud_target->height);
    for (size_t i = 0; i < cloud_target->points.size (); ++i)
        {
            cloud_target->points[i].x = target->points[i].x;
            cloud_target->points[i].y = target->points[i].y;
            cloud_target->points[i].z = 0;
        }

    cloud_match->width    = match->npoints;
    cloud_match->height   = 1;
    cloud_match->is_dense = false;
    cloud_match->points.resize (cloud_match->width * cloud_match->height);
    for (size_t i = 0; i < cloud_match->points.size (); ++i)
        {
            cloud_match->points[i].x = match->points[i].x;
            cloud_match->points[i].y = match->points[i].y;
            cloud_match->points[i].z = 0;
        }

    pcl::IterativeClosestPointNonLinear<pcl::PointXYZ, pcl::PointXYZ> icp;
    boost::shared_ptr<pcl::WarpPointRigid3D<pcl::PointXYZ, pcl::PointXYZ> > warp_fcn 
      (new pcl::WarpPointRigid3D<pcl::PointXYZ, pcl::PointXYZ>);
    
    boost::shared_ptr<pcl::registration::TransformationEstimationLM<pcl::PointXYZ, pcl::PointXYZ> > te (new pcl::registration::TransformationEstimationLM<pcl::PointXYZ, pcl::PointXYZ>);
    te->setWarpFunction (warp_fcn);
    
    // Pass the TransformationEstimation objec to the ICP algorithm
    icp.setTransformationEstimation (te);

    int iter = 500;
    double rans = 0.5; //was 0.05
    double dist = 5.0;

    icp.setMaximumIterations (iter);
    icp.setMaxCorrespondenceDistance (dist);
    icp.setRANSACOutlierRejectionThreshold (rans);
    

    icp.setInputTarget(cloud_target);
    icp.setInputCloud(cloud_match);
    
    pcl::PointCloud<pcl::PointXYZ> final;
    //call icp
    icp.align(final);

    Eigen::Matrix4f tf = icp.getFinalTransformation(); 

    for(int i=0; i < 3; i++){
        fprintf(stderr, "\n");
        for(int j=0; j<4; j++){
            transform[i*4 + j] = tf(i,j);
            fprintf(stderr, "%f\t", transform[i*4 + j]);
        }
    }

    double theta = acos(transform[0]);
    fprintf(stderr, "\nRotation : %f = Deg : %f\n", theta, bot_to_degrees(theta)); 
    

    if( icp.hasConverged()  == 0)
        return 0; 
    std::cout << "has converged:" << icp.hasConverged() << " score: " <<
        icp.getFitnessScore() << std::endl;
    std::cout << icp.getFinalTransformation() << std::endl;
    
    cloud_target->points.resize(0);
    cloud_match->points.resize(0);
    final.points.resize(0);
    
    return 1;
    //delete cloud_target;
    //delete cloud_match;
    }*/

pointlist2d_t* doPCLScanMatchWithTransform(pointlist2d_t *target, pointlist2d_t *match, double transform[12]){
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_target (new pcl::PointCloud<pcl::PointXYZ>); //target scan 
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_match (new pcl::PointCloud<pcl::PointXYZ>); //the one that should be transformed to match the target scan 
    
    cloud_target->width    = target->npoints; 
    cloud_target->height   = 1;
    cloud_target->is_dense = false;
    cloud_target->points.resize (cloud_target->width * cloud_target->height);
    for (size_t i = 0; i < cloud_target->points.size (); ++i)
        {
            cloud_target->points[i].x = target->points[i].x;
            cloud_target->points[i].y = target->points[i].y;
            cloud_target->points[i].z = 0;
        }

    cloud_match->width    = match->npoints;
    cloud_match->height   = 1;
    cloud_match->is_dense = false;
    cloud_match->points.resize (cloud_match->width * cloud_match->height);
    for (size_t i = 0; i < cloud_match->points.size (); ++i)
        {
            cloud_match->points[i].x = match->points[i].x;
            cloud_match->points[i].y = match->points[i].y;
            cloud_match->points[i].z = 0;
        }
    
    
    
    /*int iter = 500;
    double rans = 0.5; //was 0.05
    double dist = 0.5;
    */
    pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;
    
    /*icp.setMaximumIterations (iter);
    icp.setMaxCorrespondenceDistance (dist);
    icp.setRANSACOutlierRejectionThreshold (rans);
    */
  
    icp.setInputTarget(cloud_target);
    icp.setInputCloud(cloud_match);

    pcl::PointCloud<pcl::PointXYZ> final;
    
    icp.setMaximumIterations (2);
    Eigen::Matrix4f Ti = Eigen::Matrix4f::Identity (), prev, targetToSource;
    
    pcl::PointCloud<pcl::PointXYZ>::Ptr reg_result = cloud_match;
    for (int i = 0; i < 30; ++i){
        PCL_INFO ("Iteration Nr. %d.\n", i);
        
        cloud_match = reg_result;
        icp.setInputCloud(cloud_match);
        //call icp
        icp.align(*reg_result);

        Ti = icp.getFinalTransformation () * Ti;
        
        /*if (fabs ((icp.getLastIncrementalTransformation () - prev).sum ()) < icp.getTransformationEpsilon ())
            icp.setMaxCorrespondenceDistance (icp.getMaxCorrespondenceDistance () - 0.001);
        */
        prev = icp.getLastIncrementalTransformation ();

        std::cout << "has converged:" << icp.hasConverged() << " score: " <<
            icp.getFitnessScore() << std::endl;
        
        std::cout << icp.getFinalTransformation() << std::endl;
    }

    final = *reg_result;

    std::cout << "Final Transform : " << Ti << std::endl;

    if(!icp.hasConverged())
        return NULL;
    
    std::cout << "has converged:" << icp.hasConverged() << " score: " <<
         icp.getFitnessScore() << std::endl;
    std::cout << icp.getFinalTransformation() << std::endl;

    Eigen::Matrix4f tf = icp.getFinalTransformation(); 

    for(int i=0; i < 3; i++){
        for(int j=0; j<4; j++){
            transform[i*4 + j] = tf(i,j);
        }
    }

    pointlist2d_t *result = pointlist2d_new(final.points.size());

    for (size_t i = 0; i < final.points.size (); ++i)
        {
            result->points[i].x = final.points[i].x;
            result->points[i].y = final.points[i].y;
        }


    std::cout << "has converged:" << icp.hasConverged() << " score: " <<
        icp.getFitnessScore() << std::endl;
    std::cout << icp.getFinalTransformation() << std::endl;
    
    cloud_target->points.resize(0);
    cloud_match->points.resize(0);
    final.points.resize(0);
    
    return result;
    //delete cloud_target;
    //delete cloud_match;
}


/*
  pointlist2d_t* doPCLScanMatchWithTransform(pointlist2d_t *target, pointlist2d_t *match, double transform[12]){
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_target (new pcl::PointCloud<pcl::PointXYZ>); //target scan 
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_match (new pcl::PointCloud<pcl::PointXYZ>); //the one that should be transformed to match the target scan 
    
    cloud_target->width    = target->npoints; 
    cloud_target->height   = 1;
    cloud_target->is_dense = false;
    cloud_target->points.resize (cloud_target->width * cloud_target->height);
    for (size_t i = 0; i < cloud_target->points.size (); ++i)
        {
            cloud_target->points[i].x = target->points[i].x;
            cloud_target->points[i].y = target->points[i].y;
            cloud_target->points[i].z = 0;
        }

    cloud_match->width    = match->npoints;
    cloud_match->height   = 1;
    cloud_match->is_dense = false;
    cloud_match->points.resize (cloud_match->width * cloud_match->height);
    for (size_t i = 0; i < cloud_match->points.size (); ++i)
        {
            cloud_match->points[i].x = match->points[i].x;
            cloud_match->points[i].y = match->points[i].y;
            cloud_match->points[i].z = 0;
        }
    
    
    pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;
    
    //icp.setMaximumIterations (iter);
    //icp.setMaxCorrespondenceDistance (dist);
    //icp.setRANSACOutlierRejectionThreshold (rans);
    
  
    icp.setInputTarget(cloud_target);
    icp.setInputCloud(cloud_match);

    pcl::PointCloud<pcl::PointXYZ> final;

    //call icp
    icp.align(final);

    if(!icp.hasConverged())
        return NULL;
    
    std::cout << "has converged:" << icp.hasConverged() << " score: " <<
         icp.getFitnessScore() << std::endl;
    std::cout << icp.getFinalTransformation() << std::endl;

    Eigen::Matrix4f tf = icp.getFinalTransformation(); 

    for(int i=0; i < 3; i++){
        for(int j=0; j<4; j++){
            transform[i*4 + j] = tf(i,j);
        }
    }

    pointlist2d_t *result = pointlist2d_new(final.points.size());

    for (size_t i = 0; i < final.points.size (); ++i)
        {
            result->points[i].x = final.points[i].x;
            result->points[i].y = final.points[i].y;
        }


    std::cout << "has converged:" << icp.hasConverged() << " score: " <<
        icp.getFitnessScore() << std::endl;
    std::cout << icp.getFinalTransformation() << std::endl;
    
    cloud_target->points.resize(0);
    cloud_match->points.resize(0);
    final.points.resize(0);
    
    return result;
    //delete cloud_target;
    //delete cloud_match;
}
  
 */

/*
      double dist = 0.05;
  pcl::console::parse_argument (argc, argv, "-d", dist);

  double rans = 0.05;
  pcl::console::parse_argument (argc, argv, "-r", rans);

  int iter = 50;
  pcl::console::parse_argument (argc, argv, "-i", iter);

  std::vector<int> pcd_indices;
  pcd_indices = pcl::console::parse_file_extension_argument (argc, argv, ".pcd");

  CloudPtr model (new Cloud);
  if (pcl::io::loadPCDFile (argv[pcd_indices[0]], *model) == -1)
  {
    std::cout << "Could not read file" << std::endl;
    return -1;
  }
  std::cout << argv[pcd_indices[0]] << " width: " << model->width << " height: " << model->height << std::endl;

  std::string result_filename (argv[pcd_indices[0]]);
  result_filename = result_filename.substr (result_filename.rfind ("/") + 1);
  pcl::io::savePCDFile (result_filename.c_str (), *model);
  std::cout << "saving first model to " << result_filename << std::endl;

  Eigen::Matrix4f t (Eigen::Matrix4f::Identity ());

  for (size_t i = 1; i < pcd_indices.size (); i++)
  {
    CloudPtr data (new Cloud);
    if (pcl::io::loadPCDFile (argv[pcd_indices[i]], *data) == -1)
    {
      std::cout << "Could not read file" << std::endl;
      return -1;
    }
    std::cout << argv[pcd_indices[i]] << " width: " << data->width << " height: " << data->height << std::endl;

    pcl::IterativeClosestPointNonLinear<PointType, PointType> icp;

    boost::shared_ptr<pcl::WarpPointRigid3D<PointType, PointType> > warp_fcn 
      (new pcl::WarpPointRigid3D<PointType, PointType>);

    // Create a TransformationEstimationLM object, and set the warp to it
    boost::shared_ptr<pcl::registration::TransformationEstimationLM<PointType, PointType> > te (new pcl::registration::TransformationEstimationLM<PointType, PointType>);
    te->setWarpFunction (warp_fcn);

    // Pass the TransformationEstimation objec to the ICP algorithm
    icp.setTransformationEstimation (te);

    icp.setMaximumIterations (iter);
    icp.setMaxCorrespondenceDistance (dist);
    icp.setRANSACOutlierRejectionThreshold (rans);

    icp.setInputTarget (model);

    icp.setInputCloud (data);

    CloudPtr tmp (new Cloud);
    icp.align (*tmp);

    t = icp.getFinalTransformation () * t;

    pcl::transformPointCloud (*data, *tmp, t);

    std::cout << icp.getFinalTransformation () << std::endl;

    *model = *data;

    std::string result_filename (argv[pcd_indices[i]]);
    result_filename = result_filename.substr (result_filename.rfind ("/") + 1);
    pcl::io::savePCDFileBinary (result_filename.c_str (), *tmp);
    std::cout << "saving result to " << result_filename << std::endl;

     */
