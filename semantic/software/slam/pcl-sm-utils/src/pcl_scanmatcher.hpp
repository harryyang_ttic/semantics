#ifndef PCL_SCANMATCHER_H
#define PCL_SCANMATCHER_H

#include <iostream>
#include <pcl/point_types.h>
#include <pcl/registration/icp.h>
#include <pcl/registration/icp_nl.h>
#include <pcl/registration/transformation_estimation_lm.h>
#include <pcl/registration/warp_point_rigid_3d.h>
#include "pcl/common/pca.h"
#include <bot_core/bot_core.h>
#include <lcm/lcm.h>
#include <geom_utils/geometry.h>
#include <bot_lcmgl_client/lcmgl.h>
#include <GL/gl.h>

/*#ifdef __cplusplus
extern "C" {
#endif*/

typedef struct _icp_result_t{
    double angle;
    //Eigen::Matrix4f Tf;
    BotTrans Tf; 
    double score; 
    int n_match;
    int converged; 
} icp_result_t;

//get the transformed points out as well 
pointlist2d_t *doScanMatchPCL(pointlist2d_t *target, pointlist2d_t *match, 
                                     BotTrans target_to_pf, 
                                     BotTrans match_to_pf, 
                                     BotTrans *sm_to_target, //resulting transform 
                                     double *fitness_score, 
                                     int *matches, 
                                     bot_lcmgl_t *lcmgl_target, 
                                     bot_lcmgl_t *lcmgl_match, 
                                     bot_lcmgl_t *lcmgl_result, 
                                     double max_corr_dist, 
                                     int align_scans, //use PCA to align the scans 
                                     int *converged);

void print_trans(BotTrans tf, char *name);

/*#ifdef __cplusplus
}
#endif*/

#endif

