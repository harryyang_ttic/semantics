#include "pcl_scanmatcher.hpp"
#include "icp_mod.h"

void print_trans(BotTrans tf, char *name){

    double rpy[3];
    bot_quat_to_roll_pitch_yaw (tf.rot_quat, rpy);   

    fprintf(stderr, "Transform : %s => %f,%f, %f\n", name, tf.trans_vec[0], 
            tf.trans_vec[1],  rpy[2]);            

}

pointlist2d_t * doScanMatchPCL(pointlist2d_t *target, pointlist2d_t *match, 
                                     //match and target points are in the pf reference frame - maybe they should 
                                     //be in their own reference frames - might make it clearer
                                     BotTrans target_to_pf, 
                                     BotTrans match_to_pf, 
                                     BotTrans *result_sm_to_target, //resulting transform 
                                     double *fitness_score, 
                                     int *matches, 
                                     bot_lcmgl_t *lcmgl_target, 
                                     bot_lcmgl_t *lcmgl_match, 
                                     bot_lcmgl_t *lcmgl_result, 
                                     double max_corr_dist, 
                                     int align_scans, //use PCA to align the scans 
                                     int *converged){

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_target (new pcl::PointCloud<pcl::PointXYZ>); //target scan 
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_match (new pcl::PointCloud<pcl::PointXYZ>); //the one that should be transformed to match the target scan 

    cloud_target->width    = target->npoints; 
    cloud_target->height   = 1;
    cloud_target->is_dense = false;
    cloud_target->points.resize (cloud_target->width * cloud_target->height);

    double mx_t = 0, my_t = 0;

    //put the stuff in to a point cloud - and then run pca on them 
    double p_1[3] = {0}, tf_p_1[3];
    for (size_t i = 0; i < target->npoints; ++i){
        p_1[0] = target->points[i].x;
        p_1[1] = target->points[i].y;
        bot_trans_apply_vec(&target_to_pf, p_1, tf_p_1);
        cloud_target->points[i].x = tf_p_1[0];
        cloud_target->points[i].y = tf_p_1[1];
        cloud_target->points[i].z = 0;
    }

    pcl::PCA<pcl::PointXYZ>pca;
    pca.setInputCloud(cloud_target->makeShared());
    
    Eigen::Vector4f xyz = pca.getMean();
    Eigen::MatrixXf eigen_vectors = pca.getEigenVectors();
    Eigen::VectorXf eigen_values = pca.getEigenValues();
    std::cout << "PCA Mean : " << xyz << std::endl;

    mx_t = xyz(0);
    my_t = xyz(1);

    print_trans(target_to_pf, "Target to PF");
    print_trans(target_to_pf, "Match to PF");

    //angle 
    double target_tf_angle = atan2(eigen_vectors(1,0), eigen_vectors(0,0));

    BotTrans pf_to_target = target_to_pf;
    bot_trans_invert(&pf_to_target);

    BotTrans pf_to_tf_target1;
    
    //first move and then rotate (BotTrans first rotates and then moves) 

    pf_to_tf_target1.trans_vec[0] = -mx_t;
    pf_to_tf_target1.trans_vec[1] = -my_t;
    pf_to_tf_target1.trans_vec[2] = 0;

    double rpy_pf_to_tf_target1[3] = {.0,.0, 0};
    bot_roll_pitch_yaw_to_quat (rpy_pf_to_tf_target1,  pf_to_tf_target1.rot_quat);   

    BotTrans tf_target1_to_tf_target;
    
    tf_target1_to_tf_target.trans_vec[0] = 0;
    tf_target1_to_tf_target.trans_vec[1] = 0;
    tf_target1_to_tf_target.trans_vec[2] = 0;

    double rpy_target_to_tf_target[3] = {.0,.0, 0};
    if(align_scans){
        rpy_target_to_tf_target[2] = -target_tf_angle;
    }
    bot_roll_pitch_yaw_to_quat (rpy_target_to_tf_target,  tf_target1_to_tf_target.rot_quat);   

    //this is the actual transform we would require 
    
    BotTrans pf_to_tf_target;     
    bot_trans_apply_trans_to(&tf_target1_to_tf_target, &pf_to_tf_target1, &pf_to_tf_target);
    
    BotTrans target_to_tf_target;     
    bot_trans_apply_trans_to(&pf_to_tf_target, &target_to_pf, &target_to_tf_target);
    
    BotTrans tf_target_to_target = target_to_tf_target;  
    bot_trans_invert(&tf_target_to_target);

    //apply transform to the target 
    double pos_1[3] = {0}, trans_pos_1[3];
    for (size_t i = 0; i < target->npoints; ++i){
        pos_1[0] = target->points[i].x;
        pos_1[1] = target->points[i].y;
        bot_trans_apply_vec(&target_to_tf_target, pos_1, trans_pos_1);
        
        cloud_target->points[i].x = trans_pos_1[0];
        cloud_target->points[i].y = trans_pos_1[1];
        cloud_target->points[i].z = 0;
    }

    cloud_match->width    = match->npoints;
    cloud_match->height   = 1;
    cloud_match->is_dense = false;
    cloud_match->points.resize (cloud_match->width * cloud_match->height);

    double mx_m = 0, my_m = 0;
    
    pcl::IterativeClosestPointMod<pcl::PointXYZ, pcl::PointXYZ> icp;
    pcl::PointCloud<pcl::PointXYZ>::Ptr orig_match = cloud_match;;
    pcl::PointCloud<pcl::PointXYZ> final; // = cloud_match;

    int draw_act = 1;//1;

    //add a way to decide which is the best solution - and then finally - draw that solution 

    int angle_size = 8;
    std::vector<icp_result_t> results;

    int nr = 0;
    double max_dist = max_corr_dist;
    
    double p_2[3] = {0}, tf_p_2[3];

    for (size_t i = 0; i < match->npoints; ++i){
        p_2[0] = match->points[i].x;
        p_2[1] = match->points[i].y;

        bot_trans_apply_vec(&match_to_pf, p_2, tf_p_2);
        
        cloud_match->points[i].x = tf_p_2[0];
        cloud_match->points[i].y = tf_p_2[1];
        cloud_match->points[i].z = 0;
    }

    pcl::PCA<pcl::PointXYZ>pca1;
    pca1.setInputCloud(cloud_match->makeShared());
    
    Eigen::Vector4f xyz_m = pca1.getMean();
    Eigen::MatrixXf eigen_vectors_m = pca1.getEigenVectors();
    Eigen::VectorXf eigen_values_m = pca1.getEigenValues();

    mx_m = xyz_m(0);
    my_m = xyz_m(1);
        
    double match_tf_angle = atan2(eigen_vectors_m(1,0), eigen_vectors_m(0,0));
    //fprintf(stderr, "Match Eigen Transform angle : %f\n", bot_to_degrees(match_tf_angle));
    //std::cout << "PCA Mean : " << xyz_m << std::endl;
    //std::cout << "Eigen Vectors : " << eigen_vectors_m << std::endl;
    //std::cout << "Eigen Values : " << eigen_values_m << std::endl;

    //do the same for the match point clouds (however there are three steps here - which take it to the center 
    //and the rotate it again - to see which best scanmatches) 

    BotTrans pf_to_tf_match_m;
    
    pf_to_tf_match_m.trans_vec[0] = -mx_m;
    pf_to_tf_match_m.trans_vec[1] = -my_m;
    pf_to_tf_match_m.trans_vec[2] = 0;

    double rpy_pf_to_tf_match_m[3] = {.0,.0,.0};
    bot_roll_pitch_yaw_to_quat (rpy_pf_to_tf_match_m,  pf_to_tf_match_m.rot_quat);

    BotTrans tf_match_m_to_tf_match_1;
    
    tf_match_m_to_tf_match_1.trans_vec[0] = 0;
    tf_match_m_to_tf_match_1.trans_vec[1] = 0;
    tf_match_m_to_tf_match_1.trans_vec[2] = 0;

    double rpy_match_to_tf_match_1[3] = {.0,.0, 0};
    if(align_scans){
        rpy_match_to_tf_match_1[2] = -match_tf_angle;
    }

    bot_roll_pitch_yaw_to_quat (rpy_match_to_tf_match_1,  tf_match_m_to_tf_match_1.rot_quat);

    BotTrans pf_to_tf_match_1;

    bot_trans_apply_trans_to(&tf_match_m_to_tf_match_1, &pf_to_tf_match_m, &pf_to_tf_match_1);

    //draw the target
    if(lcmgl_target){
        if(draw_act){
            bot_lcmgl_color3f(lcmgl_target, 0, .0, .5);
            bot_lcmgl_point_size(lcmgl_target, 2);
            bot_lcmgl_begin(lcmgl_target, GL_POINTS);

            double pos_target[3] = {0}, pos_pf[3];
            for (size_t i = 0; i < target->npoints; ++i){
                pos_target[0] = target->points[i].x;
                pos_target[1] = target->points[i].y;
                bot_trans_apply_vec(&target_to_pf, pos_target, pos_pf);
                bot_lcmgl_vertex3f(lcmgl_target, pos_pf[0], pos_pf[1], 0);
            }
            
            bot_lcmgl_end(lcmgl_target);
        }
        else{
            bot_lcmgl_color3f(lcmgl_target, 0, .0, 1.0);
            bot_lcmgl_point_size(lcmgl_target, 2);
            bot_lcmgl_begin(lcmgl_target, GL_POINTS);
            
            for (size_t i = 0; i < cloud_target->points.size (); ++i){
                bot_lcmgl_vertex3f(lcmgl_target, cloud_target->points[i].x, cloud_target->points[i].y, 0);
            }
            
            bot_lcmgl_end(lcmgl_target);
        }
        bot_lcmgl_switch_buffer(lcmgl_target);
    }
    
    for(int k = 0; k < angle_size; k++){        

        Eigen::Matrix4f Ti = Eigen::Matrix4f::Identity ();

        double w = 2 * M_PI /angle_size * (k+1);

        //figure out the last rotation - of the match 
        BotTrans tf_match_1_to_tf_match;
    
        tf_match_1_to_tf_match.trans_vec[0] = 0;
        tf_match_1_to_tf_match.trans_vec[1] = 0;
        tf_match_1_to_tf_match.trans_vec[2] = 0;
        
        double rpy_tf_match_1_to_tf_match[3] = {.0,.0, w};
        bot_roll_pitch_yaw_to_quat (rpy_tf_match_1_to_tf_match, tf_match_1_to_tf_match.rot_quat);

        BotTrans pf_to_tf_match;
        bot_trans_apply_trans_to(&tf_match_1_to_tf_match, &pf_to_tf_match_1, &pf_to_tf_match);
                
        icp_result_t res; 
        res.angle = w;

        BotTrans match_to_tf_match;     
        bot_trans_apply_trans_to(&pf_to_tf_match, &match_to_pf, &match_to_tf_match);

        //transform the points 
        double pos_2[3] = {0}, trans_pos_2[3];
        for (size_t i = 0; i < match->npoints; ++i){
            pos_2[0] = match->points[i].x;
            pos_2[1] = match->points[i].y;
            bot_trans_apply_vec(&match_to_tf_match, pos_2, trans_pos_2);
            
            cloud_match->points[i].x = trans_pos_2[0];
            cloud_match->points[i].y = trans_pos_2[1];
            cloud_match->points[i].z = 0;
        }

        //draw the points 
        if(lcmgl_match){
            if(draw_act){
                double pos[3] = {0}, trans_pos[3];

                bot_lcmgl_color3f(lcmgl_match, 1.0, .0, .0);
                bot_lcmgl_point_size(lcmgl_match, 2);
                bot_lcmgl_begin(lcmgl_match, GL_POINTS);

                for (size_t i = 0; i < match->npoints; ++i)
                    {
                        pos[0] = match->points[i].x;
                        pos[1] = match->points[i].y;
                        bot_trans_apply_vec(&match_to_pf, pos, trans_pos);

                        bot_lcmgl_vertex3f(lcmgl_match, trans_pos[0], trans_pos[1], 0);
                     }
                bot_lcmgl_end(lcmgl_match);
            }
            else{
                bot_lcmgl_color3f(lcmgl_match, 1.0, .0, .0);
                bot_lcmgl_point_size(lcmgl_match, 2);
                bot_lcmgl_begin(lcmgl_match, GL_POINTS);
                
                for (size_t i = 0; i < cloud_match->points.size (); ++i)
                    {
                        bot_lcmgl_vertex3f(lcmgl_match, cloud_match->points[i].x, cloud_match->points[i].y, 0);
                    }
                bot_lcmgl_end(lcmgl_match);
            }

            bot_lcmgl_switch_buffer(lcmgl_match);
        }
        
        //max distance at which to consider a correspondance 
        double max_c_dist = 3.0;//3.0;//10.0;
        icp.setMaxCorrespondenceDistance (max_c_dist);
        //no of ransac iters 
        icp.setRANSACIterations(200); //was at 10


        //icp.setMaximumIterations (40); //was at 20
    
        icp.setTransformationEpsilon(1e-8);
        icp.setEuclideanFitnessEpsilon (1e-5);
        Eigen::Matrix4f prev;
        double ransac_threshold = 2.0;//2.0;//0.5; 

        //rejection threshold for outliers
        icp.setRANSACOutlierRejectionThreshold (ransac_threshold);
        
        //set the target and the input clouds 
        icp.setInputTarget(cloud_target);
        icp.setInputCloud(cloud_match);
        
        // Set the maximum number of iterations (criterion 1)
        icp.setMaximumIterations (40); //was at 20
        int nr_basic = 0;
        
        int v = 0;
        //run icp (will try to align the cloud_match to cloud_target 
        //the transform will take cloud match to the scanmatched location 
        icp.align(final);
        Ti = icp.getFinalTransformation () * Ti;
        
        if(icp.hasConverged()){
            res.converged = 1;
        }

        //this transform doesnt make much sense for now - since its in the frame of the transformed points 
        std::cout << "Final Transform : \n" << Ti << std::endl; 

        BotTrans tf_sm_to_tf_target;
        tf_sm_to_tf_target.trans_vec[0] = Ti(0,3);
        tf_sm_to_tf_target.trans_vec[1] = Ti(1,3);
        tf_sm_to_tf_target.trans_vec[2] = 0;

        double angle = atan2(-Ti(0,1), Ti(0,0));

        double rpy1[3] = {0,0, angle};
        bot_roll_pitch_yaw_to_quat (rpy1, tf_sm_to_tf_target.rot_quat);
        
        //this part sounds iffy 
        BotTrans sm_to_tf_sm = match_to_tf_match; //this is the same transform that took the match to the transformed frame 

        BotTrans sm_to_tf_target; 
        bot_trans_apply_trans_to(&tf_sm_to_tf_target, &sm_to_tf_sm, &sm_to_tf_target);

        BotTrans sm_to_target; 
        bot_trans_apply_trans_to(&tf_target_to_target, &sm_to_tf_target, &sm_to_target);

        BotTrans sm_to_pf;
        bot_trans_apply_trans_to(&target_to_pf, &sm_to_target, &sm_to_pf);

        double f_score = icp.getFitnessScoreWithCount( 0.2, &nr);
        res.Tf = sm_to_pf; //match_sm_to_target;//target_to_act;
        res.score = f_score;
        res.n_match = nr; 
        results.push_back(res);
        
        if(lcmgl_result){

            if(draw_act){
                bot_lcmgl_color3f(lcmgl_result, 0.5, 0.5, .0);
                bot_lcmgl_point_size(lcmgl_result, 5);
                bot_lcmgl_begin(lcmgl_result, GL_POINTS);
                
                double pos[3] = {0}, trans_pos[3];
                                
                for(int i = 0; i < match->npoints ; i++){
                    pos[0] = match->points[i].x;
                    pos[1] = match->points[i].y;
                    bot_trans_apply_vec(&sm_to_pf, pos, trans_pos);
                    bot_lcmgl_vertex3f(lcmgl_result, trans_pos[0], trans_pos[1], 0);
                }
                bot_lcmgl_end(lcmgl_result);
                char match_info[512];

                pos[0] = match->points[0].x;
                pos[1] = match->points[0].y;
                
                bot_trans_apply_vec(&sm_to_pf, pos, trans_pos);
                
                sprintf(match_info,"Match : %d Score : %f", nr, f_score);
                double text_pos[3] = {trans_pos[0] + 5, trans_pos[1] + 5,0};
                bot_lcmgl_text(lcmgl_result, text_pos, match_info);
            }

            else{
                bot_lcmgl_color3f(lcmgl_result, 0.5, .0, .5);
                bot_lcmgl_point_size(lcmgl_result, 5);
                bot_lcmgl_begin(lcmgl_result, GL_POINTS);
                
                for (size_t i = 0; i < final.points.size (); ++i){
                    bot_lcmgl_vertex3f(lcmgl_result, final.points[i].x, final.points[i].y, 0);
                }
                bot_lcmgl_end(lcmgl_result);
                
                char match_info[512];
                sprintf(match_info,"Match : %d Score : %f", nr, f_score);
                double text_pos[3] = {final.points[0].x, final.points[0].y + 5,0};
                bot_lcmgl_text(lcmgl_result, text_pos, match_info);
            }
            bot_lcmgl_switch_buffer(lcmgl_result);
        }
       
        if(0){
            std::cout << "Sleeping \n" ;
            sleep(2);
        }
    }

    double f_score = 0; 
    int max_matched = 0;
    int matched_id = -1;
 
    for(int i=0; i < results.size(); i++){
        icp_result_t r = results.at(i);
        fprintf(stderr, "Angle : %f -> Score : %f Matched : %d Converged : %d\n", 
                bot_to_degrees(r.angle), 
                r.score, 
                r.n_match, 
                r.converged);
        
        if( r.converged && max_matched < r.n_match){
            matched_id = i;
            max_matched = r.n_match; 
            f_score = r.score;
        }

        double rpy_act[3]; 
        bot_quat_to_roll_pitch_yaw (r.Tf.rot_quat, rpy_act);

        fprintf(stderr, "Transform : %f, %f - %f\n", r.Tf.trans_vec[0], r.Tf.trans_vec[1], bot_to_degrees(rpy_act[2]));
    }

    *fitness_score = f_score;
    *matches = max_matched; 

    
    pointlist2d_t *result_points = NULL;
    
    if(matched_id >=0){
        fprintf(stderr, "Found Valid Match\n");
        *converged = 1;

        result_points = pointlist2d_new(match->npoints);

        BotTrans result = results.at(matched_id).Tf;

        BotTrans sm_to_pf = result;
        
        BotTrans sm_to_target; //this is what we want 
        bot_trans_apply_trans_to(&pf_to_target,  &sm_to_pf, &sm_to_target);
        
        double rpy_result[3]; 
        bot_quat_to_roll_pitch_yaw (result.rot_quat, rpy_result);

        *result_sm_to_target = sm_to_target;

        print_trans(result, "\n\n Matched Result (sm to pf) Transform");

        print_trans(sm_to_target, "\n\n Matched Result (sm to target) Transform");

        print_trans(*result_sm_to_target, "\n\n Returned Result (sm to pf) Transform");

        double pos[3] = {0}, trans_pos[3];
        for(int i=0; i < match->npoints; i++){
            pos[0] = match->points[i].x;
            pos[1] = match->points[i].y;
            bot_trans_apply_vec(&result, pos, trans_pos);
            result_points->points[i].x = trans_pos[0];
            result_points->points[i].y = trans_pos[1];
        }

        if(lcmgl_result){
            if(max_matched > 700 || max_matched / (double)  match->npoints > 0.5 ){
                bot_lcmgl_color3f(lcmgl_result, 0.5, 1.0, .0);
            }
            else{
                bot_lcmgl_color3f(lcmgl_result, 1.0, 0.0, .0);
            }

            bot_lcmgl_point_size(lcmgl_result, 5);
            bot_lcmgl_begin(lcmgl_result, GL_POINTS);
            
            fprintf(stderr, "ICP - (PCL) Drawing result +++++ \n");
            
            for(int i = 0; i < result_points->npoints ; i++){
                bot_lcmgl_vertex3f(lcmgl_result, result_points->points[i].x, result_points->points[i].y, 0);
            }

            bot_lcmgl_end(lcmgl_result);

            pos[0] = match->points[0].x;
            pos[1] = match->points[0].y;
            
            bot_trans_apply_vec(&result, pos, trans_pos);

            char match_info[512];
            sprintf(match_info,"Match : %d Score : %f", results.at(matched_id).n_match, results.at(matched_id).score);
            double text_pos[3] = {trans_pos[0]+5.0, trans_pos[1] + 5,0};
            bot_lcmgl_text(lcmgl_result, text_pos, match_info);
            
            bot_lcmgl_switch_buffer(lcmgl_result);
        }
    }
    else{
        fprintf(stderr, "Failed to find match\n");
    }

    std::cout << "Rough iteration has converged:" << *converged << " score: " <<
         *fitness_score << std::endl;
        
    fprintf(stderr, "Found Matches : %d out of : %d\n", max_matched , match->npoints);

    //if overlap is less than 50% of if the count is less than 3 * no of scans (assumed to be 250 points) 

    int valid_overlap = 0;
    
    if((max_matched / (double) match->npoints) > 0.5  && max_matched > 500)
        valid_overlap = 1;

    if(matched_id == -1){// || fitness_score > 0.05 || valid_overlap == 0){
        cloud_target->points.resize(0);
        cloud_match->points.resize(0);
        orig_match->points.resize(0);
        return result_points; 
    }
    
    cloud_target->points.resize(0);
    cloud_match->points.resize(0);
    orig_match->points.resize(0);
    
    return result_points;
}
