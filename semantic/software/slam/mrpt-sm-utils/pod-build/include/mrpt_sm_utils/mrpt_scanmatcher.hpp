#ifndef MRPT_SCANMATCHER_H
#define MRPT_SCANMATCHER_H

#include <mrpt/slam.h>

#include <iostream>
#include <fstream>
#include <bot_core/bot_core.h>
#include <lcm/lcm.h>
#include <geom_utils/geometry.h>
#include <bot_lcmgl_client/lcmgl.h>
#include <GL/gl.h>

typedef struct _icp_result_t{
    double angle;
    int flipped; 
    //Eigen::Matrix4f Tf;
    BotTrans Tf; 
    double score; 
    int n_match;
    int converged; 
} icp_result_t;


//variance - row major 
pointlist2d_t *scanmatchRegionsMRPT(pointlist2d_t *target, pointlist2d_t *match, 
                                    BotTrans &result_sm_to_target, //resulting transform 
                                    double *fitness_score, 
                                    int *matches, double variance[9],
                                    bot_lcmgl_t *lcmgl_target = NULL, 
                                    bot_lcmgl_t *lcmgl_match = NULL, 
                                    bot_lcmgl_t *lcmgl_result = NULL);

//variance - row major 
pointlist2d_t *scanmatchRegionsMRPTQuick(pointlist2d_t *target, pointlist2d_t *match, 
                                    BotTrans &result_sm_to_target, //resulting transform 
                                    double *fitness_score, 
                                    int *matches, double variance[9],
                                    bot_lcmgl_t *lcmgl_target = NULL, 
                                    bot_lcmgl_t *lcmgl_match = NULL, 
                                    bot_lcmgl_t *lcmgl_result = NULL);

pointlist2d_t * scanmatchRegionsExtensiveMRPT(pointlist2d_t *p1, pointlist2d_t *p2, 
                                              BotTrans p1_to_pf, 
                                              BotTrans p2_to_pf, 
                                              BotTrans *result_sm_to_target,
                                              double *fitness_score, 
                                              double variance[9], 
                                              int align_scans, //use PCA to align the scans 
                                              double min_acceptance_threshold, 
                                              double break_threshold, 
                                              int pause, 
                                              int draw_failed,
                                              bot_lcmgl_t *lcmgl_target = NULL, 
                                              bot_lcmgl_t *lcmgl_match = NULL, 
                                              bot_lcmgl_t *lcmgl_result = NULL);

//get the transformed points out as well 
pointlist2d_t *doScanMatchMRPT(pointlist2d_t *target, pointlist2d_t *match, 
                               BotTrans target_to_pf, 
                               BotTrans match_to_pf, 
                               BotTrans *sm_to_target, //resulting transform 
                               double *fitness_score, 
                               int *matches, 
                               bot_lcmgl_t *lcmgl_target, 
                               bot_lcmgl_t *lcmgl_match, 
                               bot_lcmgl_t *lcmgl_result, 
                               double max_corr_dist, 
                               int align_scans, //use PCA to align the scans 
                               int *converged);

pointlist2d_t *doScanMatchMRPTFull(pointlist2d_t *target, pointlist2d_t *match, 
                                   BotTrans target_to_pf, 
                                   BotTrans match_to_pf, 
                                   BotTrans *sm_to_target, //resulting transform 
                                   double *fitness_score, 
                                   //int *matches, 
                                   bot_lcmgl_t *lcmgl_target, 
                                   bot_lcmgl_t *lcmgl_match, 
                                   bot_lcmgl_t *lcmgl_result, 
                                   //double max_corr_dist, 
                                   int align_scans, //use PCA to align the scans 
                                   double min_acceptance_threshold, 
                                   double break_threshold, 
                                   //int *converged, 
                                   int pause, 
                                   int draw_failed);

void print_trans(BotTrans tf, char *name);

#endif

