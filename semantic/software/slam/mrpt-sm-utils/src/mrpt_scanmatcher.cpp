#include "mrpt_scanmatcher.hpp"

using namespace mrpt;
using namespace mrpt::utils;
using namespace mrpt::slam;
using namespace std;

bool skip_window=false;
int  ICP_method = (int) icpClassic;

void print_trans(BotTrans tf, char *name){

    double rpy[3];
    bot_quat_to_roll_pitch_yaw (tf.rot_quat, rpy);   

    fprintf(stderr, "Transform : %s => %f,%f, %f\n", name, tf.trans_vec[0], 
            tf.trans_vec[1],  rpy[2]);            

}

pointlist2d_t * doScanMatchMRPT(pointlist2d_t *target, pointlist2d_t *match, 
                                //match and target points are in the pf reference frame - maybe they should 
                                //be in their own reference frames - might make it clearer
                                BotTrans target_to_pf, 
                                BotTrans match_to_pf, 
                                BotTrans *result_sm_to_target, //resulting transform 
                                double *fitness_score, 
                                int *matches, 
                                bot_lcmgl_t *lcmgl_target, 
                                bot_lcmgl_t *lcmgl_match, 
                                bot_lcmgl_t *lcmgl_result, 
                                double max_corr_dist, 
                                int align_scans, //use PCA to align the scans 
                                int *converged){

    CSimplePointsMap		m1,m2;
    float					runningTime;
    CICP::TReturnInfo		info;
    CICP					ICP;

    ICP.options.ICP_algorithm = (TICPAlgorithm)ICP_method;

    ICP.options.maxIterations			= 500;
    ICP.options.thresholdAng			= DEG2RAD(40.0f); //10.0f
    ICP.options.thresholdDist			= 20.0f;//10.0f//0.75f;
    ICP.options.ALFA					= 0.5f;
    ICP.options.smallestThresholdDist	= 0.05f;
    ICP.options.doRANSAC = true;//false; //?? True or false??
    
    //ICP.options.dumpToConsole();

    CPose2D		initialPose(0.f,0.0f,(float)DEG2RAD(0.0f));

    pointlist2d_t *cloud_target = pointlist2d_new(target->npoints);
    pointlist2d_t *cloud_match = pointlist2d_new(match->npoints);

    int draw_tf = 0;
    
    double mx_t = 0, my_t = 0;

    //put the stuff in to a point cloud - and then run pca on them 
    double p_1[3] = {0}, tf_p_1[3];
    
    for (size_t i = 0; i < target->npoints; ++i){
        p_1[0] = target->points[i].x;
        p_1[1] = target->points[i].y;
        bot_trans_apply_vec(&target_to_pf, p_1, tf_p_1);
        cloud_target->points[i].x = tf_p_1[0];
        cloud_target->points[i].y = tf_p_1[1];
    }

    //print_trans(target_to_pf, "Target to PF");
    //print_trans(match_to_pf, "Match to PF");

    int draw_act = 1;

    std::vector<icp_result_t> results;

    int nr = 0;
    
    double p_2[3] = {0}, tf_p_2[3];

    for (size_t i = 0; i < match->npoints; ++i){
        p_2[0] = match->points[i].x;
        p_2[1] = match->points[i].y;

        bot_trans_apply_vec(&match_to_pf, p_2, tf_p_2);
        
        cloud_match->points[i].x = tf_p_2[0];
        cloud_match->points[i].y = tf_p_2[1];
    }

    if(lcmgl_target){
        bot_lcmgl_color3f(lcmgl_target, 0, .0, .5);
        bot_lcmgl_point_size(lcmgl_target, 2);
        bot_lcmgl_begin(lcmgl_target, GL_POINTS);
        
        double pos_target[3] = {0}, pos_pf[3];
        for (size_t i = 0; i < cloud_target->npoints; ++i){
            pos_target[0] = cloud_target->points[i].x;
            pos_target[1] = cloud_target->points[i].y;
            bot_trans_apply_vec(&target_to_pf, pos_target, pos_pf);
            bot_lcmgl_vertex3f(lcmgl_target, pos_pf[0], pos_pf[1], 0);
        }
        
        bot_lcmgl_end(lcmgl_target);
        bot_lcmgl_switch_buffer(lcmgl_target);
    }

    //insert the points 
    for(int i=0; i < cloud_target->npoints ; i++){
        m2.insertPointFast(cloud_target->points[i].x, cloud_target->points[i].y,0);
    }

    //transform the points 
    double pos_2[3] = {0}, trans_pos_2[3];
    for (size_t i = 0; i < match->npoints; ++i){
        pos_2[0] = match->points[i].x;
        pos_2[1] = match->points[i].y;
        bot_trans_apply_vec(&match_to_pf, pos_2, trans_pos_2);
        
        cloud_match->points[i].x = trans_pos_2[0];
        cloud_match->points[i].y = trans_pos_2[1];
    }
    
    for(int i=0; i < cloud_match->npoints ; i++){
        m1.insertPointFast(cloud_match->points[i].x, cloud_match->points[i].y,0);
    }

    //draw the points 
    if(lcmgl_match){
        bot_lcmgl_color3f(lcmgl_match, 1.0, .0, .0);
        bot_lcmgl_point_size(lcmgl_match, 2);
        bot_lcmgl_begin(lcmgl_match, GL_POINTS);
        
        for (size_t i = 0; i < cloud_match->npoints; ++i)
            {
                bot_lcmgl_vertex3f(lcmgl_match, cloud_match->points[i].x, cloud_match->points[i].y, 0);
            }
        bot_lcmgl_end(lcmgl_match);
        bot_lcmgl_switch_buffer(lcmgl_match);
    }
        
    CPosePDFPtr pdf = ICP.Align(
                                &m1,
                                &m2,
                                initialPose,
                                &runningTime,
                                (void*)&info);
    
    CPose2D mean =  pdf->getMeanVal();
    
    //cout << "Mean of estimation: " << pdf->getMeanVal() << endl<< endl;
    
    CPosePDFGaussian  gPdf;
    gPdf.copyFrom(*pdf);
    
    
    cout << "Covariance of estimation: " << endl << gPdf.cov << endl;
    
    BotTrans target_to_match;
    target_to_match.trans_vec[0] = mean[0];
    target_to_match.trans_vec[1] = mean[1];
    target_to_match.trans_vec[2] = 0;
    
    double angle = mean[2];
    
    double rpy1[3] = {0,0, angle};
    bot_roll_pitch_yaw_to_quat (rpy1, target_to_match.rot_quat);
        

    printf("ICP run in %.02fms, %d iterations (%.02fms/iter), %.01f%% goodness\n -> ",
           runningTime*1000,
           info.nIterations,
           runningTime*1000.0f/info.nIterations,
           info.goodness*100 );
    
    if(lcmgl_result){
        bot_lcmgl_color3f(lcmgl_result, 0.5, 0.5, .0);
        bot_lcmgl_point_size(lcmgl_result, 5);
        bot_lcmgl_begin(lcmgl_result, GL_POINTS);
        
        double pos[3] = {0}, trans_pos[3];
        
        for(int i = 0; i < cloud_target->npoints ; i++){
            pos[0] = cloud_target->points[i].x;
            pos[1] = cloud_target->points[i].y;
            bot_trans_apply_vec(&target_to_match, pos, trans_pos);
            bot_lcmgl_vertex3f(lcmgl_result, trans_pos[0], trans_pos[1], 0);
        }
        bot_lcmgl_end(lcmgl_result);
        bot_lcmgl_switch_buffer(lcmgl_result);
    }

    pointlist2d_t *result_points = NULL;

    pointlist2d_free(cloud_target);
    pointlist2d_free(cloud_match);
    return result_points;
}


pointlist2d_t * scanmatchRegionsMRPT(pointlist2d_t *target, pointlist2d_t *match, 
                                     BotTrans &result_sm_to_target, //resulting transform 
                                     double *fitness_score, 
                                     int *matches, double variance[9], 
                                     bot_lcmgl_t *lcmgl_target,
                                     bot_lcmgl_t *lcmgl_match,
                                     bot_lcmgl_t *lcmgl_result){
    CSimplePointsMap		m1,m2;
    float			runningTime;
    CICP::TReturnInfo		info;
    CICP			ICP;

    ICP.options.ICP_algorithm = (TICPAlgorithm)ICP_method;

    ICP.options.maxIterations			= 500;
    ICP.options.thresholdAng			= DEG2RAD(40.0f); //10.0f
    ICP.options.thresholdDist			= 20.0f;//10.0f//0.75f;
    ICP.options.ALFA				= 0.5f;
    ICP.options.smallestThresholdDist	        = 0.05f;
    ICP.options.doRANSAC = false; //?? True or false??
    
    int flipped = 0;
    if( match->npoints > target->npoints){
        flipped;
    }

    //ICP.options.dumpToConsole();


    BotTrans second_to_first = result_sm_to_target;
    if(flipped){
        bot_trans_invert(&second_to_first);
    }
    
    
    double rpy_init[3] = {0};
    bot_quat_to_roll_pitch_yaw(second_to_first.rot_quat, rpy_init);

    CPose2D initialPose(second_to_first.trans_vec[0], second_to_first.trans_vec[1],(float)rpy_init[2]);
    //CPose2D initialPose1(result_sm_to_target.trans_vec[0], result_sm_to_target.trans_vec[1],(float)rpy_init[2]);

    int draw_tf = 0;
    
    int draw_act = 1;

    std::vector<icp_result_t> results;

    if(lcmgl_target){
        bot_lcmgl_color3f(lcmgl_target, 0, .0, .5);
        bot_lcmgl_point_size(lcmgl_target, 2);
        bot_lcmgl_begin(lcmgl_target, GL_POINTS);
        
        for (size_t i = 0; i < target->npoints; ++i){
            bot_lcmgl_vertex3f(lcmgl_target, target->points[i].x, target->points[i].y, 0);
        }
        
        bot_lcmgl_end(lcmgl_target);
        bot_lcmgl_switch_buffer(lcmgl_target);
    }

    //insert the points 
    for(int i=0; i < target->npoints ; i++){
        m1.insertPointFast(target->points[i].x, target->points[i].y,0);
    }

    for(int i=0; i < match->npoints ; i++){
        m2.insertPointFast(match->points[i].x, match->points[i].y,0);
    }

    //draw the points 
    if(lcmgl_match){
        bot_lcmgl_color3f(lcmgl_match, 1.0, .0, .0);
        bot_lcmgl_point_size(lcmgl_match, 2);
        bot_lcmgl_begin(lcmgl_match, GL_POINTS);
        
        for (size_t i = 0; i < match->npoints; ++i){
            bot_lcmgl_vertex3f(lcmgl_match, match->points[i].x, match->points[i].y, 0);
        }
        bot_lcmgl_end(lcmgl_match);
        bot_lcmgl_switch_buffer(lcmgl_match);
    }
    
    CPosePDFPtr pdf;
    if(!flipped){
        pdf = ICP.Align(&m1,
                        &m2,
                        initialPose,
                        &runningTime,
                        (void*)&info);
        //fprintf(stderr, "ICP Result : => Matches : %f [%d -> %d]\n", info.goodness, (int) m1.size(), (int) m2.size());
    }
    else{
        pdf = ICP.Align(&m2,
                        &m1,
                        initialPose,
                        &runningTime,
                        (void*)&info);
        //fprintf(stderr, "ICP Result : => Matches : %f [%d -> %d]\n", info.goodness, (int) m2.size(), (int) m1.size());
    }
    
    CPose2D mean =  pdf->getMeanVal();

    CPosePDFGaussian  gPdf;
    gPdf.copyFrom(*pdf);
    
    CMatrixDouble33 cov = gPdf.cov;
    //cout << "+++++=========== Covariance : \n";
    //fprintf(stderr, "%f,%f,%f,%f\n", cov(0,0), cov(0,1), cov(1,1), cov(2,2));
    for(int i=0; i < 3; i++){
        for(int j=0; j < 3; j++){
            variance[3*i+j] = cov(i,j); //row major
        }
    }

    //cout << cov << endl;
    //cout << "Mean of estimation: " << pdf->getMeanVal() << endl<< endl;
    
   
    //there is a function to draw samples from the distribution as well 
    // Draws a number of samples from the distribution, and saves as a list of 1x3 vectors, where each row contains a (x,y,phi) datum.
    //void  drawManySamples( size_t N, std::vector<vector_double> & outSamples ) const;
    

    //need to pass this up 

    //fprintf(stderr, "Score : %f\n", info.goodness); 
    //fprintf(stderr, "Matches : %d / %d\n", (int) (info.goodness * target->npoints), target->npoints);
    
    *fitness_score = info.goodness;
    *matches = info.goodness * target->npoints;

    //we might want to use this at some point 
    //cout << "Covariance of estimation: " << endl << gPdf.cov << endl;
    
    result_sm_to_target.trans_vec[0] = mean[0];
    result_sm_to_target.trans_vec[1] = mean[1];
    result_sm_to_target.trans_vec[2] = 0;

    double angle = mean[2];
    double rpy1[3] = {0,0, angle};

    //fprintf(stderr, "Scanmatch TF : %f,%f - %f\n", mean[0], mean[1], mean[2]);
    bot_roll_pitch_yaw_to_quat (rpy1, result_sm_to_target.rot_quat);

    if(flipped){
        bot_trans_invert(&result_sm_to_target);
    }
    
    /*printf("ICP run in %.02fms, %d iterations (%.02fms/iter), %.01f%% goodness\n -> ",
           runningTime*1000,
           info.nIterations,
           runningTime*1000.0f/info.nIterations,
           info.goodness*100 );*/
    
    if(lcmgl_result){
        bot_lcmgl_color3f(lcmgl_result, 0.5, 0.5, .0);
        bot_lcmgl_point_size(lcmgl_result, 5);
        bot_lcmgl_begin(lcmgl_result, GL_POINTS);
        
        double pos[3] = {0}, trans_pos[3];
        
        for(int i = 0; i < match->npoints ; i++){
            pos[0] = match->points[i].x;
            pos[1] = match->points[i].y;
            bot_trans_apply_vec(&result_sm_to_target, pos, trans_pos);
            bot_lcmgl_vertex3f(lcmgl_result, trans_pos[0], trans_pos[1], 0);
        }
        bot_lcmgl_end(lcmgl_result);
        bot_lcmgl_switch_buffer(lcmgl_result);
    }

    pointlist2d_t *result_points = NULL;

    return result_points;
}

pointlist2d_t * scanmatchRegionsMRPTQuick(pointlist2d_t *target, pointlist2d_t *match, 
                                     BotTrans &result_sm_to_target, //resulting transform 
                                     double *fitness_score, 
                                     int *matches, double variance[9], 
                                     bot_lcmgl_t *lcmgl_target,
                                     bot_lcmgl_t *lcmgl_match,
                                     bot_lcmgl_t *lcmgl_result){
    CSimplePointsMap		m1,m2;
    float			runningTime;
    CICP::TReturnInfo		info;
    CICP			ICP;

    ICP.options.ICP_algorithm = (TICPAlgorithm)ICP_method;

    ICP.options.maxIterations			= 20;
    ICP.options.thresholdAng			= DEG2RAD(40.0f); //10.0f
    ICP.options.thresholdDist			= 20.0f;//10.0f//0.75f;
    ICP.options.ALFA				= 0.5f;
    ICP.options.smallestThresholdDist	        = 0.05f;
    ICP.options.doRANSAC = false; //?? True or false??
    
    int flipped = 0;
    if( match->npoints > target->npoints){
        flipped;
    }

    //ICP.options.dumpToConsole();


    BotTrans second_to_first = result_sm_to_target;
    if(flipped){
        bot_trans_invert(&second_to_first);
    }
    
    
    double rpy_init[3] = {0};
    bot_quat_to_roll_pitch_yaw(second_to_first.rot_quat, rpy_init);

    CPose2D initialPose(second_to_first.trans_vec[0], second_to_first.trans_vec[1],(float)rpy_init[2]);
    //CPose2D initialPose1(result_sm_to_target.trans_vec[0], result_sm_to_target.trans_vec[1],(float)rpy_init[2]);

    int draw_tf = 0;
    
    int draw_act = 1;

    std::vector<icp_result_t> results;

    if(lcmgl_target){
        bot_lcmgl_color3f(lcmgl_target, 0, .0, .5);
        bot_lcmgl_point_size(lcmgl_target, 2);
        bot_lcmgl_begin(lcmgl_target, GL_POINTS);
        
        for (size_t i = 0; i < target->npoints; ++i){
            bot_lcmgl_vertex3f(lcmgl_target, target->points[i].x, target->points[i].y, 0);
        }
        
        bot_lcmgl_end(lcmgl_target);
        bot_lcmgl_switch_buffer(lcmgl_target);
    }

    //insert the points 
    for(int i=0; i < target->npoints ; i++){
        m1.insertPointFast(target->points[i].x, target->points[i].y,0);
    }

    for(int i=0; i < match->npoints ; i++){
        m2.insertPointFast(match->points[i].x, match->points[i].y,0);
    }

    //draw the points 
    if(lcmgl_match){
        bot_lcmgl_color3f(lcmgl_match, 1.0, .0, .0);
        bot_lcmgl_point_size(lcmgl_match, 2);
        bot_lcmgl_begin(lcmgl_match, GL_POINTS);
        
        for (size_t i = 0; i < match->npoints; ++i){
            bot_lcmgl_vertex3f(lcmgl_match, match->points[i].x, match->points[i].y, 0);
        }
        bot_lcmgl_end(lcmgl_match);
        bot_lcmgl_switch_buffer(lcmgl_match);
    }
    
    CPosePDFPtr pdf;
    if(!flipped){
        pdf = ICP.Align(&m1,
                        &m2,
                        initialPose,
                        &runningTime,
                        (void*)&info);
        //fprintf(stderr, "ICP Result : => Matches : %f [%d -> %d]\n", info.goodness, (int) m1.size(), (int) m2.size());
    }
    else{
        pdf = ICP.Align(&m2,
                        &m1,
                        initialPose,
                        &runningTime,
                        (void*)&info);
        //fprintf(stderr, "ICP Result : => Matches : %f [%d -> %d]\n", info.goodness, (int) m2.size(), (int) m1.size());
    }
    
    CPose2D mean =  pdf->getMeanVal();

    CPosePDFGaussian  gPdf;
    gPdf.copyFrom(*pdf);
    
    CMatrixDouble33 cov = gPdf.cov;
    //cout << "+++++=========== Covariance : \n";
    //fprintf(stderr, "%f,%f,%f,%f\n", cov(0,0), cov(0,1), cov(1,1), cov(2,2));
    for(int i=0; i < 3; i++){
        for(int j=0; j < 3; j++){
            variance[3*i+j] = cov(i,j); //row major
        }
    }

    //cout << cov << endl;
    //cout << "Mean of estimation: " << pdf->getMeanVal() << endl<< endl;
    
   
    //there is a function to draw samples from the distribution as well 
    // Draws a number of samples from the distribution, and saves as a list of 1x3 vectors, where each row contains a (x,y,phi) datum.
    //void  drawManySamples( size_t N, std::vector<vector_double> & outSamples ) const;
    

    //need to pass this up 

    //fprintf(stderr, "Score : %f\n", info.goodness); 
    //fprintf(stderr, "Matches : %d / %d\n", (int) (info.goodness * target->npoints), target->npoints);
    
    *fitness_score = info.goodness;
    *matches = info.goodness * target->npoints;

    //we might want to use this at some point 
    //cout << "Covariance of estimation: " << endl << gPdf.cov << endl;
    
    result_sm_to_target.trans_vec[0] = mean[0];
    result_sm_to_target.trans_vec[1] = mean[1];
    result_sm_to_target.trans_vec[2] = 0;

    double angle = mean[2];
    double rpy1[3] = {0,0, angle};

    //fprintf(stderr, "Scanmatch TF : %f,%f - %f\n", mean[0], mean[1], mean[2]);
    bot_roll_pitch_yaw_to_quat (rpy1, result_sm_to_target.rot_quat);

    if(flipped){
        bot_trans_invert(&result_sm_to_target);
    }
    
    /*printf("ICP run in %.02fms, %d iterations (%.02fms/iter), %.01f%% goodness\n -> ",
           runningTime*1000,
           info.nIterations,
           runningTime*1000.0f/info.nIterations,
           info.goodness*100 );*/
    
    if(lcmgl_result){
        bot_lcmgl_color3f(lcmgl_result, 0.5, 0.5, .0);
        bot_lcmgl_point_size(lcmgl_result, 5);
        bot_lcmgl_begin(lcmgl_result, GL_POINTS);
        
        double pos[3] = {0}, trans_pos[3];
        
        for(int i = 0; i < match->npoints ; i++){
            pos[0] = match->points[i].x;
            pos[1] = match->points[i].y;
            bot_trans_apply_vec(&result_sm_to_target, pos, trans_pos);
            bot_lcmgl_vertex3f(lcmgl_result, trans_pos[0], trans_pos[1], 0);
        }
        bot_lcmgl_end(lcmgl_result);
        bot_lcmgl_switch_buffer(lcmgl_result);
    }

    pointlist2d_t *result_points = NULL;

    return result_points;
}


pointlist2d_t * scanmatchRegionsMRPTFixed(pointlist2d_t *target, pointlist2d_t *match, 
                                     BotTrans &result_sm_to_target, //resulting transform 
                                     double *fitness_score, 
                                     int *matches, 
                                     bot_lcmgl_t *lcmgl_target, 
                                     bot_lcmgl_t *lcmgl_match, 
                                     bot_lcmgl_t *lcmgl_result){
    CSimplePointsMap		m1,m2;
    float					runningTime;
    CICP::TReturnInfo		info;
    CICP					ICP;

    ICP.options.ICP_algorithm = (TICPAlgorithm)ICP_method;

    ICP.options.maxIterations			= 500;
    ICP.options.thresholdAng			= DEG2RAD(40.0f); //10.0f
    ICP.options.thresholdDist			= 20.0f;//10.0f//0.75f;
    ICP.options.ALFA				= 0.5f;
    ICP.options.smallestThresholdDist	        = 0.05f;
    ICP.options.doRANSAC = false; //?? True or false??
    
    //ICP.options.dumpToConsole();

    double rpy_init[3] = {0};
    bot_quat_to_roll_pitch_yaw(result_sm_to_target.rot_quat, rpy_init);
    
    CPose2D initialPose(result_sm_to_target.trans_vec[0], result_sm_to_target.trans_vec[1],(float)rpy_init[2]);

    int draw_tf = 0;
    
    int draw_act = 1;

    std::vector<icp_result_t> results;

    if(lcmgl_target){
        bot_lcmgl_color3f(lcmgl_target, 0, .0, .5);
        bot_lcmgl_point_size(lcmgl_target, 2);
        bot_lcmgl_begin(lcmgl_target, GL_POINTS);
        
        for (size_t i = 0; i < target->npoints; ++i){
            bot_lcmgl_vertex3f(lcmgl_target, target->points[i].x, target->points[i].y, 0);
        }
        
        bot_lcmgl_end(lcmgl_target);
        bot_lcmgl_switch_buffer(lcmgl_target);
    }

    //insert the points 
    for(int i=0; i < target->npoints ; i++){
        m1.insertPointFast(target->points[i].x, target->points[i].y,0);
    }

    for(int i=0; i < match->npoints ; i++){
        m2.insertPointFast(match->points[i].x, match->points[i].y,0);
    }

    //draw the points 
    if(lcmgl_match){
        bot_lcmgl_color3f(lcmgl_match, 1.0, .0, .0);
        bot_lcmgl_point_size(lcmgl_match, 2);
        bot_lcmgl_begin(lcmgl_match, GL_POINTS);
        
        for (size_t i = 0; i < match->npoints; ++i){
            bot_lcmgl_vertex3f(lcmgl_match, match->points[i].x, match->points[i].y, 0);
        }
        bot_lcmgl_end(lcmgl_match);
        bot_lcmgl_switch_buffer(lcmgl_match);
    }

    CPosePDFPtr pdf = ICP.Align(&m1,
                                &m2,
                                initialPose,
                                &runningTime,
                                (void*)&info);

    CPose2D mean =  pdf->getMeanVal();
    
    //cout << "Mean of estimation: " << pdf->getMeanVal() << endl<< endl;
    
    CPosePDFGaussian  gPdf;
    gPdf.copyFrom(*pdf);

    fprintf(stderr, "Score : %f\n", info.goodness); 
    fprintf(stderr, "Matches : %d / %d\n", (int) (info.goodness * target->npoints), target->npoints);
    
    *fitness_score = info.goodness;
    *matches = info.goodness * target->npoints;

    cout << "Covariance of estimation: " << endl << gPdf.cov << endl;
    
    /*BotTrans target_to_match;
    target_to_match.trans_vec[0] = mean[0];
    target_to_match.trans_vec[1] = mean[1];
    target_to_match.trans_vec[2] = 0;
    
    double angle = mean[2];
  
    double rpy1[3] = {0,0, angle};
    bot_roll_pitch_yaw_to_quat (rpy1, target_to_match.rot_quat);

    result_sm_to_target = target_to_match;        */
    result_sm_to_target.trans_vec[0] = mean[0];
    result_sm_to_target.trans_vec[1] = mean[1];
    result_sm_to_target.trans_vec[2] = 0;

    double angle = mean[2];
    double rpy1[3] = {0,0, angle};

    fprintf(stderr, "Scanmatch TF : %f,%f - %f\n", mean[0], mean[1], mean[2]);
    bot_roll_pitch_yaw_to_quat (rpy1, result_sm_to_target.rot_quat);

    printf("ICP run in %.02fms, %d iterations (%.02fms/iter), %.01f%% goodness\n -> ",
           runningTime*1000,
           info.nIterations,
           runningTime*1000.0f/info.nIterations,
           info.goodness*100 );
    
    if(lcmgl_result){
        bot_lcmgl_color3f(lcmgl_result, 0.5, 0.5, .0);
        bot_lcmgl_point_size(lcmgl_result, 5);
        bot_lcmgl_begin(lcmgl_result, GL_POINTS);
        
        double pos[3] = {0}, trans_pos[3];
        
        for(int i = 0; i < match->npoints ; i++){
            pos[0] = match->points[i].x;
            pos[1] = match->points[i].y;
            bot_trans_apply_vec(&result_sm_to_target, pos, trans_pos);
            bot_lcmgl_vertex3f(lcmgl_result, trans_pos[0], trans_pos[1], 0);
        }
        bot_lcmgl_end(lcmgl_result);
        bot_lcmgl_switch_buffer(lcmgl_result);
    }

    pointlist2d_t *result_points = NULL;

    return result_points;
}



//this one rotates the scans 
//first is target, second is match
pointlist2d_t * doScanMatchMRPTFull(pointlist2d_t *p1, pointlist2d_t *p2, 
                                    //match and target points are in the pf reference frame - maybe they should 
                                    //be in their own reference frames - might make it clearer
                                    BotTrans p1_to_pf, 
                                    BotTrans p2_to_pf, 
                                    BotTrans *result_sm_to_target, //resulting transform 
                                    double *fitness_score, 
                                    bot_lcmgl_t *lcmgl_target, 
                                    bot_lcmgl_t *lcmgl_match, 
                                    bot_lcmgl_t *lcmgl_result, 
                                    //double max_corr_dist, 
                                    int align_scans, //use PCA to align the scans 
                                    double min_acceptance_threshold, 
                                    double break_threshold, 
                                    //int *converged, 
                                    int pause = 0, 
                                    int draw_failed = 0){

    //maybe the first one should just check if they converge given the current transforms?? 
    

    
    float					runningTime;
    CICP::TReturnInfo		info;
    CICP					ICP;

    pointlist2d_t *target = NULL, *match = NULL, *c_target = NULL, *c_match = NULL, 
        *cloud_target = NULL, *cloud_match = NULL;
    std::vector<icp_result_t> results;

    BotTrans match_to_pf, target_to_pf;

    c_target = pointlist2d_new(p1->npoints);
    c_match = pointlist2d_new(p2->npoints);

    //seems to work better when the target has more points that the match 
    
    ICP.options.ICP_algorithm = (TICPAlgorithm)ICP_method;

    ICP.options.maxIterations			= 500;//2000;
    ICP.options.thresholdAng			= DEG2RAD(40.0f); //10.0f
    ICP.options.thresholdDist			= 20.0f;//10.0f//0.75f;
    ICP.options.ALFA					= 0.5f;
    ICP.options.smallestThresholdDist	= 0.05f;
    ICP.options.doRANSAC = false;//true;//false;

    //ICP.options.dumpToConsole();

    CPose2D		initialPose(0.f,0.0f,(float)DEG2RAD(0.0f));

    int flip = 1;

    int count = flip ? 2: 1;

    int solution_found  = 0;

    //or maybe flip the match - as a second option??
    for(int k=0; k < count; k++){ 
        //comment this out when running 
        if(solution_found) 
            break;
        CSimplePointsMap m1;
        if(k==0){//p1->npoints < p2->npoints){
            match = p2;
            target = p1;
            target_to_pf = p1_to_pf;
            match_to_pf = p2_to_pf;
            cloud_target = c_target;
            cloud_match = c_match;
        }
        else{
            match = p1;
            target = p2;
            target_to_pf = p2_to_pf;
            match_to_pf = p1_to_pf;
            cloud_match = c_target;
            cloud_target = c_match;
        }

        //print_trans(target_to_pf, "Target to PF");
        //print_trans(match_to_pf, "Match to PF");

        double mx_t = 0, my_t = 0;

        //put the stuff in to a point cloud - and then run pca on them 
        double p_1[3] = {0}, tf_p_1[3];

        if(align_scans){
            for (size_t i = 0; i < target->npoints; ++i){
                mx_t += target->points[i].x;
                my_t += target->points[i].y;
            }
            
            mx_t /= (double) target->npoints; 
            my_t /= (double) target->npoints; 
        }

        //move the points to mean x,y

        BotTrans target_to_tf_target; 
        target_to_tf_target.trans_vec[0] = -mx_t;
        target_to_tf_target.trans_vec[1] = -my_t;
        target_to_tf_target.trans_vec[2] = 0;
    
        double rpy_tf[3] = {0};
        bot_roll_pitch_yaw_to_quat(rpy_tf, target_to_tf_target.rot_quat);

        BotTrans tf_target_to_target = target_to_tf_target; 
        bot_trans_invert(&tf_target_to_target);
    
        for (size_t i = 0; i < target->npoints; ++i){
            p_1[0] = target->points[i].x;
            p_1[1] = target->points[i].y;
            if(align_scans){
                bot_trans_apply_vec(&target_to_tf_target, p_1, tf_p_1);
                cloud_target->points[i].x = tf_p_1[0];
                cloud_target->points[i].y = tf_p_1[1];
            }
            else{
                cloud_target->points[i].x = p_1[0];
                cloud_target->points[i].y = p_1[1];
            }
        }

         //insert the points 
        for(int i=0; i < cloud_target->npoints ; i++){
            m1.insertPointFast(cloud_target->points[i].x, cloud_target->points[i].y,0);
        }

        //print_trans(target_to_tf_target, "Target to Transformed");

        //print_trans(target_to_pf, "Target to PF");
        // print_trans(match_to_pf, "Match to PF");

        int draw_act = 0;

        int nr = 0;

        double mx_m = 0, my_m = 0;
        if(align_scans){
            for (size_t i = 0; i < match->npoints; ++i){
                mx_m += match->points[i].x;
                my_m += match->points[i].y;
            }
            
            mx_m /= (double) match->npoints; 
            my_m /= (double) match->npoints; 
        }

        BotTrans match_to_tf_match1; 
        match_to_tf_match1.trans_vec[0] = -mx_m;
        match_to_tf_match1.trans_vec[1] = -my_m;
        match_to_tf_match1.trans_vec[2] = 0;
        double rpy_tf_m[3] = {0};
        bot_roll_pitch_yaw_to_quat(rpy_tf_m, match_to_tf_match1.rot_quat);
    
        double p_2[3] = {0}, tf_p_2[3];
       

        if(lcmgl_target){
            double pos_target[3] = {0}, pos_pf[3];
            if(draw_act){
                bot_lcmgl_color3f(lcmgl_target, 0, .0, .5);
                bot_lcmgl_point_size(lcmgl_target, 2);
                bot_lcmgl_begin(lcmgl_target, GL_POINTS);
                
                for (size_t i = 0; i < cloud_target->npoints; ++i){
                    pos_pf[0] = cloud_target->points[i].x;
                    pos_pf[1] = cloud_target->points[i].y;
                    bot_lcmgl_vertex3f(lcmgl_target, pos_pf[0], pos_pf[1], 0);
                }
                
                bot_lcmgl_end(lcmgl_target);
            }

            bot_lcmgl_color3f(lcmgl_target, 0, .0, 1.0);
            bot_lcmgl_point_size(lcmgl_target, 2);
            bot_lcmgl_begin(lcmgl_target, GL_POINTS);

            for (size_t i = 0; i < target->npoints; ++i){
                pos_target[0] = target->points[i].x;
                pos_target[1] = target->points[i].y;
                bot_trans_apply_vec(&target_to_pf, pos_target, pos_pf);
                bot_lcmgl_vertex3f(lcmgl_target, pos_pf[0], pos_pf[1], 0);
            }
            bot_lcmgl_end(lcmgl_target);

            bot_lcmgl_switch_buffer(lcmgl_target);
        }

        int size = 8;  //try 8 
     
        for(int i=0; i < size; i++){
            CSimplePointsMap m2;

            BotTrans tf_match1_to_tf_match;
        
            tf_match1_to_tf_match.trans_vec[0] = 0;
            tf_match1_to_tf_match.trans_vec[1] = 0;
            tf_match1_to_tf_match.trans_vec[2] = 0;
        
            double w = M_PI*2/ size * i; 
        
            double rpy_1[3] = {.0,.0, w};
            bot_roll_pitch_yaw_to_quat (rpy_1, tf_match1_to_tf_match.rot_quat);

            BotTrans match_to_tf_match;

            bot_trans_apply_trans_to(&tf_match1_to_tf_match, &match_to_tf_match1, &match_to_tf_match);

            //print_trans(match_to_tf_match, "Match to Transformed");

            for (size_t i = 0; i < match->npoints; ++i){
                p_2[0] = match->points[i].x;
                p_2[1] = match->points[i].y;
            
                bot_trans_apply_vec(&match_to_tf_match, p_2, tf_p_2);
            
                cloud_match->points[i].x = tf_p_2[0];
                cloud_match->points[i].y = tf_p_2[1];
            }
        
            for(int i=0; i < cloud_match->npoints ; i++){
                m2.insertPointFast(cloud_match->points[i].x, cloud_match->points[i].y,0);
            }

            //draw the points 
            if(lcmgl_match){
                if(draw_act){
                    bot_lcmgl_color3f(lcmgl_match, .5, .0, .0);
                    bot_lcmgl_point_size(lcmgl_match, 2);
                    bot_lcmgl_begin(lcmgl_match, GL_POINTS);
        
                    for (size_t i = 0; i < cloud_match->npoints; ++i)
                        {
                            bot_lcmgl_vertex3f(lcmgl_match, cloud_match->points[i].x, cloud_match->points[i].y, 0);
                        }
                    bot_lcmgl_end(lcmgl_match);
                }
                
                bot_lcmgl_color3f(lcmgl_match, 1.0, .0, .0);
                bot_lcmgl_point_size(lcmgl_match, 2);
                bot_lcmgl_begin(lcmgl_match, GL_POINTS);

                double pos_target[3] = {0}, pos_pf[3];
                for (size_t i = 0; i < match->npoints; ++i){
                    pos_target[0] = match->points[i].x;
                    pos_target[1] = match->points[i].y;
                    bot_trans_apply_vec(&match_to_pf, pos_target, pos_pf);
                    bot_lcmgl_vertex3f(lcmgl_match, pos_pf[0], pos_pf[1], 0);
                }
                bot_lcmgl_end(lcmgl_match);


                bot_lcmgl_switch_buffer(lcmgl_match);
            }
        
            CPosePDFPtr pdf = ICP.Align(
                                        &m1,
                                        &m2,
                                        initialPose,
                                        &runningTime,
                                        (void*)&info);

            //calculates the relative pose of m2 in m1's frame 
    
            CPose2D mean =  pdf->getMeanVal();
    
            //cout << "Mean of estimation: " << pdf->getMeanVal() << endl<< endl;
    
            CPosePDFGaussian  gPdf;
            gPdf.copyFrom(*pdf);
    
            //cout << "Covariance of estimation: " << endl << gPdf.cov << endl;
    
            BotTrans tf_sm_to_tf_target;
            tf_sm_to_tf_target.trans_vec[0] = mean[0];
            tf_sm_to_tf_target.trans_vec[1] = mean[1];
            tf_sm_to_tf_target.trans_vec[2] = 0;

            double angle = mean[2];
    
            double rpy1[3] = {0,0, angle};
            bot_roll_pitch_yaw_to_quat (rpy1, tf_sm_to_tf_target.rot_quat);

            BotTrans sm_to_tf_sm = match_to_tf_match; //this is the same transform that took the match to the transformed frame 
            
            BotTrans sm_to_tf_target; 
            bot_trans_apply_trans_to(&tf_sm_to_tf_target, &sm_to_tf_sm, &sm_to_tf_target);
            
            BotTrans sm_to_target; 
            bot_trans_apply_trans_to(&tf_target_to_target, &sm_to_tf_target, &sm_to_target);

            //printf("ICP run in %.02fms, %d iterations (%.02fms/iter), %.01f%% goodness Quality : %f\n -> ",
            //       runningTime*1000,
            //       info.nIterations,
            //      runningTime*1000.0f/info.nIterations,
            //       info.goodness*100, info.quality );
        
            icp_result_t res;
            res.angle = w;
            res.flipped = k;
            res.Tf = sm_to_target;//target_to_match;
            if(k){
                bot_trans_invert(&res.Tf);
            }

            res.score = info.goodness; 
            res.n_match = info.goodness * target->npoints;
            results.push_back(res);

            if(lcmgl_result && ((!draw_failed && res.score > min_acceptance_threshold)|| draw_failed)){
                double pos[3] = {0}, trans_pos[3];
                if(draw_act){
                    if(res.score > min_acceptance_threshold)
                        bot_lcmgl_color3f(lcmgl_result, 0.0, 0.5, .0);
                    else
                        bot_lcmgl_color3f(lcmgl_result, 0.7, 0.2, .0);

                    bot_lcmgl_point_size(lcmgl_result, 5);
                    bot_lcmgl_begin(lcmgl_result, GL_POINTS);
        
                    for(int i = 0; i < cloud_target->npoints ; i++){
                        pos[0] = cloud_target->points[i].x;
                        pos[1] = cloud_target->points[i].y;
                        bot_trans_apply_vec(&tf_sm_to_tf_target, pos, trans_pos);
                        bot_lcmgl_vertex3f(lcmgl_result, trans_pos[0], trans_pos[1], 0);
                    }
                    bot_lcmgl_end(lcmgl_result);
                }
                
                if(res.score > min_acceptance_threshold)
                    bot_lcmgl_color3f(lcmgl_result, 0.0, 1.0, .0);
                else
                    bot_lcmgl_color3f(lcmgl_result, 0.5, 0.2, .0);
                    
                
                bot_lcmgl_point_size(lcmgl_result, 5);
                bot_lcmgl_begin(lcmgl_result, GL_POINTS);
        
                BotTrans sm_to_pf;
                bot_trans_apply_trans_to(&target_to_pf, &sm_to_target, &sm_to_pf);
                
                //print_trans(sm_to_pf, "SM to PF");

                for(int i = 0; i < match->npoints ; i++){
                    pos[0] = match->points[i].x;
                    pos[1] = match->points[i].y;
                    
                    bot_trans_apply_vec(&sm_to_pf, pos, trans_pos);
                    bot_lcmgl_vertex3f(lcmgl_result, trans_pos[0], trans_pos[1], 0);
                }
                bot_lcmgl_end(lcmgl_result);
                bot_lcmgl_switch_buffer(lcmgl_result);
            }

            if(res.score >= break_threshold){
                solution_found = 1;
                break;
            }

            if(pause){
                std::cout << "Sleeping \n" ;
                usleep(10000);
            }
        }
    }

    double threshold = 0.6;
    int best_id = -1;
    double max_score = 0;
    for(int i = 0; i < results.size(); i++){
        icp_result_t r = results[i];
        if(max_score < r.score){
            max_score = r.score;
            best_id = i;
        }
        fprintf(stderr, "%d Result : %f - Score : %f - Flipped : %d\t", i, bot_to_degrees(r.angle), r.score, r.flipped);
        print_trans(r.Tf, "");
    }

    pointlist2d_t *result_points = NULL;
    if(best_id >=0){
        icp_result_t r = results[best_id];
        *result_sm_to_target = r.Tf;
        *fitness_score = r.score;
        fprintf(stderr, "Flipped : %d Best Result (%d) : %f - Score : %f\n", r.flipped, best_id, bot_to_degrees(r.angle), r.score);
        result_points = pointlist2d_new(p2->npoints);
        double pos[3] = {0}, trans_pos[3];
        for(int i = 0; i < p2->npoints ; i++){
            pos[0] = p2->points[i].x;
            pos[1] = p2->points[i].y;
            BotTrans sm_to_pf;
            bot_trans_apply_trans_to(&target_to_pf, &r.Tf, &sm_to_pf);
            bot_trans_apply_vec(&sm_to_pf, pos, trans_pos);
            result_points->points[i].x = trans_pos[0];
            result_points->points[i].y = trans_pos[1];
        }
    }

    pointlist2d_free(cloud_target);
    pointlist2d_free(cloud_match);
    return result_points;
}

//this one rotates the scans 
//first is target, second is match
pointlist2d_t * scanmatchRegionsExtensiveMRPT(pointlist2d_t *p1, pointlist2d_t *p2, 
                                              BotTrans p1_to_pf, 
                                              BotTrans p2_to_pf, 
                                              BotTrans *result_sm_to_target,
                                              double *fitness_score, 
                                              double variance[9], 
                                              int align_scans, //use PCA to align the scans 
                                              double min_acceptance_threshold, 
                                              double break_threshold, 
                                              int pause,// = 0, 
                                              int draw_failed,// = 0,
                                              bot_lcmgl_t *lcmgl_target, 
                                              bot_lcmgl_t *lcmgl_match, 
                                              bot_lcmgl_t *lcmgl_result)
{

    //maybe the first one should just check if they converge given the current transforms?? 
    

    
    float					runningTime;
    CICP::TReturnInfo		info;
    CICP					ICP;

    pointlist2d_t *target = NULL, *match = NULL, *c_target = NULL, *c_match = NULL, 
        *cloud_target = NULL, *cloud_match = NULL;
    std::vector<icp_result_t> results;

    BotTrans match_to_pf, target_to_pf;

    c_target = pointlist2d_new(p1->npoints);
    c_match = pointlist2d_new(p2->npoints);

    //seems to work better when the target has more points that the match 
    
    ICP.options.ICP_algorithm = (TICPAlgorithm)ICP_method;

    ICP.options.maxIterations			= 500;//2000;
    ICP.options.thresholdAng			= DEG2RAD(40.0f); //10.0f
    ICP.options.thresholdDist			= 20.0f;//10.0f//0.75f;
    ICP.options.ALFA					= 0.5f;
    ICP.options.smallestThresholdDist	= 0.05f;
    ICP.options.doRANSAC = false;//true;//false;

    //ICP.options.dumpToConsole();

    CPose2D  initialPose(0.f,0.0f,(float)DEG2RAD(0.0f));

    int flip = 1;

    int count = flip ? 2: 1;

    int solution_found  = 0;

    //or maybe flip the match - as a second option??
    for(int k=0; k < count; k++){ 
        //comment this out when running 
        if(solution_found) 
            break;
        CSimplePointsMap m1;
        if(k==0){//p1->npoints < p2->npoints){
            match = p2;
            target = p1;
            target_to_pf = p1_to_pf;
            match_to_pf = p2_to_pf;
            cloud_target = c_target;
            cloud_match = c_match;
        }
        else{
            match = p1;
            target = p2;
            target_to_pf = p2_to_pf;
            match_to_pf = p1_to_pf;
            cloud_match = c_target;
            cloud_target = c_match;
        }

        //print_trans(target_to_pf, "Target to PF");
        //print_trans(match_to_pf, "Match to PF");

        double mx_t = 0, my_t = 0;

        //put the stuff in to a point cloud - and then run pca on them 
        double p_1[3] = {0}, tf_p_1[3];

        if(align_scans){
            for (size_t i = 0; i < target->npoints; ++i){
                mx_t += target->points[i].x;
                my_t += target->points[i].y;
            }
            
            mx_t /= (double) target->npoints; 
            my_t /= (double) target->npoints; 
        }

        //move the points to mean x,y

        BotTrans target_to_tf_target; 
        target_to_tf_target.trans_vec[0] = -mx_t;
        target_to_tf_target.trans_vec[1] = -my_t;
        target_to_tf_target.trans_vec[2] = 0;
    
        double rpy_tf[3] = {0};
        bot_roll_pitch_yaw_to_quat(rpy_tf, target_to_tf_target.rot_quat);

        BotTrans tf_target_to_target = target_to_tf_target; 
        bot_trans_invert(&tf_target_to_target);
    
        for (size_t i = 0; i < target->npoints; ++i){
            p_1[0] = target->points[i].x;
            p_1[1] = target->points[i].y;
            if(align_scans){
                bot_trans_apply_vec(&target_to_tf_target, p_1, tf_p_1);
                cloud_target->points[i].x = tf_p_1[0];
                cloud_target->points[i].y = tf_p_1[1];
            }
            else{
                cloud_target->points[i].x = p_1[0];
                cloud_target->points[i].y = p_1[1];
            }
        }

         //insert the points 
        for(int i=0; i < cloud_target->npoints ; i++){
            m1.insertPointFast(cloud_target->points[i].x, cloud_target->points[i].y,0);
        }

        //print_trans(target_to_tf_target, "Target to Transformed");

        //print_trans(target_to_pf, "Target to PF");
        // print_trans(match_to_pf, "Match to PF");

        int draw_act = 0;

        int nr = 0;

        double mx_m = 0, my_m = 0;
        if(align_scans){
            for (size_t i = 0; i < match->npoints; ++i){
                mx_m += match->points[i].x;
                my_m += match->points[i].y;
            }
            
            mx_m /= (double) match->npoints; 
            my_m /= (double) match->npoints; 
        }

        BotTrans match_to_tf_match1; 
        match_to_tf_match1.trans_vec[0] = -mx_m;
        match_to_tf_match1.trans_vec[1] = -my_m;
        match_to_tf_match1.trans_vec[2] = 0;
        double rpy_tf_m[3] = {0};
        bot_roll_pitch_yaw_to_quat(rpy_tf_m, match_to_tf_match1.rot_quat);
    
        double p_2[3] = {0}, tf_p_2[3];
       

        if(lcmgl_target){
            double pos_target[3] = {0}, pos_pf[3];
            if(draw_act){
                bot_lcmgl_color3f(lcmgl_target, 0, .0, .5);
                bot_lcmgl_point_size(lcmgl_target, 2);
                bot_lcmgl_begin(lcmgl_target, GL_POINTS);
                
                for (size_t i = 0; i < cloud_target->npoints; ++i){
                    pos_pf[0] = cloud_target->points[i].x;
                    pos_pf[1] = cloud_target->points[i].y;
                    bot_lcmgl_vertex3f(lcmgl_target, pos_pf[0], pos_pf[1], 0);
                }
                
                bot_lcmgl_end(lcmgl_target);
            }

            bot_lcmgl_color3f(lcmgl_target, 0, .0, 1.0);
            bot_lcmgl_point_size(lcmgl_target, 2);
            bot_lcmgl_begin(lcmgl_target, GL_POINTS);

            for (size_t i = 0; i < target->npoints; ++i){
                pos_target[0] = target->points[i].x;
                pos_target[1] = target->points[i].y;
                bot_trans_apply_vec(&target_to_pf, pos_target, pos_pf);
                bot_lcmgl_vertex3f(lcmgl_target, pos_pf[0], pos_pf[1], 0);
            }
            bot_lcmgl_end(lcmgl_target);

            bot_lcmgl_switch_buffer(lcmgl_target);
        }

        int size = 8;  //try 8 
     
        for(int i=0; i < size; i++){
            CSimplePointsMap m2;

            BotTrans tf_match1_to_tf_match;
        
            tf_match1_to_tf_match.trans_vec[0] = 0;
            tf_match1_to_tf_match.trans_vec[1] = 0;
            tf_match1_to_tf_match.trans_vec[2] = 0;
        
            double w = M_PI*2/ size * i; 
        
            double rpy_1[3] = {.0,.0, w};
            bot_roll_pitch_yaw_to_quat (rpy_1, tf_match1_to_tf_match.rot_quat);

            BotTrans match_to_tf_match;

            bot_trans_apply_trans_to(&tf_match1_to_tf_match, &match_to_tf_match1, &match_to_tf_match);

            //print_trans(match_to_tf_match, "Match to Transformed");

            for (size_t i = 0; i < match->npoints; ++i){
                p_2[0] = match->points[i].x;
                p_2[1] = match->points[i].y;
            
                bot_trans_apply_vec(&match_to_tf_match, p_2, tf_p_2);
            
                cloud_match->points[i].x = tf_p_2[0];
                cloud_match->points[i].y = tf_p_2[1];
            }
        
            for(int i=0; i < cloud_match->npoints ; i++){
                m2.insertPointFast(cloud_match->points[i].x, cloud_match->points[i].y,0);
            }

            //draw the points 
            if(lcmgl_match){
                if(draw_act){
                    bot_lcmgl_color3f(lcmgl_match, .5, .0, .0);
                    bot_lcmgl_point_size(lcmgl_match, 2);
                    bot_lcmgl_begin(lcmgl_match, GL_POINTS);
        
                    for (size_t i = 0; i < cloud_match->npoints; ++i)
                        {
                            bot_lcmgl_vertex3f(lcmgl_match, cloud_match->points[i].x, cloud_match->points[i].y, 0);
                        }
                    bot_lcmgl_end(lcmgl_match);
                }
                
                bot_lcmgl_color3f(lcmgl_match, 1.0, .0, .0);
                bot_lcmgl_point_size(lcmgl_match, 2);
                bot_lcmgl_begin(lcmgl_match, GL_POINTS);

                double pos_target[3] = {0}, pos_pf[3];
                for (size_t i = 0; i < match->npoints; ++i){
                    pos_target[0] = match->points[i].x;
                    pos_target[1] = match->points[i].y;
                    bot_trans_apply_vec(&match_to_pf, pos_target, pos_pf);
                    bot_lcmgl_vertex3f(lcmgl_match, pos_pf[0], pos_pf[1], 0);
                }
                bot_lcmgl_end(lcmgl_match);


                bot_lcmgl_switch_buffer(lcmgl_match);
            }
        
            CPosePDFPtr pdf = ICP.Align(
                                        &m1,
                                        &m2,
                                        initialPose,
                                        &runningTime,
                                        (void*)&info);

            //calculates the relative pose of m2 in m1's frame 
    
            CPose2D mean =  pdf->getMeanVal();
    
            //cout << "Mean of estimation: " << pdf->getMeanVal() << endl<< endl;
    
            CPosePDFGaussian  gPdf;
            gPdf.copyFrom(*pdf);
    
            //cout << "Covariance of estimation: " << endl << gPdf.cov << endl;
    
            BotTrans tf_sm_to_tf_target;
            tf_sm_to_tf_target.trans_vec[0] = mean[0];
            tf_sm_to_tf_target.trans_vec[1] = mean[1];
            tf_sm_to_tf_target.trans_vec[2] = 0;

            double angle = mean[2];
    
            double rpy1[3] = {0,0, angle};
            bot_roll_pitch_yaw_to_quat (rpy1, tf_sm_to_tf_target.rot_quat);

            BotTrans sm_to_tf_sm = match_to_tf_match; //this is the same transform that took the match to the transformed frame 
            
            BotTrans sm_to_tf_target; 
            bot_trans_apply_trans_to(&tf_sm_to_tf_target, &sm_to_tf_sm, &sm_to_tf_target);
            
            BotTrans sm_to_target; 
            bot_trans_apply_trans_to(&tf_target_to_target, &sm_to_tf_target, &sm_to_target);

            //printf("ICP run in %.02fms, %d iterations (%.02fms/iter), %.01f%% goodness Quality : %f\n -> ",
            //       runningTime*1000,
            //       info.nIterations,
            //      runningTime*1000.0f/info.nIterations,
            //       info.goodness*100, info.quality );
        
            icp_result_t res;
            res.angle = w;
            res.flipped = k;
            res.Tf = sm_to_target;//target_to_match;
            if(k){
                bot_trans_invert(&res.Tf);
            }

            res.score = info.goodness; 
            res.n_match = info.goodness * target->npoints;
            results.push_back(res);

            if(lcmgl_result && ((!draw_failed && res.score > min_acceptance_threshold)|| draw_failed)){
                double pos[3] = {0}, trans_pos[3];
                if(draw_act){
                    if(res.score > min_acceptance_threshold)
                        bot_lcmgl_color3f(lcmgl_result, 0.0, 0.5, .0);
                    else
                        bot_lcmgl_color3f(lcmgl_result, 0.7, 0.2, .0);

                    bot_lcmgl_point_size(lcmgl_result, 5);
                    bot_lcmgl_begin(lcmgl_result, GL_POINTS);
        
                    for(int i = 0; i < cloud_target->npoints ; i++){
                        pos[0] = cloud_target->points[i].x;
                        pos[1] = cloud_target->points[i].y;
                        bot_trans_apply_vec(&tf_sm_to_tf_target, pos, trans_pos);
                        bot_lcmgl_vertex3f(lcmgl_result, trans_pos[0], trans_pos[1], 0);
                    }
                    bot_lcmgl_end(lcmgl_result);
                }
                
                if(res.score > min_acceptance_threshold)
                    bot_lcmgl_color3f(lcmgl_result, 0.0, 1.0, .0);
                else
                    bot_lcmgl_color3f(lcmgl_result, 0.5, 0.2, .0);
                    
                
                bot_lcmgl_point_size(lcmgl_result, 5);
                bot_lcmgl_begin(lcmgl_result, GL_POINTS);
        
                BotTrans sm_to_pf;
                bot_trans_apply_trans_to(&target_to_pf, &sm_to_target, &sm_to_pf);
                
                //print_trans(sm_to_pf, "SM to PF");

                for(int i = 0; i < match->npoints ; i++){
                    pos[0] = match->points[i].x;
                    pos[1] = match->points[i].y;
                    
                    bot_trans_apply_vec(&sm_to_pf, pos, trans_pos);
                    bot_lcmgl_vertex3f(lcmgl_result, trans_pos[0], trans_pos[1], 0);
                }
                bot_lcmgl_end(lcmgl_result);
                bot_lcmgl_switch_buffer(lcmgl_result);
            }

            if(res.score >= break_threshold){
                solution_found = 1;
                break;
            }

            if(pause){
                std::cout << "Sleeping \n" ;
                usleep(10000);
            }
        }
    }

    double threshold = 0.6;
    int best_id = -1;
    double max_score = 0;
    for(int i = 0; i < results.size(); i++){
        icp_result_t r = results[i];
        if(max_score < r.score){
            max_score = r.score;
            best_id = i;
        }
        fprintf(stderr, "%d Result : %f - Score : %f - Flipped : %d\t", i, bot_to_degrees(r.angle), r.score, r.flipped);
        print_trans(r.Tf, "");
    }

    pointlist2d_t *result_points = NULL;
    if(best_id >=0){
        icp_result_t r = results[best_id];
        *result_sm_to_target = r.Tf;
        *fitness_score = r.score;
        fprintf(stderr, "Flipped : %d Best Result (%d) : %f - Score : %f\n", r.flipped, best_id, bot_to_degrees(r.angle), r.score);
        result_points = pointlist2d_new(p2->npoints);
        double pos[3] = {0}, trans_pos[3];
        for(int i = 0; i < p2->npoints ; i++){
            pos[0] = p2->points[i].x;
            pos[1] = p2->points[i].y;
            BotTrans sm_to_pf;
            bot_trans_apply_trans_to(&target_to_pf, &r.Tf, &sm_to_pf);
            bot_trans_apply_vec(&sm_to_pf, pos, trans_pos);
            result_points->points[i].x = trans_pos[0];
            result_points->points[i].y = trans_pos[1];
        }
    }

    pointlist2d_free(cloud_target);
    pointlist2d_free(cloud_match);
    return result_points;
}


pointlist2d_t * doScanMatchMRPTBroken(pointlist2d_t *target, pointlist2d_t *match, 
                                      //match and target points are in the pf reference frame - maybe they should 
                                      //be in their own reference frames - might make it clearer
                                      BotTrans target_to_pf, 
                                      BotTrans match_to_pf, 
                                      BotTrans *result_sm_to_target, //resulting transform 
                                      double *fitness_score, 
                                      int *matches, 
                                      bot_lcmgl_t *lcmgl_target, 
                                      bot_lcmgl_t *lcmgl_match, 
                                      bot_lcmgl_t *lcmgl_result, 
                                      double max_corr_dist, 
                                      int align_scans, //use PCA to align the scans 
                                      int *converged){

    CSimplePointsMap		m1,m2;
    float					runningTime;
    CICP::TReturnInfo		info;
    CICP					ICP;

    ICP.options.ICP_algorithm = (TICPAlgorithm)ICP_method;

	ICP.options.maxIterations			= 500;
	ICP.options.thresholdAng			= DEG2RAD(40.0f); //10.0f
	ICP.options.thresholdDist			= 20.0f;//10.0f//0.75f;
	ICP.options.ALFA					= 0.5f;
	ICP.options.smallestThresholdDist	= 0.05f;
	ICP.options.doRANSAC = true;//false;

	//ICP.options.dumpToConsole();

    CPose2D		initialPose(0.f,0.0f,(float)DEG2RAD(0.0f));

    pointlist2d_t *cloud_target = pointlist2d_new(target->npoints);
    pointlist2d_t *cloud_match = pointlist2d_new(match->npoints);

    double mx_t = 0, my_t = 0;

    //put the stuff in to a point cloud - and then run pca on them 
    double p_1[3] = {0}, tf_p_1[3];
    
    for (size_t i = 0; i < target->npoints; ++i){
        p_1[0] = target->points[i].x;
        p_1[1] = target->points[i].y;
        bot_trans_apply_vec(&target_to_pf, p_1, tf_p_1);
        cloud_target->points[i].x = tf_p_1[0];
        cloud_target->points[i].y = tf_p_1[1];
        mx_t += 0;//target->points[i].x;
        my_t += 0;//target->points[i].y;
    }


    mx_t /= (double) target->npoints;
    my_t /= (double) target->npoints;

    print_trans(target_to_pf, "Target to PF");
    print_trans(target_to_pf, "Match to PF");

    //angle 
    double target_tf_angle = 0;

    BotTrans pf_to_target = target_to_pf;
    bot_trans_invert(&pf_to_target);

    BotTrans pf_to_tf_target1;
    
    //first move and then rotate (BotTrans first rotates and then moves) 

    pf_to_tf_target1.trans_vec[0] = -mx_t;
    pf_to_tf_target1.trans_vec[1] = -my_t;
    pf_to_tf_target1.trans_vec[2] = 0;

    double rpy_pf_to_tf_target1[3] = {.0,.0, 0};
    bot_roll_pitch_yaw_to_quat (rpy_pf_to_tf_target1,  pf_to_tf_target1.rot_quat);   

    BotTrans tf_target1_to_tf_target;
    
    tf_target1_to_tf_target.trans_vec[0] = 0;
    tf_target1_to_tf_target.trans_vec[1] = 0;
    tf_target1_to_tf_target.trans_vec[2] = 0;

    double rpy_target_to_tf_target[3] = {.0,.0, 0};
    if(align_scans){
        rpy_target_to_tf_target[2] = -target_tf_angle;
    }
    bot_roll_pitch_yaw_to_quat (rpy_target_to_tf_target,  tf_target1_to_tf_target.rot_quat);   

    //this is the actual transform we would require 
    
    BotTrans pf_to_tf_target;     
    bot_trans_apply_trans_to(&tf_target1_to_tf_target, &pf_to_tf_target1, &pf_to_tf_target);
    
    BotTrans target_to_tf_target;     
    bot_trans_apply_trans_to(&pf_to_tf_target, &target_to_pf, &target_to_tf_target);
    
    BotTrans tf_target_to_target = target_to_tf_target;  
    bot_trans_invert(&tf_target_to_target);

    //apply transform to the target 
    double pos_1[3] = {0}, trans_pos_1[3];
    for (size_t i = 0; i < target->npoints; ++i){
        pos_1[0] = target->points[i].x;
        pos_1[1] = target->points[i].y;
        bot_trans_apply_vec(&target_to_tf_target, pos_1, trans_pos_1);
        
        cloud_target->points[i].x = trans_pos_1[0];
        cloud_target->points[i].y = trans_pos_1[1];
    }

    double mx_m = 0, my_m = 0;
    
    int draw_act = 1;

    std::vector<icp_result_t> results;

    int nr = 0;
    //double max_dist = max_corr_dist;
    
    double p_2[3] = {0}, tf_p_2[3];

    for (size_t i = 0; i < match->npoints; ++i){
        p_2[0] = match->points[i].x;
        p_2[1] = match->points[i].y;

        bot_trans_apply_vec(&match_to_pf, p_2, tf_p_2);
        
        cloud_match->points[i].x = tf_p_2[0];
        cloud_match->points[i].y = tf_p_2[1];

        mx_m += 0;//match->points[i].x;
        my_m += 0;//match->points[i].y;
    }

    mx_m /= (double) match->npoints;
    my_m /= (double) match->npoints;

    double match_tf_angle = 0;

    BotTrans pf_to_tf_match_m;
    
    pf_to_tf_match_m.trans_vec[0] = -mx_m;
    pf_to_tf_match_m.trans_vec[1] = -my_m;
    pf_to_tf_match_m.trans_vec[2] = 0;

    double rpy_pf_to_tf_match_m[3] = {.0,.0,.0};
    bot_roll_pitch_yaw_to_quat (rpy_pf_to_tf_match_m,  pf_to_tf_match_m.rot_quat);

    BotTrans tf_match_m_to_tf_match_1;
    
    tf_match_m_to_tf_match_1.trans_vec[0] = 0;
    tf_match_m_to_tf_match_1.trans_vec[1] = 0;
    tf_match_m_to_tf_match_1.trans_vec[2] = 0;

    double rpy_match_to_tf_match_1[3] = {.0,.0, 0};
    if(align_scans){
        rpy_match_to_tf_match_1[2] = -match_tf_angle;
    }

    bot_roll_pitch_yaw_to_quat (rpy_match_to_tf_match_1,  tf_match_m_to_tf_match_1.rot_quat);

    BotTrans pf_to_tf_match_1;

    bot_trans_apply_trans_to(&tf_match_m_to_tf_match_1, &pf_to_tf_match_m, &pf_to_tf_match_1);

    //draw the target
    if(lcmgl_target){
        if(draw_act){
            bot_lcmgl_color3f(lcmgl_target, 0, .0, .5);
            bot_lcmgl_point_size(lcmgl_target, 2);
            bot_lcmgl_begin(lcmgl_target, GL_POINTS);

            double pos_target[3] = {0}, pos_pf[3];
            for (size_t i = 0; i < target->npoints; ++i){
                pos_target[0] = target->points[i].x;
                pos_target[1] = target->points[i].y;
                //bot_trans_apply_vec(&target_to_pf, pos_target, pos_pf);
                bot_lcmgl_vertex3f(lcmgl_target, pos_pf[0], pos_pf[1], 0);
            }
            
            bot_lcmgl_end(lcmgl_target);
        }
        else{
            bot_lcmgl_color3f(lcmgl_target, 0, .0, 1.0);
            bot_lcmgl_point_size(lcmgl_target, 2);
            bot_lcmgl_begin(lcmgl_target, GL_POINTS);
            
            for (size_t i = 0; i < cloud_target->npoints; ++i){
                bot_lcmgl_vertex3f(lcmgl_target, cloud_target->points[i].x, cloud_target->points[i].y, 0);
            }
            
            bot_lcmgl_end(lcmgl_target);
        }
        bot_lcmgl_switch_buffer(lcmgl_target);
    }

    for(int i=0; i < target->npoints ; i++){
        m1.insertPointFast(target->points[i].x, target->points[i].y,0);
    }

    int angle_size = 1;
    double delta = M_PI*2/ angle_size; 
    

    for(int k = 0; k < angle_size; k++){        

        Eigen::Matrix4f Ti = Eigen::Matrix4f::Identity ();

        double w = delta * (k - angle_size/2.0); //0;//2 * M_PI /angle_size * (k);

        //figure out the last rotation - of the match 
        BotTrans tf_match_1_to_tf_match;
    
        tf_match_1_to_tf_match.trans_vec[0] = 0;
        tf_match_1_to_tf_match.trans_vec[1] = 0;
        tf_match_1_to_tf_match.trans_vec[2] = 0;
        
        double rpy_tf_match_1_to_tf_match[3] = {.0,.0, w};
        bot_roll_pitch_yaw_to_quat (rpy_tf_match_1_to_tf_match, tf_match_1_to_tf_match.rot_quat);

        BotTrans pf_to_tf_match;
        bot_trans_apply_trans_to(&tf_match_1_to_tf_match, &pf_to_tf_match_1, &pf_to_tf_match);
                
        icp_result_t res; 
        res.angle = w;

        BotTrans match_to_tf_match;     
        bot_trans_apply_trans_to(&pf_to_tf_match, &match_to_pf, &match_to_tf_match);

        //transform the points 
        double pos_2[3] = {0}, trans_pos_2[3];
        for (size_t i = 0; i < match->npoints; ++i){
            pos_2[0] = match->points[i].x;
            pos_2[1] = match->points[i].y;
            bot_trans_apply_vec(&match_to_tf_match, pos_2, trans_pos_2);
            
            cloud_match->points[i].x = trans_pos_2[0];
            cloud_match->points[i].y = trans_pos_2[1];
        }

        for(int i=0; i < match->npoints ; i++){
            m2.insertPointFast(match->points[i].x, match->points[i].y,0);
        }

        //draw the points 
        if(lcmgl_match){
            if(draw_act){
                double pos[3] = {0}, trans_pos[3];

                bot_lcmgl_color3f(lcmgl_match, 1.0, .0, .0);
                bot_lcmgl_point_size(lcmgl_match, 2);
                bot_lcmgl_begin(lcmgl_match, GL_POINTS);

                for (size_t i = 0; i < match->npoints; ++i)
                    {
                        pos[0] = match->points[i].x;
                        pos[1] = match->points[i].y;
                        //bot_trans_apply_vec(&match_to_pf, pos, trans_pos);

                        bot_lcmgl_vertex3f(lcmgl_match, pos[0], pos[1], 0);
                    }
                bot_lcmgl_end(lcmgl_match);
            }
            else{
                bot_lcmgl_color3f(lcmgl_match, 1.0, .0, .0);
                bot_lcmgl_point_size(lcmgl_match, 2);
                bot_lcmgl_begin(lcmgl_match, GL_POINTS);
                
                for (size_t i = 0; i < cloud_match->npoints; ++i)
                    {
                        bot_lcmgl_vertex3f(lcmgl_match, cloud_match->points[i].x, cloud_match->points[i].y, 0);
                    }
                bot_lcmgl_end(lcmgl_match);
            }

            bot_lcmgl_switch_buffer(lcmgl_match);
        }
        
        CPosePDFPtr pdf = ICP.Align(
                                    &m1,
                                    &m2,
                                    initialPose,
                                    &runningTime,
                                    (void*)&info);

        CPose2D mean =  pdf->getMeanVal();

        cout << "Mean of estimation: " << pdf->getMeanVal() << endl<< endl;

        CPosePDFGaussian  gPdf;
        gPdf.copyFrom(*pdf);

        
        cout << "Covariance of estimation: " << endl << gPdf.cov << endl;

        BotTrans tf_sm_to_tf_target;
        tf_sm_to_tf_target.trans_vec[0] = mean[0];
        tf_sm_to_tf_target.trans_vec[1] = mean[1];
        tf_sm_to_tf_target.trans_vec[2] = 0;

        double angle = mean[2];

        double rpy1[3] = {0,0, angle};
        bot_roll_pitch_yaw_to_quat (rpy1, tf_sm_to_tf_target.rot_quat);
        
        //this part sounds iffy 
        BotTrans sm_to_tf_sm = match_to_tf_match; //this is the same transform that took the match to the transformed frame 

        BotTrans sm_to_tf_target; 
        bot_trans_apply_trans_to(&tf_sm_to_tf_target, &sm_to_tf_sm, &sm_to_tf_target);

        BotTrans sm_to_target; 
        bot_trans_apply_trans_to(&tf_target_to_target, &sm_to_tf_target, &sm_to_target);

        BotTrans sm_to_pf;
        bot_trans_apply_trans_to(&target_to_pf, &sm_to_target, &sm_to_pf);

        printf("ICP run in %.02fms, %d iterations (%.02fms/iter), %.01f%% goodness\n -> ",
               runningTime*1000,
               info.nIterations,
               runningTime*1000.0f/info.nIterations,
               info.goodness*100 );

        double f_score = info.goodness; 
        res.Tf = sm_to_pf; //match_sm_to_target;//target_to_act;
        res.score = f_score;
        res.n_match = nr; 
        results.push_back(res);
        
        if(lcmgl_result){

            if(draw_act){
                bot_lcmgl_color3f(lcmgl_result, 0.5, 0.5, .0);
                bot_lcmgl_point_size(lcmgl_result, 5);
                bot_lcmgl_begin(lcmgl_result, GL_POINTS);
                
                double pos[3] = {0}, trans_pos[3];
                                
                for(int i = 0; i < match->npoints ; i++){
                    pos[0] = match->points[i].x;
                    pos[1] = match->points[i].y;
                    bot_trans_apply_vec(&sm_to_pf, pos, trans_pos);
                    bot_lcmgl_vertex3f(lcmgl_result, trans_pos[0], trans_pos[1], 0);
                }
                bot_lcmgl_end(lcmgl_result);
                char match_info[512];

                pos[0] = match->points[0].x;
                pos[1] = match->points[0].y;
                
                bot_trans_apply_vec(&sm_to_pf, pos, trans_pos);
                
                sprintf(match_info,"Match : %d Score : %f", nr, f_score);
                double text_pos[3] = {trans_pos[0] + 5, trans_pos[1] + 5,0};
                bot_lcmgl_text(lcmgl_result, text_pos, match_info);
            }

            bot_lcmgl_switch_buffer(lcmgl_result);
        }
       
        if(1){
            std::cout << "Sleeping \n" ;
            sleep(1);
        }
    }

    double f_score = 0; 
    int max_matched = 0;
    int matched_id = -1;
 
    for(int i=0; i < results.size(); i++){
        icp_result_t r = results.at(i);
        fprintf(stderr, "Angle : %f -> Score : %f Matched : %d Converged : %d\n", 
                bot_to_degrees(r.angle), 
                r.score, 
                r.n_match, 
                r.converged);
        
        if( r.converged && max_matched < r.n_match){
            matched_id = i;
            max_matched = r.n_match; 
            f_score = r.score;
        }

        double rpy_act[3]; 
        bot_quat_to_roll_pitch_yaw (r.Tf.rot_quat, rpy_act);

        fprintf(stderr, "Transform : %f, %f - %f\n", r.Tf.trans_vec[0], r.Tf.trans_vec[1], bot_to_degrees(rpy_act[2]));
    }

    *fitness_score = f_score;
    *matches = max_matched; 

    
    pointlist2d_t *result_points = NULL;

    if(lcmgl_target){
        bot_lcmgl_color3f(lcmgl_target, 0, .0, .5);
        bot_lcmgl_point_size(lcmgl_target, 2);
        bot_lcmgl_begin(lcmgl_target, GL_POINTS);
         
        double pos_target[3] = {0}, pos_pf[3];
        for (size_t i = 0; i < target->npoints; ++i){
            pos_target[0] = target->points[i].x;
            pos_target[1] = target->points[i].y;
            bot_trans_apply_vec(&target_to_pf, pos_target, pos_pf);
            bot_lcmgl_vertex3f(lcmgl_target, pos_pf[0], pos_pf[1], 0);
        }
         
        bot_lcmgl_end(lcmgl_target);
    }

    //draw the points 
    if(lcmgl_match){
        double pos[3] = {0}, trans_pos[3];
        
        bot_lcmgl_color3f(lcmgl_match, 1.0, .0, .0);
        bot_lcmgl_point_size(lcmgl_match, 2);
        bot_lcmgl_begin(lcmgl_match, GL_POINTS);
        
        for (size_t i = 0; i < match->npoints; ++i)
            {
                pos[0] = match->points[i].x;
                pos[1] = match->points[i].y;
                bot_trans_apply_vec(&match_to_pf, pos, trans_pos);
                
                bot_lcmgl_vertex3f(lcmgl_match, trans_pos[0], trans_pos[1], 0);
            }
        bot_lcmgl_end(lcmgl_match);
    }
    
    if(matched_id >=0){
        fprintf(stderr, "Found Valid Match\n");
        *converged = 1;

        result_points = pointlist2d_new(match->npoints);

        BotTrans result = results.at(matched_id).Tf;

        BotTrans sm_to_pf = result;
        
        BotTrans sm_to_target; //this is what we want 
        bot_trans_apply_trans_to(&pf_to_target,  &sm_to_pf, &sm_to_target);
        
        double rpy_result[3]; 
        bot_quat_to_roll_pitch_yaw (result.rot_quat, rpy_result);

        *result_sm_to_target = sm_to_target;

        print_trans(result, "\n\n Matched Result (sm to pf) Transform");

        print_trans(sm_to_target, "\n\n Matched Result (sm to target) Transform");

        print_trans(*result_sm_to_target, "\n\n Returned Result (sm to pf) Transform");

        double pos[3] = {0}, trans_pos[3];
        for(int i=0; i < match->npoints; i++){
            pos[0] = match->points[i].x;
            pos[1] = match->points[i].y;
            bot_trans_apply_vec(&result, pos, trans_pos);
            result_points->points[i].x = trans_pos[0];
            result_points->points[i].y = trans_pos[1];
        }

        if(lcmgl_result){
            if(max_matched > 700 || max_matched / (double)  match->npoints > 0.5 ){
                bot_lcmgl_color3f(lcmgl_result, 0.5, 1.0, .0);
            }
            else{
                bot_lcmgl_color3f(lcmgl_result, 1.0, 0.0, .0);
            }

            bot_lcmgl_point_size(lcmgl_result, 5);
            bot_lcmgl_begin(lcmgl_result, GL_POINTS);
            
            fprintf(stderr, "ICP - (PCL) Drawing result +++++ \n");
            
            for(int i = 0; i < result_points->npoints ; i++){
                bot_lcmgl_vertex3f(lcmgl_result, result_points->points[i].x, result_points->points[i].y, 0);
            }

            bot_lcmgl_end(lcmgl_result);

            pos[0] = match->points[0].x;
            pos[1] = match->points[0].y;
            
            bot_trans_apply_vec(&result, pos, trans_pos);

            char match_info[512];
            sprintf(match_info,"Match : %d Score : %f", results.at(matched_id).n_match, results.at(matched_id).score);
            double text_pos[3] = {trans_pos[0]+5.0, trans_pos[1] + 5,0};
            bot_lcmgl_text(lcmgl_result, text_pos, match_info);
            
            bot_lcmgl_switch_buffer(lcmgl_result);
        }
    }
    else{
        fprintf(stderr, "Failed to find match\n");
    }

    std::cout << "Rough iteration has converged:" << *converged << " score: " <<
        *fitness_score << std::endl;
        
    fprintf(stderr, "Found Matches : %d out of : %d\n", max_matched , match->npoints);

    //if overlap is less than 50% of if the count is less than 3 * no of scans (assumed to be 250 points) 

    int valid_overlap = 0;
    
    pointlist2d_free(cloud_target);
    pointlist2d_free(cloud_match);
    return result_points;
}
