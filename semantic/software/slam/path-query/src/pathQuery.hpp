#ifndef PATHQUERY_H_
#define PATHQUERY_H_

#include <vector>
#include <list>
#include <glib.h>

#include <bot_core/bot_core.h>
#include <bot_lcmgl_client/lcmgl.h>
#include <geom_utils/geometry.h>

#include <lcmtypes/slam_laser_pose_t.h>
#include <lcmtypes/slam_graph_particle_list_t.h>

namespace pathquery {

typedef std::list<point2d_t *>Path;
    
class pathQuery
{
public:
    pathQuery (bool _verbose);
    ~pathQuery();

    bool
    getPath (int particle_id, int node_start_id, int node_goal_id, 
             std::list<point2d_t> &path);

  std::list<point2d_t> *
  getPathPython (int particle_id, int node_start_id, int node_goal_id);

    lcm_t *lcm;
    int lcm_fileno;
    GMutex *mutex;
    
    GHashTable *laser_hash;
    GHashTable *particle_hash;

    bot_lcmgl_t *lcmgl_map;
    bot_lcmgl_t *lcmgl_prm;
    bot_lcmgl_t *lcmgl_trajs;

  int last_node_id;

};


}

#endif
