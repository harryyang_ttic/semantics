#include "single_integrator_system.hpp"
#include <vector>
#include <list>
#include <bot_core/bot_core.h>
#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif
#include <eigen_utils/eigen_utils.hpp>

using namespace occ_map;
using namespace std;
using namespace Eigen;

#define FREE_THRESH .1
#define DIST_DELTA .1

namespace prm_planner {

    double siGetPath (const Eigen::Vector3d x_start, const Eigen::Vector3d x_end,
                          SIPath & path)
    {
        double dx = x_end(0) - x_start(0);
        double dy = x_end(1) - x_start(1);
        double distance = sqrt (dx*dx + dy*dy);
        int num_increments = distance/DIST_DELTA;

        if (num_increments == 1) {
            path.push_back (Eigen::Vector3d(x_start(0), x_start(1), x_start(2)));
            path.push_back (Eigen::Vector3d(x_end(0), x_end(1), x_end(2)));

            return distance;
        }

        for (int i=0; i<(num_increments-1); i++) {
            double x = x_start(0) + i * dx/num_increments;
            double y = x_start(1) + i * dy/num_increments;
            double t = 0;

            path.push_back (Eigen::Vector3d(x, y, t));
        }

        path.push_back (Eigen::Vector3d(x_end(0), x_end(1), x_end(2)));

        return distance;
    }
        


    System::System(const occ_map::FloatPixelMap * _map, double _footprint_radius, 
                   double _collision_threshold) :
        footprint_radius(_footprint_radius), map(_map)
    {
        for (int ind = 0; ind < map->num_cells; ind++) {
            if (map->data[ind] < FREE_THRESH)
                free_cell_inds.push_back(ind);
        }

        collision_threshold = _collision_threshold;
    }
    System::~System()
    {
        //delete dubins_cost_ratio_table;
    }

    Node * System::sampleNode()
    {
        Vector3d state;
        int rand_cell = bot_irand(free_cell_inds.size());//bot_randf() * free_cell_inds.size();
        int ixy[2];
        //map->indToLoc(free_cell_inds.at(rand_cell), ixy);
        map->indToLoc(rand_cell, ixy);
        map->tableToWorld(ixy, state.data());
        state(2) = bot_randf_in_range(-M_PI, M_PI);

        return new Node(state);
    }

    Edge * System::steer(Node * start, Node * end)
    {
 
        //compute the optimal straight-line path path
        Edge * edge = new Edge(start, end);
        bot_tictoc("single integrator: get path");
        edge->edge_cost = siGetPath(start->state, end->state, edge->path);
        bot_tictoc("single integrator: get path");

        //collision check the path
        bot_tictoc("single integrator: collision check");
        bool collision = false;

        list<Eigen::Vector3d>::iterator path_it;
        for (path_it = edge->path.begin(); path_it != edge->path.end(); path_it++) {
            Eigen::Vector3d xyt = *path_it;
            if (!map->isInMap(xyt.data()) || map->readValue(xyt.data()) > collision_threshold) {
                collision = true;
                break;
            }
        }
        bot_tictoc("single integrator: collision check");

        if (collision) {
            delete edge;
            edge = NULL;
        }

        return edge;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////
    // Edge Class Methods
    //////////////////////////////////////////////////////////////////////////////////////////////////

    Node::Node(const Eigen::Vector3d & _state, bool _is_goal) :
        state(_state), cost(DBL_MAX), is_goal(_is_goal), best_in_edge(NULL)
    {
    }
    Node::~Node()
    {
        for (std::list<Edge *>::iterator it = edges_in.begin(); it != edges_in.end(); it++) {
            delete *it;
        }
    }

    void Node::drawLCMGL(bot_lcmgl_t * lcmgl)
    {
        const float * color = bot_color_util_blue;
        double xyz[3] = {this->state(0), this->state(1), 0};
        double size[2] = {1.0, 1.0};
        if (this->isGoal()) {
            color = bot_color_util_green;
            
            bot_lcmgl_circle (lcmgl, xyz, 0.5);
        }
        else 
            bot_lcmgl_rect (lcmgl, xyz, size, 1);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////
    // Edge Class Methods
    //////////////////////////////////////////////////////////////////////////////////////////////////
    Edge::Edge(Node * _start, Node * _end) :
        start(_start), end(_end), edge_cost(-1)
    {
    }
    Edge::~Edge()
    {
        list<Eigen::Vector3d>::iterator path_it = path.begin();
        while (!path.empty())
            path_it = path.erase (path_it);
    }
    void Edge::drawLCMGL(bot_lcmgl_t * lcmgl)
    {
        lcmglLineWidth(2);
        lcmglColor3fv(bot_color_util_green);

        bot_lcmgl_begin (lcmgl, GL_LINE_STRIP);
        list<Eigen::Vector3d>::iterator path_it;
        for (path_it = path.begin(); path_it != path.end(); path_it++) {
            Eigen::Vector3d xyt = *path_it;
            bot_lcmgl_vertex3d (lcmgl, xyt(0), xyt(1), 0);
        }
        bot_lcmgl_end (lcmgl);
    }

} //namespace

