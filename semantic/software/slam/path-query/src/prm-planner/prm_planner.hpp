// A simple Probabilistic Roadmap Planner...
#ifndef __prm_planner_hpp__
#define __prm_planner_hpp__
#include <occ_map/PixelMap.hpp>
#include <occ_map/VoxelMap.hpp>
#include <Eigen/Dense>
#include <list>
#include <bot_lcmgl_client/lcmgl.h>

// For now, hardcoded dubins vehicle navigating in a PixelMap
// TODO: generalize to other system types... static polymorphism?
#include "single_integrator_system.hpp"

namespace prm_planner {
    class PrmPlanner {
    public:
        PrmPlanner(System * _system);
        ~PrmPlanner();
        void addNode(Node * node);
        void createGraphNodes(int num_samples);
        void connectGraph(double connect_radius);
        const Node * runDijkstras(Node * start, bool stop_at_first_goal);

        void printStats();
        void drawLCMGL(bot_lcmgl_t * lcmgl);

        System * system;
        std::list<Node *> nodes;
    };

}

#endif
