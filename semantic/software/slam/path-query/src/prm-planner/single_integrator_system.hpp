#ifndef __simple_integrator_system_hpp__
#define __simple_integrator_system_hpp__
#include <occ_map/PixelMap.hpp>
#include <occ_map/VoxelMap.hpp>
#include <Eigen/Dense>
#include <vector>
#include <list>
#include <float.h>

#include <bot_lcmgl_client/lcmgl.h>
#include <bot_vis/gl_util.h>
#include <bot_core/math_util.h>

typedef std::list<Eigen::Vector3d> SIPath;

#define COL_THRESH .5

//a simple 2d single integrator for a Probabilistic Roadmap Planner...
namespace prm_planner {

    //predefine the class prototype
    class Edge;

    /**
     * A class to store the graph node info
     */
    class Node {
    public:
        Node(const Eigen::Vector3d & _state, bool _is_goal = false);
        ~Node();
        inline bool isGoal()
        {
            return is_goal;
        }
        Eigen::Block<Eigen::Vector3d, 2, 1> getStateKey()
        {
            return state.head<2>();
        }
        void drawLCMGL(bot_lcmgl_t * lcmgl);

        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Eigen::Vector3d state;
        std::list<Edge *> edges_in;
        std::list<Edge *> edges_out;
        double cost; //the cost to reach this node
        int is_goal; //whether this node is a goal node
        Edge * best_in_edge;

        //needed for priority queue used in dijkstras
        class NodeCostCompare {
        public:
            bool operator()(const Node * n1, const Node * n2)
            {
                return n1->cost > n2->cost;
            }
        };
    };

    /**
     * Class to store information about the edge between two nodes
     */
    class Edge {
    public:
        Edge(Node * _start, Node * _end);
        ~Edge();
        void drawLCMGL(bot_lcmgl_t * lcmgl);

        Node * start;
        Node * end;
        double edge_cost;
        std::list<Eigen::Vector3d> path;

    };

    /**
     * The system class defines the vehicle model
     */
    class System {
    public:
        System(const occ_map::FloatPixelMap * _map, double _footprint_radius, 
               double _collision_threshold = COL_THRESH);
        ~System();
        Node * sampleNode();
        Edge * steer(Node *start, Node * end);

        //system state
        double footprint_radius;
        double collision_threshold;
        const occ_map::FloatPixelMap * map;
        std::vector<int> free_cell_inds; //vector containing a list of the free map cells
    };

}

#endif
