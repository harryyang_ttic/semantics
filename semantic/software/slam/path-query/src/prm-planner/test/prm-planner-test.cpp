#include <prm_planner/prm_planner.hpp>

#include <iostream>
#include <algorithm>
#include <bot_core/bot_core.h>
#include <bot_lcmgl_client/lcmgl.h>
#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

using namespace prm_planner;
using namespace std;
using namespace Eigen;

static uint8_t floatToUint8(float v)
{
  return (1.0 - v) * 255.0;
}
void drawMap(bot_lcmgl_t * lcmgl_map, occ_map::FloatPixelMap * map)
{
  bot_lcmgl_color3f(lcmgl_map, 0, 1, 0);
  occ_map::Uint8PixelMap * drawmap = new occ_map::Uint8PixelMap(map,true, floatToUint8);

  int texid = bot_lcmgl_texture2d(lcmgl_map, drawmap->data, drawmap->dimensions[0], drawmap->dimensions[1],
      drawmap->dimensions[0] * sizeof(uint8_t), BOT_LCMGL_LUMINANCE, BOT_LCMGL_UNSIGNED_BYTE, BOT_LCMGL_COMPRESS_ZLIB);

  bot_lcmgl_color4f(lcmgl_map, 1, 1, 1, .7);
  bot_lcmgl_enable(lcmgl_map, GL_BLEND);
  bot_lcmgl_enable(lcmgl_map, GL_DEPTH_TEST);

  double z_offset = -.1;
  bot_lcmgl_texture_draw_quad(lcmgl_map, texid,
      drawmap->xy0[0], drawmap->xy0[1], z_offset,
      drawmap->xy0[0], drawmap->xy1[1], z_offset,
      drawmap->xy1[0], drawmap->xy1[1], z_offset,
      drawmap->xy1[0], drawmap->xy0[1], z_offset);

  bot_lcmgl_disable(lcmgl_map, GL_BLEND);
  bot_lcmgl_disable(lcmgl_map, GL_DEPTH_TEST);
}

void drawTrajToNode(bot_lcmgl_t * lcmgl, Node * goalNode)
{
  Node * n = goalNode;
  while (n->best_in_edge != NULL) {
    n->best_in_edge->drawLCMGL(lcmgl);
    n = n->best_in_edge->start;
  }
}

int main(int argc, char ** argv)
{

    //if (argc < 1 || argc > 1) {
    //printf("usage:\n");
    //printf("     %s map-name [num_samples] [connect_radius]\n", argv[0]);
    //exit(1);
    //}
  //string mapFilename = argv[1];

  float footprint_radius = 2;
  int num_samples = 5000;
  float connect_radius = 5;

  if (argc >= 1)
      num_samples = atoi(argv[1]);
  if (argc >= 2)
      connect_radius = atof(argv[2]);

  cout << "PRM is alive with " << num_samples << " samples, and " << " footprint_radius " << footprint_radius << endl;

  srand( time(NULL));

  lcm_t * lcm = lcm_create(NULL);
  bot_lcmgl_t * lcmgl_prm = bot_lcmgl_init(lcm, "prm-roadmap");
  bot_lcmgl_t * lcmgl_map = bot_lcmgl_init(lcm, "prm-occmap");
  bot_lcmgl_t * lcmgl_trajs = bot_lcmgl_init(lcm, "prm-trajs");

  double xyz0[2] = {0, 0};
  double xyz1[2] = {10,10};
  double mpp = 0.1;
  occ_map::FloatPixelMap * map = new occ_map::FloatPixelMap(xyz0, xyz1, mpp, 0.08);

  System * sys = new System(map, footprint_radius);
  Node * start = new Node(Vector3d(2, 2, 0));
  Node * goal = new Node(Vector3d(4, 8, 0), true);

  //draw the start and goal
  start->drawLCMGL(lcmgl_map);
  goal->drawLCMGL(lcmgl_map);
  //draw the map
  drawMap(lcmgl_map, map);
  bot_lcmgl_switch_buffer(lcmgl_map);


  PrmPlanner * prm = new PrmPlanner(sys);
  //add the start and goal nodes
  prm->addNode(start);
  prm->addNode(goal);

  prm->createGraphNodes(num_samples);
  prm->connectGraph(connect_radius);

  cout << "PRM is done, getting best path" << endl;

  //get the best trajectory the the goal nodes
  const Node * closestGoal = prm->runDijkstras(start, false);


  //draw the graph
  //this overloads LCM buffers
  //prm->drawLCMGL(lcmgl_prm);
  //bot_lcmgl_switch_buffer(lcmgl_prm);

  if (closestGoal != NULL) {
    //draw the trajectories
    for (list<Node *>::iterator it = prm->nodes.begin(); it != prm->nodes.end(); it++) {
      Node * n = *it;
      if (n->isGoal() && n->cost > 0) {
        drawTrajToNode(lcmgl_trajs, n);
      }
    }
    bot_lcmgl_switch_buffer(lcmgl_trajs);

    cout << "Found a path to the goal!\n";
  }
  else {
    cout << "No path to the goal found!\n";
  }

  //cleanup
  delete prm;
  bot_lcmgl_destroy(lcmgl_map);
  bot_lcmgl_destroy(lcmgl_prm);
  bot_lcmgl_destroy(lcmgl_trajs);
  lcm_destroy(lcm);
  return 1;
}
