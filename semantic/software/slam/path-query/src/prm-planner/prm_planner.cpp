#include "prm_planner.hpp"
#include <queue>
#include <kdtree/kdtree.h>
#include <bot_core/bot_core.h>
#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

using namespace occ_map;
using namespace std;
using namespace Eigen;

#define FREE_THRESH .1
#define OCC_THRESH .15

namespace prm_planner {

    //the abstact PRM planning module
    PrmPlanner::PrmPlanner(System * _system) :
        system(_system)
    {
    }
    PrmPlanner::~PrmPlanner()
    {
        for (list<Node *>::iterator it = nodes.begin(); it != nodes.end(); it++) {
            delete *it;
        }
        delete system;
    }

    void PrmPlanner::addNode(Node * node)
    {
        nodes.push_back(node);
    }

    void PrmPlanner::createGraphNodes(int numSamples)
    {
        for (int i = 0; i < numSamples; i++)
            {
                addNode(system->sampleNode());
            }
    }

    void PrmPlanner::connectGraph(double connectRadius)
    {

        //put all the nodes into a kdtree
        kdtree_t * kdtree = kd_create(nodes.front()->getStateKey().rows());
        for (list<Node *>::iterator it = nodes.begin(); it != nodes.end(); it++) {
            Node * n = *it;
            kd_insert(kdtree, n->getStateKey().data(), n);
        }

        //for each unconnected node, try to connect it to all nodes within the connectRadius
        for (list<Node *>::iterator it = nodes.begin(); it != nodes.end(); it++) {
            Node * start_node = *it;

            //get the nearest neighbors using kdtree
            bot_tictoc("prm: query kdtree");
            kdres_t * kdres = kd_nearest_range(kdtree, start_node->getStateKey().data(), connectRadius);
            bot_tictoc("prm: query kdtree");
            kd_res_rewind(kdres);

            //iterate through nearby neighbors
            while (!kd_res_end(kdres)) {
                Node *end_node = (Node *) kd_res_item_data(kdres);
                kd_res_next(kdres);

                // try to connect the two nodes
                if (start_node != end_node) {
                    Edge * edge = system->steer(start_node, end_node);
                    if (edge != NULL) {
                        start_node->edges_out.push_back(edge);
                        end_node->edges_in.push_back(edge);
                    }
                }
            }
            // Free temporary memory allocated by kdtree
            kd_res_free(kdres);
        }
    }

    void PrmPlanner::printStats()
    {
        bot_tictoc_print_stats (BOT_TICTOC_AVG);
    }

    void PrmPlanner::drawLCMGL(bot_lcmgl_t * lcmgl)
    {
        for (list<Node *>::iterator nit = nodes.begin(); nit != nodes.end(); nit++) {
            Node * n = *nit;
            //draw the node
            n->drawLCMGL(lcmgl);
            //draw all the edges out
            for (list<Edge *>::iterator eit = n->edges_out.begin(); eit != n->edges_out.end(); eit++) {
                Edge * e = *eit;
                e->drawLCMGL(lcmgl);
            }
        }
    }

    const Node * PrmPlanner::runDijkstras(Node * start, bool stop_at_first_goal)
    {
        //set all nodes to infinite cost
        for (list<Node *>::iterator nit = nodes.begin(); nit != nodes.end(); nit++) {
            Node * n = *nit;
            n->cost = DBL_MAX;
        }

        Node * goalNode = NULL;

        priority_queue<Node *, std::vector<Node *>, Node::NodeCostCompare> frontier;
        start->cost = 0;
        start->best_in_edge = NULL;
        frontier.push(start);
        int count = 0;
        while (!frontier.empty()) {
            Node * n = frontier.top();
            frontier.pop();

            if (n->cost < 0)
                continue; //node has already been processed (was a duplicate)

            if (n->isGoal() || (goalNode != NULL && n->cost < goalNode->cost))
                goalNode = n;
            if (stop_at_first_goal && goalNode != NULL) {
                goalNode = n;
                break;
            }

            std::list<Edge *>::iterator it;
            for (it = n->edges_out.begin(); it != n->edges_out.end(); it++) {
                Edge * e = *it;
                Node * en = e->end;
                double costToNode = n->cost + e->edge_cost;
                if (en->cost > costToNode) {
                    en->cost = costToNode;
                    en->best_in_edge = e;
                    frontier.push(en); //may be a duplicate...
                }
            }

            //make n's cost negative to mark it as processed...
            n->cost = -n->cost;
        }

        //put the costs back to positive, make unreachable nodes have cost -1
        for (list<Node *>::iterator nit = nodes.begin(); nit != nodes.end(); nit++) {
            Node * n = *nit;
            if (n->cost < 0) {
                n->cost = -n->cost;
            }
            else if (n->cost > FLT_MAX) {
                n->cost = -1;
            }
        }

        return goalNode;
    }

} //namespace

