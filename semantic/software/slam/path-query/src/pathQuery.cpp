#include <stdio.h>
#include <unistd.h>
#include <occ_map/PixelMap.hpp>
#include <prm_planner/prm_planner.hpp>

#  define __PRI64_PREFIX        "l"

# define PRId64         __PRI64_PREFIX "d"


#include "pathQuery.hpp"

using namespace std;
using namespace prm_planner;
using namespace Eigen;

#define HIT_INC 0.2
#define MISS_INC -0.1

#define PRM_NUM_SAMPLES 1000
#define PRM_CONNECT_RADIUS 1.0

namespace pathquery {

    void 
    destroy_particle(gpointer data)
    {
        if(data != NULL){
            slam_graph_particle_t *p = (slam_graph_particle_t *)data;
            slam_graph_particle_t_destroy(p);
        }
    }

    static void
    on_particle_list (const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                      const char *channel __attribute__((unused)),
                      const slam_graph_particle_list_t *msg, void *user)
    {
        pathQuery *self = (pathQuery *) user;

        g_hash_table_foreach (self->particle_hash, (GHFunc) slam_graph_particle_t_destroy, NULL);
        g_hash_table_remove_all (self->particle_hash);

        for (int i=0; i<msg->no_particles; i++) {
            slam_graph_particle_t *particle =  slam_graph_particle_t_copy(&msg->particle_list[i]);
            g_hash_table_insert (self->particle_hash, &(particle->id), particle);
        }
    }


    static void
    on_slam_laser_pose (const lcm_recv_buf_t *rbuf, const char *channel,
                        const slam_laser_pose_t *msg, void *user)
    {

        pathQuery *self = (pathQuery *) user;

        slam_laser_pose_t *laser_pose = slam_laser_pose_t_copy (msg);
        g_hash_table_insert (self->laser_hash, &(laser_pose->id), laser_pose);
        printf("pathQuery: received laser pose: %d\n", laser_pose->id);
        self->last_node_id = laser_pose->id;

    }



    pathQuery::pathQuery(bool _verbose = false)
    {
        lcm = bot_lcm_get_global (NULL);

        lcmgl_prm = bot_lcmgl_init (lcm, "prm-roadmap");
        lcmgl_map = bot_lcmgl_init (lcm, "prm-occmap");
        lcmgl_trajs = bot_lcmgl_init (lcm, "prm-trajs");
        lcm_fileno = lcm_get_fileno(lcm);

        bot_glib_mainloop_attach_lcm (lcm);

        mutex = g_mutex_new();

        laser_hash = g_hash_table_new (g_int_hash, g_int_equal);
        particle_hash = g_hash_table_new (g_int_hash, g_int_equal);


        slam_graph_particle_list_t_subscribe (lcm, "PARTICLE_ISAM_RESULT",
                                              on_particle_list, this);

        slam_laser_pose_t_subscribe (lcm, "SLAM_POSE_LASER_POINTS",
                                     on_slam_laser_pose, this);

    } 
   

    pathQuery::~pathQuery()
    {
        g_hash_table_foreach (laser_hash, (GHFunc) slam_laser_pose_t_destroy, NULL);
        g_hash_table_remove_all (laser_hash);

        g_hash_table_foreach (particle_hash, (GHFunc) slam_graph_particle_t_destroy, NULL);
        g_hash_table_remove_all (particle_hash);

        bot_lcmgl_destroy(lcmgl_map);
        bot_lcmgl_destroy(lcmgl_prm);
        bot_lcmgl_destroy(lcmgl_trajs);

    }
    
    static uint8_t floatToUint8(float v)
    {
        return (1.0 - v) * 255.0;
    }


    void drawMap(bot_lcmgl_t * _lcmgl_map, occ_map::FloatPixelMap * map)
    {
        bot_lcmgl_color3f(_lcmgl_map, 0, 1, 0);
        occ_map::Uint8PixelMap * drawmap = new occ_map::Uint8PixelMap(map,true, floatToUint8);

        int texid = bot_lcmgl_texture2d(_lcmgl_map, drawmap->data, drawmap->dimensions[0], drawmap->dimensions[1],
                                        drawmap->dimensions[0] * sizeof(uint8_t), BOT_LCMGL_LUMINANCE, BOT_LCMGL_UNSIGNED_BYTE, BOT_LCMGL_COMPRESS_ZLIB);

        bot_lcmgl_color4f(_lcmgl_map, 1, 1, 1, .7);
        bot_lcmgl_enable(_lcmgl_map, GL_BLEND);
        bot_lcmgl_enable(_lcmgl_map, GL_DEPTH_TEST);

        double z_offset = -.1;
        bot_lcmgl_texture_draw_quad(_lcmgl_map, texid,
                                    drawmap->xy0[0], drawmap->xy0[1], z_offset,
                                    drawmap->xy0[0], drawmap->xy1[1], z_offset,
                                    drawmap->xy1[0], drawmap->xy1[1], z_offset,
                                    drawmap->xy1[0], drawmap->xy0[1], z_offset);

        bot_lcmgl_disable(_lcmgl_map, GL_BLEND);
        bot_lcmgl_disable(_lcmgl_map, GL_DEPTH_TEST);
    }


    void drawTrajToNode(bot_lcmgl_t * lcmgl, Node * goalNode)
    {
        Node * n = goalNode;
        while (n->best_in_edge != NULL) {
            n->best_in_edge->drawLCMGL(lcmgl);
            n = n->best_in_edge->start;
        }
    }

  std::list<point2d_t> *
  pathQuery::getPathPython (int particle_id, int node_start_id, int node_goal_id)
    {
      std::list<point2d_t> * path = new std::list<point2d_t>();
      getPath(particle_id, node_start_id, node_goal_id, *path);
      return path;
    }


    bool
    pathQuery::getPath (int particle_id, int node_start_id, int node_goal_id,
                       std::list<point2d_t> &path)
    {
      if (node_start_id == -1) {
        node_start_id = last_node_id;
      }
      if (node_goal_id == -1) {
        node_goal_id = last_node_id;
      }
        g_mutex_lock (mutex);

        // Clear lcmgl
        bot_lcmgl_destroy(lcmgl_map);
        bot_lcmgl_destroy(lcmgl_prm);
        bot_lcmgl_destroy(lcmgl_trajs);
        lcmgl_prm = bot_lcmgl_init (lcm, "prm-roadmap");
        lcmgl_map = bot_lcmgl_init (lcm, "prm-occmap");
        lcmgl_trajs = bot_lcmgl_init (lcm, "prm-trajs");

        slam_graph_particle_t *particle = (slam_graph_particle_t *) g_hash_table_lookup (this->particle_hash, &particle_id);

        if (!particle) {
            fprintf (stderr, "Error: pathQuery does not have a particle with id %"PRId64"\n", particle_id);
            g_mutex_unlock (mutex);
            return false;
        }

        slam_laser_pose_t *laser_pose_start =  (slam_laser_pose_t *) g_hash_table_lookup (this->laser_hash, &node_start_id);

        if (!laser_pose_start) {
            fprintf (stderr, "Error: pathQuery does not have laser_pose with start id %"PRId64"\n", node_start_id);
            g_mutex_unlock (mutex);
            return false;
        }

        slam_laser_pose_t *laser_pose_goal = (slam_laser_pose_t *) g_hash_table_lookup (this->laser_hash, &node_goal_id);

        if (!laser_pose_goal) {
            fprintf (stderr, "Error: pathQuery does not have laser_pose with end id %"PRId64"\n", node_goal_id);
            g_mutex_unlock (mutex);
            return false;
        }

        slam_graph_node_t *node_start = (slam_graph_node_t *) calloc (1, sizeof (slam_graph_node_t));
        slam_graph_node_t *node_goal = (slam_graph_node_t *) calloc (1, sizeof (slam_graph_node_t));

        for (int i=0; i<particle->no_nodes; i++) {
            slam_graph_node_t node = particle->node_list[i];
        
            if (node.id == node_start_id)
                node_start = slam_graph_node_t_copy (&node);
            else if (node.id == node_goal_id)
                node_goal = slam_graph_node_t_copy (&node);
        }

        if (!node_start) {
            fprintf (stderr, "Error: pathQuery does not have a node with id %"PRId64" for particle id %"PRId64"\n",
                     node_start_id, particle_id);
            g_mutex_unlock (mutex);
            return false;
        }

        if (!node_goal) {
            fprintf (stderr, "Error: pathQuery does not have a node with id %"PRId64" for particle id %"PRId64"\n",
                     node_goal_id, particle_id);
            g_mutex_unlock (mutex);
            return false;
        }

        double xy0[2], xy1[2];

        xy0[0] = min(node_start->xy[0], node_goal->xy[0]);
        xy0[1] = min(node_start->xy[1], node_goal->xy[1]);
        xy1[0] = max(node_start->xy[0], node_goal->xy[0]);
        xy1[1] = max(node_start->xy[1], node_goal->xy[1]);

        // add a buffer
        xy0[0] += -5;
        xy0[1] += -5;
        xy1[0] += 5;
        xy1[1] += 5;

        occ_map::FloatPixelMap * pixmap = new occ_map::FloatPixelMap (xy0, xy1, 0.1, 0.0);

        bot_tictoc ("populate pixmap");
        for (int i=0; i<particle->no_nodes; i++) {
            slam_graph_node_t node = particle->node_list[i];
            
            // Hack: ignore nodes that are more than

            // Hack: ignore nodes that are outside the pixel map
            if ( (node.xy[0] < xy0[0]) ||  (node.xy[1] < xy0[1]) || (node.xy[0] > xy1[0]) ||  (node.xy[1] > xy1[1]) )
                continue;

            slam_laser_pose_t *laser_pose = (slam_laser_pose_t *) g_hash_table_lookup (laser_hash, &(node.id));
        
            if (!laser_pose) {
              fprintf (stderr, "Error: pathQuery: Node in particle has no matching slam_laser_pose: %d\n", node.id);
              continue;
            }

            BotTrans bodyToLocal;
            bodyToLocal.trans_vec[0] = node.xy[0];
            bodyToLocal.trans_vec[1] = node.xy[1];
            bodyToLocal.trans_vec[2] = 0;
            double rpy[3] = {0.0, 0.0, node.heading };
            bot_roll_pitch_yaw_to_quat (rpy, bodyToLocal.rot_quat);
            double pBody[3] = {0, 0, 0};
            double pLocal[3];

            float clamp_bounds[2] = {0.0, 1.0};

            for (int j=0; j < laser_pose->pl.no; j++) {
                pBody[0] = laser_pose->pl.points[j].pos[0];
                pBody[1] = laser_pose->pl.points[j].pos[1];
                pBody[2] = 0;
                bot_trans_apply_vec(&bodyToLocal, pBody, pLocal);

                double pXY[2] = {pLocal[0], pLocal[1]};
                if (pixmap->isInMap (pXY))
                    pixmap->updateValue (pXY, HIT_INC, clamp_bounds);
            }
        }
        bot_tictoc ("populate pixmap");

        System *sys = new System (pixmap, 1, 0.5);
        Node *start = new Node (Vector3d(node_start->xy[0], node_start->xy[1], node_start->heading), false);
        Node *goal = new Node (Vector3d(node_goal->xy[0], node_goal->xy[1], node_goal->heading), true);

        start->drawLCMGL(this->lcmgl_map);
        goal->drawLCMGL(this->lcmgl_map);
        drawMap (this->lcmgl_map, pixmap);
        bot_lcmgl_switch_buffer(this->lcmgl_map);


        PrmPlanner *prm = new PrmPlanner(sys);
        prm->addNode(start);
        prm->addNode(goal);

        prm->createGraphNodes(PRM_NUM_SAMPLES);
        prm->connectGraph(PRM_CONNECT_RADIUS);

        cout << "PRM is done, getting best path" << endl;

        //get the best trajectory the the goal nodes
        bot_tictoc("prm: dijkstras");
        const Node * closestGoal = prm->runDijkstras(start, false);
        bot_tictoc("prm: dijkstras");

        //draw the graph
        //this overloads LCM buffers
        //prm->drawLCMGL(lcmgl_prm);
        //bot_lcmgl_switch_buffer(lcmgl_prm);

        bool foundPath = false;
        if (closestGoal != NULL) {
            foundPath = true;
            
            //draw the trajectories
            for (list<Node *>::iterator it = prm->nodes.begin(); it != prm->nodes.end(); it++) {
                Node * n = *it;
                if (n->isGoal() && n->cost > 0) {
                    Node *nFromGoal = n;
                    point2d_t pos;
                    while (nFromGoal->best_in_edge != NULL) {
                        pos.x = nFromGoal->state(0);
                        pos.y = nFromGoal->state(1);
                        path.push_front (pos);
                        nFromGoal = nFromGoal->best_in_edge->start;
                    }
                    pos.x = nFromGoal->state(0);
                    pos.y = nFromGoal->state(1);
                    path.push_front (pos);
                    drawTrajToNode(lcmgl_trajs, n);
                }
            }
            bot_lcmgl_switch_buffer(lcmgl_trajs);

            cout << "Found a path to the goal!\n";
        }
        else {
            cout << "No path to the goal found!\n";
        }

        prm->printStats();

        delete prm;
        //delete start;
        //delete goal;
        delete pixmap;

        
        g_mutex_unlock (mutex);
        return foundPath;

    }
}
    
        
        
