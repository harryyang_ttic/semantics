# Create a shared library libprm_planner.so with a single source file
add_library(pathquery SHARED pathQuery.cpp)

# make the header public
# install it to include/prm_planner
pods_install_headers(pathQuery.hpp DESTINATION pathquery)

# make the library public
pods_install_libraries(pathquery)

# uncomment these lines to link against another library via pkg-config
set(REQUIRED_PACKAGES prm-planner eigen3 occ-map kdtree bot2-core bot2-lcmgl-client bot2-vis lcmtypes_slam-lcmtypes)
pods_use_pkg_config_packages(pathquery ${REQUIRED_PACKAGES})

# create a pkg-config file for the library, to make it easier for other
# software to use.
pods_install_pkg_config_file(pathquery
    CFLAGS
    LIBS -lpathquery
    REQUIRES ${REQUIRED_PACKAGES}
    VERSION 0.0.1)
