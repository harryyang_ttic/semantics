/** THIS IS AN AUTOMATICALLY GENERATED FILE.  DO NOT MODIFY
 * BY HAND!!
 *
 * Generated by lcm-gen
 **/

#include <string.h>
#include "lcmtypes/slam_label_info_t.h"

static int __slam_label_info_t_hash_computed;
static int64_t __slam_label_info_t_hash;

int64_t __slam_label_info_t_hash_recursive(const __lcm_hash_ptr *p)
{
    const __lcm_hash_ptr *fp;
    for (fp = p; fp != NULL; fp = fp->parent)
        if (fp->v == __slam_label_info_t_get_hash)
            return 0;

    __lcm_hash_ptr cp;
    cp.parent =  p;
    cp.v = (void*)__slam_label_info_t_get_hash;
    (void) cp;

    int64_t hash = (int64_t)0x8402a44bb199099cLL
         + __int32_t_hash_recursive(&cp)
         + __int32_t_hash_recursive(&cp)
         + __string_hash_recursive(&cp)
        ;

    return (hash<<1) + ((hash>>63)&1);
}

int64_t __slam_label_info_t_get_hash(void)
{
    if (!__slam_label_info_t_hash_computed) {
        __slam_label_info_t_hash = __slam_label_info_t_hash_recursive(NULL);
        __slam_label_info_t_hash_computed = 1;
    }

    return __slam_label_info_t_hash;
}

int __slam_label_info_t_encode_array(void *buf, int offset, int maxlen, const slam_label_info_t *p, int elements)
{
    int pos = 0, thislen, element;

    for (element = 0; element < elements; element++) {

        thislen = __int32_t_encode_array(buf, offset + pos, maxlen - pos, &(p[element].count), 1);
        if (thislen < 0) return thislen; else pos += thislen;

        thislen = __int32_t_encode_array(buf, offset + pos, maxlen - pos, p[element].id, p[element].count);
        if (thislen < 0) return thislen; else pos += thislen;

        thislen = __string_encode_array(buf, offset + pos, maxlen - pos, p[element].names, p[element].count);
        if (thislen < 0) return thislen; else pos += thislen;

    }
    return pos;
}

int slam_label_info_t_encode(void *buf, int offset, int maxlen, const slam_label_info_t *p)
{
    int pos = 0, thislen;
    int64_t hash = __slam_label_info_t_get_hash();

    thislen = __int64_t_encode_array(buf, offset + pos, maxlen - pos, &hash, 1);
    if (thislen < 0) return thislen; else pos += thislen;

    thislen = __slam_label_info_t_encode_array(buf, offset + pos, maxlen - pos, p, 1);
    if (thislen < 0) return thislen; else pos += thislen;

    return pos;
}

int __slam_label_info_t_encoded_array_size(const slam_label_info_t *p, int elements)
{
    int size = 0, element;
    for (element = 0; element < elements; element++) {

        size += __int32_t_encoded_array_size(&(p[element].count), 1);

        size += __int32_t_encoded_array_size(p[element].id, p[element].count);

        size += __string_encoded_array_size(p[element].names, p[element].count);

    }
    return size;
}

int slam_label_info_t_encoded_size(const slam_label_info_t *p)
{
    return 8 + __slam_label_info_t_encoded_array_size(p, 1);
}

int __slam_label_info_t_decode_array(const void *buf, int offset, int maxlen, slam_label_info_t *p, int elements)
{
    int pos = 0, thislen, element;

    for (element = 0; element < elements; element++) {

        thislen = __int32_t_decode_array(buf, offset + pos, maxlen - pos, &(p[element].count), 1);
        if (thislen < 0) return thislen; else pos += thislen;

        p[element].id = (int32_t*) lcm_malloc(sizeof(int32_t) * p[element].count);
        thislen = __int32_t_decode_array(buf, offset + pos, maxlen - pos, p[element].id, p[element].count);
        if (thislen < 0) return thislen; else pos += thislen;

        p[element].names = (char**) lcm_malloc(sizeof(char*) * p[element].count);
        thislen = __string_decode_array(buf, offset + pos, maxlen - pos, p[element].names, p[element].count);
        if (thislen < 0) return thislen; else pos += thislen;

    }
    return pos;
}

int __slam_label_info_t_decode_array_cleanup(slam_label_info_t *p, int elements)
{
    int element;
    for (element = 0; element < elements; element++) {

        __int32_t_decode_array_cleanup(&(p[element].count), 1);

        __int32_t_decode_array_cleanup(p[element].id, p[element].count);
        if (p[element].id) free(p[element].id);

        __string_decode_array_cleanup(p[element].names, p[element].count);
        if (p[element].names) free(p[element].names);

    }
    return 0;
}

int slam_label_info_t_decode(const void *buf, int offset, int maxlen, slam_label_info_t *p)
{
    int pos = 0, thislen;
    int64_t hash = __slam_label_info_t_get_hash();

    int64_t this_hash;
    thislen = __int64_t_decode_array(buf, offset + pos, maxlen - pos, &this_hash, 1);
    if (thislen < 0) return thislen; else pos += thislen;
    if (this_hash != hash) return -1;

    thislen = __slam_label_info_t_decode_array(buf, offset + pos, maxlen - pos, p, 1);
    if (thislen < 0) return thislen; else pos += thislen;

    return pos;
}

int slam_label_info_t_decode_cleanup(slam_label_info_t *p)
{
    return __slam_label_info_t_decode_array_cleanup(p, 1);
}

int __slam_label_info_t_clone_array(const slam_label_info_t *p, slam_label_info_t *q, int elements)
{
    int element;
    for (element = 0; element < elements; element++) {

        __int32_t_clone_array(&(p[element].count), &(q[element].count), 1);

        q[element].id = (int32_t*) lcm_malloc(sizeof(int32_t) * q[element].count);
        __int32_t_clone_array(p[element].id, q[element].id, p[element].count);

        q[element].names = (char**) lcm_malloc(sizeof(char*) * q[element].count);
        __string_clone_array(p[element].names, q[element].names, p[element].count);

    }
    return 0;
}

slam_label_info_t *slam_label_info_t_copy(const slam_label_info_t *p)
{
    slam_label_info_t *q = (slam_label_info_t*) malloc(sizeof(slam_label_info_t));
    __slam_label_info_t_clone_array(p, q, 1);
    return q;
}

void slam_label_info_t_destroy(slam_label_info_t *p)
{
    __slam_label_info_t_decode_array_cleanup(p, 1);
    free(p);
}

int slam_label_info_t_publish(lcm_t *lc, const char *channel, const slam_label_info_t *p)
{
      int max_data_size = slam_label_info_t_encoded_size (p);
      uint8_t *buf = (uint8_t*) malloc (max_data_size);
      if (!buf) return -1;
      int data_size = slam_label_info_t_encode (buf, 0, max_data_size, p);
      if (data_size < 0) {
          free (buf);
          return data_size;
      }
      int status = lcm_publish (lc, channel, buf, data_size);
      free (buf);
      return status;
}

struct _slam_label_info_t_subscription_t {
    slam_label_info_t_handler_t user_handler;
    void *userdata;
    lcm_subscription_t *lc_h;
};
static
void slam_label_info_t_handler_stub (const lcm_recv_buf_t *rbuf,
                            const char *channel, void *userdata)
{
    int status;
    slam_label_info_t p;
    memset(&p, 0, sizeof(slam_label_info_t));
    status = slam_label_info_t_decode (rbuf->data, 0, rbuf->data_size, &p);
    if (status < 0) {
        fprintf (stderr, "error %d decoding slam_label_info_t!!!\n", status);
        return;
    }

    slam_label_info_t_subscription_t *h = (slam_label_info_t_subscription_t*) userdata;
    h->user_handler (rbuf, channel, &p, h->userdata);

    slam_label_info_t_decode_cleanup (&p);
}

slam_label_info_t_subscription_t* slam_label_info_t_subscribe (lcm_t *lcm,
                    const char *channel,
                    slam_label_info_t_handler_t f, void *userdata)
{
    slam_label_info_t_subscription_t *n = (slam_label_info_t_subscription_t*)
                       malloc(sizeof(slam_label_info_t_subscription_t));
    n->user_handler = f;
    n->userdata = userdata;
    n->lc_h = lcm_subscribe (lcm, channel,
                                 slam_label_info_t_handler_stub, n);
    if (n->lc_h == NULL) {
        fprintf (stderr,"couldn't reg slam_label_info_t LCM handler!\n");
        free (n);
        return NULL;
    }
    return n;
}

int slam_label_info_t_subscription_set_queue_capacity (slam_label_info_t_subscription_t* subs,
                              int num_messages)
{
    return lcm_subscription_set_queue_capacity (subs->lc_h, num_messages);
}

int slam_label_info_t_unsubscribe(lcm_t *lcm, slam_label_info_t_subscription_t* hid)
{
    int status = lcm_unsubscribe (lcm, hid->lc_h);
    if (0 != status) {
        fprintf(stderr,
           "couldn't unsubscribe slam_label_info_t_handler %p!\n", hid);
        return -1;
    }
    free (hid);
    return 0;
}

