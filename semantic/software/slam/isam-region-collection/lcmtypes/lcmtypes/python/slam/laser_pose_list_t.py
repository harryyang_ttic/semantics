"""LCM type definitions
This file automatically generated by lcm.
DO NOT MODIFY BY HAND!!!!
"""

try:
    import cStringIO.StringIO as BytesIO
except ImportError:
    from io import BytesIO
import struct

import slam.laser_pose_t

class laser_pose_list_t(object):
    __slots__ = ["utime", "no_poses", "scans"]

    def __init__(self):
        self.utime = 0
        self.no_poses = 0
        self.scans = []

    def encode(self):
        buf = BytesIO()
        buf.write(laser_pose_list_t._get_packed_fingerprint())
        self._encode_one(buf)
        return buf.getvalue()

    def _encode_one(self, buf):
        buf.write(struct.pack(">qi", self.utime, self.no_poses))
        for i0 in range(self.no_poses):
            assert self.scans[i0]._get_packed_fingerprint() == slam.laser_pose_t._get_packed_fingerprint()
            self.scans[i0]._encode_one(buf)

    def decode(data):
        if hasattr(data, 'read'):
            buf = data
        else:
            buf = BytesIO(data)
        if buf.read(8) != laser_pose_list_t._get_packed_fingerprint():
            raise ValueError("Decode error")
        return laser_pose_list_t._decode_one(buf)
    decode = staticmethod(decode)

    def _decode_one(buf):
        self = laser_pose_list_t()
        self.utime, self.no_poses = struct.unpack(">qi", buf.read(12))
        self.scans = []
        for i0 in range(self.no_poses):
            self.scans.append(slam.laser_pose_t._decode_one(buf))
        return self
    _decode_one = staticmethod(_decode_one)

    _hash = None
    def _get_hash_recursive(parents):
        if laser_pose_list_t in parents: return 0
        newparents = parents + [laser_pose_list_t]
        tmphash = (0xe32ec48ebd574c53+ slam.laser_pose_t._get_hash_recursive(newparents)) & 0xffffffffffffffff
        tmphash  = (((tmphash<<1)&0xffffffffffffffff)  + (tmphash>>63)) & 0xffffffffffffffff
        return tmphash
    _get_hash_recursive = staticmethod(_get_hash_recursive)
    _packed_fingerprint = None

    def _get_packed_fingerprint():
        if laser_pose_list_t._packed_fingerprint is None:
            laser_pose_list_t._packed_fingerprint = struct.pack(">Q", laser_pose_list_t._get_hash_recursive([]))
        return laser_pose_list_t._packed_fingerprint
    _get_packed_fingerprint = staticmethod(_get_packed_fingerprint)

