"""LCM type definitions
This file automatically generated by lcm.
DO NOT MODIFY BY HAND!!!!
"""

try:
    import cStringIO.StringIO as BytesIO
except ImportError:
    from io import BytesIO
import struct

class dirichlet_update_t(object):
    __slots__ = ["particle_id", "node_id", "label_id", "increment"]

    def __init__(self):
        self.particle_id = 0
        self.node_id = 0
        self.label_id = 0
        self.increment = 0.0

    def encode(self):
        buf = BytesIO()
        buf.write(dirichlet_update_t._get_packed_fingerprint())
        self._encode_one(buf)
        return buf.getvalue()

    def _encode_one(self, buf):
        buf.write(struct.pack(">qqid", self.particle_id, self.node_id, self.label_id, self.increment))

    def decode(data):
        if hasattr(data, 'read'):
            buf = data
        else:
            buf = BytesIO(data)
        if buf.read(8) != dirichlet_update_t._get_packed_fingerprint():
            raise ValueError("Decode error")
        return dirichlet_update_t._decode_one(buf)
    decode = staticmethod(decode)

    def _decode_one(buf):
        self = dirichlet_update_t()
        self.particle_id, self.node_id, self.label_id, self.increment = struct.unpack(">qqid", buf.read(28))
        return self
    _decode_one = staticmethod(_decode_one)

    _hash = None
    def _get_hash_recursive(parents):
        if dirichlet_update_t in parents: return 0
        tmphash = (0x1e6bdde2acf09980) & 0xffffffffffffffff
        tmphash  = (((tmphash<<1)&0xffffffffffffffff)  + (tmphash>>63)) & 0xffffffffffffffff
        return tmphash
    _get_hash_recursive = staticmethod(_get_hash_recursive)
    _packed_fingerprint = None

    def _get_packed_fingerprint():
        if dirichlet_update_t._packed_fingerprint is None:
            dirichlet_update_t._packed_fingerprint = struct.pack(">Q", dirichlet_update_t._get_hash_recursive([]))
        return dirichlet_update_t._packed_fingerprint
    _get_packed_fingerprint = staticmethod(_get_packed_fingerprint)

