"""LCM type definitions
This file automatically generated by lcm.
DO NOT MODIFY BY HAND!!!!
"""

try:
    import cStringIO.StringIO as BytesIO
except ImportError:
    from io import BytesIO
import struct

import slam.slu_landmark_result_t

class slu_figure_result_t(object):
    __slots__ = ["figure_id", "prob", "no_landmarks", "landmark_results", "no_nodes", "path_node_ids"]

    def __init__(self):
        self.figure_id = 0
        self.prob = 0.0
        self.no_landmarks = 0
        self.landmark_results = []
        self.no_nodes = 0
        self.path_node_ids = []

    def encode(self):
        buf = BytesIO()
        buf.write(slu_figure_result_t._get_packed_fingerprint())
        self._encode_one(buf)
        return buf.getvalue()

    def _encode_one(self, buf):
        buf.write(struct.pack(">qdi", self.figure_id, self.prob, self.no_landmarks))
        for i0 in range(self.no_landmarks):
            assert self.landmark_results[i0]._get_packed_fingerprint() == slam.slu_landmark_result_t._get_packed_fingerprint()
            self.landmark_results[i0]._encode_one(buf)
        buf.write(struct.pack(">i", self.no_nodes))
        buf.write(struct.pack('>%di' % self.no_nodes, *self.path_node_ids[:self.no_nodes]))

    def decode(data):
        if hasattr(data, 'read'):
            buf = data
        else:
            buf = BytesIO(data)
        if buf.read(8) != slu_figure_result_t._get_packed_fingerprint():
            raise ValueError("Decode error")
        return slu_figure_result_t._decode_one(buf)
    decode = staticmethod(decode)

    def _decode_one(buf):
        self = slu_figure_result_t()
        self.figure_id, self.prob, self.no_landmarks = struct.unpack(">qdi", buf.read(20))
        self.landmark_results = []
        for i0 in range(self.no_landmarks):
            self.landmark_results.append(slam.slu_landmark_result_t._decode_one(buf))
        self.no_nodes = struct.unpack(">i", buf.read(4))[0]
        self.path_node_ids = struct.unpack('>%di' % self.no_nodes, buf.read(self.no_nodes * 4))
        return self
    _decode_one = staticmethod(_decode_one)

    _hash = None
    def _get_hash_recursive(parents):
        if slu_figure_result_t in parents: return 0
        newparents = parents + [slu_figure_result_t]
        tmphash = (0x6b295102df05f28e+ slam.slu_landmark_result_t._get_hash_recursive(newparents)) & 0xffffffffffffffff
        tmphash  = (((tmphash<<1)&0xffffffffffffffff)  + (tmphash>>63)) & 0xffffffffffffffff
        return tmphash
    _get_hash_recursive = staticmethod(_get_hash_recursive)
    _packed_fingerprint = None

    def _get_packed_fingerprint():
        if slu_figure_result_t._packed_fingerprint is None:
            slu_figure_result_t._packed_fingerprint = struct.pack(">Q", slu_figure_result_t._get_hash_recursive([]))
        return slu_figure_result_t._packed_fingerprint
    _get_packed_fingerprint = staticmethod(_get_packed_fingerprint)

