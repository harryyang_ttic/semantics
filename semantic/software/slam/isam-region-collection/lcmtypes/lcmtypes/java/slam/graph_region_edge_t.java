/* LCM type definition class file
 * This file was automatically generated by lcm-gen
 * DO NOT MODIFY BY HAND!!!!
 */

package slam;
 
import java.io.*;
import java.util.*;
import lcm.lcm.*;
 
public final class graph_region_edge_t implements lcm.lcm.LCMEncodable
{
    public long edge_id;
    public byte type;
    public long region_1_id;
    public long region_2_id;
 
    public graph_region_edge_t()
    {
    }
 
    public static final long LCM_FINGERPRINT;
    public static final long LCM_FINGERPRINT_BASE = 0x1fff77a3c52609b6L;
 
    public static final byte TYPE_INCREMENTAL = (byte) 0;
    public static final byte TYPE_LOOPCLOSURE = (byte) 1;

    static {
        LCM_FINGERPRINT = _hashRecursive(new ArrayList<Class<?>>());
    }
 
    public static long _hashRecursive(ArrayList<Class<?>> classes)
    {
        if (classes.contains(slam.graph_region_edge_t.class))
            return 0L;
 
        classes.add(slam.graph_region_edge_t.class);
        long hash = LCM_FINGERPRINT_BASE
            ;
        classes.remove(classes.size() - 1);
        return (hash<<1) + ((hash>>63)&1);
    }
 
    public void encode(DataOutput outs) throws IOException
    {
        outs.writeLong(LCM_FINGERPRINT);
        _encodeRecursive(outs);
    }
 
    public void _encodeRecursive(DataOutput outs) throws IOException
    {
        outs.writeLong(this.edge_id); 
 
        outs.writeByte(this.type); 
 
        outs.writeLong(this.region_1_id); 
 
        outs.writeLong(this.region_2_id); 
 
    }
 
    public graph_region_edge_t(byte[] data) throws IOException
    {
        this(new LCMDataInputStream(data));
    }
 
    public graph_region_edge_t(DataInput ins) throws IOException
    {
        if (ins.readLong() != LCM_FINGERPRINT)
            throw new IOException("LCM Decode error: bad fingerprint");
 
        _decodeRecursive(ins);
    }
 
    public static slam.graph_region_edge_t _decodeRecursiveFactory(DataInput ins) throws IOException
    {
        slam.graph_region_edge_t o = new slam.graph_region_edge_t();
        o._decodeRecursive(ins);
        return o;
    }
 
    public void _decodeRecursive(DataInput ins) throws IOException
    {
        this.edge_id = ins.readLong();
 
        this.type = ins.readByte();
 
        this.region_1_id = ins.readLong();
 
        this.region_2_id = ins.readLong();
 
    }
 
    public slam.graph_region_edge_t copy()
    {
        slam.graph_region_edge_t outobj = new slam.graph_region_edge_t();
        outobj.edge_id = this.edge_id;
 
        outobj.type = this.type;
 
        outobj.region_1_id = this.region_1_id;
 
        outobj.region_2_id = this.region_2_id;
 
        return outobj;
    }
 
}

