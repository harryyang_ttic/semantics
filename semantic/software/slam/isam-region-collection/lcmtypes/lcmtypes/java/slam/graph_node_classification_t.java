/* LCM type definition class file
 * This file was automatically generated by lcm-gen
 * DO NOT MODIFY BY HAND!!!!
 */

package slam;
 
import java.io.*;
import java.util.*;
import lcm.lcm.*;
 
public final class graph_node_classification_t implements lcm.lcm.LCMEncodable
{
    public long node_id;
    public byte no_image_classes;
    public byte no_laser_classes;
    public slam.probability_element_t image_classification[];
    public slam.probability_element_t laser_classification[];
 
    public graph_node_classification_t()
    {
    }
 
    public static final long LCM_FINGERPRINT;
    public static final long LCM_FINGERPRINT_BASE = 0x168d695ba47e14bdL;
 
    static {
        LCM_FINGERPRINT = _hashRecursive(new ArrayList<Class<?>>());
    }
 
    public static long _hashRecursive(ArrayList<Class<?>> classes)
    {
        if (classes.contains(slam.graph_node_classification_t.class))
            return 0L;
 
        classes.add(slam.graph_node_classification_t.class);
        long hash = LCM_FINGERPRINT_BASE
             + slam.probability_element_t._hashRecursive(classes)
             + slam.probability_element_t._hashRecursive(classes)
            ;
        classes.remove(classes.size() - 1);
        return (hash<<1) + ((hash>>63)&1);
    }
 
    public void encode(DataOutput outs) throws IOException
    {
        outs.writeLong(LCM_FINGERPRINT);
        _encodeRecursive(outs);
    }
 
    public void _encodeRecursive(DataOutput outs) throws IOException
    {
        outs.writeLong(this.node_id); 
 
        outs.writeByte(this.no_image_classes); 
 
        outs.writeByte(this.no_laser_classes); 
 
        for (int a = 0; a < this.no_image_classes; a++) {
            this.image_classification[a]._encodeRecursive(outs); 
        }
 
        for (int a = 0; a < this.no_laser_classes; a++) {
            this.laser_classification[a]._encodeRecursive(outs); 
        }
 
    }
 
    public graph_node_classification_t(byte[] data) throws IOException
    {
        this(new LCMDataInputStream(data));
    }
 
    public graph_node_classification_t(DataInput ins) throws IOException
    {
        if (ins.readLong() != LCM_FINGERPRINT)
            throw new IOException("LCM Decode error: bad fingerprint");
 
        _decodeRecursive(ins);
    }
 
    public static slam.graph_node_classification_t _decodeRecursiveFactory(DataInput ins) throws IOException
    {
        slam.graph_node_classification_t o = new slam.graph_node_classification_t();
        o._decodeRecursive(ins);
        return o;
    }
 
    public void _decodeRecursive(DataInput ins) throws IOException
    {
        this.node_id = ins.readLong();
 
        this.no_image_classes = ins.readByte();
 
        this.no_laser_classes = ins.readByte();
 
        this.image_classification = new slam.probability_element_t[(int) no_image_classes];
        for (int a = 0; a < this.no_image_classes; a++) {
            this.image_classification[a] = slam.probability_element_t._decodeRecursiveFactory(ins);
        }
 
        this.laser_classification = new slam.probability_element_t[(int) no_laser_classes];
        for (int a = 0; a < this.no_laser_classes; a++) {
            this.laser_classification[a] = slam.probability_element_t._decodeRecursiveFactory(ins);
        }
 
    }
 
    public slam.graph_node_classification_t copy()
    {
        slam.graph_node_classification_t outobj = new slam.graph_node_classification_t();
        outobj.node_id = this.node_id;
 
        outobj.no_image_classes = this.no_image_classes;
 
        outobj.no_laser_classes = this.no_laser_classes;
 
        outobj.image_classification = new slam.probability_element_t[(int) no_image_classes];
        for (int a = 0; a < this.no_image_classes; a++) {
            outobj.image_classification[a] = this.image_classification[a].copy();
        }
 
        outobj.laser_classification = new slam.probability_element_t[(int) no_laser_classes];
        for (int a = 0; a < this.no_laser_classes; a++) {
            outobj.laser_classification[a] = this.laser_classification[a].copy();
        }
 
        return outobj;
    }
 
}

