/* LCM type definition class file
 * This file was automatically generated by lcm-gen
 * DO NOT MODIFY BY HAND!!!!
 */

package slam;
 
import java.io.*;
import java.util.*;
import lcm.lcm.*;
 
public final class graph_region_particle_t implements lcm.lcm.LCMEncodable
{
    public long id;
    public int no_nodes;
    public int no_close_node_pairs;
    public int no_same_region_close_pairs;
    public int max_dist_of_close_node_pairs;
    public int no_failed_close_node_pairs;
    public double average_distance_of_close_node_pairs;
    public int no_regions;
    public slam.graph_region_t region_list[];
    public int no_region_edges;
    public slam.graph_region_edge_t region_edges[];
    public int no_edges;
    public slam.graph_edge_t edge_list[];
    public double weight;
    public double weight_multiplier;
    public int no_rejected_edges;
    public slam.graph_edge_t rejected_edge_list[];
    public slam.graph_segment_list_t segments;
    public int no_objects;
    public slam.object_t objects[];
 
    public graph_region_particle_t()
    {
    }
 
    public static final long LCM_FINGERPRINT;
    public static final long LCM_FINGERPRINT_BASE = 0x66f1faffeb22e468L;
 
    static {
        LCM_FINGERPRINT = _hashRecursive(new ArrayList<Class<?>>());
    }
 
    public static long _hashRecursive(ArrayList<Class<?>> classes)
    {
        if (classes.contains(slam.graph_region_particle_t.class))
            return 0L;
 
        classes.add(slam.graph_region_particle_t.class);
        long hash = LCM_FINGERPRINT_BASE
             + slam.graph_region_t._hashRecursive(classes)
             + slam.graph_region_edge_t._hashRecursive(classes)
             + slam.graph_edge_t._hashRecursive(classes)
             + slam.graph_edge_t._hashRecursive(classes)
             + slam.graph_segment_list_t._hashRecursive(classes)
             + slam.object_t._hashRecursive(classes)
            ;
        classes.remove(classes.size() - 1);
        return (hash<<1) + ((hash>>63)&1);
    }
 
    public void encode(DataOutput outs) throws IOException
    {
        outs.writeLong(LCM_FINGERPRINT);
        _encodeRecursive(outs);
    }
 
    public void _encodeRecursive(DataOutput outs) throws IOException
    {
        outs.writeLong(this.id); 
 
        outs.writeInt(this.no_nodes); 
 
        outs.writeInt(this.no_close_node_pairs); 
 
        outs.writeInt(this.no_same_region_close_pairs); 
 
        outs.writeInt(this.max_dist_of_close_node_pairs); 
 
        outs.writeInt(this.no_failed_close_node_pairs); 
 
        outs.writeDouble(this.average_distance_of_close_node_pairs); 
 
        outs.writeInt(this.no_regions); 
 
        for (int a = 0; a < this.no_regions; a++) {
            this.region_list[a]._encodeRecursive(outs); 
        }
 
        outs.writeInt(this.no_region_edges); 
 
        for (int a = 0; a < this.no_region_edges; a++) {
            this.region_edges[a]._encodeRecursive(outs); 
        }
 
        outs.writeInt(this.no_edges); 
 
        for (int a = 0; a < this.no_edges; a++) {
            this.edge_list[a]._encodeRecursive(outs); 
        }
 
        outs.writeDouble(this.weight); 
 
        outs.writeDouble(this.weight_multiplier); 
 
        outs.writeInt(this.no_rejected_edges); 
 
        for (int a = 0; a < this.no_rejected_edges; a++) {
            this.rejected_edge_list[a]._encodeRecursive(outs); 
        }
 
        this.segments._encodeRecursive(outs); 
 
        outs.writeInt(this.no_objects); 
 
        for (int a = 0; a < this.no_objects; a++) {
            this.objects[a]._encodeRecursive(outs); 
        }
 
    }
 
    public graph_region_particle_t(byte[] data) throws IOException
    {
        this(new LCMDataInputStream(data));
    }
 
    public graph_region_particle_t(DataInput ins) throws IOException
    {
        if (ins.readLong() != LCM_FINGERPRINT)
            throw new IOException("LCM Decode error: bad fingerprint");
 
        _decodeRecursive(ins);
    }
 
    public static slam.graph_region_particle_t _decodeRecursiveFactory(DataInput ins) throws IOException
    {
        slam.graph_region_particle_t o = new slam.graph_region_particle_t();
        o._decodeRecursive(ins);
        return o;
    }
 
    public void _decodeRecursive(DataInput ins) throws IOException
    {
        this.id = ins.readLong();
 
        this.no_nodes = ins.readInt();
 
        this.no_close_node_pairs = ins.readInt();
 
        this.no_same_region_close_pairs = ins.readInt();
 
        this.max_dist_of_close_node_pairs = ins.readInt();
 
        this.no_failed_close_node_pairs = ins.readInt();
 
        this.average_distance_of_close_node_pairs = ins.readDouble();
 
        this.no_regions = ins.readInt();
 
        this.region_list = new slam.graph_region_t[(int) no_regions];
        for (int a = 0; a < this.no_regions; a++) {
            this.region_list[a] = slam.graph_region_t._decodeRecursiveFactory(ins);
        }
 
        this.no_region_edges = ins.readInt();
 
        this.region_edges = new slam.graph_region_edge_t[(int) no_region_edges];
        for (int a = 0; a < this.no_region_edges; a++) {
            this.region_edges[a] = slam.graph_region_edge_t._decodeRecursiveFactory(ins);
        }
 
        this.no_edges = ins.readInt();
 
        this.edge_list = new slam.graph_edge_t[(int) no_edges];
        for (int a = 0; a < this.no_edges; a++) {
            this.edge_list[a] = slam.graph_edge_t._decodeRecursiveFactory(ins);
        }
 
        this.weight = ins.readDouble();
 
        this.weight_multiplier = ins.readDouble();
 
        this.no_rejected_edges = ins.readInt();
 
        this.rejected_edge_list = new slam.graph_edge_t[(int) no_rejected_edges];
        for (int a = 0; a < this.no_rejected_edges; a++) {
            this.rejected_edge_list[a] = slam.graph_edge_t._decodeRecursiveFactory(ins);
        }
 
        this.segments = slam.graph_segment_list_t._decodeRecursiveFactory(ins);
 
        this.no_objects = ins.readInt();
 
        this.objects = new slam.object_t[(int) no_objects];
        for (int a = 0; a < this.no_objects; a++) {
            this.objects[a] = slam.object_t._decodeRecursiveFactory(ins);
        }
 
    }
 
    public slam.graph_region_particle_t copy()
    {
        slam.graph_region_particle_t outobj = new slam.graph_region_particle_t();
        outobj.id = this.id;
 
        outobj.no_nodes = this.no_nodes;
 
        outobj.no_close_node_pairs = this.no_close_node_pairs;
 
        outobj.no_same_region_close_pairs = this.no_same_region_close_pairs;
 
        outobj.max_dist_of_close_node_pairs = this.max_dist_of_close_node_pairs;
 
        outobj.no_failed_close_node_pairs = this.no_failed_close_node_pairs;
 
        outobj.average_distance_of_close_node_pairs = this.average_distance_of_close_node_pairs;
 
        outobj.no_regions = this.no_regions;
 
        outobj.region_list = new slam.graph_region_t[(int) no_regions];
        for (int a = 0; a < this.no_regions; a++) {
            outobj.region_list[a] = this.region_list[a].copy();
        }
 
        outobj.no_region_edges = this.no_region_edges;
 
        outobj.region_edges = new slam.graph_region_edge_t[(int) no_region_edges];
        for (int a = 0; a < this.no_region_edges; a++) {
            outobj.region_edges[a] = this.region_edges[a].copy();
        }
 
        outobj.no_edges = this.no_edges;
 
        outobj.edge_list = new slam.graph_edge_t[(int) no_edges];
        for (int a = 0; a < this.no_edges; a++) {
            outobj.edge_list[a] = this.edge_list[a].copy();
        }
 
        outobj.weight = this.weight;
 
        outobj.weight_multiplier = this.weight_multiplier;
 
        outobj.no_rejected_edges = this.no_rejected_edges;
 
        outobj.rejected_edge_list = new slam.graph_edge_t[(int) no_rejected_edges];
        for (int a = 0; a < this.no_rejected_edges; a++) {
            outobj.rejected_edge_list[a] = this.rejected_edge_list[a].copy();
        }
 
        outobj.segments = this.segments.copy();
 
        outobj.no_objects = this.no_objects;
 
        outobj.objects = new slam.object_t[(int) no_objects];
        for (int a = 0; a < this.no_objects; a++) {
            outobj.objects[a] = this.objects[a].copy();
        }
 
        return outobj;
    }
 
}

