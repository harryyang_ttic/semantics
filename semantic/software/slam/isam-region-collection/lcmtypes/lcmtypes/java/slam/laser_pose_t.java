/* LCM type definition class file
 * This file was automatically generated by lcm-gen
 * DO NOT MODIFY BY HAND!!!!
 */

package slam;
 
import java.io.*;
import java.util.*;
import lcm.lcm.*;
 
public final class laser_pose_t implements lcm.lcm.LCMEncodable
{
    public long utime;
    public int id;
    public double rp[];
    public slam.scan_point_list_t pl;
 
    public laser_pose_t()
    {
        rp = new double[2];
    }
 
    public static final long LCM_FINGERPRINT;
    public static final long LCM_FINGERPRINT_BASE = 0xebfb144cccd689bcL;
 
    static {
        LCM_FINGERPRINT = _hashRecursive(new ArrayList<Class<?>>());
    }
 
    public static long _hashRecursive(ArrayList<Class<?>> classes)
    {
        if (classes.contains(slam.laser_pose_t.class))
            return 0L;
 
        classes.add(slam.laser_pose_t.class);
        long hash = LCM_FINGERPRINT_BASE
             + slam.scan_point_list_t._hashRecursive(classes)
            ;
        classes.remove(classes.size() - 1);
        return (hash<<1) + ((hash>>63)&1);
    }
 
    public void encode(DataOutput outs) throws IOException
    {
        outs.writeLong(LCM_FINGERPRINT);
        _encodeRecursive(outs);
    }
 
    public void _encodeRecursive(DataOutput outs) throws IOException
    {
        outs.writeLong(this.utime); 
 
        outs.writeInt(this.id); 
 
        for (int a = 0; a < 2; a++) {
            outs.writeDouble(this.rp[a]); 
        }
 
        this.pl._encodeRecursive(outs); 
 
    }
 
    public laser_pose_t(byte[] data) throws IOException
    {
        this(new LCMDataInputStream(data));
    }
 
    public laser_pose_t(DataInput ins) throws IOException
    {
        if (ins.readLong() != LCM_FINGERPRINT)
            throw new IOException("LCM Decode error: bad fingerprint");
 
        _decodeRecursive(ins);
    }
 
    public static slam.laser_pose_t _decodeRecursiveFactory(DataInput ins) throws IOException
    {
        slam.laser_pose_t o = new slam.laser_pose_t();
        o._decodeRecursive(ins);
        return o;
    }
 
    public void _decodeRecursive(DataInput ins) throws IOException
    {
        this.utime = ins.readLong();
 
        this.id = ins.readInt();
 
        this.rp = new double[(int) 2];
        for (int a = 0; a < 2; a++) {
            this.rp[a] = ins.readDouble();
        }
 
        this.pl = slam.scan_point_list_t._decodeRecursiveFactory(ins);
 
    }
 
    public slam.laser_pose_t copy()
    {
        slam.laser_pose_t outobj = new slam.laser_pose_t();
        outobj.utime = this.utime;
 
        outobj.id = this.id;
 
        outobj.rp = new double[(int) 2];
        System.arraycopy(this.rp, 0, outobj.rp, 0, 2); 
        outobj.pl = this.pl.copy();
 
        return outobj;
    }
 
}

