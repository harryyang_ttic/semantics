/* LCM type definition class file
 * This file was automatically generated by lcm-gen
 * DO NOT MODIFY BY HAND!!!!
 */

package slam;
 
import java.io.*;
import java.util.*;
import lcm.lcm.*;
 
public final class label_info_t implements lcm.lcm.LCMEncodable
{
    public int count;
    public int id[];
    public String names[];
 
    public label_info_t()
    {
    }
 
    public static final long LCM_FINGERPRINT;
    public static final long LCM_FINGERPRINT_BASE = 0x8402a44bb199099cL;
 
    static {
        LCM_FINGERPRINT = _hashRecursive(new ArrayList<Class<?>>());
    }
 
    public static long _hashRecursive(ArrayList<Class<?>> classes)
    {
        if (classes.contains(slam.label_info_t.class))
            return 0L;
 
        classes.add(slam.label_info_t.class);
        long hash = LCM_FINGERPRINT_BASE
            ;
        classes.remove(classes.size() - 1);
        return (hash<<1) + ((hash>>63)&1);
    }
 
    public void encode(DataOutput outs) throws IOException
    {
        outs.writeLong(LCM_FINGERPRINT);
        _encodeRecursive(outs);
    }
 
    public void _encodeRecursive(DataOutput outs) throws IOException
    {
        char[] __strbuf = null;
        outs.writeInt(this.count); 
 
        for (int a = 0; a < this.count; a++) {
            outs.writeInt(this.id[a]); 
        }
 
        for (int a = 0; a < this.count; a++) {
            __strbuf = new char[this.names[a].length()]; this.names[a].getChars(0, this.names[a].length(), __strbuf, 0); outs.writeInt(__strbuf.length+1); for (int _i = 0; _i < __strbuf.length; _i++) outs.write(__strbuf[_i]); outs.writeByte(0); 
        }
 
    }
 
    public label_info_t(byte[] data) throws IOException
    {
        this(new LCMDataInputStream(data));
    }
 
    public label_info_t(DataInput ins) throws IOException
    {
        if (ins.readLong() != LCM_FINGERPRINT)
            throw new IOException("LCM Decode error: bad fingerprint");
 
        _decodeRecursive(ins);
    }
 
    public static slam.label_info_t _decodeRecursiveFactory(DataInput ins) throws IOException
    {
        slam.label_info_t o = new slam.label_info_t();
        o._decodeRecursive(ins);
        return o;
    }
 
    public void _decodeRecursive(DataInput ins) throws IOException
    {
        char[] __strbuf = null;
        this.count = ins.readInt();
 
        this.id = new int[(int) count];
        for (int a = 0; a < this.count; a++) {
            this.id[a] = ins.readInt();
        }
 
        this.names = new String[(int) count];
        for (int a = 0; a < this.count; a++) {
            __strbuf = new char[ins.readInt()-1]; for (int _i = 0; _i < __strbuf.length; _i++) __strbuf[_i] = (char) (ins.readByte()&0xff); ins.readByte(); this.names[a] = new String(__strbuf);
        }
 
    }
 
    public slam.label_info_t copy()
    {
        slam.label_info_t outobj = new slam.label_info_t();
        outobj.count = this.count;
 
        outobj.id = new int[(int) count];
        if (this.count > 0)
            System.arraycopy(this.id, 0, outobj.id, 0, this.count); 
        outobj.names = new String[(int) count];
        if (this.count > 0)
            System.arraycopy(this.names, 0, outobj.names, 0, this.count); 
        return outobj;
    }
 
}

