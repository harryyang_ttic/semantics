/* LCM type definition class file
 * This file was automatically generated by lcm-gen
 * DO NOT MODIFY BY HAND!!!!
 */

package slam;
 
import java.io.*;
import java.util.*;
import lcm.lcm.*;
 
public final class probability_element_t implements lcm.lcm.LCMEncodable
{
    public int type;
    public double probability;
 
    public probability_element_t()
    {
    }
 
    public static final long LCM_FINGERPRINT;
    public static final long LCM_FINGERPRINT_BASE = 0x5360c53b571cca6eL;
 
    static {
        LCM_FINGERPRINT = _hashRecursive(new ArrayList<Class<?>>());
    }
 
    public static long _hashRecursive(ArrayList<Class<?>> classes)
    {
        if (classes.contains(slam.probability_element_t.class))
            return 0L;
 
        classes.add(slam.probability_element_t.class);
        long hash = LCM_FINGERPRINT_BASE
            ;
        classes.remove(classes.size() - 1);
        return (hash<<1) + ((hash>>63)&1);
    }
 
    public void encode(DataOutput outs) throws IOException
    {
        outs.writeLong(LCM_FINGERPRINT);
        _encodeRecursive(outs);
    }
 
    public void _encodeRecursive(DataOutput outs) throws IOException
    {
        outs.writeInt(this.type); 
 
        outs.writeDouble(this.probability); 
 
    }
 
    public probability_element_t(byte[] data) throws IOException
    {
        this(new LCMDataInputStream(data));
    }
 
    public probability_element_t(DataInput ins) throws IOException
    {
        if (ins.readLong() != LCM_FINGERPRINT)
            throw new IOException("LCM Decode error: bad fingerprint");
 
        _decodeRecursive(ins);
    }
 
    public static slam.probability_element_t _decodeRecursiveFactory(DataInput ins) throws IOException
    {
        slam.probability_element_t o = new slam.probability_element_t();
        o._decodeRecursive(ins);
        return o;
    }
 
    public void _decodeRecursive(DataInput ins) throws IOException
    {
        this.type = ins.readInt();
 
        this.probability = ins.readDouble();
 
    }
 
    public slam.probability_element_t copy()
    {
        slam.probability_element_t outobj = new slam.probability_element_t();
        outobj.type = this.type;
 
        outobj.probability = this.probability;
 
        return outobj;
    }
 
}

