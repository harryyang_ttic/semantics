# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/harry/Documents/Robotics/semantic/software/slam/isam-region-collection/renderer/src/renderer_region_graph.cpp" "/home/harry/Documents/Robotics/semantic/software/slam/isam-region-collection/renderer/pod-build/src/CMakeFiles/semantic-map-renderers.dir/renderer_region_graph.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "include"
  "/home/harry/Documents/Robotics/semantic/software/build/include"
  "/usr/include/glib-2.0"
  "/usr/lib/x86_64-linux-gnu/glib-2.0/include"
  "/usr/include/gtk-2.0"
  "/usr/lib/x86_64-linux-gnu/gtk-2.0/include"
  "/usr/include/atk-1.0"
  "/usr/include/cairo"
  "/usr/include/gdk-pixbuf-2.0"
  "/usr/include/pango-1.0"
  "/usr/include/gio-unix-2.0"
  "/usr/include/freetype2"
  "/usr/include/pixman-1"
  "/usr/include/libpng12"
  "/usr/include/harfbuzz"
  "/usr/include/libdrm"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
