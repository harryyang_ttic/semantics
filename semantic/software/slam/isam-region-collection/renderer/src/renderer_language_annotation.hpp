#ifndef LANGUAGE_RENDERER_H_
#define LANGUAGE_RENDERER_H_

#ifdef __cplusplus
extern "C" {
#endif
void setup_renderer_language_annotation (BotViewer *viewer, int priority, BotParam * param);
#ifdef __cplusplus
}
#endif
#endif
