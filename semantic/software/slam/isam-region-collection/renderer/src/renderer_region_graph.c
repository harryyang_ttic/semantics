#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <glib.h>
#include <string.h>
#include <assert.h>
#include <gdk/gdkkeysyms.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

#include <bot_vis/bot_vis.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <math.h>

#include <bot_core/bot_core.h>
#include <bot_vis/viewer.h>
#include <bot_vis/gtk_util.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>
#include <bot_core/math_util.h>
#include <geom_utils/geometry.h>

#include "lcmtypes/er_lcmtypes.h" 
#include <lcmtypes/slam_lcmtypes.h>

#define LINE_WIDTH 2.0
#define DTOR M_PI/180
#define RTOD 180/M_PI

#define MAX_CLASS_ID 10.0

#define PARTICLE_HISTORY_SIZE 10

#define RENDERER_NAME "Advanced Region Topological Graph"
#define DATA_CIRC_SIZE 10
#define POSE_DATA_CIRC_SIZE 200 

#define PARAM_REQUEST_RESULT "Request Result"
#define PARAM_ANIMATION_SPEED "Diff Animation Speed (ms)"
#define PARAM_ORDER_PARTICLES "Particle Order"
#define PARAM_REQUEST_MAP "Request map"
#define PARAM_CLEAR_MAP "Clear Map"
#define PARAM_COLOR_MENU "Color"
#define VALID_MAP_IND "Draw Map"
#define PARAM_DISP_PROB "Display prob."
#define PARAM_DRAW_LEGAND "Draw legend"
#define PARAM_DRAW_OBJECTS "Draw Objects"
#define PARAM_DRAW_COMPLEX_LANGUAGE "Draw Complex Language"
#define PARAM_DRAW_STATS "Draw stats"
#define PARAM_DRAW_ALL_GRAPHS "Draw all graphs"
#define PARAM_FINISH_SLAM "Finish SLAM"
//#define PARAM_DRAW_ONE_GRAPH "Draw one graph"
#define PARAM_DRAW_SIDE_BY_SIDE "Draw side-by-side"
#define PARAM_USE_AUTO_COLOR "Use Auto Color"
#define PARAM_DRAW_GRAPH_1_ID "Graph ID 1 :"
#define PARAM_DRAW_COV "Draw covariance"
//#define PARAM_DRAW_LABEL_PIE_CHART "Draw label pie chart"
#define PARAM_DRAW_LABEL_NAME "Draw name"
#define PARAM_DRAW_ALL_MAX_LABELS "Draw All Max Labels"
#define PARAM_DRAW_MAX_LABEL "Draw Max Label"
#define PARAM_PUBLISH_REGION_TRANSITION "Publish Region Change"
#define PARAM_DRAW_GRAPH_2_ID "Graph ID 2 :"
#define PARAM_DRAW_VALID_GRAPH "Draw Valid Graph"
#define PARAM_DRAW_NODE_ID "Draw Node IDs"
#define PARAM_DRAW_REGION_ID "Draw Region IDs"
#define PARAM_DRAW_SEGMENT_ID "Draw Segment IDs"
#define PARAM_DRAW_NODES "Draw Nodes"
#define PARAM_DRAW_REGIONS "Draw Regions"
#define PARAM_DRAW_NO_LANG_DIST "Draw No LANG Semantics" //draw the distributions without language input 
#define PARAM_DRAW_NO_ANSWERS_DIST "Draw No Answers Semantics" //draw the distributions without language input 
#define PARAM_DRAW_REGION_SEMANTICS "Draw Region Semantic Dist"
#define PARAM_DRAW_REGION_SEMANTICS_MAX "Draw Region Semantic Max"
#define PARAM_DRAW_REGION_LABEL "Draw Region Label Dist"
#define PARAM_DRAW_REGION_LABEL_VALID "Draw Valid Labels"
#define PARAM_DRAW_REGION_LABEL_INVALID "Draw Invalid Labels (black)"
#define PARAM_DRAW_PERFORMANCE "Draw Performance"
#define PARAM_DRAW_REGION_LABEL_MAX "Draw Region Label Max"
#define PARAM_DRAW_REGION_LABEL_BAR_GRAPH "Draw Label Bar Graph"
#define PARAM_DRAW_NODE_CLASSIFICATION "Draw Node Class"
#define PARAM_DRAW_NODE_CLASSIFICATION_MAX "Draw Node Max-like"
#define PARAM_DRAW_LASER_CLASSIFICATION "Draw Laser Class"
#define PARAM_DRAW_LASER_CLASSIFICATION_MAX "Draw Laser Max-like"
#define PARAM_DRAW_IMAGE_CLASSIFICATION "Draw Image Class"
#define PARAM_DRAW_IMAGE_CLASSIFICATION_MAX "Draw Image Max-like"
#define PARAM_DRAW_CLASSIFICATION_TEXT "Draw Text"
#define PARAM_DRAW_REGIONS "Draw Regions"
#define PARAM_DRAW_REGION_CONNECTIONS "Draw Region Connections"
#define PARAM_DRAW_INTRA_REGION_EDGES "Draw Intra-region Edges"
#define PARAM_DRAW_INTER_REGION_EDGES "Draw Inter-region Edges"
#define PARAM_DRAW_EDGES_IN_BLACK "Draw Edges in Black"
#define PARAM_REMAP_REGIONS "Remap Region Colors"
#define PARAM_DRAW_LOG "Draw LOG Scale"
#define PARAM_DRAW_ODOM "Highlight Odometry"
#define PARAM_DRAW_DEAD_EDGES "Draw Dead Edges"
#define PARAM_DRAW_MAX_GRAPH "Draw Max Graph"
#define PARAM_COLOR_REGIONS "Color regions"
#define PARAM_DRAW_MAP_POINTS "Draw Map Points"
#define PARAM_EDGE_THICKNESS "Edge Thickness"
#define PARAM_LINE_THICKNESS "Line (pie) Thickness"
#define PARAM_COLOR_MENU "Color"
#define PARAM_DRAW_HEIGHT "Draw node height"
#define PARAM_NODE_RADIUS "Node Radius"
#define PARAM_BAR_GRAPH_SCALE "Bar Graph Scale"
#define PARAM_HISTORY_LENGTH "Scan Hist. Len."
#define PARAM_HISTORY_FREQUENCY "Scan Hist. Freq."
#define PARAM_DRAW_REGION_MEAN "Draw Region Mean"
#define PARAM_POINT_SIZE "Point Size" 
#define PARAM_POINT_ALPHA "Point Alpha" 
#define PARAM_DRAW_EQUAL_DISTANCE "Equal graph spacing"
#define PARAM_DISTANCE_SCALE "Graph Dist Scale"
#define PARAM_ADD_CONSTRAINT_LABEL "Add Constraint"
#define PARAM_CONSTRAINT_TYPE "Type: "
#define PARAM_CONFIRM_AND_PUBLISH "Confirm and Publish"
#define ADD_LANG_LABEL "Write language label"
#define LANG_LABEL "Language label: "
#define LANG_CONSTRAINT_TYPE "Type of constraint: "
#define PARAM_CREATE_LANGUAGE_FILE "Start new annotation"
#define PARAM_SAVE_LANGUAGE_FILE "Save annotation file"
#define PARAM_LANGUAGE_UPDATE "Language update text: "
#define PARAM_BOUNDING_BOXES "Bounding boxes"

#define PARAM_DRAW_COMPLEX_LANGUAGE_PATHS "Draw Complex Language Paths"
#define PARAM_COMPLEX_LANGUAGE_EVENT_INDEX "Complex Language Event"
#define PARAM_DRAW_ALL_COMPLEX_LANGUAGE_PATHS "Draw All Paths"
#define PARAM_DRAW_FIG_PROB "Draw Fig Prob"
#define PARAM_IGNORE_PRIOR "Ignore Prior"
#define PARAM_DRAW_ANSWER_PROB "Draw Answer Prob"
#define PARAM_DRAW_ORIG_PROB "Draw Original Prob"
#define PARAM_DRAW_ANSWER_IND "Answer Ind"
#define PARAM_COMPLEX_PATH_INDEX "Path Index"
//#define PARAM_COMPLEX_LANGUAGE_EVENT_INDEX "Complex Language Event"
#define PARAM_DRAW_LANDMARKS "Draw Landmarks"

#define PARAM_COMPARE_PARTICLE_HISTORY_INDEX "Past Particle History"
#define PARAM_COMPARE_PARTICLE_HISTORY "Compare with Particle"

#define GRAPH_ANIMATION_RESOLUTION 10

#define QUESTION_TIMEOUT_SEC 10.0

#define PW_BASIC "Region Graph - Basic"
#define PW_GP "Region Graph - GP"
#define PW_SC "Region Graph - SC"
#define PW_LG "Region Graph - LG"
#define PW_GI "Region Graph - GI"
#define PW_DM "Region Graph - DM"
#define PW_HI "Region Graph - HI"
#define PW_GC "Region Graph - GC"
#define PW_AL "Region Graph - AL"

//#define DRAW_EDGES_TO_SUPERNODE "draw edges to supernode (versus closest nodes)"

enum {
    COLOR_Z,
    COLOR_INTENSITY,
    COLOR_NONE,
};

typedef struct _params_t{
    int draw_landmarks;
    int draw_label_name;
    int draw_odom;
    int draw_height;
    int draw_max_map;
    int draw_prob;
    int draw_map_points;
    int draw_all_graphs;
    int draw_region_ids;
    int draw_segment_ids;
    int draw_dead_edges;
    int edge_thickness;
    int line_thickness;
    int draw_valid_maps;
    int draw_cov;
    int draw_objects;
    //int draw_pie_chart;
    int draw_bar_graph;
    int draw_max_label;
    int draw_nodes;
    int draw_complex_language_paths;
    int draw_all_complex_language_paths;
    int path_index;
    int draw_regions;
    int bounding_boxes;
    int color_regions;
    int remap_regions;
    int draw_diff;
    int draw_inter_region_edges;
    int draw_intra_region_edges;
    int log_scale;
    int draw_node_ids;
    int draw_region_connections;
    int draw_edges_in_black;
    int draw_side_by_side;
    int g_id_1;
    int g_id_2;
    int particle_ordering_mode;
    int draw_region_mean;
    int distance_scale;
    int draw_semantic_label_invalid_black;
    int equal_dist;
    double node_radius;
    double bar_graph_scale;
    int draw_without_lang;
    int draw_without_answers; 
    int draw_semantic_class;
    int draw_semantic_class_max;
    int draw_semantic_label;
    int draw_semantic_label_max;
    int draw_semantic_label_valid;
    int draw_complex_language_event_index;
    int draw_laser_class; 
    int draw_complex_language;
    int draw_laser_class_max;
    int draw_node_class; 
    int draw_node_class_max;
    int draw_image_class;
    int draw_image_class_max; 
    int draw_class_text; 
    int draw_legand;
    int draw_all_max_labels;
    int draw_stats;
    int use_c_util;
    int draw_performance;
    int draw_fig_prob; 
    int ignore_prior;
    int draw_answer_prob;
    int draw_original_prob;
    int answer_ind; 
} params_t;

typedef struct{
    int id;
    int index;
} class_index_t;

typedef struct{
    int id;
    char *name;
} class_map_t;

typedef struct _RendererRegionTopology RendererRegionTopology;
struct _RendererRegionTopology {
    BotRenderer renderer;
    BotEventHandler ehandler;
    
    lcm_t *lcm;
    BotParam *param;

    int have_data;
    GHashTable *node_scans;
    BotPtrCircular   *data_circ;

    BotViewer         *viewer;
    BotGtkParamWidget *pw;   
    BotGtkParamWidget *gp_pw;   
    BotGtkParamWidget *sc_pw;   
    BotGtkParamWidget *lg_pw;   
    BotGtkParamWidget *gi_pw;   
    BotGtkParamWidget *dm_pw;   
    BotGtkParamWidget *hi_pw;   
    BotGtkParamWidget *gc_pw;   
    BotGtkParamWidget *al_pw;   
    
    params_t params;

    int last_sent_particle_id; 

    GMutex *mutex;
    int requested_scans;
    double *xy_first;
    double *xy_second;
    int no_particles; 
    int active_particle;
    int node_1;
    int node_2;

    slam_language_question_t *last_question;
    GtkWidget *question_dialog;
    
    int64_t question_utime; 

    int current_ind;
    int active; //1 = add new constraint, 0 = inactive

    //insert to a sorted GList 
    BotPtrCircular *particle_history;

    GHashTable *node_classifications;
    
    GHashTable *type_map;
    GHashTable *appearance_map;
    GHashTable *label_map;

    GHashTable *sorted_classes;
    //GHashTable *sorted_laser_classes;
    //GHashTable *sorted_image_classes;

    GList *particle_list;
    slam_performance_t *performance; 
    slam_graph_region_particle_t *diff_particle;
    slam_complex_language_collection_list_t *language_update;
    erlcm_rigid_transform_list_t *last_slam_transform;
    int64_t diff_utime;
    int diff_iter;

    int64_t last_pose_utime;
    gchar *lang_label_filename;
};

//#define MAX_LABEL_TYPES 11

static inline int remap_region_ind(int ind, int middle){
    //split them in half - based on even or odd 
    int rem = ind % 2;
    
    int add = 0;
    
    if(middle %2==0)
        add = 1;

    //even 
    if(rem ==0){
        if(ind <= middle)
            return ind;
        else
            return ind - (middle+add);
    }
    else{
        if(ind <= middle)
            return ind+(middle+add);
        else
            return ind;
    }
}

//set a fixed set of colors - up to 12 ids 

const float *renderer_color_util_jet(int id)
{
    static const float color_orange[3] = {1, 0.647, 0};
    static const float color_purple[3] = {0.627, 0.125, 0.941};
    static const float color_rose[3] = {1.0, 0.894, 0.882};
    static const float color_brown[3] = {0.545, 0.27, 0.075};
    static const float color_green_yellow[3] = {0.486, 0.988 , 0.};
    static const float color_forrest_green[3] = {0.133, 0.545, 0.133};
    static const float color_navy_blue[3] = {0, 0., 0.5};
    static const float color_maroon[3] = {0.5, 0., 0};

    switch(id){
    case 0:
        return bot_color_util_blue;
    case 1:
        return bot_color_util_red;
    case 2:
        return bot_color_util_green;
    case 3:
        return color_forrest_green;
    case 4:
        return color_purple;//color_maroon;
    case 5:
        return color_orange;        
    case 6:        
        return bot_color_util_yellow;//
    case 7:        
        return color_navy_blue;
    case 8:        
        return bot_color_util_magenta;
    case 9:        
        return bot_color_util_cyan; 
    default:
        return bot_color_util_black; 
    }     
}

static double get_color_ratio_from_appearance_id(RendererRegionTopology *self, int id){
    //int middle = ((g_hash_table_size(self->appearance_map) +1) / 2.0) -1;
    //id  = remap_region_ind(id, middle);
    double ratio = id / (double) (g_hash_table_size(self->appearance_map) - 1);
    return ratio;
}

static double get_color_ratio_from_class_id(RendererRegionTopology *self, int id){
    //int middle = ((g_hash_table_size(self->type_map) + 1) / 2.0) -1;
    //id  = remap_region_ind(id, middle);
    /*class_index_t *map = (class_index_t *) g_hash_table_lookup(self->sorted_classes, &id);
    if(map == NULL){
        //fprintf(stderr, "Error - no class id found\n");
        return 0.5;
        }*/
    double ratio = id / (double) (g_hash_table_size(self->type_map) - 1);
    return ratio;
}

static double get_color_ratio_from_label_id(RendererRegionTopology *self, int id){
    //int middle = ((g_hash_table_size(self->label_map) + 1) / 2.0) -1;
    //id  = remap_region_ind(id, middle);
    return id / (double) (g_hash_table_size(self->label_map) - 1);//MAX_LABEL_TYPES;
}

//this needs to be fixed 
static char *get_label_name_from_id(RendererRegionTopology *self, int class_id){
    class_map_t *map = (class_map_t *) g_hash_table_lookup(self->label_map, &class_id);
    if(map == NULL){
        return "Unknown";
    }
    else 
        return map->name;
}

static char *get_class_name_from_id(RendererRegionTopology *self, int class_id){
    class_map_t *map = (class_map_t *) g_hash_table_lookup(self->type_map, &class_id);
    if(map == NULL){
        return "Unknown";
    }
    else 
        return map->name;
}

static gint compare_region_prob (gconstpointer a, gconstpointer b){
    slam_graph_region_particle_t *p1 = (slam_graph_region_particle_t *)a;
    slam_graph_region_particle_t *p2 = (slam_graph_region_particle_t *)b;

    double prob1 = exp(p1->weight);
    double prob2 = exp(p2->weight);
    if(prob1 > prob2)
        return 0;
    return 1;
}

static gint compare_class_id (gconstpointer a, gconstpointer b){
    class_index_t *p1 = (class_index_t *)a;
    class_index_t *p2 = (class_index_t *)b;

    if(p1->id < p2->id)
        return 0;
    return 1;
}

static gint compare_region_id (gconstpointer a, gconstpointer b){
    slam_graph_region_t *p1 = (slam_graph_region_t *)a;
    slam_graph_region_t *p2 = (slam_graph_region_t *)b;

    if(p1->id < p2->id)
        return 0;
    return 1;
}


static gint compare_graph_id (gconstpointer a, gconstpointer b){
    slam_graph_region_particle_t *p1 = (slam_graph_region_particle_t *)a;
    slam_graph_region_particle_t *p2 = (slam_graph_region_particle_t *)b;

    if(p1->id < p2->id)
        return 0;
    return 1;
}

static void destroy_int(gpointer data){
    if(data != NULL){
        int *p = (int *)data;
        free(p);
    }
}

static void clear_class_maps(GHashTable *table){
    GHashTableIter iter;
    gpointer key, value;
    g_hash_table_iter_init (&iter, table);

    while (g_hash_table_iter_next (&iter, &key, &value)){
        class_map_t *map = (class_map_t *) g_hash_table_lookup(table, key);
        fprintf(stderr, "Clearing map : %d => %s\n", map->id, map->name);
        free(map);
    }

    g_hash_table_remove_all(table);
}

static void clear_node_classifications(GHashTable *table){
    GHashTableIter iter;
    gpointer key, value;
    g_hash_table_iter_init (&iter, table);

    while (g_hash_table_iter_next (&iter, &key, &value)){
        slam_graph_node_classification_t *classification = (slam_graph_node_classification_t *) g_hash_table_lookup(table, key);
        slam_graph_node_classification_t_destroy(classification);
    }

    g_hash_table_remove_all(table);
}


static void add_node_classifications(RendererRegionTopology *self, const slam_graph_region_particle_list_t *p){
    GHashTable *type_map = self->type_map;
    GHashTable *label_map = self->label_map;

    //the class id's should be consistant in all nodes 
    

    for(int i=0; i < p->type_info.count; i++){
        int c_id = p->type_info.id[i];
        class_map_t *map = (class_map_t *) g_hash_table_lookup(self->type_map, &c_id);
        if(map == NULL){
            fprintf(stderr, "Adding Class Type : %s - %d\n", p->type_info.names[i], p->type_info.id[i]);
            map = (class_map_t *) calloc(1, sizeof(class_map_t));
            map->id =  p->type_info.id[i];
            map->name =  strdup(p->type_info.names[i]);
            g_hash_table_insert(type_map, &map->id, map);
        }
    }

    for(int i=0; i < p->label_info.count; i++){
        int c_id = p->label_info.id[i];
        class_map_t *map = (class_map_t *) g_hash_table_lookup(self->label_map, &c_id);
        if(map == NULL){
            fprintf(stderr, "Adding Label Type : %s - %d\n", p->label_info.names[i], p->label_info.id[i]);
            map = (class_map_t *) calloc(1, sizeof(class_map_t));
            map->id =  p->label_info.id[i];
            map->name =  strdup(p->label_info.names[i]);
            g_hash_table_insert(label_map, &map->id, map);
        }
    }

    for(int i=0; i < p->appearance_info.count; i++){
        int c_id = p->appearance_info.id[i];
        class_map_t *map = (class_map_t *) g_hash_table_lookup(self->appearance_map, &c_id);
        if(map == NULL){
            fprintf(stderr, "Adding Label Type : %s - %d\n", p->appearance_info.names[i], p->appearance_info.id[i]);
            map = (class_map_t *) calloc(1, sizeof(class_map_t));
            map->id =  p->appearance_info.id[i];
            map->name =  strdup(p->appearance_info.names[i]);
            g_hash_table_insert(self->appearance_map, &map->id, map);
        }
    }
    
    clear_node_classifications(self->node_classifications);
    for(int i=0; i< p->no_nodes; i++){
        slam_graph_node_classification_t *classification = slam_graph_node_classification_t_copy(&p->node_classifications[i]);
        //fprintf(stderr, "Adding classification for node : %d\n", (int) classification->node_id);
        g_hash_table_insert(self->node_classifications, &(classification->node_id), classification);	
    }
}

static void destroy_region_particle(gpointer data){
    if(data != NULL){
        slam_graph_region_particle_t *p = (slam_graph_region_particle_t *)data;
        slam_graph_region_particle_t_destroy(p);
    }
}

static void draw_circle(double pos[3], double color_ratio, double resolution_radians, double radius){
    double theta = 0;
        
    int count = fmax(2,ceil(2 *M_PI / resolution_radians));
    double delta =  2* M_PI/count;
    glColor3fv(bot_color_util_jet(color_ratio));

    glBegin(GL_TRIANGLE_FAN);
    glVertex3d(pos[0], pos[1], pos[2]);
    glVertex3d(pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2]);
    for(int k=0; k< count; k++){
        theta += delta;
        glVertex3d( pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2]);
    }
    glEnd();
}

static void draw_pose(double pos[3], double theta_node, double color_ratio, double resolution_radians, double radius){
    double theta = 0;
        
    int count = fmax(2,ceil(2 *M_PI / resolution_radians));
    double delta =  2* M_PI/count;
    glColor3fv(bot_color_util_jet(color_ratio));

    glBegin(GL_TRIANGLE_FAN);
    glVertex3d(pos[0], pos[1], pos[2]);
    glVertex3d(pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2]);
    for(int k=0; k< count; k++){
        theta += delta;
        glVertex3d( pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2]);
    }
    glEnd();

    //draw line from center to point out 
    glLineWidth(LINE_WIDTH);
    glBegin(GL_LINES);
    glVertex3d(pos[0], pos[1], pos[2]);
    glVertex3d(pos[0] + radius*2*cos(theta_node), pos[1] + 2 * radius * sin(theta_node), pos[2]);
    glEnd();
}

static void draw_circle_black(double pos[3], double resolution_radians, double radius){
    double theta = 0;
        
    int count = fmax(2,ceil(2 *M_PI / resolution_radians));
    double delta =  2* M_PI/count;
    glColor3fv(bot_color_util_black);

    glBegin(GL_TRIANGLE_FAN);
    glVertex3d(pos[0], pos[1], pos[2]);
    glVertex3d(pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2]);
    for(int k=0; k< count; k++){
        theta += delta;
        glVertex3d( pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2]);
    }
    glEnd();
}

static void draw_object(double pos[3], double color_ratio, double length){
    glPushMatrix();
    glColor3fv(bot_color_util_jet(color_ratio));
    glTranslated(pos[0], pos[1], pos[2]);
    glScalef (length, length, length);
    bot_gl_draw_cube();
    glPopMatrix();
}

static void draw_triangle_pose(double pos[3], double theta_node, double color_ratio){
    double theta = 0;
    
    double width = 1.0;
    double length = 1.0; 

    BotTrans body_to_local;
    
    body_to_local.trans_vec[0] = pos[0];
    body_to_local.trans_vec[1] = pos[1];
    body_to_local.trans_vec[2] = pos[2];
    
    double rpy[3] = {0,0, theta_node}; 
    bot_roll_pitch_yaw_to_quat(rpy, body_to_local.rot_quat);

    double body_to_local_m[16], body_to_local_m_opengl[16];

    bot_trans_get_mat_4x4(&body_to_local, body_to_local_m);
    bot_matrix_transpose_4x4d(body_to_local_m, body_to_local_m_opengl);
    glPushMatrix();
    glMultMatrixd(body_to_local_m_opengl); // rotate and translate the vehicle

    glPushAttrib (GL_ENABLE_BIT | GL_DEPTH_BUFFER_BIT);
    glDisable (GL_DEPTH_TEST);
    glLineWidth(2);
    glColor4f (0, 0, 1.0, 1.0);
    glEnable(GL_POLYGON_OFFSET_FILL);
    glPolygonOffset (1, 1);

    glBegin(GL_TRIANGLES);
    /*glVertex3f( front_middle[0], front_middle[1], 0);
      glVertex3f( self->footprint[6], self->footprint[7], 0); 
      glVertex3f(  self->footprint[4], self->footprint[5], 0);*/
    
    glVertex3f( 0, width / 2.0, 0);
    glVertex3f( length, 0, 0); 
    glVertex3f( 0, -width / 2.0, 0);
    
    glEnd();

    glPopAttrib ();
    glPopMatrix();

    /*int count = fmax(2,ceil(2 *M_PI / resolution_radians));
    double delta =  2* M_PI/count;
    glColor3fv(bot_color_util_jet(color_ratio));

    glBegin(GL_TRIANGLE_FAN);
    glVertex3d(pos[0], pos[1], pos[2]);
    glVertex3d(pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2]);
    for(int k=0; k< count; k++){
        theta += delta;
        glVertex3d( pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2]);
    }
    glEnd();

    //draw line from center to point out 
    glLineWidth(LINE_WIDTH);
    glBegin(GL_LINES);
    glVertex3d(pos[0], pos[1], pos[2]);
    glVertex3d(pos[0] + radius*2*cos(theta_node), pos[1] + 2 * radius * sin(theta_node), pos[2]);
    glEnd();    */
}

static void
draw_bar_chart (RendererRegionTopology *self, double pos[3], slam_probability_distribution_t *classification, 
                int draw_laser, double width, double height, int max, int draw_text) {


    // Draw the bar charts such that they have zero rotation wrt the up view axis
    double eye[3];
    double look[3];
    double up[3];
    self->viewer->view_handler->get_eye_look (self->viewer->view_handler, eye, look, up);

    double oxy[3] = {1, 0, 0};
    double theta = bot_vector_angle_2d (oxy, up);

    double offset[3] = {0.5, 0.5, 0};

    glPushMatrix();
    glTranslatef (pos[0] + offset[0], pos[1] + offset[1], pos[2] + offset[2]);
    glRotatef (theta*180/M_PI, 0, 0, 1);

    double x = 0;
    double y = 0;
    double z = 0;
    double spacing = 0.05;


    double max_prob = 0; 
    int max_ind = -1;
    double min_prob = 1.0; 
    for(int c =0; c < classification->count; c++){
        double p = classification->classes[c].probability;
        if(max_prob < p){
            max_prob = p;
            max_ind = c; 
        }
        if(min_prob > p){
            min_prob = p; 
        }
    }

    if(max > 0){
        if(max_prob == min_prob){
            glPopMatrix();
            return;
        }
    }

    double scale_p = 2.0;
    
    //we need to rotate and also add a colored background to make it more visible 
    double total_width = (width + spacing) * classification->count;
    glColor3fv(bot_color_util_white);
    glBegin(GL_TRIANGLE_FAN);
    glVertex3d (x - spacing, y - spacing, z - 0.01);
    glVertex3d (x + scale_p + spacing, y - spacing, z - 0.01);
    glVertex3d (x + scale_p + spacing, y + total_width + spacing, z - 0.01);
    glEnd();
    glBegin(GL_TRIANGLE_FAN);
    glVertex3d (x + scale_p + spacing, y + total_width + spacing, z - 0.01);
    glVertex3d (x - spacing , y + total_width + spacing, z - 0.01);
    glVertex3d (x - spacing, y - spacing, z - 0.01);
    glEnd();

    glLineWidth(3.0);
    glColor3fv(bot_color_util_black);
    glBegin (GL_LINE_LOOP);
    glVertex3d (x - spacing, y - spacing, z - 0.005);
    glVertex3d (x + scale_p + spacing, y - spacing, z - 0.005);
    glVertex3d (x + scale_p + spacing, y + total_width + spacing, z - 0.005);
    glVertex3d (x - spacing , y + total_width + spacing, z - 0.005);
    glVertex3d (x - spacing, y - spacing, z - 0.005);
    glEnd();

    double max_p = 0;

    int rescale = 1;

    int draw_line_to_node = 1;

    if(rescale){
        for (int c=0; c < classification->count; c++) {
            double p = classification->classes[c].probability * height;
            
            if(max_p < p){
                max_p = p;
            }
        }
    }

    for (int c=0; c < classification->count; c++) {
        double p = scale_p * classification->classes[c].probability * height;
        if(rescale && max_p > 0){
            p /= max_p;
        }
        // y = y + c * (width + spacing);

        //glColor3f (1.0, 0.0, 0.0);
        glColor3fv(bot_color_util_jet(get_color_ratio_from_label_id(self, classification->classes[c].type)));//
        
        glBegin(GL_TRIANGLE_FAN);
        glVertex3d (x , y , z);
        glVertex3d (x  + p, y , z);
        glVertex3d (x + p, y + width, z);
        glEnd();
        glBegin(GL_TRIANGLE_FAN);
        glVertex3d (x + p , y + width, z);
        glVertex3d (x , y + width , z);
        glVertex3d (x , y , z);
        glEnd();

        y = y + width + spacing;
    }

    glPopMatrix();
    if(draw_line_to_node){
        x = pos[0] + offset[0];
        y = pos[1] + offset[1];
        z = pos[2] + offset[2];
        //double mid_point[3] = { x +(scale_p + spacing) /2.0, y + total_width / 2.0, z - 0.1}; 
        
        glLineWidth(5.0);
        glColor3fv(bot_color_util_black);
        glBegin (GL_LINES);
        glVertex3d (pos[0], pos[1], pos[2]);
        //glVertex3d (mid_point[0], mid_point[1], mid_point[2]);
        glVertex3d (x, y, z);
        //x - spacing, 
        glEnd();
    }
    //glPopMatrix();
}

static void
draw_bar_chart_semantic_class (RendererRegionTopology *self, double pos[3], slam_probability_distribution_t *classification, 
                int draw_laser, double width, double height, int max, int draw_text) {
    
    double offset[3] = {0.5, 0.5, 0};

    double x = pos[0] + offset[0];
    double y = pos[1] + offset[1];
    double z = pos[2] + offset[2];
    double spacing = 0.05;


    double max_prob = 0; 
    int max_ind = -1;
    double min_prob = 1.0; 
    for(int c =0; c < classification->count; c++){
        double p = classification->classes[c].probability;
        if(max_prob < p){
            max_prob = p;
            max_ind = c; 
        }
        if(min_prob > p){
            min_prob = p; 
        }
    }

    if(max > 0){
        if(max_prob == min_prob){
            return;
        }
    }

    double scale_p = 2.0;
    
    //we need to rotate and also add a colored background to make it more visible 
    double total_width = (width + spacing) * classification->count;
    glColor3fv(bot_color_util_white);
    glBegin(GL_TRIANGLE_FAN);
    glVertex3d (x - spacing, y - spacing, z - 0.01);
    glVertex3d (x + scale_p + spacing, y - spacing, z - 0.01);
    glVertex3d (x + scale_p + spacing, y + total_width + spacing, z - 0.01);
    glEnd();
    glBegin(GL_TRIANGLE_FAN);
    glVertex3d (x + scale_p + spacing, y + total_width + spacing, z - 0.01);
    glVertex3d (x - spacing , y + total_width + spacing, z - 0.01);
    glVertex3d (x - spacing, y - spacing, z - 0.01);
    glEnd();
    /*glBegin (GL_QUADS);
    glVertex3d (x - spacing, y - spacing, z - 0.01);
    glVertex3d (x + scale_p + spacing, y - spacing, z - 0.01);
    glVertex3d (x + scale_p + spacing, y + total_width + spacing, z - 0.01);
    glVertex3d (x - spacing , y + total_width + spacing, z - 0.01);
    glEnd();*/

    glLineWidth(3.0);
    glColor3fv(bot_color_util_black);
    glBegin (GL_LINE_LOOP);
    glVertex3d (x - spacing, y - spacing, z - 0.005);
    glVertex3d (x + scale_p + spacing, y - spacing, z - 0.005);
    glVertex3d (x + scale_p + spacing, y + total_width + spacing, z - 0.005);
    glVertex3d (x - spacing , y + total_width + spacing, z - 0.005);
    glVertex3d (x - spacing, y - spacing, z - 0.005);
    glEnd();

    double max_p = 0;

    int rescale = 1;

    int draw_line_to_node = 1;

    if(rescale){
        for (int c=0; c < classification->count; c++) {
            double p = classification->classes[c].probability * height;
            
            if(max_p < p){
                max_p = p;
            }
        }
    }

    for (int c=0; c < classification->count; c++) {
        double p = scale_p * classification->classes[c].probability * height;
        if(rescale && max_p > 0){
            p /= max_p;
        }
        // y = y + c * (width + spacing);

        //glColor3f (1.0, 0.0, 0.0);
        glColor3fv(bot_color_util_jet(get_color_ratio_from_class_id(self, classification->classes[c].type)));//
        
        glBegin(GL_TRIANGLE_FAN);
        glVertex3d (x , y , z);
        glVertex3d (x  + p, y , z);
        glVertex3d (x + p, y + width, z);
        glEnd();
        glBegin(GL_TRIANGLE_FAN);
        glVertex3d (x + p , y + width, z);
        glVertex3d (x , y + width , z);
        glVertex3d (x , y , z);
        glEnd();

        /*glBegin (GL_QUADS);
        glVertex3d (x, y, z);
        glVertex3d (x + p, y, z);
        glVertex3d (x + p, y + width, z);
        glVertex3d (x, y + width, z);
        glEnd();*/

        y = y + width + spacing;
    }

    if(draw_line_to_node){
        x = pos[0] + offset[0];
        y = pos[1] + offset[1];
        z = pos[2] + offset[2];
        double mid_point[3] = { x +(scale_p + spacing) /2.0, y + total_width / 2.0, z - 0.1}; 
        
        glLineWidth(5.0);
        glColor3fv(bot_color_util_black);
        glBegin (GL_LINES);
        glVertex3d (pos[0], pos[1], pos[2]);
        glVertex3d (mid_point[0], mid_point[1], mid_point[2]);
        //x - spacing, 
        glEnd();
    }
}


static int draw_label_chart(RendererRegionTopology *self, double pos[3], slam_probability_distribution_t *classification, 
                             int draw_laser, double resolution_radians, double radius, int max, int draw_text, double line_thick){
    double theta = 0;
    int drawn = 0;
    double max_prob = 0; 
    int max_ind = -1;
    double min_prob = 1.0; 
    for(int c =0; c < classification->count; c++){
        double p = classification->classes[c].probability;
        if(max_prob < p){
            max_prob = p;
            max_ind = c; 
        }
        if(min_prob > p){
            min_prob = p; 
        }
    }

    if(max > 0){
        if(max_prob == min_prob){
            return 0;
        }
    }
    
    if(max==1){
        if(max_prob > min_prob && max_ind >=0){
            int count = fmax(2,ceil(2 *M_PI / resolution_radians));
            double delta = 2 * M_PI /count;
            if(self->params.use_c_util){
                glColor3fv(bot_color_util_jet(get_color_ratio_from_label_id(self, classification->classes[max_ind].type)));//
            }
            else{
                glColor3fv(renderer_color_util_jet(classification->classes[max_ind].type));
            }

            //glColor3fv(bot_color_util_jet(classification->laser_classification[c].name/MAX_CLASS_ID));
            glBegin(GL_TRIANGLE_FAN);
            glVertex3d(pos[0], pos[1], pos[2]);
            glVertex3d(pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2]);
            for(int k=0; k< count; k++){
                theta += delta;
                glVertex3d( pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2]);
            }
            glEnd();
            if(draw_text){
                double textpos[3] = {pos[0] + radius * 1.5, pos[1] + radius * 1.5 , pos[2] + 0.1};
                glColor3f(1.0,1.0,1.0);
                bot_gl_draw_text(textpos, GLUT_BITMAP_HELVETICA_12, get_label_name_from_id(self, classification->classes[max_ind].type), //label,
                                 BOT_GL_DRAW_TEXT_DROP_SHADOW);
            }
            drawn = 1;
        }
    }
    else{
        for(int c =0; c < classification->count; c++){
            double p = classification->classes[c].probability * 2 * M_PI;
            
            //scale the prob 
            //p /= total_valid_prob;
            /*if(p < resolution_radians){
              continue;
              }*/
            int count = fmax(2,ceil(p / resolution_radians));
            double delta = p/count;
            if(self->params.use_c_util){
                glColor3fv(bot_color_util_jet(get_color_ratio_from_label_id(self, classification->classes[c].type)));//
            }
            else{
                glColor3fv(renderer_color_util_jet(classification->classes[c].type));
            }
            
            //fprintf(stderr, "\tClass : %s [%d] -> %f\n", get_label_name_from_id(self, classification->classes[c].type) , classification->classes[c].type, p);
            //glColor3fv(bot_color_util_jet(classification->laser_classification[c].name/MAX_CLASS_ID));

            int mid_index = count / 2.0;

            double textpos[3] = {0,0,0};

            glBegin(GL_TRIANGLE_FAN);
            glVertex3d(pos[0], pos[1], pos[2]);
            glVertex3d(pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2]);
            for(int k=0; k< count; k++){
                theta += delta;
                glVertex3d( pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2]);
                if(k == mid_index){
                    textpos[0] = pos[0] + radius*cos(theta);
                    textpos[1] = pos[1] + radius*sin(theta);
                    textpos[2] = pos[2] + 0.1;
                }
            }
            glEnd();
            if(draw_text && classification->classes[c].probability > 0.1){
                glColor3f(1.0,1.0,1.0);
                bot_gl_draw_text(textpos, GLUT_BITMAP_HELVETICA_12, get_label_name_from_id(self, classification->classes[c].type), //label,
                                 BOT_GL_DRAW_TEXT_DROP_SHADOW);
            }

            glLineWidth(LINE_WIDTH);
            glColor3f(1,1,1);
            glBegin(GL_LINES);
            glVertex3d(pos[0], pos[1], pos[2]+0.01);
            glVertex3d(pos[0] + radius*cos(theta -p), pos[1] + radius*sin(theta - p), pos[2] + 0.01);
            glEnd();
            
            glBegin(GL_LINES);
            glVertex3d(pos[0], pos[1], pos[2]+0.01);
            glVertex3d( pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2] + 0.01);
            glEnd();
        }

        drawn = 1;
    }

    if(drawn){
        theta = 0;
        glColor3fv(bot_color_util_black);
        glEnable(GL_LINE_SMOOTH);
        glDepthMask(GL_FALSE);
        glLineWidth(line_thick);
        int count  = 2 * M_PI /  resolution_radians;
        glBegin(GL_LINE_LOOP);
        for(int k=0; k< count; k++){
            theta += resolution_radians;
            glVertex3d( pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2] + 0.02);
        }
        glEnd();
        glDepthMask(GL_TRUE);
    }
    return 1;
}

static void draw_semantic_chart(RendererRegionTopology *self, double pos[3], slam_probability_distribution_t *classification, int draw_laser, double resolution_radians, double radius, int max, int draw_text, double line_thick){
    double theta = 0;

    int drawn = 0;

    /*for(int c =0; c < classification->count; c++){
        double p = classification->classes[c].probability;
        fprintf(stderr, "\tClass : %s -> Prob : %f\n", get_class_name_from_id(self, classification->classes[c].type), p);
        }*/

    if(max){
        double max_prob = 0; 
        int max_ind = -1;
        for(int c =0; c < classification->count; c++){
            double p = classification->classes[c].probability;
            if(max_prob < p){
                max_prob = p;
                max_ind = c; 
            }
        }
        char max_classes[2000];
        sprintf(max_classes,"");
        for(int c =0; c < classification->count; c++){
            double p = classification->classes[c].probability;
            if(p == max_prob){
                char max_class_name[100];
                sprintf(max_class_name, "%s\n",get_class_name_from_id(self, classification->classes[c].type));
                strcat(max_classes, max_class_name); 
            }
        }

        if(max_ind >=0){
            int count = fmax(2,ceil(2 *M_PI / resolution_radians));
            double delta = 2 * M_PI /count;
            if(self->params.use_c_util){
                glColor3fv(bot_color_util_jet(get_color_ratio_from_class_id(self, classification->classes[max_ind].type)));//
            }
            else{
                glColor3fv(renderer_color_util_jet(classification->classes[max_ind].type));
            }
            //glColor3fv(bot_color_util_jet(classification->laser_classification[c].name/MAX_CLASS_ID));
            glBegin(GL_TRIANGLE_FAN);
            glVertex3d(pos[0], pos[1], pos[2]);
            glVertex3d(pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2]);
            for(int k=0; k< count; k++){
                theta += delta;
                glVertex3d( pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2]);
            }
            glEnd();
            if(draw_text){
                double textpos[3] = {pos[0] + radius * 1.5, pos[1] + radius * 1.5 , pos[2] + 0.1};
                glColor3f(1.0,1.0,1.0);
                if(draw_text == 2){
                    bot_gl_draw_text(textpos, GLUT_BITMAP_HELVETICA_12, max_classes,
                                     BOT_GL_DRAW_TEXT_DROP_SHADOW);
                }
                else{
                    bot_gl_draw_text(textpos, GLUT_BITMAP_HELVETICA_12, get_class_name_from_id(self, classification->classes[max_ind].type),
                                     BOT_GL_DRAW_TEXT_DROP_SHADOW);
                }
            }
            drawn = 1;
        }
    }
    else{
        for(int c =0; c < classification->count; c++){
            double p = classification->classes[c].probability * 2 * M_PI;
            
            //scale the prob 
            //p /= total_valid_prob;
            /*if(p < resolution_radians){
              continue;
              }*/
            int count = fmax(2,ceil(p / resolution_radians));
            double delta = p/count;

            if(self->params.use_c_util){
                glColor3fv(bot_color_util_jet(get_color_ratio_from_class_id(self, classification->classes[c].type)));//
            }
            else{
                glColor3fv(renderer_color_util_jet(classification->classes[c].type));
            }
            int mid_index = count / 2.0;

            double textpos[3] = {0,0,0};

            glBegin(GL_TRIANGLE_FAN);
            glVertex3d(pos[0], pos[1], pos[2]);
            glVertex3d(pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2]);
            for(int k=0; k< count; k++){
                theta += delta;
                glVertex3d( pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2]);
                if(k == mid_index){
                    textpos[0] = pos[0] + radius*cos(theta);
                    textpos[1] = pos[1] + radius*sin(theta);
                    textpos[2] = pos[2] + 0.1;
                }
            }
            glEnd();

            if(draw_text && classification->classes[c].probability > 0.1){
                glColor3f(1.0,1.0,1.0);
                bot_gl_draw_text(textpos, GLUT_BITMAP_HELVETICA_12, get_class_name_from_id(self, classification->classes[c].type), //label,
                                 BOT_GL_DRAW_TEXT_DROP_SHADOW);
            }

            glLineWidth(LINE_WIDTH);
            glColor3f(1,1,1);
            glBegin(GL_LINES);
            glVertex3d(pos[0], pos[1], pos[2]+0.01);
            glVertex3d(pos[0] + radius*cos(theta -p), pos[1] + radius*sin(theta - p), pos[2] + 0.01);
            glEnd();
            
            glBegin(GL_LINES);
            glVertex3d(pos[0], pos[1], pos[2]+0.01);
            glVertex3d( pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2] + 0.01);
            glEnd();
        }
        drawn = 1;
    }

    if(drawn){
        theta = 0;
        glColor3fv(bot_color_util_black);
        glEnable(GL_LINE_SMOOTH);
        glDepthMask(GL_FALSE);
        glLineWidth(line_thick);
        int count  = 2 * M_PI /  resolution_radians;
        glBegin(GL_LINE_LOOP);
        for(int k=0; k< count; k++){
            theta += resolution_radians;
            glVertex3d( pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2] + 0.02);
        }
        glEnd();
        glDepthMask(GL_TRUE);
    }    
}

static void draw_pie_chart(RendererRegionTopology *self, double pos[3], slam_graph_node_classification_t *classification, int draw_laser, double resolution_radians, double radius, double line_thick){
    int drawn = 0;
    if(draw_laser){
        double theta = 0;

        for(int c =0; c < classification->no_laser_classes; c++){
            double p = classification->laser_classification[c].probability * 2 * M_PI;

            //scale the prob 
            //p /= total_valid_prob;
            /*if(p < resolution_radians){
              continue;
              }*/
            int count = fmax(2,ceil(p / resolution_radians));
            double delta = p/count;
            if(self->params.use_c_util){
                glColor3fv(bot_color_util_jet(get_color_ratio_from_appearance_id(self, classification->laser_classification[c].type)));//
            }
            else{
                glColor3fv(renderer_color_util_jet(classification->laser_classification[c].type));//
            }
            //glColor3fv(bot_color_util_jet(classification->laser_classification[c].name/MAX_CLASS_ID));
            glBegin(GL_TRIANGLE_FAN);
            glVertex3d(pos[0], pos[1], pos[2]);
            glVertex3d(pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2]);
            for(int k=0; k< count; k++){
                theta += delta;
                glVertex3d( pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2]);
            }
            glEnd();
            
            glLineWidth(LINE_WIDTH);
            glColor3f(1,1,1);
            glBegin(GL_LINES);
            glVertex3d(pos[0], pos[1], pos[2]+0.01);
            glVertex3d(pos[0] + radius*cos(theta -p), pos[1] + radius*sin(theta - p), pos[2] + 0.01);
            glEnd();
            
            glBegin(GL_LINES);
            glVertex3d(pos[0], pos[1], pos[2]+0.01);
            glVertex3d( pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2] + 0.01);
            glEnd();
        }
        drawn = 1;
    }
    else{
        double theta = 0;
        for(int c =0; c < classification->no_image_classes; c++){
            if(self->params.use_c_util){
                glColor3fv(bot_color_util_jet(get_color_ratio_from_appearance_id(self, classification->image_classification[c].type)));
            }
            else{
                glColor3fv(renderer_color_util_jet(classification->laser_classification[c].type));//
            }
            //glColor3fv(bot_color_util_jet(classification->image_classification[c].name/MAX_CLASS_ID));
            glBegin(GL_TRIANGLE_FAN);
            double p = classification->image_classification[c].probability * 2 * M_PI;
            int count = fmax(2,ceil(p / resolution_radians));
            double delta = p/count;
            glVertex3d(pos[0], pos[1], pos[2]);
            glVertex3d(pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2]);
            for(int k=0; k< count; k++){
                theta += delta;
                glVertex3d( pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2]);
            }            
            glEnd();

            glLineWidth(LINE_WIDTH);
            glColor3f(1,1,1);
            glBegin(GL_LINES);
            glVertex3d(pos[0], pos[1], pos[2]+0.01);
            glVertex3d(pos[0] + radius*cos(theta -p), pos[1] + radius*sin(theta - p), pos[2] + 0.01);
            glEnd();
            
            glBegin(GL_LINES);
            glVertex3d(pos[0], pos[1], pos[2]+0.01);
            glVertex3d( pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2] + 0.01);
            glEnd();
        }
        drawn = 1;
    }
    if(drawn){
        double theta = 0;
        glColor3fv(bot_color_util_black);
        glEnable(GL_LINE_SMOOTH);
        glDepthMask(GL_FALSE);
        glLineWidth(line_thick);
        int count  = 2 * M_PI /  resolution_radians;
        glBegin(GL_LINE_LOOP);
        for(int k=0; k< count; k++){
            theta += resolution_radians;
            glVertex3d( pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2] + 0.02);
        }
        glEnd();
        glDepthMask(GL_TRUE);
    }    
}





static void draw_node_class_pie_chart(RendererRegionTopology *self, double pos[3], slam_probability_distribution_t *classification, double resolution_radians, double radius, int max){
    double theta = 0;
    
    double max_prob = 0; 
    int max_ind = -1;
    double min_prob = 1.0; 
    for(int c =0; c < classification->count; c++){
        double p = classification->classes[c].probability;
        if(max_prob < p){
            max_prob = p;
            max_ind = c; 
        }
        if(min_prob > p){
            min_prob = p; 
        }
    }

    if(max > 0){
        if(max_prob == min_prob){
            return;
        }
    }
    
    if(max==1){
        if(max_prob > min_prob && max_ind >=0){
            int count = fmax(2,ceil(2 *M_PI / resolution_radians));
            double delta = 2 * M_PI /count;
            if(self->params.use_c_util){
                glColor3fv(bot_color_util_jet(get_color_ratio_from_appearance_id(self, classification->classes[max_ind].type)));//
            }
            else{
                glColor3fv(renderer_color_util_jet(classification->classes[max_ind].type));//
            }

            //glColor3fv(bot_color_util_jet(classification->laser_classification[c].name/MAX_CLASS_ID));
            glBegin(GL_TRIANGLE_FAN);
            glVertex3d(pos[0], pos[1], pos[2]);
            glVertex3d(pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2]);
            for(int k=0; k< count; k++){
                theta += delta;
                glVertex3d( pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2]);
            }
            glEnd();
        }
    }
    else{
        for(int c =0; c < classification->count; c++){
            double p = classification->classes[c].probability * 2 * M_PI;
            
            //scale the prob 
            //p /= total_valid_prob;
            /*if(p < resolution_radians){
              continue;
              }*/
            int count = fmax(2,ceil(p / resolution_radians));
            double delta = p/count;
            
            if(self->params.use_c_util){
                glColor3fv(bot_color_util_jet(get_color_ratio_from_appearance_id(self, classification->classes[c].type)));
            }
            else{
                glColor3fv(renderer_color_util_jet(classification->classes[c].type));//
            }

            
            int mid_index = count / 2.0;

            double textpos[3] = {0,0,0};

            glBegin(GL_TRIANGLE_FAN);
            glVertex3d(pos[0], pos[1], pos[2]);
            glVertex3d(pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2]);
            for(int k=0; k< count; k++){
                theta += delta;
                glVertex3d( pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2]);
                if(k == mid_index){
                    textpos[0] = pos[0] + radius*cos(theta);
                    textpos[1] = pos[1] + radius*sin(theta);
                    textpos[2] = pos[2] + 0.1;
                }
            }
            glEnd();
            
            glLineWidth(LINE_WIDTH);
            glColor3f(1,1,1);
            glBegin(GL_LINES);
            glVertex3d(pos[0], pos[1], pos[2]+0.01);
            glVertex3d(pos[0] + radius*cos(theta -p), pos[1] + radius*sin(theta - p), pos[2] + 0.01);
            glEnd();
            
            glBegin(GL_LINES);
            glVertex3d(pos[0], pos[1], pos[2]+0.01);
            glVertex3d( pos[0] + radius*cos(theta), pos[1] + radius*sin(theta), pos[2] + 0.01);
            glEnd();
        }
    }
}




static void draw_region_graph_particle(RendererRegionTopology *self, slam_graph_region_particle_t *p, slam_complex_language_collection_t *complex_lang, 
                                int k, double min_prob, 
                                int active_particle, double scale, int num_particles){

    params_t params = self->params;

    slam_graph_region_particle_list_t *current_list = bot_ptr_circular_index(self->particle_history, 0);

    char label[1042];
    int j = p->id;
    double prob = p->weight; 
    if(!params.log_scale)
        prob = exp(prob);
    double alpha = 1.0;
    double weight_color = (prob - min_prob) * scale;
    
    double weight = 0;

    if(self->params.draw_all_graphs && self->params.particle_ordering_mode == 0){
        weight = (prob - min_prob) * scale * params.distance_scale;
    }
    else if(self->params.draw_all_graphs && self->params.particle_ordering_mode == 1){
        weight = k / (double) num_particles * scale * params.distance_scale;
    }

    if(self->params.draw_all_graphs && params.equal_dist){
        //make the gaps equal
        weight = (num_particles - k) * params.distance_scale;
    }        

    GHashTable *slam_nodes = g_hash_table_new(g_int_hash, g_int_equal);

    GList * sorted_region_list = NULL;
    
    //we need to refer to the nodes for drawing edges 
    for(int i=0; i < p->no_regions; i++){
        slam_graph_region_t *region = &p->region_list[i];
        sorted_region_list = g_list_insert_sorted (sorted_region_list,region, compare_region_id);

        for(int j=0; j< region->count; j++){
            g_hash_table_insert(slam_nodes, &(region->nodes[j].id), &region->nodes[j]);
        }
    }

    //GHashTable *region_mapped_ind = g_hash_table_new(g_int_hash, g_int_equal);//
    GHashTable *region_mapped_ind = g_hash_table_new_full(g_int_hash, g_int_equal, NULL, destroy_int);

    //int *region_maped_ind = (int *) calloc(p->no_regions, sizeof(int));

    for(guint i=0; i <  g_list_length(sorted_region_list); i++){
        slam_graph_region_t *p = g_list_nth_data (sorted_region_list, i); 
        int *ind = calloc(1, sizeof(int)); 
        *ind = i;
        g_hash_table_insert(region_mapped_ind, &(p->id), ind);
        //region_maped_ind[p->id] = i;
    }

    g_list_free(sorted_region_list);

    double gap = params.distance_scale; 
    double inc = 0.00001;
    if(params.draw_height){
        inc = gap / p->no_nodes;
    }

    glPushAttrib (GL_DEPTH_BUFFER_BIT | GL_POINT_BIT | GL_CURRENT_BIT);
    glEnable (GL_DEPTH_TEST);
    glEnable(GL_BLEND);
        
    glColor3fv(bot_color_util_jet(weight_color));
        
    glEnable( GL_POINT_SMOOTH );
    glEnable(GL_LINE_SMOOTH);
    //glEnable( GL_POLYGON_SMOOTH );
    glEnable(GL_MULTISAMPLE);
    
    //glHint( GL_POLYGON_SMOOTH_HINT, GL_NICEST ) ;
    glEnable( GL_BLEND );
    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    glPointSize( 8.0 );
                   
    if(params.draw_cov){
        glEnable(GL_LINE_SMOOTH);
        glDepthMask(GL_FALSE);
        for(int i=0; i < p->no_regions; i++){
            slam_graph_region_t *region = &p->region_list[i];
            
            for(int j=0; j< region->count; j++){
                slam_graph_node_t node = region->nodes[j]; 
                if(node.is_supernode){
                    double node_cov_xy[] = { node.cov[0], node.cov[1], node.cov[3], node.cov[4] };
                
                    gsl_matrix_view cov_xy = 
                        gsl_matrix_view_array (node_cov_xy, 2, 2);
                
                    gsl_vector *eval = gsl_vector_alloc (2);
                    gsl_matrix *evec = gsl_matrix_alloc (2, 2);
                
                    gsl_eigen_symmv_workspace *w =
                        gsl_eigen_symmv_alloc (2);
                
                    gsl_eigen_symmv (&cov_xy.matrix, eval, evec, w);
                
                    gsl_eigen_symmv_free (w);
                
                    //the eigen vectors are in evec
                    double m_opengl[16] = {evec->data[0], evec->data[1], 0, 0,
                                           evec->data[2], evec->data[3], 0, 0,
                                           0, 0, 1, 0,
                                           0, 0, 0, 1};
                
                
                    //fprintf(stderr, "Node : %d - Eigen values (sqrt) : %f,%f\n", (int) node.id, sqrt(eval->data[0]), sqrt(eval->data[1]));
                    
                    glPushMatrix();
                
                    
                    glTranslatef (node.xy[0], node.xy[1], weight);
                    glMultMatrixd (m_opengl); 
                    glScalef (sqrt(eval->data[0]), sqrt(eval->data[1]), 1);
                    gsl_vector_free (eval);
                    gsl_matrix_free (evec);
                
                    glLineWidth(2.0);
                    bot_gl_draw_circle(1.0);
                    glPopMatrix();
                }                       
            }
        }
        glDepthMask(GL_TRUE);
    }                 
        
    int div = p->no_regions;
    //make this even 
    if(div %2 == 1){
        div++;
    }
    int middle = (div / 2.0) -1;
                    
    if(params.bounding_boxes){
        double region_weight = 0;
        for(int i=0; i < p->no_regions; i++){
            slam_graph_region_t *region = &p->region_list[i];
            if(params.color_regions){
                int r_id = *(int *) g_hash_table_lookup(region_mapped_ind, &(region->id));
                if(params.remap_regions){
                    //int remapped_ind = remap_region_ind(region->id, middle);
                    int remapped_ind = remap_region_ind(r_id, middle);
                    region_weight = remapped_ind/((double) p->no_regions-1);
                }
                else{
                    region_weight =  region->id / ((double) p->no_regions-1);
                }  
                
                glColor3fv(bot_color_util_jet(region_weight));
            }

            for(int j=0; j< region->count; j++){
                slam_graph_node_t node = region->nodes[j]; 
                
                if(params.draw_regions && !node.is_supernode)
                    continue;

                glBegin(GL_LINE_LOOP);
                for(int k=0; k< node.no_points; k++){
                    glVertex3d( node.x_coords[k], node.y_coords[k] , weight);
                }
                glEnd();
            }      
        }                 
    }
        
    
    if(params.draw_complex_language){
        for(int i=0; i < current_list->no_failed; i++){
            slam_graph_node_t *node = (slam_graph_node_t *) g_hash_table_lookup(slam_nodes, &current_list->annotations_failed[i].node_id);
            double textpos[3] = {node->xy[0] + 0.2, node->xy[1] + 0.2 , 0.1};
            glColor3f(1.0,1.0,1.0);
            bot_gl_draw_text(textpos, GLUT_BITMAP_HELVETICA_12, 
                             current_list->annotations_failed[i].label, BOT_GL_DRAW_TEXT_DROP_SHADOW);
        }
        for(int i=0; i < current_list->no_outstanding; i++){
            slam_graph_node_t *node = (slam_graph_node_t *) g_hash_table_lookup(slam_nodes, &current_list->annotations_outstanding[i].node_id);
            double textpos[3] = {node->xy[0] + 0.2, node->xy[1] + 0.2 , 0.1};
            glColor3f(1.0,1.0,1.0);
            bot_gl_draw_text(textpos, GLUT_BITMAP_HELVETICA_12, 
                             current_list->annotations_outstanding[i].label, BOT_GL_DRAW_TEXT_DROP_SHADOW);
        }
        for(int i=0; i < current_list->no_grounded; i++){
            slam_graph_node_t *node = (slam_graph_node_t *) g_hash_table_lookup(slam_nodes, &current_list->annotations_grounded[i].node_id);
            double textpos[3] = {node->xy[0] + 0.2, node->xy[1] + 0.2 , 0.1};
            glColor3f(1.0,1.0,1.0);
            bot_gl_draw_text(textpos, GLUT_BITMAP_HELVETICA_12, 
                             current_list->annotations_grounded[i].label, BOT_GL_DRAW_TEXT_DROP_SHADOW);
        }
    }
    

    if(params.draw_nodes && !(params.draw_semantic_class || params.draw_semantic_class_max) 
       && !(params.draw_semantic_label || params.draw_semantic_label_max)){  
        double region_weight = 0;
        
        for(int i=0; i < p->no_regions; i++){
            slam_graph_region_t *region = &p->region_list[i];
            int r_id = *(int *) g_hash_table_lookup(region_mapped_ind, &(region->id));
            if(params.color_regions){
                if(params.remap_regions){
                    //int remapped_ind = remap_region_ind(region->id, middle);
                    int remapped_ind = remap_region_ind(r_id, middle);
                    region_weight = remapped_ind/((double) p->no_regions-1);
                }
                else{
                    region_weight =  region->id / ((double) p->no_regions-1);
                }                
            }
                
            for(int j=0; j< region->count; j++){
                slam_graph_node_t node = region->nodes[j];  
                        
                if(params.draw_regions && !params.draw_region_mean && node.is_supernode){
                    continue;
                }
                double c_weight = weight;
                //if(params.draw_height){
                c_weight += inc * node.id;
                    //}
                        
                //skip drawing the node if the pie chart is drawn
                /*if((params.draw_pie_chart || self->params.draw_max_label) && node.is_supernode == 1){
                    slam_label_distribution_t ld = node.labeldist;
                    double prob = ld.observation_frequency[0];
                    int different = 0;
                    for(int k=0; k<ld.num_labels; k++){
                        if(ld.observation_frequency[k] != prob){
                            different = 1;
                            break;
                        }
                    }
                    if(different)
                        continue;
                        }*/
                        
                slam_graph_node_classification_t *classification = (slam_graph_node_classification_t *) g_hash_table_lookup(self->node_classifications,&(node.node_id));                
                int max_class = -1;
                double max_prob = 0;
                        
                if(params.draw_laser_class){
                    if(classification == NULL){
                        fprintf(stderr, "Classification is not there for node : %d\n", (int) node.node_id);
                        continue;
                    }
                    //have to draw the pie charts for these 
                    double pos[3] = {node.xy[0], node.xy[1], c_weight + 0.05};
                    draw_pie_chart(self, pos, classification, 1, bot_to_radians(10.0), params.node_radius, params.line_thickness);
                }
                else if(params.draw_image_class){
                    if(classification == NULL){
                        fprintf(stderr, "Classification is not there for node : %d\n", (int) node.node_id);
                        continue;
                    }
                    //have to draw the pie charts for these 
                    double pos[3] = {node.xy[0], node.xy[1], c_weight + 0.05};
                    draw_pie_chart(self, pos, classification, 0, bot_to_radians(10.0), params.node_radius, params.line_thickness);
                }
                else if(params.draw_laser_class_max){
                    if(classification == NULL){
                        fprintf(stderr, "Classification is not there for node : %d\n", (int) node.node_id);
                        continue;
                    }
                    for(int c =0; c < classification->no_laser_classes; c++){
                        if(max_prob < classification->laser_classification[c].probability){
                            max_prob = classification->laser_classification[c].probability;
                            max_class = classification->laser_classification[c].type;
                        }
                    }
                    double node_pos[3] = {node.xy[0], node.xy[1], c_weight + 0.05};
                    draw_circle(node_pos, get_color_ratio_from_appearance_id(self, max_class), bot_to_radians(10.0), params.node_radius);  
                }
                else if(params.draw_image_class_max){
                    if(classification == NULL){
                        fprintf(stderr, "Classification is not there for node : %d\n", (int) node.node_id);
                        continue;
                    }
                    for(int c =0; c < classification->no_image_classes; c++){
                        if(max_prob < classification->image_classification[c].probability){
                            max_prob = classification->image_classification[c].probability;
                            max_class = classification->image_classification[c].type;
                        }
                    }
                    double node_pos[3] = {node.xy[0], node.xy[1], c_weight + 0.05};
                    draw_circle(node_pos, get_color_ratio_from_appearance_id(self, max_class), bot_to_radians(10.0), params.node_radius);  
                }
                else if(params.draw_node_class){
                    if(classification == NULL){
                        fprintf(stderr, "Classification is not there for node : %d\n", (int) node.node_id);
                        continue;
                    }
                    //have to draw the pie charts for these 
                    double pos[3] = {node.xy[0], node.xy[1], c_weight + 0.05};
                    draw_node_class_pie_chart(self, pos, &node.appearance_dist, bot_to_radians(10.0), params.node_radius, 0);                 
                }
                else if(params.draw_node_class_max){
                    if(classification == NULL){
                        fprintf(stderr, "Classification is not there for node : %d\n", (int) node.node_id);
                        continue;
                    }
                    double pos[3] = {node.xy[0], node.xy[1], c_weight + 0.05};
                    draw_node_class_pie_chart(self, pos, &node.appearance_dist, bot_to_radians(10.0), params.node_radius, 1);                 
                }
                else{
                    double node_pos[3] = {node.xy[0], node.xy[1], c_weight + 0.05};
                    //draw_circle(node_pos, region_weight, bot_to_radians(10.0), params.node_radius);   
                    draw_pose(node_pos, node.heading, region_weight, bot_to_radians(10.0), params.node_radius);   
                }
            }
        }                           
    }

    if(params.draw_objects){
        for(int i=0; i < p->no_objects; i++){
            slam_object_t obj = p->objects[i];
            slam_graph_node_t *node = (slam_graph_node_t *) g_hash_table_lookup(slam_nodes, &obj.node_id);
            
            double object_color_ratio = obj.type / 11.0;
            draw_object(obj.pos, object_color_ratio, .5);
            //draw a line between the object and the attached node 
            if(node){
                glLineWidth(5.0);
                glColor3f(0,0,0);
                glBegin(GL_LINES);
                glVertex3d(obj.pos[0], obj.pos[1], obj.pos[2]);
                glVertex3d(node->xy[0], node->xy[1], 0);
                glEnd();
            }
        }
    }

    if(params.draw_regions || params.draw_semantic_class || params.draw_semantic_class_max ||
       params.draw_semantic_label || params.draw_semantic_label_max){
        //draw the region mean 
        //glPointSize( 25.0 );
        //glBegin( GL_POINTS );
        
        double region_weight = 0;
        for(int i=0; i < p->no_regions; i++){
            slam_graph_region_t *region = &p->region_list[i];
            int r_id = *(int*) g_hash_table_lookup(region_mapped_ind, &(region->id));
            
            slam_probability_distribution_t *sem_classification;
            slam_probability_distribution_t *sem_label;
            if(self->params.draw_without_lang){
                sem_classification =  &region->region_type_dist_no_lang;
                sem_label =  &region->region_label_dist_no_lang;                
            }
            else if(self->params.draw_without_answers){
                sem_classification =  &region->region_type_dist_no_answer_lang;
                sem_label =  &region->region_label_dist_no_answer_lang;
            }
            else{
                sem_classification =  &region->region_type_dist;
                sem_label =  &region->region_label_dist;
            }

            if(params.color_regions){                
                if(params.remap_regions){
                    int remapped_ind = remap_region_ind(r_id, middle);//region->id, middle);
                    region_weight = remapped_ind/((double) p->no_regions-1);
                }
                else{
                    region_weight = region->id / ((double) p->no_regions-1);
                }               
            } 
            
            //glColor3fv(bot_color_util_jet(region_weight));
            if(params.draw_region_mean){
                double c_weight = weight;// + 0.001 * region->id;

                int mean_node_id = 0;
                for(int j=0; j< region->count; j++){
                    slam_graph_node_t node = region->nodes[j];  

                    if(node.is_supernode == 1){
                        mean_node_id = node.id;
                        break;
                    }
                }
                
                //glVertex3d( region->mean_xy[0], region->mean_xy[1] , c_weight);  
                if(params.draw_height){
                    c_weight += inc * mean_node_id;
                }
                else{
                    c_weight += inc * mean_node_id;
                    //c_weight += 0.000001 * mean_node_id;
                }

                double node_pos[3] = {region->mean_xy[0], region->mean_xy[1], c_weight};

                if(params.draw_semantic_class){
                    if (params.draw_bar_graph) 
                        draw_bar_chart_semantic_class(self, node_pos, sem_classification, 1, 0.5*params.bar_graph_scale, 4*params.bar_graph_scale, 2, params.draw_label_name);
                    else 
                    //have to draw the pie charts for these 
                        draw_semantic_chart(self, node_pos, sem_classification, 1, bot_to_radians(10.0), params.node_radius * 2, 0, params.draw_label_name, params.line_thickness);
                }
                else if(params.draw_semantic_class_max){
                    int draw_mode = 0;
                    if(params.draw_label_name)
                        draw_mode = 1;
                    if(params.draw_all_max_labels)
                        draw_mode = 2;
                    //have to draw the pie charts for these 
                    draw_semantic_chart(self, node_pos, sem_classification, 1, bot_to_radians(10.0), params.node_radius * 2, 1, draw_mode, params.line_thickness);
                    //(self, pos, classification, 1, bot_to_radians(10.0), params.node_radius);
                }
                else if(params.draw_semantic_label){
                    //have to draw the pie charts for these 
                    if(params.draw_semantic_label_valid && params.draw_bar_graph) 
                        draw_bar_chart(self, node_pos, sem_label, 1, 0.5*params.bar_graph_scale, 4*params.bar_graph_scale, 2, params.draw_label_name);
                    else if (params.draw_bar_graph) 
                         draw_bar_chart(self, node_pos, sem_label, 1, 0.5*params.bar_graph_scale, 4*params.bar_graph_scale, 2, params.draw_label_name);
                    else if (params.draw_semantic_label_valid){
                        int valid = draw_label_chart(self, node_pos, sem_label, 1, bot_to_radians(10.0), params.node_radius * 2, 2, params.draw_label_name, params.line_thickness);
                        if(!valid && params.draw_semantic_label_invalid_black){
                            draw_circle_black(node_pos, bot_to_radians(10.0), params.node_radius * 0.5);   
                        }
                    }
                    
                    else
                        draw_label_chart(self, node_pos, sem_label, 1, bot_to_radians(10.0), params.node_radius * 2, 0, params.draw_label_name,params.line_thickness);
                }
                else if(params.draw_semantic_label_max){
                    //have to draw the pie charts for these 
                    draw_label_chart(self, node_pos, sem_label, 1, bot_to_radians(10.0), params.node_radius * 2, 1, params.draw_label_name, params.line_thickness);
                    //(self, pos, classification, 1, bot_to_radians(10.0), params.node_radius);
                }

                else{
                    draw_circle(node_pos, region_weight, bot_to_radians(10.0), 2 * params.node_radius);   
                }
            }
            else{
                for(int j=0; j< region->count; j++){
                    slam_graph_node_t node = region->nodes[j];  
                    double c_weight = weight;
                    
                    if(params.draw_height){
                        c_weight += inc * node.id;
                    }
                    else{
                        c_weight += 0.01 * node.id;
                    }
                    //+ 0.001 * region->id;
                    //}
                    if(node.is_supernode == 1){
                        double node_pos[3] = {node.xy[0], node.xy[1] , c_weight + 0.05};
                        
                        if(params.draw_semantic_class){
                            if (params.draw_bar_graph) 
                                draw_bar_chart_semantic_class(self, node_pos, sem_classification, 1, 0.5*params.bar_graph_scale, 4*params.bar_graph_scale, 2, params.draw_label_name);
                            else 
                                //have to draw the pie charts for these 
                                draw_semantic_chart(self, node_pos, sem_classification, 1, bot_to_radians(10.0), params.node_radius * 2, 0,  params.draw_label_name, params.line_thickness);
                        }
                        else if(params.draw_semantic_class_max){
                            //have to draw the pie charts for these
                            int draw_mode = 0;
                            if(params.draw_label_name)
                                draw_mode = 1;
                            if(params.draw_all_max_labels)
                                draw_mode = 2;
                            draw_semantic_chart(self, node_pos, sem_classification, 1, bot_to_radians(10.0), params.node_radius * 2, 1,  draw_mode, params.line_thickness);
                        }
                        else if(params.draw_semantic_label){
                            //have to draw the pie charts for these 
                            //fprintf(stderr, "Region : %d\n", r_id); 
                            
                            if(params.draw_semantic_label_valid && params.draw_bar_graph) 
                                draw_bar_chart(self, node_pos, sem_label, 1, 0.5*params.bar_graph_scale, 4*params.bar_graph_scale, 2, params.draw_label_name);
                            else if (params.draw_bar_graph) 
                                draw_bar_chart(self, node_pos, sem_label, 1, 0.5*params.bar_graph_scale, 4*params.bar_graph_scale, 2, params.draw_label_name);
                            else if(params.draw_semantic_label_valid){
                                int valid = draw_label_chart(self, node_pos, sem_label, 1, bot_to_radians(10.0), params.node_radius * 2, 2, params.draw_label_name, params.line_thickness);
                                if(!valid && params.draw_semantic_label_invalid_black){
                                    draw_circle_black(node_pos, bot_to_radians(10.0), params.node_radius * 0.5);   
                                }
                                
                            }
                                //draw_label_chart(self, node_pos, sem_label, 1, bot_to_radians(10.0), params.node_radius * 2, 2, params.draw_label_name, params.line_thickness);
                            else
                                draw_label_chart(self, node_pos, sem_label, 1, bot_to_radians(10.0), params.node_radius * 2, 0, params.draw_label_name, params.line_thickness);
                            //draw_label_chart(self, node_pos, sem_label, 1, bot_to_radians(10.0), params.node_radius * 2, 0, params.draw_label_name);
                        }
                        else if(params.draw_semantic_label_max){
                           
                            //have to draw the pie charts for these
                            
                            draw_label_chart(self, node_pos, sem_label, 1, bot_to_radians(10.0), params.node_radius * 2, 1, params.draw_label_name, params.line_thickness);
                            //(self, pos, classification, 1, bot_to_radians(10.0), params.node_radius);
                        }
                        else{
                            draw_circle(node_pos, region_weight, bot_to_radians(10.0), 2 * params.node_radius);   
                        }
                        //glVertex3d( node.xy[0], node.xy[1] , c_weight);     
                        break;
                    }
                }
            }
        }        
    }
    glPopAttrib();

    glColor3fv(bot_color_util_jet(weight_color));
        
    if(params.draw_prob || params.draw_node_ids|| params.draw_region_ids || params.draw_segment_ids){
        glColor3f(1,1,1);
        for(int i=0; i < p->no_regions; i++){
            slam_graph_region_t *region = &p->region_list[i];
            if(params.draw_region_ids){
                slam_graph_node_t *node = (slam_graph_node_t *) g_hash_table_lookup(slam_nodes, &(region->center_ind));
                double c_weight = weight;
                if(params.draw_height){
                    c_weight += inc * node->id; //these heights are going to be messed up 
                }
                double textpos[3] = {node->xy[0] +0.4, node->xy[1] +0.4, c_weight};
                if(params.draw_segment_ids){
                    sprintf(label,"%d : %d", (int) region->id, (int) node->segment_id);
                }
                else{
                    sprintf(label,"%d", (int) region->id);
                }
                bot_gl_draw_text(textpos, GLUT_BITMAP_HELVETICA_12, label,
                                 BOT_GL_DRAW_TEXT_DROP_SHADOW);
            }
            else{
                for(int j=0; j< region->count; j++){
                    slam_graph_node_t node = region->nodes[j];     
                    if(node.is_supernode == 1 && params.draw_regions || params.draw_nodes){  
                        //draw the prob 
                        double c_weight = weight;
                        if(params.draw_height){
                            c_weight += inc * i;
                        }
                        double textpos[3] = {node.xy[0] +0.4, node.xy[1] +0.4, c_weight};
                
                        if(params.draw_prob){
                            sprintf(label,"%.3f", node.pofz);
                            bot_gl_draw_text(textpos, GLUT_BITMAP_HELVETICA_12, label,
                                             BOT_GL_DRAW_TEXT_DROP_SHADOW);
                        }
                        else{
                            if(params.draw_segment_ids && params.draw_node_ids){
                                sprintf(label,"(%d)=>%d", (int) node.segment_id, (int)node.id);
                            }
                            else if(params.draw_segment_ids){
                                sprintf(label,"(%d)", (int) node.segment_id);
                            }
                            else{
                                sprintf(label,"[%d]=>%d", (int) region->id, (int)node.id);
                            }
                            bot_gl_draw_text(textpos, GLUT_BITMAP_HELVETICA_12, label,
                                             BOT_GL_DRAW_TEXT_DROP_SHADOW);
                    
                        }                
                    }
                }
            }
        }
    }
        
    if(params.draw_map_points){
        glPointSize( 2.0 );
        glBegin( GL_POINTS );
        glColor3fv(bot_color_util_jet(weight_color));
                    
        for(int i=0; i < p->no_regions; i++){
            slam_graph_region_t *region = &p->region_list[i];
            int r_id = *(int *) g_hash_table_lookup(region_mapped_ind, &(region->id));
            for(int j=0; j< region->count; j++){
                slam_graph_node_t node = region->nodes[j];        
                        
                slam_laser_pose_t *laser_pose = (slam_laser_pose_t *) g_hash_table_lookup(self->node_scans, &node.node_id);
                if(!laser_pose){
                    if(!self->requested_scans){
                        self->requested_scans = 1;
                         slam_pixel_map_request_t msg; 
                         msg.utime = bot_timestamp_now();
                         msg.particle_id = p->id;
                         msg.request = SLAM_PIXEL_MAP_REQUEST_T_REQ_SCAN_POINTS;
                         slam_pixel_map_request_t_publish(self->lcm, "SLAM_SCAN_REQUEST", &msg);
                    }
                    continue;
                }

                if(params.color_regions){
                    double region_weight = 0;
                    if(params.remap_regions){
                        int remapped_ind = remap_region_ind(r_id, middle);//node.parent_supernode, middle);
                        region_weight = remapped_ind/((double) p->no_regions-1);
                    }
                    else{
                        region_weight = node.parent_supernode / ((double) p->no_regions-1);
                    }                
                    glColor3fv(bot_color_util_jet(region_weight));
                }

                BotTrans bodyToLocal;
                bodyToLocal.trans_vec[0] = node.xy[0];
                bodyToLocal.trans_vec[1] = node.xy[1];
                bodyToLocal.trans_vec[2] = 0;
                double rpy[3] = { 0, 0, node.heading };
                bot_roll_pitch_yaw_to_quat(rpy, bodyToLocal.rot_quat);
                double pBody[3] = { 0, 0, 0 };
                double pLocal[3];

                double c_weight = weight;
                if(params.draw_height){
                    c_weight += inc * i;
                }

                for(int k=0; k < laser_pose->pl.no; k++){
                    pBody[0] = laser_pose->pl.points[k].pos[0];
                    pBody[1] = laser_pose->pl.points[k].pos[1];
                    pBody[2] = 0;
                    bot_trans_apply_vec(&bodyToLocal, pBody, pLocal);
                            
                    glVertex3d( pLocal[0], pLocal[1] , c_weight);
                }
            }
        }
        glEnd();
    }

    //draw the edges 
    glPushAttrib (GL_DEPTH_BUFFER_BIT | GL_POINT_BIT | GL_CURRENT_BIT);
    glEnable (GL_DEPTH_TEST);
    glEnable(GL_BLEND);
        
    glDepthMask(GL_FALSE);

    if(params.draw_inter_region_edges || params.draw_intra_region_edges){
        for(int i=0; i < p->no_edges; i++){            
            int a;
            int b;
            
            a = p->edge_list[i].actual_scanned_node_id_1;
            b = p->edge_list[i].actual_scanned_node_id_2;

            slam_graph_node_t *nodea = (slam_graph_node_t *) g_hash_table_lookup(slam_nodes, &a);
            slam_graph_node_t *nodeb = (slam_graph_node_t *) g_hash_table_lookup(slam_nodes, &b);
            
            if(!params.draw_intra_region_edges){
                if(nodea->parent_supernode == nodeb->parent_supernode){
                    continue;
                }
            }

            if(!params.draw_inter_region_edges){
                if(nodea->parent_supernode != nodeb->parent_supernode){
                    continue;
                }
            }
            
            double c_weight_1 = weight;
            double c_weight_2 = weight;
            if(params.draw_height){
                c_weight_1 += inc * a;
                c_weight_2 += inc * b;
            }
            
            if(p->edge_list[i].status == SLAM_GRAPH_EDGE_T_STATUS_INITIALIZED){
                //we should prob skip them
                continue;
            }
            else{
                //this should be deprecated 
                if(p->edge_list[i].status == SLAM_GRAPH_EDGE_T_STATUS_FAILED){ 
                    if(!params.draw_dead_edges)
                        continue;
                    double width = params.edge_thickness; //params.edge_thickness * p->edge_list[i].scanmatch_hit;
                    if(width == 0)
                        width = 0.1;
                    glLineWidth(width);
                    glColor3fv(bot_color_util_black);                            
                }
                else if(p->edge_list[i].status == SLAM_GRAPH_EDGE_T_STATUS_SUCCESS){
                    double width = params.edge_thickness * p->edge_list[i].scanmatch_hit;
                    if(width == 0)
                        width = 0.1;
                    glLineWidth(width);
                    
                    if(p->edge_list[i].type == SLAM_GRAPH_EDGE_T_TYPE_SM_LC){
                        float loop_color[3] = {1.0, 0, 0};
                        if (params.draw_edges_in_black)
                            glColor3fv (bot_color_util_black);
                        else
                            glColor3fv(bot_color_util_jet(1.0));
                    }
                    else if(p->edge_list[i].type == SLAM_GRAPH_EDGE_T_TYPE_LANGUAGE || p->edge_list[i].type == SLAM_GRAPH_EDGE_T_TYPE_LANGUAGE_FULL){
                        if (params.draw_edges_in_black)
                            glColor3fv (bot_color_util_black);
                        else {
                            float loop_color[3] = {0, 1.0, 0};
                            glColor3fv(loop_color);
                        }
                    }
                    else if(p->edge_list[i].type == SLAM_GRAPH_EDGE_T_TYPE_ODOM_INC && params.draw_odom){
                        if (params.draw_edges_in_black)
                            glColor3fv (bot_color_util_black);
                        else {
                            float loop_color[3] = {1.0, 0.8, 0.34};
                            glColor3fv(loop_color);
                        }
                    }
                    else if(p->edge_list[i].type == SLAM_GRAPH_EDGE_T_TYPE_SM_INFERRED){
                        if (params.draw_edges_in_black)
                            glColor3fv (bot_color_util_black);
                        else {
                            float loop_color[3] = {1.0, 0.3, 1.0};
                            glColor3fv(loop_color);
                        }
                    }
                    else if(p->edge_list[i].type == SLAM_GRAPH_EDGE_T_TYPE_SM_NEW_INFO){
                        if (params.draw_edges_in_black)
                            glColor3fv (bot_color_util_black);
                        else {
                            float loop_color[3] = {0.4, 0.1, 1.0};
                            glColor3fv(loop_color);
                        }
                    }
                    
                    else{
                        if (params.draw_edges_in_black)
                            glColor3fv (bot_color_util_black);
                        else 
                            glColor3fv(bot_color_util_jet(weight_color));
                    }
                }                        
            }                    
            
            
            double alpha = 1.0;
            double scale = 0.05;
            
            glPointSize(4.0f);
            glBegin(GL_LINES);

            if(params.color_regions){
                double region_weight = 0;
                if(params.remap_regions){
                    int r_id = *(int*) g_hash_table_lookup(region_mapped_ind, &(nodeb->parent_supernode));//region->id));
                    int remapped_ind = remap_region_ind(r_id, middle);//nodeb->parent_supernode, middle);
                    region_weight = remapped_ind/((double) p->no_regions-1);
                }
                else{
                    region_weight = nodeb->parent_supernode / ((double) p->no_regions-1);
                }        
                if(!params.draw_edges_in_black)
                    glColor3fv(bot_color_util_jet(region_weight));
            }
            
            glVertex3d(nodea->xy[0],
                       nodea->xy[1],
                       c_weight_1);
            glVertex3d(nodeb->xy[0],
                       nodeb->xy[1],
                       c_weight_2);
            glEnd();
        }

        if(params.draw_dead_edges){
            for(int i=0; i < p->no_rejected_edges; i++){
                int a;
                int b;
                
                a = p->rejected_edge_list[i].actual_scanned_node_id_1;
                b = p->rejected_edge_list[i].actual_scanned_node_id_2;
                
                double c_weight_1 = weight;
                double c_weight_2 = weight;
                if(params.draw_height){
                    c_weight_1 += inc * a;
                    c_weight_2 += inc * b;
                }
                
                if(p->rejected_edge_list[i].status == SLAM_GRAPH_EDGE_T_STATUS_INITIALIZED){
                    //we should prob skip them
                    continue;
                }
                else{
                    if(p->rejected_edge_list[i].status == SLAM_GRAPH_EDGE_T_STATUS_FAILED){ 
                        double width =  params.edge_thickness;// * p->rejected_edge_list[i].scanmatch_hit;

                        if(width == 0)
                            width = 0.1;
                        glLineWidth(width);
                        glColor3fv(bot_color_util_black);                            
                    }                                     
                }                    
                slam_graph_node_t *nodea = (slam_graph_node_t *) g_hash_table_lookup(slam_nodes, &a);
                slam_graph_node_t *nodeb = (slam_graph_node_t *) g_hash_table_lookup(slam_nodes, &b);
                
                double alpha = 1.0;
                double scale = 0.05;
                
                glPointSize(4.0f);
                glBegin(GL_LINES);
                
                glVertex3d(nodea->xy[0],
                           nodea->xy[1],
                           c_weight_1);
                glVertex3d(nodeb->xy[0],
                           nodeb->xy[1],
                           c_weight_2);
                glEnd();
            }
        }        
    }

    glDepthMask(GL_TRUE);

    //draw the complex language paths 
    if(params.draw_complex_language_paths && complex_lang != NULL){
        if(complex_lang->count > 0){
            if(self->params.draw_complex_language_event_index <= (complex_lang->count -1)){
                //we should draw the question and answer 

                char prob_l[200];
                slam_complex_language_path_list_t *path_list = &complex_lang->language[self->params.draw_complex_language_event_index]; 
                
                if(self->params.draw_landmarks){
                    for(int j=0; j < path_list->valid_landmark_count; j++){
                        int id = path_list->valid_landmarks[j];
                        slam_graph_node_t *node = (slam_graph_node_t *) g_hash_table_lookup(slam_nodes, &id);
                        double c_weight = weight + 0.1;
                        double node_pos[3] = {node->xy[0], node->xy[1], c_weight};
                        if(params.draw_height){
                            c_weight += inc * id;
                        }
                        draw_circle(node_pos, 1.0, bot_to_radians(10.0), 2.5 *params.node_radius);  
                    }
                }

                char buf[500]; 

                if(params.draw_answer_prob && params.answer_ind < path_list->no_questions){
                    slam_language_question_result_t *question = &path_list->questions[params.answer_ind];
                    slam_graph_node_t *q_nd = (slam_graph_node_t *) g_hash_table_lookup(slam_nodes, &question->q_asked_id);
                    char *question_str = question->question;
                    char *answer_str = question->answer;
                    double c_weight = weight + 0.15;

                    if(params.draw_height){
                        c_weight += inc * q_nd->id;
                    }
                    double node_pos[3] = {q_nd->xy[0], q_nd->xy[1], c_weight + 0.1};
                    draw_triangle_pose(node_pos, q_nd->heading, 0);
                    sprintf(buf, "%d - %s : %s", (int) q_nd->id, question_str, answer_str);
                    double textpos[3] = {q_nd->xy[0], q_nd->xy[1] , c_weight + 0.1};
                    glColor3f(1.0,1.0,1.0);
                    bot_gl_draw_text(textpos, GLUT_BITMAP_HELVETICA_12, buf, BOT_GL_DRAW_TEXT_DROP_SHADOW);
                }

                double sum = 0; 
                double o_sum = 0; 

                if(params.draw_fig_prob){
                    for(int j=0; j< path_list->no_paths; j++){
                        slam_node_path_t path = path_list->path[j];
                        if(params.ignore_prior){
                            sum += path.prob;
                            o_sum += path.orig_prob;
                        }
                        else{
                            sum += path.prob * path.prior; 
                            o_sum += path.orig_prob * path.prior;
                        }
                    }                    
                }

                if(o_sum < 1e-10){
                    o_sum = 1.0;
                }

                if(sum < 1e-10){
                    sum = 1.0;
                }

                for(int j=0; j< path_list->no_paths; j++){
                    if(!self->params.draw_all_complex_language_paths){
                        //check the valid index and continue;
                        if(j != params.path_index)
                            continue;
                    }
                    slam_node_path_t path = path_list->path[j];
                    slam_graph_node_t *p_nd = (slam_graph_node_t *) g_hash_table_lookup(slam_nodes, &path.node_id);

                    double c_weight = weight + 0.15;
                    double node_pos[3] = {p_nd->xy[0], p_nd->xy[1], c_weight};
                    if(params.draw_height){
                        c_weight += inc * path.node_id;
                    }

                    draw_circle(node_pos, 0.5, bot_to_radians(10.0), 1.5 *params.node_radius);  
                    
                    if(params.draw_fig_prob){
                        if(params.ignore_prior){
                            sprintf(buf, "%d - %.2f", (int) p_nd->parent_supernode, path.prob / sum);
                        }
                        else{
                            sprintf(buf, "%d - %.2f", (int) p_nd->parent_supernode, path.prob * path.prior/ sum);
                        }
                        double textpos[3] = {p_nd->xy[0], p_nd->xy[1] , c_weight + 0.1};
                        glColor3f(1.0,1.0,1.0);
                        bot_gl_draw_text(textpos, GLUT_BITMAP_HELVETICA_12, buf, BOT_GL_DRAW_TEXT_DROP_SHADOW);
                    }                    
                    else if(params.draw_answer_prob && params.answer_ind < path.no_questions){
                        //p_nd->answer_prob[params.answer_ind].type
                        sprintf(buf, "%d - %.2f", (int) p_nd->parent_supernode, path.answer_prob[params.answer_ind].probability);
                        double textpos[3] = {p_nd->xy[0], p_nd->xy[1] , c_weight + 0.1};
                        glColor3f(1.0,1.0,1.0);
                        bot_gl_draw_text(textpos, GLUT_BITMAP_HELVETICA_12, buf, BOT_GL_DRAW_TEXT_DROP_SHADOW);
                    }
                    else if(params.draw_original_prob){
                        //p_nd->answer_prob[params.answer_ind].type
                        if(params.ignore_prior){
                            sprintf(buf, "%d - %.2f", (int) p_nd->parent_supernode, path.orig_prob / o_sum);
                        }
                        else{
                            sprintf(buf, "%d - %.2f", (int) p_nd->parent_supernode, path.orig_prob * path.prior/ o_sum);
                        }
                        double textpos[3] = {p_nd->xy[0], p_nd->xy[1] , c_weight + 0.1};
                        glColor3f(1.0,1.0,1.0);
                        bot_gl_draw_text(textpos, GLUT_BITMAP_HELVETICA_12, buf, BOT_GL_DRAW_TEXT_DROP_SHADOW);
                    }

                    //fprintf(stderr, "\tPath to Node : %d (%d) \n", (int) p_nd->parent_supernode, path.count);
                    if(path.count < 2){
                        continue;
                    }
                    glLineWidth(3);
                    if(path.updated){
                        glColor3f(0,1.0,0);  
                    }
                    else{
                        glColor3f(1.0,0,0);  
                    }

                    double first_xy[3] = {.0};
                    double last_xy[3] = {.0};
                    glBegin(GL_LINES);                    

                    for(int k=1; k < path.count; k++){
                        int64_t id_1 = path.nodes[k-1];
                        int64_t id_2 = path.nodes[k];
                        double c_weight_1 = weight + 0.1;
                        double c_weight_2 = weight + 0.1;
                        if(params.draw_height){
                            c_weight_1 += inc * id_1;
                            c_weight_1 += inc * id_2;
                        }
                                                
                        slam_graph_node_t *nd1 = (slam_graph_node_t *) g_hash_table_lookup(slam_nodes, &id_1);
                        slam_graph_node_t *nd2 = (slam_graph_node_t *) g_hash_table_lookup(slam_nodes, &id_2);
                        if(nd1 == NULL || nd2 == NULL){
                            fprintf(stderr, "Error Node not found : %d - %d\n", (int) id_1, (int) id_2);
                            break;
                        }
                        
                        if(k==1){
                            first_xy[0] = nd1->xy[0];
                            first_xy[1] = nd1->xy[1];
                            first_xy[2] = c_weight_1;
                        }

                        if(k==(path.count -1)){
                            last_xy[0] = nd2->xy[0];
                            last_xy[1] = nd2->xy[1];
                            last_xy[2] = c_weight_2;
                        }

                        glVertex3d(nd1->xy[0],
                                   nd1->xy[1],
                                   c_weight_1);
                        glVertex3d(nd2->xy[0],
                                   nd2->xy[1],
                                   c_weight_2);
                    }
                    glEnd();

                    /*sprintf(prob_l, "Path : %.3f Landmark : %.2f Ground : %.2f", );
                    double mean_pos[3] = {(first_xy[0] + last_xy[0])/2, (first_xy[1] + last_xy[1])/2, (first_xy[2] + last_xy[2])/2};
                    glColor3f(1.0,1.0,1.0);
                    
                    bot_gl_draw_text(mean_pos, GLUT_BITMAP_HELVETICA_12, prob_l,
                    BOT_GL_DRAW_TEXT_DROP_SHADOW);*/
                }                
            }
            else{
                bot_gtk_param_widget_set_int(self->lg_pw, PARAM_COMPLEX_LANGUAGE_EVENT_INDEX, 0);
            }
        }
    }
    
    if(params.draw_region_connections){    
        if (params.draw_edges_in_black)
            glColor3f(0,0,0);
        else
            glColor3f(0,0,1.0);

        glLineWidth(4);
        GHashTable *regions = g_hash_table_new(g_int_hash, g_int_equal);
        //fprintf(stderr, "Particle ID : %d - No of regions: %d\n", p->id, p->no_regions);
        for(int i=0; i < p->no_regions; i++){
            slam_graph_region_t *rn = &p->region_list[i];
            //fprintf(stderr, "\tRegion : %d -> Center : %d\n", rn->id, rn->center_ind);
            g_hash_table_insert(regions, &(rn->id), rn);
        }
        
        for(int i=0; i < p->no_region_edges; i++){
            slam_graph_region_edge_t edge = p->region_edges[i];
            if(edge.type == SLAM_GRAPH_REGION_EDGE_T_TYPE_INCREMENTAL){
                glColor3f(0,0,1);
            }
            else if(edge.type == SLAM_GRAPH_REGION_EDGE_T_TYPE_LOOPCLOSURE){
                glColor3f(1,0,0);
            }
            else{
                glColor3f(0,1,0);
            }

            int rn_1_id = edge.region_1_id;
            int rn_2_id = edge.region_2_id;
            
            slam_graph_region_t *rn1 =  (slam_graph_region_t *) g_hash_table_lookup(regions, &rn_1_id);
            slam_graph_region_t *rn2 =  (slam_graph_region_t *) g_hash_table_lookup(regions, &rn_2_id);

            if(rn1 == NULL|| rn2 == NULL){
                fprintf(stderr, "Region %d - %p = %d = %p\n", rn_1_id, (void *) rn1, 
                        rn_2_id, (void *) rn2);
                continue;
            }

            double ra[2] = {0}, rb[2] = {0};
            
            if(params.draw_region_mean){
                ra[0] = rn1->mean_xy[0];
                ra[1] = rn1->mean_xy[1];
                rb[0] = rn2->mean_xy[0];
                rb[1] = rn2->mean_xy[1];
            }
            else{
                slam_graph_node_t *nodea = (slam_graph_node_t *) g_hash_table_lookup(slam_nodes, &(rn1->center_ind));
                ra[0] = nodea->xy[0];
                ra[1] = nodea->xy[1];
                slam_graph_node_t *nodeb = (slam_graph_node_t *) g_hash_table_lookup(slam_nodes, &(rn2->center_ind));
                rb[0] = nodeb->xy[0];
                rb[1] = nodeb->xy[1];
            }
            
            double c_weight_1 = weight;
            double c_weight_2 = weight;
            if(params.draw_height){
                c_weight_1 += inc * rn1->center_ind;
                c_weight_2 += inc * rn2->center_ind;
            }
            glPointSize(4.0f);
            
            glBegin(GL_LINES);
                
            glVertex3d(ra[0], ra[1], c_weight_1);
            glVertex3d(rb[0], rb[1], c_weight_2);
            glEnd();
        }
        //.region_1_id
        //region_2_id
        g_hash_table_destroy (regions);
    }

    glPopAttrib();
    //for(guint i=0; i <  g_list_length(region_mapped_ind); i++){
    //
    //}
    g_hash_table_destroy (region_mapped_ind);
    g_hash_table_destroy (slam_nodes);
}

static slam_graph_region_particle_t *get_diff_region_particle(slam_graph_region_particle_list_t *current, slam_graph_region_particle_list_t *old, int index, int iter){
    slam_graph_region_particle_t *new_particle = &current->particle_list[index];
    slam_graph_region_particle_t *old_particle = NULL;
    
    for(int i=0; i <  old->no_particles; i++){
        if(old->particle_list[index].id == new_particle->id){
            old_particle = &old->particle_list[index];
            break;
        }
    }
    
    if(old_particle == NULL){
        fprintf(stderr, "New particle did not exist\n");
        return NULL;
    }
    
    slam_graph_region_particle_t *diff_particle = slam_graph_region_particle_t_copy(&current->particle_list[index]);
    //find the transition 
    //put the old nodes to a hash table 
    GHashTable *old_nodes = g_hash_table_new(g_int_hash, g_int_equal);

    //we need to refer to the nodes for drawing edges 
    for(int i=0; i < old_particle->no_regions; i++){
        slam_graph_region_t *region = &old_particle->region_list[i];
        
        for(int j=0; j< region->count; j++){
            g_hash_table_insert(old_nodes, &(region->nodes[j].id), &region->nodes[j]);
        }
    }

    for(int i=0; i < diff_particle->no_regions; i++){
        slam_graph_region_t *region = &diff_particle->region_list[i];
        
        for(int j=0; j< region->count; j++){
            slam_graph_node_t *node = &region->nodes[j]; 
            slam_graph_node_t *old_node = (slam_graph_node_t *) g_hash_table_lookup(old_nodes, &(node->node_id));
            if(old_node == NULL)
                continue;

            double dx = (node->xy[0] -  old_node->xy[0]) / GRAPH_ANIMATION_RESOLUTION;
            double dy = (node->xy[1] -  old_node->xy[1]) / GRAPH_ANIMATION_RESOLUTION;
            node->xy[0] = old_node->xy[0] + dx * iter;
            node->xy[1] = old_node->xy[1] + dy * iter;
        }
    }

    g_hash_table_destroy (old_nodes);    
    return diff_particle;
}

static void on_slam_progress (const lcm_recv_buf_t *rbuf, const char *channel,
                              const slam_performance_t *msg, void *user)
{
    RendererRegionTopology *self = (RendererRegionTopology *)user;
    g_assert(self);
    if(self->performance){
        slam_performance_t_destroy(self->performance);
    }
    self->performance = slam_performance_t_copy(msg);
}

static void send_map_request(RendererRegionTopology *self, int id){
    slam_pixel_map_request_t msg; 
    msg.utime = bot_timestamp_now();
    msg.particle_id = id;
    msg.request = SLAM_PIXEL_MAP_REQUEST_T_REQ_PIXEL_MAP;
    slam_pixel_map_request_t_publish(self->lcm, "PIXEL_MAP_REQUEST", &msg);
}

static void on_topo_graph (const lcm_recv_buf_t *rbuf, const char *channel,
                           const slam_graph_region_particle_list_t *msg, void *user)
{
    RendererRegionTopology *self = (RendererRegionTopology *)user;
    g_assert(self);

    bot_ptr_circular_add(self->particle_history, slam_graph_region_particle_list_t_copy(msg));
    if(bot_ptr_circular_size(self->particle_history) > 1){
        bot_gtk_param_widget_set_enabled (self->hi_pw, PARAM_COMPARE_PARTICLE_HISTORY_INDEX, 1);
        int last_history_value = fmin(bot_gtk_param_widget_get_int(self->hi_pw, PARAM_COMPARE_PARTICLE_HISTORY_INDEX) + 1,
                                      bot_ptr_circular_size(self->particle_history)-1);
        //if(bot_ptr_circular_size(self->particle_history)-1 > 1){
        bot_gtk_param_widget_modify_int(self->hi_pw, PARAM_COMPARE_PARTICLE_HISTORY_INDEX, 0, bot_ptr_circular_size(self->particle_history)-1, 1,last_history_value);
        //}
    }
    else{
        bot_gtk_param_widget_set_enabled (self->hi_pw, PARAM_COMPARE_PARTICLE_HISTORY_INDEX, 0);
    }
    //destory the list 
    g_list_free_full (self->particle_list, destroy_region_particle);
    self->particle_list = NULL;
    
    add_node_classifications(self, msg);
    
    //we should clear this 
    for(int i=0; i <  msg->no_particles; i++){
        slam_graph_region_particle_t *p = slam_graph_region_particle_t_copy(&msg->particle_list[i]);

        
        self->particle_list  = g_list_insert_sorted ( self->particle_list , p, compare_graph_id);
    }
    
    int last_valid_ind = bot_gtk_param_widget_get_int(self->pw, VALID_MAP_IND);
    int last_valid_g1 = bot_gtk_param_widget_get_int(self->gc_pw, PARAM_DRAW_GRAPH_1_ID);
    int last_valid_g2 = bot_gtk_param_widget_get_int(self->gc_pw, PARAM_DRAW_GRAPH_2_ID);
    if(msg->no_particles > 1){
        
        bot_gtk_param_widget_set_enabled (self->pw, VALID_MAP_IND, 1);
        bot_gtk_param_widget_set_enabled (self->gc_pw, PARAM_DRAW_GRAPH_1_ID, 1);
        bot_gtk_param_widget_set_enabled (self->gc_pw, PARAM_DRAW_GRAPH_2_ID, 1);
        
        bot_gtk_param_widget_modify_int(self->pw, VALID_MAP_IND, 0, msg->no_particles-1, 1, last_valid_ind);
        bot_gtk_param_widget_modify_int(self->gc_pw, PARAM_DRAW_GRAPH_1_ID, 0, msg->no_particles-1, 1, last_valid_g1);
        bot_gtk_param_widget_modify_int(self->gc_pw, PARAM_DRAW_GRAPH_2_ID, 0, msg->no_particles-1, 1, last_valid_g2);
    }
    else{
        bot_gtk_param_widget_set_enabled (self->pw, VALID_MAP_IND, 0);
        bot_gtk_param_widget_set_enabled (self->gc_pw, PARAM_DRAW_GRAPH_1_ID, 0);
        bot_gtk_param_widget_set_enabled (self->gc_pw, PARAM_DRAW_GRAPH_2_ID, 0);
    }

    self->no_particles = msg->no_particles;
    self->have_data = 1;    

    send_map_request(self, self->current_ind);

    if(bot_gtk_param_widget_get_bool(self->hi_pw, PARAM_COMPARE_PARTICLE_HISTORY) && bot_ptr_circular_size(self->particle_history) > 1){
        self->diff_utime = bot_timestamp_now();
        self->diff_iter = 0;
    }
    else{
        self->params.draw_diff = 0;
        bot_viewer_request_redraw (self->viewer);
    }
}

static void on_pose (const lcm_recv_buf_t *rbuf, const char *channel,
                     const bot_core_pose_t *msg, void *user)
{
    RendererRegionTopology *self = (RendererRegionTopology *)user;

    self->last_pose_utime = msg->utime;

    return;
}

static void on_laser_pose (const lcm_recv_buf_t *rbuf, const char *channel,
                           const slam_laser_pose_t *msg, void *user)
{
    RendererRegionTopology *self = (RendererRegionTopology *)user;
    g_assert(self);

    slam_laser_pose_t *laser_pose = slam_laser_pose_t_copy(msg);
    g_hash_table_insert(self->node_scans, &(laser_pose->id), laser_pose);
    bot_viewer_request_redraw (self->viewer);
}

static void send_answer(RendererRegionTopology *self, const char *answer){
    fprintf(stderr, "User answered %s\n", answer);
    if(self->question_dialog){
        gtk_widget_destroy(self->question_dialog);
        self->question_dialog = NULL;
    }

    if(self->last_question){
        fprintf(stderr, "Publishing answer\n");
        slam_language_answer_t msg; 
        msg.utime = bot_timestamp_now();
        msg.id = self->last_question->id;
        msg.particle_id = self->last_question->particle_id;
        msg.answer = (char *) answer; 
        slam_language_answer_t_publish(self->lcm, "SLAM_SR_ANSWER", &msg);
        
        slam_language_question_t_destroy(self->last_question);
        self->last_question = NULL;
    }
    else{
        fprintf(stderr, "Error - no outstanding question\n");
    }
}

static
void yes_answer_button(GtkWidget *button __attribute__ ((unused)),
                       gpointer user_data __attribute__ ((unused)))
{
    RendererRegionTopology *self = (RendererRegionTopology*) user_data;
    
    /*fprintf(stderr, "User answered yes\n");
    gtk_widget_destroy(self->question_dialog);
    //send the anwer - and destroy the existing question message - as this was answered 
    self->question_dialog = NULL;

    if(self->last_question){
        fprintf(stderr, "Publishing answer\n");
        slam_language_answer_t msg; 
        msg.utime = bot_timestamp_now();
        msg.id = self->last_question->id;
        msg.particle_id = self->last_question->particle_id;
        msg.answer = "yes";
        slam_language_answer_t_publish(self->lcm, "SLAM_SR_ANSWER", &msg);
        
        slam_language_question_t_destroy(self->last_question);
        self->last_question = NULL;
    }
    else{
        fprintf(stderr, "Error - no outstanding question\n");
        }*/
    send_answer(self, "yes");
}

static
void no_answer_button(GtkWidget *button __attribute__ ((unused)),
                       gpointer user_data __attribute__ ((unused)))
{
    RendererRegionTopology *self = (RendererRegionTopology*) user_data;
    
    /*fprintf(stderr, "User answered no\n");
    gtk_widget_destroy(self->question_dialog);
    self->question_dialog = NULL;

    if(self->last_question){
        fprintf(stderr, "Publishing answer\n");
        slam_language_answer_t msg; 
        msg.utime = bot_timestamp_now();
        msg.id = self->last_question->id;
        msg.particle_id = self->last_question->particle_id;
        msg.answer = "no";
        slam_language_answer_t_publish(self->lcm, "SLAM_SR_ANSWER", &msg);
        
        slam_language_question_t_destroy(self->last_question);
        self->last_question = NULL;
    }
    else{
        fprintf(stderr, "Error - no outstanding question\n");
        }*/
    send_answer(self, "no");
}

static
void invalid_answer_button(GtkWidget *button __attribute__ ((unused)),
                           gpointer user_data __attribute__ ((unused)))
{
    RendererRegionTopology *self = (RendererRegionTopology*) user_data;
    
    fprintf(stderr, "User answered invalid\n");
    /*gtk_widget_destroy(self->question_dialog);
    self->question_dialog = NULL;

    if(self->last_question){
        fprintf(stderr, "Publishing answer\n");
        slam_language_answer_t msg; 
        msg.utime = bot_timestamp_now();
        msg.id = self->last_question->id;
        msg.particle_id = self->last_question->particle_id;
        msg.answer = "invalid";
        slam_language_answer_t_publish(self->lcm, "SLAM_SR_ANSWER", &msg);
        
        slam_language_question_t_destroy(self->last_question);
        self->last_question = NULL;
    }
    else{
        fprintf(stderr, "Error - no outstanding question\n");
        }*/
    send_answer(self, "invalid");
}

static void show_question_dialog(RendererRegionTopology *self){
    //keep track of what is already asked - and either destroy the old question or not 
    //show the new question till the old one was answered 
    if(!self->last_question || !self->last_question->question)
        return;

    //for now destroy the question dialog box - the old one 
    if(self->question_dialog)
        gtk_widget_destroy(self->question_dialog);

    self->question_dialog = NULL;

    if(self->last_question->id == -1){
        fprintf(stderr, "Question timeout - removing dialog\n");
        return;
    }

    static GtkWidget *question_label;
    GtkWidget *hbox, *label, *button;
    char buffer[10];
    
    self->question_utime = bot_timestamp_now();    

    self->question_dialog = gtk_dialog_new();
    hbox = gtk_hbox_new(FALSE, 0);
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG (self->question_dialog)->vbox),
                        hbox, TRUE, TRUE, 0);
    //insert the question text here - as long as its not null 
    fprintf(stderr, "Question String : %s\n", self->last_question->question);
    char question[2000]; 
    sprintf(question, "%s", self->last_question->question);
    question_label = gtk_label_new(question);//"Please answer the question");
    gtk_box_pack_start (GTK_BOX (hbox), question_label, TRUE, TRUE, 0);
  
    button = gtk_button_new_with_label("YES");
    gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 5);
  
    gtk_signal_connect(GTK_OBJECT(button), "clicked", 
                       (GtkSignalFunc)yes_answer_button, self);
  
    button = gtk_button_new_with_label("NO");
    gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 5);		
  
    gtk_signal_connect(GTK_OBJECT(button), "clicked", 
                       (GtkSignalFunc)no_answer_button, self);

    button = gtk_button_new_with_label("Invalid");
    gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 5);		
  
    gtk_signal_connect(GTK_OBJECT(button), "clicked", 
                       (GtkSignalFunc)invalid_answer_button, self);
    
    /*gtk_signal_connect_object(GTK_OBJECT(button),
                              "clicked", (GtkSignalFunc)gtk_widget_destroy, 
                              (gpointer)self->placename_dialog);	
    */

    gtk_widget_show_all(self->question_dialog);
}

static void on_slam_question (const lcm_recv_buf_t *rbuf, const char *channel,
                              const slam_language_question_t *msg, void *user)
{
    RendererRegionTopology *self = (RendererRegionTopology *)user;
    g_assert(self);
    if(self->last_question){
        slam_language_question_t_destroy(self->last_question);
    }
    self->last_question = slam_language_question_t_copy(msg);
    show_question_dialog(self);

    //lets do a pop up with a yes/no buttons 
    //bot_viewer_request_redraw (self->viewer);
}

static void on_laser_scan_list (const lcm_recv_buf_t *rbuf, const char *channel,
                           const slam_laser_pose_list_t *msg, void *user)
{
    RendererRegionTopology *self = (RendererRegionTopology *)user;
    g_assert(self);
    for(int i=0; i < msg->no_poses; i++){
        slam_laser_pose_t *n_lp = &msg->scans[i]; 
        slam_laser_pose_t *laser_pose = (slam_laser_pose_t *) g_hash_table_lookup(self->node_scans, &n_lp->id);
        if(!laser_pose){
            laser_pose = slam_laser_pose_t_copy(n_lp);
            g_hash_table_insert(self->node_scans, &(laser_pose->id), laser_pose);
        }
    }
    self->requested_scans = 0;
    bot_viewer_request_redraw (self->viewer);
}


static void clear_region_points(RendererRegionTopology *self){
    GHashTableIter iter;
    gpointer key, value;
    g_hash_table_iter_init (&iter, self->node_scans);

    fprintf(stderr, "Slam reset - clearing the old pose info\n");
    while (g_hash_table_iter_next (&iter, &key, &value)){
        slam_laser_pose_t *laser_pose = (slam_laser_pose_t *) g_hash_table_lookup(self->node_scans, key);
        slam_laser_pose_t_destroy(laser_pose);
    }

    g_hash_table_remove_all(self->node_scans);

    //destory the list 
    g_list_free_full (self->particle_list, destroy_region_particle);
    self->particle_list = NULL;
}

static void publish_updated_transform(RendererRegionTopology *self){
    if(self->last_slam_transform == NULL)
        return;

    int to_match = self->current_ind;

    erlcm_rigid_transform_list_t *msg = self->last_slam_transform;

    //find the matching transform and publish it out 
    for(int i=0; i < msg->num; i++){
        if(msg->list[i].id  == to_match){
            bot_core_rigid_transform_t_publish (self->lcm, "GLOBAL_TO_LOCAL", &msg->list[i].transform); 
            return;
        }        
    }
}

static void on_language_update (const lcm_recv_buf_t *rbuf, const char *channel,
                                const slam_complex_language_collection_list_t *msg, void *user)
{
    RendererRegionTopology *self = (RendererRegionTopology *)user;
    g_assert(self);
    
    if(self->language_update){
        slam_complex_language_collection_list_t_destroy(self->language_update);
    }

    self->language_update = slam_complex_language_collection_list_t_copy(msg);

    int max_events = 0;
    for(int i=0; i < self->language_update->no_particles; i++){
        slam_complex_language_collection_t *collection = &self->language_update->collection[i];
        if(collection->count > max_events){
            max_events = collection->count;
        }
    }
    if(max_events > 0){
        if(max_events >1)
            bot_gtk_param_widget_set_enabled (self->lg_pw, PARAM_COMPLEX_LANGUAGE_EVENT_INDEX, 1);
        int last_index = bot_gtk_param_widget_get_int (self->lg_pw, PARAM_COMPLEX_LANGUAGE_EVENT_INDEX);
        if(last_index > (max_events -1)){
            last_index = 0;
        }
        bot_gtk_param_widget_modify_int(self->lg_pw, PARAM_COMPLEX_LANGUAGE_EVENT_INDEX, 0, fmax(1,max_events-1), 1,last_index);
    }
    else{
        bot_gtk_param_widget_modify_int(self->lg_pw, PARAM_COMPLEX_LANGUAGE_EVENT_INDEX, 0, 1, 1, 0);
        bot_gtk_param_widget_set_enabled (self->lg_pw, PARAM_COMPLEX_LANGUAGE_EVENT_INDEX, 0);
    }
}

static void on_slam_transforms (const lcm_recv_buf_t *rbuf, const char *channel,
                                const erlcm_rigid_transform_list_t *msg, void *user)
{
    RendererRegionTopology *self = (RendererRegionTopology *)user;
    g_assert(self);

    if(self->last_slam_transform != NULL){
        erlcm_rigid_transform_list_t_destroy(self->last_slam_transform);
    }
    
    self->last_slam_transform = erlcm_rigid_transform_list_t_copy(msg);
    
    publish_updated_transform(self);
}


static void on_slam_status (const lcm_recv_buf_t *rbuf, const char *channel,
                            const slam_status_t *msg, void *user)
{
    RendererRegionTopology *self = (RendererRegionTopology *)user;
    g_assert(self);
    clear_region_points(self);
    
    fprintf(stderr, "Clearing class Maps\n");

    if(self->language_update){
        slam_complex_language_collection_list_t_destroy(self->language_update);
        self->language_update = NULL;
    }

    if(self->last_question){
        slam_language_question_t_destroy(self->last_question);
        self->last_question = NULL;
    }

    if(self->question_dialog){
        gtk_widget_destroy(self->question_dialog);
        self->question_dialog = NULL;
    }
    
    bot_gtk_param_widget_modify_int(self->lg_pw, PARAM_COMPLEX_LANGUAGE_EVENT_INDEX, 0, 1, 1, 0);
    bot_gtk_param_widget_set_enabled (self->lg_pw, PARAM_COMPLEX_LANGUAGE_EVENT_INDEX, 0);
     
    /*clear_class_maps(self->type_map);
    clear_class_maps(self->label_map);
    clear_class_maps(self->appearance_map);*/

    fprintf(stderr, "Slam reset - clearing the old pose info - Finished\n");    
    bot_viewer_request_redraw (self->viewer);
}



static void
renderer_advanced_topological_graph_destroy (BotRenderer *renderer)
{
    if (!renderer)
        return;

    RendererRegionTopology *self = (RendererRegionTopology *) renderer->user;
    if (!self)
        return;
    
    free (self);
}

static void 
renderer_advanced_topological_graph_draw (BotViewer *viewer, BotRenderer *renderer)
{
    RendererRegionTopology *self = (RendererRegionTopology*)renderer->user;
    g_assert(self);

    if(g_list_length (self->particle_list) == 0)
        return;

    int draw_map_ind = 0;
    if(self->active_particle >=0){
        draw_map_ind = self->active_particle;
    }    
        
    int num_particles = g_list_length (self->particle_list);
        
    int max_prob_id = 0; 
    int min_prob_id = 0;
    int count = 0;
        
    char prob_status[100];
    char *full_status = calloc(num_particles *100 , sizeof(char));

    GList * sorted_list = NULL;

    GHashTable *complex_language =  g_hash_table_new(g_int_hash, g_int_equal);
    if(self->params.draw_complex_language_paths && self->language_update){
        for(int i=0; i < self->language_update->no_particles; i++){
            slam_complex_language_collection_t *collection = &self->language_update->collection[i];
            g_hash_table_insert(complex_language, &(collection->particle_id), collection);
        }
    }

    if(self->params.particle_ordering_mode == 0){
        for(guint i=0; i <  g_list_length (self->particle_list); i++){
            slam_graph_region_particle_t *p = g_list_nth_data (self->particle_list, i);
            sorted_list = g_list_insert_sorted (sorted_list, p, compare_region_prob);
        }
    }
    else if(self->params.particle_ordering_mode == 1){
        for(guint i=0; i <  g_list_length (self->particle_list); i++){
            slam_graph_region_particle_t *p = g_list_nth_data (self->particle_list, i);
            sorted_list  = g_list_insert_sorted (sorted_list , p, compare_graph_id);
        }
    }

    if(sorted_list == NULL){
        fprintf(stderr, "Error - No particles in sorted list\n");
        return;
    }
        
    for(guint i=0; i <  g_list_length (sorted_list); i++){
        slam_graph_region_particle_t *p = g_list_nth_data (sorted_list, i);
        slam_graph_region_particle_t *p_max = g_list_nth_data (sorted_list, max_prob_id);
        slam_graph_region_particle_t *p_min = g_list_nth_data (sorted_list, min_prob_id);
            
        double prob = p->weight;
        double max_prob = p_max->weight;
        double min_prob = p_min->weight;
        if(!self->params.log_scale){
            prob = exp(prob);
            max_prob = exp(max_prob);
            min_prob = exp(min_prob);
        }
        count++;
        //sprintf(prob_status, "[%d] : %.3f\n", (int) p->id, prob);
        //strcat( full_status, prob_status);
        if(prob > max_prob)
            max_prob_id = i;
        else if(prob < min_prob)
            min_prob_id = i;
    }

    slam_graph_region_particle_t *valid_map = g_list_nth_data (sorted_list, draw_map_ind);

    /*if(self->last_sent_particle_id != valid_map->id){
        fprintf(stderr, "Requesting new map\n");
        self->last_sent_particle_id = valid_map->id;
        send_map_request(self, valid_map->id);
        }*/
          
    slam_graph_region_particle_t *p_max = g_list_nth_data (sorted_list, max_prob_id);
    slam_graph_region_particle_t *p_min = g_list_nth_data (sorted_list, min_prob_id);
        
    double min_prob = p_min->weight; 
    double max_prob = p_max->weight; 
        
    if(!self->params.log_scale){
        max_prob = exp(max_prob);
        min_prob = exp(min_prob);
    }

    if(count == 0){
        g_list_free (sorted_list);
        return;
    }
    double scale = 1.0;
        
    if(max_prob > min_prob){
        scale = 0.8/ (max_prob - min_prob);
    }

    int last_valid_ind = self->current_ind;

    if(self->params.draw_all_graphs || self->params.draw_max_map){
        self->current_ind = p_max->id;
    }
    if(self->params.draw_valid_maps){
        self->current_ind = valid_map->id;
    }

    /*if(self->last_sent_particle_id != self->current_ind){
        fprintf(stderr, "Requesting new map\n");
        self->last_sent_particle_id = self->current_ind;
        send_map_request(self, self->current_ind);
        }*/

    if(self->current_ind != last_valid_ind){
        publish_updated_transform(self);
    }

    
    if(self->params.draw_performance && self->performance){
        double delta1 = -(self->performance->last_node_in_graph_utime - self->performance->last_node_utime) / 1.0e6;
        double delta2 = -(self->performance->last_node_in_graph_utime - self->performance->last_sensor_utime) / 1.0e6;
        sprintf(prob_status, "Current [%d]\n%.3f %.3f\n", (int) valid_map->id, delta1, delta2);
    }
    else{
        sprintf(prob_status, "Current [%d]\n", (int) valid_map->id);
    }
    strcat( full_status, prob_status);
    if(self->params.draw_stats){
        for(guint k=0; k <  g_list_length (sorted_list); k++){
            slam_graph_region_particle_t *p = g_list_nth_data (sorted_list, k); 
            if(valid_map == p){
                sprintf(prob_status, "Cl:%d- S: %d \n-> F:%d \nA:%.3f (M:%d)\n", p->no_close_node_pairs, 
                        p->no_same_region_close_pairs, p->no_failed_close_node_pairs, p->average_distance_of_close_node_pairs, 
                        p->max_dist_of_close_node_pairs);
                strcat( full_status, prob_status);
                /*fprintf(stderr, "Particle : %d Stat: Cl %d - Same : %d \n-> Failed : %d Avg : %.3f (Max : %d)\n", 
                        (int) valid_map->id,
                        p->no_close_node_pairs, 
                        p->no_same_region_close_pairs, p->no_failed_close_node_pairs, p->average_distance_of_close_node_pairs, 
                        p->max_dist_of_close_node_pairs);*/
                break;
            }
        }
       
    }
    for(guint i=0; i <  g_list_length (sorted_list); i++){
        slam_graph_region_particle_t *p = g_list_nth_data (sorted_list, i);
        double prob = p->weight;
        if(!self->params.log_scale){
            prob = exp(prob);
        }
        sprintf(prob_status, "[%d] : %.3f\n", (int) p->id, prob);
        strcat( full_status, prob_status);
    }
    
    char label[1042];

    if(!self->params.draw_diff){//!bot_gtk_param_widget_get_bool(self->pw, PARAM_COMPARE_PARTICLE_HISTORY)){
        if(self->params.draw_all_graphs){
            for(guint k=0; k <  g_list_length (sorted_list); k++){
                //draw this particle if it's valid 
                slam_graph_region_particle_t *p = g_list_nth_data (sorted_list, k);     
                slam_complex_language_collection_t *collection = (slam_complex_language_collection_t *) g_hash_table_lookup(complex_language, &(p->id));
                draw_region_graph_particle(self, p, collection, k, min_prob, draw_map_ind, scale, num_particles);
            }
        }
        else if(self->params.draw_valid_maps){
            for(guint k=0; k <  g_list_length (sorted_list); k++){
                slam_graph_region_particle_t *p = g_list_nth_data (sorted_list, k); 
                if(valid_map == p){
                    slam_complex_language_collection_t *collection = (slam_complex_language_collection_t *) g_hash_table_lookup(complex_language, &(p->id));
                    draw_region_graph_particle(self, p, collection, k, min_prob, draw_map_ind, scale, num_particles); 
                    break;
                }
            }
        }
        else if(self->params.draw_max_map){
            for(guint k=0; k <  g_list_length (sorted_list); k++){
                slam_graph_region_particle_t *p = g_list_nth_data (sorted_list, k); 
                if(p_max == p){
                    slam_complex_language_collection_t *collection = (slam_complex_language_collection_t *) g_hash_table_lookup(complex_language, &(p->id));
                    draw_region_graph_particle(self, p, collection, k, min_prob, draw_map_ind, scale, num_particles); 
                    break;
                }
            }
        }
    }
    else{
        //fprintf(stderr, "Redrawing\n");
        if(self->diff_particle){
            //we need to do something funcky to change from the last particle to the next particle 
            draw_region_graph_particle(self, self->diff_particle, NULL, 0, min_prob, draw_map_ind, scale, num_particles); 
        }
    }

    g_hash_table_destroy(complex_language);

    g_list_free (sorted_list);

    double class_xyz[] = {150, 90, 100};
    /*bot_gl_draw_text(class_xyz, NULL, full_status,
      BOT_GL_DRAW_TEXT_JUSTIFY_CENTER |
      BOT_GL_DRAW_TEXT_ANCHOR_VCENTER |
      BOT_GL_DRAW_TEXT_ANCHOR_HCENTER |
      BOT_GL_DRAW_TEXT_DROP_SHADOW);*/

    /*glPushAttrib (GL_DEPTH_BUFFER_BIT | GL_POINT_BIT | GL_CURRENT_BIT);
      glEnable (GL_DEPTH_TEST);
      glEnable(GL_BLEND);
        
      glEnable( GL_POINT_SMOOTH );
      glEnable( GL_BLEND );
      glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
      glPointSize( 8.0 );

      glColor3f(1,0,0);
      glPointSize( 20.0 );
      glBegin(GL_POINTS);
      glVertex3d(150, 90, 100);
      glEnd();
      int c_count = 0; 

      glBegin(GL_POLYGON);
      glVertex3d(150, 90 - height *c_count, 100);
      glVertex3d(150 + height, 90 - height *c_count, 100);
      glVertex3d(150 + height, 90 - height *(c_count+1), 100);
      glVertex3d(150 , 90 - height *(c_count+1), 100);
      glEnd();

      glPopAttrib();*/

    // Render the current robot status
    GLint viewport[4];
    glGetIntegerv (GL_VIEWPORT, viewport);
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluOrtho2D(0, viewport[2], 0, viewport[3]);

    glColor3f(1,1,1);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

   
    if(self->params.draw_legand){
        GHashTableIter iter;
        gpointer key, value;

        int no_labels = g_hash_table_size(self->label_map);
        if(self->params.draw_semantic_label || self->params.draw_semantic_label_max){
            double x_laser = 200;
            double height = 15;
            double y_laser = height * no_labels;//MAX_LABEL_TYPES;
            int c_count = 0; 
            char c_name[100];
            int class_size =  no_labels;//MAX_LABEL_TYPES; //g_hash_table_size(self->sorted_classes);
            //while (g_hash_table_iter_next (&iter, &key, &value)){
            //class_index_t *class_id =  (class_index_t *) value;
            for(int i=0; i < no_labels; i++){
                //fprintf(stderr, "Laser Class : %d - %s\n", class_id->id, get_class_name_from_id(class_id->id));
                sprintf(c_name, "%s", get_label_name_from_id(self, i));
                double pos[3] = {x_laser - 20, y_laser - height *(c_count - 0.5) , 100};
                glColor3f(0,0,0);
                bot_gl_draw_text(pos, NULL, c_name,
                                 BOT_GL_DRAW_TEXT_JUSTIFY_LEFT |
                                 BOT_GL_DRAW_TEXT_ANCHOR_RIGHT);// |
                //BOT_GL_DRAW_TEXT_ANCHOR_VL |
                //BOT_GL_DRAW_TEXT_ANCHOR_HCENTER |
                //BOT_GL_DRAW_TEXT_DROP_SHADOW);
                //glColor3f(1,0,0);
                if(self->params.use_c_util){
                    glColor3fv(bot_color_util_jet(get_color_ratio_from_label_id(self, i)));
                }
                else{
                    glColor3fv(renderer_color_util_jet(i));
                }
                glPointSize( 10.0 );
                glBegin(GL_POINTS);
                glVertex2d(x_laser, y_laser - height * c_count);//, 100);
                glEnd();
                c_count++;
            }
        }

        if(self->params.draw_laser_class || self->params.draw_laser_class_max || self->params.draw_image_class || self->params.draw_image_class_max){
            double x_laser = 200;
            g_hash_table_iter_init (&iter, self->appearance_map);//self->sorted_classes);
            double height = 15;
            double y_laser = height * g_hash_table_size(self->appearance_map) + 100;
            int c_count = 0; 
            char c_name[100];
            int class_size =  g_hash_table_size(self->appearance_map);
            while (g_hash_table_iter_next (&iter, &key, &value)){
                class_map_t *class_id =  (class_map_t *) value;
                //fprintf(stderr, "Laser Class : %d - %s\n", class_id->id, get_class_name_from_id(class_id->id));
                sprintf(c_name, "%s", class_id->name);
                double pos[3] = {x_laser - 20, y_laser - height *(c_count - 0.5) , 100};
                glColor3f(0,0,0);
                bot_gl_draw_text(pos, NULL, c_name,
                                 BOT_GL_DRAW_TEXT_JUSTIFY_LEFT |
                                 BOT_GL_DRAW_TEXT_ANCHOR_RIGHT);
                if(self->params.use_c_util){
                    glColor3fv(bot_color_util_jet(get_color_ratio_from_appearance_id(self, class_id->id)));
                }
                else{
                    glColor3fv(renderer_color_util_jet(class_id->id));
                }
                glPointSize( 10.0 );
                glBegin(GL_POINTS);
                glVertex2d(x_laser, y_laser - height * c_count);//, 100);
                glEnd();
                c_count++;
            }
        }
    
        if(self->params.draw_semantic_class || self->params.draw_semantic_class_max){
            double x_laser = 200;
            g_hash_table_iter_init (&iter, self->type_map);//self->sorted_classes);
            double height = 15;
            double y_laser = height * g_hash_table_size(self->type_map) + 100;
            int c_count = 0; 
            char c_name[100];
            int class_size =  g_hash_table_size(self->type_map);
            while (g_hash_table_iter_next (&iter, &key, &value)){
                class_map_t *class_id =  (class_map_t *) value;
                //fprintf(stderr, "Laser Class : %d - %s\n", class_id->id, get_class_name_from_id(class_id->id));
                sprintf(c_name, "%s", class_id->name);
                double pos[3] = {x_laser - 20, y_laser - height *(c_count - 0.5) , 100};
                glColor3f(0,0,0);
                bot_gl_draw_text(pos, NULL, c_name,
                                 BOT_GL_DRAW_TEXT_JUSTIFY_LEFT |
                                 BOT_GL_DRAW_TEXT_ANCHOR_RIGHT);// |
                //BOT_GL_DRAW_TEXT_ANCHOR_VL |
                //BOT_GL_DRAW_TEXT_ANCHOR_HCENTER |
                //BOT_GL_DRAW_TEXT_DROP_SHADOW);
                //glColor3f(1,0,0);
                if(self->params.use_c_util){
                    glColor3fv(bot_color_util_jet(get_color_ratio_from_class_id(self, class_id->id)));
                }
                else{
                    glColor3fv(renderer_color_util_jet(class_id->id));
                }
                glPointSize( 10.0 );
                glBegin(GL_POINTS);
                glVertex2d(x_laser, y_laser - height * c_count);//, 100);
                glEnd();
                c_count++;
            }
        }
    

        glColor3f(1,1,1);
        double state_xyz[] = {50, 90, 100};
        bot_gl_draw_text(state_xyz, NULL, full_status,
                         BOT_GL_DRAW_TEXT_JUSTIFY_CENTER |
                         BOT_GL_DRAW_TEXT_ANCHOR_VCENTER |
                         BOT_GL_DRAW_TEXT_ANCHOR_HCENTER |
                         BOT_GL_DRAW_TEXT_DROP_SHADOW);
        free(full_status);
    }
    glLoadIdentity();

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
}

static void activate(RendererRegionTopology *self, int type)
{
    self->active = type;
    if(type==0){
        fprintf(stderr,"Reset.\n");
    }
    if(type==1){
        fprintf(stderr,"Ready for first click\n");
    }
    if(type==2){
        fprintf(stderr,"Ready for second click\n");
    }
    if(type==3){
        fprintf(stderr,"Ready to publish\n");
    }    
}



static void send_constraint (BotViewer *viewer, BotEventHandler *ehandler)
{
    RendererRegionTopology *self = (RendererRegionTopology*) ehandler->user;
    //fprintf(stderr, "ready to calculate constraint!\n");
   
    fprintf(stderr, "active particle: %d\n", self->active_particle);
    if(self->active_particle == -1 || self->active_particle > g_list_length(self->particle_list)-1)
        return;
      
    slam_graph_region_particle_t *p = g_list_nth_data (self->particle_list, self->active_particle);
    fprintf(stderr, "--------------------------------------------------------\nParticle index %d (id %d)\n>First node: %d\n>Second node: %d\n",(int)self->active_particle, (int)p->id, self->node_1, self->node_2);
   
    if(self->active != 3)
        return;
   
    slam_language_edge_t msg;
    //msg.particle_id = self->active_particle;  -- this is actually the index
    slam_graph_region_particle_t *particle = g_list_nth_data (self->particle_list, self->active_particle);
    msg.particle_id = particle->id;
    msg.node_id_1 = self->node_1;
    msg.node_id_2 = self->node_2;
    //double trsfm[3] = {0,0,0};
    //double cv[9] = {1,0,0, 0,1,0, 0,0,100};
    msg.transformation[0] = 0;
    msg.transformation[1] = 0;
    msg.transformation[2] = 0;
    msg.cov[0] = .5;
    msg.cov[1] = 0;
    msg.cov[2] = 0;
   
    msg.cov[3] = 0;
    msg.cov[4] = .5;
    msg.cov[5] = 0;
   
    msg.cov[6] = 0;
    msg.cov[7] = 0;
    msg.cov[8] = 10;
   
    msg.type = (int) bot_gtk_param_widget_get_enum(self->pw, PARAM_CONSTRAINT_TYPE);
    slam_language_edge_t_publish(self->lcm, "LANGUAGE_EDGE", &msg);
}

static void highlight_node(BotViewer *viewer, BotEventHandler *ehandler, int endpoint)
{
    RendererRegionTopology *self = (RendererRegionTopology*) ehandler->user;
    fprintf(stderr, "Color node %d!\n", endpoint);
   
    double *xy;
   
    if(endpoint==1)
        xy = self->xy_first;
    else xy = self->xy_second;
    double min_dist = HUGE;
    int node = -1;
    //fprintf(stderr, "initialized variables!\n");
    //fprintf(stderr, "active particle: %d\n", self->active_particle);
    if(self->active_particle == -1 || self->active_particle > g_list_length(self->particle_list)-1)
        return;
      
    slam_graph_region_particle_t *p = g_list_nth_data (self->particle_list, self->active_particle);
   
    //fprintf(stderr, "got particle! %p\n", (void *)p);
   
    //fprintf(stderr, "num nodes: %d\n", (int) p->no_nodes);
    
    for(int i=0; i < p->no_regions; i++){
        slam_graph_region_t *region = &p->region_list[i];
        
        for(int j=0; j< region->count; j++){
            double *temp = region->nodes[j].xy; 
            double dist = sqrt(pow(temp[0] - xy[0],2)+pow(temp[1]-xy[1],2));
            if(dist < min_dist){
                min_dist = dist;
                node = i;
            }
        }
    }
    if(endpoint==1)
        self->node_1 = node;
    else self->node_2 = node;
    fprintf(stderr, "--------------------------------------------------------\nParticle index %d (id %d)\n>First node: %d\n>Distance one: %f\n",(int)self->active_particle, (int)p->id, node, min_dist);
}

static int mouse_press (BotViewer *viewer, BotEventHandler *ehandler,
                        const double ray_start[3], const double ray_dir[3], 
                        const GdkEventButton *event)
{
    RendererRegionTopology *self = (RendererRegionTopology*) ehandler->user;

    double xy[2];
    int consumed = 0;

    geom_ray_z_plane_intersect_3d(POINT3D(ray_start), POINT3D(ray_dir), 
                                  0, POINT2D(xy));

    if(self->active == 1){
        activate(self,2);
        self->xy_first = xy;
        highlight_node(self->viewer, &self->ehandler, 1);
    }
    else if(self->active == 2){
        self->xy_second = xy;
        highlight_node(self->viewer, &self->ehandler,2);
        activate(self,3);
    }

    bot_viewer_request_redraw(viewer);

    return consumed;
}

static void update_region_params(RendererRegionTopology *self){

    self->params.particle_ordering_mode = bot_gtk_param_widget_get_enum(self->pw,PARAM_ORDER_PARTICLES);
    self->params.draw_complex_language = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_COMPLEX_LANGUAGE);
    self->params.draw_all_graphs = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_ALL_GRAPHS);
    self->params.draw_max_map = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_MAX_GRAPH);
    self->params.draw_valid_maps = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_VALID_GRAPH);
    self->params.distance_scale = bot_gtk_param_widget_get_int(self->pw, PARAM_DISTANCE_SCALE);
    self->params.equal_dist = bot_gtk_param_widget_get_bool(self->pw, PARAM_DRAW_EQUAL_DISTANCE);
    self->params.draw_height = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_HEIGHT);

    self->params.use_c_util = bot_gtk_param_widget_get_bool (self->gp_pw, PARAM_USE_AUTO_COLOR);
    self->params.draw_region_connections = bot_gtk_param_widget_get_bool (self->gp_pw, PARAM_DRAW_REGION_CONNECTIONS);
    self->params.draw_inter_region_edges = bot_gtk_param_widget_get_bool (self->gp_pw, PARAM_DRAW_INTER_REGION_EDGES);
    self->params.draw_intra_region_edges = bot_gtk_param_widget_get_bool (self->gp_pw, PARAM_DRAW_INTRA_REGION_EDGES);
    self->params.draw_edges_in_black = bot_gtk_param_widget_get_bool (self->gp_pw, PARAM_DRAW_EDGES_IN_BLACK);
    self->params.draw_nodes = bot_gtk_param_widget_get_bool(self->gp_pw, PARAM_DRAW_NODES);
    self->params.draw_regions = bot_gtk_param_widget_get_bool(self->gp_pw, PARAM_DRAW_REGIONS);
    self->params.draw_region_mean = bot_gtk_param_widget_get_bool(self->gp_pw, PARAM_DRAW_REGION_MEAN);

    self->params.draw_label_name = bot_gtk_param_widget_get_bool (self->sc_pw, PARAM_DRAW_LABEL_NAME);
    self->params.draw_all_max_labels = bot_gtk_param_widget_get_bool (self->sc_pw, PARAM_DRAW_ALL_MAX_LABELS);
    self->params.draw_legand = bot_gtk_param_widget_get_bool (self->sc_pw, PARAM_DRAW_LEGAND);
    self->params.draw_performance = bot_gtk_param_widget_get_bool (self->sc_pw, PARAM_DRAW_PERFORMANCE);
    self->params.draw_stats = bot_gtk_param_widget_get_bool (self->sc_pw, PARAM_DRAW_STATS);
    self->params.draw_objects = bot_gtk_param_widget_get_bool (self->sc_pw, PARAM_DRAW_OBJECTS);
    self->params.draw_without_lang = bot_gtk_param_widget_get_bool (self->sc_pw, PARAM_DRAW_NO_LANG_DIST);
    self->params.draw_without_answers = bot_gtk_param_widget_get_bool (self->sc_pw, PARAM_DRAW_NO_ANSWERS_DIST);    
    self->params.draw_semantic_label = bot_gtk_param_widget_get_bool (self->sc_pw, PARAM_DRAW_REGION_LABEL);
    self->params.draw_semantic_label_max = bot_gtk_param_widget_get_bool (self->sc_pw, PARAM_DRAW_REGION_LABEL_MAX);
    self->params.draw_semantic_label_valid = bot_gtk_param_widget_get_bool (self->sc_pw, PARAM_DRAW_REGION_LABEL_VALID);
    self->params.draw_semantic_label_invalid_black = bot_gtk_param_widget_get_bool (self->sc_pw, PARAM_DRAW_REGION_LABEL_INVALID);
    self->params.draw_bar_graph = bot_gtk_param_widget_get_bool (self->sc_pw, PARAM_DRAW_REGION_LABEL_BAR_GRAPH);
    self->params.draw_semantic_class = bot_gtk_param_widget_get_bool (self->sc_pw, PARAM_DRAW_REGION_SEMANTICS);
    self->params.draw_semantic_class_max = bot_gtk_param_widget_get_bool (self->sc_pw, PARAM_DRAW_REGION_SEMANTICS_MAX);
    self->params.draw_node_class = bot_gtk_param_widget_get_bool(self->sc_pw, PARAM_DRAW_NODE_CLASSIFICATION);
    self->params.draw_node_class_max = bot_gtk_param_widget_get_bool(self->sc_pw, PARAM_DRAW_NODE_CLASSIFICATION_MAX);
    self->params.draw_laser_class = bot_gtk_param_widget_get_bool(self->sc_pw, PARAM_DRAW_LASER_CLASSIFICATION);
    self->params.draw_laser_class_max = bot_gtk_param_widget_get_bool(self->sc_pw, PARAM_DRAW_LASER_CLASSIFICATION_MAX);
    self->params.draw_image_class = bot_gtk_param_widget_get_bool(self->sc_pw, PARAM_DRAW_IMAGE_CLASSIFICATION);
    self->params.draw_image_class_max = bot_gtk_param_widget_get_bool(self->sc_pw, PARAM_DRAW_IMAGE_CLASSIFICATION_MAX);
    //self->params.draw_max_label = bot_gtk_param_widget_get_bool (self->sc_pw, PARAM_DRAW_MAX_LABEL);

    self->params.draw_complex_language_paths = bot_gtk_param_widget_get_bool (self->lg_pw, PARAM_DRAW_COMPLEX_LANGUAGE_PATHS);
    self->params.draw_complex_language_event_index = bot_gtk_param_widget_get_int (self->lg_pw, PARAM_COMPLEX_LANGUAGE_EVENT_INDEX);
    self->params.draw_all_complex_language_paths = bot_gtk_param_widget_get_bool (self->lg_pw, PARAM_DRAW_ALL_COMPLEX_LANGUAGE_PATHS);
    self->params.draw_fig_prob = bot_gtk_param_widget_get_bool (self->lg_pw, PARAM_DRAW_FIG_PROB);
    self->params.ignore_prior = bot_gtk_param_widget_get_bool (self->lg_pw, PARAM_IGNORE_PRIOR);
    self->params.path_index = bot_gtk_param_widget_get_int (self->lg_pw, PARAM_COMPLEX_PATH_INDEX);
    self->params.draw_answer_prob = bot_gtk_param_widget_get_bool (self->lg_pw, PARAM_DRAW_ANSWER_PROB);
    self->params.draw_original_prob = bot_gtk_param_widget_get_bool (self->lg_pw, PARAM_DRAW_ORIG_PROB);
    self->params.answer_ind = bot_gtk_param_widget_get_int (self->lg_pw, PARAM_DRAW_ANSWER_IND);
    self->params.draw_landmarks = bot_gtk_param_widget_get_bool (self->lg_pw, PARAM_DRAW_LANDMARKS);

    self->params.draw_prob = bot_gtk_param_widget_get_bool (self->gi_pw, PARAM_DISP_PROB);
    self->params.log_scale = bot_gtk_param_widget_get_bool (self->gi_pw, PARAM_DRAW_LOG);
    self->params.bounding_boxes = bot_gtk_param_widget_get_bool(self->gi_pw, PARAM_BOUNDING_BOXES);
    self->params.draw_cov = bot_gtk_param_widget_get_bool (self->gi_pw, PARAM_DRAW_COV);
    self->params.draw_region_ids = bot_gtk_param_widget_get_bool(self->gi_pw, PARAM_DRAW_REGION_ID);
    self->params.draw_node_ids = bot_gtk_param_widget_get_bool(self->gi_pw, PARAM_DRAW_NODE_ID);
    self->params.draw_segment_ids = bot_gtk_param_widget_get_bool(self->gi_pw, PARAM_DRAW_SEGMENT_ID);
    self->params.draw_dead_edges = bot_gtk_param_widget_get_bool (self->gi_pw, PARAM_DRAW_DEAD_EDGES);
    self->params.draw_odom = bot_gtk_param_widget_get_bool (self->gi_pw, PARAM_DRAW_ODOM);
    self->params.remap_regions = bot_gtk_param_widget_get_bool(self->gi_pw, PARAM_REMAP_REGIONS);
    self->params.color_regions = bot_gtk_param_widget_get_bool(self->gi_pw, PARAM_COLOR_REGIONS);
    self->params.edge_thickness = bot_gtk_param_widget_get_int (self->gi_pw, PARAM_EDGE_THICKNESS);
    self->params.line_thickness = bot_gtk_param_widget_get_int (self->gi_pw, PARAM_LINE_THICKNESS);
    self->params.bar_graph_scale = bot_gtk_param_widget_get_double( self->gi_pw, PARAM_BAR_GRAPH_SCALE);
    self->params.node_radius = bot_gtk_param_widget_get_double( self->gi_pw, PARAM_NODE_RADIUS);

    self->params.draw_side_by_side = bot_gtk_param_widget_get_bool(self->gc_pw, PARAM_DRAW_SIDE_BY_SIDE); 
    self->params.g_id_1 = bot_gtk_param_widget_get_int(self->gc_pw, PARAM_DRAW_GRAPH_1_ID);
    self->params.g_id_2 = bot_gtk_param_widget_get_int(self->gc_pw, PARAM_DRAW_GRAPH_2_ID);

    self->params.draw_map_points = bot_gtk_param_widget_get_bool (self->dm_pw, PARAM_DRAW_MAP_POINTS);
}

static void
on_param_widget_changed (BotGtkParamWidget *pw, const char *name, void *user)
{
    RendererRegionTopology *self = user;
    
    update_region_params(self);    
    
    if(!strcmp(name, PARAM_ADD_CONSTRAINT_LABEL)) {
        activate(self, 1);
    }
    if(!strcmp(name, PARAM_DRAW_FIG_PROB) || !strcmp(name, PARAM_DRAW_ANSWER_PROB) || !strcmp(name, PARAM_DRAW_ORIG_PROB)){
        if(bot_gtk_param_widget_get_bool(self->lg_pw, PARAM_DRAW_FIG_PROB)){
            bot_gtk_param_widget_set_bool(self->lg_pw, PARAM_DRAW_ANSWER_PROB, 0);
            bot_gtk_param_widget_set_bool(self->lg_pw, PARAM_DRAW_ORIG_PROB, 0);
            bot_gtk_param_widget_set_enabled(self->lg_pw, PARAM_DRAW_ANSWER_IND, 0);
        }
        else if(bot_gtk_param_widget_get_bool(self->lg_pw, PARAM_DRAW_ANSWER_PROB)){
            bot_gtk_param_widget_set_bool(self->lg_pw, PARAM_DRAW_FIG_PROB, 0);
            bot_gtk_param_widget_set_bool(self->lg_pw, PARAM_DRAW_ORIG_PROB, 0);
            bot_gtk_param_widget_set_enabled(self->lg_pw, PARAM_DRAW_ANSWER_IND, 1);
        }
         else if(bot_gtk_param_widget_get_bool(self->lg_pw, PARAM_DRAW_ORIG_PROB)){
            bot_gtk_param_widget_set_bool(self->lg_pw, PARAM_DRAW_FIG_PROB, 0);
            bot_gtk_param_widget_set_bool(self->lg_pw, PARAM_DRAW_ANSWER_PROB, 0);
            bot_gtk_param_widget_set_enabled(self->lg_pw, PARAM_DRAW_ANSWER_IND, 0);
        }
        update_region_params(self);  
    }

    if(!strcmp(name, PARAM_DRAW_NODE_CLASSIFICATION)){
        if(self->params.draw_node_class ){
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_LABEL_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_SEMANTICS, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_SEMANTICS_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_LABEL_BAR_GRAPH, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_LASER_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_LASER_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_IMAGE_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_IMAGE_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_NODE_CLASSIFICATION_MAX, 0);
        }
    }
    if(!strcmp(name, PARAM_DRAW_NODE_CLASSIFICATION_MAX)){
        if(self->params.draw_node_class ){
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_LABEL_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_SEMANTICS, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_SEMANTICS_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_LABEL_BAR_GRAPH, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_LASER_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_LASER_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_IMAGE_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_IMAGE_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_NODE_CLASSIFICATION, 0);
        }
    }
    if(!strcmp(name, PARAM_DRAW_REGION_LABEL)){
        if(self->params.draw_semantic_label ){
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_LABEL_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_SEMANTICS, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_SEMANTICS_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_LASER_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_LASER_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_IMAGE_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_IMAGE_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_NODE_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_NODE_CLASSIFICATION, 0);
        }
    }
    if(!strcmp(name, PARAM_DRAW_REGION_LABEL_MAX)){
        if(self->params.draw_semantic_label_max){
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_LABEL, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_LABEL_BAR_GRAPH, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_SEMANTICS, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_SEMANTICS_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_LASER_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_LASER_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_IMAGE_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_IMAGE_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_NODE_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_NODE_CLASSIFICATION, 0);
        }
    }
    if(!strcmp(name, PARAM_DRAW_REGION_SEMANTICS)){
        if(self->params.draw_semantic_class ){
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_LABEL, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_LABEL_BAR_GRAPH, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_LABEL_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_SEMANTICS_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_LASER_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_LASER_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_IMAGE_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_IMAGE_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_NODE_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_NODE_CLASSIFICATION, 0);
        }
    }
    if(!strcmp(name, PARAM_DRAW_REGION_SEMANTICS_MAX)){
        if(self->params.draw_semantic_class_max){
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_LABEL, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_LABEL_BAR_GRAPH, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_LABEL_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_SEMANTICS, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_LASER_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_LASER_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_IMAGE_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_IMAGE_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_NODE_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_NODE_CLASSIFICATION, 0);
        }
    }
    if(!strcmp(name, PARAM_DRAW_LASER_CLASSIFICATION)){
        if(self->params.draw_laser_class){
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_LABEL, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_LABEL_BAR_GRAPH, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_LABEL_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_LASER_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_IMAGE_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_IMAGE_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_SEMANTICS, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_SEMANTICS_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_NODE_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_NODE_CLASSIFICATION, 0);
        }
    }
    if(!strcmp(name, PARAM_DRAW_LASER_CLASSIFICATION_MAX)){
        if(self->params.draw_laser_class_max){
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_LABEL, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_LABEL_BAR_GRAPH, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_LABEL_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_IMAGE_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_IMAGE_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_LASER_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_SEMANTICS, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_SEMANTICS_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_NODE_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_NODE_CLASSIFICATION, 0);
        }
    }
    if(!strcmp(name, PARAM_DRAW_IMAGE_CLASSIFICATION)){
        if(self->params.draw_image_class){
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_LABEL, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_LABEL_BAR_GRAPH, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_LABEL_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_LASER_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_LASER_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_IMAGE_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_SEMANTICS, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_SEMANTICS_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_NODE_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_NODE_CLASSIFICATION, 0);
        }
    }
    if(!strcmp(name, PARAM_DRAW_IMAGE_CLASSIFICATION_MAX)){
        if(self->params.draw_image_class_max){
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_LABEL, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_LABEL_BAR_GRAPH, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_LABEL_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_LASER_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_LASER_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_IMAGE_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_SEMANTICS, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_SEMANTICS_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_NODE_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_NODE_CLASSIFICATION, 0);
        }
    }
    if(!strcmp(name, PARAM_CLEAR_MAP)){
        clear_region_points(self);
    }
    if(!strcmp(name, VALID_MAP_IND)) {
        int raw_value = bot_gtk_param_widget_get_int(self->pw, VALID_MAP_IND);
        if(raw_value < self->no_particles){
            self->active_particle = raw_value;
            //fprintf(stderr,"Changing valid map index: %d\n", raw_value);
        }
        else{
            self->active_particle =  self->no_particles -1;
            //fprintf(stderr, "Outside the max particle no\n");
        }
    }
    if(!strcmp(name, PARAM_REQUEST_RESULT)){
        slam_particle_request_t msg; 
        msg.utime = bot_timestamp_now();
        msg.particle_id = -1;
        msg.request = SLAM_PARTICLE_REQUEST_T_SAVE_PARTICLE;
        slam_particle_request_t_publish(self->lcm, "SEMANTIC_PARTICLE_RESULT_REQUEST", &msg);
    }
    if(!strcmp(name, PARAM_CONFIRM_AND_PUBLISH)) {
        fprintf(stderr,"\nConfirm button clicked!\n");
        send_constraint(self->viewer,&self->ehandler);
        activate(self, 0);
        self->node_1 = -1;
        self->node_2 = -1;
    }
    
    if(!strcmp(name, PARAM_PUBLISH_REGION_TRANSITION)){
        fprintf(stderr, "Publishing region transition\n");
        slam_region_transition_t region_msg;
        region_msg.utime = bot_timestamp_now();
        region_msg.prob = 1; 
        region_msg.type = SLAM_REGION_TRANSITION_T_TYPE_VIEWER;
        slam_region_transition_t_publish(self->lcm, "REGION_TRANSITION", &region_msg);
    }

    if(!strcmp(name, PARAM_FINISH_SLAM)){
        fprintf(stderr, "Telling SLAM to scanmatch current region - because the log is done\n");
        slam_command_t cmd_msg;
        cmd_msg.utime = bot_timestamp_now();
        cmd_msg.command = SLAM_COMMAND_T_FINISH_SLAM; 
        cmd_msg.particle_ind = -1;
        slam_command_t_publish(self->lcm, "SLAM_COMMAND", &cmd_msg);
    }

    if(!strcmp(name, PARAM_REQUEST_MAP)){
        int draw_map_ind = bot_gtk_param_widget_get_int(pw, VALID_MAP_IND);
        
        if(g_list_length (self->particle_list) > 0){
            GList * sorted_list = NULL;

            if(self->params.particle_ordering_mode == 0){
                for(guint i=0; i <  g_list_length (self->particle_list); i++){
                    slam_graph_region_particle_t *p = g_list_nth_data (self->particle_list, i);
                    sorted_list = g_list_insert_sorted (sorted_list, p, compare_region_prob);
                }
            }
            else if(self->params.particle_ordering_mode == 1){
                for(guint i=0; i <  g_list_length (self->particle_list); i++){
                    slam_graph_region_particle_t *p = g_list_nth_data (self->particle_list, i);
                    sorted_list  = g_list_insert_sorted (sorted_list , p, compare_graph_id);
                }
            }

            if(draw_map_ind < g_list_length (self->particle_list)){
                slam_graph_region_particle_t *p = g_list_nth_data (sorted_list, draw_map_ind);
                
                if(p == NULL){
                    g_list_free (sorted_list);
                    return;
                }
                self->last_sent_particle_id = p->id;
                //fprintf(stderr, "Particle Index : %d -> List Index : %d\n", (int) p->id, draw_map_ind);
                send_map_request(self, p->id);
            }
            g_list_free (sorted_list);
        }
    }
    
    if(!strcmp(name, ADD_LANG_LABEL)){

        if (self->lang_label_filename) {
            FILE *fp = fopen (self->lang_label_filename, "a");
            fprintf (fp, "%"PRId64",%d,%d,%s\n", self->last_pose_utime,
                     (int32_t) bot_gtk_param_widget_get_enum(self->al_pw,LANG_LABEL),
                     (int32_t) bot_gtk_param_widget_get_enum(self->al_pw,LANG_CONSTRAINT_TYPE),
                     bot_gtk_param_widget_get_text_entry(self->al_pw, PARAM_LANGUAGE_UPDATE));
            fclose (fp);
        } else
            fprintf (stderr, "Error: You have to choose a file to save the language labels to\n");
    }

    if (!strcmp(name, PARAM_CREATE_LANGUAGE_FILE)) {

        GtkWidget *dialog;
        dialog = gtk_file_chooser_dialog_new("Add language labels to file", NULL,
                                             GTK_FILE_CHOOSER_ACTION_SAVE,
                                             GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                             GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
                                             NULL);
    
        if (self->lang_label_filename)
            gtk_file_chooser_set_filename (GTK_FILE_CHOOSER(dialog),
                                           self->lang_label_filename);
        
        if (gtk_dialog_run (GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT) {
            char *filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
            if (filename != NULL) {
                if (self->lang_label_filename)
                    g_free (self->lang_label_filename);
                self->lang_label_filename = g_strdup (filename);
                
                free (filename);
            }
        }
        
        gtk_widget_destroy (dialog);
    }

    bot_viewer_request_redraw (self->viewer);
}

/*
  static void update_region_params(RendererRegionTopology *self, BotGtkParamWidget *pw){

    self->params.use_c_util = bot_gtk_param_widget_get_bool (self->gp_pw, PARAM_USE_AUTO_COLOR);
    self->params.draw_region_connections = bot_gtk_param_widget_get_bool (self->gp_pw, PARAM_DRAW_REGION_CONNECTIONS);
    self->params.draw_inter_region_edges = bot_gtk_param_widget_get_bool (self->gp_pw, PARAM_DRAW_INTER_REGION_EDGES);
    self->params.draw_intra_region_edges = bot_gtk_param_widget_get_bool (self->gp_pw, PARAM_DRAW_INTRA_REGION_EDGES);
    self->params.draw_edges_in_black = bot_gtk_param_widget_get_bool (self->gp_pw, PARAM_DRAW_EDGES_IN_BLACK);
    self->params.draw_nodes = bot_gtk_param_widget_get_bool(self->gp_pw, PARAM_DRAW_NODES);
    self->params.draw_regions = bot_gtk_param_widget_get_bool(self->gp_pw, PARAM_DRAW_REGIONS);
    self->params.draw_region_mean = bot_gtk_param_widget_get_bool(self->gp_pw, PARAM_DRAW_REGION_MEAN);


    self->params.draw_label_name = bot_gtk_param_widget_get_bool (self->sc_pw, PARAM_DRAW_LABEL_NAME);
    self->params.draw_all_max_labels = bot_gtk_param_widget_get_bool (self->sc_pw, PARAM_DRAW_ALL_MAX_LABELS);
    self->params.draw_legand = bot_gtk_param_widget_get_bool (self->sc_pw, PARAM_DRAW_LEGAND);
    self->params.draw_performance = bot_gtk_param_widget_get_bool (self->sc_pw, PARAM_DRAW_PERFORMANCE);
    self->params.draw_stats = bot_gtk_param_widget_get_bool (self->sc_pw, PARAM_DRAW_STATS);
    self->params.draw_objects = bot_gtk_param_widget_get_bool (self->sc_pw, PARAM_DRAW_OBJECTS);
    self->params.draw_without_lang = bot_gtk_param_widget_get_bool (self->sc_pw, PARAM_DRAW_NO_LANG_DIST);
    self->params.draw_semantic_label = bot_gtk_param_widget_get_bool (self->sc_pw, PARAM_DRAW_REGION_LABEL);
    self->params.draw_semantic_label_max = bot_gtk_param_widget_get_bool (self->sc_pw, PARAM_DRAW_REGION_LABEL_MAX);
    self->params.draw_semantic_label_valid = bot_gtk_param_widget_get_bool (self->sc_pw, PARAM_DRAW_REGION_LABEL_VALID);
    self->params.draw_semantic_label_invalid_black = bot_gtk_param_widget_get_bool (self->sc_pw, PARAM_DRAW_REGION_LABEL_INVALID);
    self->params.draw_bar_graph = bot_gtk_param_widget_get_bool (self->sc_pw, PARAM_DRAW_REGION_LABEL_BAR_GRAPH);
    self->params.draw_semantic_class = bot_gtk_param_widget_get_bool (self->sc_pw, PARAM_DRAW_REGION_SEMANTICS);
    self->params.draw_semantic_class_max = bot_gtk_param_widget_get_bool (self->sc_pw, PARAM_DRAW_REGION_SEMANTICS_MAX);
    self->params.draw_max_label = bot_gtk_param_widget_get_bool (self->sc_pw, PARAM_DRAW_MAX_LABEL);


    self->params.draw_all_complex_language_paths = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_ALL_COMPLEX_LANGUAGE_PATHS);
    self->params.path_index = bot_gtk_param_widget_get_int (self->pw, PARAM_COMPLEX_PATH_INDEX);
    self->params.draw_landmarks = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_LANDMARKS);
    self->params.draw_complex_language_event_index = bot_gtk_param_widget_get_int (self->pw, PARAM_COMPLEX_LANGUAGE_EVENT_INDEX);
    self->params.draw_complex_language_paths = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_COMPLEX_LANGUAGE_PATHS);


    self->params.draw_complex_language = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_COMPLEX_LANGUAGE);



    
    self->params.bar_graph_scale = bot_gtk_param_widget_get_double( self->pw, PARAM_BAR_GRAPH_SCALE);
    self->params.node_radius = bot_gtk_param_widget_get_double( self->pw, PARAM_NODE_RADIUS);
    self->params.draw_odom = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_ODOM);
    self->params.draw_height = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_HEIGHT);
    
    self->params.draw_max_map = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_MAX_GRAPH);
    self->params.draw_prob = bot_gtk_param_widget_get_bool (self->pw, PARAM_DISP_PROB);
    self->params.draw_map_points = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_MAP_POINTS);
    self->params.draw_all_graphs = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_ALL_GRAPHS);
    self->params.draw_dead_edges = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_DEAD_EDGES);
    self->params.edge_thickness = bot_gtk_param_widget_get_int (self->pw, PARAM_EDGE_THICKNESS);
    self->params.line_thickness = bot_gtk_param_widget_get_int (self->pw, PARAM_LINE_THICKNESS);
    self->params.draw_valid_maps = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_VAID_GRAPH);
    self->params.draw_cov = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_COV);
    //this should be set to draw the pie charts
    //self->params.draw_pie_chart = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_LABEL_PIE_CHART);
    
    
    self->params.draw_node_ids = bot_gtk_param_widget_get_bool(self->pw, PARAM_DRAW_NODE_ID);
    self->params.draw_region_ids = bot_gtk_param_widget_get_bool(self->pw, PARAM_DRAW_REGION_ID);
    self->params.draw_segment_ids = bot_gtk_param_widget_get_bool(self->pw, PARAM_DRAW_SEGMENT_ID);
    //this should be 1 to draw supernodes
    
    self->params.bounding_boxes = bot_gtk_param_widget_get_bool(self->pw, PARAM_BOUNDING_BOXES);
    self->params.color_regions = bot_gtk_param_widget_get_bool(self->pw, PARAM_COLOR_REGIONS);
    self->params.remap_regions = bot_gtk_param_widget_get_bool(self->pw, PARAM_REMAP_REGIONS);

    self->params.log_scale = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_LOG);

    //maybe turn the others off if this is true??
    self->params.draw_side_by_side = bot_gtk_param_widget_get_bool(self->pw, PARAM_DRAW_SIDE_BY_SIDE); 
    self->params.g_id_1 = bot_gtk_param_widget_get_int(self->pw, PARAM_DRAW_GRAPH_1_ID);
    //maybe turn the others off if this is true??
    self->params.g_id_2 = bot_gtk_param_widget_get_int(self->pw, PARAM_DRAW_GRAPH_2_ID);

    self->params.particle_ordering_mode = bot_gtk_param_widget_get_enum(pw,PARAM_ORDER_PARTICLES);

    self->params.distance_scale = bot_gtk_param_widget_get_int(self->pw, PARAM_DISTANCE_SCALE);
    
    self->params.equal_dist = bot_gtk_param_widget_get_bool(self->pw, PARAM_DRAW_EQUAL_DISTANCE);

    self->params.draw_node_class = bot_gtk_param_widget_get_bool(self->pw, PARAM_DRAW_NODE_CLASSIFICATION);
    self->params.draw_node_class_max = bot_gtk_param_widget_get_bool(self->pw, PARAM_DRAW_NODE_CLASSIFICATION_MAX);
    self->params.draw_laser_class = bot_gtk_param_widget_get_bool(self->pw, PARAM_DRAW_LASER_CLASSIFICATION);
    self->params.draw_laser_class_max = bot_gtk_param_widget_get_bool(self->pw, PARAM_DRAW_LASER_CLASSIFICATION_MAX);
    self->params.draw_image_class = bot_gtk_param_widget_get_bool(self->pw, PARAM_DRAW_IMAGE_CLASSIFICATION);
    self->params.draw_image_class_max = bot_gtk_param_widget_get_bool(self->pw, PARAM_DRAW_IMAGE_CLASSIFICATION_MAX);
    //self->params.draw_class_text = bot_gtk_param_widget_get_bool(self->pw, PARAM_DRAW_CLASSIFICATION_TEXT);
}

static void
on_param_widget_changed (BotGtkParamWidget *pw, const char *name, void *user)
{
    RendererRegionTopology *self = user;
    
    update_region_params(self, pw);    
    
    if(!strcmp(name, PARAM_ADD_CONSTRAINT_LABEL)) {
        activate(self, 1);
    }
    if(!strcmp(name, PARAM_DRAW_NODE_CLASSIFICATION)){
        if(self->params.draw_node_class ){
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_LABEL_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_SEMANTICS, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_SEMANTICS_MAX, 0);
            bot_gtk_param_widget_set_bool (self->sc_pw, PARAM_DRAW_REGION_LABEL_BAR_GRAPH, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_LASER_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_LASER_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_IMAGE_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_IMAGE_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_NODE_CLASSIFICATION_MAX, 0);
        }
    }
    if(!strcmp(name, PARAM_DRAW_NODE_CLASSIFICATION_MAX)){
        if(self->params.draw_node_class ){
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_LABEL_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_SEMANTICS, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_SEMANTICS_MAX, 0);
            bot_gtk_param_widget_set_bool (self->sc_pw, PARAM_DRAW_REGION_LABEL_BAR_GRAPH, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_LASER_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_LASER_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_IMAGE_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_IMAGE_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_NODE_CLASSIFICATION, 0);
        }
    }
    if(!strcmp(name, PARAM_DRAW_REGION_LABEL)){
        if(self->params.draw_semantic_label ){
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_LABEL_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_SEMANTICS, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_SEMANTICS_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_LASER_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_LASER_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_IMAGE_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_IMAGE_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_NODE_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_NODE_CLASSIFICATION, 0);
        }
    }
    if(!strcmp(name, PARAM_DRAW_REGION_LABEL_MAX)){
        if(self->params.draw_semantic_label_max){
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_LABEL, 0);
            bot_gtk_param_widget_set_bool (self->sc_pw, PARAM_DRAW_REGION_LABEL_BAR_GRAPH, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_SEMANTICS, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_SEMANTICS_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_LASER_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_LASER_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_IMAGE_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_IMAGE_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_NODE_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_NODE_CLASSIFICATION, 0);
        }
    }
    if(!strcmp(name, PARAM_DRAW_REGION_SEMANTICS)){
        if(self->params.draw_semantic_class ){
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_LABEL, 0);
            bot_gtk_param_widget_set_bool (self->sc_pw, PARAM_DRAW_REGION_LABEL_BAR_GRAPH, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_LABEL_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_SEMANTICS_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_LASER_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_LASER_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_IMAGE_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_IMAGE_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_NODE_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_NODE_CLASSIFICATION, 0);
        }
    }
    if(!strcmp(name, PARAM_DRAW_REGION_SEMANTICS_MAX)){
        if(self->params.draw_semantic_class_max){
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_LABEL, 0);
            bot_gtk_param_widget_set_bool (self->sc_pw, PARAM_DRAW_REGION_LABEL_BAR_GRAPH, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_LABEL_MAX, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_REGION_SEMANTICS, 0);
            bot_gtk_param_widget_set_bool(self->sc_pw, PARAM_DRAW_LASER_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_LASER_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_IMAGE_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_IMAGE_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_NODE_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_NODE_CLASSIFICATION, 0);
        }
    }
    if(!strcmp(name, PARAM_DRAW_LASER_CLASSIFICATION)){
        if(self->params.draw_laser_class){
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_REGION_LABEL, 0);
            bot_gtk_param_widget_set_bool (self->pw, PARAM_DRAW_REGION_LABEL_BAR_GRAPH, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_REGION_LABEL_MAX, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_LASER_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_IMAGE_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_IMAGE_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_REGION_SEMANTICS, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_REGION_SEMANTICS_MAX, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_NODE_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_NODE_CLASSIFICATION, 0);
        }
    }
    if(!strcmp(name, PARAM_DRAW_LASER_CLASSIFICATION_MAX)){
        if(self->params.draw_laser_class_max){
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_REGION_LABEL, 0);
            bot_gtk_param_widget_set_bool (self->pw, PARAM_DRAW_REGION_LABEL_BAR_GRAPH, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_REGION_LABEL_MAX, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_IMAGE_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_IMAGE_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_LASER_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_REGION_SEMANTICS, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_REGION_SEMANTICS_MAX, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_NODE_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_NODE_CLASSIFICATION, 0);
        }
    }
    if(!strcmp(name, PARAM_DRAW_IMAGE_CLASSIFICATION)){
        if(self->params.draw_image_class){
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_REGION_LABEL, 0);
            bot_gtk_param_widget_set_bool (self->pw, PARAM_DRAW_REGION_LABEL_BAR_GRAPH, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_REGION_LABEL_MAX, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_LASER_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_LASER_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_IMAGE_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_REGION_SEMANTICS, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_REGION_SEMANTICS_MAX, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_NODE_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_NODE_CLASSIFICATION, 0);
        }
    }
    if(!strcmp(name, PARAM_DRAW_IMAGE_CLASSIFICATION_MAX)){
        if(self->params.draw_image_class_max){
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_REGION_LABEL, 0);
            bot_gtk_param_widget_set_bool (self->pw, PARAM_DRAW_REGION_LABEL_BAR_GRAPH, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_REGION_LABEL_MAX, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_LASER_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_LASER_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_IMAGE_CLASSIFICATION, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_REGION_SEMANTICS, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_REGION_SEMANTICS_MAX, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_NODE_CLASSIFICATION_MAX, 0);
            bot_gtk_param_widget_set_bool(self->pw, PARAM_DRAW_NODE_CLASSIFICATION, 0);
        }
    }
    if(!strcmp(name, PARAM_CLEAR_MAP)){
        clear_region_points(self);
    }
    if(!strcmp(name, VALID_MAP_IND)) {
        int raw_value = bot_gtk_param_widget_get_int(pw, VALID_MAP_IND);
        if(raw_value < self->no_particles){
            self->active_particle = raw_value;
            //fprintf(stderr,"Changing valid map index: %d\n", raw_value);
        }
        else{
            self->active_particle =  self->no_particles -1;
            //fprintf(stderr, "Outside the max particle no\n");
        }
    }
    if(!strcmp(name, PARAM_REQUEST_RESULT)){
        slam_particle_request_t msg; 
        msg.utime = bot_timestamp_now();
        msg.particle_id = -1;
        msg.request = SLAM_PARTICLE_REQUEST_T_SAVE_PARTICLE;
        slam_particle_request_t_publish(self->lcm, "SEMANTIC_PARTICLE_RESULT_REQUEST", &msg);
    }
    if(!strcmp(name, PARAM_CONFIRM_AND_PUBLISH)) {
        fprintf(stderr,"\nConfirm button clicked!\n");
        send_constraint(self->viewer,&self->ehandler);
        activate(self, 0);
        self->node_1 = -1;
        self->node_2 = -1;
    }
    
    if(!strcmp(name, PARAM_PUBLISH_REGION_TRANSITION)){
        fprintf(stderr, "Publishing region transition\n");
        slam_region_transition_t region_msg;
        region_msg.utime = bot_timestamp_now();
        region_msg.prob = 1; 
        region_msg.type = SLAM_REGION_TRANSITION_T_TYPE_VIEWER;
        slam_region_transition_t_publish(self->lcm, "REGION_TRANSITION", &region_msg);
    }

    if(!strcmp(name, PARAM_FINISH_SLAM)){
        fprintf(stderr, "Telling SLAM to scanmatch current region - because the log is done\n");
        slam_command_t cmd_msg;
        cmd_msg.utime = bot_timestamp_now();
        cmd_msg.command = SLAM_COMMAND_T_FINISH_SLAM; 
        cmd_msg.particle_ind = -1;
        slam_command_t_publish(self->lcm, "SLAM_COMMAND", &cmd_msg);
    }

    if(!strcmp(name, PARAM_REQUEST_MAP)){
        int draw_map_ind = bot_gtk_param_widget_get_int(self->pw, VALID_MAP_IND);
        
        if(g_list_length (self->particle_list) > 0){
            GList * sorted_list = NULL;

            if(self->params.particle_ordering_mode == 0){
                for(guint i=0; i <  g_list_length (self->particle_list); i++){
                    slam_graph_region_particle_t *p = g_list_nth_data (self->particle_list, i);
                    sorted_list = g_list_insert_sorted (sorted_list, p, compare_region_prob);
                }
            }
            else if(self->params.particle_ordering_mode == 1){
                for(guint i=0; i <  g_list_length (self->particle_list); i++){
                    slam_graph_region_particle_t *p = g_list_nth_data (self->particle_list, i);
                    sorted_list  = g_list_insert_sorted (sorted_list , p, compare_graph_id);
                }
            }

            if(draw_map_ind < g_list_length (self->particle_list)){
                slam_graph_region_particle_t *p = g_list_nth_data (sorted_list, draw_map_ind);
                
                if(p == NULL){
                    g_list_free (sorted_list);
                    return;
                }
                self->last_sent_particle_id = p->id;
                //fprintf(stderr, "Particle Index : %d -> List Index : %d\n", (int) p->id, draw_map_ind);
                send_map_request(self, p->id);
            }
            g_list_free (sorted_list);
        }
    }
    
    if(!strcmp(name, ADD_LANG_LABEL)){

        if (self->lang_label_filename) {
            FILE *fp = fopen (self->lang_label_filename, "a");
            fprintf (fp, "%"PRId64",%d,%d,%s\n", self->last_pose_utime,
                     (int32_t) bot_gtk_param_widget_get_enum(pw,LANG_LABEL),
                     (int32_t) bot_gtk_param_widget_get_enum(pw,LANG_CONSTRAINT_TYPE),
                     bot_gtk_param_widget_get_text_entry(pw, PARAM_LANGUAGE_UPDATE));
            fclose (fp);
        } else
            fprintf (stderr, "Error: You have to choose a file to save the language labels to\n");
    }

    if (!strcmp(name, PARAM_CREATE_LANGUAGE_FILE)) {

        GtkWidget *dialog;
        dialog = gtk_file_chooser_dialog_new("Add language labels to file", NULL,
                                             GTK_FILE_CHOOSER_ACTION_SAVE,
                                             GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                             GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
                                             NULL);
    
        if (self->lang_label_filename)
            gtk_file_chooser_set_filename (GTK_FILE_CHOOSER(dialog),
                                           self->lang_label_filename);
        
        if (gtk_dialog_run (GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT) {
            char *filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
            if (filename != NULL) {
                if (self->lang_label_filename)
                    g_free (self->lang_label_filename);
                self->lang_label_filename = g_strdup (filename);
                
                free (filename);
            }
        }
        
        gtk_widget_destroy (dialog);
    }

    bot_viewer_request_redraw (self->viewer);
}

 */

static void
on_load_preferences (BotViewer *viewer, GKeyFile *keyfile, void *user_data)
{
    RendererRegionTopology *self = user_data;
    bot_gtk_param_widget_load_from_key_file (self->pw, keyfile, PW_BASIC);
    bot_gtk_param_widget_load_from_key_file (self->gp_pw, keyfile, PW_GP);
    bot_gtk_param_widget_load_from_key_file (self->sc_pw, keyfile, PW_SC);
    bot_gtk_param_widget_load_from_key_file (self->lg_pw, keyfile, PW_LG);
    bot_gtk_param_widget_load_from_key_file (self->gi_pw, keyfile, PW_GI);
    bot_gtk_param_widget_load_from_key_file (self->dm_pw, keyfile, PW_DM);
    bot_gtk_param_widget_load_from_key_file (self->hi_pw, keyfile, PW_HI);
    bot_gtk_param_widget_load_from_key_file (self->gc_pw, keyfile, PW_GC);
    bot_gtk_param_widget_load_from_key_file (self->al_pw, keyfile, PW_AL);
}

static void
on_save_preferences (BotViewer *viewer, GKeyFile *keyfile, void *user_data)
{
    RendererRegionTopology *self = user_data;    
    bot_gtk_param_widget_save_to_key_file (self->pw, keyfile, PW_BASIC);
    bot_gtk_param_widget_save_to_key_file (self->gp_pw, keyfile, PW_GP);
    bot_gtk_param_widget_save_to_key_file (self->sc_pw, keyfile, PW_SC);
    bot_gtk_param_widget_save_to_key_file (self->lg_pw, keyfile, PW_LG);
    bot_gtk_param_widget_save_to_key_file (self->gi_pw, keyfile, PW_GI);
    bot_gtk_param_widget_save_to_key_file (self->dm_pw, keyfile, PW_DM);
    bot_gtk_param_widget_save_to_key_file (self->hi_pw, keyfile, PW_HI);
    bot_gtk_param_widget_save_to_key_file (self->gc_pw, keyfile, PW_GC);
    bot_gtk_param_widget_save_to_key_file (self->al_pw, keyfile, PW_AL);
}

static void region_particle_destroy(void *user, void *p){
    slam_graph_region_particle_list_t *part = (slam_graph_region_particle_list_t *) p;
    slam_graph_region_particle_list_t_destroy(part);
}

static gboolean
on_timer (RendererRegionTopology * self)
{
    //should we do this based on the new topologies??     
    /*if(self->question_dialog){
        int64_t c_utime = bot_timestamp_now();

        double delta = (c_utime - self->question_utime) / 1.0e6;

        if(delta > QUESTION_TIMEOUT_SEC){
            send_answer(self, "invalid");
            fprintf(stderr, "Question timeout\n");
        }
        }*/    
    
    return TRUE;
}

static RendererRegionTopology *
renderer_region_topological_graph_new (BotViewer *viewer, int priority, BotParam * param)
{    
    RendererRegionTopology *self = (RendererRegionTopology*) calloc (1, sizeof (*self));

    self->viewer = viewer;

    self->particle_history = bot_ptr_circular_new(PARTICLE_HISTORY_SIZE, region_particle_destroy, self);

    BotRenderer *renderer = &self->renderer;
    renderer->draw = renderer_advanced_topological_graph_draw;
    renderer->destroy = renderer_advanced_topological_graph_destroy;
    renderer->widget = bot_gtk_param_widget_new();
    renderer->name = RENDERER_NAME;
    renderer->user = self;
    renderer->enabled = 1;

    self->lcm = bot_lcm_get_global (NULL);
    if (!self->lcm) {
        fprintf (stderr,"Error: setup_renderer_laser() failed to get global lcm object\n");
        renderer_advanced_topological_graph_destroy (renderer);
        return NULL;
    }

    self->param = param;
    if (!self->param) {
        fprintf (stderr,"Error: setup_renderer_laser() failed to get BotParam instance\n");
        renderer_advanced_topological_graph_destroy (renderer);
        return NULL;
    }

    self->mutex = g_mutex_new ();
     
    //self->pw = BOT_GTK_PARAM_WIDGET (renderer->widget);
    self->particle_list = NULL;
        
    gtk_widget_show_all (renderer->widget);
   
    g_signal_connect (G_OBJECT (viewer), "load-preferences", 
                      G_CALLBACK (on_load_preferences), self);
    g_signal_connect (G_OBJECT (viewer), "save-preferences",
                      G_CALLBACK (on_save_preferences), self);

    GtkWidget *expander_gr = gtk_expander_new("Graphs to Render");    
    self->pw = BOT_GTK_PARAM_WIDGET (bot_gtk_param_widget_new ());
    GtkWidget *vbox_gr = gtk_vbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(self->renderer.widget), expander_gr, TRUE, TRUE, 0);
    gtk_container_add(GTK_CONTAINER(expander_gr), vbox_gr);
    gtk_box_pack_start(GTK_BOX(vbox_gr), GTK_WIDGET(self->pw), TRUE, TRUE, 0);
    gtk_widget_show_all(GTK_WIDGET(expander_gr));
    gtk_expander_set_expanded(GTK_EXPANDER(expander_gr), 1);    

    bot_gtk_param_widget_add_separator(self->pw, "");//Graphs to Render");
    
    bot_gtk_param_widget_add_buttons(self->pw, PARAM_REQUEST_RESULT, NULL);

    bot_gtk_param_widget_add_buttons(self->pw, PARAM_FINISH_SLAM, NULL);

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       0, 
                                       PARAM_DRAW_COMPLEX_LANGUAGE, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       0,
                                       PARAM_DRAW_ALL_GRAPHS, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       0,
                                       PARAM_DRAW_MAX_GRAPH, 0, NULL);

    
    bot_gtk_param_widget_add_booleans (self->pw, 
                                       0,
                                       PARAM_DRAW_VALID_GRAPH, 0, NULL);

    bot_gtk_param_widget_add_int(self->pw, VALID_MAP_IND, 
                                 BOT_GTK_PARAM_WIDGET_SLIDER, 0,50 , 1, 0);

    bot_gtk_param_widget_add_enum(self->pw, PARAM_ORDER_PARTICLES, BOT_GTK_PARAM_WIDGET_MENU, 
                                  0, 
                                  "Prob",0,
                                  "ID",1,
                                  NULL);

    bot_gtk_param_widget_add_int(self->pw,PARAM_DISTANCE_SCALE, 
                                 BOT_GTK_PARAM_WIDGET_SLIDER, 0 , 40 , 1, 5);
    
    bot_gtk_param_widget_add_booleans (self->pw, 
                                       0,
                                       PARAM_DRAW_EQUAL_DISTANCE, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       0,
                                       PARAM_DRAW_HEIGHT, 0, NULL);

    GtkWidget *expander_gp = gtk_expander_new("Graph Properties");    
    self->gp_pw = BOT_GTK_PARAM_WIDGET (bot_gtk_param_widget_new ());
    GtkWidget *vbox_gp = gtk_vbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(self->renderer.widget), expander_gp, TRUE, TRUE, 0);
    gtk_container_add(GTK_CONTAINER(expander_gp), vbox_gp);
    gtk_box_pack_start(GTK_BOX(vbox_gp), GTK_WIDGET(self->gp_pw), TRUE, TRUE, 0);
    gtk_widget_show_all(GTK_WIDGET(expander_gp));
    gtk_expander_set_expanded(GTK_EXPANDER(expander_gp), 1);    

    //bot_gtk_param_widget_add_separator(self->pw, "Graph Properties");
    bot_gtk_param_widget_add_separator(self->gp_pw, "");
    
    bot_gtk_param_widget_add_booleans (self->gp_pw, 
                                        0, PARAM_USE_AUTO_COLOR, 
                                        0, NULL);
    bot_gtk_param_widget_add_booleans (self->gp_pw, 
                                       0,
                                       PARAM_DRAW_REGIONS, 0, NULL);
    
    bot_gtk_param_widget_add_booleans (self->gp_pw, 
                                       0,
                                       PARAM_DRAW_NODES, 0, NULL);
       
    bot_gtk_param_widget_add_booleans (self->gp_pw, 
                                       0,
                                       PARAM_DRAW_REGION_CONNECTIONS, 
                                       0, NULL);
    
    bot_gtk_param_widget_add_booleans (self->gp_pw, 
                                       0,
                                       PARAM_DRAW_INTRA_REGION_EDGES, 
                                       0, NULL);
    
    bot_gtk_param_widget_add_booleans (self->gp_pw, 
                                       0,
                                       PARAM_DRAW_INTER_REGION_EDGES, 
                                       0, NULL);

    bot_gtk_param_widget_add_booleans (self->gp_pw, 
                                       0,
                                       PARAM_DRAW_EDGES_IN_BLACK,
                                       0, NULL);

    bot_gtk_param_widget_add_booleans (self->gp_pw, 
                                       0,
                                       PARAM_DRAW_REGION_MEAN, 
                                       0, NULL);
    
    GtkWidget *expander_sc = gtk_expander_new("Semantic Classifications");    
    self->sc_pw = BOT_GTK_PARAM_WIDGET (bot_gtk_param_widget_new ());
    GtkWidget *vbox_sc = gtk_vbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(self->renderer.widget), expander_sc, TRUE, TRUE, 0);
    gtk_container_add(GTK_CONTAINER(expander_sc), vbox_sc);
    gtk_box_pack_start(GTK_BOX(vbox_sc), GTK_WIDGET(self->sc_pw), TRUE, TRUE, 0);
    gtk_widget_show_all(GTK_WIDGET(expander_sc));
    gtk_expander_set_expanded(GTK_EXPANDER(expander_sc), 1);    
    
    bot_gtk_param_widget_add_separator(self->sc_pw, "");
    //bot_gtk_param_widget_add_separator(self->pw, "Semantic Classifications");

    bot_gtk_param_widget_add_booleans (self->sc_pw, 
                                       0, 
                                       PARAM_DRAW_LABEL_NAME, 0, NULL);
    bot_gtk_param_widget_add_booleans (self->sc_pw, 
                                       0, 
                                       PARAM_DRAW_ALL_MAX_LABELS, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->sc_pw, 
                                       0, 
                                       PARAM_DRAW_LEGAND, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->sc_pw, 
                                       0, 
                                       PARAM_DRAW_PERFORMANCE, 1, NULL);

    bot_gtk_param_widget_add_booleans (self->sc_pw, 
                                       0, 
                                       PARAM_DRAW_STATS, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->sc_pw, 
                                       0, 
                                       PARAM_DRAW_OBJECTS, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->sc_pw, 
                                       0,
                                       PARAM_DRAW_NO_LANG_DIST, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->sc_pw, 
                                       0,
                                       PARAM_DRAW_NO_ANSWERS_DIST, 0, NULL);   


    bot_gtk_param_widget_add_booleans (self->sc_pw, 
                                       0,
                                       PARAM_DRAW_REGION_LABEL, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->sc_pw, 
                                       0,
                                       PARAM_DRAW_REGION_LABEL_VALID, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->sc_pw, 
                                       0,
                                       PARAM_DRAW_REGION_LABEL_INVALID, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->sc_pw, 
                                       0,
                                       PARAM_DRAW_REGION_LABEL_MAX, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->sc_pw, 
                                       0,
                                       PARAM_DRAW_REGION_LABEL_BAR_GRAPH, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->sc_pw, 
                                       0,
                                       PARAM_DRAW_REGION_SEMANTICS, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->sc_pw, 
                                       0,
                                       PARAM_DRAW_REGION_SEMANTICS_MAX, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->sc_pw, 
                                       0,
                                       PARAM_DRAW_NODE_CLASSIFICATION, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->sc_pw, 
                                       0,
                                       PARAM_DRAW_NODE_CLASSIFICATION_MAX, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->sc_pw, 
                                       0,
                                       PARAM_DRAW_LASER_CLASSIFICATION, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->sc_pw, 
                                       0,
                                       PARAM_DRAW_LASER_CLASSIFICATION_MAX, 0, NULL);
                                       
    bot_gtk_param_widget_add_booleans (self->sc_pw, 
                                       0,
                                       PARAM_DRAW_IMAGE_CLASSIFICATION, 0, NULL);
    
    bot_gtk_param_widget_add_booleans (self->sc_pw, 
                                       0,
                                       PARAM_DRAW_IMAGE_CLASSIFICATION_MAX, 0, NULL);       

    GtkWidget *expander_lg = gtk_expander_new("Language Grounding");    
    self->lg_pw = BOT_GTK_PARAM_WIDGET (bot_gtk_param_widget_new ());
    GtkWidget *vbox_lg = gtk_vbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(self->renderer.widget), expander_lg, TRUE, TRUE, 0);
    gtk_container_add(GTK_CONTAINER(expander_lg), vbox_lg);
    gtk_box_pack_start(GTK_BOX(vbox_lg), GTK_WIDGET(self->lg_pw), TRUE, TRUE, 0);
    gtk_widget_show_all(GTK_WIDGET(expander_lg));
    gtk_expander_set_expanded(GTK_EXPANDER(expander_lg), 1);    

    bot_gtk_param_widget_add_separator(self->lg_pw, "");
    //bot_gtk_param_widget_add_separator(self->lg_pw, "Language Grounding");
    
    bot_gtk_param_widget_add_booleans (self->lg_pw, 
                                       0,
                                       PARAM_DRAW_COMPLEX_LANGUAGE_PATHS, 0, NULL); 
    
    bot_gtk_param_widget_add_int(self->lg_pw, PARAM_COMPLEX_LANGUAGE_EVENT_INDEX, BOT_GTK_PARAM_WIDGET_SLIDER, 0, 1, 1, 0);
    bot_gtk_param_widget_set_enabled (self->lg_pw, PARAM_COMPLEX_LANGUAGE_EVENT_INDEX, 0);

    bot_gtk_param_widget_add_booleans (self->lg_pw, 
                                       0,
                                       PARAM_DRAW_ALL_COMPLEX_LANGUAGE_PATHS, 0, NULL); 
       
    //lets just set this high enough 
    bot_gtk_param_widget_add_int(self->lg_pw, PARAM_COMPLEX_PATH_INDEX, BOT_GTK_PARAM_WIDGET_SLIDER, 0, 20, 1, 0);

    bot_gtk_param_widget_add_booleans (self->lg_pw, 
                                       0,
                                       PARAM_DRAW_FIG_PROB, 0, NULL); 
    
    bot_gtk_param_widget_add_booleans (self->lg_pw, 
                                       0,
                                       PARAM_IGNORE_PRIOR, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->lg_pw, 
                                       0,
                                       PARAM_DRAW_ANSWER_PROB, 0, NULL); 

    bot_gtk_param_widget_add_booleans (self->lg_pw, 
                                       0,
                                       PARAM_DRAW_ORIG_PROB, 0, NULL); 

    bot_gtk_param_widget_add_int(self->lg_pw, PARAM_DRAW_ANSWER_IND, BOT_GTK_PARAM_WIDGET_SLIDER, 0, 20, 1, 0);

    bot_gtk_param_widget_add_booleans (self->lg_pw, 
                                       0,
                                       PARAM_DRAW_LANDMARKS, 0, NULL);   

    GtkWidget *expander_gi = gtk_expander_new("Graph Info");    
    self->gi_pw = BOT_GTK_PARAM_WIDGET (bot_gtk_param_widget_new ());
    GtkWidget *vbox_gi = gtk_vbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(self->renderer.widget), expander_gi, TRUE, TRUE, 0);
    gtk_container_add(GTK_CONTAINER(expander_gi), vbox_gi);
    gtk_box_pack_start(GTK_BOX(vbox_gi), GTK_WIDGET(self->gi_pw), TRUE, TRUE, 0);
    gtk_widget_show_all(GTK_WIDGET(expander_gi));
    gtk_expander_set_expanded(GTK_EXPANDER(expander_gi), 1);    

    bot_gtk_param_widget_add_separator(self->gi_pw, "");
    //bot_gtk_param_widget_add_separator(self->gi_pw, "Graph Info");

    bot_gtk_param_widget_add_booleans (self->gi_pw, 
                                       0,
                                       PARAM_DISP_PROB, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->gi_pw, 
                                       0,
                                       PARAM_DRAW_LOG, 0, NULL);
                                       
    bot_gtk_param_widget_add_booleans (self->gi_pw, 
                                       0,
                                       PARAM_BOUNDING_BOXES, 0, NULL);
    
    bot_gtk_param_widget_add_booleans (self->gi_pw, 
                                       0,
                                       PARAM_DRAW_COV, 0, NULL);
    
    bot_gtk_param_widget_add_booleans (self->gi_pw, 
                                       0,
                                       PARAM_DRAW_REGION_ID, 0, NULL);
    
    bot_gtk_param_widget_add_booleans (self->gi_pw, 
                                       0,
                                       PARAM_DRAW_SEGMENT_ID, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->gi_pw, 
                                       0,
                                       PARAM_DRAW_NODE_ID, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->gi_pw, 
                                       0,
                                       PARAM_DRAW_DEAD_EDGES, 0, NULL);
    
    bot_gtk_param_widget_add_booleans ( self->gi_pw, 0,
                                        PARAM_DRAW_ODOM, 1, NULL);

    bot_gtk_param_widget_add_booleans (self->gi_pw, 
                                       0,
                                       PARAM_REMAP_REGIONS, 1, NULL);

    bot_gtk_param_widget_add_booleans (self->gi_pw, 
                                       0,
                                       PARAM_COLOR_REGIONS, 1, NULL);    

    bot_gtk_param_widget_add_int(self->gi_pw,
                                 PARAM_EDGE_THICKNESS, 
                                 BOT_GTK_PARAM_WIDGET_SLIDER, 0 , 20 , 1, 4);

    bot_gtk_param_widget_add_int(self->gi_pw, PARAM_LINE_THICKNESS, 
                                 BOT_GTK_PARAM_WIDGET_SLIDER, 0 , 20 , 1, 4);
   
    bot_gtk_param_widget_add_double (self->gi_pw, 
                                     PARAM_NODE_RADIUS, 0, 0.1, 2.0, 0.05, 0.25);

    bot_gtk_param_widget_add_double (self->gi_pw, 
                                     PARAM_BAR_GRAPH_SCALE, 0, 0.05, 2.0, 0.05, 0.25);

    GtkWidget *expander_dm = gtk_expander_new("Draw Map");    
    self->dm_pw = BOT_GTK_PARAM_WIDGET (bot_gtk_param_widget_new ());
    GtkWidget *vbox_dm = gtk_vbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(self->renderer.widget), expander_dm, TRUE, TRUE, 0);
    gtk_container_add(GTK_CONTAINER(expander_dm), vbox_dm);
    gtk_box_pack_start(GTK_BOX(vbox_dm), GTK_WIDGET(self->dm_pw), TRUE, TRUE, 0);
    gtk_widget_show_all(GTK_WIDGET(expander_dm));
    gtk_expander_set_expanded(GTK_EXPANDER(expander_dm), 1);    

    bot_gtk_param_widget_add_separator(self->dm_pw, "");
    //bot_gtk_param_widget_add_separator(self->pw, "Draw Map");

    bot_gtk_param_widget_add_booleans (self->dm_pw, 
                                       0,
                                       PARAM_DRAW_MAP_POINTS, 0, NULL);

    bot_gtk_param_widget_add_buttons(self->dm_pw, PARAM_REQUEST_MAP, NULL);

    bot_gtk_param_widget_add_buttons(self->dm_pw, PARAM_CLEAR_MAP, NULL);

    /*bot_gtk_param_widget_add_separator(self->pw, "Semantic Layer");
    
    bot_gtk_param_widget_add_booleans (self->pw, 
                                       0,
                                       PARAM_DRAW_LABEL_PIE_CHART, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       0,
                                       PARAM_DRAW_MAX_LABEL, 0, NULL);*/

    GtkWidget *expander_hi = gtk_expander_new("RBPF History");    
    self->hi_pw = BOT_GTK_PARAM_WIDGET (bot_gtk_param_widget_new ());
    GtkWidget *vbox_hi = gtk_vbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(self->renderer.widget), expander_hi, TRUE, TRUE, 0);
    gtk_container_add(GTK_CONTAINER(expander_hi), vbox_hi);
    gtk_box_pack_start(GTK_BOX(vbox_hi), GTK_WIDGET(self->hi_pw), TRUE, TRUE, 0);
    gtk_widget_show_all(GTK_WIDGET(expander_hi));
    gtk_expander_set_expanded(GTK_EXPANDER(expander_hi), 1);    

    bot_gtk_param_widget_add_separator(self->hi_pw, "");
    //bot_gtk_param_widget_add_separator(self->pw, "RBPF History");
    
    bot_gtk_param_widget_add_booleans (self->hi_pw, 
                                       0,
                                       PARAM_COMPARE_PARTICLE_HISTORY, 0, NULL);
      
    bot_gtk_param_widget_add_int(self->hi_pw, PARAM_COMPARE_PARTICLE_HISTORY_INDEX, BOT_GTK_PARAM_WIDGET_SLIDER, 1, PARTICLE_HISTORY_SIZE-1, 1, 1);
    bot_gtk_param_widget_add_int(self->hi_pw, PARAM_ANIMATION_SPEED, BOT_GTK_PARAM_WIDGET_SLIDER, 0, 1000, 10, 10);
    bot_gtk_param_widget_set_enabled (self->hi_pw, PARAM_COMPARE_PARTICLE_HISTORY_INDEX, 0);
       
    
    GtkWidget *expander_gc = gtk_expander_new("Graph Comparison");    
    self->gc_pw = BOT_GTK_PARAM_WIDGET (bot_gtk_param_widget_new ());
    GtkWidget *vbox_gc = gtk_vbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(self->renderer.widget), expander_gc, TRUE, TRUE, 0);
    gtk_container_add(GTK_CONTAINER(expander_gc), vbox_gc);
    gtk_box_pack_start(GTK_BOX(vbox_gc), GTK_WIDGET(self->gc_pw), TRUE, TRUE, 0);
    gtk_widget_show_all(GTK_WIDGET(expander_gc));
    gtk_expander_set_expanded(GTK_EXPANDER(expander_gc), 1);   

    bot_gtk_param_widget_add_separator(self->gc_pw, "");
    //bot_gtk_param_widget_add_separator(self->pw, "Graph Comparison");

    bot_gtk_param_widget_add_booleans (self->gc_pw, 
                                       0,
                                       PARAM_DRAW_SIDE_BY_SIDE, 0, NULL);
   
    bot_gtk_param_widget_add_int(self->gc_pw, PARAM_DRAW_GRAPH_1_ID, BOT_GTK_PARAM_WIDGET_SLIDER, 0, 50, 1, 0);
    
    bot_gtk_param_widget_add_int(self->gc_pw, PARAM_DRAW_GRAPH_2_ID, BOT_GTK_PARAM_WIDGET_SLIDER, 0, 50, 1, 0);

    GtkWidget *expander_al = gtk_expander_new("Annotate Location");    
    self->al_pw = BOT_GTK_PARAM_WIDGET (bot_gtk_param_widget_new ());
    GtkWidget *vbox_al = gtk_vbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(self->renderer.widget), expander_al, TRUE, TRUE, 0);
    gtk_container_add(GTK_CONTAINER(expander_al), vbox_al);
    gtk_box_pack_start(GTK_BOX(vbox_al), GTK_WIDGET(self->al_pw), TRUE, TRUE, 0);
    gtk_widget_show_all(GTK_WIDGET(expander_al));
    gtk_expander_set_expanded(GTK_EXPANDER(expander_al), 1);   

    bot_gtk_param_widget_add_separator(self->al_pw, "");
    //bot_gtk_param_widget_add_separator(self->pw, "Annotate Location");
                                       
    
    // BUTTON - Create new file to write language labels
    bot_gtk_param_widget_add_buttons (self->al_pw, PARAM_CREATE_LANGUAGE_FILE, NULL);    
    
    //DROP DOWN MENU - label
    bot_gtk_param_widget_add_enum(self->al_pw, LANG_LABEL, BOT_GTK_PARAM_WIDGET_MENU, 
                                  0, 
                                  "courtyard",0,
                                  "gym",1,
                                  "hallway",2, 
                                  "amphitheater", 3,
                                  "cafeteria", 4, 
                                  "elevator lobby", 5, 
                                  "entrance", 6, NULL);
                                  
    //DROP DOWN MENU - constraint type
    bot_gtk_param_widget_add_enum(self->al_pw, LANG_CONSTRAINT_TYPE, BOT_GTK_PARAM_WIDGET_MENU, 
                                  0, 
                                  "At location X",0,
                                  "language update", 1, 
                                  NULL);
                                  
    bot_gtk_param_widget_add_text_entry(self->al_pw, PARAM_LANGUAGE_UPDATE, BOT_GTK_PARAM_WIDGET_ENTRY, "Insert language update here");


    // BUTTON - Writes the label to the text file
    bot_gtk_param_widget_add_buttons(self->al_pw, ADD_LANG_LABEL, NULL);

    g_signal_connect (G_OBJECT (self->pw), "changed", 
                      G_CALLBACK (on_param_widget_changed), self);
    g_signal_connect (G_OBJECT (self->gp_pw), "changed", 
                      G_CALLBACK (on_param_widget_changed), self);
    g_signal_connect (G_OBJECT (self->sc_pw), "changed", 
                      G_CALLBACK (on_param_widget_changed), self);
    g_signal_connect (G_OBJECT (self->lg_pw), "changed", 
                      G_CALLBACK (on_param_widget_changed), self);
    g_signal_connect (G_OBJECT (self->gi_pw), "changed", 
                      G_CALLBACK (on_param_widget_changed), self);
    g_signal_connect (G_OBJECT (self->dm_pw), "changed", 
                      G_CALLBACK (on_param_widget_changed), self);
    g_signal_connect (G_OBJECT (self->hi_pw), "changed", 
                      G_CALLBACK (on_param_widget_changed), self);
    g_signal_connect (G_OBJECT (self->gc_pw), "changed", 
                      G_CALLBACK (on_param_widget_changed), self);
    g_signal_connect (G_OBJECT (self->al_pw), "changed", 
                      G_CALLBACK (on_param_widget_changed), self);
    
    self->node_scans = g_hash_table_new(g_int_hash, g_int_equal);
    self->language_update = NULL;
    self->type_map = g_hash_table_new(g_int_hash, g_int_equal);
    self->label_map = g_hash_table_new(g_int_hash, g_int_equal);

    self->node_classifications = g_hash_table_new(g_int_hash, g_int_equal);
    
    self->sorted_classes = g_hash_table_new(g_int_hash, g_int_equal);
    self->appearance_map = g_hash_table_new(g_int_hash, g_int_equal);
    //self->sorted_image_classes = g_hash_table_new(g_int_hash, g_int_equal);
    
    self->question_utime = -1;

    slam_graph_region_particle_list_t_subscribe(self->lcm, "REGION_PARTICLE_ISAM_RESULT", on_topo_graph, self);
   
    slam_laser_pose_t_subscribe(self->lcm, "SLAM_POSE_LASER_POINTS", on_laser_pose, self);

    slam_laser_pose_list_t_subscribe(self->lcm, "ISAM_LASER_SCANS", on_laser_scan_list, self);
       
    bot_core_pose_t_subscribe (self->lcm, "POSE", on_pose, self);

    self->ehandler.name = (char*)RENDERER_NAME;
    self->ehandler.enabled = 1;
    self->ehandler.mouse_press = mouse_press;
    self->ehandler.user = self;
    
    bot_viewer_add_event_handler(viewer, &self->ehandler, priority);
    //tells us when to dump the old buffer
    slam_status_t_subscribe(self->lcm, "SLAM_STATUS", on_slam_status, self);
    erlcm_rigid_transform_list_t_subscribe(self->lcm, "SLAM_TRANSFORMS", on_slam_transforms, self);
    slam_complex_language_collection_list_t_subscribe(self->lcm, "COMPLEX_LANGUAGE_PATHS", on_language_update, self);
    slam_performance_t_subscribe(self->lcm, "SLAM_PROGRESS", on_slam_progress, self);

    slam_language_question_t_subscribe(self->lcm, "SLAM_SR_QUESTION", on_slam_question, self);
    
    g_timeout_add(10, (GSourceFunc) on_timer, self);

    self->last_sent_particle_id = -1;
    self->active = 0;
    self->active_particle = -1;
    self->no_particles = 0;
    self->node_1 = -1;
    self->node_2 = -1;
    self->diff_particle = NULL;
    self->params.draw_diff = 0;
    self->last_slam_transform = NULL;
    self->current_ind = -1;
    self->diff_utime = 0;
    self->diff_iter = 0;
    self->performance = NULL;
    self->last_question = NULL;
    self->question_dialog = NULL;
    
    return self;
}

void
setup_renderer_region_graph (BotViewer *viewer, int priority, BotParam * param)
{
    RendererRegionTopology *self = renderer_region_topological_graph_new (viewer, priority, param);
    bot_viewer_add_renderer (viewer, &self->renderer, priority);
}

