#include "SlamGraph.hpp"

RegionSegment::RegionSegment(int segment_id){
    id = segment_id;
    region = NULL;
}

void RegionSegment::addNode(SlamNode *node){
    nodes.push_back(node);
    node->segment = this;
    region = node->region_node;
}

void RegionSegment::addNodes(vector<SlamNode *> node_list){
    for(int i = 0; i < node_list.size(); i++){
        addNode(node_list[i]);
        //a segment should not be in multiple regions 
        region = node_list[i]->region_node;
    }
}

//this should not be called - regions should not be deleted 
int RegionSegment::removeNode(SlamNode *node){
    for(int i = 0; i < nodes.size(); i++){
        if(node == nodes[i]){
            nodes.erase(nodes.begin() + i);
            node->segment = NULL;
            return 1;
        }
    }
    return 0;
}

int RegionSegment::removeNodes(vector<SlamNode *> node_list){
    int status = 1;
    for(int i = 0; i < node_list.size(); i++){
        int s = removeNode(node_list[i]);
        if(s ==0){
            status = 0;
        }
    }
    return status;
}


SlamNode *RegionSegment::getClosestNode(SlamNode *nd, double *min_dist){
    *min_dist = 1000000;
    SlamNode *closest_node = NULL;
    for(int j=0; j < nodes.size(); j++){
        SlamNode *c_node = nodes[j];
        Pose2d delta = c_node->getPose().ominus(nd->getPose());
        double dist = hypot(delta.x(), delta.y());

        if(dist < *min_dist){
            *min_dist = dist;
            closest_node = c_node;
        }
    }
    return closest_node;
}
