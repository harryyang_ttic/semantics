#include "SlamGraph.hpp"

RegionEdge::RegionEdge(RegionNode *_nd1, RegionNode *_nd2)
{
    node1 = _nd1;
    node2 = _nd2;
    type = REGION_EDGE_LOOPCLOSURE;
    //r_node1_id = _nd1->region_id;
    //r_node2_id = _nd2->region_id;
}

RegionEdge::RegionEdge(RegionNode *_nd1, RegionNode *_nd2, region_edge_type_t _type)
{
    node1 = _nd1;
    node2 = _nd2;
    type = _type;
    //r_node1_id = _nd1->region_id;
    //r_node2_id = _nd2->region_id;
}


void RegionEdge::addEdge(SlamConstraint *ct)
{
    int id = ct->id;
    if(ct->type == SLAM_GRAPH_EDGE_T_TYPE_SM_INC || ct->type == SLAM_GRAPH_EDGE_T_TYPE_ODOM_INC){
        type = REGION_EDGE_INCREMENTAL;
    }
    edge_list.insert(make_pair(id, ct));
}

RegionEdge::~RegionEdge()
{

}
