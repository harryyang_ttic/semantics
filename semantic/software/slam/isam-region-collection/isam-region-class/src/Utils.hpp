#ifndef SEMANTIC_UTILS_H_
#define SEMANTIC_UTILS_H_

#include <gsl/gsl_blas.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_sf_bessel.h>
#include <gsl/gsl_integration.h>
#include <math.h>
#include "SlamGraph.hpp"

#define SCAN_Z_MAX 20.0
#define SCAN_Z_MIN 0.01

typedef struct _dist_params 
{ 
    double mu; 
    double sigma;
} dist_params;

namespace Utils{
    string getSR(spatialRelation sr);

    //string createQuestion(string label, spatialRelation sr);
 
    double get_sigmoid_value(double val, double mean, double threshold, double sigma);

    //BotTrans getBotTransFromPose(double x, double y, double theta);
    double getLikelihoodOfEdge(map<int, double> likelihood, map<int, double> dist_i, map<int, double> dist_j);

    BotTrans getBotTransRPY(double pos[3], double rpy[3]);
    BotTrans getBotTransQuat(double pos[3], double quat[3]);

    //e.g. pose 1 is to the left of pose 2 
    double getSRLikelihood(Pose2d p1, spatialRelation sr, Pose2d p2);
    double getSRLikelihoodNew(Pose2d figure, spatialRelation sr, Pose2d landmark);
    spatialRelation getSpatialRelationType(string name);
    double getSRLikelihoodGeneral(spatialRelation sr, Pose2d robot_pose, Pose2d figure, Pose2d landmark, bool use_robot);
    vector<spatialRelation> getSRList();
    bool isSRHandled(spatialRelation sr);

    Pose2d getPoseFromBotTrans(BotTrans tf);
    BotTrans getBotTransFromPose(Pose2d pose);
    BotTrans getTransformFromSlamNodes(SlamNode *match, SlamNode *target);

    double get_edge_likelihood(double mean_d, double cov);
    double get_prob_to_segment_region_size(int no_nodes);
    double get_prob_to_merge(double closest_dist, double alpha=0.1);
    double get_prob_to_segment_from_ncut(double ncut);
    double get_prob_to_segment_from_ncut_model_1(double ncut);
    bool compareDistance (nodeDist node1, nodeDist node2);
    bool compareNodePairDistance (nodePairDist node1, nodePairDist node2);
    bool compareRegionPathProbs(regionPathProb p1, regionPathProb p2);
    bool compareRegionOverlap (regionDist node1, regionDist node2);
    bool compareRegionPairOverlap (regionPairDist node1, regionPairDist node2);

    void printDistribution(OutputWriter *op, const vector<regionProb> &dist, string prefix);

    double rice_dist_probability (double x, void * p);
    int check_to_add(SlamNode *node1, SlamNode *node2, int find_ground_truth);
    int check_to_add_basic(SlamNode *node1, SlamNode *node2, int find_ground_truth, MatrixXd cov);
    double get_observation_probability(double mu, double sigma);
    double  get_prob_of_split_overlap(double max_overlap);
    bool compareID (SemVar v1, SemVar v2);

    bool compareRegionProb(regionProb p1, regionProb p2);
    bool compareRegionPaths(RegionPath *p1, RegionPath *p2);
    bool compareSRProbs(spatialResult p1, spatialResult p2);
    bool compareSRQuestions(SRQuestion p1, SRQuestion p2);

    double calculateEntropy(vector<regionProb> dist);
    void normalizeDistribution(vector<regionProb> &dist);

    double pHit (double z_star, double z);
    double pShort (double z_star, double z);

    //for slu classifier - this Pose is defined in SLU classifier
    //relative_pose = true - means the orientation is extracted based on where the next node is 
    vector<Pose> getPosesFromNodes(const vector<SlamNode*> &nodes, bool relative_pose=false); 

    vector<SlamNode *> getPath(map<SlamNode*, SlamNode *> &parent_map, SlamNode *goal, bool reverse_path=false);

    double getPathLength(map<SlamNode *, double> &dist_map, SlamNode *goal);
    double getPathLengthToClosestNode(map<SlamNode*, SlamNode *> &parent_map, SlamNode *goal);
    vector<SlamNode *> getPathAndLength(map<SlamNode*, SlamNode *> &parent_map, SlamNode *goal, double *dist, bool reverse_path=false);

    double calculateSimilarity (LabelDistribution *ldist1, LabelDistribution *ldist2);

    string getAnswerString(int answer);

    double folded_gaussian_probability_for_merging_regions(double x, void *p);
    double folded_gaussian_probability_for_dist(double x, void *p);
    double folded_gaussian_probability_for_constraint(double x, void *p);
    double folded_gaussian_probability(double x, void *p);
    double getCosineSimilarity(map<int, double> dist_i, map<int, double> dist_j);
    double calculateCloseNodeRatio(vector<SlamNode *> &nodes, vector<SlamNode *> &nodes1, vector<nodePairDist> &close_node_pairs);
    int get_prob_for_edge(SlamNode *node1, SlamNode *node2, int find_ground_truth);
    smPoint *get_scan_points_from_laser(const bot_core_planar_lidar_t *laser_msg, int beam_skip,  double spatialDecimationThresh, 
                                        double maxRange, float validBeamAngles[2], int *noPoints, BotTrans *trans= NULL);
    slam_language_label_t *getAnnotatedLanguage(map<int, slam_language_label_t *> &lang_labels, int node_id);
    int getObsIndFromLabel(LabelInfo *info, string label);
    double get_prob_to_propose_regions_no_cov(SlamNode *node1, SlamNode *node2, double dist_threshold);
    double get_prob_to_propose_regions(SlamNode *node1, SlamNode *node2, MatrixXd cov, double dist_threshold);
    double getCosineSimilarity(map<int, double> dist_i, map<int, double> dist_j);

    double getDistanceBetweenNodes(SlamNode *nd1, SlamNode *nd2);

    //drawing utils
    void drawScan(bot_lcmgl_t *lcmgl, NodeScan *pose_to_match, ScanTransform T, double color[3]);
    void drawPoints(bot_lcmgl_t *lcmgl, pointlist2d_t *list, ScanTransform *T, double color[3], int transform);
}
#endif
