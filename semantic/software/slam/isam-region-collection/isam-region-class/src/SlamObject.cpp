#include "SlamGraph.hpp"
#include "Utils.hpp"
using namespace Utils;

SlamObject::SlamObject(ObjectDetection *_objectDetection, LabelInfo *label_info){
    objectDetection = _objectDetection;    
    id = objectDetection->id;
    type = objectDetection->type;
    valid = 0;
    assigned_node = NULL;
    assigned_region = NULL;
    pose = Pose2d(1000,1000,0);
    object_to_region_type_factor = NULL;

    if(label_info){
        object_type = new SemVar(OBJECT_TYPE_INCREMENT + id, (int) label_info->object_name_to_index.size());
    }
    object_type_observation = new SemFactor(*object_type);
        
    //this doesn't inject uncertainty in to the detection for now 
    //if uncertainty is there 
    //dont be surprised when seeing different likelihoods for region categories even 
    //though both categories are equally likely upon seeing an object 
    //this is because of the weights in the other objects that are not equal 
    for(int i=0; i < object_type->states(); i++){ 
        if(i == label_info->getIndexFromObjectId(objectDetection->type)){
            object_type_observation->set(i, 1);
        }
        else{
            //object_type_observation->set(i, 0.1);
            object_type_observation->set(i, 0);
        }
    }
}

void SlamObject::updateDetection(ObjectDetection *_objectDetection){
    objectDetection = _objectDetection;    
}    

void SlamObject::print(){
    if(assigned_node){
        fprintf(stderr, MAJENTA "\t Object : %d - Type : %d Closest Node : %d [%d] -> %f,%f,%f\n" RESET_COLOR , id, type, 
                assigned_node->id, assigned_node->region_node->region_id, pose.x(), pose.y(), pose.t());
    }
    else{
        fprintf(stderr, MAJENTA "\t Object : %d - Type : %d => No pose\n" RESET_COLOR, id, type);
    }
}

void SlamObject::removeAssignedRegion(){
    assigned_region = NULL;
    if(object_to_region_type_factor){
        delete object_to_region_type_factor; 
        object_to_region_type_factor = NULL;
    }
}

void SlamObject::updateAssignedNode(SlamNode *node, LabelInfo *label_info){
    assigned_node = node;

    if(assigned_region != node->region_node){
        if(assigned_region != NULL){
            //make sure that when a region is deleted this is set to NULL
            assigned_region->removeObject(this);
            //if(object_to_region_type_factor)
            //delete object_to_region_type_factor; 
        }
        assigned_region = node->region_node;
        assigned_region->addObject(this);
    }
   
    //create a factor 
    if(object_to_region_type_factor)
        delete object_to_region_type_factor;

    object_to_region_type_factor = label_info->getObjectToRegionTypeFactor(assigned_region->region_type, object_type);
}

Pose2d SlamObject::getPose(int *valid){        
    return pose; 
}

void SlamObject::updatePose(){
    if(assigned_node){
        Pose2d node_to_global = assigned_node->getPose();
        Pose2d object_to_node = objectDetection->pose_to_close_node;
        
        BotTrans node_to_global_tf = getBotTransFromPose(node_to_global);
        BotTrans object_to_node_tf = getBotTransFromPose(object_to_node);
        BotTrans object_to_global_tf; 
        bot_trans_apply_trans_to(&node_to_global_tf, &object_to_node_tf, &object_to_global_tf);
        valid = 1;
        pose = getPoseFromBotTrans(object_to_global_tf);
    }
}
