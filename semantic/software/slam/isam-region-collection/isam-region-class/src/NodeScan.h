/*
 * NodeScan.h
 *
 *  Created on: Oct 22, 2009
 *      Author: abachrac
 */

#ifndef NODESCAN_H_
#define NODESCAN_H_
//#include <bot/scanmatch/Scan.hpp>
#include  <classify-semantic/classify_semantic.hpp>
#include <scanmatch/Scan.hpp>
#include <vector>
#include <isam/isam.h>
//#include "AnchorPose.h"
//#include "PoseToPoseTransform.h"
#include <bot_core/bot_core.h>
#include <lcmtypes/slam_laser_pose_t.h>
// ANN declarations
#include <ANN/ANN.h> 
#include <ANN/ANNx.h>
#include <ANN/ANNperf.h>

using namespace std;
using namespace scanmatch;
using namespace isam;

class PoseToPoseTransform;

typedef struct{
    ANNpointArray dataPts;
    int no_points;
} ann_t;

class NodeScan {
public:
    //NodeScan(int64_t _utime, int _node_id, Scan * _scan, bool _is_supernode);
    NodeScan(int64_t _utime, int _node_id, Scan * _scan, bool _is_supernode, Pose2d _odom_pose, classification_result_t *laser_classification = NULL, classification_result_t *image_classification=NULL);
    NodeScan(NodeScan *node);
    virtual ~NodeScan();
    double getObsProbImage(int class_id);
    double getObsProbLaser(int class_id);

    void get_slampose_message(slam_laser_pose_t &msg);

    ann_t *kd_points;
    ANNkd_tree *kd_tree;
    int updateScan();
    Pose2d odom_pose;
    int64_t utime;
    int node_id;
    Scan * scan;
    double max_prob; 
    Pose2d_Node *pose2d_node;
    bot_core_planar_lidar_t *laser;
    bool is_supernode; //we will get rid of this soon 
    bool transition_node; //this was a node added after a transition signal from an external process (e.g door detection)
    BotTrans body_to_local;
    vector<Scan *> allScans;
    PoseToPoseTransform * inc_constraint_odom;
    PoseToPoseTransform * inc_constraint_sm;
    //vector<PoseToPoseTransform *> constraints;
    map<int, vector<PoseToPoseTransform *> *> sm_full_constraints;
    map<int, PoseToPoseTransform *> sm_lang_constraints;
    map<int, PoseToPoseTransform *> sm_constraints;
    
    map<int, double> laser_classification_obs;
    map<int, double> image_classification_obs;

    classification_result_t *laser_classification;
    classification_result_t *image_classification;
    
};

class PoseToPoseTransform {
 public:
        
    Pose2d *transform;
    NodeScan *node_current;
    NodeScan *node_relative_to;

    Eigen::Matrix3d cov_mat;

    //some indication of the prior 
    double distance_difference;
    double angle_difference;

    Noise *noise;
    double hitpct; 
    int accepted; 
    int32_t type;

    PoseToPoseTransform(Pose2d *_transform, NodeScan *_node_current, NodeScan *_node_relative_to, int32_t _type,  
                        Noise *_noise, Eigen::Matrix3d _cov_mat, double _hitpct, double dist_diff, double ang_diff, int _accepted){
        node_current = _node_current;
        node_relative_to = _node_relative_to;
        type = _type;
        transform = new Pose2d(*_transform); 
        noise = new Noise(*_noise);
        hitpct = _hitpct;
        accepted = _accepted;
        distance_difference = dist_diff;
        angle_difference = ang_diff;
        cov_mat = _cov_mat; 
    }

    //don't have a destructor 
    ~PoseToPoseTransform(){
        //assumes that these were calloc'ed - prob better to create copies (and ensure that the originals are destroyed
        delete transform;
        delete noise;
    }
};


#endif /* SLAMPOSE_H_ */
