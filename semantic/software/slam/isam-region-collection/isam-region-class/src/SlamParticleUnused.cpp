#include "SlamParticle.hpp"

using namespace Utils;
using namespace ExpUtils;

double SlamParticle::getProbOfMeasurement(SlamNode *last_nd){
    Pose2d tocheck_value = last_nd->getPose();

    // Clear the copy of the local occupancy grid
    memset (local_px_map->data, 0, local_px_map->num_cells*sizeof(float));

    double dist =0;
    double pos[2], next_pos[2];
    double start[2];


    // Node->Slam_Pose->Scan laser scans are decimated and in the body frame
    // We will generate the local occupancy grid in the Query Body frame
    BotTrans localToQueryBody;
    localToQueryBody.trans_vec[0] = tocheck_value.x();
    localToQueryBody.trans_vec[1] = tocheck_value.y();
    localToQueryBody.trans_vec[2] = 0.0;
    double rpyQueryBody[3] = {0.0, 0.0, tocheck_value.t()};
    bot_roll_pitch_yaw_to_quat (rpyQueryBody, localToQueryBody.rot_quat);
    bot_trans_invert (&localToQueryBody);

    BotTrans matchBodyToLocal;
    //BotTrans matchLaserToLocal;
    BotTrans matchBodyToQueryBody;

    double rpyMatchBody[3] = {0.0, 0.0, 0.0};
    double laserStartMatchBody[3] = {0.4460, -0.0101, 0.1};
    double laserStartQueryBody[3];
    double pMatchBody[3] =  {0.0, 0.0, 0.0};
    double pMatchBodyNext[3] = {0.0, 0.0, 0.0};
    double pQueryBody[3];
    double pQueryBodyNext[3];

    // Find the nodes (and scans) near the local search area
    for(int k = 0; k < last_nd->position; k++){
        SlamNode *nd = graph->getSlamNodeFromPosition(k);
        Pose2d value = nd->getPose();

        Pose2d delta = tocheck_value.ominus(value);
        dist = hypot(delta.x(), delta.y());
        if(dist < 10.0){
            //fprintf(stderr, "[%d] - at dist : %f - adding for the simulation\n", (int) nd->id, dist);
            //loop through the points and add
            ScanTransform T;
            T.x = value.x();
            T.y = value.y();
            T.theta = value.t();

            Pose2d match = nd->getPose();

            // Determine the transform from Match Body ---> Query Body
            
            // Set the BotTrans from Match BODY to LOCAL
            matchBodyToLocal.trans_vec[0] = match.x();
            matchBodyToLocal.trans_vec[1] = match.y();
            matchBodyToLocal.trans_vec[2] = 0.0;
            rpyMatchBody[2] = match.t();
            bot_roll_pitch_yaw_to_quat (rpyMatchBody, matchBodyToLocal.rot_quat);

            // ... and now Match Body ---> Query Body
            bot_trans_apply_trans_to (&localToQueryBody, &matchBodyToLocal, &matchBodyToQueryBody);

            Scan *scan = nd->slam_pose->scan;

            float clamp[2] = {0,1.0} ;
            float ray_trace_clamp[2] = {-11.0, 6};

            // Transform Match Laser (0,0,0) ---> Query Laser frame
            bot_trans_apply_vec ( &matchBodyToQueryBody, laserStartMatchBody, laserStartQueryBody);
            start[0] = laserStartQueryBody[0];
            start[1] = laserStartQueryBody[1];

            //start[0] = bodyToLocal.trans_vec[0];
            //start[1] = bodyToLocal.trans_vec[1];

            for (unsigned i = 0; i < scan->numPoints; i++) {

                pMatchBody[0] = scan->points[i].x;
                pMatchBody[1] = scan->points[i].y;

                bot_trans_apply_vec ( &matchBodyToQueryBody, pMatchBody, pQueryBody);
                pos[0] = pQueryBody[0];
                pos[1] = pQueryBody[1];

                //px_map->rayTrace(start, pos, -2, 4.24, ray_trace_clamp);
                local_px_map->rayTrace(start, pos, -1, 2, ray_trace_clamp);
                
                //doesn't work well - prob because of the scans hitting the floor 
                if(0){
                    if(i < scan->numPoints-1){
                        pMatchBodyNext[0] = scan->points[i+1].x;
                        pMatchBodyNext[1] = scan->points[i+1].y;

                        bot_trans_apply_vec ( &matchBodyToQueryBody, pMatchBodyNext, pQueryBodyNext);
                        next_pos[0] = pQueryBodyNext[0];
                        next_pos[1] = pQueryBodyNext[1];
                    
                        //px_map->rayTrace(start, pos, -2, 4.24, ray_trace_clamp);
                        local_px_map->rayTrace(pos, next_pos, 1, 0, ray_trace_clamp);
                        //draw line 
                    
                    }
                }
            }
        }            
    }

    //lets publish the pixelmap also 
    if(1){
        const occ_map_pixel_map_t *map = local_px_map->get_pixel_map_t(bot_timestamp_now());
        occ_map_pixel_map_t_publish(lcm, "PIXEL_MAP", map);
    }

    LaserSim2D laserSim(local_px_map, 1081, -2.3561945, 0.004363323, 30.0);

    Pose2d value = last_nd->getPose();
    BotTrans bodyToLocal;
    
    const bot_core_planar_lidar_t *sim_laser = laserSim.simulate(0, 0, 0, bot_timestamp_now());
    double prob = 0.0;
        
    if(last_nd->slam_pose->laser != NULL){
        //lets assume that these scans are aligned as they should be 
        prob = compareScans(sim_laser, last_nd->slam_pose->laser);
        //double prob2 = scanLikelihood(sim_laser, last_nd->slam_pose->laser);
    }

    if(0){
        bot_core_planar_lidar_t_publish(lcm,"SIM_SLAM_LASER", sim_laser);
        bot_core_planar_lidar_t_publish(lcm,"COM_SLAM_LASER", last_nd->slam_pose->laser);
    }

    return prob; //HACK - just to see if it makes particle diversity more reasonable
}

//try to scanmatch - and see how much we need to move to get a resonable scanmatch 
//and use the uncertainity of the pose to come to 
//a prob estimate
double SlamParticle::getProbOfMeasurementSM(SlamNode *last_nd){
    Pose2d tocheck_value = last_nd->getPose();
    double dist = 0;

    double dist_threshold = 10.0;
    int max_no_nodes = 5;
    vector< pair<int,int> > matched_nodeid_pointidx;
    vector<SlamNode *> valid_nodes;
    int use_correspondance = 1;

    if(last_nd->slam_pose->scan->numPoints < 20){
        fprintf(stderr, RED "Not enough scans to scanmatch - returning minimum prob\n" RESET_COLOR);
        return 0.2;
    }

    int skip_current_region = 1;

    int no_of_points_to_add = 0;

    if(use_correspondance){
        double skip_threshold = 0.95;
        vector<nodeDist> valid_node_dist;
        for(int i=0; i < graph->region_node_list.size(); i++){
            RegionNode *region = graph->region_node_list[i];
            //if(region == node->region_node)
            //  continue;

            for(int j=0; j < region->nodes.size(); j++){           
                SlamNode *nd = region->nodes[j];
                if(nd == last_nd)
                    continue;
                
                if(nd->slam_pose->scan->numPoints < 20)
                    continue;

                if(skip_current_region && last_nd->region_node == nd->region_node)
                    continue;

                //or should we just use the correspondance - since we have that already??
                double dist = getDistanceBetweenNodes(last_nd, nd);
                if(dist < dist_threshold){
                    //inverted - because of the sorting function 
                    double score = 1 - getUpdatedCorrespondenceScore(last_nd, nd);
                    if(score >= skip_threshold)
                        continue;
                    valid_node_dist.push_back(make_pair(nd, score));
                }
            }
        }
        std::sort(valid_node_dist.begin(), valid_node_dist.end(),compareDistance);
        for(int i=0; i < valid_node_dist.size(); i++){
            //fprintf(stderr, "\tNode : %d - %f\n", valid_node_dist[i].first->id, valid_node_dist[i].second);
            if(i >=max_no_nodes){
                break;
            }
            valid_nodes.push_back(valid_node_dist[i].first);
        }
    }
    else{
        vector<nodeDist> valid_node_dist;
        for(int i=0; i < graph->region_node_list.size(); i++){
            RegionNode *region = graph->region_node_list[i];
            //if(region == node->region_node)
            //  continue;

            for(int j=0; j < region->nodes.size(); j++){           
                SlamNode *nd = region->nodes[j];
                if(nd == last_nd)
                    continue;

                if(nd->slam_pose->scan->numPoints < 20)
                    continue;

                if(skip_current_region && last_nd->region_node == nd->region_node)
                    continue;

                //or should we just use the correspondance - since we have that already??
                double dist = getDistanceBetweenNodes(last_nd, nd);
                if(dist < dist_threshold){
                    /*Pose2d value = nd->getPose();
                    nd->valid = 1;
            
                    ScanTransform T;
                    T.x = value.x();
                    T.y = value.y();
                    T.theta = value.t();

                    // Determine the transform from Match Body ---> Query Body
            
                    // Set the BotTrans from Match BODY to LOCAL
                    matchBodyToLocal.trans_vec[0] = value.x();
                    matchBodyToLocal.trans_vec[1] = value.y();
                    matchBodyToLocal.trans_vec[2] = 0.0;
                    rpyMatchBody[2] = value.t();
                    bot_roll_pitch_yaw_to_quat (rpyMatchBody, matchBodyToLocal.rot_quat);
            
                    // ... and now Match Body ---> Query Body
                    bot_trans_apply_trans_to (&localToQueryBody, &matchBodyToLocal, &matchBodyToQueryBody);
                    nd->queryBodyToMatchBody = matchBodyToQueryBody;
                    bot_trans_invert(&nd->queryBodyToMatchBody);*/
                    valid_node_dist.push_back(make_pair(nd, dist));
                }
            }
        }
        std::sort(valid_node_dist.begin(), valid_node_dist.end(),compareDistance);
        for(int i=0; i < valid_node_dist.size(); i++){
            //fprintf(stderr, "\tNode : %d - %f\n", valid_node_dist[i].first->id, valid_node_dist[i].second);
            if(i >=max_no_nodes){
                break;
            }
            valid_nodes.push_back(valid_node_dist[i].first);
        }
    }


    if(valid_nodes.size() == 0){
        return 0.2;
    }
    
    // Find the nodes (and scans) near the local search area
    //maybe this should be built on the closest slam node frame 
    double min_dist = 10000;
    SlamNode *closest_node = NULL;
    for(int k = 0; k < valid_nodes.size(); k++){
        SlamNode *nd = valid_nodes[k];
        Pose2d value = nd->getPose();

        Pose2d delta = tocheck_value.ominus(value);
        dist = hypot(delta.x(), delta.y());

        if(dist < min_dist){
            min_dist = dist;
            closest_node = nd;
        }        
    }
    
    if(closest_node == NULL)
        return 0.2;


    BotTrans target_to_local = getBotTransFromPose(closest_node->getPose());
    BotTrans local_to_target = target_to_local; 
    bot_trans_invert(&local_to_target);

    for(int k = 0; k < valid_nodes.size(); k++){
        SlamNode *nd = valid_nodes[k];
        Pose2d value = nd->getPose();

        BotTrans nd_to_local = getBotTransFromPose(nd->getPose());

        //we need the transform from this node to the closest node 
        BotTrans nd_to_target;
        
        bot_trans_apply_trans_to(&local_to_target, &nd_to_local, &nd_to_target);

        Pose2d tf = getPoseFromBotTrans(nd_to_target);

        Pose2d delta = tocheck_value.ominus(value);
        dist = hypot(delta.x(), delta.y());

        //we should transform this to the closest node's frame of reference 

        ScanTransform T;
        T.x = tf.x();//value.x();
        T.y = tf.y();//value.y();
        T.theta = tf.t();//value.t();
        
        // applyTransform copies everything from T to nd->slam_pose->scan->T
        // so we should populate all of T first
        T.score = nd->slam_pose->scan->T.score;
        T.hits = nd->slam_pose->scan->T.hits;
        for (int i=0; i<9; i++)
            T.sigma[i] = nd->slam_pose->scan->T.sigma[i];
        
        nd->slam_pose->scan->applyTransform(T);
        sm->addScan(nd->slam_pose->scan, false);
    }

    sm->addScan(NULL, true);     
    
    BotTrans l_nd_to_local = getBotTransFromPose(last_nd->getPose());

    //we need the transform from this node to the closest node 
    BotTrans l_nd_to_target;
    
    bot_trans_apply_trans_to(&local_to_target, &l_nd_to_local, &l_nd_to_target);

    Pose2d tf = getPoseFromBotTrans(l_nd_to_target);

    ScanTransform T;
    T.x = tf.x();//value.x();
    T.y = tf.y();//value.y();
    T.theta = tf.t();//value.t();
    /*T.x = tocheck_value.x();
    T.y = tocheck_value.y();
    T.theta = tocheck_value.t();*/

    // applyTransform copies everything from T to last_nd->slam_pose->scan->T
    // so we should populate all of T first
    T.score = last_nd->slam_pose->scan->T.score;
    T.hits = last_nd->slam_pose->scan->T.hits;
    for (int i=0; i<9; i++)
        T.sigma[i] = last_nd->slam_pose->scan->T.sigma[i];
    

    last_nd->slam_pose->scan->applyTransform(T);

    ScanTransform lc_r = sm->gridMatch(last_nd->slam_pose->scan->points, last_nd->slam_pose->scan->numPoints,
                                       &last_nd->slam_pose->scan->T, 0.5, 0.5, M_PI/12.0);//1.0, 1.0, M_PI / 12.0);

    double hit_pct = lc_r.hits/ (double) last_nd->slam_pose->scan->numPoints;
    sm->clearScans(false);   

    //fprintf(stderr, RED "SM Result %.3f Transform : %f,%f,%f - Map transform : %f, %f, %f\n" RESET_COLOR, hit_pct, lc_r.x, lc_r.y, lc_r.theta, 
    //      tocheck_value.x(), tocheck_value.y(), tocheck_value.t());
    fprintf(stderr, RED "[%d] SM Result %.3f Delta Transform : %f,%f,%f\n" RESET_COLOR, graph_id, hit_pct, lc_r.x , lc_r.y , lc_r.theta);
            

    Matrix3d cov_sm = Matrix3d::Zero();
    //give higher cov - if the scanmatch failed
    if(hit_pct < 0.4){
        cov_sm(0,0) = 0.02;
        cov_sm(1,1) = 0.02;
        cov_sm(2,2) = 0.01;
    }
    else{
        cov_sm(0,0) = 0.00002;
        cov_sm(1,1) = 0.00002;
        cov_sm(2,2) = 0.00001;
    }
    Pose2d sm_tf(lc_r.x, lc_r.y, lc_r.theta);

    double prob = graph->calculateProbability(last_nd, closest_node, cov_sm, sm_tf);
    fprintf(stderr, "Probability : %f\n", prob);

    //for now 
    if(hit_pct < 0.2){
        hit_pct = 0.2;
    }

    //return hit_pct;//prob; //HACK - just to see if it makes particle diversity more reasonable*/
    
    if(prob < 0.2)
        prob = 0.2;
    return prob;
}

double SlamParticle::getProbOfMeasurementLikelihoodComplex(SlamNode *last_nd, double *maximum_probability){

    // Settings that determine behavior
    // The distance threshold used for kNN
    double radius_bound = 0.25; // Distance threshold for NN (types (i) and (iii))
    double point_match_threshold = 0.1; // Distance threshold for valid matches (type (i))
    double prob_2 = 0.05; // Probability associated with type 2 
    double prob_3 = 0.05; // Probability associated with type 3

    static int64_t total_time = 0;
    static int called_count = 0;

    //querry the kd tree - find the closest point 
    Pose2d tocheck_value = last_nd->getPose();
    Scan *tocheck_scan = last_nd->slam_pose->scan;
    double dist =0;
    double pos[2], next_pos[2];
    double start[2];

    // Node->Slam_Pose->Scan laser scans are decimated and in the body frame
    // We will generate the local occupancy grid in the Query Body frame
    BotTrans localToQueryBody;
    BotTrans queryBodyToLocal;
    queryBodyToLocal.trans_vec[0] = tocheck_value.x();
    queryBodyToLocal.trans_vec[1] = tocheck_value.y();
    queryBodyToLocal.trans_vec[2] = 0.0;
    double rpyQueryBody[3] = {0.0, 0.0, tocheck_value.t()};
    bot_roll_pitch_yaw_to_quat (rpyQueryBody, queryBodyToLocal.rot_quat);
    localToQueryBody = queryBodyToLocal;
    bot_trans_invert (&localToQueryBody);

    last_nd->resetProbability();

    double X1 = tocheck_value.x();
    double Y1 = tocheck_value.y();
    double T1 = tocheck_value.t();

    BotTrans matchBodyToLocal;
    BotTrans matchBodyToQueryBody;
    BotTrans queryBodyToMatchBody;

    double rpyMatchBody[3] = {0.0, 0.0, 0.0};

    int query_size = 1;
    double epsilon =  0.01;//0.1;
   
    ANNidxArray  nnIdx = new ANNidx[query_size];
    ANNdistArray dists = new ANNdist[query_size];

    ANNpoint qp = annAllocPt(2);

    int64_t s_utime = bot_timestamp_now();

    int64_t time_to_search = 0;

    int count = 0;
    // Find the nodes (and scans) near the local search area
    double pQuery[3] = {.0};
    double pLaser[3];
    double pQueryInMatched[3];

    int valid_trees = 0;
    int kd_searches = 0;
    int first = 1;

    double dist_threshold = 10.0;

    vector< pair<int,int> > matched_nodeid_pointidx;

    vector<SlamNode *> valid_nodes;
    //find the valid nodes to search - and cache the transform 
    for(int k = 0; k < last_nd->position; k++){
        SlamNode *nd = graph->getSlamNodeFromPosition(k);
        
        // Ignore scans with the same parent supernode
        if (nd->parent_supernode == last_nd->parent_supernode)
            continue;

        if ((last_nd->id - nd->id) < 10)
            continue;

        Pose2d value = nd->getPose();
        nd->valid = 0;
        Pose2d delta = tocheck_value.ominus(value);
        dist = hypot(delta.x(), delta.y());
        if(dist < dist_threshold){
            nd->valid = 1;
            
            ScanTransform T;
            T.x = value.x();
            T.y = value.y();
            T.theta = value.t();

            // Determine the transform from Match Body ---> Query Body
            
            // Set the BotTrans from Match BODY to LOCAL
            matchBodyToLocal.trans_vec[0] = value.x();
            matchBodyToLocal.trans_vec[1] = value.y();
            matchBodyToLocal.trans_vec[2] = 0.0;
            rpyMatchBody[2] = value.t();
            bot_roll_pitch_yaw_to_quat (rpyMatchBody, matchBodyToLocal.rot_quat);
            
            // ... and now Match Body ---> Query Body
            bot_trans_apply_trans_to (&localToQueryBody, &matchBodyToLocal, &matchBodyToQueryBody);
            nd->queryBodyToMatchBody = matchBodyToQueryBody;
            bot_trans_invert(&nd->queryBodyToMatchBody);
            valid_nodes.push_back(nd);
            
            //MatrixXd curr_cov = graph->getCovariances(last_nd, nd);
            
            //cov.insert(make_pair(nd->id, curr_cov));
        }
    }
    
    
    
    //fprintf(stderr, "Total Time taken : %f\n", total_time / 1.0e6 / called_count );

    map <int, MatrixXd>::iterator it;
    
    //vaid no of kd trees 
    valid_trees = valid_nodes.size();
    
    double cum_prob = 0;
    double avg_prob = 0;
    double max_prob = 0;  

    int no_of_matches = 0;

    map<int, SlamNode *> match_pt_map;

    // For each point in query scan, find the closest point among the scans of each nearby node (valid_nodes)
    for(unsigned i = 0; i < tocheck_scan->numPoints; i++){
        int my_no_matches = 0;
        int point_type = 0; // Either 1, 2, or 3
        
        pQuery[0] = tocheck_scan->points[i].x;
        pQuery[1] = tocheck_scan->points[i].y;

                     
        double pQueryLocal[3];
        double pMatchedLocal[3];

        // Transform the query point into the local frame
        bot_trans_apply_vec(&queryBodyToLocal, pQuery, pQueryLocal);

        //fprintf(stderr, "Querry Point in Local frame : %f,%f\n", pQueryLocal[0], pQueryLocal[1]);

        double min_dist = 1000; 
        int close_ind  = -1;
        double pMatched[3] = {0.}; 

        int match_id = -1;
        int match_idx = -1;
        // loop through the neighbor nodes to find the nearest scan point
        for(int k = 0; k < valid_nodes.size(); k++){
            SlamNode *nd = valid_nodes.at(k); 
            
            kd_searches++;

            // Transform the point from the query pose frame to the match frame
            bot_trans_apply_vec ( &nd->queryBodyToMatchBody, pQuery, pQueryInMatched);
                
            qp[0] = pQueryInMatched[0];
            qp[1] = pQueryInMatched[1];
            
            int64_t search_utime = bot_timestamp_now();

            nd->slam_pose->kd_tree->annkFRSearch (qp,                    // query point
                                                  ANN_POW(radius_bound), // squared radius search bound
                                                  query_size,            // number of near neighbors
                                                  nnIdx,                 // nearest neighbors (returned)
                                                  dists,                 // distance (returned)
                                                  epsilon);
            
            int64_t search_end_utime = bot_timestamp_now();
            time_to_search += (search_end_utime - search_utime);
            int found = 0;

            
            // Point is type 2 if there are no NN
            if (nnIdx[0] < 0)
                point_type = 2;
            else
                point_type = 3; // Assume outlier for now
            
            // Loop through the set of nearest scan points
            for(int l=0;l< query_size ;l++){
                if(nnIdx[l] < 0){//because we have reached the end of the valid neighbours
                    break; 
                    //continue;
                }
                
                double dist_val = sqrt(dists[l]); 
                
                // Doensn't pass the test to be type 1
                if (dist_val > point_match_threshold)
                    continue;

                //count++;
                found = 1;
                point_type = 1;
                no_of_matches ++;
                my_no_matches ++;
                //dists[l] = sqrt(dists[l]); // MRW: Commented out
                //cout << i << " " << nnIdx[l] << " " << dists[l] << "\n";

                //nnIdx[l];
                
                vector< pair<int,int> >::iterator pit;
                pair <int, int> test_id_idx(nd->id, nnIdx[(l)]);
                pit = find ( matched_nodeid_pointidx.begin(),  matched_nodeid_pointidx.end(), test_id_idx);

                if(min_dist > dist_val && pit ==  matched_nodeid_pointidx.end()){
                    min_dist = dist_val;
                    ANNpointArray kdtree_points = nd->slam_pose->kd_tree->thePoints();
                    pMatched[0] = (kdtree_points[nnIdx[(l)]][(0)]);
                    pMatched[1] = (kdtree_points[nnIdx[(l)]][(1)]);
                    pMatched[2] = 0.0;
                    close_ind = k;
                    
                    match_id = nd->id;
                    match_idx = nnIdx[(l)];
                }               
            }
        }

        double prob;

        // Did we find a match?
        // Point type 1
        if(close_ind >=0 && min_dist < 0.5){            
            pair <int, int> id_idx(match_id, match_idx);
            matched_nodeid_pointidx.push_back (id_idx);
            SlamNode *nd = valid_nodes.at(close_ind);
            match_pt_map.insert(pair<int, SlamNode *>(nd->id, nd));
        }
        else{
            pair <int, int> id_idx(-1, point_type);
            matched_nodeid_pointidx.push_back (id_idx);
        }
    }

    vector<SlamNode *> cov_querry_nodes;
    map<int, SlamNode *>::iterator s_it;
    for ( s_it= match_pt_map.begin() ; s_it != match_pt_map.end(); s_it++ ){        
        cov_querry_nodes.push_back(s_it->second);
    }

    map<int, MatrixXd> cov;
    if(cov_querry_nodes.size() == 0){
        fprintf(stderr, "Error - no valid close nodes\n");
    }
    else{
        called_count++;
        int64_t s_utime = bot_timestamp_now();
        cov = graph->getCovariances(last_nd, cov_querry_nodes);
        int64_t e_utime = bot_timestamp_now();
        total_time += (e_utime - s_utime); 
    }
    
    double pMatched[3] = {0.}; 
    double pQueryLocal[3];
    double pMatchedLocal[3];
    double prob = 0;
    for(int i =0; i < matched_nodeid_pointidx.size(); i++){
        pair<int, int> m_pt =  matched_nodeid_pointidx.at(i);

        if(m_pt.first >=0){
            
            SlamNode *nd = graph->getSlamNodeFromID(m_pt.first);//valid_nodes.at(m_pt.first);//close_ind);
            
            ANNpointArray kdtree_points = nd->slam_pose->kd_tree->thePoints();
            pMatched[0] = (kdtree_points[m_pt.second][(0)]);
            pMatched[1] = (kdtree_points[m_pt.second][(1)]);
            pMatched[2] = 0.0;
            
            MatrixXd qPoseMPoseCov;
            it = cov.find(nd->id);
            if(it == cov.end()){
                //qPoseMPoseCov = graph->getCovariances(last_nd, nd);
                //cov.insert(make_pair(nd->id, qPoseMPoseCov));

                fprintf(stderr, "Error - this should not have happened\n");
                continue;
            }
            else{
                qPoseMPoseCov = it->second;
            }

            //cout << "Covariance : \n" << it->second << endl;
            
            Pose2d match = nd->getPose();

            // Determine the transform from Match Body ---> Query Body
            
            // Set the BotTrans from Match BODY to LOCAL
            matchBodyToLocal.trans_vec[0] = match.x();
            matchBodyToLocal.trans_vec[1] = match.y();
            matchBodyToLocal.trans_vec[2] = 0.0;
            rpyMatchBody[2] = match.t();
            bot_roll_pitch_yaw_to_quat (rpyMatchBody, matchBodyToLocal.rot_quat);
            bot_trans_apply_vec (&matchBodyToLocal, pMatched, pMatchedLocal);

            
            if(param.drawScanmatch){
                bot_lcmgl_point_size (lcmgl_sm_graph, 3);
                bot_lcmgl_begin (lcmgl_sm_graph, GL_POINTS);
                bot_lcmgl_color3f (lcmgl_sm_graph, 0.0, 1.0, 0.0);
                bot_lcmgl_vertex3f (lcmgl_sm_graph, pQueryLocal[0], pQueryLocal[1], 0);
                bot_lcmgl_color3f (lcmgl_sm_graph, 1.0, 0.7, 0.0);
                bot_lcmgl_vertex3f (lcmgl_sm_graph, pMatchedLocal[0], pMatchedLocal[1], 0);
                bot_lcmgl_end (lcmgl_sm_graph);
                
                bot_lcmgl_color3f (lcmgl_sm_graph, 1.0, 0.0, 0.0);
                bot_lcmgl_line_width (lcmgl_sm_graph, 3);
                bot_lcmgl_begin (lcmgl_sm_graph, GL_LINES);
                bot_lcmgl_vertex3f (lcmgl_sm_graph, pQueryLocal[0], pQueryLocal[1], 0);
                bot_lcmgl_vertex3f (lcmgl_sm_graph, pMatchedLocal[0], pMatchedLocal[1], 0);
                bot_lcmgl_end (lcmgl_sm_graph);
            }

            //the point for last nd
            MatrixXd pQuerryCov = getLaserCov(pQuery);
            //the matched one 
            MatrixXd pMatchCov = getLaserCov(pMatched);



            prob = getObservationProbabilityComplex(qPoseMPoseCov, pQuerryCov, qPoseMPoseCov, 
                                                    pQuery, pMatched, tocheck_value, match);

            if(prob > 1.0)
                prob = 1.0;

            if(max_prob < prob)
                max_prob = prob;

            //saved in the slam node - to be used when updating 
            //last_nd->prob_of_scan[i] = full_prob;
            //fprintf (stdout,"\t\t my_no_matches = %d, valid_nodes.size() = %d\n", my_no_matches, valid_nodes.size());
            /*avg_prob += prob; //0.6 * prob + 0.4 * 0.2; 
              cum_prob += log(prob); //log(0.6 * prob + 0.4 * 0.2);// prob);*/
            //avg_prob += full_prob; //0.6 * prob + 0.4 * 0.2; 
            //cum_prob += log(full_prob);//0.6 * prob + 0.4 * 0.2);
            //fprintf(stderr, "\tProb : %f\n", prob);
            count++;

        }
        else{
            int point_type = m_pt.second;
            if (point_type == 3) 
                prob = prob_3;
            else
                prob = prob_2;

            //fprintf(stderr, "Not found => Min dist : %f Prob : %f\n", min_dist, 0.4 * 0.8);
            //avg_prob += 0.5;//0.4 * 0.8; 
            //cum_prob += log(0.5);//0.4 * 0.8);
            //avg_prob += 1/(valid_nodes.size()+1);
            //cum_prob += log(1/(valid_nodes.size()+1));
            //last_nd->prob_of_scan[i] =  0.5;//0.4 * 0.8;//0; 
            //last_nd->prob_of_scan[i] =  1/(valid_nodes.size()+1);//0.4 * 0.8;//0; 
        }

        avg_prob += prob;
        cum_prob += log (prob);
        last_nd->prob_of_scan[i] = prob;

    }

    fprintf(stderr, "No of Valid nodes : %d - No of Matched Nodes : %d\n", (int) valid_nodes.size(), (int) match_pt_map.size());

    //fprintf(stderr, "\t\t [%d] Slam Graph No of matches : %d\n", graph_id, no_of_matches);
    
    last_nd->max_scan_prob = max_prob; 
    *maximum_probability = max_prob;
    avg_prob /= tocheck_scan->numPoints;//count, 
        
    if(param.verbose)
        fprintf(stderr, "Count : %d Max Prob : %f Avg : %f Cumalative Prob : (log) %f => %f \n", 
                count, max_prob, avg_prob, 
                cum_prob, exp(cum_prob));
 
    annDeallocPt(qp);
    delete [] nnIdx;
    delete [] dists; 
    int64_t e_utime = bot_timestamp_now();
    if(param.verbose){
        fprintf(stderr, "\t Total : %d => Matched Count : %d\n", tocheck_scan->numPoints, count);
        fprintf(stderr, "\t Valid Trees : %d => Total Searches : %d Time per search : %f\n", valid_trees, kd_searches, (time_to_search) / 1.0e6 / (double) kd_searches);
        fprintf(stderr, "Time to serach kd tree : %f\n", (time_to_search) / 1.0e6);
    }
    
    bot_lcmgl_switch_buffer (lcmgl_sm_graph);
    cum_prob -= log(max_prob) * count; 
    //return exp(cum_prob);
    return avg_prob; //exp(cum_prob); //avg_prob / count; //exp(cum_prob); //count / (double) tocheck_scan->numPoints;
}


// TODO: Go through calculation of probability
//
// Matt - I updated this to update the weights based on the pofz returned from getProbOfMeasurementLikelihoodCount
void SlamParticle::calculateObservationProbability(int probMode, double *maximum_probability){
    
    static int64_t time_taken_1 = 0;
    static int64_t time_taken_2 = 0;
    static int times_called = 0;
    
    if(probMode != 0){
        for(int k =0; k < unprocessed_slam_nodes.size(); k++){
            SlamNode *node1 = unprocessed_slam_nodes.at(k);
            double max_prob = 0;
            // Populates unnormalized pofz for each lidar return
            int64_t s_time = bot_timestamp_now();
            //this one seems to give pretty different values 
            double pofz_new = 0;
            //double pofz_new = getProbOfMeasurementLikelihoodComplex(node1, &max_prob);//
            int64_t e_time_1 = bot_timestamp_now();
            double pofz = getProbOfMeasurementLikelihoodCount(node1, &max_prob);
            int64_t e_time_2 = bot_timestamp_now();

            time_taken_1 += (e_time_1 - s_time);
            time_taken_2 += (e_time_2 - e_time_1);
            times_called++;
            
            fprintf(stderr, "P new : %f P Old : %f\n", pofz_new, pofz);
            //fprintf(stderr, "Time Taken - New : %f Old : %f\n", (e_time_1 - s_time) / 1.0e6, 
            //      (e_time_2 - e_time_1) / 1.0e6);
            fprintf(stderr, "Avg Time Taken - New : %f Old : %f\n", (time_taken_1) / 1.0e6 / times_called, 
                    (time_taken_2) / 1.0e6 / times_called);

            double log_pofz = log(pofz);
            weight += log_pofz; 

            //getProbOfMeasurementLikelihood(node1, &max_prob);
            
            if(node1->slam_pose->max_prob < max_prob){
                node1->slam_pose->max_prob = max_prob;
            }
            
            //now this is not used 
            node1->pofz = pofz;
            *maximum_probability = max_prob; 
        }
    }
    else{
        //this method is now only valid for 
        //deprecated        
        updateWeight(probMode, unprocessed_slam_nodes);
    }

    //we should clear the unprocessed nodes (since they have been processed) 
    if(param.verbose)
        fprintf(stderr, "Clearing the processed nodes\n");
    unprocessed_slam_nodes.clear();

    return;

    //hmm - right now this already assumes that the pofz's have been calculated - prob should do it seperately
    //because the 
    if(probMode == 0)
        return;
    
    //mode 1 will average the weight 
    if(probMode == 1){
        for(int i=0; i< graph->slam_node_list.size(); i++){
            //create the nodes 
            SlamNode *nd = (SlamNode *) graph->slam_node_list.at(i);
            
            if(nd->prob_used)
                continue; 
            double max_prob = nd->slam_pose->max_prob; 
            if(max_prob == 0)
                max_prob = 0.1;
            nd->prob_used = 1;
            double log_pofz = 0;
            double avg_pofz = 0;
            double p = 0;
            for(int k=0; k < nd->slam_pose->scan->numPoints; k++){
                //normalize 
                p = (nd->prob_of_scan[k]/max_prob);
                /*if(p < 0.2)
                  p = 0.2;*/
                log_pofz += log(p);
                avg_pofz += p;
            }
            //to take the average 
            log_pofz = log(avg_pofz) - log((double) nd->slam_pose->scan->numPoints);

            
            //the straight up joint prob jumps too much one way or the other - Matt - debug please :) 
            if(param.verbose)
                fprintf(stderr, "[%d] %d => Total Probability : log : %f => %f\n", graph_id , i, 
                        log_pofz, exp(log_pofz));
            /*if (ret < 0)
              assert(false);*/
            weight += log_pofz; 
        }
    }
    
    //mode 2 will use the product - theoritically correct - pratically sucks 
    if(probMode == 2){
        for(int i=0; i< graph->slam_node_list.size(); i++){
            //create the nodes 
            SlamNode *nd = (SlamNode *) graph->slam_node_list.at(i);

            if(nd->prob_used)
                continue; 
            double max_prob = nd->slam_pose->max_prob; 
            if(max_prob == 0)
                max_prob = 0.1;
            nd->prob_used = 1;
            double log_pofz = 0;
            double avg_pofz = 0;
            double p = 0;
            for(int k=0; k < nd->slam_pose->scan->numPoints; k++){
                //normalize 
                p = (nd->prob_of_scan[k]/max_prob);
                /*if(p < 0.2)
                  p = 0.2;*/
                log_pofz += log(p);
                avg_pofz += p;
            }
            //to take the average 
            //log_pofz = log(avg_pofz) - log((double) nd->slam_pose->scan->numPoints);

            if (param.verbose)
                fprintf(stderr, "Prob log : %f -> %f\n", log_pofz, exp(log_pofz)); 
        
            //the straight up joint prob jumps too much one way or the other - Matt - debug please :) 
            int ret = fprintf(stderr, "[%d] %d => Total Probability : log : %f => %f\n", graph_id , i, 
                              log_pofz, exp(log_pofz));
            /*if (ret < 0)
              assert(false);*/
            weight += log_pofz; 
        }
    }
}

void SlamParticle::processMeasurementLikelihood(int probMode){
    //hmm - right now this already assumes that the pofz's have been calculated - prob should do it seperately
    //because the 
    if(probMode == 0)
        return;
    
    //mode 1 will average the weight 
    if(probMode == 1){
        for(int i=0; i< graph->slam_node_list.size(); i++){
            //create the nodes 
            SlamNode *nd = (SlamNode *) graph->slam_node_list.at(i);
            
            if(nd->prob_used)
                continue; 
            double max_prob = nd->slam_pose->max_prob; 
            if(max_prob == 0)
                max_prob = 0.1;
            nd->prob_used = 1;
            double log_pofz = 0;
            double avg_pofz = 0;
            double p = 0;
            for(int k=0; k < nd->slam_pose->scan->numPoints; k++){
                //normalize 
                p = (nd->prob_of_scan[k]/max_prob);
                /*if(p < 0.2)
                  p = 0.2;*/
                log_pofz += log(p);
                avg_pofz += p;
            }
            //to take the average 
            log_pofz = log(avg_pofz) - log((double) nd->slam_pose->scan->numPoints);

            
            //the straight up joint prob jumps too much one way or the other - Matt - debug please :) 
            int ret = fprintf(stderr, "[%d] %d => Total Probability : log : %f => %f\n", graph_id , i, 
                              log_pofz, exp(log_pofz));
            /*if (ret < 0)
              assert(false);*/
            weight += log_pofz; 
        }
    }
    
    //mode 2 will use the product - theoritically correct - pratically sucks 
    if(probMode == 2){
        for(int i=0; i< graph->slam_node_list.size(); i++){
            //create the nodes 
            SlamNode *nd = (SlamNode *) graph->slam_node_list.at(i);

            if(nd->prob_used)
                continue; 
            double max_prob = nd->slam_pose->max_prob; 
            if(max_prob == 0)
                max_prob = 0.1;
            nd->prob_used = 1;
            double log_pofz = 0;
            double avg_pofz = 0;
            double p = 0;
            for(int k=0; k < nd->slam_pose->scan->numPoints; k++){
                //normalize 
                p = (nd->prob_of_scan[k]/max_prob);
                /*if(p < 0.2)
                  p = 0.2;*/
                log_pofz += log(p);
                avg_pofz += p;
            }
            //to take the average 
            //log_pofz = log(avg_pofz) - log((double) nd->slam_pose->scan->numPoints);

            if (param.verbose)
                fprintf(stderr, "Prob log : %f -> %f\n", log_pofz, exp(log_pofz)); 
        
            //the straight up joint prob jumps too much one way or the other - Matt - debug please :) 
            int ret = fprintf(stderr, "[%d] %d => Total Probability : log : %f => %f\n", graph_id , i, 
                              log_pofz, exp(log_pofz));
            /*if (ret < 0)
              assert(false);*/
            weight += log_pofz; 
        }
    }    
}

double SlamParticle::getObservationProbabilityOld(MatrixXd cov_match_in_particle_frame, MatrixXd cov_querry_in_particle_frame, 
                                                  double pMatch[2], double pQuery[2]){
    //these points need to be in the particle frame - not their body frame 

    //will this have the same issue as the edge creation stuff???
    //in which case - we should evaluvate the two transforms - not the distances 
    
    //i.e. (x_match-x_querry) , (y_match - y_querry)

    MatrixXd H(1,4);
    
    H.setZero();
    
    H(0,0) = 2 * (pMatch[0] - pQuery[0]);
    H(0,1) = 2 * (pMatch[1] - pQuery[1]);
    H(0,2) = -2 * (pMatch[0] - pQuery[0]);
    H(0,3) = -2 * (pMatch[1] - pQuery[1]);

    if(0){
        fprintf(stderr, "Diff : %f,%f Match : %f, %f => Querry : %f,%f\n", 
                pMatch[0] - pQuery[0], 
                pMatch[1] - pQuery[1], 
                pMatch[0], pMatch[1], 
                pQuery[0], pQuery[1]);
    }

    double mean[2] = {pMatch[0] - pQuery[0], pMatch[1] - pQuery[1]};

    //build the full covariance 
    MatrixXd point_cov(4,4);
    point_cov.setZero(4,4);
    for(int i=0; i < 2; i++){
        for(int j=0; j < 2; j++){
            point_cov(i,j) = cov_match_in_particle_frame(i,j);
        }
    }

    for(int i=0; i < 2; i++){
        for(int j=0; j < 2; j++){
            point_cov(i+2,j+2) = cov_querry_in_particle_frame(i,j);
        }
    }

    MatrixXd cov = H * point_cov * H.transpose();
        
    //evaluate the probability 
    double deter = cov.determinant();

    MatrixXd dx(1,1);
    dx(0,0) = pow(mean[0],2) + pow(mean[1],2);

    //cout << "Z Score : " << dx(0,0) << ", " << dx(0,1)  << endl;
    //cout << "Cov " << cov <<endl;
    
    MatrixXd mh_dist = dx.transpose() * cov.inverse() * dx;

    //cout << "MH dist : " << mh_dist(0,0) << "Det " << deter <<  endl;
    //applying to gaussian probability 
    double prob = exp(-mh_dist(0,0)/2)/ ((2 * M_PI) * pow(deter, 0.5));

    double full_prob = 0.5 * prob;



    return full_prob;
}

double SlamParticle::getObservationProbabilityComplex(MatrixXd qPoseMPoseCov, MatrixXd pQuerryCov, MatrixXd pMatchCov, 
                                                      double qLaserPos[2], double mLaserPos[2], 
                                                      Pose2d qPose, Pose2d mPose){

    MatrixXd H(1,10);

    double X1 = qPose.x();
    double Y1 = qPose.y();
    double T1 = qPose.t();

    double X2 = mPose.x();
    double Y2 = mPose.y();
    double T2 = mPose.t();
    
    double x1 = qLaserPos[0];
    double y1 = qLaserPos[1];

    double x2 = mLaserPos[0];
    double y2 = mLaserPos[1];

    double x_q = X1 + x1 * cos(T1) - y1 * sin(T1);
    double y_q = Y1 + x1 * sin(T1) + y1 * cos(T1);

    double x_m = X2 + x2 * cos(T1) - y2 * sin(T1);
    double y_m = Y2 + x2 * sin(T1) + y2 * cos(T1);

    double D = hypot((x_q - x_m), (y_q-y_m));
    if(D == 0) 
        D = 1.0e-5;
    
    double dX = (x_q - x_m)/D;
    double dY = (y_q - y_m)/D;

    H(0,0) = dX * cos(T1) + dY * sin(T1);
    H(0,1) = -dX * sin(T1) + dY * cos(T1);
    H(0,2) = dX;
    H(0,3) = dY; 
    H(0,4) = dX * (-x1* sin(T1) - y1 * cos(T1)) + dY * (x1 * cos(T1) - y1*sin(T1));
    H(0,5) = -dX;
    H(0,6) = -dY; 
    H(0,7) = -(dX * (-x2* sin(T2) - y2 * cos(T2)) + dY * (x2 * cos(T2) - y2*sin(T2)));
    H(0,8) = -(dX * cos(T2) + dY * sin(T2));
    H(0,9) = -(-dX * sin(T2) + dY * cos(T2));
    
    MatrixXd Cov(10,10);
    Cov.setZero(10,10);

    for(int i=0; i < 2; i++){
        for(int j=0; j < 2; j++){
            Cov(i, j) = pQuerryCov(i,j);
        }
    }
    
    for(int i=0; i < 6; i++){
        for(int j=0; j < 6; j++){
            Cov(i+2, j+2) = qPoseMPoseCov(i,j);
        }
    }
        
    for(int i=0; i < 2; i++){
        for(int j=0; j < 2; j++){
            Cov(i+8, j+8) = pMatchCov(i,j);
        }
    }

    //cout << "Input Cov :\n" << Cov << endl;
    //cout << "Jacobian : \n" << H << endl;
    
    MatrixXd R = H * Cov * H.transpose();

    //cout << "Result :" <<  R << endl;
    double full_prob = get_observation_probability(D, pow(R(0,0),0.5));

    //cout << "Prob : " << full_prob << endl;
    
    return full_prob;
}


//use a fixed covariance function
double SlamParticle::getObservationProbabilityFixed(double pMatch[2], double pQuery[2]){
    double mean[2] = {pMatch[0] - pQuery[0], pMatch[1] - pQuery[1]};

    MatrixXd cov =  MatrixXd::Zero(2,2);
    cov(0,0) = 0.1;
    cov(0,1) = cov(1,0) = 0.02;
    cov(1,1) = 0.1;

    //evaluate the probability 
    double deter = cov.determinant();

    MatrixXd dx(2,1);
    dx(0,0) = (0-mean[0]);
    dx(1,0) = (0-mean[1]);

    //cout << "Z Score : " << dx(0,0) << ", " << dx(0,1)  << endl;
    //cout << "Cov " << cov <<endl;
    
    MatrixXd mh_dist = dx.transpose() * cov.inverse() * dx;

    //cout << "MH dist : " << mh_dist(0,0) << "Det " << deter <<  endl;
    //applying to gaussian probability 
    double prob = exp(-mh_dist(0,0)/2)/ ((2 * M_PI) * pow(deter, 0.5));

    double full_prob = 0.5 * prob;
    return full_prob;
}

void SlamParticle::updateWeight(int probMode, vector<SlamNode *> nodes_to_process){
    if(probMode == 0){
        map<int, SlamConstraint *>::iterator c_it;
        for ( c_it= graph->slam_constraints.begin() ; c_it != graph->slam_constraints.end(); c_it++ ){     
            SlamConstraint *edge = (SlamConstraint *) c_it->second;

            if(!edge->processed && edge->type == SLAM_GRAPH_EDGE_T_TYPE_SM_LC && edge->status == SLAM_GRAPH_EDGE_T_STATUS_SUCCESS){
                double prob = edge->pofz;
                if(prob == 0){
                    prob = 0.0000000001;
                    fprintf(stderr, "XXXXXXXXXXXXXXXX Error - Edge Probability set to zero XXXXXXXXXXXXXXXXXX\n");
                }
                if(prob == -1){
                    fprintf(stderr, "XXXXXXXXXXXXXXXX Error - Edge Probability not set XXXXXXXXXXXXXXXXXX\n");
                    prob = 0.0000000001;
                }
                
                prob = prob/1000;
                
                //fprintf(stderr, MAKE_GREEN "Particle reweighting factor: %f" RESET_COLOR "\n", prob);
                
                weight += log(prob);
                edge->processed = 1;
            }
        }
    }
    
    //the problem here is that in this way - the probability can't be calculated here - it needs to be 
    //done before the new constraints are added - so needs to go to the update_particle section 
    if(probMode == 1 && nodes_to_process.size() > 0){
        for(int i=0; i<  nodes_to_process.size(); i++){
            SlamNode *nd = nodes_to_process.at(i);
            double prob = nd->pofz;
            if(prob == 0){
                prob = 0.0000000001;
            }
            weight += log(prob);
            if(param.verbose)
                fprintf(stderr, "\tWeight %f\n", weight);
        }
    }
}


MatrixXd SlamParticle::getLaserCov(double pBody[3]){
    double r_var = pow(0.3, 2);
    double t_var = pow(0.2, 2);

    double x_laser_offset = laser_to_body.trans_vec[0];
    double y_laser_offset = laser_to_body.trans_vec[1];
    double rpy_laser_offset[3];
    bot_quat_to_roll_pitch_yaw (laser_to_body.rot_quat, rpy_laser_offset);
    double theta_laser_offset = rpy_laser_offset[2];
    double c_l = cos(theta_laser_offset);
    double s_l = sin(theta_laser_offset);
    
    double pLaser[3];
    //calculate the theta in the laser frame         
    bot_trans_apply_vec (&body_to_laser, pBody, pLaser);

    double r = hypot(pLaser[0] , pLaser[1]);
    double theta = atan2(pLaser[1], pLaser[0]);
    double c = cos(theta);
    double s = sin(theta);
    // Covariance of (x,y) in laser frame 
    MatrixXd cov_laser(2,2); 
    
    //this assumes a variance on both r and theta 
    Matrix2d H0  = Matrix2d::Zero();
    H0(0,0) = c_l;
    H0(1,0) = s_l;
    H0(0,1) = -r * s_l;
    H0(1,1) = r * c_l;

    MatrixXd cov_l(2,2);
    cov_l.setZero();
    cov_l(0,0) = r_var;
    cov_l(1,1) = t_var;
    
    cov_laser = H0 * cov_l * H0.transpose();
    
    MatrixXd H1(2,2); 
    H1(0,0) = H1(1,1) = c_l;
    H1(0,1) = - s_l;
    H1(1,0) = s_l;

    //cov of the query laser in body frame
    MatrixXd cov_body_querry = H1 * cov_laser * H1.transpose();
    return cov_body_querry; 
}


//Sachi - this is the model used right now 
// this model calculates the covariances between the two laser points ( querry point for the last scan) 
// and the matched point 
double SlamParticle::getProbOfMeasurementLikelihood(SlamNode *last_nd, double *maximum_probability){

    //querry the kd tree - find the closest point 
    Pose2d tocheck_value = last_nd->getPose();
    Scan *tocheck_scan = last_nd->slam_pose->scan;
    double dist =0;
    double pos[2], next_pos[2];
    double start[2];

    // Node->Slam_Pose->Scan laser scans are decimated and in the body frame
    // We will generate the local occupancy grid in the Query Body frame
    BotTrans localToQueryBody;
    BotTrans queryBodyToLocal;
    queryBodyToLocal.trans_vec[0] = tocheck_value.x();
    queryBodyToLocal.trans_vec[1] = tocheck_value.y();
    queryBodyToLocal.trans_vec[2] = 0.0;
    double rpyQueryBody[3] = {0.0, 0.0, tocheck_value.t()};
    bot_roll_pitch_yaw_to_quat (rpyQueryBody, queryBodyToLocal.rot_quat);
    localToQueryBody = queryBodyToLocal;
    bot_trans_invert (&localToQueryBody);

    last_nd->resetProbability();

    static int64_t total_time = 0;

    BotTrans matchBodyToLocal;
    BotTrans matchBodyToQueryBody;
    BotTrans queryBodyToMatchBody;

    double rpyMatchBody[3] = {0.0, 0.0, 0.0};

    double radius_bound = 0.2;//0.05;//0.3; //was 0.05 
    int query_size = 1;
    double epsilon =  0.05;//0.1;
   
    ANNidxArray  nnIdx = new ANNidx[query_size];
    ANNdistArray dists = new ANNdist[query_size];

    ANNpoint qp = annAllocPt(2);

    int64_t s_utime = bot_timestamp_now();

    int64_t time_to_search = 0;

    int count = 0;
    // Find the nodes (and scans) near the local search area
    double pQuery[3] = {.0};
    double pLaser[3];
    double pQueryInMatched[3];

    int valid_trees = 0;
    int kd_searches = 0;
    int first = 1;

    double dist_threshold = 10.0;

    vector<SlamNode *> valid_nodes;
    //find the valid nodes to search - and cache the transform 
    for(int k = 0; k < last_nd->position; k++){
        SlamNode *nd = graph->getSlamNodeFromPosition(k);
        Pose2d value = nd->getPose();
        nd->valid = 0;
        Pose2d delta = tocheck_value.ominus(value);
        dist = hypot(delta.x(), delta.y());
        if(dist < dist_threshold){
            nd->valid = 1;
            
            ScanTransform T;
            T.x = value.x();
            T.y = value.y();
            T.theta = value.t();

            // Determine the transform from Match Body ---> Query Body
            
            // Set the BotTrans from Match BODY to LOCAL
            matchBodyToLocal.trans_vec[0] = value.x();
            matchBodyToLocal.trans_vec[1] = value.y();
            matchBodyToLocal.trans_vec[2] = 0.0;
            rpyMatchBody[2] = value.t();
            bot_roll_pitch_yaw_to_quat (rpyMatchBody, matchBodyToLocal.rot_quat);
            
            // ... and now Match Body ---> Query Body
            bot_trans_apply_trans_to (&localToQueryBody, &matchBodyToLocal, &matchBodyToQueryBody);
            nd->queryBodyToMatchBody = matchBodyToQueryBody;
            bot_trans_invert(&nd->queryBodyToMatchBody);
            valid_nodes.push_back(nd);
        }
    }

    //vaid no of kd trees 
    valid_trees = valid_nodes.size();
    
    double cum_prob = 0;
    double avg_prob = 0;
    double max_prob = 0;  

    // For each point in query scan, find the closest point among the scans of each nearby node (valid_nodes)
    for(unsigned i = 0; i < tocheck_scan->numPoints; i++){
        
        pQuery[0] = tocheck_scan->points[i].x;
        pQuery[1] = tocheck_scan->points[i].y;
               
        double pQueryLocal[3];
        double pMatchedLocal[3];

        // Transform the query point into the local frame
        bot_trans_apply_vec(&queryBodyToLocal, pQuery, pQueryLocal);

        //fprintf(stderr, "Querry Point in Local frame : %f,%f\n", pQueryLocal[0], pQueryLocal[1]);

        double min_dist = 1000; 
        int close_ind  = -1;
        double pMatched[3] = {0.}; 

        // loop through the neighbor nodes to find the nearest scan point
        for(int k = 0; k < valid_nodes.size(); k++){
            SlamNode *nd = valid_nodes.at(k); 
            
            kd_searches++;

            // Transform the point from the query pose frame to the match frame
            bot_trans_apply_vec ( &nd->queryBodyToMatchBody, pQuery, pQueryInMatched);
                
            qp[0] = pQueryInMatched[0];
            qp[1] = pQueryInMatched[1];
            
            int64_t search_utime = bot_timestamp_now();

            nd->slam_pose->kd_tree->annkFRSearch (qp,                    // query point
                                                  ANN_POW(radius_bound), // squared radius search bound
                                                  query_size,            // number of near neighbors
                                                  nnIdx,                 // nearest neighbors (returned)
                                                  dists,                 // distance (returned)
                                                  epsilon);
            
            int64_t search_end_utime = bot_timestamp_now();
            time_to_search += (search_end_utime - search_utime);
            int found = 0;

            for(int l=0;l< query_size ;l++){
                if(nnIdx[l] < 0){//because we have reached the end of the valid neighbours
                    break; 
                    //continue;
                }
                //count++;
                found = 1;
                //dists[l] = sqrt(dists[l]); // MRW: Commented out
                double dist_val = sqrt(dists[l]); 
                //cout << i << " " << nnIdx[l] << " " << dists[l] << "\n";

                //nnIdx[l];
                
                if(min_dist > dist_val){
                    min_dist = dist_val;
                    ANNpointArray kdtree_points = nd->slam_pose->kd_tree->thePoints();
                    pMatched[0] = (kdtree_points[nnIdx[(l)]][(0)]);
                    pMatched[1] = (kdtree_points[nnIdx[(l)]][(1)]);
                    pMatched[2] = 0.0;
                    close_ind = k;
                }               
            }
        }

        //fprintf(stderr, "Distance - recal : %f\n", hypot(pMatched[0] - pQueryInMatched[0], pMatched[1] - pQueryInMatched[1]));
        //fprintf(stderr, "Min Tree : %d\n", close_ind);
        
        // Did we find a match?
        if(close_ind >=0 && min_dist < 0.5){
            SlamNode *nd = valid_nodes.at(close_ind);

            Pose2d match = nd->getPose();

            // Determine the transform from Match Body ---> Query Body
            
            // Set the BotTrans from Match BODY to LOCAL
            matchBodyToLocal.trans_vec[0] = match.x();
            matchBodyToLocal.trans_vec[1] = match.y();
            matchBodyToLocal.trans_vec[2] = 0.0;
            rpyMatchBody[2] = match.t();
            bot_roll_pitch_yaw_to_quat (rpyMatchBody, matchBodyToLocal.rot_quat);
            bot_trans_apply_vec (&matchBodyToLocal, pMatched, pMatchedLocal);

            int64_t s_t = bot_timestamp_now();
            
            //get the covariance of the scan point based on the uncertainity of the 
            MatrixXd cov_querry_in_particle_frame = getObservationCovarianceInParticleFrame(pQuery, last_nd);
            int64_t e_t = bot_timestamp_now();
            
            //fprintf(stderr, "Matched Point in Local frame : %f,%f\n", pMatchedLocal[0], pMatchedLocal[1]);

            //fprintf(stderr, "Dist : %f\n", hypot(pMatchedLocal[0] - pQueryLocal[0] , pMatchedLocal[1] - pQueryLocal[1]));
            
            MatrixXd cov_match_in_particle_frame = getObservationCovarianceInParticleFrame(pMatched, nd);
            //looks reasonable enough 
            //cout << endl << "Match Cov : " << endl << cov_match_in_particle_frame << endl; 
            double prob = getObservationProbability(cov_match_in_particle_frame, cov_querry_in_particle_frame, 
                                                    pMatchedLocal, pQueryLocal);
            //this is the fixed model - wont take in to account the cov of the nodes 
            
            //double prob =  getObservationProbabilityFixed(pMatchedLocal, pQueryLocal);

            //fprintf(stderr, "Found => Min dist : %f Prob : %f\n", min_dist, prob);
            
            if(prob > 1.0)
                prob = 1.0;

            if(prob < 0.5) 
                prob = 0.5; 

            double full_prob = prob; // 0.6 * prob + 0.4 * 0.2
            
            //fprintf(stderr, "Found => Min dist : %f Prob : %f\n", min_dist, prob);

            if(max_prob < full_prob){
                max_prob = full_prob;
            }
            //saved in the slam node - to be used when updating 
            last_nd->prob_of_scan[i] = full_prob;
            /*avg_prob += prob; //0.6 * prob + 0.4 * 0.2; 
              cum_prob += log(prob); //log(0.6 * prob + 0.4 * 0.2);// prob);*/
            
            avg_prob += full_prob; //0.6 * prob + 0.4 * 0.2; 
            cum_prob += log(full_prob);//0.6 * prob + 0.4 * 0.2);
            //fprintf(stderr, "\tProb : %f\n", prob);
            count++;

        }
        else{
            //fprintf(stderr, "Not found => Min dist : %f Prob : %f\n", min_dist, 0.4 * 0.8);
            avg_prob += 0.5;//0.4 * 0.8; 
            cum_prob += log(0.5);//0.4 * 0.8);
            last_nd->prob_of_scan[i] =  0.5;//0.4 * 0.8;//0; 
        }
    }
    
    last_nd->max_scan_prob = max_prob; 
    *maximum_probability = max_prob;
    avg_prob /= tocheck_scan->numPoints;//count, 
    
    if(param.verbose)
        fprintf(stderr, "Count : %d Max Prob : %f Avg : %f Cumalative Prob : (log) %f => %f \n", 
                count, max_prob, avg_prob, 
                cum_prob, exp(cum_prob));
 
    annDeallocPt(qp);
    delete [] nnIdx;
    delete [] dists; 
    int64_t e_utime = bot_timestamp_now();
    if(param.verbose){
        fprintf(stderr, "\t Total : %d => Matched Count : %d\n", tocheck_scan->numPoints, count);
        fprintf(stderr, "\t Valid Trees : %d => Total Searches : %d Time per search : %f\n", valid_trees, kd_searches, (time_to_search) / 1.0e6 / (double) kd_searches);
        fprintf(stderr, "Time to serach kd tree : %f\n", (time_to_search) / 1.0e6);
    }
    
    cum_prob -= log(max_prob) * count; 
   
    return exp(cum_prob);
    //return avg_prob; //exp(cum_prob); //avg_prob / count; //exp(cum_prob); //count / (double) tocheck_scan->numPoints;
}

void SlamParticle::calculateObservationProbabilityOfRegion(RegionNode *region, double *maximum_probability){
    //for each node in the region (or maybe the nodes that were not already processed??
    //maybe the region and then scanmatch is not using the same information as the vanilar laser points 
    double total_prob = 0;
    for(int i=0; i < region->nodes.size(); i++){
        SlamNode *nd = region->nodes[i];
        //log likelihood
        double pofz = getProbOfMeasurementLikelihoodRegion(nd, maximum_probability);        
        fprintf(stderr, "Log likelihood - %f  => Prob : %f\n", log(pofz), pofz);
        total_prob += log(pofz);      
    }
    fprintf(stderr, "Total for region => Log likelihood - %f  => Prob : %f\n", total_prob, exp(total_prob));
    weight += total_prob; 

    //this will have implications if we resample - since the weights of the particles are not synchronized - i.e. some of them might not be calculated yet
}







//Sachi - this is the model used right now 
// this model calculates the covariances between the two laser points ( querry point for the last scan) 
// and the matched point 
//
// Matt - This is how it works now;
//        We consider three cases:
//         (i)   Scan point is within a small threshold of existing point. The likelihood is evaluated via covariance calculation
//         (ii)  Scan point corresponds to an unobserved part of the environment
//         (iii) Scan point has a match, but is twoo far away
//
//        For a given point we find the kNN among known scans for some threshold radius > point_match_threshold.
//        If a point has no NN, we assume it is of type (ii) and assign a fixed likelihood
//        If a point has a NN that is within point_match_threshold, we assume it is type (i)
//        If a point has a NN but is farther than point_match_threshold, we assume it is type (iii)

double SlamParticle::getProbOfMeasurementLikelihoodCount(SlamNode *last_nd, double *maximum_probability){

    // Settings that determine behavior
    // The distance threshold used for kNN
    double radius_bound = 0.25; // Distance threshold for NN (types (i) and (iii))
    double point_match_threshold = 0.1; // Distance threshold for valid matches (type (i))
    double prob_2 = 0.05; // Probability associated with type 2 
    double prob_3 = 0.05; // Probability associated with type 3

    //querry the kd tree - find the closest point 
    Pose2d tocheck_value = last_nd->getPose();
    Scan *tocheck_scan = last_nd->slam_pose->scan;
    double dist =0;
    double pos[2], next_pos[2];
    double start[2];

    // Node->Slam_Pose->Scan laser scans are decimated and in the body frame
    // We will generate the local occupancy grid in the Query Body frame
    BotTrans localToQueryBody;
    BotTrans queryBodyToLocal;
    queryBodyToLocal.trans_vec[0] = tocheck_value.x();
    queryBodyToLocal.trans_vec[1] = tocheck_value.y();
    queryBodyToLocal.trans_vec[2] = 0.0;
    double rpyQueryBody[3] = {0.0, 0.0, tocheck_value.t()};
    bot_roll_pitch_yaw_to_quat (rpyQueryBody, queryBodyToLocal.rot_quat);
    localToQueryBody = queryBodyToLocal;
    bot_trans_invert (&localToQueryBody);

    last_nd->resetProbability();

    BotTrans matchBodyToLocal;
    BotTrans matchBodyToQueryBody;
    BotTrans queryBodyToMatchBody;

    double rpyMatchBody[3] = {0.0, 0.0, 0.0};

    int query_size = 1;
    double epsilon =  0.01;//0.1;
   
    ANNidxArray  nnIdx = new ANNidx[query_size];
    ANNdistArray dists = new ANNdist[query_size];

    ANNpoint qp = annAllocPt(2);

    int64_t s_utime = bot_timestamp_now();

    int64_t time_to_search = 0;

    int count = 0;
    // Find the nodes (and scans) near the local search area
    double pQuery[3] = {.0};
    double pLaser[3];
    double pQueryInMatched[3];

    int valid_trees = 0;
    int kd_searches = 0;
    int first = 1;

    double dist_threshold = 10.0;

    vector< pair<int,int> > matched_nodeid_pointidx;

    vector<SlamNode *> valid_nodes;
    //find the valid nodes to search - and cache the transform 
    for(int k = 0; k < last_nd->position; k++){
        SlamNode *nd = graph->getSlamNodeFromPosition(k);
        
        // Ignore scans with the same parent supernode
        if (nd->parent_supernode == last_nd->parent_supernode)
            continue;

        if ((last_nd->id - nd->id) < 10)
            continue;

        Pose2d value = nd->getPose();
        nd->valid = 0;
        Pose2d delta = tocheck_value.ominus(value);
        dist = hypot(delta.x(), delta.y());
        if(dist < dist_threshold){
            nd->valid = 1;
            
            ScanTransform T;
            T.x = value.x();
            T.y = value.y();
            T.theta = value.t();

            // Determine the transform from Match Body ---> Query Body
            
            // Set the BotTrans from Match BODY to LOCAL
            matchBodyToLocal.trans_vec[0] = value.x();
            matchBodyToLocal.trans_vec[1] = value.y();
            matchBodyToLocal.trans_vec[2] = 0.0;
            rpyMatchBody[2] = value.t();
            bot_roll_pitch_yaw_to_quat (rpyMatchBody, matchBodyToLocal.rot_quat);
            
            // ... and now Match Body ---> Query Body
            bot_trans_apply_trans_to (&localToQueryBody, &matchBodyToLocal, &matchBodyToQueryBody);
            nd->queryBodyToMatchBody = matchBodyToQueryBody;
            bot_trans_invert(&nd->queryBodyToMatchBody);
            valid_nodes.push_back(nd);
        }
    }    
    
    //vaid no of kd trees 
    valid_trees = valid_nodes.size();
    
    double cum_prob = 0;
    double avg_prob = 0;
    double max_prob = 0;  

    int no_of_matches = 0;

    // For each point in query scan, find the closest point among the scans of each nearby node (valid_nodes)
    for(unsigned i = 0; i < tocheck_scan->numPoints; i++){

        int my_no_matches = 0;
        int point_type = 0; // Either 1, 2, or 3
        
        pQuery[0] = tocheck_scan->points[i].x;
        pQuery[1] = tocheck_scan->points[i].y;
               
        double pQueryLocal[3];
        double pMatchedLocal[3];

        // Transform the query point into the local frame
        bot_trans_apply_vec(&queryBodyToLocal, pQuery, pQueryLocal);

        //fprintf(stderr, "Querry Point in Local frame : %f,%f\n", pQueryLocal[0], pQueryLocal[1]);

        double min_dist = 1000; 
        int close_ind  = -1;
        double pMatched[3] = {0.}; 

        int match_id = -1;
        int match_idx = -1;
        // loop through the neighbor nodes to find the nearest scan point
        for(int k = 0; k < valid_nodes.size(); k++){
            SlamNode *nd = valid_nodes.at(k); 
            
            kd_searches++;

            // Transform the point from the query pose frame to the match frame
            bot_trans_apply_vec ( &nd->queryBodyToMatchBody, pQuery, pQueryInMatched);
                
            qp[0] = pQueryInMatched[0];
            qp[1] = pQueryInMatched[1];
            
            int64_t search_utime = bot_timestamp_now();

            nd->slam_pose->kd_tree->annkFRSearch (qp,                    // query point
                                                  ANN_POW(radius_bound), // squared radius search bound
                                                  query_size,            // number of near neighbors
                                                  nnIdx,                 // nearest neighbors (returned)
                                                  dists,                 // distance (returned)
                                                  epsilon);
            
            int64_t search_end_utime = bot_timestamp_now();
            time_to_search += (search_end_utime - search_utime);
            int found = 0;

            
            // Point is type 2 if there are no NN
            if (nnIdx[0] < 0)
                point_type = 2;
            else
                point_type = 3; // Assume outlier for now
            
            // Loop through the set of nearest scan points
            for(int l=0;l< query_size ;l++){
                if(nnIdx[l] < 0){//because we have reached the end of the valid neighbours
                    break; 
                    //continue;
                }
                
                double dist_val = sqrt(dists[l]); 
                
                // Doensn't pass the test to be type 1
                if (dist_val > point_match_threshold)
                    continue;

                //count++;
                found = 1;
                point_type = 1;
                no_of_matches ++;
                my_no_matches ++;
                //dists[l] = sqrt(dists[l]); // MRW: Commented out
                //cout << i << " " << nnIdx[l] << " " << dists[l] << "\n";

                //nnIdx[l];
                
                vector< pair<int,int> >::iterator pit;
                pair <int, int> test_id_idx(nd->id, nnIdx[(l)]);
                pit = find ( matched_nodeid_pointidx.begin(),  matched_nodeid_pointidx.end(), test_id_idx);

                if(min_dist > dist_val && pit ==  matched_nodeid_pointidx.end()){
                    min_dist = dist_val;
                    ANNpointArray kdtree_points = nd->slam_pose->kd_tree->thePoints();
                    pMatched[0] = (kdtree_points[nnIdx[(l)]][(0)]);
                    pMatched[1] = (kdtree_points[nnIdx[(l)]][(1)]);
                    pMatched[2] = 0.0;
                    close_ind = k;
                    
                    match_id = nd->id;
                    match_idx = nnIdx[(l)];
                }               
            }
        }

        //fprintf(stderr, "Distance - recal : %f\n", hypot(pMatched[0] - pQueryInMatched[0], pMatched[1] - pQueryInMatched[1]));
        //fprintf(stderr, "Min Tree : %d\n", close_ind);
        
        double prob;

        // Did we find a match?
        // Point type 1
        if(close_ind >=0 && min_dist < radius_bound){
            pair <int, int> id_idx(match_id, match_idx);
            matched_nodeid_pointidx.push_back (id_idx);

            SlamNode *nd = valid_nodes.at(close_ind);

            Pose2d match = nd->getPose();

            // Determine the transform from Match Body ---> Query Body
            
            // Set the BotTrans from Match BODY to LOCAL
            matchBodyToLocal.trans_vec[0] = match.x();
            matchBodyToLocal.trans_vec[1] = match.y();
            matchBodyToLocal.trans_vec[2] = 0.0;
            rpyMatchBody[2] = match.t();
            bot_roll_pitch_yaw_to_quat (rpyMatchBody, matchBodyToLocal.rot_quat);
            bot_trans_apply_vec (&matchBodyToLocal, pMatched, pMatchedLocal);

            if(param.drawScanmatch){
                bot_lcmgl_point_size (lcmgl_sm_graph, 3);
                bot_lcmgl_begin (lcmgl_sm_graph, GL_POINTS);
                bot_lcmgl_color3f (lcmgl_sm_graph, 0.0, 1.0, 0.0);
                bot_lcmgl_vertex3f (lcmgl_sm_graph, pQueryLocal[0], pQueryLocal[1], 0);
                bot_lcmgl_color3f (lcmgl_sm_graph, 1.0, 0.7, 0.0);
                bot_lcmgl_vertex3f (lcmgl_sm_graph, pMatchedLocal[0], pMatchedLocal[1], 0);
                bot_lcmgl_end (lcmgl_sm_graph);
                
                bot_lcmgl_color3f (lcmgl_sm_graph, 1.0, 0.0, 0.0);
                bot_lcmgl_line_width (lcmgl_sm_graph, 3);
                bot_lcmgl_begin (lcmgl_sm_graph, GL_LINES);
                bot_lcmgl_vertex3f (lcmgl_sm_graph, pQueryLocal[0], pQueryLocal[1], 0);
                bot_lcmgl_vertex3f (lcmgl_sm_graph, pMatchedLocal[0], pMatchedLocal[1], 0);
                bot_lcmgl_end (lcmgl_sm_graph);
            }
            
            //fprintf(stderr, "Matched Point in Local frame : %f,%f\n", pMatchedLocal[0], pMatchedLocal[1]);
            int64_t s_t = bot_timestamp_now();    
            //get the covariance of the scan point based on the uncertainity of the 
            MatrixXd cov_querry_in_particle_frame = getObservationCovarianceInParticleFrame(pQuery, last_nd);
            int64_t e_t = bot_timestamp_now();
            //fprintf(stderr, "Dist : %f\n", hypot(pMatchedLocal[0] - pQueryLocal[0] , pMatchedLocal[1] - pQueryLocal[1]));
            
            MatrixXd cov_match_in_particle_frame = getObservationCovarianceInParticleFrame(pMatched, nd);
            //looks reasonable enough 
            //cout << endl << "Match Cov : " << endl << cov_match_in_particle_frame << endl; 
            prob = getObservationProbability(cov_match_in_particle_frame, cov_querry_in_particle_frame, 
                                             pMatchedLocal, pQueryLocal);

            // This probability represents the number of matches as a fraction of the maximum possible (one per matched node)
            // we add 1 to each to deal with the case of scans from new areas (see below)
            //double prob = (my_no_matches+1)/(valid_nodes.size() + 1);

            //this is the fixed model - wont take in to account the cov of the nodes 
            //double prob =  getObservationProbabilityFixed(pMatchedLocal, pQueryLocal);

            //fprintf(stderr, "Found => Min dist : %f Prob : %f\n", min_dist, prob);
            
            if(prob > 1.0)
                prob = 1.0;

            //if(prob < 0.5) 
            //    prob = 0.5; 

            //if (prob < 0.1)
            //    prob = 0.1;

            //double full_prob = prob; // 0.6 * prob + 0.4 * 0.2
            
            //fprintf(stderr, "Found => Min dist : %f Prob : %f\n", min_dist, prob);

            if(max_prob < prob)
                max_prob = prob;

            //saved in the slam node - to be used when updating 
            //last_nd->prob_of_scan[i] = full_prob;
            //fprintf (stdout,"\t\t my_no_matches = %d, valid_nodes.size() = %d\n", my_no_matches, valid_nodes.size());
            /*avg_prob += prob; //0.6 * prob + 0.4 * 0.2; 
              cum_prob += log(prob); //log(0.6 * prob + 0.4 * 0.2);// prob);*/
            //avg_prob += full_prob; //0.6 * prob + 0.4 * 0.2; 
            //cum_prob += log(full_prob);//0.6 * prob + 0.4 * 0.2);
            //fprintf(stderr, "\tProb : %f\n", prob);
            count++;

        }
        else{

            if (point_type == 3) 
                prob = prob_3;
            else
                prob = prob_2;

            //fprintf(stderr, "Not found => Min dist : %f Prob : %f\n", min_dist, 0.4 * 0.8);
            //avg_prob += 0.5;//0.4 * 0.8; 
            //cum_prob += log(0.5);//0.4 * 0.8);
            //avg_prob += 1/(valid_nodes.size()+1);
            //cum_prob += log(1/(valid_nodes.size()+1));
            //last_nd->prob_of_scan[i] =  0.5;//0.4 * 0.8;//0; 
            //last_nd->prob_of_scan[i] =  1/(valid_nodes.size()+1);//0.4 * 0.8;//0; 
        }

        avg_prob += prob;
        cum_prob += log (prob);
        last_nd->prob_of_scan[i] = prob;

    }

    //fprintf(stderr, "\t\t [%d] Slam Graph No of matches : %d\n", graph_id, no_of_matches);
    
    last_nd->max_scan_prob = max_prob; 
    *maximum_probability = max_prob;
    avg_prob /= tocheck_scan->numPoints;//count, 
        
    if(param.verbose)
        fprintf(stderr, "Count : %d Max Prob : %f Avg : %f Cumalative Prob : (log) %f => %f \n", 
                count, max_prob, avg_prob, 
                cum_prob, exp(cum_prob));
 
    annDeallocPt(qp);
    delete [] nnIdx;
    delete [] dists; 
    int64_t e_utime = bot_timestamp_now();
    if(param.verbose){
        fprintf(stderr, "\t Total : %d => Matched Count : %d\n", tocheck_scan->numPoints, count);
        fprintf(stderr, "\t Valid Trees : %d => Total Searches : %d Time per search : %f\n", valid_trees, kd_searches, (time_to_search) / 1.0e6 / (double) kd_searches);
        fprintf(stderr, "Time to serach kd tree : %f\n", (time_to_search) / 1.0e6);
    }
    
    bot_lcmgl_switch_buffer (lcmgl_sm_graph);
    cum_prob -= log(max_prob) * count; 
    //return exp(cum_prob);
    return avg_prob; //exp(cum_prob); //avg_prob / count; //exp(cum_prob); //count / (double) tocheck_scan->numPoints;
}


double SlamParticle::getProbOfMeasurementLikelihoodRegion(SlamNode *node, double *max_probability){

    // Settings that determine behavior
    // The distance threshold used for kNN
    double radius_bound = 0.25; // Distance threshold for NN (types (i) and (iii))
    double point_match_threshold = 0.1; // Distance threshold for valid matches (type (i))
    double prob_2 = 0.05; // Probability associated with type 2 
    double prob_3 = 0.05; // Probability associated with type 3

    //querry the kd tree - find the closest point 
    Pose2d tocheck_value = node->getPose();
    Scan *tocheck_scan = node->slam_pose->scan;
    double dist = 0;
    double pos[2], next_pos[2];
    double start[2];

    // Node->Slam_Pose->Scan laser scans are decimated and in the body frame
    // We will generate the local occupancy grid in the Query Body frame
    BotTrans localToQueryBody;
    BotTrans queryBodyToLocal = getBotTransFromPose(tocheck_value);
    localToQueryBody = queryBodyToLocal;
    bot_trans_invert (&localToQueryBody);

    node->resetProbability();

    BotTrans matchBodyToLocal;
    BotTrans matchBodyToQueryBody;
    BotTrans queryBodyToMatchBody;

    double rpyMatchBody[3] = {0.0, 0.0, 0.0};

    int query_size = 1;
    double epsilon =  0.01;//0.1;
   
    ANNidxArray  nnIdx = new ANNidx[query_size];
    ANNdistArray dists = new ANNdist[query_size];

    ANNpoint qp = annAllocPt(2);

    int64_t s_utime = bot_timestamp_now();

    int64_t time_to_search = 0;

    int count = 0;
    // Find the nodes (and scans) near the local search area
    double pQuery[3] = {.0};
    double pLaser[3];
    double pQueryInMatched[3];

    int valid_trees = 0;
    int kd_searches = 0;
    int first = 1;

    double dist_threshold = 10.0;
    int max_no_nodes = 5;
    vector< pair<int,int> > matched_nodeid_pointidx;

    //we can prob cache this search - when we do the correspondance - depending on how we choose to do it 

    vector<SlamNode *> valid_nodes;
    //find the valid nodes to search - and cache the transform 

    int use_correspondance = 1;

    if(use_correspondance){
        double skip_threshold = 0.95;
        vector<nodeDist> valid_node_dist;
        for(int i=0; i < graph->region_node_list.size(); i++){
            RegionNode *region = graph->region_node_list[i];
            if(region == node->region_node)
                continue;

            for(int j=0; j < region->nodes.size(); j++){           
                SlamNode *nd = region->nodes[j];
                //or should we just use the correspondance - since we have that already??
                double dist = getDistanceBetweenNodes(node, nd);
                if(dist < dist_threshold){
                    Pose2d value = nd->getPose();
                    nd->valid = 1;
            
                    ScanTransform T;
                    T.x = value.x();
                    T.y = value.y();
                    T.theta = value.t();

                    // Determine the transform from Match Body ---> Query Body
            
                    // Set the BotTrans from Match BODY to LOCAL
                    matchBodyToLocal.trans_vec[0] = value.x();
                    matchBodyToLocal.trans_vec[1] = value.y();
                    matchBodyToLocal.trans_vec[2] = 0.0;
                    rpyMatchBody[2] = value.t();
                    bot_roll_pitch_yaw_to_quat (rpyMatchBody, matchBodyToLocal.rot_quat);
            
                    // ... and now Match Body ---> Query Body
                    bot_trans_apply_trans_to (&localToQueryBody, &matchBodyToLocal, &matchBodyToQueryBody);
                    nd->queryBodyToMatchBody = matchBodyToQueryBody;
                    bot_trans_invert(&nd->queryBodyToMatchBody);

                    //inverted - because of the sorting function 
                    double score = 1 - getUpdatedCorrespondenceScore(node, nd);
                    if(score >= skip_threshold)
                        continue;
                    valid_node_dist.push_back(make_pair(nd, score));
                }
            }
        }
        std::sort(valid_node_dist.begin(), valid_node_dist.end(),compareDistance);
        for(int i=0; i < valid_node_dist.size(); i++){
            //fprintf(stderr, "\tNode : %d - %f\n", valid_node_dist[i].first->id, valid_node_dist[i].second);
            if(i >=max_no_nodes){
                break;
            }
            valid_nodes.push_back(valid_node_dist[i].first);
        }
    }
    else{
        vector<nodeDist> valid_node_dist;
        for(int i=0; i < graph->region_node_list.size(); i++){
            RegionNode *region = graph->region_node_list[i];
            if(region == node->region_node)
                continue;

            for(int j=0; j < region->nodes.size(); j++){           
                SlamNode *nd = region->nodes[j];
                //or should we just use the correspondance - since we have that already??
                double dist = getDistanceBetweenNodes(node, nd);
                if(dist < dist_threshold){
                    Pose2d value = nd->getPose();
                    nd->valid = 1;
            
                    ScanTransform T;
                    T.x = value.x();
                    T.y = value.y();
                    T.theta = value.t();

                    // Determine the transform from Match Body ---> Query Body
            
                    // Set the BotTrans from Match BODY to LOCAL
                    matchBodyToLocal.trans_vec[0] = value.x();
                    matchBodyToLocal.trans_vec[1] = value.y();
                    matchBodyToLocal.trans_vec[2] = 0.0;
                    rpyMatchBody[2] = value.t();
                    bot_roll_pitch_yaw_to_quat (rpyMatchBody, matchBodyToLocal.rot_quat);
            
                    // ... and now Match Body ---> Query Body
                    bot_trans_apply_trans_to (&localToQueryBody, &matchBodyToLocal, &matchBodyToQueryBody);
                    nd->queryBodyToMatchBody = matchBodyToQueryBody;
                    bot_trans_invert(&nd->queryBodyToMatchBody);
                    valid_node_dist.push_back(make_pair(nd, dist));
                }
            }
        }
        std::sort(valid_node_dist.begin(), valid_node_dist.end(),compareDistance);
        for(int i=0; i < valid_node_dist.size(); i++){
            //fprintf(stderr, "\tNode : %d - %f\n", valid_node_dist[i].first->id, valid_node_dist[i].second);
            if(i >=max_no_nodes){
                break;
            }
            valid_nodes.push_back(valid_node_dist[i].first);
        }
    }

    //vaid no of kd trees 

    //should only check with a maximum no of trees
    valid_trees = valid_nodes.size();
    
    double cum_prob = 0;
    double avg_prob = 0;
    double max_prob = 0;  

    int no_of_matches = 0;

    // For each point in query scan, find the closest point among the scans of each nearby node (valid_nodes)
    for(unsigned i = 0; i < tocheck_scan->numPoints; i++){
        int my_no_matches = 0;
        int point_type = 0; // Either 1, 2, or 3
        
        pQuery[0] = tocheck_scan->points[i].x;
        pQuery[1] = tocheck_scan->points[i].y;
               
        double pQueryLocal[3];
        double pMatchedLocal[3];

        // Transform the query point into the local frame
        bot_trans_apply_vec(&queryBodyToLocal, pQuery, pQueryLocal);

        //fprintf(stderr, "Querry Point in Local frame : %f,%f\n", pQueryLocal[0], pQueryLocal[1]);

        double min_dist = 1000; 
        int close_ind  = -1;
        double pMatched[3] = {0.}; 

        int match_id = -1;
        int match_idx = -1;
        // loop through the neighbor nodes to find the nearest scan point
        for(int k = 0; k < valid_nodes.size(); k++){
            SlamNode *nd = valid_nodes.at(k); 
            
            kd_searches++;

            // Transform the point from the query pose frame to the match frame
            bot_trans_apply_vec ( &nd->queryBodyToMatchBody, pQuery, pQueryInMatched);
                
            qp[0] = pQueryInMatched[0];
            qp[1] = pQueryInMatched[1];
            
            int64_t search_utime = bot_timestamp_now();

            nd->slam_pose->kd_tree->annkFRSearch (qp,                    // query point
                                                  ANN_POW(radius_bound), // squared radius search bound
                                                  query_size,            // number of near neighbors
                                                  nnIdx,                 // nearest neighbors (returned)
                                                  dists,                 // distance (returned)
                                                  epsilon);
            
            int64_t search_end_utime = bot_timestamp_now();
            time_to_search += (search_end_utime - search_utime);
            int found = 0;

            
            // Point is type 2 if there are no NN
            if (nnIdx[0] < 0)
                point_type = 2;
            else
                point_type = 3; // Assume outlier for now
            
            // Loop through the set of nearest scan points
            for(int l=0;l< query_size ;l++){
                if(nnIdx[l] < 0){//because we have reached the end of the valid neighbours
                    break; 
                    //continue;
                }
                
                double dist_val = sqrt(dists[l]); 
                
                // Doensn't pass the test to be type 1
                if (dist_val > point_match_threshold)
                    continue;

                //count++;
                found = 1;
                point_type = 1;
                no_of_matches ++;
                my_no_matches ++;
                //dists[l] = sqrt(dists[l]); // MRW: Commented out
                //cout << i << " " << nnIdx[l] << " " << dists[l] << "\n";

                //nnIdx[l];
                
                vector< pair<int,int> >::iterator pit;
                pair <int, int> test_id_idx(nd->id, nnIdx[(l)]);
                pit = find ( matched_nodeid_pointidx.begin(),  matched_nodeid_pointidx.end(), test_id_idx);

                if(min_dist > dist_val && pit ==  matched_nodeid_pointidx.end()){
                    min_dist = dist_val;
                    ANNpointArray kdtree_points = nd->slam_pose->kd_tree->thePoints();
                    pMatched[0] = (kdtree_points[nnIdx[(l)]][(0)]);
                    pMatched[1] = (kdtree_points[nnIdx[(l)]][(1)]);
                    pMatched[2] = 0.0;
                    close_ind = k;
                    
                    match_id = nd->id;
                    match_idx = nnIdx[(l)];
                }               
            }
        }

        //fprintf(stderr, "Distance - recal : %f\n", hypot(pMatched[0] - pQueryInMatched[0], pMatched[1] - pQueryInMatched[1]));
        //fprintf(stderr, "Min Tree : %d\n", close_ind);
        
        double prob;

        // Did we find a match?
        // Point type 1
        if(close_ind >=0 && min_dist < radius_bound){
            pair <int, int> id_idx(match_id, match_idx);
            matched_nodeid_pointidx.push_back (id_idx);

            SlamNode *nd = valid_nodes.at(close_ind);

            Pose2d match = nd->getPose();

            // Determine the transform from Match Body ---> Query Body
            
            // Set the BotTrans from Match BODY to LOCAL
            matchBodyToLocal.trans_vec[0] = match.x();
            matchBodyToLocal.trans_vec[1] = match.y();
            matchBodyToLocal.trans_vec[2] = 0.0;
            rpyMatchBody[2] = match.t();
            bot_roll_pitch_yaw_to_quat (rpyMatchBody, matchBodyToLocal.rot_quat);
            bot_trans_apply_vec (&matchBodyToLocal, pMatched, pMatchedLocal);

            if(0 && param.drawScanmatch){
                bot_lcmgl_point_size (lcmgl_sm_graph, 3);
                bot_lcmgl_begin (lcmgl_sm_graph, GL_POINTS);
                bot_lcmgl_color3f (lcmgl_sm_graph, 0.0, 1.0, 0.0);
                bot_lcmgl_vertex3f (lcmgl_sm_graph, pQueryLocal[0], pQueryLocal[1], 0);
                bot_lcmgl_color3f (lcmgl_sm_graph, 1.0, 0.7, 0.0);
                bot_lcmgl_vertex3f (lcmgl_sm_graph, pMatchedLocal[0], pMatchedLocal[1], 0);
                bot_lcmgl_end (lcmgl_sm_graph);
                
                bot_lcmgl_color3f (lcmgl_sm_graph, 1.0, 0.0, 0.0);
                bot_lcmgl_line_width (lcmgl_sm_graph, 3);
                bot_lcmgl_begin (lcmgl_sm_graph, GL_LINES);
                bot_lcmgl_vertex3f (lcmgl_sm_graph, pQueryLocal[0], pQueryLocal[1], 0);
                bot_lcmgl_vertex3f (lcmgl_sm_graph, pMatchedLocal[0], pMatchedLocal[1], 0);
                bot_lcmgl_end (lcmgl_sm_graph);
            }
            
            //fprintf(stderr, "Matched Point in Local frame : %f,%f\n", pMatchedLocal[0], pMatchedLocal[1]);
            int64_t s_t = bot_timestamp_now();    
            //get the covariance of the scan point based on the uncertainity of the 
            MatrixXd cov_querry_in_particle_frame = getObservationCovarianceInParticleFrame(pQuery, node);
            int64_t e_t = bot_timestamp_now();
            //fprintf(stderr, "Dist : %f\n", hypot(pMatchedLocal[0] - pQueryLocal[0] , pMatchedLocal[1] - pQueryLocal[1]));
            
            MatrixXd cov_match_in_particle_frame = getObservationCovarianceInParticleFrame(pMatched, nd);
            //looks reasonable enough 
            //cout << endl << "Match Cov : " << endl << cov_match_in_particle_frame << endl; 
            prob = getObservationProbability(cov_match_in_particle_frame, cov_querry_in_particle_frame, 
                                             pMatchedLocal, pQueryLocal);

            // This probability represents the number of matches as a fraction of the maximum possible (one per matched node)
            // we add 1 to each to deal with the case of scans from new areas (see below)
            //double prob = (my_no_matches+1)/(valid_nodes.size() + 1);

            //this is the fixed model - wont take in to account the cov of the nodes 
            //double prob =  getObservationProbabilityFixed(pMatchedLocal, pQueryLocal);

            //fprintf(stderr, "Found => Min dist : %f Prob : %f\n", min_dist, prob);
            
            if(prob > 1.0)
                prob = 1.0;

            //if(prob < 0.5) 
            //    prob = 0.5; 

            //if (prob < 0.1)
            //    prob = 0.1;

            //double full_prob = prob; // 0.6 * prob + 0.4 * 0.2
            
            //fprintf(stderr, "Found => Min dist : %f Prob : %f\n", min_dist, prob);

            if(max_prob < prob)
                max_prob = prob;

            //saved in the slam node - to be used when updating 
            //last_nd->prob_of_scan[i] = full_prob;
            //fprintf (stdout,"\t\t my_no_matches = %d, valid_nodes.size() = %d\n", my_no_matches, valid_nodes.size());
            /*avg_prob += prob; //0.6 * prob + 0.4 * 0.2; 
              cum_prob += log(prob); //log(0.6 * prob + 0.4 * 0.2);// prob);*/
            //avg_prob += full_prob; //0.6 * prob + 0.4 * 0.2; 
            //cum_prob += log(full_prob);//0.6 * prob + 0.4 * 0.2);
            //fprintf(stderr, "\tProb : %f\n", prob);
            count++;

        }
        else{

            if (point_type == 3) 
                prob = prob_3;
            else
                prob = prob_2;

            //fprintf(stderr, "Not found => Min dist : %f Prob : %f\n", min_dist, 0.4 * 0.8);
            //avg_prob += 0.5;//0.4 * 0.8; 
            //cum_prob += log(0.5);//0.4 * 0.8);
            //avg_prob += 1/(valid_nodes.size()+1);
            //cum_prob += log(1/(valid_nodes.size()+1));
            //last_nd->prob_of_scan[i] =  0.5;//0.4 * 0.8;//0; 
            //last_nd->prob_of_scan[i] =  1/(valid_nodes.size()+1);//0.4 * 0.8;//0; 
        }

        avg_prob += prob;
        cum_prob += log (prob);
        node->prob_of_scan[i] = prob;

    }

    //fprintf(stderr, "\t\t [%d] Slam Graph No of matches : %d\n", graph_id, no_of_matches);
    
    node->max_scan_prob = max_prob; 
    *max_probability = max_prob;
    avg_prob /= tocheck_scan->numPoints;//count, 
        
    if(param.verbose)
        fprintf(stderr, "Count : %d Max Prob : %f Avg : %f Cumalative Prob : (log) %f => %f \n", 
                count, max_prob, avg_prob, 
                cum_prob, exp(cum_prob));
 
    annDeallocPt(qp);
    delete [] nnIdx;
    delete [] dists; 
    int64_t e_utime = bot_timestamp_now();
    if(param.verbose){
        fprintf(stderr, "\t Total : %d => Matched Count : %d\n", tocheck_scan->numPoints, count);
        fprintf(stderr, "\t Valid Trees : %d => Total Searches : %d Time per search : %f\n", valid_trees, kd_searches, (time_to_search) / 1.0e6 / (double) kd_searches);
        fprintf(stderr, "Time to serach kd tree : %f\n", (time_to_search) / 1.0e6);
    }
    
    bot_lcmgl_switch_buffer (lcmgl_sm_graph);
    cum_prob -= log(max_prob) * count; 
    //return exp(cum_prob);
    return avg_prob; //exp(cum_prob); //avg_prob / count; //exp(cum_prob); //count / (double) tocheck_scan->numPoints;
}


    

double SlamParticle::getProbOfMeasurementLikelihoodScanMatchNode(SlamNode *node, double *max_probability){

    // Settings that determine behavior
    // The distance threshold used for kNN
    double radius_bound = 0.1;//0.25; // Distance threshold for NN (types (i) and (iii))
    double point_match_threshold = 0.1; // Distance threshold for valid matches (type (i))
    double prob_2 = 0.05; // Probability associated with type 2 
    double prob_3 = 0.05; // Probability associated with type 3

    //querry the kd tree - find the closest point 
    Pose2d tocheck_value = node->getPose();
    Scan *tocheck_scan = node->slam_pose->scan;
    double dist = 0;
    double pos[2], next_pos[2];
    double start[2];

    // Node->Slam_Pose->Scan laser scans are decimated and in the body frame
    // We will generate the local occupancy grid in the Query Body frame
    BotTrans localToQueryBody;
    BotTrans queryBodyToLocal = getBotTransFromPose(tocheck_value);
    localToQueryBody = queryBodyToLocal;
    bot_trans_invert (&localToQueryBody);

    node->resetProbability();

    BotTrans matchBodyToLocal;
    BotTrans matchBodyToQueryBody;
    BotTrans queryBodyToMatchBody;

    double rpyMatchBody[3] = {0.0, 0.0, 0.0};

    int query_size = 1;
    double epsilon =  0.01;//0.1;
   
    ANNidxArray  nnIdx = new ANNidx[query_size];
    ANNdistArray dists = new ANNdist[query_size];

    ANNpoint qp = annAllocPt(2);

    int64_t s_utime = bot_timestamp_now();

    int64_t time_to_search = 0;

    int count = 0;
    // Find the nodes (and scans) near the local search area
    double pQuery[3] = {.0};
    double pLaser[3];
    double pQueryInMatched[3];

    int valid_trees = 0;
    int kd_searches = 0;
    int first = 1;

    double dist_threshold = 10.0;
    int max_no_nodes = 5;
    vector< pair<int,int> > matched_nodeid_pointidx;

    //we can prob cache this search - when we do the correspondance - depending on how we choose to do it 

    vector<SlamNode *> valid_nodes;
    //find the valid nodes to search - and cache the transform 

    int use_correspondance = 1;

    if(use_correspondance){
        double skip_threshold = 0.95;
        vector<nodeDist> valid_node_dist;
        for(int i=0; i < graph->region_node_list.size(); i++){
            RegionNode *region = graph->region_node_list[i];
            //if(region == node->region_node)
            //  continue;

            for(int j=0; j < region->nodes.size(); j++){           
                SlamNode *nd = region->nodes[j];
                if(nd == node)
                    continue;
                //or should we just use the correspondance - since we have that already??
                double dist = getDistanceBetweenNodes(node, nd);
                if(dist < dist_threshold){
                    Pose2d value = nd->getPose();
                    nd->valid = 1;
            
                    ScanTransform T;
                    T.x = value.x();
                    T.y = value.y();
                    T.theta = value.t();

                    // Determine the transform from Match Body ---> Query Body
            
                    // Set the BotTrans from Match BODY to LOCAL
                    matchBodyToLocal.trans_vec[0] = value.x();
                    matchBodyToLocal.trans_vec[1] = value.y();
                    matchBodyToLocal.trans_vec[2] = 0.0;
                    rpyMatchBody[2] = value.t();
                    bot_roll_pitch_yaw_to_quat (rpyMatchBody, matchBodyToLocal.rot_quat);
            
                    // ... and now Match Body ---> Query Body
                    bot_trans_apply_trans_to (&localToQueryBody, &matchBodyToLocal, &matchBodyToQueryBody);
                    nd->queryBodyToMatchBody = matchBodyToQueryBody;
                    bot_trans_invert(&nd->queryBodyToMatchBody);

                    //inverted - because of the sorting function 
                    double score = 1 - getUpdatedCorrespondenceScore(node, nd);
                    if(score >= skip_threshold)
                        continue;
                    valid_node_dist.push_back(make_pair(nd, score));
                }
            }
        }
        std::sort(valid_node_dist.begin(), valid_node_dist.end(),compareDistance);
        for(int i=0; i < valid_node_dist.size(); i++){
            //fprintf(stderr, "\tNode : %d - %f\n", valid_node_dist[i].first->id, valid_node_dist[i].second);
            if(i >=max_no_nodes){
                break;
            }
            valid_nodes.push_back(valid_node_dist[i].first);
        }
    }
    else{
        vector<nodeDist> valid_node_dist;
        for(int i=0; i < graph->region_node_list.size(); i++){
            RegionNode *region = graph->region_node_list[i];
            //if(region == node->region_node)
            //  continue;

            for(int j=0; j < region->nodes.size(); j++){           
                SlamNode *nd = region->nodes[j];
                if(nd == node)
                    continue;
                //or should we just use the correspondance - since we have that already??
                double dist = getDistanceBetweenNodes(node, nd);
                if(dist < dist_threshold){
                    Pose2d value = nd->getPose();
                    nd->valid = 1;
            
                    ScanTransform T;
                    T.x = value.x();
                    T.y = value.y();
                    T.theta = value.t();

                    // Determine the transform from Match Body ---> Query Body
            
                    // Set the BotTrans from Match BODY to LOCAL
                    matchBodyToLocal.trans_vec[0] = value.x();
                    matchBodyToLocal.trans_vec[1] = value.y();
                    matchBodyToLocal.trans_vec[2] = 0.0;
                    rpyMatchBody[2] = value.t();
                    bot_roll_pitch_yaw_to_quat (rpyMatchBody, matchBodyToLocal.rot_quat);
            
                    // ... and now Match Body ---> Query Body
                    bot_trans_apply_trans_to (&localToQueryBody, &matchBodyToLocal, &matchBodyToQueryBody);
                    nd->queryBodyToMatchBody = matchBodyToQueryBody;
                    bot_trans_invert(&nd->queryBodyToMatchBody);
                    valid_node_dist.push_back(make_pair(nd, dist));
                }
            }
        }
        std::sort(valid_node_dist.begin(), valid_node_dist.end(),compareDistance);
        for(int i=0; i < valid_node_dist.size(); i++){
            //fprintf(stderr, "\tNode : %d - %f\n", valid_node_dist[i].first->id, valid_node_dist[i].second);
            if(i >=max_no_nodes){
                break;
            }
            valid_nodes.push_back(valid_node_dist[i].first);
        }
    }

    

    //vaid no of kd trees 

    //should only check with a maximum no of trees
    valid_trees = valid_nodes.size();
    
    if(valid_nodes.size() == 0){
        fprintf(stderr, "No observations to match against : Giving prob of 1\n");
        return 1;
    }

    double cum_prob = 0;
    double avg_prob = 0;
    double max_prob = 0;  

    int no_of_matches = 0;
    int no_points_matched = 0;
    // For each point in query scan, find the closest point among the scans of each nearby node (valid_nodes)
    for(unsigned i = 0; i < tocheck_scan->numPoints; i++){
        int my_no_matches = 0;
        int point_type = 0; // Either 1, 2, or 3
        
        pQuery[0] = tocheck_scan->points[i].x;
        pQuery[1] = tocheck_scan->points[i].y;
               
        double pQueryLocal[3];
        double pMatchedLocal[3];

        // Transform the query point into the local frame
        bot_trans_apply_vec(&queryBodyToLocal, pQuery, pQueryLocal);

        //fprintf(stderr, "Querry Point in Local frame : %f,%f\n", pQueryLocal[0], pQueryLocal[1]);

        double min_dist = 1000; 
        int close_ind  = -1;
        double pMatched[3] = {0.}; 

        int match_id = -1;
        int match_idx = -1;
        // loop through the neighbor nodes to find the nearest scan point
        int found_match = 0;
        
        for(int k = 0; k < valid_nodes.size(); k++){
            SlamNode *nd = valid_nodes.at(k); 
            
            kd_searches++;

            // Transform the point from the query pose frame to the match frame
            bot_trans_apply_vec ( &nd->queryBodyToMatchBody, pQuery, pQueryInMatched);
                
            qp[0] = pQueryInMatched[0];
            qp[1] = pQueryInMatched[1];
            
            int64_t search_utime = bot_timestamp_now();

            nd->slam_pose->kd_tree->annkFRSearch (qp,                    // query point
                                                  ANN_POW(radius_bound), // squared radius search bound
                                                  query_size,            // number of near neighbors
                                                  nnIdx,                 // nearest neighbors (returned)
                                                  dists,                 // distance (returned)
                                                  epsilon);
            
            int64_t search_end_utime = bot_timestamp_now();
            time_to_search += (search_end_utime - search_utime);
            int found = 0;

            
            // Point is type 2 if there are no NN
            if (nnIdx[0] < 0)
                point_type = 2;
            else
                point_type = 3; // Assume outlier for now
            
            // Loop through the set of nearest scan points
            for(int l=0;l< query_size ;l++){
                if(nnIdx[l] < 0){//because we have reached the end of the valid neighbours
                    break; 
                    //continue;
                }
                
                double dist_val = sqrt(dists[l]); 
                
                // Doensn't pass the test to be type 1
                if (dist_val > point_match_threshold)
                    continue;

                found_match = 1;
                //count++;
                found = 1;
                point_type = 1;
                no_of_matches ++;
                my_no_matches ++;
                //dists[l] = sqrt(dists[l]); // MRW: Commented out
                //cout << i << " " << nnIdx[l] << " " << dists[l] << "\n";

                //nnIdx[l];
                
                vector< pair<int,int> >::iterator pit;
                pair <int, int> test_id_idx(nd->id, nnIdx[(l)]);
                pit = find ( matched_nodeid_pointidx.begin(),  matched_nodeid_pointidx.end(), test_id_idx);

                if(min_dist > dist_val && pit ==  matched_nodeid_pointidx.end()){
                    min_dist = dist_val;
                    ANNpointArray kdtree_points = nd->slam_pose->kd_tree->thePoints();
                    pMatched[0] = (kdtree_points[nnIdx[(l)]][(0)]);
                    pMatched[1] = (kdtree_points[nnIdx[(l)]][(1)]);
                    pMatched[2] = 0.0;
                    close_ind = k;
                    
                    match_id = nd->id;
                    match_idx = nnIdx[(l)];
                }               
            }
        }
        if(found_match){
            no_points_matched++;
        }

        //fprintf(stderr, "Distance - recal : %f\n", hypot(pMatched[0] - pQueryInMatched[0], pMatched[1] - pQueryInMatched[1]));
        //fprintf(stderr, "Min Tree : %d\n", close_ind);
        
        double prob;

        // Did we find a match?
        // Point type 1
        if(close_ind >=0 && min_dist < radius_bound){
            pair <int, int> id_idx(match_id, match_idx);
            matched_nodeid_pointidx.push_back (id_idx);

            SlamNode *nd = valid_nodes.at(close_ind);

            Pose2d match = nd->getPose();

            // Determine the transform from Match Body ---> Query Body
            
            // Set the BotTrans from Match BODY to LOCAL
            matchBodyToLocal.trans_vec[0] = match.x();
            matchBodyToLocal.trans_vec[1] = match.y();
            matchBodyToLocal.trans_vec[2] = 0.0;
            rpyMatchBody[2] = match.t();
            bot_roll_pitch_yaw_to_quat (rpyMatchBody, matchBodyToLocal.rot_quat);
            bot_trans_apply_vec (&matchBodyToLocal, pMatched, pMatchedLocal);

            if(0 && param.drawScanmatch){
                bot_lcmgl_point_size (lcmgl_sm_graph, 3);
                bot_lcmgl_begin (lcmgl_sm_graph, GL_POINTS);
                bot_lcmgl_color3f (lcmgl_sm_graph, 0.0, 1.0, 0.0);
                bot_lcmgl_vertex3f (lcmgl_sm_graph, pQueryLocal[0], pQueryLocal[1], 0);
                bot_lcmgl_color3f (lcmgl_sm_graph, 1.0, 0.7, 0.0);
                bot_lcmgl_vertex3f (lcmgl_sm_graph, pMatchedLocal[0], pMatchedLocal[1], 0);
                bot_lcmgl_end (lcmgl_sm_graph);
                
                bot_lcmgl_color3f (lcmgl_sm_graph, 1.0, 0.0, 0.0);
                bot_lcmgl_line_width (lcmgl_sm_graph, 3);
                bot_lcmgl_begin (lcmgl_sm_graph, GL_LINES);
                bot_lcmgl_vertex3f (lcmgl_sm_graph, pQueryLocal[0], pQueryLocal[1], 0);
                bot_lcmgl_vertex3f (lcmgl_sm_graph, pMatchedLocal[0], pMatchedLocal[1], 0);
                bot_lcmgl_end (lcmgl_sm_graph);
            }
            
            //fprintf(stderr, "Matched Point in Local frame : %f,%f\n", pMatchedLocal[0], pMatchedLocal[1]);
            int64_t s_t = bot_timestamp_now();    
            //get the covariance of the scan point based on the uncertainity of the 
            MatrixXd cov_querry_in_particle_frame = getObservationCovarianceInParticleFrame(pQuery, node);
            int64_t e_t = bot_timestamp_now();
            //fprintf(stderr, "Dist : %f\n", hypot(pMatchedLocal[0] - pQueryLocal[0] , pMatchedLocal[1] - pQueryLocal[1]));
            
            MatrixXd cov_match_in_particle_frame = getObservationCovarianceInParticleFrame(pMatched, nd);
            //looks reasonable enough 
            //cout << endl << "Match Cov : " << endl << cov_match_in_particle_frame << endl; 
            prob = getObservationProbability(cov_match_in_particle_frame, cov_querry_in_particle_frame, 
                                             pMatchedLocal, pQueryLocal);

            // This probability represents the number of matches as a fraction of the maximum possible (one per matched node)
            // we add 1 to each to deal with the case of scans from new areas (see below)
            //double prob = (my_no_matches+1)/(valid_nodes.size() + 1);

            //this is the fixed model - wont take in to account the cov of the nodes 
            //double prob =  getObservationProbabilityFixed(pMatchedLocal, pQueryLocal);

            //fprintf(stderr, "Found => Min dist : %f Prob : %f\n", min_dist, prob);
            
            if(prob > 1.0)
                prob = 1.0;
            
            if(prob < 0.05){
                prob = 0.05;//prob_2 + 0.05;
            }
            
            /*if(prob < 0.1){
                //fprintf(stderr, "Prob too low\n");
                prob = 0.1;//prob_2 + 0.05;
                }*/

            //if(prob < 0.5) 
            //    prob = 0.5; 

            //if (prob < 0.1)
            //    prob = 0.1;

            //double full_prob = prob; // 0.6 * prob + 0.4 * 0.2
            
            //fprintf(stderr, "Found => Min dist : %f Prob : %f\n", min_dist, prob);

            if(max_prob < prob)
                max_prob = prob;

            //saved in the slam node - to be used when updating 
            //last_nd->prob_of_scan[i] = full_prob;
            //fprintf (stdout,"\t\t my_no_matches = %d, valid_nodes.size() = %d\n", my_no_matches, valid_nodes.size());
            /*avg_prob += prob; //0.6 * prob + 0.4 * 0.2; 
              cum_prob += log(prob); //log(0.6 * prob + 0.4 * 0.2);// prob);*/
            //avg_prob += full_prob; //0.6 * prob + 0.4 * 0.2; 
            //cum_prob += log(full_prob);//0.6 * prob + 0.4 * 0.2);
            //fprintf(stderr, "\tProb : %f\n", prob);
            count++;

        }
        else{

            if (point_type == 3) 
                prob = prob_3;
            else
                prob = prob_2;

            //fprintf(stderr, "Not found => Min dist : %f Prob : %f\n", min_dist, 0.4 * 0.8);
            //avg_prob += 0.5;//0.4 * 0.8; 
            //cum_prob += log(0.5);//0.4 * 0.8);
            //avg_prob += 1/(valid_nodes.size()+1);
            //cum_prob += log(1/(valid_nodes.size()+1));
            //last_nd->prob_of_scan[i] =  0.5;//0.4 * 0.8;//0; 
            //last_nd->prob_of_scan[i] =  1/(valid_nodes.size()+1);//0.4 * 0.8;//0; 
        }

        avg_prob += prob;
        cum_prob += log (prob);
        node->prob_of_scan[i] = prob;

    }

    //fprintf(stderr, YELLOW "\t\t [%d] Slam Graph No of Unique : %d No of matches (multiple) : %d\n" RESET_COLOR, graph_id, no_points_matched, no_of_matches);
    
    node->max_scan_prob = max_prob; 
    *max_probability = max_prob;
    avg_prob /= tocheck_scan->numPoints;//count, 
        
    if(1)//param.verbose)
        fprintf(stderr, YELLOW "Count : %d Max Prob : %f Avg : %f Cumalative Prob : (log) %f => %f \n" RESET_COLOR, 
                count, max_prob, avg_prob, 
                cum_prob, exp(cum_prob));
 
    annDeallocPt(qp);
    delete [] nnIdx;
    delete [] dists; 
    int64_t e_utime = bot_timestamp_now();
    if(param.verbose){
        fprintf(stderr, "\t Total : %d => Matched Count : %d\n", tocheck_scan->numPoints, count);
        fprintf(stderr, "\t Valid Trees : %d => Total Searches : %d Time per search : %f\n", valid_trees, kd_searches, (time_to_search) / 1.0e6 / (double) kd_searches);
        fprintf(stderr, "Time to serach kd tree : %f\n", (time_to_search) / 1.0e6);
    }
    
    bot_lcmgl_switch_buffer (lcmgl_sm_graph);
    cum_prob -= log(max_prob) * count; 
    //return exp(cum_prob);
    return avg_prob; //exp(cum_prob); //avg_prob / count; //exp(cum_prob); //count / (double) tocheck_scan->numPoints;
}

// Determine the likelihood of a the actual_laser scan based upon the ray-traced version from an occupancy grid
// Algorithm implements the beam_range_finder_model() from Probabilistic Robotics
double 
SlamParticle::scanLikelihood(const bot_core_planar_lidar_t *sim_laser, bot_core_planar_lidar_t *actual_laser)
{
    // Mixing likelihoods
    double a_hit = 0.5;
    double a_short = 0.3;
    double a_max = 0.1;
    double a_rand = 0.1;

    double q = 0.0;
    double p;

    for (int i=0; i<actual_laser->nranges; i++) {
        // Ignore this scan if (i) actual range is less than min (ii) simulated range is less than min
        // or (iii) bot actual range and simulated range are greater than max
        if ( (actual_laser->ranges[i] < SCAN_Z_MIN) || (sim_laser->ranges[i] < SCAN_Z_MIN) ||
             ((actual_laser->ranges[i] > SCAN_Z_MAX) && (sim_laser->ranges[i] > SCAN_Z_MAX)))
            continue;

        double p_hit = pHit (sim_laser->ranges[i], actual_laser->ranges[i]);
        double p_short = pShort (sim_laser->ranges[i], actual_laser->ranges[i]);
        double p_rand = 1/SCAN_Z_MAX;
        double p_max = 0;

        if (actual_laser->ranges[i] == SCAN_Z_MAX)
            p_max = 1;

        
        p = a_hit * p_hit + a_short * p_short + a_max * p_max + a_rand * p_rand;
        if (param.verbose)
            fprintf (stdout, "----- p = %.4f, log(p) = %.4f, p_hit = %.4f, p_short = %.4f, p_max = %.4f, p_rand = %.4f\n",
                     p, log(p), p_hit, p_short, p_max, p_rand);


        q = q + log(p);
    }


    // Convert back to probabilities
    return exp(q);

}


double SlamParticle::compareScans(const bot_core_planar_lidar_t *sim_laser, bot_core_planar_lidar_t *actual_laser){
    int matched_count = 0;
    int scan_dist = 4;
    double acceptance_threshold = 0.1;
    for(int i=0; i < actual_laser->nranges; i++){
        int min_ind = fmax(0, i-scan_dist);
        int max_ind = fmin(actual_laser->nranges, i+scan_dist);
        if(actual_laser->ranges[i] < 0.01 || actual_laser->ranges[i] > 20.0)
            continue;
        for(int j=min_ind; j < max_ind; j++){
            if(sim_laser->ranges[i] < 0.01 || sim_laser->ranges[i] > 20.0)
                continue;
            if(fabs(actual_laser->ranges[i] - sim_laser->ranges[j]) < acceptance_threshold){
                matched_count++;
                break;
            }
        }
    }
    return matched_count / (double) actual_laser->nranges;
}

void SlamParticle::getLCMMessageFromGraph(slam_graph_particle_t *particle){
    particle->id = graph_id;
    particle->no_nodes = graph->slam_node_list.size();
    particle->node_list = (slam_graph_node_t *) calloc(particle->no_nodes, sizeof(slam_graph_node_t));
    graph->updateBoundingBoxes();
    particle->no_regions = graph->last_region_id;//region_node_list.size();
    for(int i=0; i < graph->slam_node_list.size(); i++){
        SlamNode *node = graph->slam_node_list.at(i);
        
        particle->node_list[i].id = node->position;
        particle->node_list[i].node_id = node->id;
        particle->node_list[i].utime = node->slam_pose->utime;
        particle->node_list[i].xy[0] = node->getPose().x();
        particle->node_list[i].xy[1] = node->getPose().y();
        particle->node_list[i].heading = node->getPose().t();
        particle->node_list[i].is_supernode = node->slam_pose->is_supernode;
        particle->node_list[i].parent_supernode = node->region_node->region_id;//node->parent_supernode;
        if(node->segment != NULL){
            particle->node_list[i].segment_id = node->segment->id;
            //fprintf(stderr, YELLOW "Node : %d => %d\n" RESET_COLOR, node->id, node->segment->id);
        }
        else{
            //fprintf(stderr, YELLOW "Node : %d => %d\n" RESET_COLOR, node->id, -1);
            particle->node_list[i].segment_id = -1;
        }
        //&(particle->node_list[i].labeldist = (slam_label_distribution_t *) calloc(1, sizeof(slam_label_distribution_t));
        particle->node_list[i].labeldist.num_labels = node->labeldist->num_labels;
        particle->node_list[i].labeldist.total_obs  = node->labeldist->total_obs ;
        //nothing to send over the labels atm
        //particle->node_list[i].labeldist.observation_frequency = node->labeldist->observation_frequency;
        particle->node_list[i].labeldist.observation_frequency = (double *) calloc(node->labeldist->num_labels, sizeof(double));
        for(int j=0; j<node->labeldist->num_labels; j++)
            particle->node_list[i].labeldist.observation_frequency[j] = node->labeldist->observation_frequency.at(j);
        
        particle->node_list[i].no_points = node->bounding_box_global->npoints;
        particle->node_list[i].x_coords = (double *) calloc(node->bounding_box_global->npoints, sizeof(double));
        particle->node_list[i].y_coords = (double *) calloc(node->bounding_box_global->npoints, sizeof(double));
        for(int j=0; j<node->bounding_box_global->npoints; j++){
            particle->node_list[i].x_coords[j] = node->bounding_box_global->points[j].x;
            particle->node_list[i].y_coords[j] = node->bounding_box_global->points[j].y;
        }
        particle->node_list[i].pofz = node->pofz;
        memcpy(particle->node_list[i].cov,node->cov, 9 * sizeof(double));
    }
    particle->no_edges = graph->slam_constraints.size();
    particle->edge_list = (slam_graph_edge_t *) calloc(particle->no_edges, sizeof(slam_graph_edge_t));

    map<int, SlamConstraint *>::iterator c_it;
    int j= 0;
    for ( c_it= graph->slam_constraints.begin() ; c_it != graph->slam_constraints.end(); c_it++, j++ ){        
        SlamConstraint *ct = c_it->second;
        
        particle->edge_list[j].id = ct->id;
        particle->edge_list[j].node_id_1 = ct->node1->position; 
        particle->edge_list[j].node_id_2 = ct->node2->position; 
        particle->edge_list[j].actual_scanned_node_id_1 = ct->actualnode1->position;
        particle->edge_list[j].actual_scanned_node_id_2 = ct->actualnode2->position;
        particle->edge_list[j].scanmatch_hit = ct->hitpct;
        particle->edge_list[j].type = ct->type;
        particle->edge_list[j].status = ct->status;
    }

    particle->no_rejected_edges = graph->failed_slam_constraints.size();
    particle->rejected_edge_list = (slam_graph_edge_t *) calloc(particle->no_rejected_edges, sizeof(slam_graph_edge_t));

    j= 0;
    for ( c_it= graph->failed_slam_constraints.begin() ; c_it != graph->failed_slam_constraints.end(); c_it++, j++ ){        
        SlamConstraint *ct = c_it->second;
        
        particle->rejected_edge_list[j].id = ct->id;
        particle->rejected_edge_list[j].node_id_1 = ct->node1->position; 
        particle->rejected_edge_list[j].node_id_2 = ct->node2->position; 
        particle->rejected_edge_list[j].actual_scanned_node_id_1 = ct->actualnode1->position;
        particle->rejected_edge_list[j].actual_scanned_node_id_2 = ct->actualnode2->position;
        particle->rejected_edge_list[j].scanmatch_hit = ct->hitpct;
        particle->rejected_edge_list[j].type = ct->type;
        particle->rejected_edge_list[j].status = ct->status;
    }
    
    particle->segments.no_segments = graph->region_segment_list.size();
        
    particle->segments.list = (slam_graph_segment_t *) calloc(particle->segments.no_segments, sizeof(slam_graph_segment_t));

    for(int i= 0; i < graph->region_segment_list.size(); i++){
        particle->segments.list[i].id = graph->region_segment_list[i]->id;
        fprintf(stderr, YELLOW "Segment : %d\n" RESET_COLOR, graph->region_segment_list[i]->id);
        particle->segments.list[i].no_nodes = graph->region_segment_list[i]->nodes.size();
        particle->segments.list[i].node_ids = (int64_t *) calloc(graph->region_segment_list[i]->nodes.size(), sizeof(int64_t));
        for(int j=0; j < graph->region_segment_list[i]->nodes.size(); j++){
            //fprintf(stderr, YELLOW "\tNode : %d\n" RESET_COLOR, graph->region_segment_list[i]->nodes[j]->id);
            particle->segments.list[i].node_ids[j] = graph->region_segment_list[i]->nodes[j]->id;
        }
    }
    
    particle->weight = normalized_weight;
    if(param.verbose)
        fprintf(stderr, "Weight : %f\n", normalized_weight);
}

void SlamParticle::getLCMMessageFromBasicGraph(slam_graph_particle_t *particle){
    SlamGraph *basic_graph = graph;
    if(param.useBasicGraph){
        basic_graph = sparse_graph;
    }

    particle->id = graph_id;
    particle->no_nodes = basic_graph->slam_node_list.size();
    particle->node_list = (slam_graph_node_t *) calloc(particle->no_nodes, sizeof(slam_graph_node_t));
    basic_graph->updateBoundingBoxes();
    particle->no_regions = graph->last_region_id;//region_node_list.size();
    for(int i=0; i < basic_graph->slam_node_list.size(); i++){
        SlamNode *node = basic_graph->slam_node_list.at(i);
        
        particle->node_list[i].id = node->position;
        particle->node_list[i].node_id = node->id;        
        particle->node_list[i].utime = node->slam_pose->utime;
        particle->node_list[i].xy[0] = node->getPose().x();
        particle->node_list[i].xy[1] = node->getPose().y();
        particle->node_list[i].heading = node->getPose().t();
        particle->node_list[i].is_supernode = node->slam_pose->is_supernode;
        particle->node_list[i].parent_supernode = node->region_node->region_id;//parent_supernode;
        
        //&(particle->node_list[i].labeldist = (slam_label_distribution_t *) calloc(1, sizeof(slam_label_distribution_t));
        particle->node_list[i].labeldist.num_labels = node->labeldist->num_labels;
        particle->node_list[i].labeldist.total_obs  = node->labeldist->total_obs ;
        //nothing to send over the labels atm
        //particle->node_list[i].labeldist.observation_frequency = node->labeldist->observation_frequency;
        particle->node_list[i].labeldist.observation_frequency = (double *) calloc(node->labeldist->num_labels, sizeof(double));
        for(int j=0; j<node->labeldist->num_labels; j++)
            particle->node_list[i].labeldist.observation_frequency[j] = node->labeldist->observation_frequency.at(j);
        
        particle->node_list[i].no_points = node->bounding_box_global->npoints;
        particle->node_list[i].x_coords = (double *) calloc(node->bounding_box_global->npoints, sizeof(double));
        particle->node_list[i].y_coords = (double *) calloc(node->bounding_box_global->npoints, sizeof(double));
        for(int j=0; j<node->bounding_box_global->npoints; j++){
            particle->node_list[i].x_coords[j] = node->bounding_box_global->points[j].x;
            particle->node_list[i].y_coords[j] = node->bounding_box_global->points[j].y;
        }
        particle->node_list[i].pofz = node->pofz;
        memcpy(particle->node_list[i].cov,node->cov, 9 * sizeof(double));
    }
    particle->no_edges = basic_graph->slam_constraints.size();
    particle->edge_list = (slam_graph_edge_t *) calloc(particle->no_edges, sizeof(slam_graph_edge_t));

    map<int, SlamConstraint *>::iterator c_it;
    int j= 0;
    for ( c_it= basic_graph->slam_constraints.begin() ; c_it != basic_graph->slam_constraints.end(); c_it++, j++ ){        
        SlamConstraint *ct = c_it->second;
        
        particle->edge_list[j].id = ct->id;
        particle->edge_list[j].node_id_1 = ct->node1->position; 
        particle->edge_list[j].node_id_2 = ct->node2->position; 
        particle->edge_list[j].actual_scanned_node_id_1 = ct->actualnode1->position;
        particle->edge_list[j].actual_scanned_node_id_2 = ct->actualnode2->position;
        particle->edge_list[j].scanmatch_hit = ct->hitpct;
        particle->edge_list[j].type = ct->type;
        particle->edge_list[j].status = ct->status;
    }

    particle->no_rejected_edges = basic_graph->failed_slam_constraints.size();
    particle->rejected_edge_list = (slam_graph_edge_t *) calloc(particle->no_rejected_edges, sizeof(slam_graph_edge_t));

    j= 0;
    for ( c_it= basic_graph->failed_slam_constraints.begin() ; c_it != basic_graph->failed_slam_constraints.end(); c_it++, j++ ){        
        SlamConstraint *ct = c_it->second;
        
        particle->rejected_edge_list[j].id = ct->id;
        particle->rejected_edge_list[j].node_id_1 = ct->node1->position; 
        particle->rejected_edge_list[j].node_id_2 = ct->node2->position; 
        particle->rejected_edge_list[j].actual_scanned_node_id_1 = ct->actualnode1->position;
        particle->rejected_edge_list[j].actual_scanned_node_id_2 = ct->actualnode2->position;
        particle->rejected_edge_list[j].scanmatch_hit = ct->hitpct;
        particle->rejected_edge_list[j].type = ct->type;
        particle->rejected_edge_list[j].status = ct->status;
    }
    particle->weight = normalized_weight;
    if(param.verbose)
        fprintf(stderr, "Weight : %f\n", normalized_weight);
}
