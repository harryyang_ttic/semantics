/*
 * NodeScan.h
 *
 *  Created on: Oct 22, 2009
 *      Author: abachrac
 */

#ifndef SLAMGRAPH_H_
#define SLAMGRAPH_H_
#include <vector>
#include <map>
#include <isam/isam.h>
#include <geom_utils/geometry.h>
#include <geom_utils/convexhull.h>

#include <dai/alldai.h>  // Include main libDAI header file
#include <dai/jtree.h>
#include <dai/bp.h>
#include <dai/decmap.h>
#include <dai/factorgraph.h>
#include <lcmtypes/slam_lcmtypes.h>
#include <boost/config.hpp>
#include <boost/graph/adjacency_list.hpp>

#include <sr_trainer/slu_rf_classifier.hpp>
#include "NodeScan.h"
#include "Pose2d_Pose2d_Point2d_Factor.hpp"
#include <lcmtypes/perception_object_detection_list_t.h>
#include "OutputWriter.hpp"

using namespace std;
using namespace scanmatch;
using namespace isam;
using namespace Eigen;

//dont use dai namespace - conflicts with isam which also has factors 

#define MAX_COST_QUESTION 1.0e6

#define SUPERNODE_SEARCH_DISTANCE 20
#define LABEL_INCREMENT 1.0
#define LABEL_BLEED_INCREMENT 0.5

#define RESET_COLOR "\e[m"
#define BOLD        "\e[1m"
#define BLACK       "\e[30m"
#define RED         "\e[31m"
#define BLUE        "\e[32m"
#define YELLOW      "\e[33m"
#define GREEN       "\e[34m"
#define MAJENTA     "\e[35m"
#define CYAN        "\e[36m"
#define WHITE       "\e[37m"
#define BOLD_BLACK  "\e[1;30m"
/*  'bold red'     => "\e[1;31m",
  'bold blue'    => "\e[1;32m",
  'bold yellow'  => "\e[1;33m",
  'bold green'   => "\e[1;34m",
  'bold majenta' => "\e[1;35m",
  'bold cyan'    => "\e[1;36m",
  'bold white'   => "\e[1;37m",*/
#define MAKE_GREEN "\e[34m"

//we need to rationalize the classes - they are crappily named 
//#define NO_OF_CLASSES 11 //we should get rid of the old classes - might be possible to change the trained models by replacing the class ids 

//#define NO_OF_LABELS 11 //this would most likely be higher than the no of classes 

#define REGION_FACTOR_ID_INCREMENT 10000

#define REGION_LABEL_ID_INCREMENT 20000

#define PHI_ID_INCREMENT 30000

#define LANG_OBS_ID_INCREMENT 40000

#define NODE_LASER_TYPE_INCREMENT 50000

#define NODE_IMAGE_TYPE_INCREMENT 60000

#define OBJECT_TYPE_INCREMENT 70000

#define REGION_APPEARENCE_ID_INCREMENT 80000

//just change the label in model 
//label 1 10 2 3 4 5 6
//be careful because the feature files will no longer be valid 
using namespace boost;
//Boost stuff
typedef struct _Vertex
{
    int id;
} Vertex;

enum answer_t{
    ANSWER_NO=0, ANSWER_YES =1, ANSWER_INVALID=2, ANSWER_INIT=3, ANSWER_LEFT_OF=4, ANSWER_RIGHT_OF=5, ANSWER_IN_FRONT_OF=6, ANSWER_BEHIND=7, 
    ANSWER_NEAR=8, ANSWER_AT=9, ANSWER_AWAY=10
}; 

typedef enum {
    REGION_EDGE_INCREMENTAL = 0,//SLAM_GRAPH_REGION_EDGE_T_TYPE_INCREMENTAL, 
    REGION_EDGE_LOOPCLOSURE = 1//SLAM_GRAPH_REGION_EDGE_T_TYPE_LOOPCLOSURE
} region_edge_type_t;

enum spatialRelation {LEFT_OF, RIGHT_OF, IN_FRONT_OF, BEHIND, NEAR, AT, AWAY, DOWN_FROM, THROUGH, ACROSS, INVALID};

typedef pair<int, double> indexProb; 

//this might need to be for each spatial result - or the list of results (esp if we consider for all particles) 

struct spatialResult{
    spatialRelation sr;
    double info_gain;
    double cost;
    
    //vector<indexProb> sr_likelihood;
    //map<int, double> sr_likelihood;
    
    //for region - given question whats the likelihood of generating each answer
    map<int, map<int, double> > sr_likelihood;
    
    spatialResult():  sr(INVALID), info_gain(0.0), cost(MAX_COST_QUESTION)
    {}

    spatialResult(spatialRelation _sr, double _info_gain): sr(_sr), info_gain(_info_gain), cost(MAX_COST_QUESTION)
    {}
    
    spatialResult(spatialRelation _sr, double _info_gain, double _cost, map<int, map<int, double> > _sr_likelihood): 
        sr(_sr), info_gain(_info_gain), cost(_cost), sr_likelihood(_sr_likelihood)
    {}
    
    spatialResult(const spatialResult &spatial_result): sr(spatial_result.sr), cost(spatial_result.cost), info_gain(spatial_result.info_gain), sr_likelihood(spatial_result.sr_likelihood)
    {}
};

//typedef pair<spatialRelation, double> spatialResults;
//typedef adjacency_list < listS, vecS, undirectedS, property < vertex_name_t, int >, property < edge_weight_t, double > > graph_t;
typedef adjacency_list < listS, vecS, undirectedS, Vertex, property < edge_weight_t, double > > graph_t;
//typedef UndirectedGraph graph_t;
//typedef typename UndirectedGraph::edge_property_type Weight;
typedef graph_traits < graph_t >::vertex_descriptor vertex_descriptor;
typedef graph_traits < graph_t >::edge_descriptor edge_descriptor;
typedef property < edge_weight_t, double >Weight;
typedef property < vertex_name_t, double >Name;
//typedef typename DirectedGraph::edge_property_type Weight;
//the edges can be slam nodes as well 
typedef std::pair<int, int> Edge;
//end boost stuff
typedef pair<string,string> StringPair;
typedef pair<string, int> LabelCount;
typedef dai::FactorGraph SemFactorGraph;
typedef dai::Var SemVar; 
typedef dai::Factor SemFactor;


typedef pair<int, int> nodePairKey;

class SpatialRelationResult {
    spatialRelation sr;
    double information_gain;
    vector<indexProb> region_likelihood;

    SpatialRelationResult(spatialRelation _sr, double _information_gain, vector<indexProb> _region_likelihood){
        sr = _sr;
        information_gain = _information_gain;
        region_likelihood = _region_likelihood;
    }    
};


class SlamNode;
class LanguageObservation;

class SemanticTpye;

typedef pair<SlamNode*, double> nodeDist;

typedef pair<SlamNode*, SlamNode*> nodePair;
typedef pair< pair<SlamNode*, SlamNode*>, double> nodePairDist;

class RegionNode;
class RegionSegment;
class RegionEdge;

typedef pair<RegionNode*, RegionNode *> regionPair;
typedef pair<RegionNode*, double> regionDist;

typedef pair<regionPair, double> regionPairDist; 

typedef pair<RegionNode*, double> regionProb;

class RegionPath;
typedef pair<RegionPath *, double> regionPathProb;

class RegionConstraint;

class SlamConstraint;

class LabelDistribution;

class LabelInfo;

class SRQuestion;

//the result of question asking calculations

struct answerResult {
    double prob_invalid; 
    double prior; 
    map<int, double> answer_likelihood; 
};

struct questionEvalResult {

public:
    spatialRelation sr;
    SlamNode *q_node;
    //likelihood of generating given answer for that region 
    map<int, map<int, double> > sr_likelihood;  //region , answer_id, prob_of_answer_given_question_and_grounding

    map<int, answerResult> answer_result_map;

    set<int> answer_set; 
    
    //overall likelihood of recieving the answers (marginalizing out the groundings)
    map<int, double> answer_likelihood;  //likelihood of receiving answer
    
    //prior distribution 
    map<int, double> prior_distribution;//vector<regionProb> > prior_distribution;
    map<int, map<int,double> > new_distribution;  //answer_id, region, 
    OutputWriter *output_writer;
    int event_id; 
    char buf[1024];

    questionEvalResult(int _event_id, spatialRelation _sr, SlamNode *_q_node, OutputWriter *_output_writer);

    void add_region(int region_id, double prior, double prob_invalid, map<int, double> answer_likelihood_region, string prefix=string("[Eval SR]"));

    //normalize 
    void normalize_map(map<int, double> &p_map) const;

    bool is_normalized(map<int, double> &p_map) const;

    double get_entropy(map<int, double> &p_map) const;

    double get_expected_information();  

    double get_expected_information_method_1();  

    double get_expected_information_method_2();  

    double get_expected_information_method_3();  

    map<int, map<int,double> > get_sr_answer_map() const;
   
};


struct complex_language_event_t{
    int language_event_id; 
    int64_t node_id;
    //slam_language_label_t *label;
    slam_slu_parse_language_querry_t *parse_result;
    int no_of_new_regions_added;   
    int64_t last_checked_node_id; 
};

struct objectNodePair{
    NodeScan *nd;
    int64_t object_utime;
    Pose2d pose_to_node;
};

typedef pair<RegionNode *, double> regionLandmark;

//typedef vector<SlamNode *> NodePath;

class NodePath{
public:
    NodePath();
    NodePath(vector<SlamNode *> _path);
    NodePath(const NodePath &np);

    vector<Pose> getPathPoses();
    vector<SlamNode *> path;
    size_t size();
    SlamNode *operator[](int pos);
    bool operator==(const NodePath &np);
};

class ComplexLanguageEvent{
public:
        
    ComplexLanguageEvent(complex_language_event_t *event, OutputWriter *_output_writer, LabelInfo *info, bool use_factor_graph=true);

    ComplexLanguageEvent(complex_language_event_t *event, OutputWriter *_output_writer, SlamNode *node, LabelInfo *info, bool use_factor_graph=true);

    bool evaluateOutstandingPaths(SLUClassifier *slu_classifier);
    
    void updateFailedGrounding();
    void updateLastCheckedGrounding(int64_t id);
    int getIDsAddedSinceFailure();
    double getCostOfQuestion(spatialRelation sr, bool q_landmark, double expected_info_gain, SlamNode *q_nd, int last_asked_q_id_global);
    int getNoAskedQuestions();
    void updateAskedQuestions(spatialRelation sr);
    vector<regionProb> getGroundedRegions();
    vector<RegionPath *> getGroundedRegionPaths();
    void setNode(SlamNode *node);
    bool addNodePath(SlamNode *node, NodePath path);
    void removeNodePath(RegionNode *region);
    void clearNodePaths();
    int getOutstandingCount();
    int getNoPaths();
    void printPath(SlamNode *node);
    map<int, double> getLandmarkLikelihoodsForFigure(RegionNode *fig_region);
    void addNewRegionPath(RegionNode *region_node, NodePath path, map<int, double> grounding_prob, double prob);
    void fillNodePaths(slam_complex_language_path_list_t &msg);
    void fillOutstandingNodePaths(slam_complex_language_path_list_t &msg);
    vector<RegionPath *> getRegionPaths();
    int updateResult(vector<pair<RegionNode *, slam_complex_language_path_result_t*> > &path_results);
    bool evaluateRegionPaths(bool ground_only_if_high, vector<RegionNode *> &grounded_regions, double ground_threshold, int max_count);
    
    vector<SRQuestion *> getQuestionsToUpdate();
    vector<SlamNode *> getAnsweredQuestionNodes();

    void evaluateAnsweredQuestions(SLUClassifier *slu_classifier, RegionNode *figure_node, map<SlamNode*, SlamNode *> &parent_map_fig);

    void reevaluateAnsweredQuestion(SLUClassifier *slu_classifier, SRQuestion *question, map<SlamNode*, SlamNode *> &parent_map_q);

    RegionNode *getLandmarkNode(int id);
    RegionPath *getRegionPath(RegionNode *region);
    void print(bool print_path=true);

    void clearLandmarks();
    void updateLandmarks(vector<RegionNode *> landmarks);
    void addLandmark(RegionNode *landmark, int type=0); //0 - check, 1 - valid, 2 - invalid 
    vector<RegionNode *> getValidLandmarks();
    vector<RegionNode *> getInvalidLandmarks();
    void removeLandmark(RegionNode *region);
    vector<regionPathProb> getNormalizePaths(int max_size = -1);
    vector<regionPathProb> getNormalizePathsNoAnswers(int max_size = -1);
    vector<regionProb> getFigureDistribution();
    vector<regionProb> getLandmarkDistribution();
    //need to send back whether the SR is about the landmark or the figure 
    spatialResult evalSRQuestionsFromCurrentLocation(SLUClassifier *slu_classifier, SlamNode *nd, 
                                                     int *landmark, 
                                                     map<SlamNode *, SlamNode *> &parent_map);

    spatialResult evalSRQuestionsFromLandmark(SLUClassifier *slu_classifier, SlamNode *nd, 
                                              int *landmark, 
                                              map<SlamNode *, SlamNode *> &parent_map, bot_lcmgl_t *lcmgl = NULL);

    vector<spatialResult> getQuestionCosts(SLUClassifier *slu_classifier, SlamNode *nd, 
                                           int *landmark, 
                                           map<SlamNode *, SlamNode *> &parent_map, 
                                           const vector<spatialRelation> sr_list);
    
    void printFigureDistribution(string prefix);

    double evalQuestionsAboutRegion(RegionNode *region, double info_gain_threshold, int *landmark);

    void updateSRQuestionAnswer(SRQuestion *question, string answer);

    int64_t node_id;
    SlamNode *utterence_node;
    int event_id;
    LabelInfo *info; //this prob should be a static class 
    string utterance; 
    string landmark; //do we keep SDC representations?? 
    string figure; 
    int figure_id;
    string sr; 
    int outstanding_language; 
    //if we index this with RegionNodes we need to make sure to update properly when a region is deleted 
    map<RegionNode *, RegionPath *> node_paths;
    map<int, regionLandmark> valid_landmarks;
    map<int, regionLandmark> invalid_landmarks;

    vector<SRQuestion *> answered_questions;

    //there should be a list of associated SR results with this complex language event 
    //this could be for all the answers that we received 
    //when the likelihoods are calculated - 

    int id_at_failed_time;
    int last_checked_id;
    int last_added_id;

    /*int64_t utime_failure;
    int64_t utime_last_added; 
    int64_t utime_last_checked; */

    bool complete;
    bool handled_internally;
    bool failed_grounding; 
    int number_of_regions_added_since_failure;
    
    bool use_factor_graph;
    //set<RegionNode *> valid_landmarks;
    //map<RegionNode *, NodePath> outstanding_paths;

    vector<spatialRelation> asked_questions;
    char buf[1024];
    OutputWriter *output_writer;
};

class SlamNode {
public:
    //static int phi_last_id;
    //static int language_last_id;
    int id;
    vertex_descriptor b_vertex;
    int position;
    int processed; 
    int prob_used;
    RegionSegment *segment;
    NodeScan *slam_pose; //should not be deleted
    RegionNode *region_node;
    int parent_supernode;
    double cov[9]; 
    SlamNode *prev_node;
    SlamNode *next_node;
    
    void printID();
    //this should be in the LanguageObservation class - and the factors should be created in the constructor
    //int getNextPhiID();
    //int getNextLangID();
    SlamNode(NodeScan *_slam_pose, LabelInfo *info, graph_t &_graph);
    //SlamNode(NodeScan *_slam_pose, LabelDistribution *ld);
    SlamNode(SlamNode *_node, graph_t &_graph);//, LabelDistribution *ld);
    void updateBoundingBox();
    void updateScanPoints();
    double pofz;
    pointlist2d_t *ptr;
    pointlist2d_t *ptr_global;
    pointlist2d_t *bounding_box_bot_frame;
    pointlist2d_t *bounding_box_global;
    
    //all these 
    vector<LanguageObservation *> language_observations;

    virtual ~SlamNode();
    void updateNodeMsg(slam_graph_node_t *nd, int polygonMode);
    Pose2d getPose();
    void resetProbability();
    void updateLanguageFactors();
    void addLanguageAnnotation(LabelInfo *info, double prob_correspondance, slam_language_label_t *annotation);
    void addLanguageAnnotation(LabelInfo *info, double prob_correspondance, int label_id);
    double getDistanceToNode(SlamNode *nd);
    void updateRegion(RegionNode *region);
    RegionNode *getRegion();

    SlamConstraint *inc_constraint;
    //these should only contain loop closure constraints now 
    //constraints coming in to this node 
    map<int, SlamConstraint *> constraints; //this is keyed on the id of the second node - should be deleted on destroy
    map<int, SlamConstraint *> constraints_from;

    map<int, double> node_appearance_dist;

    Pose2d_Node *pose2d_node; //should be delted on deletion

    //************************** Semantic Layer *************************************// 
    //The slam nodes should contain factors that represent semantic observations made at each node 
    SemVar  *node_type;
    SemFactor *node_appearance_factor;
    SemFactor *node_type_to_laser_obs; //add the confusion matrix here 
    SemFactor *node_type_to_image_obs;
    SemVar    *type_obs_laser;
    SemVar    *type_obs_image;
    SemFactor *sem_type_laser_obs; //observation of semantic type using laser
    SemFactor *sem_type_image_obs; //observation of semantic type using image
    
    //the label distributions might be depricated in the future 
    LabelDistribution *labeldist;
    
    double *prob_of_scan; 
    double max_scan_prob; 

    BotTrans queryBodyToMatchBody;
    int valid;
};



struct InfoResult {
    double info_gain;
    double cost;

    InfoResult(double info_gain_, double cost_):info_gain(info_gain_), cost(cost_){
    }
};

class SRAbstractQuestion {
public:
    int event_id; 
    int node_id; 
    int landmark_id; 
    spatialRelation sr; 

    double weighted_info_gain; 
    double weighted_cost;
    double sum_weights; 
    //this might be complicated for landmark based questions 
    map<int, InfoResult> info_result; 

    SRAbstractQuestion(int event_id_, spatialRelation sr_, int node_id_, int landmark_id_=-1);

    double getAverageCost();

    slam_language_question_t *get_question();

    void addParticleQuestion(int particle_id, double info_gain, double cost, double weight);
    /*
      SRAbstractQuestion(int event_id_, spatialRelation sr_, int node_id_, int landmark_id_=-1):
        event_id(event_id_), node_id(node_id_), landmark_id(landmark_id_), sr(sr_), 
        weighted_info_gain(0), weighted_cost(0), sum_weights(0)
    {
    }

    double getAverageCost(){
        return weighted_cost / sum_weights; 
    }

    slam_language_question_t *get_question(){
        slam_language_question_t *msg = (slam_language_question_t *)calloc(1, sizeof(slam_language_question_t));
        msg->id = outstanding_question->id;
        msg->particle_id = p_id;
        msg->utime = bot_timestamp_now();
        string question_string = outstanding_question->getQuestionString();
        msg->question = strdup(question_string.c_str());
        //asked_question = outstanding_question;
        //outstanding_question = NULL;
    }

    void addParticleQuestion(int particle_id, double info_gain, double cost, double weight){
        sum_weights += weight; 
        weighted_info_gain += info_gain * weight; 
        weighted_cost += cost * weight;
        info_result.insert(make_pair(particle_id, InfoResult(info_gain, cost)));
    }
     */
};

class SRQuestion {
public:
    ComplexLanguageEvent *event; 
    int landmark; 
    spatialResult sr_result;
    int id; 
    SlamNode *node;
    Pose2d evaluated_pose; 
    answer_t answer;

    RegionNode *landmark_region;  //if the question is w.r.t. to a landmark - this should be the landmark 
    
    SRQuestion(ComplexLanguageEvent *_event, int _landmark, spatialResult _sr_results, SlamNode *_node, RegionNode *_landmark_region=NULL, int _id=-1);

    SRQuestion(const SRQuestion &question);

    bool hasQuestionPoseChanged();
    
    void updateQuestionPose();

    bool getLandmarkString(string &label_name);

    string getQuestionString();

    string fixLabelString(string str);

    int getLandmarkRegionID();
    
    string getAnswerString();

    int getNodeID();

    void updateAnswer(answer_t _answer);

    void setID(int _id);

    void updateEventCount();

    ~SRQuestion();    
};

class GroundingProb {
public:
    double path_prob; 
    double landmark_prob;
    bool evaluated; 

    map<int, double> answer_likelihoods; 
    
    GroundingProb(double _landmark_prob=.0, double _path_prob=.0):path_prob(_path_prob), landmark_prob(_landmark_prob), evaluated(false){}; 

    void addAnswerLikelihood(int q_id, double prob){
        map<int, double>::iterator it;
        it = answer_likelihoods.find(q_id);
        if(it != answer_likelihoods.end()){
            answer_likelihoods.erase(it);
        }
        answer_likelihoods.insert(make_pair(q_id, prob));
    }    
};

class RegionPath {
public:
    //figure region 
    RegionNode *region;
    NodePath path; 
    LanguageObservation *lang_obs;
    //landmark nodes 
    map<RegionNode *, GroundingProb> landmark_prob;
    bool updated;
    map<int, double> sr_answers;

    double prob; //probability after marginalizing out the landmarks 

    //SLUDataPoint *slu_datapoint;

    int ungrounded_landmarks;

    RegionPath(RegionNode *region, NodePath _path, LabelInfo *info, int figure_id, bool use_factor_graph);
    RegionPath(const RegionPath &path, bool use_factor_graph);

    bool evaluateLanguage(SLUClassifier *slu_classifier, string sr, OutputWriter *output_writer);
    bool evaluateLanguageNoLandmark(SLUClassifier *slu_classifier, string sr, OutputWriter *output_writer);

    map<int, double> getPathProbs();
    ~RegionPath();
    void addLandmark(regionLandmark region);
    void removeLandmark(RegionNode *region);
    bool updatePath(NodePath path);    
    void print();
    void updateProbability(RegionNode *l_region, double _prob);
    void addLandmarkAnswerResult(RegionNode *l_region, int q_id, double prob);

    //void createSLUDataPoint();    

    int addFigureAnswer(int q_id, double _prob);
    bool getFigureAnswerLikelihood(int q_id, double &_prob);
    void marginalizeLandmarks();
    //finds the max combination
    void maxLandmarks();
    double getProbability();
    double getInitialProbability();
    void printProbability();
    bool isUpdated();
    double getPathProbability(RegionNode *region);
    map<int, double> getNormalizedLandmarkDistribution();
};

//similar to node scan?
class ObjectDetection {
public:
    int id; 
    int type; 
    objectNodePair *closest_node_pair;
    Pose2d pose_to_close_node; 

    ObjectDetection(int _id, int _type);
    int updateDetection(perception_object_detection_t *detection, NodeScan *nd);
    
    //int assigned_node_id; 
    //SlamNode *assigned_node; //this can change - if we find a node that is closer to the object 
    //Pose2d trans_to_node; //in which case the transform will also change - should this be a bottrans?
    map<NodeScan *, objectNodePair *> observation_map;
    //each particle might pick its own assigned node?? 
    //vector<nodeTransform> observations; //observations of the object and the node ids at which it was observed 
};

class RawDetection{
public:
    ObjectDetection *object; 
    perception_object_detection_t *observation;

    RawDetection(ObjectDetection *_object, perception_object_detection_t *_observation){
        object = _object;
        observation = _observation; 
    }
    
    ~RawDetection(){
        perception_object_detection_t_destroy(observation);
    }
};

class SlamObject {
public:
    int id; 
    int type; 
    int valid;
    ObjectDetection *objectDetection;
    SlamNode *assigned_node;
    RegionNode *assigned_region;
    SemVar  *object_type;  //represents the type of object 
    SemFactor *object_type_observation; //this is based on the april tags observation 
    SemFactor *object_to_region_type_factor; //this is based on the april tags observation 
    //Pose2d trans_to_node; //in which case the transform will also change - should this be a bottrans?
    Pose2d pose; //pose in map frame 
    
    SlamObject(ObjectDetection *_objectDetection, LabelInfo *label_info);

    void updateDetection(ObjectDetection *_objectDetection);

    void updateAssignedNode(SlamNode *node, LabelInfo *label_info);

    void removeAssignedRegion();

    Pose2d getPose(int *valid);

    void print();

    void updatePose();
};

class LabelInfo{
public:
    map<string,int> appearance_to_index;
    map<string, int> category_to_index;
    map<int, string> index_to_category;
    map<string, int> object_name_to_index;
    map<int, string> object_index_to_name; 
    map<int, int> object_id_to_index;
    map<int, int> object_index_to_id;
    map<string, int> labels_to_index; 
    map<int, string> index_to_labels;
    
    //node appearance given laser appearance 
    map<StringPair, double> p_appearance_given_laser;
    //node appearance given image appearance 
    map<StringPair, double> p_appearance_given_image;
    //region appearance given node appearance 
    map<StringPair, double> p_r_appearance_given_node_a;
    map<StringPair, double> p_category_given_appearance;
    map<StringPair, double> p_category_given_object;
    map<StringPair, double> p_label_given_category;
    map<StringPair, double> p_category_given_label;
    map<StringPair, double> p_label_given_label_h;
    map<StringPair, double> p_landmark_given_label;

    LabelInfo();
    
    double getProbabilityOfLandmarkGivenLabel(string landmark, string label);
    void buildLandmarkToLabel(map<string, int> &labels_to_index, vector<set<string> > &synonym_set, map<StringPair, double> &p_landmark_given_label);
    void normalizeDistribution(map<string, int> &g_var, map<string, int> &var, map<StringPair, double> &dist, bool v=false);
    void printDistribution(map<string, int> &g_var, map<string, int> &var, map<StringPair, double> &dist);
    void print();
    int getIndexFromClassifierClassId(int index);
    int getClassifierClassIdFromIndex(int index);

    int getIndexFromObjectId(int id);

    void getLabelLcmMessage(slam_label_info_t *msg);
    void getTypeLcmMessage(slam_label_info_t *msg);
    void getApperenceLcmMessage(slam_label_info_t *msg);
    int getIndexForLabel(string label);
    int getLabelSimilarity(string label1, string label2);
    string getTypeNameFromID(int id);
    string getLabelNameFromID(int id);
    
    SemVar *getAppearenceVar(int id);
    SemVar *getLaserAppearenceVar(int id);
    SemVar *getImageAppearenceVar(int id);
    SemVar *getRegionTypeVar(int id);
    SemVar *getRegionLabelVar(int id);
    SemVar *getNodeLabelObsVar(int id);
    SemVar *getNodePhiVar(int id);

    //node appearance given image appearance 
    SemFactor *getImageConfusionMatrix(SemVar *node_type, SemVar *image_observation);
    //node appearance given laser appearance 
    SemFactor *getLaserConfusionMatrix(SemVar *node_type, SemVar *laser_observation);
    //region appearance given node appearance 
    SemFactor *getRegionAppearenceToAppearenceFactor(SemVar *region_appearance, SemVar *node_type);
    //region type given region appearance     
    SemFactor *getRegionTypeToAppearenceFactor(SemVar *region_type, SemVar *region_appearance);
    //this is fuzzy 
    //region type given region label 
    SemFactor *getRegionTypeToLabelFactor(SemVar *region_type, SemVar *region_label);
    //region category given object 
    SemFactor *getObjectToRegionTypeFactor(SemVar *region_type, SemVar *object_type);
    //region label observation factor 
    SemFactor *getRegionLabelFactor(SemVar *node_label, SemVar *phi_nd_region, SemVar *region_label);
    //region label prior - not sure if we should add this - or if it makes a difference 
    SemFactor *getRegionLabelPriorFactor(SemVar *region_label);

    bool verbose;
};

/*class LabelInfo{
public:
    map<string, int> object_name_to_index;
    map<int, string> object_index_to_name; 
    map<int, int> object_id_to_index;
    map<int, int> object_index_to_id;

    //room type, object, occurance 
    double high_object_value;
    double low_object_value;
    map< pair<string, string>, double> object_occurance_map;

    map<string, int> label_to_index; 
    map<string, map<string, int> > type_to_labels;
    map<string, int> type_to_index;
    map<string, int> class_to_classifier_index; //this is the classifier mapping - which is not the same 
    double high_type_value;
    double medium_type_value;
    double low_type_value; 
    map< pair<string, string>, double> region_type_to_appearance_class; //this is the classifier mapping - which is not the same 
    
    int high_label_value;
    int medium_label_value;
    map< pair<string, string>, int> synonym_map;
    map<int, int> index_to_classifier_class;
    map<int, int> classifier_class_to_index;
    map<string, int> classifier_label_to_index;

    map<string, map<string, double> > laser_confusion_matrix;
    map<string, map<string, double> > image_confusion_matrix;

    LabelInfo();
    void print();
    int getIndexFromClassifierClassId(int index);
    int getClassifierClassIdFromIndex(int index);

    int getIndexFromObjectId(int id);

    void getLabelLcmMessage(slam_label_info_t *msg);
    void getTypeLcmMessage(slam_label_info_t *msg);
    void getApperenceLcmMessage(slam_label_info_t *msg);
    int getIndexForLabel(string label);
    int getLabelSimilarity(string label1, string label2);
    };*/




class LanguageObservation {
public:
    SemVar *label_obs_node;
    SemVar *phi;  //how likely this observation came from this node 
    SemFactor *label_obs_factor;
    SemFactor *phi_obs_factor;
    static int phi_last_id;
    static int language_last_id;
    //lets assume that this happens everytime the region changes hands - so we will not worry about this factor not being updated 
    //otherwise we should keep the currently attached region id in hand as well 
    SemFactor *label_phi_region; //this needs to be rebuilt every time the node changes hands between regions 

    LanguageObservation(LabelInfo *info, RegionNode *region=NULL);

    void setPhi(double p_corr);
    
    void setLabelObservation(int label_id);

    void updateRegionFactor(LabelInfo *info, RegionNode *region);

    int getNextPhiID();

    int getNextLangID();

    ~LanguageObservation();
};

class ConstraintKey {    
    int id_1;
    int id_2;
    
    ConstraintKey();
    ConstraintKey(int _id_1, int _id_2);
};


class SlamGraph {
public:
    SlamGraph(LabelInfo *info, int64_t _utime, OutputWriter *output_writer, bool use_factor_graphs=true, SLUClassifier *_slu_classifier = NULL, 
              double ignore_landmark_dist = 20.0, double ignore_figure_dist = 20.0, bool use_laser_for_semantic = true, 
              bool use_image_for_semantic = true);
    
    virtual ~SlamGraph();
    
    SlamNode * addSlamNode(NodeScan *pose);
    int getSlamNodePosition(int64_t id);
    //just removes the node - doesnt care about other constraints it might break
    int removeSlamNodeBasic(int node_id);
    
    graph_t b_graph;
    //removes slam node and assignes the constraints (least for now to the next SlamNode
    int removeSlamNode(int node_id);

    int64_t getLastNodeID();
    SlamNode * getSlamNodeFromPosition(int64_t id);
    SlamNode * getSlamNodeFromID(int64_t id);
    void addOriginConstraint(SlamNode *node1, Pose2d transform, Noise noise);
    SlamConstraint * addConstraint(int id, SlamNode *node1,  SlamNode *node2, SlamNode *actualnode1, 
                                   SlamNode *actualnode2, Pose2d transform, Noise noise, 
                                   double hitpct, int32_t type, int32_t status);
    void addDummyConstraint(int id, SlamNode *node1,  SlamNode *node2, SlamNode *actualnode1, 
                            SlamNode *actualnode2, Pose2d transform, Noise noise, 
                            double hit_pct, int32_t type, int32_t status);
    SlamConstraint * addConstraint(SlamNode *node1,  SlamNode *node2, SlamNode *actualnode1, 
                                   SlamNode *actualnode2, Pose2d transform, Noise noise, 
                                   double hitpct, int32_t type, int32_t status);
    void addDummyConstraint(SlamNode *node1,  SlamNode *node2, SlamNode *actualnode1, 
                            SlamNode *actualnode2, Pose2d transform, Noise noise, 
                            double hit_pct, int32_t type, int32_t status);
    int removeConstraint(int constraint_id);
    vector<RegionNode*> getConnectedRegions(RegionNode *current_region);
    vector<pair<RegionNode*, RegionNode *> > getConnectedRegionPairs(RegionNode *skip=NULL);

    vector<RegionNode *> getLabeledRegions(RegionNode *current_region, double threshold);

    int hasConstraint(int cons_id);
    int hasConstraint(int id_1, int id_2, int32_t type); 
    int hasConstraint(int id_1, int id_2); 
    int getConstraintID(int id_1, int id_2, int32_t type);
    int removeConstraint(int id_1, int id_2, int32_t type);
    int removeConstraint(SlamConstraint *constraint);
    SlamNode * addSlamNode(SlamNode *_node);
    SlamNode * getLastSlamNode();
    //string askSRQuestion(SRQuestion &question);
    int updateAnswer(int id, string answer);
    void clearQuestion();
    slam_language_question_t* getOutstandingQuestion(int64_t p_id);
    void checkSRQuestionInfoGain(SlamNode *node, bool considerBotRelativeYesNoQuestions, bool considerBotRelativeMCQQuestions, 
                                 bool considerLandmarkQuestions, bot_lcmgl_t *lcmgl = NULL);
    void updateOutstandingQuestion(SRQuestion &question);
    void checkCurrentRegionForInfoGain(RegionNode *current_region);
    void updateComplexLanguage(map<int, ComplexLanguageEvent *> &language_to_match);
    int addComplexLanguageUpdates(slam_complex_language_result_t *slu_msg, vector<RegionNode *> &grounded_regions, bool finish_with_grounding=false, bool finish_always = false);
    void addComplexLanguageEvent(complex_language_event_t *complex_lang);
    
    void updateConnectivityGraph();
    ComplexLanguageEvent *getComplexLanguageEvent(int event_id);
    vector<SRQuestion> getSRQuestionForLanguageEvent(int event_id, 
                                                     bool considerBotRelativeYesNoQuestions=true, 
                                                     bool considerBotRelativeMCQQuestions=false, 
                                                     bool considerLandmarkQuestions=false, 
                                                     bot_lcmgl_t *lcmgl=NULL);
    ComplexLanguageEvent * getComplexLanguageEvent(SlamNode *node);
    //SlamNode * addSlamNode(NodeScan *pose, LabelDistribution *ld);
    //not sure why this is a pointer
    int hasConstraint(int const_id, int32_t *type, int32_t *status);
    MatrixXd getCovariances(SlamNode *node1, SlamNode *node2);
    MatrixXd getCovariance(SlamNode *node);
    map<int,MatrixXd> getCovariances(SlamNode *nd1, vector<SlamNode *> nd_list);
    double calculateProbability(SlamNode *node1, SlamNode *node2, Matrix3d cov_sm, Pose2d sm_constraint);
    
    void updateSupernodeIDs();
    void updateBoundingBoxes();

    //void findShortestPath(SlamNode *node);
    //void findShortestPathRebuild(SlamNode *node);
    map<SlamNode*, SlamNode *> getShortestPathTree(SlamNode *node);
    map<SlamNode*, SlamNode *> getShortestPathTree(SlamNode *node, graph_t &g);
    void getShortestPathTree(SlamNode *node, graph_t &g, map<SlamNode*, SlamNode *> &parent_map, map<SlamNode *, double> &dist_map);

    //void buildRegionBoostGraph(graph_t &g);
    map<RegionNode*, RegionNode *> getShortestPathRegionTree(RegionNode *node);
    map<RegionNode*, int> getRegionDistance(RegionNode *region, vector<RegionNode *> c_region_list, int max_dist=100);
    void buildBoostGraph(graph_t &g, bool add_close_node_connections=true);
    //returns the no
    vector<RegionNode *> findShortestPathFromOutstandingComplexLanguage(RegionNode *current_region, bool complete_event, bool complete_if_valid);
    void findShortestPathFromAllComplexLanguage(RegionNode *current_region);
    void findShortestPathFromAllComplexLanguageToCurrentRegion(RegionNode *current_region);

    void printParentMap();
    void updateParentMap();
    void updateParentMap(graph_t &g);
    vector<SlamNode *> getPathsToRegion(RegionNode *node, double *dist);

    void evaluateAllComplexLanguageGroundings(RegionNode *current_region);

    void printComplexLanguageEvents();
    int getOutstandingLanguageCount();

    int getLanguagePathCount();

    RegionNode *createNewRegion();
    RegionNode *createNewRegion(int id);
    void printRegionConnections(bool print_edges = true);
    void calculateRegionEfficiency();
    void pruneRegionConnections();
    int getNextSupernodeID(int ind, int search_distance);
    int getPreviousSupernodeID(int ind, int search_distance);
    //this will be changed soon 
    int belongsToCurrentSupernode(int ind, int curr_sn, int prev_sn, int next_sn);
    
    void updateRegionConnections();
    void updateRegionDistances();

    //this will update slam and fill the covariances 
    void runSlam();
    void clearDummyConstraints();
    SlamConstraint * getConstraint(int const_id);

    int addNodeToRegion(SlamNode *node, int id);
    int addNodeToRegion(SlamNode *node, RegionNode *region);

    void updateRegionMeans();

    RegionNode * getRegionFromID(int region_id);
    void removeRegion(RegionNode *r_node);
    void removeComplexLanguageNodePaths(RegionNode *region);
    RegionNode* mergeRegions(RegionNode *region1, RegionNode *region2);

    //double getClosestDistanceToRegion(double xy[2], RegionNode *region, SlamNode *closestNode);

    SlamNode *getClosestDistanceToRegion(Pose2d pose, RegionNode *current_region, double *distance);

    double getClosestDistanceToRegion(SlamNode *node_to_check, RegionNode *region, 
                                             SlamNode *closestNode);

    double findClosestRegion(RegionNode *region1, RegionNode *region2, SlamNode *node1, SlamNode *node2);

    double findDistanceToRegions(RegionNode *querry_region, std::vector<nodePairDist> *region_dist_list);
    
    double findDistanceToSegments(RegionSegment *querry_region, std::vector<nodePairDist> &segment_dist_list, double threshold = 100000);

    double findDistanceToRegions(SlamNode *querry_node, std::vector<nodePairDist> *region_dist_list);

    double findMeanDistanceToRegions(SlamNode *querry_node, std::vector<nodePairDist> *region_dist_list);

    double findMeanDistanceToRegions(RegionNode *querry_region, std::vector<nodePairDist> *region_dist_list);
    
    double findDistanceToRegionMeans(RegionNode *querry_region, std::vector<nodePairDist> *region_dist_list, double add_threshold=10000);
    
    void getDistanceToRegions(Pose2d pose, RegionNode *current_region, std::vector<nodeDist> *region_dist_list);

    void getDistanceToNodesInRegion(Pose2d pose, RegionNode *region, 
                                               std::vector<nodeDist> *region_dist_list);

    RegionSegment *createNewSegment();
    RegionSegment *createNewSegment(vector<SlamNode*> nodes); //creates and adds the nodes to the new segment

    void calculateRegionDistance();

    void updateObjects(map<int, ObjectDetection *> &objects);
    void updateObjectSlamNode(SlamObject *object);
    void updateObjects();
    void updateObjectPoses();
    void fillLanguageCollection(slam_complex_language_collection_t &msg);
    void fillOutstandingLanguageCollection(slam_complex_language_collection_t &msg);

    int getNextQuestionID();
    RegionSegment *last_segment; 
    int no_segments_in_region;
    int last_segment_id;
    
    map<pair<int, int>, int> topo_distance;
    bool use_laser_for_semantic;
    bool use_image_for_semantic; 

    RegionNode *last_created_region;
    int last_region_id;
    double ignore_figure_dist;
    double ignore_landmark_dist;
    int64_t utime;
    int updated; //this should be set to 1 after the particle is processed
    
    int status;

    Pose2d_Node *origin_node;
    Pose2d_Factor *prior;

    int no_close_node_pairs; 
    int no_same_region_close_pairs;
    int max_dist_of_close_node_pairs;
    int no_failed_close_node_pairs;
    double average_distance_of_close_node_pairs;
        
    map<int, SlamObject *> object_map; 
    //should we index this by event?
    map<int, ComplexLanguageEvent *> complex_language_events; 
    map<int, ComplexLanguageEvent *> complex_language_events_by_node; 
    vector<RegionNode *> region_node_list; 
    map<int, RegionNode *> region_nodes; 
    vector<RegionSegment *> region_segment_list;
    map<int, RegionSegment *> region_segment_map;
    vector<SlamNode *> slam_node_list;
    map<int, SlamNode *> slam_nodes; //deleted on destroy
    map<int, SlamConstraint *> slam_constraints; //deleted on destroy
    map<int, SlamConstraint *> failed_slam_constraints; //deleted on destroy

    int no_edges_added;
    SlamNode *last_added_node;
    vector<nodePairKey> proposed_language_edges;
    //this should prob be how we use it 
    Pose2d_Pose2d_Factor *origin_constraint; //deleted on destroy

    //---------------- Semantic Layer ---------------//
    //The Slam Graph should contain the factor graph for the observed topology 
    
    //this might get trashed and rebuilt when new regions are added /old regions are removed
    SemFactorGraph *factor_graph;

    void runBP(bool ignore_language= false);

    void runBP(RegionNode *region);

    //this is the conditional factors 
    vector<SemFactor *> obs_model_factors;
    //
    vector<SemFactor *> obs_factors; //some of these are general - describing the observation relationship 
    vector<SemVar *> vars;
    LabelInfo *label_info;
    SRQuestion *outstanding_question; 
    SRQuestion *asked_question; 
    bool use_factor_graphs;
    int new_regions_added_threshold;

    SLUClassifier *slu_classifier; 

    static int asked_question_id;

    OutputWriter *output_writer; 

    map<SlamNode *, SlamNode *> parent_map_current; //based on the shortest path to the current node 
    map<SlamNode *, double> dist_map_current; 

private:
    Slam *slam;
};

class LabelDistribution {
public:
    int num_labels;
    double prior;
    map<string, int> label_map;
    map<int, double> saliency_map;
    vector<string> labels;
    vector<double> saliency; // Saliency threshold (>= 1/num_labels) for the labels. Lower means more salient
    int last_direct_obs_id;
    double initial_count;
    double total_obs;
    vector<double> observation_frequency;
    map<int, int> direct_observations;
    map<int, int>  bled_observations;
    vector<int> count;
    vector<int> labeled_node_ids; // IDs from which the distribution was bled;
    void addDirectObservation(int l, double increment, int lang_update_id);
    bool hasDirectObservation();
    void addObservation(int l, double increment, int src_node_id);
    int bleedLabels (LabelDistribution *ld_src);
    LabelDistribution();
    int getLabelID(string label);
    //LabelDistribution(vector<double> obs);
    LabelDistribution * copy();
    bool areLabelsSalient();
    map<int, double> getProbabilities();
    void getLabelLcmMessage(slam_label_info_t *msg);
    double getBasicCosineSimilarity();
};


//

class RegionEdge {
public:
    RegionNode *node1;
    RegionNode *node2;   
    //int r_node1_id;
    //int r_node2_id;

    //this can be incremental, loop closure etc 
    region_edge_type_t type;

    map<int, SlamConstraint *> edge_list;

    RegionEdge(RegionNode *_nd1, RegionNode *_nd2);
    
    RegionEdge(RegionNode *_nd1, RegionNode *_nd2, region_edge_type_t _type);

    //maybe region edges can also create factors - that connect variables in two regoins - this can lead to bleeding 
    //of certain observations between regions 

    void addEdge(SlamConstraint *ct);
            
    virtual ~RegionEdge();
};


//supernode data structure 
class RegionSegment {
public:
    //a region should be defined by its first node??
    RegionSegment(int _segment_id);
    
    void addNode(SlamNode *node);
    void addNodes(vector<SlamNode *> node_list);
    int removeNode(SlamNode *node);
    SlamNode *getClosestNode(SlamNode *nd, double *min_dist);
    int removeNodes(vector<SlamNode *>node_list);
    int id;
    double ncut;
    RegionNode *region;
    vector<SlamNode *> nodes; //these won't get removed once added 
};

//supernode data structure 
class RegionNode {
public:
    //a region should be defined by its first node??
    RegionNode(int _region_id, bool use_factor_graph, LabelInfo *info=NULL);
    RegionNode(int _region_id, SlamNode *node, bool use_factor_graph, LabelInfo *info=NULL);
    void removeAllNodesFromRegion();
    void addSegment(RegionSegment *segment);
    int addNode(SlamNode *node);
    SlamNode* getLastNode();
    void addCurrentNodesToOtherRegion(RegionNode *region);
    int removeNode(SlamNode *node);
    RegionEdge* addEdge(RegionNode *region2);
    bool isConnected(RegionNode *region2);
    RegionEdge* addEdge(RegionNode *region2, SlamConstraint *ct);
    RegionEdge* getEdge(RegionNode *region2);
    void printRegionEdges();
    vector<RegionNode *> getConnectedRegions();
    double calculateCloseNodes(RegionNode *region);
    int getRegionNodeSemanticID();
    int getRegionLabelSemanticID();
    int getRegionNodeAppearenceID();
    void pruneRegionEdges(bool v = false);
    int deleteEdge(RegionNode *region2);
    void updateMean();
    void addNewObservationFactor(SlamNode *node);
    void removeObservationFactor(SlamNode *node);
    double getOdometryRatio(int *odom_edge_count, int *sm_edge_count);
    void insertRayTracedPoints(pointlist2d_t *points);
    void updateRayTracedPoints();
    bool isIncrementalEdge(RegionNode *region);

    //is this actually what we want?? 
    double getProbabilityOfLabel(string label);
    double getProbabilityOfLabelPrior(string label);
    void printLabelDistribution();

    bool getMaxLabelAndLikelihood(string &label, double &prob);
    vector<Point> getLandmarkPoints();
    
    virtual ~RegionNode();

    pointlist2d_t *getRegionBoundingbox();
    //for now we will use the global slam map 
    //a region node should have its own slam graph 
    Pose2d_Pose2d_Factor *origin_constraint; 
    //I think this needs anchor nodes?? 
    void runSlam();
    SlamNode *getClosestNode(Pose2d pose, double *min_dist);
    double getClosestDistanceToRegion(RegionNode *q_region, SlamNode *c_node, SlamNode *q_node);
    double getClosestDistanceToRegion(RegionNode *q_region, int *c_node_id, int *q_node_id);
    double getMeanDistanceToRegion(RegionNode *q_region, int *c_node_id, int *q_node_id);
    double getMeanDistanceToRegion(RegionNode *q_region, 
                                   vector<pair<SlamNode *, SlamNode*> > &closest_nodes, 
                                   int *c_node_id, int *q_node_id);
    double getMeanDistanceFromNode(SlamNode *node, int *c_node_id);
    void printRegionNodes();

    int region_id;
    int semantic_id;

    bool use_factor_graph;
    //mean location of the region 
    
    map<int, SlamObject*> observed_objects;
    void removeObject(SlamObject *obj);
    void addObject(SlamObject *obj);
    void removeAllObjects();
    void updateBoundingBox();

    void setActive();
    void setFinished();
    bool getActive();
    
    int getSize();
    pointlist2d_t *ptr_global;
    pointlist2d_t *convex_ptr;

    pointlist2d_t *ptr_simple;
    pointlist2d_t *ptr_simple_global;

    pointlist2d_t *ptr_raytraced_local;
    pointlist2d_t *ptr_raytraced_global;
    pointlist2d_t *ptr_raytraced_convex;
    pointlist2d_t *ptr_raytraced_convex_global;

    Pose2d mean;
    SlamNode *mean_node;
    LabelInfo *label_info;
    LabelDistribution *labeldist;
    vector<SlamNode*> nodes;
    vector<RegionSegment *> segments;
    //internal constraints - between slam nodes that belong to the region 
    vector<SlamConstraint *>constraints;
    Pose2d_Node *pose2d_node; //should be delted on deletion
    
    //external constraints - these should be between the regions 
    //these should be temparary - especially if we go with the Optimize intra-regions first and 
    //inter-regions second 
    //vector<RegionConstraint *> region_constraints; 
    
    //these contain edges to other regions 
    map<int, RegionEdge *> edges;

    //************************* Semantic Layer ************************//
    //Each region should contain nodes for different variables associated with each region 
    SemVar *region_type; 

    SemVar *region_appearance; 
    SemFactor *region_type_given_appearance;    
    map<nodePairKey, SemFactor *> prob_type_given_observation; //this should be created everytime a new node is added to the region 

    //we should have another semantic variable for the region label 
    //we should connect it based on a word to type co-occurence factor 
    //this will result in the label distribution changing when we infer a room type 
    
    //There could be other variables for each region - including the region label 
    //possible affordances etc 
    //in which case there could be a list of factors that connect these variables with eachother 
    //e.g. knowing the label can impact the distribution of the region type - using some co-occurance factor
    SemFactor *label_prior;
    SemVar *region_label; 

    SemFactor *region_type_to_label;
    //vector<SemFactor *> region_node_label_factors;
    
    map<int, double> region_type_dist;

    //these should prob be new classes 
    map<int, double> region_label_dist;

    //distributions without language 
    map<int, double> region_label_dist_no_lang;
    map<int, double> region_type_dist_no_lang;

    //distributions without language 
    map<int, double> region_label_dist_no_answers;
    map<int, double> region_type_dist_no_answers;
        
    //we will prob not have a label distribution for each region 
    //we should have other stuff that gets added - for inference etc - e.g. the label distribution 
    LabelDistribution *label_dist; 

    bool active_region; 
    
private:
    Slam *region_slam;
};

class RegionConstraint{
public:
    int id;
    RegionNode *node1;
    RegionNode *node2;
    //this is prob necessary 
    SlamNode *actualnode1;
    SlamNode *actualnode2;   
    //at the very least these factors, transform, and noise will get updated everytime the slamgraph is optimized
    Pose2d_Pose2d_Factor *ct_pose2d; //should be deleted on deletion
    Pose2d transform;
    Noise noise;
    int processed; 
    RegionConstraint(int id, RegionNode *_node1,  RegionNode *_node2, SlamNode *_actualnode1, SlamNode *_actualnode2, 
                     Pose2d transform, Noise noise);
    virtual ~RegionConstraint();
};

class SlamConstraint{
public:
    int id;
    SlamNode *node1;
    SlamNode *node2;
    SlamNode *actualnode1;
    SlamNode *actualnode2;    
    Pose2d_Pose2d_Factor *ct_pose2d; //should be deleted on deletion
    double hitpct;
    int valid_sm;
    double pofz;
    Pose2d transform;
    Noise noise;
    int processed; 
    int32_t type;
    int32_t status;
    SlamConstraint(int id, SlamNode *_node1,  SlamNode *_node2, SlamNode *_actualnode1, SlamNode *_actualnode2, Pose2d transform, Noise noise, double hitpct, int valid_sm, int32_t type, int32_t status);
    bool checkIfSameConstraint(SlamConstraint *ct);
    virtual ~SlamConstraint();
};

#endif
