#include "SlamGraph.hpp"

NodePath::NodePath(vector<SlamNode *> _path){
    path = _path;
}

NodePath::NodePath() {}

NodePath::NodePath(const NodePath &np){
    path = np.path;
}

vector<Pose> NodePath::getPathPoses(){
    vector<Pose> p_path;
    
    for(int i=0; i < path.size(); i++){
        Pose2d pose = path[i]->getPose();
        double xyzt[4] = {pose.x(), pose.y(), 0, pose.t()};
        Pose pt(i, xyzt);
        p_path.push_back(pt);
    }
    return p_path;
}

size_t NodePath::size(){
    return path.size();
}

SlamNode *NodePath::operator[](int pos){
    if(pos >= 0 && pos < size()){
        return path[pos];
    }
    return NULL;
}

bool NodePath::operator==(const NodePath &np){
    return (path == np.path);
}

