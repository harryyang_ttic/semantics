#include "SlamGraph.hpp"

//initialize the static variables
int LanguageObservation::phi_last_id = PHI_ID_INCREMENT;
int LanguageObservation::language_last_id = LANG_OBS_ID_INCREMENT;

LanguageObservation::LanguageObservation(LabelInfo *info, RegionNode *region){
    label_obs_node = info->getNodeLabelObsVar(getNextLangID());
    phi = info->getNodePhiVar(getNextPhiID());
    label_obs_factor = new SemFactor(dai::VarSet(*label_obs_node));
    phi_obs_factor = new SemFactor(dai::VarSet(*phi));
    label_phi_region = NULL;
    if(region){
        //create the region to language factor 
        updateRegionFactor(info, region);
    }

    setPhi(0.0);
    //this last one needs the region 
}

void LanguageObservation::setPhi(double p_corr){
    for(int i=0; i < phi->states(); i++){
        if(i==0){
            phi_obs_factor->set(i, (1 - p_corr));
        }
        else{
            phi_obs_factor->set(i, p_corr);
        }
    }
}
    
void LanguageObservation::setLabelObservation(int label_id){
    for(int i=0; i < label_obs_node->states(); i++){
        if(label_id == i)
            label_obs_factor->set(i, 1.0);
        else
            label_obs_factor->set(i, .0);
    }
}

void LanguageObservation::updateRegionFactor(LabelInfo *info, RegionNode *region){
    assert(region);
    delete label_phi_region;
    label_phi_region = info->getRegionLabelFactor(label_obs_node, phi, region->region_label);
}

int LanguageObservation::getNextPhiID(){
    return LanguageObservation::phi_last_id++;
}

int LanguageObservation::getNextLangID(){
    return LanguageObservation::language_last_id++;
}

LanguageObservation::~LanguageObservation(){
    delete label_obs_node;
    delete phi;
    delete phi_obs_factor;
    delete label_obs_factor;
    delete label_phi_region;
}
