#include "SlamGraph.hpp"
#include "Utils.hpp"

#define LANGUAGE_TOP_N_GROUNDINGS 5
#define MIN_LANG_GROUNDING_PROB 0.1
#define NO_NODES_ADDED_AFTER_FAILURE 30

//#define REBUILD_BOOST_GRAPH 

using namespace Utils;

int SlamGraph::asked_question_id = 0;

SlamGraph::SlamGraph(LabelInfo *_label_info, int64_t _utime, OutputWriter *_output_writer, bool _use_factor_graphs, SLUClassifier *_slu_classifier, 
                     double _ignore_landmark_dist, double _ignore_figure_dist, bool _use_laser_for_semantic, bool _use_image_for_semantic):
    slu_classifier(_slu_classifier), ignore_landmark_dist(_ignore_landmark_dist), ignore_figure_dist(_ignore_figure_dist), output_writer(_output_writer), 
    last_region_id(0), last_segment_id(0), no_segments_in_region(0), no_edges_added(0), status(0), last_added_node(NULL), 
    last_created_region(NULL), last_segment(NULL), factor_graph(NULL), 
    use_laser_for_semantic(_use_laser_for_semantic), use_image_for_semantic(_use_image_for_semantic), 
    utime(_utime), updated(1), outstanding_question(NULL), asked_question(NULL), use_factor_graphs(_use_factor_graphs)
{
    //label_info = NULL;
    slam = new Slam();
    label_info = _label_info;
    //add the origin node 
    origin_node = new Pose2d_Node();
    slam->add_node(origin_node);
    //add prior for the origin 
    Pose2d prior_origin(0., 0., 0.);
    Noise noise = SqrtInformation(10000. * eye(3));
    prior = new Pose2d_Factor(origin_node, prior_origin, noise);
    slam->add_factor(prior);
    new_regions_added_threshold = 5;
}

 SlamGraph::~SlamGraph()
{
    delete slam;
    //might want to look at this 
    delete prior;
    delete origin_node;
    map<int, SlamNode *>::iterator it;
    for ( it= slam_nodes.begin() ; it != slam_nodes.end(); it++ )
        delete it->second;
    slam_nodes.clear();
    map<int, SlamConstraint *>::iterator c_it;
    for ( c_it= slam_constraints.begin() ; c_it != slam_constraints.end(); c_it++ ){        
        delete c_it->second;
    }
    
    slam_constraints.clear();
    for ( c_it= failed_slam_constraints.begin() ; c_it != failed_slam_constraints.end(); c_it++ ){        
        delete c_it->second;
    }
    failed_slam_constraints.clear();    
    
    delete factor_graph; 
    delete outstanding_question;
    delete asked_question;
}

void SlamGraph::updateObjectPoses(){
    map<int, SlamObject *>::iterator it_o;

    fprintf(stderr, "MAJENTA \n\n================= Objects [%d] ===============\n\n" RESET_COLOR, (int) object_map.size());

    for(it_o = object_map.begin(); it_o != object_map.end(); it_o++){
        SlamObject *object = it_o->second;
        object->updatePose();
        //object->print();
    }
}

void SlamGraph::updateObjectSlamNode(SlamObject *object){
    //search through the detection - find the node that is closest to the observation 
    //for now lets use only the nodes from which it was observed from 
    ObjectDetection *det = object->objectDetection;
    map<NodeScan *, objectNodePair *>::iterator it;

    objectNodePair *closest_node_pair = det->closest_node_pair;
        
    if(closest_node_pair){
        NodeScan *nd_scan = closest_node_pair->nd;
        SlamNode *node = getSlamNodeFromID(nd_scan->node_id);
        object->updateAssignedNode(node, label_info);
    }
}

void SlamGraph::updateObjects(){
    if(object_map.size() == 0)
        return;
    map<int, SlamObject *>::iterator it_o;
    for(it_o = object_map.begin(); it_o != object_map.end(); it_o++){
        SlamObject *object = it_o->second;
        //we can probably skip this if we set the closest node scan when its updated 
        updateObjectSlamNode(object);
    }
    updateObjectPoses();
}

void SlamGraph::updateObjects(map<int, ObjectDetection *> &objects){
    if(objects.size() == 0)
        return;

    char buf[1024];
    sprintf(buf, "[Slam Graph] [Update Objects] Objects size : %d", (int) objects.size());
    output_writer->write_to_buffer(buf);

    map<int, ObjectDetection *>::iterator it_d;
    map<int, SlamObject *>::iterator it_o;
    for(it_d = objects.begin(); it_d != objects.end(); it_d++){
        ObjectDetection *det = it_d->second;
        
        it_o = object_map.find(it_d->first);
        SlamObject *object = NULL;
        if(it_o == object_map.end()){
            sprintf(buf, "[Slam Graph] [Update Objects] Object not found - Creating new (%d)", object->id);
            output_writer->write_to_buffer(buf);
            object = new SlamObject(det, label_info);            
            object_map.insert(make_pair(object->id, object));
        }
        else{
            //we don't need to do this since its already pointing to the right one
            object = it_o->second;
        }
        //we can probably skip this if we set the closest node scan when its updated 
        updateObjectSlamNode(object);
    }
    updateObjectPoses();
}

void SlamGraph::updateComplexLanguage(map<int, ComplexLanguageEvent *> &language_to_match){
    //clear the existing complex language groundings - and recreate them from the given set of groundings
    //the regions and nodes in this complex language event map are from a different particle 
    
    //for each complex language get each grounded figure 
    //and add factors 
    map<int, ComplexLanguageEvent *>::iterator it_lang; 
    map<int, ComplexLanguageEvent *>::iterator it;
    for(it_lang = language_to_match.begin(); it_lang != language_to_match.end(); it_lang++){
        //two data structures maintain the complex language - but neither map should need to change 
        ComplexLanguageEvent *target_event = it_lang->second; //this is the target - we want the old event to match the target event 
        it = complex_language_events.find(it_lang->first);
        assert(it != complex_language_events.end());
            
        ComplexLanguageEvent *old_event = it->second;
        
        //remove the old landmarks
        old_event->clearLandmarks();
        old_event->clearNodePaths(); //we need to compy over the paths from the target 
        fprintf(stderr, "Cleared landmarks and figures\n");

        //now lets fill these from the other particle 
        vector<RegionNode *> valid_lm = target_event->getValidLandmarks();
        vector<RegionNode *> invalid_lm = target_event->getInvalidLandmarks();

        for(int i=0; i < valid_lm.size(); i++){
            RegionNode *nd = valid_lm[i];
            RegionNode *c_nd = getRegionFromID(nd->region_id);
            assert(c_nd);
            old_event->addLandmark(c_nd, 1); //adding as valid landmark
            fprintf(stderr, "Adding Valid Landmark : %d\n", c_nd->region_id);
        }

        for(int i=0; i < invalid_lm.size(); i++){
            RegionNode *nd = invalid_lm[i];
            RegionNode *c_nd = getRegionFromID(nd->region_id);
            assert(c_nd);
            fprintf(stderr, "Adding Invalid Landmark : %d\n", c_nd->region_id);
            old_event->addLandmark(c_nd, 2); //adding as invalid landmark
        }

        //add the node paths 
        vector<RegionPath *> region_paths = target_event->getRegionPaths();
        for(int i=0; i < region_paths.size(); i++){
            RegionPath *rp = region_paths[i];
            RegionNode *c_nd = getRegionFromID(rp->region->region_id);
            old_event->addNewRegionPath(c_nd, rp->path, rp->getPathProbs(), rp->prob);
        }
        
        old_event->complete = target_event->complete;
        old_event->handled_internally = target_event->handled_internally;
        old_event->failed_grounding = target_event->failed_grounding;
        old_event->number_of_regions_added_since_failure = target_event->number_of_regions_added_since_failure;
    }
    //this is being done by the particle 
    //runBP();
}

void SlamGraph::runBP(bool ignore_language){
    static char buf[1024];
    
    if(factor_graph){
        //hopefully this wont segfault when factors are removed when regions get killed off 
        delete factor_graph; 
    }     

    sprintf(buf, "[BP] [Full Graph] Running BP");
    output_writer->write_to_buffer(buf);

    int64_t s_utime = bot_timestamp_now();
    
    //lets rebuild this here 
    vector<SemFactor> semantic_factors; 
    vector<SemFactor> semantic_factors_no_answers; 

    vector<SemFactor> semantic_factors_no_lang; 

    bool print_factors = false;

    ///map<int, RegionNode *> valid_region_labels; 
    for(int i=0; i < region_node_list.size(); i++){
        RegionNode *region = region_node_list[i];
        
        semantic_factors.push_back(*region->label_prior);
        semantic_factors_no_lang.push_back(*region->label_prior);
        semantic_factors_no_answers.push_back(*region->label_prior);
        if(print_factors){
            cout << "Region ID : " << region->region_id << endl;
            cout << "Region Label Prior: " << *region->label_prior << endl;
        }

        //the region label distribution has an effect on the region type distribution 
        if(region->region_type_to_label){
            semantic_factors.push_back(*region->region_type_to_label);
            semantic_factors_no_answers.push_back(*region->region_type_to_label);
            semantic_factors_no_lang.push_back(*region->region_type_to_label);
            if(print_factors)
                cout << "Region Type to Label : " << *region->region_type_to_label << endl;
        }

        if(region->region_type_given_appearance){
            semantic_factors.push_back(*region->region_type_given_appearance);
            semantic_factors_no_answers.push_back(*region->region_type_given_appearance);
            semantic_factors_no_lang.push_back(*region->region_type_given_appearance);
            if(print_factors)
                cout << "Region Type to Appearence : " << *region->region_type_to_label << endl;
        }

        map<nodePairKey, SemFactor *>::iterator it;
        //add p_type_given_observation
        for(it = region->prob_type_given_observation.begin(); it != region->prob_type_given_observation.end(); it++){
            //cout << "Adding factor : " << *it->second <<endl;
            semantic_factors.push_back(*it->second); //*region->region_type);
            semantic_factors_no_answers.push_back(*it->second);
            semantic_factors_no_lang.push_back(*it->second);
            if(print_factors)
                cout << "Obs Factor Image : " << *it->second << endl;
        }

        //add p_obs
        for(int j=0; j < region->nodes.size(); j++){
            SlamNode *node = region->nodes[j];
            
            semantic_factors.push_back(*node->node_appearance_factor);
            semantic_factors_no_answers.push_back(*node->node_appearance_factor);
            semantic_factors_no_lang.push_back(*node->node_appearance_factor);
            
            if(print_factors){
                cout << "Adding factor : " << *node->sem_type_laser_obs;
                cout << "Adding factor : " << *node->sem_type_image_obs;
            }

            for(int k=0; k < node->language_observations.size(); k++){
                //valid_region_labels.insert(make_pair(region->region_id, )
                LanguageObservation *lang_obs =  node->language_observations[k];
                semantic_factors.push_back(*lang_obs->phi_obs_factor);
                semantic_factors_no_answers.push_back(*lang_obs->phi_obs_factor);
                semantic_factors.push_back(*lang_obs->label_obs_factor);
                semantic_factors_no_answers.push_back(*lang_obs->label_obs_factor);
                if(lang_obs->label_phi_region){
                    semantic_factors.push_back(*lang_obs->label_phi_region);
                    semantic_factors_no_answers.push_back(*lang_obs->label_phi_region);
                    if(print_factors)
                        cout << "Language Phi To Label To Region Type " << *lang_obs->label_phi_region << endl; 
                }
                else{
                    fprintf(stderr, "Region Label to Node label factor has not been created - creating factor\n");
                    lang_obs->label_phi_region = label_info->getRegionLabelFactor(lang_obs->label_obs_node, lang_obs->phi, region->region_label);
                    semantic_factors.push_back(*lang_obs->label_phi_region);
                    semantic_factors_no_answers.push_back(*lang_obs->label_phi_region);
                    if(print_factors)
                        cout << "Language Phi To Label To Region Type " << *lang_obs->label_phi_region << endl; 
                }
            }
            //cout << "\nObs Factor Laser : " << *node->sem_type_laser_obs << endl;
            //cout << "Obs Factor Image : " << *node->sem_type_image_obs << endl;
        }
    }

    //add objects 
    map<int, SlamObject *>::iterator it_o;
    for(it_o = object_map.begin(); it_o != object_map.end(); it_o++){
        SlamObject *object = it_o->second;
        //fprintf(stderr, "Object : %d => Type : %d\n", object->id, object->type);
        semantic_factors.push_back(*object->object_type_observation);
        semantic_factors_no_answers.push_back(*object->object_type_observation);
        semantic_factors_no_lang.push_back(*object->object_type_observation);
        //this can cause issues when resampling is done - the object to region factors need to be trashed 

        if(object->object_to_region_type_factor){
            //cout << *object->object_to_region_type_factor << endl;
            semantic_factors.push_back(*object->object_to_region_type_factor);
            semantic_factors_no_answers.push_back(*object->object_to_region_type_factor);
            semantic_factors_no_lang.push_back(*object->object_to_region_type_factor);
        }
    }

    if(1){
        //add this to the languageless factor graph 
        SemFactorGraph *factor_graph_no_lang = new SemFactorGraph(semantic_factors_no_lang);
        sprintf(buf, "[BP] [Full Graph] Running BP Withouth Language");
        output_writer->write_to_buffer(buf);
 
        // Store the constants in a PropertySet object
        dai::PropertySet opts;

        // Set some constants
        size_t maxiter = 10000;
        dai::Real   tol = 1e-9;
        size_t verb = 0;
    
        opts.set("maxiter", maxiter);  // Maximum number of iterations
        opts.set("tol", tol);          // Tolerance for convergence
        opts.set("verbose",verb);     // Verbosity (amount of output generated)
        //opts.set("logdomain",true); 
    
        dai::BP bp_no_lang(*factor_graph_no_lang, opts("updates",string("SEQRND"))("logdomain",true));
        // Initialize belief propagation algorithm
        bp_no_lang.init();
        // Run belief propagation algorithm
        bp_no_lang.run();
    
        int v = 0;
        for(int i =0; i < region_node_list.size(); i++){
            RegionNode *region = region_node_list[i];
        
            SemFactor r_label_fac = bp_no_lang.belief(*region->region_label);
            r_label_fac.normalize();
            if(v)
                cout << "\nRegion (" << region->region_id << ") : Label Factor => " << r_label_fac << endl;  

            region->region_label_dist_no_lang.clear();
            //cout << bp.belief(*region->region_type) << endl; // display the belief of bp for the variables in that factor
            if(v) 
                fprintf(stderr, BLUE "=== Region : %d (Label) =====\n" RESET_COLOR, region->region_id);

            for(int j=0; j < region->region_label->states(); j++){
                //dai::Real denom = P.marginal (*(region->region_type))[j];
                dai::Real denom = r_label_fac[j];
                region->region_label_dist_no_lang.insert(make_pair(j, denom));
                if(v){
                    if(denom < 0.01)
                        continue;
                    fprintf(stderr, BLUE "\t%s = %.3f\n" RESET_COLOR, label_info->getLabelNameFromID(j).c_str(),denom);
                }
            }

            if(v)
                fprintf(stderr, BLUE " ==== Region : %d (Type) =====\n" RESET_COLOR, region->region_id);

            // Report factor marginals for fg, calculated by the belief propagation algorithm
            //cout << "Approximate (loopy belief propagation) factor marginals:" << endl;
            //cout << bp.belief(fg.factor(I).vars()) << endl; // display the belief of bp for the variables in that factor
            SemFactor r_fac = bp_no_lang.belief(*region->region_type);
            r_fac.normalize();
            region->region_type_dist_no_lang.clear();
            //cout << bp.belief(*region->region_type) << endl; // display the belief of bp for the variables in that factor
            for(int j=0; j < region->region_type->states(); j++){
                //dai::Real denom = P.marginal (*(region->region_type))[j];
                dai::Real denom = r_fac[j];
                region->region_type_dist_no_lang.insert(make_pair(j, denom));
                if(v){
                    if(denom < 0.01)
                        continue;
                    fprintf(stderr, BLUE "\t%s = %.3f\n" RESET_COLOR, label_info->getTypeNameFromID(j).c_str(),denom);
                }
            }
        }
        delete factor_graph_no_lang;
    }

    set<RegionNode *> regions_with_complex_lang; 

    //for each complex language get each grounded figure 
    //and add factors 
    if(!ignore_language){        
        //thos one normalizes the grounding probs 
        map<int, ComplexLanguageEvent *>::iterator it_lang; 
        
        int count = 0;

        for(it_lang = complex_language_events.begin(); it_lang != complex_language_events.end(); it_lang++){
            vector<regionPathProb> grounded_regions = it_lang->second->getNormalizePaths(LANGUAGE_TOP_N_GROUNDINGS);
            //should we keep them in language? 
            //it_lang->second->print(true);
            sprintf(buf, "[BP] [Full Graph] [Grounding] Event : %d No of grounded region paths : %d", 
                    it_lang->second->event_id, (int) grounded_regions.size());
            output_writer->write_to_buffer(buf);
            //fprintf(stderr, "========== No of grounded region paths : %d\n", (int) grounded_regions.size());
            //exit(-1);
            for(int l=0; l < grounded_regions.size(); l++){
                //hard coded for now - replace 
                //fprintf(stderr, "\t=>Region %d : Prob: %f\n", grounded_regions[l].first->region->region_id, 
                //grounded_regions[l].second);
                count++;
                
                regions_with_complex_lang.insert(grounded_regions[l].first->region);
                LanguageObservation *lang_obs = grounded_regions[l].first->lang_obs;
                if(lang_obs){
                    //update the phi value
                    lang_obs->setPhi(grounded_regions[l].second);

                    //add the factors 
                    semantic_factors.push_back(*lang_obs->label_phi_region);         
                    semantic_factors.push_back(*lang_obs->phi_obs_factor);
                    semantic_factors.push_back(*lang_obs->label_obs_factor);

                    sprintf(buf, "[BP] [Full Graph] [Grounding] (%d) Event : %d Figure : %d => %f\n", 
                            (int) slam_nodes.size(), it_lang->second->event_id, 
                            grounded_regions[l].first->region->region_id, grounded_regions[l].second);
                    output_writer->write_to_buffer(buf);
                }
                else{
                    fprintf(stderr, "No language factor found for complex language event - Error - Should not call runBP\n");
                    //exit(-1);
                }
            }
        }
    }

    //now add factors for the objects 
    
    factor_graph = new SemFactorGraph(semantic_factors);

    sprintf(buf, "[BP] [Full Graph] Running BP With Language");
    output_writer->write_to_buffer(buf);
     
    // Store the constants in a PropertySet object
    dai::PropertySet opts;

    // Set some constants
    size_t maxiter = 10000;
    dai::Real   tol = 1e-9;
    size_t verb = 0;
    
    opts.set("maxiter", maxiter);  // Maximum number of iterations
    opts.set("tol", tol);          // Tolerance for convergence
    opts.set("verbose",verb);     // Verbosity (amount of output generated)
    //opts.set("logdomain",true); 
    
    dai::BP bp(*factor_graph, opts("updates",string("SEQRND"))("logdomain",true));
    // Initialize belief propagation algorithm
    bp.init();
    // Run belief propagation algorithm
    bp.run();
    
    int v = 0;
    
    for(int i =0; i < region_node_list.size(); i++){
        RegionNode *region = region_node_list[i];
        
        SemFactor r_label_fac = bp.belief(*region->region_label);
        r_label_fac.normalize();
        if(v)
            cout << "\nRegion (" << region->region_id << ") : Label Factor => " << r_label_fac << endl;  

        region->region_label_dist.clear();
        //cout << bp.belief(*region->region_type) << endl; // display the belief of bp for the variables in that factor
        if(v) 
            fprintf(stderr, BLUE "=== Region : %d (Label) =====\n" RESET_COLOR, region->region_id);

        sprintf(buf, "[BP] [Full Graph] [Grounding] [Distribution] Region : %d ================\n", 
                region->region_id); 
        output_writer->write_to_buffer(buf);

        for(int j=0; j < region->region_label->states(); j++){
            //dai::Real denom = P.marginal (*(region->region_type))[j];
            dai::Real denom = r_label_fac[j];
            region->region_label_dist.insert(make_pair(j, denom));
            
            if(regions_with_complex_lang.find(region) != regions_with_complex_lang.end()){
                sprintf(buf, "[BP] [Full Graph] [Grounding] [Distribution] Region : %d => %s - %f\n", 
                        region->region_id, label_info->getLabelNameFromID(j).c_str(),denom); 
                output_writer->write_to_buffer(buf);
            }

            if(v){
                if(denom < 0.01)
                    continue;
                fprintf(stderr, BLUE "\t%s = %.3f\n" RESET_COLOR, label_info->getLabelNameFromID(j).c_str(),denom);
            }
        }

        if(v)
            fprintf(stderr, BLUE " ==== Region : %d (Type) =====\n" RESET_COLOR, region->region_id);

        // Report factor marginals for fg, calculated by the belief propagation algorithm
        //cout << "Approximate (loopy belief propagation) factor marginals:" << endl;
        //cout << bp.belief(fg.factor(I).vars()) << endl; // display the belief of bp for the variables in that factor
        SemFactor r_fac = bp.belief(*region->region_type);
        r_fac.normalize();
        region->region_type_dist.clear();
        //cout << bp.belief(*region->region_type) << endl; // display the belief of bp for the variables in that factor
        for(int j=0; j < region->region_type->states(); j++){
            //dai::Real denom = P.marginal (*(region->region_type))[j];
            dai::Real denom = r_fac[j];
            region->region_type_dist.insert(make_pair(j, denom));
            if(v){
                if(denom < 0.01)
                    continue;
                fprintf(stderr, BLUE "\t%s = %.3f\n" RESET_COLOR, label_info->getTypeNameFromID(j).c_str(),denom);
            }
        }
    }

    bool calculate_dist_without_answers = true; 

    if(calculate_dist_without_answers){
        set<RegionNode *> regions_with_complex_lang; 

        //for each complex language get each grounded figure 
        //and add factors 
              
        //thos one normalizes the grounding probs 
        map<int, ComplexLanguageEvent *>::iterator it_lang; 
        
        int count = 0;

        for(it_lang = complex_language_events.begin(); it_lang != complex_language_events.end(); it_lang++){
            vector<regionPathProb> grounded_regions = it_lang->second->getNormalizePathsNoAnswers(LANGUAGE_TOP_N_GROUNDINGS);
            //should we keep them in language? 
            //it_lang->second->print(true);
            sprintf(buf, "[BP] [Full Graph] [Grounding] [No Answers] Event : %d No of grounded region paths : %d", 
                    it_lang->second->event_id, (int) grounded_regions.size());
            output_writer->write_to_buffer(buf);
            //fprintf(stderr, "========== No of grounded region paths : %d\n", (int) grounded_regions.size());
            //exit(-1);
            for(int l=0; l < grounded_regions.size(); l++){
                //hard coded for now - replace 
                //fprintf(stderr, "\t=>Region %d : Prob: %f\n", grounded_regions[l].first->region->region_id, 
                //grounded_regions[l].second);
                count++;
                
                regions_with_complex_lang.insert(grounded_regions[l].first->region);
                LanguageObservation *lang_obs = grounded_regions[l].first->lang_obs;
                if(lang_obs){
                    //update the phi value
                    lang_obs->setPhi(grounded_regions[l].second);

                    //add the factors 
                    semantic_factors_no_answers.push_back(*lang_obs->label_phi_region);         
                    semantic_factors_no_answers.push_back(*lang_obs->phi_obs_factor);
                    semantic_factors_no_answers.push_back(*lang_obs->label_obs_factor);

                    sprintf(buf, "[BP] [Full Graph] [Grounding] [No Answers] (%d) Event : %d Figure : %d => %f\n", 
                            (int) slam_nodes.size(), it_lang->second->event_id, 
                            grounded_regions[l].first->region->region_id, grounded_regions[l].second);
                    output_writer->write_to_buffer(buf);
                }
                else{
                    fprintf(stderr, "No language factor found for complex language event - Error - Should not call runBP\n");
                    //exit(-1);
                }
            }
        }

        //now add factors for the objects 
    
        SemFactorGraph *factor_graph_no_answers = new SemFactorGraph(semantic_factors_no_answers);

        sprintf(buf, "[BP] [Full Graph] Running BP With Language");
        output_writer->write_to_buffer(buf);
     
        // Store the constants in a PropertySet object
        dai::PropertySet opts;

        // Set some constants
        size_t maxiter = 10000;
        dai::Real   tol = 1e-9;
        size_t verb = 0;
    
        opts.set("maxiter", maxiter);  // Maximum number of iterations
        opts.set("tol", tol);          // Tolerance for convergence
        opts.set("verbose",verb);     // Verbosity (amount of output generated)
        //opts.set("logdomain",true); 
    
        dai::BP bp(*factor_graph_no_answers, opts("updates",string("SEQRND"))("logdomain",true));
        // Initialize belief propagation algorithm
        bp.init();
        // Run belief propagation algorithm
        bp.run();
    
        int v = 0;
    
        for(int i =0; i < region_node_list.size(); i++){
            RegionNode *region = region_node_list[i];
        
            SemFactor r_label_fac = bp.belief(*region->region_label);
            r_label_fac.normalize();
            if(v)
                cout << "\nRegion (" << region->region_id << ") : Label Factor => " << r_label_fac << endl;  

            region->region_label_dist_no_answers.clear();
            //cout << bp.belief(*region->region_type) << endl; // display the belief of bp for the variables in that factor
            if(v) 
                fprintf(stderr, BLUE "=== Region : %d (Label) =====\n" RESET_COLOR, region->region_id);

            sprintf(buf, "[BP] [Full Graph] [Grounding] [Distribution] Region : %d ================\n", 
                    region->region_id); 
            output_writer->write_to_buffer(buf);

            for(int j=0; j < region->region_label->states(); j++){
                //dai::Real denom = P.marginal (*(region->region_type))[j];
                dai::Real denom = r_label_fac[j];
                region->region_label_dist_no_answers.insert(make_pair(j, denom));
            
                if(regions_with_complex_lang.find(region) != regions_with_complex_lang.end()){
                    sprintf(buf, "[BP] [Full Graph] [Grounding] [Distribution]  [No Answers] Region : %d => %s - %f\n", 
                            region->region_id, label_info->getLabelNameFromID(j).c_str(),denom); 
                    output_writer->write_to_buffer(buf);
                }

                if(v){
                    if(denom < 0.01)
                        continue;
                    fprintf(stderr, BLUE "\t%s = %.3f\n" RESET_COLOR, label_info->getLabelNameFromID(j).c_str(),denom);
                }
            }

            if(v)
                fprintf(stderr, BLUE " ==== Region : %d (Type) =====\n" RESET_COLOR, region->region_id);

            // Report factor marginals for fg, calculated by the belief propagation algorithm
            //cout << "Approximate (loopy belief propagation) factor marginals:" << endl;
            //cout << bp.belief(fg.factor(I).vars()) << endl; // display the belief of bp for the variables in that factor
            SemFactor r_fac = bp.belief(*region->region_type);
            r_fac.normalize();
            region->region_type_dist_no_answers.clear();
            //cout << bp.belief(*region->region_type) << endl; // display the belief of bp for the variables in that factor
            for(int j=0; j < region->region_type->states(); j++){
                //dai::Real denom = P.marginal (*(region->region_type))[j];
                dai::Real denom = r_fac[j];
                region->region_type_dist_no_answers.insert(make_pair(j, denom));
                if(v){
                    if(denom < 0.01)
                        continue;
                    fprintf(stderr, BLUE "\t%s = %.3f\n" RESET_COLOR, label_info->getTypeNameFromID(j).c_str(),denom);
                }
            }
        }
        delete factor_graph_no_answers;
    }

    int64_t e_utime = bot_timestamp_now();

    map<int, ComplexLanguageEvent *>::iterator it_lang; 
    
    for(it_lang = complex_language_events.begin(); it_lang != complex_language_events.end(); it_lang++){
        it_lang->second->printFigureDistribution(string("[Figure Dist] [After BP]"));
    }

    sprintf(buf, "[BP] [Full Graph] [Performance] Total Time for BP : %f\n", (e_utime - s_utime) / 1.0e6);
    output_writer->write_to_buffer(buf);
}

void SlamGraph::runBP(RegionNode *region){
    static char buf[1024];
    if(factor_graph){
        //hopefully this wont segfault when factors are removed when regions get killed off 
        delete factor_graph; 
    }
        
    int64_t s_utime = bot_timestamp_now();
    
    //lets rebuild this here 
    vector<SemFactor> semantic_factors; 
    vector<SemFactor> semantic_factors_no_lang; 

    bool print_factors = false;
     
    sprintf(buf, "[BP] [Region] Running BP %d", region->region_id);
    output_writer->write_to_buffer(buf);
      
    semantic_factors.push_back(*region->label_prior);
    semantic_factors_no_lang.push_back(*region->label_prior);
    if(print_factors){
        cout << "Region ID : " << region->region_id << endl;
        cout << "Region Label Prior: " << *region->label_prior << endl;
    }
    
    //the region label distribution has an effect on the region type distribution 
    if(region->region_type_to_label){
        semantic_factors.push_back(*region->region_type_to_label);
        semantic_factors_no_lang.push_back(*region->region_type_to_label);
        if(print_factors)
            cout << "Region Type to Label : " << *region->region_type_to_label << endl;
    }
    
    if(region->region_type_given_appearance){
        semantic_factors.push_back(*region->region_type_given_appearance);
        semantic_factors_no_lang.push_back(*region->region_type_given_appearance);
        if(print_factors)
            cout << "Region Type to Appearence : " << *region->region_type_to_label << endl;
    }
    
    map<nodePairKey, SemFactor *>::iterator it;
    //add p_type_given_observation
    for(it = region->prob_type_given_observation.begin(); it != region->prob_type_given_observation.end(); it++){
        //cout << "Adding factor : " << *it->second <<endl;
        semantic_factors.push_back(*it->second); //*region->region_type);
        semantic_factors_no_lang.push_back(*it->second); //*region->region_type);
        if(print_factors)
            cout << "Obs Factor Image : " << *it->second << endl;
    }
    
    //add p_obs
    for(int j=0; j < region->nodes.size(); j++){
        SlamNode *node = region->nodes[j];

        semantic_factors.push_back(*node->node_appearance_factor);
        semantic_factors_no_lang.push_back(*node->node_appearance_factor);
        
        if(print_factors){
            cout << "Adding factor : " << *node->sem_type_laser_obs;
            cout << "Adding factor : " << *node->sem_type_image_obs;
        }
        
        for(int k=0; k < node->language_observations.size(); k++){
            //valid_region_labels.insert(make_pair(region->region_id, )
            LanguageObservation *lang_obs =  node->language_observations[k];
            semantic_factors.push_back(*lang_obs->phi_obs_factor);
            semantic_factors.push_back(*lang_obs->label_obs_factor);
            if(lang_obs->label_phi_region){
                semantic_factors.push_back(*lang_obs->label_phi_region);
                if(print_factors)
                    cout << "Language Phi To Label To Region Type " << *lang_obs->label_phi_region << endl; 
            }
            else{
                fprintf(stderr, "Region Label to Node label factor has not been created - creating factor\n");
                lang_obs->label_phi_region = label_info->getRegionLabelFactor(lang_obs->label_obs_node, lang_obs->phi, region->region_label);
                semantic_factors.push_back(*lang_obs->label_phi_region);
                if(print_factors)
                    cout << "Language Phi To Label To Region Type " << *lang_obs->label_phi_region << endl; 
            }
        }
        //cout << "\nObs Factor Laser : " << *node->sem_type_laser_obs << endl;
        //cout << "Obs Factor Image : " << *node->sem_type_image_obs << endl;
    }

    map<int, SlamObject *>::iterator it_o;
    for(it_o = region->observed_objects.begin(); it_o != region->observed_objects.end(); it_o++){
        SlamObject *object = it_o->second;
        //fprintf(stderr, "Object : %d => Type : %d\n", object->id, object->type);
        semantic_factors.push_back(*object->object_type_observation);
        semantic_factors_no_lang.push_back(*object->object_type_observation);
        
        //this can cause issues when resampling is done - the object to region factors need to be trashed 
        
        if(object->object_to_region_type_factor){
            //cout << *object->object_to_region_type_factor << endl;
            semantic_factors.push_back(*object->object_to_region_type_factor);
            semantic_factors_no_lang.push_back(*object->object_to_region_type_factor);
        }
    }
    
    //add this to the languageless factor graph 
    if(1){
        SemFactorGraph *factor_graph_no_lang = new SemFactorGraph(semantic_factors_no_lang);

        sprintf(buf, "[BP] [Region] Running BP without Language");
        output_writer->write_to_buffer(buf);
 
        // Store the constants in a PropertySet object
        dai::PropertySet opts;

        // Set some constants
        size_t maxiter = 10000;
        dai::Real   tol = 1e-9;
        size_t verb = 0;
    
        opts.set("maxiter", maxiter);  // Maximum number of iterations
        opts.set("tol", tol);          // Tolerance for convergence
        opts.set("verbose",verb);     // Verbosity (amount of output generated)
        //opts.set("logdomain",true); 
    
        dai::BP bp_no_lang(*factor_graph_no_lang, opts("updates",string("SEQRND"))("logdomain",true));
        // Initialize belief propagation algorithm
        bp_no_lang.init();
        // Run belief propagation algorithm
        bp_no_lang.run();
    
        int v = 0;
           
        SemFactor r_label_fac = bp_no_lang.belief(*region->region_label);
        r_label_fac.normalize();
        if(v)
            cout << "\nRegion (" << region->region_id << ") : Label Factor => " << r_label_fac << endl;  

        region->region_label_dist_no_lang.clear();
        //cout << bp.belief(*region->region_type) << endl; // display the belief of bp for the variables in that factor
        if(v) 
            fprintf(stderr, BLUE "=== Region : %d (Label) =====\n" RESET_COLOR, region->region_id);

        for(int j=0; j < region->region_label->states(); j++){
            //dai::Real denom = P.marginal (*(region->region_type))[j];
            dai::Real denom = r_label_fac[j];
            region->region_label_dist_no_lang.insert(make_pair(j, denom));
            if(v){
                if(denom < 0.01)
                    continue;
                fprintf(stderr, BLUE "\t%s = %.3f\n" RESET_COLOR, label_info->getLabelNameFromID(j).c_str(),denom);
            }
        }

        if(v)
            fprintf(stderr, BLUE " ==== Region : %d (Type) =====\n" RESET_COLOR, region->region_id);

        // Report factor marginals for fg, calculated by the belief propagation algorithm
        //cout << "Approximate (loopy belief propagation) factor marginals:" << endl;
        //cout << bp.belief(fg.factor(I).vars()) << endl; // display the belief of bp for the variables in that factor
        SemFactor r_fac = bp_no_lang.belief(*region->region_type);
        r_fac.normalize();
        region->region_type_dist_no_lang.clear();
        //cout << bp.belief(*region->region_type) << endl; // display the belief of bp for the variables in that factor
        for(int j=0; j < region->region_type->states(); j++){
            //dai::Real denom = P.marginal (*(region->region_type))[j];
            dai::Real denom = r_fac[j];
            region->region_type_dist_no_lang.insert(make_pair(j, denom));
            if(v){
                if(denom < 0.01)
                    continue;
                fprintf(stderr, BLUE "\t%s = %.3f\n" RESET_COLOR, label_info->getTypeNameFromID(j).c_str(),denom);
            }
        }

        delete factor_graph_no_lang;
    }

    map<int, ComplexLanguageEvent *>::iterator it_lang; 
    for(it_lang = complex_language_events.begin(); it_lang != complex_language_events.end(); it_lang++){
        vector<regionPathProb> grounded_regions = it_lang->second->getNormalizePaths(LANGUAGE_TOP_N_GROUNDINGS);
        //should we keep them in language? 
        //it_lang->second->print(true);

        for(int l=0; l < grounded_regions.size(); l++){
            if(grounded_regions[l].first->region != region)
                continue;
                
            //hard coded for now - replace 
            sprintf(buf, "[BP] [Region] [Grounding] Event : %d Region %d Grounding Threshold %f", it_lang->second->event_id, 
                    grounded_regions[l].first->region->region_id, 
                    grounded_regions[l].second);
            output_writer->write_to_buffer(buf);
            
            LanguageObservation *lang_obs = grounded_regions[l].first->lang_obs;
            if(lang_obs){
                //update the phi value?? 
                lang_obs->setPhi(grounded_regions[l].second);

                //add the factors 
                semantic_factors.push_back(*lang_obs->phi_obs_factor);
                semantic_factors.push_back(*lang_obs->label_obs_factor);
                semantic_factors.push_back(*lang_obs->label_phi_region);                
            }
            else{
                fprintf(stderr, "No language factor found for complex language event - Error - Should not call runBP\n");
                exit(-1);
            }
        }
    }
    //now add factors for the objects 
        
    factor_graph = new SemFactorGraph(semantic_factors);
    
    // Store the constants in a PropertySet object
    dai::PropertySet opts;

    // Set some constants
    size_t maxiter = 10000;
    dai::Real   tol = 1e-9;
    size_t verb = 0;
    
    opts.set("maxiter", maxiter);  // Maximum number of iterations
    opts.set("tol", tol);          // Tolerance for convergence
    opts.set("verbose",verb);     // Verbosity (amount of output generated)
    //opts.set("logdomain",true); 
    
        
    dai::BP bp(*factor_graph, opts("updates",string("SEQRND"))("logdomain",true));
    // Initialize belief propagation algorithm
    bp.init();
    // Run belief propagation algorithm
    bp.run();

    int v = 0;
    SemFactor r_label_fac = bp.belief(*region->region_label);
    r_label_fac.normalize();
    if(v)
        cout << "\nRegion (" << region->region_id << ") : Label Factor => " << r_label_fac << endl;  
    
    region->region_label_dist.clear();
    //cout << bp.belief(*region->region_type) << endl; // display the belief of bp for the variables in that factor
    if(v) 
        fprintf(stderr, BLUE "=== Region : %d (Label) =====\n" RESET_COLOR, region->region_id);
    
    for(int j=0; j < region->region_label->states(); j++){
        //dai::Real denom = P.marginal (*(region->region_type))[j];
        dai::Real denom = r_label_fac[j];
        region->region_label_dist.insert(make_pair(j, denom));
        if(v){
            if(denom < 0.01)
                continue;
            fprintf(stderr, BLUE "\t%s = %.3f\n" RESET_COLOR, label_info->getLabelNameFromID(j).c_str(),denom);
        }
    }
    
    if(v)
        fprintf(stderr, BLUE " ==== Region : %d (Type) =====\n" RESET_COLOR, region->region_id);
    
    // Report factor marginals for fg, calculated by the belief propagation algorithm
    //cout << "Approximate (loopy belief propagation) factor marginals:" << endl;
    //cout << bp.belief(fg.factor(I).vars()) << endl; // display the belief of bp for the variables in that factor
    SemFactor r_fac = bp.belief(*region->region_type);
    r_fac.normalize();
    region->region_type_dist.clear();
    //cout << bp.belief(*region->region_type) << endl; // display the belief of bp for the variables in that factor
    for(int j=0; j < region->region_type->states(); j++){
        //dai::Real denom = P.marginal (*(region->region_type))[j];
        dai::Real denom = r_fac[j];
        region->region_type_dist.insert(make_pair(j, denom));
        if(v){
            if(denom < 0.01)
                continue;
            fprintf(stderr, BLUE "\t%s = %.3f\n" RESET_COLOR, label_info->getTypeNameFromID(j).c_str(),denom);
        }
    }

    int64_t e_utime = bot_timestamp_now();
    
    sprintf(buf, "[BP] [Region] [Performance] Total Time for BP : %f", (e_utime - s_utime) / 1.0e6);
    output_writer->write_to_buffer(buf);
}

void SlamGraph::runSlam()
{
    if(!status){
        status = 1; 
        output_writer->write_to_buffer("[Slam Graph] [ISAM] Performing batch optimiziation");
        slam->batch_optimization();
    }
    else{
        output_writer->write_to_buffer("[Slam Graph] [ISAM] Performing update");
        slam->update();
    }
    
    //update the covariances 
    const Covariances& covariances =  slam->covariances().clone();

    int64_t cov_stime = bot_timestamp_now();

    Covariances::node_lists_t node_lists;
    list<Node*> nodes;
    //technically the first node shouldn't have a covariance - but we have a start node and then the first node
    for(int i=0; i < slam_node_list.size(); i++){
        SlamNode *node = slam_node_list.at(i);
        if(node != NULL){
            nodes.push_back(node->pose2d_node);
            node_lists.push_back(nodes);
            nodes.clear();
        }
    }

    list<MatrixXd> cov_blocks = covariances.marginal(node_lists);
    int64_t cov_etime = bot_timestamp_now();
    
    int nd_ind = 0;
    for (list<MatrixXd>::iterator it = cov_blocks.begin(); it!=cov_blocks.end(); it++, nd_ind++) {
        SlamNode *node = slam_node_list.at(nd_ind);
        MatrixXd cv = *it;
        if(node != NULL){
            for(int k=0; k < 9; k++){
                node->cov[k] = cv(k);
            }
        }
        else{
            output_writer->write_to_buffer("[Slam Graph] [ISAM] [Error] Node not found ");
        }
    }    
}

void SlamGraph::updateBoundingBoxes(){
    for(int i=0; i < slam_node_list.size(); i++){
        SlamNode *node = slam_node_list.at(i);
        node->updateBoundingBox();
    }
}

int SlamGraph::getSlamNodePosition(int64_t id){
    SlamNode *nd = getSlamNodeFromID(id);
    if(nd == NULL){
        return -1;
    }
    
    return nd->position;
}

SlamNode * SlamGraph::getSlamNodeFromPosition(int64_t id){
    if(id >= slam_node_list.size())
        return NULL;
        
    return slam_node_list[id];
}

SlamNode * SlamGraph::getSlamNodeFromID(int64_t id){
    map<int, SlamNode *>::iterator it;
    it = slam_nodes.find(id);
    if(it == slam_nodes.end()){
        return NULL;
    }
    return it->second;
}

int SlamGraph::removeConstraint(SlamConstraint *constraint){
    removeConstraint(constraint->id);
}

RegionNode *SlamGraph::createNewRegion(){
    int id = 0;

    id = last_region_id++;
    
    RegionNode *r_node;
    r_node = new RegionNode(id, use_factor_graphs, label_info);

    last_created_region = r_node;
    region_nodes.insert(pair<int, RegionNode *>(r_node->region_id, r_node));
    region_node_list.push_back(r_node);
    //add these to the map and vector 
    
    return last_created_region;
}

RegionNode *SlamGraph::createNewRegion(int id){
    RegionNode *r_node;
    r_node = new RegionNode(id, use_factor_graphs, label_info);
    
    last_region_id = fmax(id, last_region_id)+1;
    last_created_region = r_node;
    region_nodes.insert(pair<int, RegionNode *>(r_node->region_id, r_node));
    region_node_list.push_back(r_node);
    //add these to the map and vector 
    
    return last_created_region;
}

void SlamGraph::pruneRegionConnections(){
    //fprintf(stderr, "===== Region Connectivity =====\n");
    for(int i=0; i < region_node_list.size(); i++){
        RegionNode *region = region_node_list[i];
        region->pruneRegionEdges(false);
    }
    //fprintf(stderr, "================================\n");
}

vector<RegionNode*> SlamGraph::getConnectedRegions(RegionNode *current_region){
    vector<RegionNode*> connected_regions;
    for(int i=0; i < region_node_list.size(); i++){
        RegionNode *region1 = region_node_list[i];
        if(region1==current_region) 
            continue;
        if(region1->isConnected(current_region)){
            connected_regions.push_back(region1);
        }
    }
    return connected_regions;
}

vector<regionPair> SlamGraph::getConnectedRegionPairs(RegionNode *skip){
    vector<regionPair> region_pairs;
    for(int i=0; i < region_node_list.size(); i++){
        RegionNode *region1 = region_node_list[i];
        if(region1 == skip)
            continue;
        for(int j=0; j < region_node_list.size(); j++){
            if(i==j){
                continue;
            }
            RegionNode *region2 = region_node_list[j];
            if(region2 == skip)
                continue;
            if(region1->isConnected(region2)){
                region_pairs.push_back(make_pair(region1, region2));
            }
        }
    }
    return region_pairs;
}

void SlamGraph::printRegionConnections(bool print_edges){
    fprintf(stderr, " No of Regions : %d - No of Nodes : %d No of Edges : %d\n", (int) region_node_list.size(), (int) slam_nodes.size(), (int) slam_constraints.size());

    fprintf(stderr, "===== Region Connectivity =====\n");
    for(int i=0; i < region_node_list.size(); i++){
        RegionNode *region = region_node_list[i];
        fprintf(stderr, "\t [%d] No of Nodes : %d\n", region->region_id, (int) region->nodes.size());
        if(print_edges){
            region->printRegionEdges();
        }
    }
    fprintf(stderr, "================================\n");
}

void SlamGraph::calculateRegionDistance(){
    fprintf(stderr, "===== Region Efficiency =====\n");
    
    updateRegionDistances();

    int total_count = 0; 
    int total_distance = 0;
    int no_of_failed_edges = 0;
    int max_distance = 0;

    int same_region = 0;
    int different_region = 0;

    map<pair<int, int>, int>::iterator it_1_2;
    for(int i=0; i < slam_nodes.size(); i++){
        for(int j=0; j < slam_nodes.size(); j++){
            if(i == j)
                continue; 
            
            Pose2d delta = slam_nodes[i]->getPose().ominus(slam_nodes[j]->getPose());
            if(hypot(delta.x(), delta.y()) > 1.0)
                continue;

            if(slam_nodes[i]->region_node == slam_nodes[j]->region_node){
                same_region++; 
            }                  
            else{
                different_region++;
            }

            if(slam_nodes[i]->region_node == slam_nodes[j]->region_node){
                //same_region++; 
                total_count++;
            }  
            else{
                it_1_2 = topo_distance.find(make_pair(slam_nodes[i]->region_node->region_id, slam_nodes[j]->region_node->region_id));
                int distance = it_1_2->second;
                
                //fprintf(stderr, "Region : %d - %d => Distance : %d\n", slam_nodes[i]->region_node->region_id, slam_nodes[j]->region_node->region_id, distance); 
                if(distance > 1){
                    total_distance += (distance - 1);
                    if((distance - 1) > max_distance){
                        max_distance = distance - 1;
                    }
                    no_of_failed_edges++;
                }
                total_count++;
            }            
        }
    }    

    double average_distance = 0;
    if(total_count > 0){
        average_distance = total_distance / (double) total_count;
    }

    no_close_node_pairs = total_count;
    no_same_region_close_pairs = same_region;
    no_failed_close_node_pairs = no_of_failed_edges;
    max_dist_of_close_node_pairs = max_distance;
    average_distance_of_close_node_pairs = average_distance;

    fprintf(stderr, "===================Avg Topo Distance====================");
    fprintf(stderr, " No of Regions : %d - No of Nodes : %d No of close pairs %d\n", (int) region_node_list.size(), (int) slam_nodes.size(), total_count);
    fprintf(stderr, "No of Same Regions      : %d\n", same_region);
    fprintf(stderr, "No of Different Regions : %d\n", different_region);
    fprintf(stderr, " No of failed edges : %d - Max distance : %d\n", no_of_failed_edges, max_distance);
    fprintf(stderr, "Average topological distance of close node pairs : %.3f\n", average_distance);

    fprintf(stderr, "================================\n");
}

void SlamGraph::calculateRegionEfficiency(){
    fprintf(stderr, "===== Region Efficiency =====\n");
    
    int same_region = 0;
    int different_region = 0;
    
    for(int i=0; i < slam_nodes.size(); i++){
        for(int j=0; j < slam_nodes.size(); j++){
            if(i == j)
                continue; 
            
            Pose2d delta = slam_nodes[i]->getPose().ominus(slam_nodes[j]->getPose());
            if(hypot(delta.x(), delta.y()) > 1.0)
                continue;

            if(slam_nodes[i]->region_node == slam_nodes[j]->region_node){
                same_region++; 
            }                  
            else{
                different_region++;
            }
        }
    }    

    fprintf(stderr, " No of Regions : %d - No of Nodes : %d No of Edges : %d\n", (int) region_node_list.size(), (int) slam_nodes.size(), (int) slam_constraints.size());
        
    fprintf(stderr, "No of Same Regions      : %d\n", same_region);
    
    fprintf(stderr, "No of Different Regions : %d\n", different_region);

    fprintf(stderr, "================================\n");
}

void SlamGraph::updateRegionMeans(){
    //fprintf(stderr, "===== Region Connectivity =====\n");
    for(int i=0; i < region_node_list.size(); i++){
        RegionNode *region = region_node_list[i];
        region->updateMean();
    }
    //fprintf(stderr, "================================\n");
}

//return regions that have a likelihood of a single label greater than a given threshold 
//for now we just return regions that were directly labeled 
vector<RegionNode *> SlamGraph::getLabeledRegions(RegionNode *current_region, double threshold = 0.8){ 
    static char buf[1024];
    //skip the current region 
    vector<RegionNode *> labeled_regions; 

    sprintf(buf, "[Labeled Regions] [Checking Regions]");
    output_writer->write_to_buffer(buf);

    for(int i=0; i < region_node_list.size(); i++){
        RegionNode *region = region_node_list[i];
        if(region == current_region)
            continue;

        bool added = false;
        for(int j=0; j < region->nodes.size(); j++){
            SlamNode *node = region->nodes[j];
            
            if(node->language_observations.size() > 0){
                added = true;
                break;
            }
        }
        if(added){
            string max_label;
            double prob = 0; 
            bool found_max_label = region->getMaxLabelAndLikelihood(max_label, prob);
            sprintf(buf, "[Labeled Regions] [Checking Regions] Found region with language : %d - Label : %s (%.3f)", region->region_id, max_label.c_str(), prob);
            output_writer->write_to_buffer(buf);
            //ideally - it should add based on threshold
            labeled_regions.push_back(region);
        }    
    }

    return labeled_regions;
}

void SlamGraph::updateRegionDistances(){
    //ideally this can be done when a new edge is created between two regions 
    //then we wont need to go through each of these guys 
    //go through the contraint list 
    //and add edges for between each region with connected edges 
    
    //reset all the current edges
    //map<pair<int, int>, int> edge_weights;
    topo_distance.clear();
    for(int i=0; i < region_node_list.size(); i++){
        RegionNode *r1 = region_node_list[i];
        for(int j=0; j < region_node_list.size(); j++){
            RegionNode *r2 = region_node_list[j];
            if(r1->isConnected(r2)){
                topo_distance.insert(make_pair(make_pair(r1->region_id, r2->region_id), 1));
            }
            else{
                topo_distance.insert(make_pair(make_pair(r1->region_id, r2->region_id), 1000));
            }
        }
    }

    map<pair<int, int>, int>::iterator it_1_3;
    map<pair<int, int>, int>::iterator it_3_2;
    map<pair<int, int>, int>::iterator it_1_2;
    map<pair<int, int>, int>::iterator it_2_1;
        
    for(int k=0; k < region_node_list.size(); k++){
        RegionNode *r3 = region_node_list[k];
        for(int i=0; i < region_node_list.size(); i++){
            RegionNode *r1 = region_node_list[i];
            for(int j=0; j < region_node_list.size(); j++){
                RegionNode *r2 = region_node_list[j];
                it_1_3 = topo_distance.find(make_pair(r1->region_id, r3->region_id));
                int dist_1_3 = it_1_3->second;
                it_3_2 = topo_distance.find(make_pair(r2->region_id, r3->region_id));
                int dist_3_2 = it_3_2->second;
                it_1_2 = topo_distance.find(make_pair(r1->region_id, r2->region_id));
                int dist_1_2 = it_1_2->second;

                if((dist_1_3 + dist_3_2) < dist_1_2){
                    topo_distance.erase(it_1_2);
                    topo_distance.insert(make_pair(make_pair(r1->region_id, r2->region_id), dist_1_3 + dist_3_2));
                    it_2_1 = topo_distance.find(make_pair(r2->region_id, r1->region_id));
                    topo_distance.erase(it_2_1);
                    topo_distance.insert(make_pair(make_pair(r2->region_id, r1->region_id), dist_1_3 + dist_3_2));
                }
            }
        }
    }
}

void SlamGraph::updateRegionConnections(){
    //ideally this can be done when a new edge is created between two regions 
    //then we wont need to go through each of these guys 
    //go through the contraint list 
    //and add edges for between each region with connected edges 
    
    //reset all the current edges
    pruneRegionConnections();
    
    map<int, SlamConstraint *>::iterator c_it;
    for ( c_it= slam_constraints.begin() ; c_it != slam_constraints.end(); c_it++ ){        
        SlamConstraint *ct = c_it->second;
        RegionNode *region1 = ct->node1->region_node;
        RegionNode *region2 = ct->node2->region_node;

        //we need to look at the edge type also 
        //otherwise it can be a region to region constraint - but not a region connectivity 

        if(region1 == region2)
            continue;

        /*RegionEdge *edge1 = region1->getEdge(region2);
        RegionEdge *edge2 = region2->getEdge(region1);
        
        if(edge1 == NULL && edge2 == NULL){
            fprintf(stderr, "No edge exists - adding edge\n");
            //we need to add both regions - otherwise its harder to remove edges 
            region1->addEdge(region2);
            region2->addEdge(region1);
            continue;
            }*/
        region1->addEdge(region2, ct);
        region2->addEdge(region1, ct);
        //if we are here - this means that an edge existed already - so no worries 
    }    
    
    //printRegionConnections();
}

map<RegionNode*, RegionNode *> SlamGraph::getShortestPathRegionTree(RegionNode *node)
{
    graph_t g; 

    map<int, vertex_descriptor> vertex_map;
    map<int, vertex_descriptor>::iterator it_v;

    vertex_descriptor *v_node = NULL;

    for(int i=0; i < region_node_list.size(); i++){
        //for (int i=0; i < slam_node_list.size(); i++){
        vertex_descriptor v = add_vertex(g);
        RegionNode *region = region_node_list[i];
        g[v].id = region->region_id;
        if(region->region_id == node->region_id){
            v_node = &v;
        }
        vertex_map.insert(make_pair(g[v].id, v));
    }

    map<int, SlamConstraint *>::iterator it;
    set<pair<int, int> > valid_edges;
    set<pair<int, int> >::iterator it_edge_set;
    map<int, int>::iterator it_i;
    map<int, int>::iterator it_j;

    for(int i=0; i < region_node_list.size(); i++){
         RegionNode *region1 = region_node_list[i];
         vector<RegionNode *> connected_regions = region1->getConnectedRegions();
         for(int j=0; j < connected_regions.size(); j++){
             RegionNode *region2 = connected_regions[j];
             
             it_edge_set = valid_edges.find(make_pair(region1->region_id, region2->region_id));
             if(it_edge_set != valid_edges.end())
                 continue;
             it_edge_set = valid_edges.find(make_pair(region2->region_id, region1->region_id));
             if(it_edge_set != valid_edges.end())
                 continue;
             valid_edges.insert(make_pair(region1->region_id, region2->region_id));
         }
    }
    
    for(it_edge_set = valid_edges.begin(); it_edge_set != valid_edges.end(); it_edge_set++){
        nodePairKey edge = *it_edge_set; 
        it_v = vertex_map.find(edge.first);
        if(it_v == vertex_map.end()){
            //fprintf(stderr, "Error - No vertex descriptor found for node : %d\n", it_i->first);
            continue;
        }

        vertex_descriptor v_i = it_v->second;
        it_v = vertex_map.find(edge.second);
        if(it_v == vertex_map.end()){
            //fprintf(stderr, "Error - No vertex descriptor found for node : %d\n", it_i->second);
            continue;
        }
        vertex_descriptor v_j = it_v->second;
        add_edge(v_i, v_j, Weight(1.0), g);
    }

    property_map<graph_t, edge_weight_t>::type weightmap = get(edge_weight, g);
    std::vector<vertex_descriptor> p(num_vertices(g));
    std::vector<double> d(num_vertices(g));
    
    //vertex_descriptor b_vertex;
    
    dijkstra_shortest_paths(g, *v_node,
                            predecessor_map(boost::make_iterator_property_map(p.begin(), 
                                                                              get(boost::vertex_index, g))).
                            distance_map(boost::make_iterator_property_map(d.begin(), get(boost::vertex_index, g))));
        
    int64_t e_utime = bot_timestamp_now();
    //fprintf(stderr, CYAN "Time to find Shortest Path : %f\n" RESET_COLOR, (e_utime - s_utime) / 1.0e6);
    //fprintf(stderr, CYAN "Distances from %d and parents\n" RESET_COLOR, node->id);
    graph_traits < graph_t >::vertex_iterator vi, vend;
    //from this we should build the minimum spanning tree for the graph - from the last node 

    map<RegionNode*, RegionNode*> parent_map; 
    set<RegionNode *> node_set;
    map<RegionNode*, set<RegionNode *> > neighbour_map; 

    map<RegionNode*, set<RegionNode *> >::iterator it_neighbour_map;
    
    //this is not building a minimum spanning tree right now 
    //this just tells which edges are there (which is effected by the way its included) 
    
    for (boost::tie(vi, vend) = vertices(g); vi != vend; ++vi) {        
        RegionNode *nd_i = getRegionFromID(g[*vi].id);
        RegionNode *nd_j = getRegionFromID(g[p[*vi]].id);

        parent_map.insert(make_pair(nd_i, nd_j));
    }
    return parent_map;    
}

map<RegionNode*, int> SlamGraph::getRegionDistance(RegionNode *region, vector<RegionNode *> c_region_list, int max_dist){
    map<RegionNode*, int> result;
    int64_t utime_s = bot_timestamp_now();
    
    map<RegionNode*, RegionNode *> region_result = getShortestPathRegionTree(region);
    
    int64_t utime_e = bot_timestamp_now();
    
    fprintf(stderr, YELLOW "Time to build graph : %f\n" RESET_COLOR, (utime_e - utime_s) /1.0e6);
    
    map<RegionNode*, RegionNode *>::iterator it;

    fprintf(stderr, "Current region : %d\n", (int) region->region_id);

    //for(it = region_result.begin(); it != region_result.end(); it++){
        //fprintf(stderr, "Region : %d -> %d\n", it->first->region_id, it->second->region_id);
    //}    
    
    for(int i=0; i < c_region_list.size(); i++){
        int count = 0;
        RegionNode *region2 = c_region_list[i];
        RegionNode *last_region = region2;

        fprintf(stderr, "Looking for path to region : %d\n", (int) region2->region_id);

        it = region_result.find(last_region);
        while(it != region_result.end()){
            count++;
            if(last_region == it->second){
                fprintf(stderr, "Next is same as current - setting high\n");
                count += max_dist;
                break;
            }
            last_region = it->second;
            //fprintf(stderr, "\tR : %d\n", last_region->region_id);
            if(last_region == region){
                break;
            }
            it = region_result.find(last_region);
            if(count > max_dist){
                break; 
            }
        }        
        fprintf(stderr, "Distance from region %d - Region : %d = %d\n", region->region_id, region2->region_id, count);
        result.insert(make_pair(region2,count));
    }

    return result;
}

void SlamGraph::removeComplexLanguageNodePaths(RegionNode *region){
    map<int, ComplexLanguageEvent *>::iterator it_lang; 
    for(it_lang = complex_language_events.begin(); it_lang != complex_language_events.end(); it_lang++){
        ComplexLanguageEvent *current_event = it_lang->second;
        current_event->removeNodePath(region);
        current_event->removeLandmark(region);
    }
}

void SlamGraph::removeRegion(RegionNode *r_node){
    //fprintf(stderr, "==== Removing Region %d ====\n", r_node->region_id);
    region_nodes.erase(r_node->region_id);
    
    /*fprintf(stderr, CYAN "Regions before removal : %d\n" RESET_COLOR, region_node_list.size());
    for(size_t i=0; i < region_node_list.size(); i++){
        fprintf(stderr, CYAN "Region : %d - %d\n", region_node_list[i]->region_id, (int) region_node_list[i]->nodes.size());
        }*/

    for(size_t i=0; i < region_node_list.size(); i++){
        if(region_node_list[i]->region_id == r_node->region_id){
            //fprintf(stderr, "Found Region - removing from list\n");
            region_node_list.erase(region_node_list.begin()+i);
            break;
        }
    }

    /*fprintf(stderr, CYAN "Regions after removal : %d\n" RESET_COLOR, region_node_list.size());
    for(size_t i=0; i < region_node_list.size(); i++){
        fprintf(stderr, CYAN "Region : %d\n", region_node_list[i]->region_id);
        }*/

    map<int, RegionEdge *>::iterator it;
    for(it = r_node->edges.begin(); it!= r_node->edges.end(); it++){
        //remove the edges from both regions
        RegionNode *region_2 = it->second->node2;//getRegionFromID(it->second->node2->region_id);//it->second->node2;     

        if(region_2 != NULL){
            //fprintf(stderr, CYAN "Removing region edge with Region : %d\n" RESET_COLOR, region_2->region_id);
            region_2->deleteEdge(r_node);
        }
        delete it->second;        
    }
    
    removeComplexLanguageNodePaths(r_node);

    //remove the edges here     
    delete r_node;
}

int SlamGraph::addNodeToRegion(SlamNode *node, int id){
    RegionNode *region = getRegionFromID(id);
    if(region == NULL){
        fprintf(stderr, "Error : Specified region not found\n");
        return -1;
    }
    return region->addNode(node);
}

int SlamGraph::addNodeToRegion(SlamNode *node, RegionNode *region){
    if(region == NULL){
        fprintf(stderr, "Error : Specified region not found\n");
        return -1;
    }
    return region->addNode(node);
}

RegionNode* SlamGraph::mergeRegions(RegionNode *region1, RegionNode *region2){
    //printRegionConnections();
    //merging will remove region2 
    RegionNode *surviving_region = NULL;
    RegionNode *removed_region = NULL;
    if(region1->region_id < region2->region_id){
        surviving_region = region1;
        removed_region = region2;
    }
    else{
        surviving_region = region2;
        removed_region = region1;
    }
    
    removed_region->addCurrentNodesToOtherRegion(surviving_region);
    
    //removed_region->printRegionEdges();
    //we could transfer the regions out also 
    //fprintf(stderr, "Merge regions %d - %d\n", region1->region_id, region2->region_id);
    removeRegion(removed_region);
    //printRegionConnections();
    updateRegionConnections();
    return surviving_region;
}

RegionNode * SlamGraph::getRegionFromID(int region_id){    
    map<int, RegionNode *>::iterator it;
    it = region_nodes.find(region_id);
    if(it == region_nodes.end()){
        return NULL;
    }
    return it->second;
}

RegionSegment *SlamGraph::createNewSegment(){
    RegionSegment *segment = new RegionSegment(last_segment_id++);
    region_segment_list.push_back(segment);
    region_segment_map.insert(make_pair(segment->id, segment));
    last_segment = segment;
    return segment;
}

RegionSegment *SlamGraph::createNewSegment(vector<SlamNode*> nodes){
    RegionSegment *segment = new RegionSegment(last_segment_id++);
    segment->addNodes(nodes);
    region_segment_list.push_back(segment);
    region_segment_map.insert(make_pair(segment->id, segment));
    last_segment = segment;
    return segment;
}


SlamNode *SlamGraph::getClosestDistanceToRegion(Pose2d pose, RegionNode *current_region, double *distance){
    //search through the nodes in the graph and find the closest node from the given location to the 
    //region - make sure that the position is not from a node in the region - as otherwise it will be 0
    //also return the closest node 
    //negative distance indicates failure (also the closest node will be null

    double min_dist = 10000000;

    SlamNode *res_node = NULL;

    for(int i=0; i < region_node_list.size(); i++){
        RegionNode *region = region_node_list[i];
        if(region == current_region){
            //the current region - we skip this 
            continue;
        }
        
        for(int j=0; j < region->nodes.size(); j++){
            SlamNode *c_node = region->nodes[j];
            Pose2d delta = c_node->getPose().ominus(pose);
            double dist = hypot(delta.x(), delta.y());

            if(dist < min_dist){
                min_dist = dist;
                //closest_node = c_node;
                res_node = c_node;
            }
        }
    }
    *distance = min_dist;
    return res_node;
}

void SlamGraph::getDistanceToNodesInRegion(Pose2d pose, RegionNode *region, 
                                           std::vector<nodeDist> *region_dist_list){
    //search through the nodes in the graph and find the closest node from the given location to the 
    //region - make sure that the position is not from a node in the region - as otherwise it will be 0
    //also return the closest node 
    //negative distance indicates failure (also the closest node will be null

    for(int j=0; j < region->nodes.size(); j++){
        SlamNode *c_node = region->nodes[j];
        Pose2d delta = c_node->getPose().ominus(pose);
        double dist = hypot(delta.x(), delta.y());
        
        region_dist_list->push_back(make_pair(c_node, dist));
    }
}

void SlamGraph::getDistanceToRegions(Pose2d pose, RegionNode *current_region, 
                                          std::vector<nodeDist> *region_dist_list){
    //search through the nodes in the graph and find the closest node from the given location to the 
    //region - make sure that the position is not from a node in the region - as otherwise it will be 0
    //also return the closest node 
    //negative distance indicates failure (also the closest node will be null

    for(int i=0; i < region_node_list.size(); i++){
        RegionNode *region = region_node_list[i];
        if(region == current_region){
            //the current region - we skip this 
            continue;
        }
        
        double min_dist = 100000000;
        SlamNode *res_node = region->getClosestNode(pose, &min_dist);
        if(min_dist < 1000 && res_node != NULL){
            region_dist_list->push_back(make_pair(res_node, min_dist));
        }
    }
}

double SlamGraph::getClosestDistanceToRegion(SlamNode *node_to_check, RegionNode *region, 
                                               SlamNode *closestNode){            
    double min_dist = 100000000;
    closestNode = region->getClosestNode(node_to_check->getPose(), &min_dist);
    return min_dist;
}

double SlamGraph::findClosestRegion(RegionNode *querry_region, RegionNode *closest_region, SlamNode *node1, SlamNode *node2){
    //find the cloest region to the given region 
    //also return the closest region and the nodes that were closest 
    double min_dist = 10000000; 
    node1 = NULL;
    node2 = NULL;
    closest_region = NULL;
    for(int i=0; i < region_node_list.size(); i++){
        RegionNode *region = region_node_list[i];
        //skip if its the querry region 
        if(region == querry_region)
            continue;
        SlamNode *nd1, *nd2;
        double dist = querry_region->getClosestDistanceToRegion(region, nd1, nd2);
        if(dist < min_dist){
            min_dist = dist;
            node1 = nd1;
            node2 = nd2;
            closest_region = region;
        }
    }
    return min_dist;
}

double SlamGraph::findDistanceToRegionMeans(RegionNode *querry_region, std::vector<nodePairDist> *region_dist_list, double add_threshold){
    int64_t s_utime = bot_timestamp_now();
    //find the cloest region to the given region 
    //also return the closest region and the nodes that were closest 
    
    //querry_region->updateMean();
    
    double min_dist = add_threshold; //10000000; 

    double mean_utime = 0;

    for(int i=0; i < region_node_list.size(); i++){
        RegionNode *region = region_node_list[i];
        //skip if its the querry region 
        if(region == querry_region)
            continue;
        int nd1_id, nd2_id;
        
        //region->updateMean();

        vector<pair <SlamNode *, SlamNode *> > close_node_pairs; 
        //this is very expensive - and stupid - we should find the actual closest nodes only once we figured out the 
        //closest pairs 
        int64_t utime_s = bot_timestamp_now();
        double dist = querry_region->getMeanDistanceToRegion(region, close_node_pairs, &nd1_id, &nd2_id);               
        int64_t utime_e = bot_timestamp_now();

        mean_utime += (utime_e - utime_s) / 1.0e6;

        
        /*SlamNode *q_mean_node = querry_region->mean_node;
        SlamNode *r_mean_node = region->mean_node;
              
        Pose2d delta = q_mean_node->getPose().ominus(r_mean_node->getPose());

        double dist = hypot(delta.x(), delta.y()); */

        if(dist > add_threshold)
            continue;
        //double mean_dist = hypot(querry_region->mean.x() - region->mean.x(), querry_region->mean.y() - region->mean.y());

        SlamNode *nd1 = getSlamNodeFromID(nd1_id);
        SlamNode *nd2 = getSlamNodeFromID(nd2_id);

        region_dist_list->push_back(make_pair(make_pair(nd1,nd2), dist));
        
        if(dist < min_dist){
            min_dist = dist;
        }
    }
    
    int64_t e_utime = bot_timestamp_now();
    char buf[1024];
    sprintf(buf, "[Slam Graph] [Distance Region Mean] [Performance] %f - Dist Cal : %f", (e_utime - s_utime) / 1.0e6, mean_utime);
    output_writer->write_to_buffer(buf);

    return min_dist;
}

double SlamGraph::findMeanDistanceToRegions(RegionNode *querry_region, std::vector<nodePairDist> *region_dist_list){
    int64_t s_utime = bot_timestamp_now();
    //find the cloest region to the given region 
    //also return the closest region and the nodes that were closest 
    double min_dist = 10000000; 
    for(int i=0; i < region_node_list.size(); i++){
        RegionNode *region = region_node_list[i];
        //skip if its the querry region 
        if(region == querry_region)
            continue;
        int nd1_id, nd2_id;
        
        vector<pair <SlamNode *, SlamNode *> > close_node_pairs; 
        double dist = querry_region->getMeanDistanceToRegion(region, close_node_pairs, &nd1_id, &nd2_id);
        
        //if(0 && dist < 15){
        if(0 && dist < 15){
            for(int i=0; i < close_node_pairs.size(); i++){
                //this could be cached once??
                MatrixXd cov = getCovariances(close_node_pairs[i].first, close_node_pairs[i].second);
                //cout << cov << endl;
                //sometimes getting a segfault - need to figure out why - but doesnt seem to take a long time 
                fprintf(stderr, "Covariance between %d - %d\n", close_node_pairs[i].first->id, close_node_pairs[i].second->id);
            }
        }

        SlamNode *nd1 = getSlamNodeFromID(nd1_id);
        SlamNode *nd2 = getSlamNodeFromID(nd2_id);
        //fprintf(stderr, "Node %d (%d) - %d (%d) - %f\n", nd1->id, nd2->id, nd1->region_node->region_id, nd2->region_node->region_id, dist);
        region_dist_list->push_back(make_pair(make_pair(nd1,nd2), dist));
        
        if(dist < min_dist){
            min_dist = dist;
        }
    }
    int64_t e_utime = bot_timestamp_now();
    //fprintf(stderr, RED "\n\n\nTime for cov : %f\n\n\n" RESET_COLOR, (e_utime - s_utime) / 1.0e6);
    return min_dist;
}

/*double SlamGraph::findDistanceToRegions(vector<SlamNode *> segment_nodes, std::vector<nodePairDist> &region_dist_list, double threshold){
    for(int i=0; i < region_node_list.size(); i++){
        RegionNode *region = region_node_list[i];
        if(region == q_segment
        double min_dist = threshold; 
        SlamNode *m_nd1 = NULL;
        SlamNode *m_nd2 = NULL;
        for(int j=0; j < q_segment->nodes.size(); j++){
            double dist = 0;
            SlamNode *nd1 = q_segment->nodes[j];
            SlamNode *nd2 = c_segment->getClosestNode(nd1, &dist);
            if(dist < min_dist){
                m_nd1 = nd1;
                m_nd2 = nd2;
                min_dist = dist;
            }            
        }
        if(min_dist < threshold){
            segment_dist_list.push_back(make_pair(make_pair(m_nd1,m_nd2), min_dist));
        }
        
    }    
    }*/

double SlamGraph::findDistanceToSegments(RegionSegment *q_segment, std::vector<nodePairDist> &segment_dist_list, double threshold){
    for(int i=0; i < region_segment_list.size(); i++){
        RegionSegment *c_segment = region_segment_list[i];
        if(c_segment == q_segment)
            continue;
        double min_dist = threshold; 
        SlamNode *m_nd1 = NULL;
        SlamNode *m_nd2 = NULL;
        for(int j=0; j < q_segment->nodes.size(); j++){
            double dist = 0;
            SlamNode *nd1 = q_segment->nodes[j];
            SlamNode *nd2 = c_segment->getClosestNode(nd1, &dist);
            if(dist < min_dist){
                m_nd1 = nd1;
                m_nd2 = nd2;
                min_dist = dist;
            }            
        }
        if(min_dist < threshold){
            segment_dist_list.push_back(make_pair(make_pair(m_nd1,m_nd2), min_dist));
        }
        
    }    
}

double SlamGraph::findDistanceToRegions(SlamNode *querry_node, std::vector<nodePairDist> *region_dist_list){
    double min_dist = 10000000; 
    for(int i=0; i < region_node_list.size(); i++){
        RegionNode *region = region_node_list[i];
        //skip if its the querry region 
        if(region == querry_node->region_node)
            continue;
        int c_nd_id;
        
        double dist;// = region->getMeanDistanceFromNode(querry_node, &c_nd_id);
        Pose2d current_pose = querry_node->getPose();
        SlamNode *nd2 = region->getClosestNode(current_pose, &dist);//getSlamNodeFromID(c_nd_id);

        region_dist_list->push_back(make_pair(make_pair(querry_node,nd2), dist));
        
        if(dist < min_dist){
            min_dist = dist;
        }
    }
    return min_dist;
}

double SlamGraph::findMeanDistanceToRegions(SlamNode *querry_node, std::vector<nodePairDist> *region_dist_list){
    //find the cloest region to the given region 
    //also return the closest region and the nodes that were closest 
    double min_dist = 10000000; 
    for(int i=0; i < region_node_list.size(); i++){
        RegionNode *region = region_node_list[i];
        //skip if its the querry region 
        if(region == querry_node->region_node)
            continue;
        int c_nd_id;
        double dist = region->getMeanDistanceFromNode(querry_node, &c_nd_id);

        SlamNode *nd2 = getSlamNodeFromID(c_nd_id);

        region_dist_list->push_back(make_pair(make_pair(querry_node,nd2), dist));
        
        if(dist < min_dist){
            min_dist = dist;
        }
    }
    return min_dist;
}

double SlamGraph::findDistanceToRegions(RegionNode *querry_region, std::vector<nodePairDist> *region_dist_list){
    //find the cloest region to the given region 
    //also return the closest region and the nodes that were closest 
    double min_dist = 10000000; 
    for(int i=0; i < region_node_list.size(); i++){
        RegionNode *region = region_node_list[i];
        //skip if its the querry region 
        if(region == querry_region)
            continue;
        int nd1_id, nd2_id;
        double dist = querry_region->getClosestDistanceToRegion(region, &nd1_id, &nd2_id);
        

        /*SlamNode *nd1, *nd2;
        double dist = querry_region->getClosestDistanceToRegion(region, nd1, nd2);
        
        fprintf(stderr, "Dist : %f\n", dist);
        fprintf(stderr, "Node %d (%d) - %d (%d) - %f\n", nd1->id, nd2->id, nd1->region_node->region_id, nd2->region_node->region_id, 
        dist);*/
        SlamNode *nd1 = getSlamNodeFromID(nd1_id);
        SlamNode *nd2 = getSlamNodeFromID(nd2_id);
        //fprintf(stderr, "Node %d (%d) - %d (%d) - %f\n", nd1->id, nd2->id, nd1->region_node->region_id, nd2->region_node->region_id, dist);
        region_dist_list->push_back(make_pair(make_pair(nd1,nd2), dist));
        
        if(dist < min_dist){
            min_dist = dist;
        }
    }
    return min_dist;
}



int SlamGraph::removeSlamNode(int node_id)
{
    //slam->add_node(node->pose2d_node);
    SlamNode *node = getSlamNodeFromID(node_id);
    if(node == NULL)
        return -1;
    
    slam_nodes.erase(node_id);
    slam->remove_node(node->pose2d_node);

    for(int i = 0; i< slam_node_list.size(); i++){
        if(slam_node_list[i]->id == node_id){
            slam_node_list.erase(slam_node_list.begin()+ i);

            //update the rest of the nodes with the new position
            if(i < slam_node_list.size()){ //if the removed node wasn't the last node
                for(int j=i; j < slam_node_list.size(); j++){
                    slam_node_list[i]->position = j;
                }
            }

            break;
        }
    }

    //remove the node from the node map 
    //what do we do with the constraints ?? - maybe whatever calls this should handle that
    SlamNode *p_node = node->prev_node;
    SlamNode *n_node = node->next_node;

    int reassign = 0;
    SlamConstraint *const_p_to_c  = node->inc_constraint;
    SlamConstraint *const_c_to_n = NULL;

    Pose2d orig_transform = const_p_to_c->transform;
    MatrixXd orig_noise = const_p_to_c->noise.sqrtinf().inverse();

    Pose2d new_transform;
    Pose2d new_transform_inv;
    MatrixXd new_noise;

    //cout << orig_noise << endl;
    //Matrix3d orig_noise_mat; 
    /*orig_noise_mat<< orig_noise[0] , orig_noise[1], orig_noise[2], 
        orig_noise[3] , orig_noise[4], orig_noise[5], 
        orig_noise[6] , orig_noise[7], orig_noise[8];*/
        

    if(n_node != NULL){
        const_c_to_n = n_node->inc_constraint;
        new_transform = const_c_to_n->transform;
        new_noise = const_c_to_n->noise.sqrtinf().inverse();
        Pose2d center(0,0,0);
        new_transform_inv = center.ominus(new_transform);
        reassign = 1;        
    }
    else{
        fprintf(stderr, "No next node found - ignoring incremental constraint\n");
    }

    //need to fix both constraints coming in to the slam node 
    //and constraints going out from the slam node 
    map<int, SlamConstraint *>::iterator it; 
    for (it= node->constraints_from.begin(); it!= node->constraints_from.end(); ++it){
        SlamConstraint *constraint = it->second;
        if(reassign){
            //transform the constraints to the new node and then add them to that node 
            //also remove and add a new inc constraint for the new node - coming from the previous node
            Pose2d tf = constraint->transform;
            //Noise new_noise = constraint->noise;
            MatrixXd ns = constraint->noise.sqrtinf().inverse();
            //Pose2d new_transform = orig_transform.oplus(new_to_orig);
            Pose2d updated_tf = tf.oplus(new_transform);
            
            Matrix3d J;

            double c = cos(tf.t());
            double s = sin(tf.t());
            J << c , -s , 0, 
                c , s, 0, 
                0 , 0, 1; 

            MatrixXd updated_noise_trans = ns + J * new_noise * J.transpose();
            //Noise *new_noise_n = new Covariance(new_noise_trans);
            Covariance updated_noise_n(updated_noise_trans);
            addConstraint(n_node, constraint->node2, n_node, constraint->node2, updated_tf,
                          updated_noise_n, constraint->hitpct, constraint->type, constraint->status);
            //MatrixXd new_noise_trans = orig_noise; //_mat; 
        }
        removeConstraint(constraint);
    }

    for (it= node->constraints.begin(); it!= node->constraints.end(); ++it){
        SlamConstraint *constraint = it->second;
        if(reassign){
            //transform the constraints to the new node and then add them to that node 
            //also remove and add a new inc constraint for the new node - coming from the previous node
            
            //this needs to be inverted 

            Pose2d tf = constraint->transform;
            //Noise new_noise = constraint->noise;
            MatrixXd ns = constraint->noise.sqrtinf().inverse();
            Pose2d updated_tf = new_transform_inv.oplus(tf);
            
            Matrix3d J;

            double c = cos(new_transform_inv.t());
            double s = sin(new_transform_inv.t());
            J << c , -s , 0, 
                c , s, 0, 
                0 , 0, 1; 

            MatrixXd updated_noise_trans = new_noise + J * ns * J.transpose();
            //Noise *new_noise_n = new Covariance(new_noise_trans);
            Covariance updated_noise_n(updated_noise_trans);
            addConstraint(constraint->node1, n_node, constraint->node1, n_node, updated_tf, 
                          updated_noise_n, constraint->hitpct, constraint->type, constraint->status);
        }
        removeConstraint(constraint);
    }
    fprintf(stderr, "Removing Node and constraint - someone should be adding a new one between the previous and next\n");
    delete node;

    return 0;
    //the constraints should be added to the node - then its cleaner 
}

int SlamGraph::removeSlamNodeBasic(int node_id)
{
    //slam->add_node(node->pose2d_node);
    SlamNode *node = getSlamNodeFromID(node_id);
    if(node == NULL)
        return -1;
    
    slam_nodes.erase(node_id);
    slam->remove_node(node->pose2d_node);
    for(int i = 0; i< slam_node_list.size(); i++){
        if(slam_node_list[i]->id == node_id){
            slam_node_list.erase(slam_node_list.begin()+i);
            
            //update the rest of the nodes with the new position
            if(i < slam_node_list.size()){ //if the removed node wasn't the last node
                for(int j=i; j < slam_node_list.size(); j++){
                    slam_node_list[i]->position = j;
                }
            }

            break;
        }
       
    }
    //remove the node from the node map 
    //what do we do with the constraints ?? - maybe whatever calls this should handle that
    //this will mess up the other guy's inc constraint

    map<int, SlamConstraint *>::iterator it; 
    for (it= node->constraints.begin(); it!= node->constraints.end(); ++it){
        SlamConstraint *constraint = it->second;
        removeConstraint(constraint);
    }
    fprintf(stderr, "Removing Node and constraint - someone should be adding a new one between the previous and next\n");
    delete node;

    return 0;
    //the constraints should be added to the node - then its cleaner 
}

SlamNode * SlamGraph::addSlamNode(NodeScan *pose)
{
    //slam_nodes.push_back(node);
    //slam_nodes.insert(node);
    SlamNode *node = new SlamNode(pose, label_info, b_graph);
    char buf[1024];
    //update the nodes so that they point to the previous and next nodes 
    node->prev_node = last_added_node;
    if(last_added_node != NULL){
        last_added_node->next_node = node;        
        sprintf(buf, "[Shortest Path] [Parent Map] : Appending to parent map %d - %d", last_added_node->id, node->id);
        output_writer->write_to_buffer(buf); 
        
#ifndef REBUILD_BOOST_GRAPH
        map<SlamNode * , SlamNode *>::iterator it = parent_map_current.find(last_added_node);
        if(it!= parent_map_current.end()){
            parent_map_current.erase(it);
        }
        parent_map_current.insert(make_pair(last_added_node, node));
        //add the current node as it's parent - so that if we querry distance to itself - we will not get an error
        parent_map_current.insert(make_pair(node, node));
#endif
    }   
    last_added_node = node;

    //add this to the parent map 
    

    slam_nodes.insert ( pair<int, SlamNode *>(node->id,node));
    slam_node_list.push_back(node);
    node->position = slam_node_list.size()-1;
    //also add node to slam //and maybe constraints 
    slam->add_node(node->pose2d_node);
    return node;
    //the constraints should be added to the node - then its cleaner 
}



SlamNode * SlamGraph::addSlamNode(SlamNode *_node){
    SlamNode *node = new SlamNode(_node, b_graph);//, label_info);
    node->prev_node = last_added_node;
    if(last_added_node != NULL){
        last_added_node->next_node = node;
    }
    last_added_node = node;
    slam_nodes.insert ( pair<int, SlamNode *>(node->id,node));
    slam_node_list.push_back(node);
    node->position = slam_node_list.size()-1;
    //also add node to slam //and maybe constraints 
    slam->add_node(node->pose2d_node);
    return node;
    //the constraints should be added to the node - then its cleaner 
 }
 
 MatrixXd SlamGraph::getCovariance(SlamNode *node){
    MatrixXd cov(3,3);
    for(int i=0; i < 9; i++){
        cov(i) = node->cov[i];
    }
    return cov; 
}

//this is expensive - dont call too often 
map<int,MatrixXd> SlamGraph::getCovariances(SlamNode *nd1, vector<SlamNode *> nd_list){
    const Covariances& covariances =  slam->covariances();

    Covariances::node_lists_t cv_node_lists;
    
    //fprintf(stderr, "List Size : %d\n", nd_list.size());

    list<Node*> nd_list_req;
    for(int i=0; i < nd_list.size(); i++){
        //Covariances::node_lists_t nd_cov_list;
        nd_list_req.push_back(nd1->pose2d_node);
        SlamNode *nd2 = nd_list.at(i);
        nd_list_req.push_back(nd2->pose2d_node);
        cv_node_lists.push_back(nd_list_req);
        nd_list_req.clear();
    }

    list<MatrixXd> cov_blocks = covariances.marginal(cv_node_lists);

    map<int,MatrixXd> cov_res;
    int nd_ind = 0;

    //fprintf(stderr, " Cov Size : %d - List Size : %d\n",   cov_blocks.size(), nd_list.size());
    for (list<MatrixXd>::iterator it = cov_blocks.begin(); it!=cov_blocks.end(); it++, nd_ind++) {
        SlamNode *node = nd_list.at(nd_ind);
        MatrixXd cv = *it;
        cov_res.insert(pair<int, MatrixXd>(node->id, cv));
    }
    //int64_t p_start = bot_timestamp_now();
    return cov_res;
}

//this is expensive - dont call too often 
MatrixXd SlamGraph::getCovariances(SlamNode *node1, SlamNode *node2){
    const Covariances& covariances =  slam->covariances();
    list<Node*> nd_list;
    //Covariances::node_lists_t nd_cov_list;
    nd_list.push_back(node1->pose2d_node);
    nd_list.push_back(node2->pose2d_node);
    //nd_cov_list.push_back(nd_list);
    
    //int64_t p_start = bot_timestamp_now();
    return covariances.marginal(nd_list);
}

//measurement probability calculation given the slam covariance between two nodes
//the means of the two nodes and the sm constraint

//hmm - this seems wrong - we should prob integrate this somehow 

double SlamGraph::calculateProbability(SlamNode *node1, SlamNode *node2, Matrix3d cov_sm, Pose2d sm_constraint){
    char buf[1024];
    
    MatrixXd cov = getCovariances(node2, node1);
    Pose2d value1 = node1->getPose();
    Pose2d value2 = node2->getPose();
    double x1 = value2.x(), y1 = value2.y(), t1 = value2.t();
    double x2 = value1.x(), y2 = value1.y(), t2 = value1.t();
    double c1 = cos(t1), s1 = sin(t1);
    
    //Jacobian 
    MatrixXd H(2,6); 
    int v = 0;
    
    sprintf(buf, "[Constraint Likelihood] Constraint between %d - %d", node1->region_node->region_id, node2->region_node->region_id);
    output_writer->write_to_buffer(buf);

    //jacobian is correct 
    H.setZero();
    H(0,0) = c1; H(0,1) = s1; H(0,2) = 0;
    H(0,3) = -c1; H(0,4) = -s1; H(0,5) = -(x2-x1)*s1 + (y2-y1)*c1;
    H(1,0) = -s1; H(1,1) = c1; H(1,2) = 0;
    H(1,3) = s1; H(1,4) = -c1; H(1,5) = -(x2-x1) * c1 - (y2-y1) *s1;
    
    double mean_x = (x2-x1)*c1 + (y2-y1)*s1;
    double mean_y = -(x2-x1)*s1 + (y2-y1)*c1;
    
    if(v){
        fprintf(stderr, "Mean from graph : %f,%f => Mean from scanmatch : %f,%f\n", 
                mean_x, mean_y, sm_constraint.x(), 
                sm_constraint.y()
                );

        cout << "H : " << endl << H << endl;
        cout << "Joint Covariance : " << endl << cov << endl;
    }
    //only part of the sm cov should taken??
    Matrix2d result = H * cov * H.transpose();// + cov_sm;
    if(v){
        cout << "Result  : " << endl << result << endl;
    }
    
    //joing cov is not 3x3 - its 6x6
    sprintf(buf, "[Constraint Likelihood] Joint Cov : %f, %f %f", cov(0,0), cov(0,1), cov(0,2));
    output_writer->write_to_buffer(buf);

    sprintf(buf, "[Constraint Likelihood] Joint Cov : %f, %f %f", cov(1,0), cov(1,1), cov(1,2));
    output_writer->write_to_buffer(buf);
    
    sprintf(buf, "[Constraint Likelihood] Joint Cov : %f, %f %f", cov(2,0), cov(2,1), cov(2,2));
    output_writer->write_to_buffer(buf);

    sprintf(buf, "[Constraint Likelihood] Result Cov : %f, %f", result(0,0), result(0,1));
    output_writer->write_to_buffer(buf);

    sprintf(buf, "[Constraint Likelihood] Result Cov : %f, %f", result(1,0), result(1,1));
    output_writer->write_to_buffer(buf);

    MatrixXd dx(2,1);
    
    //mean for the new random variable 
    dx(0) = (mean_x - sm_constraint.x());
    dx(1) = (mean_y - sm_constraint.y());

    double l = hypot(dx(0), dx(1));

    sprintf(buf, "[Constraint Likelihood] Mean XYT : %f, %f", mean_x, mean_y);
    output_writer->write_to_buffer(buf);

    sprintf(buf, "[Constraint Likelihood] SM Const : %f, %f", sm_constraint.x(), sm_constraint.y());
    output_writer->write_to_buffer(buf);

    if(v)
        cout << "Distance : " << dx<<endl; 
    MatrixXd mh_dist = dx.transpose() * result.inverse() * dx;
    if(v)
        cout << "Mahalanobis Distance : " << mh_dist<<endl;
    double deter = result.determinant();
    if(v)
        cout << "Determinanant : " << deter << endl;

    sprintf(buf, "[Constraint Likelihood] Distance : %f, %f -> %f", dx(0), dx(1), hypot(dx(0), dx(1)));
    output_writer->write_to_buffer(buf);
    
    
    double prob = 0.0;
    if(l< 0.001){
        prob = 1.0;
        sprintf(buf, "[Constraint Likelihood] Mean delta: %f -> Mean is zero (Cov ill defined) -> Prob : %f", l, prob);
        output_writer->write_to_buffer(buf);
    }
    else{
        //convert to the distant of the constraint and then a likelihood of this edge 
        MatrixXd H1(1,4); 
        //jacobian is correct 
        H1.setZero();
        H1(0,0) = (mean_x - sm_constraint.x())/ l  ; H1(0,1) = (mean_y - sm_constraint.y())/ l; H1(0,2) = -(mean_x - sm_constraint.x()) / l; H1(0,3) = -(mean_y - sm_constraint.y()) / l;

        MatrixXd result_cov(4,4); 
        result_cov.setZero();
        
        for(int i=0; i < 2; i++){
            for(int j=0; j < 2; j++){
                result_cov(i,j) = result(i,j);
            }
        }

        for(int i=0; i < 2; i++){
            for(int j=0; j < 2; j++){
                result_cov(2+i,2+j) = cov_sm(i,j);
            }
        }

        sprintf(buf, "[Constraint Likelihood] H1 %f,%f,%f,%f", H1(0,0), H1(0,1), H1(0,2), H1(0,3));
        output_writer->write_to_buffer(buf);

        sprintf(buf, "[Constraint Likelihood] R_cov %f,%f,%f,%f - %f,%f,%f,%f", result_cov(0,0), result_cov(0,1), result_cov(1,0), result_cov(1,1), result_cov(2,2), result_cov(2,3), result_cov(3,2), result_cov(3,3));
        output_writer->write_to_buffer(buf);

        MatrixXd cov_n = H1 * result_cov * H1.transpose();

        if(v){
            cout << "New H " << H1 << endl;
            cout << "New Cov : " << cov_n << endl; 
        }

        double cov_val = cov_n(0);

        prob = get_edge_likelihood(l, cov_val);
        
        sprintf(buf, "[Constraint Likelihood] Mean delta: %f -> Cov : %f -> Prob : %f", l, cov_val, prob);
        output_writer->write_to_buffer(buf);
    }

    sprintf(buf, "[Constraint Likelihood] MH : %f Deter : %f => Prob : %f", mh_dist(0), deter, prob);
    output_writer->write_to_buffer(buf);

    //applying to gaussian probability 
    //double prob = exp(-mh_dist(0)/2)/ (pow((2 * M_PI), 1.5) * pow(deter, 0.5));
    if(v)
        cout << "Probability : " << prob << endl;

    //exit(-1);

    return prob;
}

/*
  double SlamGraph::calculateProbability(SlamNode *node1, SlamNode *node2, Matrix3d cov_sm, Pose2d sm_constraint){
    char buf[1024];
    
    MatrixXd cov = getCovariances(node2, node1);
    Pose2d value1 = node1->getPose();
    Pose2d value2 = node2->getPose();
    double x1 = value2.x(), y1 = value2.y(), t1 = value2.t();
    double x2 = value1.x(), y2 = value1.y(), t2 = value1.t();
    double c1 = cos(t1), s1 = sin(t1);
    
    //Jacobian 
    MatrixXd H(3,6); 
    int v = 0;
    
    sprintf(buf, "[Constraint Likelihood] Constraint between %d - %d", node1->region_node->region_id, node2->region_node->region_id);
    output_writer->write_to_buffer(buf);

    //jacobian is correct 
    H.setZero();
    H(0,0) = c1; H(0,1) = s1; H(0,2) = 0;
    H(0,3) = -c1; H(0,4) = -s1; H(0,5) = -(x2-x1)*s1 + (y2-y1)*c1;
    H(1,0) = -s1; H(1,1) = c1; H(1,2) = 0;
    H(1,3) = s1; H(1,4) = -c1; H(1,5) = -(x2-x1) * c1 - (y2-y1) *s1;
    H(2,2) = 1;
    H(2,5) = -1;
    
    double mean_x = (x2-x1)*c1 + (y2-y1)*s1;
    double mean_y = -(x2-x1)*s1 + (y2-y1)*c1;
    double mean_t = bot_mod2pi(t2-t1);
    
    if(v){
        fprintf(stderr, "Mean from graph : %f,%f,%f => Mean from scanmatch : %f,%f,%f\n", 
                mean_x, mean_y, mean_t, sm_constraint.x(), 
                sm_constraint.y(),
                sm_constraint.t());

        cout << "H : " << endl << H << endl;
        cout << "Joint Covariance : " << endl << cov << endl;
    }
    Matrix3d result = H * cov * H.transpose() + cov_sm;
    if(v){
        cout << "Result  : " << endl << result << endl;
    }
    
    sprintf(buf, "[Constraint Likelihood] Joint Cov : %f, %f %f", cov(0,0), cov(0,1), cov(0,2));
    output_writer->write_to_buffer(buf);

    sprintf(buf, "[Constraint Likelihood] Joint Cov : %f, %f %f", cov(1,0), cov(1,1), cov(1,2));
    output_writer->write_to_buffer(buf);
    
    sprintf(buf, "[Constraint Likelihood] Joint Cov : %f, %f %f", cov(2,0), cov(2,1), cov(2,2));
    output_writer->write_to_buffer(buf);

    sprintf(buf, "[Constraint Likelihood] Result Cov : %f, %f %f", result(0,0), result(0,1), result(0,2));
    output_writer->write_to_buffer(buf);

    sprintf(buf, "[Constraint Likelihood] Result Cov : %f, %f %f", result(1,0), result(1,1), result(1,2));
    output_writer->write_to_buffer(buf);
    
    sprintf(buf, "[Constraint Likelihood] Result Cov : %f, %f %f", result(2,0), result(2,1), result(2,2));
    output_writer->write_to_buffer(buf);


    MatrixXd dx(3,1);
    
    //mean for the new random variable 
    dx(0) = (mean_x - sm_constraint.x());
    dx(1) = (mean_y - sm_constraint.y());
    dx(2) = bot_mod2pi(mean_t - sm_constraint.t());

    sprintf(buf, "[Constraint Likelihood] Mean XYT : %f, %f %f", mean_x, mean_y, mean_t);
    output_writer->write_to_buffer(buf);

    sprintf(buf, "[Constraint Likelihood] SM Const : %f, %f %f", sm_constraint.x(), sm_constraint.y(), sm_constraint.t());
    output_writer->write_to_buffer(buf);

    if(v)
        cout << "Distance : " << dx<<endl; 
    MatrixXd mh_dist = dx.transpose() * result.inverse() * dx;
    if(v)
        cout << "Mahalanobis Distance : " << mh_dist<<endl;
    double deter = result.determinant();
    if(v)
        cout << "Determinanant : " << deter << endl;
  

    //applying to gaussian probability 
    double prob = exp(-mh_dist(0)/2)/ (pow((2 * M_PI), 1.5) * pow(deter, 0.5));
    if(v)
        cout << "Probability : " << prob << endl;

    sprintf(buf, "[Constraint Likelihood] Distance : %f, %f -> %f", dx(0), dx(1), hypot(dx(0), dx(1)));
    output_writer->write_to_buffer(buf);
    sprintf(buf, "[Constraint Likelihood] MH : %f Deter : %f => Prob : %f", mh_dist(0), deter, prob);
    output_writer->write_to_buffer(buf);

    return prob;
}
 */

SlamNode * SlamGraph::getLastSlamNode(){
    return getSlamNodeFromPosition(slam_node_list.size()-1);
}

void SlamGraph::updateSupernodeIDs(){

    //write this algorithm properly 
    int max_id = slam_node_list.size()-1;
    
    SlamNode *last_nd = getSlamNodeFromPosition(max_id);

    if(!last_nd->slam_pose->is_supernode)
        return;

    last_nd->parent_supernode = last_nd->id;
    int prev_supernode = getPreviousSupernodeID(last_nd->id,SUPERNODE_SEARCH_DISTANCE);
    int next_supernode = getNextSupernodeID(last_nd->id,SUPERNODE_SEARCH_DISTANCE);
    
    for(int i = max_id-1; i >getSlamNodePosition(prev_supernode); i--){ 
        SlamNode *nd = getSlamNodeFromPosition(i);

        if(nd->id <= last_nd->id){
            nd->parent_supernode = last_nd->id;
        }
        else{
            nd->parent_supernode = next_supernode;
        }

        /*if(fabs(nd->id - prev_supernode) < fabs(nd->id - last_nd->id)){
            nd->parent_supernode = prev_supernode;
        }
        else{
            nd->parent_supernode = last_nd->id;
            }*/
    }

    /*for(int i = max_id; i >= 0; i--){
        SlamNode *nd = getSlamNode(i);
        fprintf(stderr, "\t[%d] - Parent : %d\n", i, nd->parent_supernode);
        }*/
}

int SlamGraph::getPreviousSupernodeID(int ind, int search_distance){
    int start_super_ind = fmax(0, getSlamNodePosition(ind) - search_distance);
    //search near the region - and find the closest two nodes - that belong to the relavent supernodes 
    int max_id = slam_node_list.size()-1;
    int prev_supernode = -1;
    for(int i = getSlamNodePosition(ind) -1; i >= start_super_ind; i--){ 
        if(i < 0 || i > max_id)
            break;
        SlamNode *nd1 = getSlamNodeFromPosition(i);
        if(nd1 == NULL)
            continue;

        NodeScan *pose_1 = nd1->slam_pose;
        
        //fprintf(stderr, "\t\t%d : %d\n", i, pose_1->is_supernode);
        //if this node is not the current node and is also a supernode - break
        if(pose_1->is_supernode == true){
            prev_supernode = nd1->id;
            break;
        }
    }
    return prev_supernode;
}


int SlamGraph::getNextSupernodeID(int ind, int search_distance){

    //search near the region - and find the closest two nodes - that belong to the relavent supernodes 
    int max_id = slam_node_list.size()-1;
    int end_super_ind = fmin(max_id, getSlamNodePosition(ind) + search_distance);
    int next_supernode = -1;
    for(int i = getSlamNodePosition(ind) +1; i <= end_super_ind; i++){ 
        if(i < 0 || i > max_id)
            break;
        SlamNode *nd1 = getSlamNodeFromPosition(i);
        if(nd1 == NULL)
            continue;
        NodeScan *pose_1 = nd1->slam_pose;
        //if this node is not the current node and is also a supernode - break
        if(pose_1->is_supernode == true){
            next_supernode = nd1->id;
            break;
        }
    }
    return next_supernode;
}

int SlamGraph::belongsToCurrentSupernode(int ind, int curr_sn, int prev_sn, int next_sn){
    //nodes behind belong to the supernode 
    if(0){
        if(prev_sn < 0)
            if(ind <= curr_sn){
                return 1;
            }
        
        if(ind > prev_sn && ind <= curr_sn){
            return 1;
        }
        return 0;
    }
    if(0){
        if(next_sn < 0)
            if(ind >= curr_sn){
                return 1;
            }
        
        if(ind < next_sn && ind >= curr_sn){
            return 1;
        }
        return 0;
    }    
    else{
        int dist_to_prev = 100;
        int dist_to_next = 100;
        int dist_to_current = fabs(ind - curr_sn);
        if(prev_sn >=0){
            dist_to_prev = fabs(ind - prev_sn);
        }
        if(next_sn >=0){
            dist_to_next = fabs(ind - next_sn);
        }
        if(dist_to_current <= dist_to_prev && dist_to_current <= dist_to_next){
            return 1;
        }
        return 0;
    }
}


void SlamGraph::addOriginConstraint(SlamNode *node1, Pose2d transform, Noise noise){
    Pose2d_Node *current_node = node1->pose2d_node;
    origin_constraint = new Pose2d_Pose2d_Factor(origin_node, current_node, 
                                                 transform, 
                                                 noise);
    slam->add_factor(origin_constraint);
}

SlamConstraint* SlamGraph::addConstraint(SlamNode *node1,  SlamNode *node2, SlamNode *actualnode1, 
                                         SlamNode *actualnode2, Pose2d transform, Noise noise, 
                                         double hit_pct, int32_t type, int32_t status){
    int id = no_edges_added;
    SlamConstraint *constraint = addConstraint(id, node1,  node2, actualnode1, actualnode2, transform, noise, hit_pct, type, status);
    return constraint;
}

void SlamGraph::fillLanguageCollection(slam_complex_language_collection_t &msg){
    msg.count = (int) complex_language_events.size();
    msg.language = (slam_complex_language_path_list_t *) calloc(msg.count, sizeof(slam_complex_language_path_list_t));
       
    map<int, ComplexLanguageEvent *>::iterator it_lang; 
    int i=0;
    vector<ComplexLanguageEvent *> valid_complex_language; 
    for(it_lang = complex_language_events.begin(); it_lang != complex_language_events.end(); it_lang++, i++){
        ComplexLanguageEvent *event = it_lang->second;
        event->fillNodePaths(msg.language[i]);
    }    
}

void SlamGraph::fillOutstandingLanguageCollection(slam_complex_language_collection_t &msg){
    msg.count = (int) complex_language_events.size();
    msg.language = (slam_complex_language_path_list_t *) calloc(msg.count, sizeof(slam_complex_language_path_list_t));
        
    map<int, ComplexLanguageEvent *>::iterator it_lang; 
    int i=0;
    vector<ComplexLanguageEvent *> valid_complex_language; 
    for(it_lang = complex_language_events.begin(); it_lang != complex_language_events.end(); it_lang++){
        ComplexLanguageEvent *event = it_lang->second;
        
        bool to_send = true; 

        if(event->complete){
            to_send = false;
        }
        if(event->handled_internally){
            to_send = false;
        }
        if(event->failed_grounding && ((event->number_of_regions_added_since_failure < new_regions_added_threshold) && (event->getIDsAddedSinceFailure() < NO_NODES_ADDED_AFTER_FAILURE))){
            to_send = false;
        }

        if(to_send){ 
            //!event->complete){                                                                
            event->fillOutstandingNodePaths(msg.language[i]);
            i++;
        }
    }    
    //reallocate to the new language size
    fprintf(stderr, "Outstanding Querry count : %d\n", i);
    msg.count = i;
    msg.language = (slam_complex_language_path_list_t *) realloc(msg.language, msg.count * sizeof(slam_complex_language_path_list_t));
}

int SlamGraph::addComplexLanguageUpdates(slam_complex_language_result_t *slu_msg, vector<RegionNode *> &grounded_regions, bool finish_if_good_grounding, bool finish_always){
    double grounding_threshold = 0.1; //dont add anything below this 
        
    map<int, ComplexLanguageEvent *>::iterator it_lang; 
    for(int i=0; i< slu_msg->count; i++){
        slam_complex_language_path_result_list_t *event_result = &slu_msg->language[i];
        it_lang = complex_language_events.find(event_result->event_id);
        assert(it_lang != complex_language_events.end());
        ComplexLanguageEvent *event = it_lang->second;           

        if(finish_always){
            event->complete = true;
        }
        
        bool integrate_paths = true; 
        
        if(finish_if_good_grounding){
            //check the max path likelihood 
            double max_path_prob = 0; 
            
            for(int i=0; i < event_result->no_paths; i++){
                slam_complex_language_path_result_t *p_result = &event_result->results[i];

                for(int j=0; j < p_result->no_landmarks; j++){
                    //not sure if we should use this - or some overall prob measure 
                    if(max_path_prob < p_result->landmark_results[j].path_probability){
                        max_path_prob = p_result->landmark_results[j].path_probability;
                    }
                }
            }

            fprintf(stderr, "Max Path prob %f\n", max_path_prob);
            if(max_path_prob < 0.2){
                fprintf(stderr, "Path prob %f is not high enough - adding to reground list \n", max_path_prob);
                integrate_paths = false;                
            }            
        }
        
        if(integrate_paths){
            vector<pair<RegionNode *, slam_complex_language_path_result_t*> > path_results; 
            if(use_factor_graphs){
                for(int i=0; i < event_result->no_paths; i++){
                    slam_complex_language_path_result_t *p_result = &event_result->results[i];
                    SlamNode *nd = getSlamNodeFromID(p_result->node_id);
                    path_results.push_back(make_pair(nd->region_node, p_result));
                }                   
                
                event->updateResult(path_results);
            }
            else{               
                vector<regionProb> figure_groundings; 

                for(int i=0; i < event_result->no_paths; i++){
                    slam_complex_language_path_result_t *p_result = &event_result->results[i];

                    SlamNode *nd = getSlamNodeFromID(p_result->node_id);
                    RegionNode *figure_region = nd->region_node;
                    
                    map<int, double> landmark_likelihoods = event->getLandmarkLikelihoodsForFigure(figure_region);

                    map<int, double>::iterator it_like; 

                    /*fprintf(stderr, "==== Landmark likelihood for figure : %d  (No of landmarks : %d)=====\n", figure_region->region_id, landmark_likelihoods.size());
                    for(it_like = landmark_likelihoods.begin(); it_like != landmark_likelihoods.end(); it_like++){
                        fprintf(stderr, "\t[%d] - %f\n", it_like->first, it_like->second);
                        }*/

                    double ground_prob = 0;
                    for(int j=0; j < p_result->no_landmarks; j++){
                        int landmark_id = p_result->landmark_results[j].landmark_id;
                        
                        double lm_prob = 0;
                        it_like = landmark_likelihoods.find(landmark_id);

                        if(it_like != landmark_likelihoods.end()){
                            lm_prob = it_like->second;
                        }

                        //fprintf(stderr, "Landmark : %d -> %f\n", landmark_id, lm_prob);
                        
                        ground_prob += p_result->landmark_results[j].path_probability * lm_prob; 
                        fprintf(stderr, "\tPath Prob : %f - Landmark (%d) Prob : %f => Overall : %f\n", p_result->landmark_results[j].path_probability, 
                                landmark_id, lm_prob, p_result->landmark_results[j].path_probability * lm_prob);
                    }
                    
                    //should the ground prob be normalized?? 
                    figure_groundings.push_back(make_pair(figure_region, ground_prob));
                }

                sort(figure_groundings.begin(), figure_groundings.end(), compareRegionProb);
                                
                for(int i=0; i < figure_groundings.size(); i++){
                    RegionNode *figure_region = figure_groundings[i].first;
                    double ground_prob = figure_groundings[i].second;
                    
                    int figure_id = figure_region->labeldist->getLabelID(event->figure);
                    
                    if(i>= LANGUAGE_TOP_N_GROUNDINGS){
                        fprintf(stderr, "Done grounding the top %d\n", LANGUAGE_TOP_N_GROUNDINGS);
                        break;
                    }
                    //this event id is wrong 
                    if(ground_prob > grounding_threshold){
                        figure_region->labeldist->addObservation(figure_id, ground_prob, 1000 + event->event_id);
                        grounded_regions.push_back(figure_region);
                        fprintf(stderr, "Grounding Prob for region : %d Adding => %f\n", (int) figure_region->region_id, ground_prob);
                    }
                    else{
                        fprintf(stderr, "Grounding Prob for region : %d Too low=> %f\n", (int) figure_region->region_id, ground_prob);
                        break;
                    }
                } 
            }
            
            //decide this based on the mode - i.e. we might not want to ground this till we get a good likelihood score 
            if(finish_if_good_grounding){
                fprintf(stderr, "Finishing integration\n");
                event->complete = true;
                
                //we need to send up the regions that got grounded - so that loop closures can be performed if desired 
                
                

                /*
                  int label_id = current_region->labeldist->getLabelID(string(annotation->update));
                    
                    if(label_id>=0){
                        current_region->labeldist->addDirectObservation(label_id, prob_correspondance, language_event_count);
                    }

                 */

                //we should integrate the information immediately to the label distributions if we are not using factor graphs 
            }
        }
        else if(finish_if_good_grounding){
            //we need a way to tell this to not try to ground immediately 
            //HACK 
            fprintf(stderr, "Failed to ground language -> adding to ungrounded language - for checking groundings later\n");
            //exit(-1);
            event->failed_grounding = true;
            event->number_of_regions_added_since_failure = 0;
            event->updateFailedGrounding();
        }
    }
}

void SlamGraph::buildBoostGraph(graph_t &g, bool add_close_node_connections){
    map<int, vertex_descriptor> vertex_map;
    map<int, vertex_descriptor>::iterator it_v;
    for (int i=0; i < slam_node_list.size(); i++){
        vertex_descriptor v = add_vertex(g);
        g[v].id = slam_node_list[i]->id;        
        vertex_map.insert(make_pair(g[v].id, v));
    }

    map<int, SlamConstraint *>::iterator it;
    set<pair<int, int> > valid_edges;
    set<pair<int, int> >::iterator it_edge_set;
    map<int, int>::iterator it_i;
    map<int, int>::iterator it_j;

    for (int i=0; i < slam_node_list.size(); i++){
        SlamNode *nd_i = slam_node_list[i];

        for (int j=0; j < slam_node_list.size(); j++){
            SlamNode *nd_j = slam_node_list[j];

            if(nd_i == nd_j){
                continue;
            }

            it = nd_i->constraints.find(nd_j->id);
            Pose2d delta = nd_i->getPose().ominus(nd_j->getPose());

            if(it != nd_i->constraints.end() || (add_close_node_connections && (hypot(delta.x(), delta.y()) < 1.0))){
                it_edge_set = valid_edges.find(make_pair(nd_i->id, nd_j->id));
                if(it_edge_set != valid_edges.end())
                    continue;
                it_edge_set = valid_edges.find(make_pair(nd_j->id, nd_i->id));
                if(it_edge_set != valid_edges.end())
                    continue;
                valid_edges.insert(make_pair(nd_i->id, nd_j->id));
            }
        }
    }
        
    for(it_edge_set = valid_edges.begin(); it_edge_set != valid_edges.end(); it_edge_set++){
        nodePairKey edge = *it_edge_set; 
        it_v = vertex_map.find(edge.first);
        if(it_v == vertex_map.end()){
            //fprintf(stderr, "Error - No vertex descriptor found for node : %d\n", it_i->first);
            continue;
        }

        vertex_descriptor v_i = it_v->second;
        it_v = vertex_map.find(edge.second);
        if(it_v == vertex_map.end()){
            //fprintf(stderr, "Error - No vertex descriptor found for node : %d\n", it_i->second);
            continue;
        }
        vertex_descriptor v_j = it_v->second;
        SlamNode *nd_i = getSlamNodeFromID(edge.first);
        SlamNode *nd_j = getSlamNodeFromID(edge.second);
        Pose2d delta = nd_i->getPose().ominus(nd_j->getPose());
        double weight = hypot(delta.x(), delta.y());
        add_edge(v_i, v_j, Weight(weight), g);
    }
}

map<SlamNode*, SlamNode *> SlamGraph::getShortestPathTree(SlamNode *node, graph_t &g)
{
    property_map<graph_t, edge_weight_t>::type weightmap = get(edge_weight, g);
    std::vector<vertex_descriptor> p(num_vertices(g));
    std::vector<double> d(num_vertices(g));
    
    dijkstra_shortest_paths(g, node->b_vertex,
                            predecessor_map(boost::make_iterator_property_map(p.begin(), 
                                                                              get(boost::vertex_index, g))).
                            distance_map(boost::make_iterator_property_map(d.begin(), get(boost::vertex_index, g))));
        
    int64_t e_utime = bot_timestamp_now();
    //fprintf(stderr, CYAN "Time to find Shortest Path : %f\n" RESET_COLOR, (e_utime - s_utime) / 1.0e6);
    //fprintf(stderr, CYAN "Distances from %d and parents\n" RESET_COLOR, node->id);
    graph_traits < graph_t >::vertex_iterator vi, vend;
    //from this we should build the minimum spanning tree for the graph - from the last node 

    map<SlamNode*, SlamNode *> parent_map; 
    set<SlamNode *> node_set;
    map<SlamNode*, set<SlamNode *> > neighbour_map; 

    map<SlamNode*, set<SlamNode *> >::iterator it_neighbour_map;
    
    //this is not building a minimum spanning tree right now 
    //this just tells which edges are there (which is effected by the way its included) 
    
    for (boost::tie(vi, vend) = vertices(g); vi != vend; ++vi) {        
        SlamNode *nd_i = getSlamNodeFromID(g[*vi].id);
        SlamNode *nd_j = getSlamNodeFromID(g[p[*vi]].id);

        parent_map.insert(make_pair(nd_i, nd_j));
    }
        
    return parent_map; 
}

void SlamGraph::getShortestPathTree(SlamNode *node, graph_t &g, map<SlamNode*, SlamNode *> &parent_map, map<SlamNode *, double> &dist_map)
{
    property_map<graph_t, edge_weight_t>::type weightmap = get(edge_weight, g);
    std::vector<vertex_descriptor> p(num_vertices(g));
    std::vector<double> d(num_vertices(g));
 
    dijkstra_shortest_paths(g, node->b_vertex,
                            predecessor_map(boost::make_iterator_property_map(p.begin(), 
                                                                              get(boost::vertex_index, g))).
                            distance_map(boost::make_iterator_property_map(d.begin(), get(boost::vertex_index, g))));
        
    int64_t e_utime = bot_timestamp_now();
    //fprintf(stderr, CYAN "Time to find Shortest Path : %f\n" RESET_COLOR, (e_utime - s_utime) / 1.0e6);
    //fprintf(stderr, CYAN "Distances from %d and parents\n" RESET_COLOR, node->id);
    graph_traits < graph_t >::vertex_iterator vi, vend;
    //from this we should build the minimum spanning tree for the graph - from the last node 

    map<SlamNode*, SlamNode *>::iterator it;
    
    //this is not building a minimum spanning tree right now 
    //this just tells which edges are there (which is effected by the way its included) 
    
    for (boost::tie(vi, vend) = vertices(g); vi != vend; ++vi) {        
        SlamNode *nd_i = getSlamNodeFromID(g[*vi].id);
        SlamNode *nd_j = getSlamNodeFromID(g[p[*vi]].id);
        
        if(nd_i == nd_j){
            continue;
        }

        it = parent_map.find(nd_i);
        if(it != parent_map.end()){
            parent_map.erase(it);
        }
        parent_map.insert(make_pair(nd_i, nd_j));
        dist_map.insert(make_pair(nd_i, d[*vi]));
    }

    //we should set special value to the same node
    parent_map.insert(make_pair(node, node));
    dist_map.insert(make_pair(node, 0));
}

map<SlamNode*, SlamNode *> SlamGraph::getShortestPathTree(SlamNode *node)
{
    graph_t g; 
    
    buildBoostGraph(g);
    return getShortestPathTree(node, g);
}

//other option is to update this every time the graph is updated?? - with some heuristic 
//then the wrong paths can get updated 

void SlamGraph::checkCurrentRegionForInfoGain(RegionNode *current_region){
    //this should be general 
    //check the reduction in entropy if the robot asks about the current region 
    if(complex_language_events.size() == 0){
        return;
    }
    vector<SRQuestion> language_questions; 
    //find the max question to ask here 
    for(int i=0; i < complex_language_events.size(); i++){
        ComplexLanguageEvent *event = complex_language_events[i];
        int landmark = 0;
        //maybe should check with topologically close locations??
        double info_gain = event->evalQuestionsAboutRegion(current_region, 0.3, &landmark);        
    }    
}

slam_language_question_t* SlamGraph::getOutstandingQuestion(int64_t p_id){
    //the SR should be given an ID and moved to the asked question queue?? 
    slam_language_question_t *msg = NULL;
    if(outstanding_question){
        if(asked_question){
            //should we ever get to this state ?? 
            fprintf(stderr, "Oustanding question asked - unable to ask this question\n");
            return NULL;
        }
        else{
            msg = (slam_language_question_t *) calloc(1, sizeof(slam_language_question_t));
            msg->id = outstanding_question->id;
            msg->particle_id = p_id;
            msg->utime = bot_timestamp_now();
            string question_string = outstanding_question->getQuestionString();//askSRQuestion(*outstanding_question);
            msg->question = strdup(question_string.c_str());
            asked_question = outstanding_question;
            outstanding_question = NULL;
        }
    }
    return msg;     
}

void SlamGraph::clearQuestion(){
    if(asked_question){
        asked_question->event->updateSRQuestionAnswer(asked_question, "invalid");
        asked_question = NULL;
        output_writer->write_to_buffer("[Slam Graph] [Answers] Question timeout - clearing question");
    }
    else{
        fprintf(stderr, "No question has been asked\n");
    }
}

int SlamGraph::updateAnswer(int id, string answer){
    //ignore the id - its handled upstairs now 
    char buf[1024];

    sprintf(buf, "[Slam Graph] [Answers] Updating answer");
    output_writer->write_to_buffer(buf);

    if(asked_question){
        //if(asked_question->id == id){
        fprintf(stderr, "Question matches answer ID\n");
        fprintf(stderr, "Answer to question : %s - %s\n", asked_question->getQuestionString().c_str(), 
                answer.c_str());

        sprintf(buf, "[Slam Graph] [Answers] Have outstanding question - ");
        output_writer->write_to_buffer(buf);
        //add routine to update the complex language with the answer 

        //TO-DO//
        //if the answer is that this is the place - then we should set the complex language as complete 

        //integrate the received answer to the complex language 
        //if(!strcmp(answer, "yes")){

        ComplexLanguageEvent *event = asked_question->event;
        
        sprintf(buf, "[Slam Graph] [Answers] Before Distribution ");
        output_writer->write_to_buffer(buf);
        event->printFigureDistribution(string("[Answers] [Before]"));
        
        if(!answer.compare("yes")){
            fprintf(stderr, "Received a yes answer\n");
            //integrate yes answer 
            asked_question->event->updateSRQuestionAnswer(asked_question, answer);
        } 
        else if(!answer.compare("no")){//!strcmp(answer, "no")){
            fprintf(stderr, "Received a no answer\n");
            asked_question->event->updateSRQuestionAnswer(asked_question, answer);
        }

        runBP();

        sprintf(buf, "[Slam Graph] [Answers] After Distribution ");
        event->printFigureDistribution(string("[Answers] [After]"));

        asked_question = NULL;
        return 0;
    }
    else{
        fprintf(stderr, "No question has been asked\n");
        return -1;
    }
}

int SlamGraph::getNextQuestionID(){
    return SlamGraph::asked_question_id++;
}

/*string SlamGraph::askSRQuestion(SRQuestion &question){
    
    //add this to a queue maybe and call from main - since main is the only place where the lcm publishing happens 
    //right now 
    string question_label; 
    if(question.landmark){
        question_label = question.event->landmark;
    }
    else{
        question_label = question.event->figure;
    }
    string question_str = createQuestion(question_label, question.sr_result.sr);
    return question_str;
    }*/

void SlamGraph::updateOutstandingQuestion(SRQuestion &question){
    if(outstanding_question){
        //this question wasn't asked yet - and we had time to eval a new question - we should delete this - and update with the new one       
        output_writer->write_to_buffer("[Question Update] Already have outstanding question - which hasn't been cleared - Deleting old one and updating");
        delete outstanding_question; 
        //return;
    }
    outstanding_question = new SRQuestion(question);
    outstanding_question->updateEventCount();
    outstanding_question->setID(getNextQuestionID());
    output_writer->write_to_buffer("[Question Update] Updated Outstanding question");
}

void SlamGraph::updateConnectivityGraph(){
#ifdef REBUILD_BOOST_GRAPH
    bool add_close_node_connections = false;
    graph_t g;
    buildBoostGraph(g, add_close_node_connections);
    updateParentMap();
#endif
}

vector<SRQuestion> SlamGraph::getSRQuestionForLanguageEvent(int event_id, 
                                                            bool considerBotRelativeYesNoQuestions, 
                                                            bool considerBotRelativeMCQQuestions, 
                                                            bool considerLandmarkQuestions, 
                                                            bot_lcmgl_t *lcmgl){
    ComplexLanguageEvent *event = getComplexLanguageEvent(event_id);
    assert(event);

    vector<SRQuestion> language_questions; 

    SlamNode *node = getLastSlamNode();
    char buf[1024];
    //ComplexLanguageEvent *event = complex_language_events[i];
    sprintf(buf, "[Slam Graph] [Questions] [Yes/No] --------------------------------------");
    output_writer->write_to_buffer(buf);
    sprintf(buf, "[Slam Graph] [Questions] [Yes/No] Event : %d No of asked questions : %d", 
            event->event_id, event->getNoAskedQuestions());
    output_writer->write_to_buffer(buf);
    
    int landmark = 0;
    
    //we need all the questions 
    const spatialRelation lst[] = {LEFT_OF, RIGHT_OF, IN_FRONT_OF, BEHIND, NEAR}; //, AT};
    vector<spatialRelation> sr_list(lst, lst + sizeof(lst)/sizeof(spatialRelation));
    
#ifdef REBUILD_BOOST_GRAPH
    bool add_close_node_connections = false;
    graph_t g;
    buildBoostGraph(g, add_close_node_connections);
    updateParentMap();
#endif
    
    if(considerBotRelativeYesNoQuestions){
        vector<spatialResult> sr_results = event->getQuestionCosts(slu_classifier, node, 
                                                        &landmark, parent_map_current, sr_list);
        
        for(int i=0; i < sr_results.size(); i++){
            SRQuestion question(event, landmark, sr_results[i], node); 
            language_questions.push_back(question);
        }
    }

    landmark = 0;
    
    if(considerBotRelativeMCQQuestions){
        
    }
    
    landmark = 0;

    if(considerLandmarkQuestions){
        vector<RegionNode *> labeled_regions = getLabeledRegions(node->region_node, 0.5);
        if(labeled_regions.size() > 0){
#ifndef REBUILD_BOOST_GRAPH
            bool add_close_node_connections = false;
            graph_t g;
            buildBoostGraph(g, add_close_node_connections);
#endif    
            //check question about valid landmark set
            for(int j=0; j < labeled_regions.size(); j++){
                RegionNode *l_region = labeled_regions[j]; 
            
                map<SlamNode*, SlamNode *> parent_map;
                map<SlamNode *, double> dist_map;
        
                int64_t s_utime_2 = bot_timestamp_now();
                getShortestPathTree(l_region->mean_node, g, parent_map, dist_map);
                int64_t e_utime_2 = bot_timestamp_now();

                //evaluate paths from these labeled regions to other regions in the world 
                int landmark = 0;

                spatialResult sr_question = event->evalSRQuestionsFromLandmark(slu_classifier, l_region->mean_node, 
                                                                               &landmark, parent_map, lcmgl);

                sprintf(buf, "[Slam Graph] [Questions] [Landmark SR] SR likelihoods");
                output_writer->write_to_buffer(buf);
                                    
                if(sr_question.sr != INVALID){ 
                    sprintf(buf, "[Slam Graph] [Questions] [Landmark SR] Adding SR : %s - Cost : %f", getSR(sr_question.sr).c_str(), sr_question.cost); 
                    output_writer->write_to_buffer(buf);
                    //these regions and nodes are particle specific - well we don't really need the node 
                    //and the region here would be needed for the label only 
                    SRQuestion question(event, landmark, sr_question, node, l_region); 
                    language_questions.push_back(question);
                }
            }
        }
    }

    return language_questions;
}

void SlamGraph::checkSRQuestionInfoGain(SlamNode *node, bool considerBotRelativeYesNoQuestions, bool considerBotRelativeMCQQuestions, 
                                        bool considerLandmarkQuestions, bot_lcmgl_t *lcmgl){
    if(complex_language_events.size() == 0 || asked_question){ //if we have an asked question we will not ask another till we hear the answer - or timeout?? 
        return;
    }
    int64_t s_utime = bot_timestamp_now();
    //we should look at which one is the best question to ask about which complex language 
    //also need to know whether we are asking a question about the landmark/figure 

    int skip_question_threshold = 3;

    //find the max 
    double min_cost = MAX_COST_QUESTION; 
    double max_info_gain = 0; 

    //right now can ask questions about stuff that is quite far away 
    
    char buf[1024];

    vector<SRQuestion> language_questions; 
    
    sprintf(buf, "[Slam Graph] [Questions] ==========================================");
    output_writer->write_to_buffer(buf);

    //bool consider_bot_questions = false;//false; //consider questions w.r.t. to robot 
    //bool consider_lm_questions = true;

#ifdef REBUILD_BOOST_GRAPH
     bool add_close_node_connections = false;
     graph_t g;
     buildBoostGraph(g, add_close_node_connections);
     updateParentMap();
#endif

    if(considerBotRelativeYesNoQuestions){
        for(int i=0; i < complex_language_events.size(); i++){
            ComplexLanguageEvent *event = complex_language_events[i];
            sprintf(buf, "[Slam Graph] [Questions] [Yes/No] --------------------------------------");
            output_writer->write_to_buffer(buf);
            sprintf(buf, "[Slam Graph] [Questions] [Yes/No] Event : %d No of asked questions : %d", 
                    event->event_id, event->getNoAskedQuestions());
            output_writer->write_to_buffer(buf);

            int landmark = 0;
        
            spatialResult sr_question = event->evalSRQuestionsFromCurrentLocation(slu_classifier, node, 
                                                                                  &landmark, parent_map_current);

            if(sr_question.sr != INVALID){
                sprintf(buf, "[Slam Graph] [Questions] [Yes/No] Adding SR : %s - Cost : %f", getSR(sr_question.sr).c_str(), sr_question.cost); 
                output_writer->write_to_buffer(buf);
                SRQuestion question(event, landmark, sr_question, node); 
                language_questions.push_back(question);

                if(min_cost > sr_question.cost){
                    min_cost = sr_question.cost; 
                    max_info_gain = sr_question.info_gain;
                }
            }
        }
    }

    if(considerBotRelativeMCQQuestions){
        for(int i=0; i < complex_language_events.size(); i++){
            ComplexLanguageEvent *event = complex_language_events[i];

            sprintf(buf, "[Slam Graph] [Questions] --------------------------------------");
            output_writer->write_to_buffer(buf);
            sprintf(buf, "[Slam Graph] [Questions] Event : %d No of asked questions : %d", 
                    event->event_id, event->getNoAskedQuestions());
            output_writer->write_to_buffer(buf);

            /*int landmark = 0;

            spatialResult sr_question = event->evalMCQQuestionsFromCurrentLocation(slu_classifier, node, 
            &landmark, parent_map_current);
            
            sprintf(buf, "[Slam Graph] [Questions] SR likelihoods");
            output_writer->write_to_buffer(buf);

            //maybe there should be a penalty for how many questions/when the last question was asked ??
            //so that we do not keep asking about the same thing??
            //also a way to not ask the same question?? 
            if(sr_question.sr != INVALID){ // &&  sr_question.cost < cost_threshold){//sr_question.info_gain > 0){
                sprintf(buf, "[Slam Graph] [Questions] Adding SR : %s - Cost : %f", getSR(sr_question.sr).c_str(), sr_question.cost); 
                output_writer->write_to_buffer(buf);
                SRQuestion question(event, landmark, sr_question, node); 
                language_questions.push_back(question);

                if(min_cost > sr_question.cost){
                    min_cost = sr_question.cost; 
                    max_info_gain = sr_question.info_gain;
                }
                }*/
        }
    }

    if(considerLandmarkQuestions){
        vector<RegionNode *> labeled_regions = getLabeledRegions(node->region_node, 0.5);   

        sprintf(buf, "[Slam Graph] [Questions] [Landmark SR] Number of labeled regions : %d", (int) labeled_regions.size());
        output_writer->write_to_buffer(buf);

        if(labeled_regions.size() > 0){
#ifndef REBUILD_BOOST_GRAPH
            bool add_close_node_connections = false;
            graph_t g;
            buildBoostGraph(g, add_close_node_connections);
#endif    
            //check question about valid landmark set
            for(int j=0; j < labeled_regions.size(); j++){
                RegionNode *l_region = labeled_regions[j]; 
            
                map<SlamNode*, SlamNode *> parent_map;
                map<SlamNode *, double> dist_map;
        
                int64_t s_utime_2 = bot_timestamp_now();
                getShortestPathTree(l_region->mean_node, g, parent_map, dist_map);
                int64_t e_utime_2 = bot_timestamp_now();
        
                for(int i=0; i < complex_language_events.size(); i++){
                    ComplexLanguageEvent *event = complex_language_events[i];
            
                    sprintf(buf, "[Slam Graph] [Questions] --------------------------------------");
                    output_writer->write_to_buffer(buf);
                    sprintf(buf, "[Slam Graph] [Questions] Event : %d No of asked questions : %d", 
                            event->event_id, event->getNoAskedQuestions());
                    output_writer->write_to_buffer(buf);
            
                    //evaluate paths from these labeled regions to other regions in the world 
                    int landmark = 0;

                    spatialResult sr_question = event->evalSRQuestionsFromLandmark(slu_classifier, l_region->mean_node, 
                                                                                   &landmark, parent_map, lcmgl);

                    sprintf(buf, "[Slam Graph] [Questions] [Landmark SR] SR likelihoods");
                    output_writer->write_to_buffer(buf);
                                    
                    if(sr_question.sr != INVALID){ 
                        sprintf(buf, "[Slam Graph] [Questions] [Landmark SR] Adding SR : %s - Cost : %f", getSR(sr_question.sr).c_str(), sr_question.cost); 
                        output_writer->write_to_buffer(buf);
                        SRQuestion question(event, landmark, sr_question, node, l_region); 
                        language_questions.push_back(question);

                        if(min_cost > sr_question.cost){
                            min_cost = sr_question.cost; 
                            max_info_gain = sr_question.info_gain;
                        }
                    }
                }                
            }
            if(lcmgl){
                bot_lcmgl_switch_buffer (lcmgl);
            }
        }
    }

    //lets sort these 
    if(language_questions.size() > 0){
        sort(language_questions.begin(), language_questions.end(), compareSRQuestions);

        sprintf(buf, "[Slam Graph] [Questions] Max SR Likelihoods");
        output_writer->write_to_buffer(buf);

        //just being negative is not enough 
        if(min_cost < -1.0){
            sprintf(buf, "[Slam Graph] [Questions] [Asking] Event : %d Node : %d  SR : %s Cost : %f - Info Gain : %f \n", 
                    language_questions[0].event->event_id, (int) node->id,  
                    getSR(language_questions[0].sr_result.sr).c_str(), min_cost, max_info_gain);
            output_writer->write_to_buffer(buf);
            updateOutstandingQuestion(language_questions[0]);
        }
    }

    int64_t e_utime = bot_timestamp_now();
}

//Used
vector<RegionNode *> SlamGraph::findShortestPathFromOutstandingComplexLanguage(RegionNode *current_region, bool complete_event, bool complete_if_valid){
    
    static char buf[1024];
    
    vector<RegionNode *> grounded_regions;
    map<int, ComplexLanguageEvent *>::iterator it_lang; 

    vector<ComplexLanguageEvent *> valid_complex_language; 
    
    //we can control which language events are considered for this region - by setting the language events as complete 
    for(it_lang = complex_language_events.begin(); it_lang != complex_language_events.end(); it_lang++){
        if(it_lang->second->utterence_node->region_node != current_region && !it_lang->second->complete){
            sprintf(buf, "[Slam Graph] [Complex Language] [Shortest Path Update] Complex Language valid %d - adding", it_lang->second->event_id);
            output_writer->write_to_buffer(buf);

            valid_complex_language.push_back(it_lang->second);
        }
    }  

    if(valid_complex_language.size() ==0){
        sprintf(buf, "[Slam Graph] [Complex Language] [Shortest Path Update] No valid language");
        output_writer->write_to_buffer(buf);
        return grounded_regions; 
    }

    sprintf(buf, "[Slam Graph] [Complex Language] [Shortest Path Update] Total Complex Language : %d -> Valid Complex language : %d", 
            (int) complex_language_events.size(), (int) valid_complex_language.size());
    output_writer->write_to_buffer(buf);
    
    int64_t s_utime = bot_timestamp_now();
    
    sprintf(buf, "[Slam Graph] [Complex Language] [Shortest Path Update] Current Region : %d", (int) current_region->region_id);
    output_writer->write_to_buffer(buf);

    graph_t g;
    buildBoostGraph(g);

    for(int i=0; i < valid_complex_language.size(); i++){
        ComplexLanguageEvent *event = valid_complex_language[i];        

        event->updateLastCheckedGrounding(getLastNodeID());
        
        event->complete = complete_event;

        SlamNode *node = event->utterence_node; 

        map<SlamNode*, SlamNode *> parent_map;
        map<SlamNode *, double> dist_map;
        getShortestPathTree(node, g, parent_map, dist_map);

        int added_count = 0;

        vector<RegionNode *> landmark_regions;

        sprintf(buf, "[Slam Graph] [Complex Language] [Shortest Path Update] Complex Language associatd with region : %d", (int) node->region_node->region_id);
        output_writer->write_to_buffer(buf);

        for(int i=0; i < region_node_list.size(); i++){
            RegionNode *region = region_node_list[i];
            //skip the region in which the utterance was heard and the current unformed region

            if(region == current_region){
                continue;
            }
                       
            double dist = getPathLength(dist_map, region->mean_node); 

            sprintf(buf, "[Slam Graph] [Complex Language] [Shortest Path Update] [Distance] Region : %d => Distance : %f", region->region_id, dist);
            output_writer->write_to_buffer(buf);

            if(dist < ignore_landmark_dist){
                sprintf(buf, "[Slam Graph] [Complex Language] [Shortest Path Update] [Distance] Adding Landmark : %d\n", region->region_id);
                output_writer->write_to_buffer(buf);
                landmark_regions.push_back(region);
            }
            //if(region->mean_node->getDistanceToNode(current_event->utterence_node) > ignore_figure_dist){
            if(region == node->region_node || dist > ignore_figure_dist){
                continue;
            }
            
            added_count++;
            vector<SlamNode *> path = getPath(parent_map, region->mean_node, true);
            //std::reverse(path.begin(), path.end());
            event->addNodePath(region->mean_node, path);      

            sprintf(buf, "[Slam Graph] [Complex Language] [Shortest Path Update] [Distance] Adding Path : %d\n", region->region_id);
            output_writer->write_to_buffer(buf);
        }
       

        /**/
        event->updateLandmarks(landmark_regions);
        sprintf(buf, "[Slam Graph] [Complex Language] [Shortest Path Update] [Distance] Done Finding Shortest Path From Language Event %d => Added %d Paths", 
                event->event_id, added_count);         
        output_writer->write_to_buffer(buf);

        if(event->failed_grounding){
            sprintf(buf, "[Slam Graph] [Complex Language] [Shortest Path Update] [Distance] Failed event - New Regions added : %d", event->number_of_regions_added_since_failure);
            output_writer->write_to_buffer(buf);
            if(event->number_of_regions_added_since_failure > new_regions_added_threshold || event->getIDsAddedSinceFailure() > NO_NODES_ADDED_AFTER_FAILURE){              
                sprintf(buf, "[Slam Graph] [Complex Language] [Shortest Path Update] [Distance] Checking internally");
                output_writer->write_to_buffer(buf);

                event->evaluateRegionPaths(complete_if_valid, grounded_regions, MIN_LANG_GROUNDING_PROB, LANGUAGE_TOP_N_GROUNDINGS);
            }
        }
        else{
            event->evaluateRegionPaths(complete_if_valid, grounded_regions, MIN_LANG_GROUNDING_PROB, LANGUAGE_TOP_N_GROUNDINGS);
        }
    }
    return grounded_regions; 
}

int64_t SlamGraph::getLastNodeID(){
    SlamNode *last_node = getLastSlamNode();
    if(last_node){
        return last_node->id;
    }
    return -1; 
}

void SlamGraph::printComplexLanguageEvents()
{
    //update the paths for all complex language - get the shortest paths from each complex language node to other regions 
    //and update the regions 
    if(complex_language_events.size() == 0)
        return;

    map<int, ComplexLanguageEvent *>::iterator it_lang; 
    for(it_lang = complex_language_events.begin(); it_lang != complex_language_events.end(); it_lang++){
        //for efficiency - this should only be called upon if the graph changed significantly or 
        //if nodes were added nearby 

        ComplexLanguageEvent *current_event = it_lang->second;
        current_event->print();
    }
}

//Used 
void SlamGraph::findShortestPathFromAllComplexLanguage(RegionNode *current_region)
{
    static char buf[1024];

    //update the paths for all complex language - get the shortest paths from each complex language node to other regions 
    //and update the regions 
    if(complex_language_events.size() == 0)
        return;

    int64_t s_utime = bot_timestamp_now();
    
    graph_t g;
    buildBoostGraph(g);

    map<int, ComplexLanguageEvent *>::iterator it_lang; 
    for(it_lang = complex_language_events.begin(); it_lang != complex_language_events.end(); it_lang++){
        //for efficiency - this should only be called upon if the graph changed significantly or 
        //if nodes were added nearby 

        ComplexLanguageEvent *current_event = it_lang->second;
        
        //current_event->clearNodePaths();
        map<SlamNode*, SlamNode *> parent_map;
        map<SlamNode *, double> dist_map;
        getShortestPathTree(current_event->utterence_node, g, parent_map, dist_map);

        vector<RegionNode *> landmark_regions;

        for(int i=0; i < region_node_list.size(); i++){
            RegionNode *region = region_node_list[i];
            if(region == current_region){
                continue;
            }
            double dist = getPathLength(dist_map, region->mean_node); 
            //double dist = region->mean_node->getDistanceToNode(current_event->utterence_node);
            //fprintf(stderr, "Distance to region %d : %f\n", region->region_id, dist);

            if(dist < ignore_landmark_dist){
                landmark_regions.push_back(region);
            }

            if(dist > ignore_figure_dist){
                //fprintf(stderr, "skipping\n");
                continue;
            }
            vector<SlamNode *> path = getPath(parent_map, region->mean_node, true);
            //std::reverse(path.begin(), path.end());
            current_event->addNodePath(region->mean_node, path);
        }
        current_event->updateLandmarks(landmark_regions);
        sprintf(buf, "[Slam Graph] [Complex Language] [Shortest Path Update] [Distance] Done Finding Shortest Path From Language Event %d", 
                current_event->event_id);         
        output_writer->write_to_buffer(buf);
    }

    int64_t e_utime = bot_timestamp_now();
    //seems pretty short time to do this 
    sprintf(buf, "[Slam Graph] [Complex Language] [Shortest Path Update] [Performance] Time : %.3f", (e_utime - s_utime) / 1.0e6);
    output_writer->write_to_buffer(buf);
}

void SlamGraph::printParentMap(){
    char buf[1024]; 
    
    sprintf(buf, "[Shortest Path] [Parent Map] [Print] ==========================");
    output_writer->write_to_buffer(buf); 
    map<SlamNode *, SlamNode *>::iterator it; 
    for(it = parent_map_current.begin(); it != parent_map_current.end(); it++){
        sprintf(buf, "[Shortest Path] [Parent Map] [Print] \t%d - %d", it->first->id, it->second->id);
        output_writer->write_to_buffer(buf); 
    }

    string path_output;
    char path_op[20];
    
    for(int i=0; i < region_node_list.size(); i++){
        RegionNode *region = region_node_list[i];
        double dist = 0; 
        vector<SlamNode *> path = getPathAndLength(parent_map_current, region->mean_node, &dist);
        
        if(0){
            for(int j=0; j < path.size(); j++){
                sprintf(path_op, "%d, ", path[j]->id);
                path_output.append(path_op);
            }
        
            sprintf(buf, "[Shortest Path] [Print] Region : %d %f - %s", region->region_id, dist, path_output.c_str());
        }
        else{
            sprintf(buf, "[Shortest Path] [Print] Region : %d %f - %d", region->region_id, dist, (int) path.size());
        }
        output_writer->write_to_buffer(buf); 
        path_output.clear();
    }
}

vector<SlamNode *> SlamGraph::getPathsToRegion(RegionNode *node, double *dist){
    //get path to region from current node 

    char buf[1024]; 
    
    vector<SlamNode *> path;

    /*for(int i=0; i < region_node_list.size(); i++){
        RegionNode *region = region_node_list[i];
        vector<SlamNode *> path = getPath(parent_map_current, region->mean_node);
        
        for(int j=0; j < path.size(); j++){
            sprintf(path_op, "%d, ", path[j]->id);
            path_output.append(path_op);
        }
        
        sprintf(buf, "[Shortest Path] [Print] Region : %d - %s", region->region_id, path_output.c_str());
        output_writer->write_to_buffer(buf); 
        path_output.clear();
        }*/

    return path;
}

void SlamGraph::updateParentMap(){
    graph_t g;
    bool add_close_node_connections = false;
    buildBoostGraph(g, add_close_node_connections);
    parent_map_current.clear();
    dist_map_current.clear();
    getShortestPathTree(last_added_node, g, parent_map_current, dist_map_current);
}

void SlamGraph::updateParentMap(graph_t &g){
    parent_map_current.clear();
    dist_map_current.clear();
    getShortestPathTree(last_added_node, g, parent_map_current, dist_map_current);
}

//Used : With the c++ slu classifier 
void SlamGraph::evaluateAllComplexLanguageGroundings(RegionNode *current_region)
{
    //update the paths for all complex language - get the shortest paths from each complex language node to other regions 
    //and update the regions 
    if(complex_language_events.size() == 0)
        return;

    double s_path_time = 0; 

    int64_t s_utime_1 = bot_timestamp_now();
    
    graph_t g;
    bool add_close_node_connections = false;
    buildBoostGraph(g, add_close_node_connections);

    int64_t e_utime_1 = bot_timestamp_now();

    s_path_time += (e_utime_1 - s_utime_1) / 1.0e6;

    bool grounded = false;

    bool updated_answers = false;

    static char buf[1024]; 

    sprintf(buf, "[Shortest Path] [Parent Map] [Print] Resetting +++++++++++++");
    output_writer->write_to_buffer(buf); 

    if(last_added_node){
        updateParentMap(g);
        printParentMap();
    }
    
    map<int, ComplexLanguageEvent *>::iterator it_lang; 
    for(it_lang = complex_language_events.begin(); it_lang != complex_language_events.end(); it_lang++){
        //for efficiency - this should only be called upon if the graph changed significantly or 
        //if nodes were added nearby 

        ComplexLanguageEvent *current_event = it_lang->second;
        
        //current_event->clearNodePaths();
        map<SlamNode*, SlamNode *> parent_map;
        map<SlamNode *, double> dist_map;
        
        //we find the shortest path from the language utterance node to each valid region 
        //there is probably more efficient ways to ignore some language events 
        int64_t s_utime_2 = bot_timestamp_now();
        getShortestPathTree(current_event->utterence_node, g, parent_map, dist_map);
        int64_t e_utime_2 = bot_timestamp_now();

        s_path_time += (e_utime_2 - s_utime_2) / 1.0e6;

        vector<RegionNode *> landmark_regions;

        vector<RegionNode *> updated_figures;

        for(int i=0; i < region_node_list.size(); i++){
            RegionNode *region = region_node_list[i];
            /*if(region == current_region){
                continue;
                }*/
            
            //if we want the distance to the closest node (vs the mean node)
            //double dist = getPathLengthToClosestNode(parent_map, region->mean_node); //
            
            double dist = getPathLength(dist_map, region->mean_node);
            if(region->mean_node == current_event->utterence_node){
                dist = 0;
            }

            if(region == current_region){
                sprintf(buf, "[Shortest Path] [Parent Map] [Current Region] Region : %d - Node : %d Dist : %f", 
                        region->region_id, (int) region->mean_node->id, dist);
                output_writer->write_to_buffer(buf); 
            }
            //double dist = region->mean_node->getDistanceToNode(current_event->utterence_node);
            //fprintf(stderr, "Distance to region %d : %f\n", region->region_id, dist);

            sprintf(buf, "[Shortest Path] [Landmark] [Distance] Event : %d Distance to region %d : %f (%d - %d)", current_event->event_id, region->region_id, dist, 
                    (int) current_event->utterence_node->id, (int) region->mean_node->id);
            output_writer->write_to_buffer(buf); 

            if(region != current_region && dist < ignore_landmark_dist){
                landmark_regions.push_back(region);
                sprintf(buf, "[Shortest Path] [Landmark] [Distance] [Adding] Event : %d Adding region to landmark list %d", current_event->event_id, region->region_id);
                output_writer->write_to_buffer(buf); 
            }

            //not sure about the entropy reduction calculation for the current region 
            //some questions don't make sense (or maybe wait till the current region is of a good size before adding)
            if(region == current_region || dist > ignore_figure_dist){
                //fprintf(stderr, "skipping\n");
                continue;
            }

            vector<SlamNode *> path = getPath(parent_map, region->mean_node, true);

            //we are considering the current region for this as well 
            if(current_event->addNodePath(region->mean_node, path)){
                updated_figures.push_back(region);
            }
        }

        current_event->updateLandmarks(landmark_regions);
        //fprintf(stderr, GREEN "Done Finding Shortest Path From Language Event %d \n\n" RESET_COLOR, current_event->event_id);    

        if(slu_classifier){
            bool grounded_event = current_event->evaluateOutstandingPaths(slu_classifier); 
            if(grounded_event){
                grounded = true;
            }
        }

        //check if the Question nodes have changed 
        vector<SRQuestion *>  questions_to_update = current_event->getQuestionsToUpdate();
        
        if(questions_to_update.size() > 0){
            sprintf(buf, "[Updated Questions] Size : %d [Error] Not handled right now", (int) questions_to_update.size());
            output_writer->write_to_buffer(buf);      
            for(int k=0; k < questions_to_update.size(); k++){
                SRQuestion *question = questions_to_update[k];
                sprintf(buf, "[Updated Questions] Event : %d Question : %d Updating Question", current_event->event_id, question->id);
                output_writer->write_to_buffer(buf);   

                //get shortest path from the robot 
                map<SlamNode*, SlamNode *> parent_map;
                map<SlamNode *, double> dist_map;
                
                if(!question->landmark_region){
                    sprintf(buf, "[Updated Questions] Event : %d Question : %d Updating Robot Relative Question", current_event->event_id, question->id);
                    output_writer->write_to_buffer(buf);       
                           
                    getShortestPathTree(question->node, g, parent_map, dist_map);
                }
                else{
                    sprintf(buf, "[Updated Questions] Event : %d Question : %d Updating Landmark Relative Question", current_event->event_id, question->id);
                    output_writer->write_to_buffer(buf);       
                           
                    getShortestPathTree(question->landmark_region->mean_node, g, parent_map, dist_map);
                }
                    
                current_event->reevaluateAnsweredQuestion(slu_classifier, question, parent_map); 
            }
        }

        if(updated_figures.size()){
            sprintf(buf, "[Shortest Path] [Parent Map] [Updated Figures] Size : %d", (int) updated_figures.size());
            output_writer->write_to_buffer(buf); 

            //we should check how many questions were asked - and then either give shortest path 
            //from the question to these nodes 
            //or from the figure nodes to all nodes 
            vector<SlamNode *> answered_question_nodes = current_event->getAnsweredQuestionNodes();
            
            if(answered_question_nodes.size()){
                sprintf(buf, "[Slam Graph] [Answers] [Reeval] Number of answered questions : %d", (int) answered_question_nodes.size());
                output_writer->write_to_buffer(buf); 
                for(int i=0; i<updated_figures.size(); i++){
                    sprintf(buf, "[Shortest Path] [Parent Map] [Updated Figures] Figure Region : %d", updated_figures[i]->region_id);
                    output_writer->write_to_buffer(buf); 
                
                    map<SlamNode*, SlamNode *> parent_map_fig;
                    map<SlamNode *, double> dist_map_fig;
                    int64_t s_utime_2 = bot_timestamp_now();
                    getShortestPathTree(updated_figures[i]->mean_node, g, parent_map_fig, dist_map_fig);
                    int64_t e_utime_2 = bot_timestamp_now();

                    current_event->evaluateAnsweredQuestions(slu_classifier, updated_figures[i], parent_map_fig);
                    updated_answers = true;
                }
            }
            //we could provide shortest paths parent maps for each of these nodes - which can be used to fill in the 
            //question likelihoods             
        }
        
        
    }  
    
    int64_t e_utime = bot_timestamp_now();
    
    sprintf(buf, "[Complex Language] [All Language] [Performance] Time taken to find shortest path : %.3f - Total function time : %.3f", s_path_time, (e_utime - s_utime_1) / 1.0e6);
    output_writer->write_to_buffer(buf); 
    
    if(grounded || updated_answers){
        sprintf(buf, "[Complex Language] [All Language] Running BP : Grounded -> %d Updated Answers -> %d", grounded, updated_answers);
        output_writer->write_to_buffer(buf); 
        runBP();
    }
}

//Used - considers partially formed region - possibly should be depricated 
//this is for the partially formed region - so the eval should be provisional - to be flushed when the region is fully formed 
void SlamGraph::findShortestPathFromAllComplexLanguageToCurrentRegion(RegionNode *region)
{
    //update the paths for all complex language - get the shortest paths from each complex language node to other regions 
    //and update the regions 
    static char buf[1024]; 

    if(complex_language_events.size() == 0)
        return;

    int64_t s_utime = bot_timestamp_now();
    
    graph_t g;
    buildBoostGraph(g);

    map<SlamNode*, SlamNode *> parent_map;
    map<SlamNode *, double> dist_map;
    getShortestPathTree(region->mean_node, g, parent_map, dist_map);
    
    map<int, ComplexLanguageEvent *>::iterator it_lang; 
    for(it_lang = complex_language_events.begin(); it_lang != complex_language_events.end(); it_lang++){
        //for efficiency - this should only be called upon if the graph changed significantly or 
        //if nodes were added nearby 

        ComplexLanguageEvent *current_event = it_lang->second;
        
        double dist = getPathLength(dist_map, current_event->utterence_node); //region->mean_node); 
        
        if(dist > ignore_figure_dist){
            continue;
        }

        sprintf(buf, "[Complex Language] [Current Region] Path to region %d for event %d (dist)", region->region_id, 
                current_event->event_id, dist);
        output_writer->write_to_buffer(buf); 
        
        vector<SlamNode *> path = getPath(parent_map, current_event->utterence_node); //region->mean_node);
        sprintf(buf, "[Complex Language] [Current Region] [Path Print] =============");
        output_writer->write_to_buffer(buf); 

        for(int i=0; i < path.size(); i++){
            sprintf(buf, "[Complex Language] [Current Region] [Path Print] [Event %d] %d - %d", current_event->event_id, i, (int) path[i]->id);
            output_writer->write_to_buffer(buf); 
        }        
        
        //we should update this - so that it is added to a provisional path 
        current_event->addNodePath(region->mean_node, path);
    }

    int64_t e_utime = bot_timestamp_now();
    //fprintf(stderr, GREEN "Done Finding Shortest Path From Complex Language to completed region : %.5f\n\n" RESET_COLOR, (e_utime - s_utime) / 1.0e6);
}

int SlamGraph::getOutstandingLanguageCount(){
    int count = 0; 
    map<int, ComplexLanguageEvent *>::iterator it_lang; 
    for(it_lang = complex_language_events.begin(); it_lang != complex_language_events.end(); it_lang++){
        ComplexLanguageEvent *current_event = it_lang->second;

        bool to_send = true; 

        if(current_event->complete){
            fprintf(stderr, "Event : %d - Complete\n", current_event->event_id);
            to_send = false;
        }
        if(current_event->handled_internally){
            fprintf(stderr, "Event : %d - Handled Internally\n", current_event->event_id);
            to_send = false;
        }
        if(current_event->failed_grounding && ((current_event->number_of_regions_added_since_failure < new_regions_added_threshold) && (current_event->getIDsAddedSinceFailure() < NO_NODES_ADDED_AFTER_FAILURE))) {
            fprintf(stderr, "Event failed [%d] - New regions added : %d - Setting false\n", current_event->event_id, current_event->number_of_regions_added_since_failure);
            to_send = false;
        }

        if(to_send){ //!current_event->complete)
            count += current_event->getOutstandingCount();
            fprintf(stderr, "Event : %d - Adding to check Grounding : %d\n", current_event->event_id, current_event->getOutstandingCount());
        }
    }        

    fprintf(stderr, RED "\n\n Outstanding Language : %d\n", count);
    return count;
}

int SlamGraph::getLanguagePathCount(){
    int count = 0; 
    map<int, ComplexLanguageEvent *>::iterator it_lang; 
    for(it_lang = complex_language_events.begin(); it_lang != complex_language_events.end(); it_lang++){
        ComplexLanguageEvent *current_event = it_lang->second;
        count += current_event->getNoPaths();
    }
    
    return count;
}

ComplexLanguageEvent *SlamGraph::getComplexLanguageEvent(int event_id){
     map<int, ComplexLanguageEvent *>::iterator it;
        
    it = complex_language_events.find(event_id);

    if(it != complex_language_events.end())
        return it->second; 
    return NULL;
}

ComplexLanguageEvent * SlamGraph::getComplexLanguageEvent(SlamNode *node){
    map<int, ComplexLanguageEvent *>::iterator it_node;
        
    it_node = complex_language_events_by_node.find(node->id);

    if(it_node != complex_language_events_by_node.end())
        return it_node->second; 
    return NULL;
}

void SlamGraph::addComplexLanguageEvent(complex_language_event_t *complex_lang){
    map<int, ComplexLanguageEvent *>::iterator it = complex_language_events.find(complex_lang->language_event_id);
    if(it == complex_language_events.end()){
        ComplexLanguageEvent *complex_event = new ComplexLanguageEvent(complex_lang, output_writer, getSlamNodeFromID(complex_lang->node_id), label_info, use_factor_graphs);
        complex_language_events.insert(make_pair(complex_lang->language_event_id, complex_event));
        map<int, ComplexLanguageEvent *>::iterator it_node;
        
        it_node = complex_language_events_by_node.find(complex_lang->node_id);
        if(it_node == complex_language_events_by_node.end()){
            complex_language_events_by_node.insert(make_pair(complex_lang->node_id, complex_event));
        }
        else{ //unlikely that there will be more than one complex language event per node 
            fprintf(stderr, "Complex event already found for node - this should not have happened (or least not supported)\n");
            assert(false); 
        }
    }
    else{
        fprintf(stderr, "Duplicate language id\n");
        assert(false);
    }
}

SlamConstraint* SlamGraph::addConstraint(int id, SlamNode *node1,  SlamNode *node2, SlamNode *actualnode1, 
                                         SlamNode *actualnode2, Pose2d transform, Noise noise, 
                                         double hit_pct, int32_t type, int32_t status)
{
    SlamConstraint *constraint = new SlamConstraint(id, node1, node2, actualnode1, actualnode2, transform, noise, hit_pct, 1, type, status);
    //lets add the relavent node as well 
    if(type == SLAM_GRAPH_EDGE_T_TYPE_ODOM_INC || type == SLAM_GRAPH_EDGE_T_TYPE_SM_INC){
        //this is an incremental constraint 
        node1->inc_constraint = constraint;
    }

    node1->constraints.insert(pair<int, SlamConstraint *>(node2->id,constraint));
    node2->constraints_from.insert(pair<int, SlamConstraint *>(node1->id,constraint));
    slam_constraints.insert(pair<int, SlamConstraint *>(constraint->id,constraint));
    slam->add_factor(constraint->ct_pose2d);
    no_edges_added++;

    add_edge(node1->b_vertex, node2->b_vertex, Weight(1.0), b_graph);//, Weight(1));
    
    return constraint;
}

void SlamGraph::clearDummyConstraints(){
    map<int, SlamConstraint *>::iterator c_it;
    for ( c_it= failed_slam_constraints.begin() ; c_it != failed_slam_constraints.end(); c_it++ ){        
        delete c_it->second;
    }
    failed_slam_constraints.clear();
}

void SlamGraph::addDummyConstraint(SlamNode *node1,  SlamNode *node2, SlamNode *actualnode1, 
                                   SlamNode *actualnode2, Pose2d transform, Noise noise, 
                                   double hit_pct, int32_t type, int32_t status){
    int id = failed_slam_constraints.size();
    SlamConstraint *constraint = new SlamConstraint(id, node1, node2, actualnode1, actualnode2, 
                                                    transform, noise, hit_pct, 0, type, status);
    //node1->constraints.insert(pair<int, SlamConstraint *>(node2->id, constraint));
    failed_slam_constraints.insert(pair<int, SlamConstraint *>(id,constraint));
    //no_edges_added++;
}

void SlamGraph::addDummyConstraint(int id, SlamNode *node1,  SlamNode *node2, SlamNode *actualnode1, 
                                   SlamNode *actualnode2, Pose2d transform, Noise noise, 
                                   double hit_pct, int32_t type, int32_t status){
    SlamConstraint *constraint = new SlamConstraint(id, node1, node2, actualnode1, actualnode2, 
                                                    transform, noise, hit_pct, 0, type, status);
    //node1->constraints.insert(pair<int, SlamConstraint *>(node2->id, constraint));
    failed_slam_constraints.insert(pair<int, SlamConstraint *>(id,constraint));
    //no_edges_added++;
}

int SlamGraph::hasConstraint(int const_id){// id_1, int id_2){
    map<int, SlamConstraint *>::iterator it; 
    it = slam_constraints.find(const_id);//->second;
    
    if(it == slam_constraints.end())
        return 0;

    SlamConstraint *ct = it->second;

    if(ct==NULL) //this shouldnt be
        return 0;

    return 1; 
}

int SlamGraph::hasConstraint(int id_1, int id_2, int32_t type){
    SlamNode *node1 = getSlamNodeFromID(id_1);
    SlamNode *node2 = getSlamNodeFromID(id_2);

    map<int, SlamConstraint *>::iterator it; 
    it = node1->constraints.find(id_2);//->second;
    
    if(it == node1->constraints.end()){
        return 0;
    }

    SlamConstraint *ct = it->second;

    if(ct==NULL) //this shouldnt be
        return 0;
    
    if(ct->type != type)
        return 0;
    
    return 1; 
}

int SlamGraph::hasConstraint(int id_1, int id_2){
    SlamNode *node1 = getSlamNodeFromID(id_1);
    SlamNode *node2 = getSlamNodeFromID(id_2);

    map<int, SlamConstraint *>::iterator it; 
    it = node1->constraints.find(id_2);//->second;
    
    if(it == node1->constraints.end())
        return 0;

    SlamConstraint *ct = it->second;

    if(ct==NULL) //this shouldnt be
        return 0;
    
    return 1; 
}

int SlamGraph::getConstraintID(int id_1, int id_2, int32_t type){
    SlamNode *node1 = getSlamNodeFromID(id_1);
    SlamNode *node2 = getSlamNodeFromID(id_2);

    map<int, SlamConstraint *>::iterator it; 
    it = node1->constraints.find(id_2);//->second;
    
    if(it == node1->constraints.end())
        return -1;

    SlamConstraint *ct = it->second;

    if(ct==NULL) //this shouldnt be
        return -1;
    
    if(ct->type != type)
        return -1;
    
    return ct->id; 
}

int SlamGraph::removeConstraint(int id_1, int id_2, int32_t type){
    SlamNode *node1 = getSlamNodeFromID(id_1);
    SlamNode *node2 = getSlamNodeFromID(id_2);

    map<int, SlamConstraint *>::iterator it; 
    it = node1->constraints.find(id_2);//->second;
    
    if(it == node1->constraints.end())
        return 0;

    
    SlamConstraint *ct = it->second;

    if(ct==NULL) //this shouldnt be
        return 0;

    if(ct->type != type)
        return 0;

    node1->constraints.erase(it);

    it = node2->constraints_from.find(id_1);
    node2->constraints_from.erase(it);

    it = slam_constraints.find(ct->id); 
    slam_constraints.erase(it);

    if(type == SLAM_GRAPH_EDGE_T_TYPE_LANGUAGE && ct->ct_pose2d != NULL){
        slam->remove_factor(ct->ct_pose2d);
    }
    else if(type == SLAM_GRAPH_EDGE_T_TYPE_SM_LC && ct->ct_pose2d != NULL){
        slam->remove_factor(ct->ct_pose2d);
    }

    delete ct; 

    return 0;
}

int SlamGraph::removeConstraint(int const_id){
    map<int, SlamConstraint *>::iterator it; 
    it = slam_constraints.find(const_id);
    
    if(it == slam_constraints.end())
        return 0;

    SlamConstraint *ct = it->second;

    if(ct==NULL){ //this shouldnt be
        return 0;
    }
    
    SlamNode *node1 = ct->node1;                
    int id_1 = node1->id;
    int id_2 = ct->node2->id;

    if (ct->ct_pose2d)
        slam->remove_factor(ct->ct_pose2d);

    slam_constraints.erase(it);

    it = node1->constraints.find(id_2);//->second;
    
    if(it == node1->constraints.end()){
        return 0;
    }

    node1->constraints.erase(it);
    
    it = ct->node2->constraints_from.find(id_1);
    ct->node2->constraints_from.erase(it);

    
    
    delete ct; 

    return 0;
}

int SlamGraph::hasConstraint(int const_id, int32_t *type, int32_t *status){
    map<int, SlamConstraint *>::iterator it; 
    it = slam_constraints.find(const_id);
    
    if(it == slam_constraints.end()){
        *type = -1;
        *status = -1;
        return 0;
    }

    SlamConstraint *ct = it->second;

    if(ct==NULL){ //this shouldnt be
        *type = -1;
        *status = -1;
        return 0;
    }
    
    *type = ct->type;
    *status = ct->status;
    return 1;    
}

SlamConstraint * SlamGraph::getConstraint(int const_id){
    map<int, SlamConstraint *>::iterator it; 
    it = slam_constraints.find(const_id);

    if(it == slam_constraints.end()){
        return NULL;
    }

    return it->second;    
}
