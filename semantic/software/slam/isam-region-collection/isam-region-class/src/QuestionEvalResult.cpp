#include "SlamGraph.hpp"
#include "Utils.hpp"

questionEvalResult::questionEvalResult(int _event_id, spatialRelation _sr, SlamNode *_q_node, OutputWriter *_output_writer):
    event_id(_event_id), sr(_sr), q_node(_q_node), output_writer(_output_writer)
{}

/*double questionEvalResult::get_expected_information(){
  double orig_entropy = get_entropy(prior_distribution);

  answer_likelihood.clear();
    
  map<int, answerResult>::iterator it; 
  set<int>::iterator it_set;

  double prob_invalid = 0; 
  map<int, map<int, double> > answer_map; 
  map<int, double> valid_map; 

  map<int, map<int, double> >::iterator it_a; 
  map<int, double>::iterator it_answer; 

  map<int, double> a_valid; 
  map<int, double> a_invalid; 

  for(it_set = answer_set.begin(); it_set != answer_set.end(); it++){
  a_valid.insert(make_pair(*it_set, 0)); 
  a_invalid.insert(make_pair(*it_set, 0)); 
  }

  answer_map.insert(make_pair(0, a_valid));
  answer_map.insert(make_pair(1, a_invalid));

  for(it = answer_result_map.begin(); it != answer_result_map.end(); it++){
  answerResult res = it->second; 
  prob_invalid += res.prior * res.prob_invalid; 
        
        
  }

  valid_map.insert(make_pair(0, (1-prob_invalid))); 
  valid_map.insert(make_pair(1, (prob_invalid))); 

  normalize_map(answer_likelihood);
  map<int, double>::iterator it;
  map<int, map<int, double> >::iterator it_new_given_answer;

  double expected_new_entropy = 0; 
  for(it = answer_likelihood.begin(); it != answer_likelihood.end(); it++){
  double p_answer = it->second;
  it_new_given_answer = new_distribution.find(it->first); 
  if(it_new_given_answer == new_distribution.end()){
  fprintf(stderr, "Error - no new distribution found for the answer\n");
  exit(-1); 
  }
  else{
  double new_entropy_given_answer = get_entropy(it_new_given_answer->second);
  expected_new_entropy += new_entropy_given_answer * p_answer; 
  }
  }
  double information = orig_entropy - expected_new_entropy;
  sprintf(buf, "[Question Eval] [Eval SR] Event : %d (Q : %d) SR : %s Original Entropy : %f - New Entropy : %f -> Information : %f", event_id, (int) q_node->id, 
  Utils::getSR(sr).c_str(), orig_entropy, expected_new_entropy, information);
  output_writer->write_to_buffer(buf);
  //fprintf(stdout, "Original Entropy : %f - New Entropy : %f -> Information : %f\n", 
  //orig_entropy, expected_new_entropy, information);
  return information; 
  }*/

//Option 1 : Can result in any SR being treated as high info gain 
double questionEvalResult::get_expected_information_method_1(){
    double orig_entropy = get_entropy(prior_distribution);
    
    //calculate the likelihood of getting valid answers 

    //also calculate the entropy of the new regions - if we get yes or no answers 
    map<int, answerResult>::iterator it; 
    map<int, map<int, double> > answer_distributions; //if valid - the distributions if we get a particular answer  
    set<int>::iterator it_set;

    map<int, double>::iterator it_answer; 
    map<int, double>::iterator it_prior; 

    map<int, double> answer_entropy; 
    map<int, double> answer_information; 
    map<int, double> answer_valid; 

    double prob_valid = 0; 
    for(it = answer_result_map.begin(); it != answer_result_map.end(); it++){
        answerResult res = it->second; 
        double prior = 0; 

        it_prior = prior_distribution.find(it->first);
        if(it_prior != prior_distribution.end()){
            prior = it_prior->second; 
        }
        prob_valid += prior * (1 - res.prob_invalid); 
    }

    sr_likelihood.clear(); 

    map<int, map<int, double> >::iterator it_sr_result; 

    for(it = answer_result_map.begin(); it != answer_result_map.end(); it++){
        map<int, double> prob_answer_given_region; 
        sr_likelihood.insert(make_pair(it->first, prob_answer_given_region)); 
    }

    //this is only if its valid 
    for(it_set = answer_set.begin(); it_set != answer_set.end(); it_set++){
        map<int, double> dist_map; 

        double prob_answer_given_valid = 0; 
        
        //we should look at the 

        for(it = answer_result_map.begin(); it != answer_result_map.end(); it++){
            answerResult res = it->second; 
            
            it_sr_result = sr_likelihood.find(it->first);             

            it_answer = res.answer_likelihood.find(*it_set); 
            
            double prior = 0; 

            it_prior = prior_distribution.find(it->first);
            if(it_prior != prior_distribution.end()){
                prior = it_prior->second; 
            }
            
            if(it_answer != res.answer_likelihood.end()){                
                double prob_answer_and_valid = it_answer->second * prior * (1 - res.prob_invalid);    
                //dist_map.insert(make_pair(it->first, it_answer->second * prior));//prob_answer_and_valid));
                dist_map.insert(make_pair(it->first, it_answer->second * prior * prob_answer_and_valid)); //option 1 is to assume that this tells us about the validity as well 
                prob_answer_given_valid += prob_answer_and_valid;

                sprintf(buf, "[Question Eval] [Eval SR] [Prob] Event : %d (Q : %d) SR : %s Answer : %s, Region : %d Prob Answer : %.2f - Prob Valid : %.2f Prob Region : %.2f => Prob Answer given Valid: %.2f ", 
                        event_id, (int) q_node->id, Utils::getSR(sr).c_str(), Utils::getAnswerString(*it_set).c_str(), 
                        it->first, it_answer->second,  (1 - res.prob_invalid), prior, it_answer->second * prior);//prob_answer_and_valid);
                output_writer->write_to_buffer(buf);

                it_sr_result->second.insert(make_pair(*it_set, it_answer->second * (1 - res.prob_invalid)));
            } 
            else{
                fprintf(stderr, "Error - Not found\n");
            }
        }
        
        //get the entropy for this answer 
        double entropy_answer = get_entropy(dist_map); 

        answer_entropy.insert(make_pair(*it_set, entropy_answer)); 
        double information_gain = orig_entropy - entropy_answer; 

        sprintf(buf, "[Question Eval] [Eval SR] [Info] Event : %d (Q : %d) SR : %s Answer : %s, Prob Answer_Given_valid : %.2f Entropy : %.2f Info Gain : %.2f", 
                event_id, (int) q_node->id, Utils::getSR(sr).c_str(), Utils::getAnswerString(*it_set).c_str(), prob_answer_given_valid, entropy_answer, information_gain); 
        output_writer->write_to_buffer(buf);

        answer_information.insert(make_pair(*it_set, information_gain)); 
        answer_valid.insert(make_pair(*it_set, prob_answer_given_valid)); 
    }
    
    normalize_map(answer_valid); //this is the likelihood of an answer given the question was valid 
    
    double expected_info_gain = 0; 

    map<int, double>::iterator it_info; 
    for(it_answer = answer_valid.begin(); it_answer != answer_valid.end(); it_answer++){
        it_info = answer_information.find(it_answer->first); 
        
        expected_info_gain += it_answer->second * it_info->second; 
    }

    expected_info_gain *= prob_valid; 

    sprintf(buf, "[Question Eval] [Eval SR] [Final] Event : %d (Q : %d) SR : %s Original Entropy : %f Prob Valid : %f -> Expected Information : %f", event_id, (int) q_node->id, 
            Utils::getSR(sr).c_str(), orig_entropy, prob_valid, expected_info_gain);
    output_writer->write_to_buffer(buf);
    
    return expected_info_gain; 
}

//Option 2 : Can result in the peaked ones being ignored - because some are invalid 
double questionEvalResult::get_expected_information_method_2(){
    double orig_entropy = get_entropy(prior_distribution);
    
    //calculate the likelihood of getting valid answers 

    //also calculate the entropy of the new regions - if we get yes or no answers 
    map<int, answerResult>::iterator it; 
    map<int, map<int, double> > answer_distributions; //if valid - the distributions if we get a particular answer  
    set<int>::iterator it_set;

    map<int, double>::iterator it_answer; 
    map<int, double>::iterator it_prior; 

    map<int, double> answer_entropy; 
    map<int, double> answer_information; 
    map<int, double> answer_valid; 

    double prob_valid = 0; 
    for(it = answer_result_map.begin(); it != answer_result_map.end(); it++){
        answerResult res = it->second; 
        double prior = 0; 

        it_prior = prior_distribution.find(it->first);
        if(it_prior != prior_distribution.end()){
            prior = it_prior->second; 
        }
        prob_valid += prior * (1 - res.prob_invalid); 
    }

    sr_likelihood.clear(); 

    map<int, map<int, double> >::iterator it_sr_result; 

    for(it = answer_result_map.begin(); it != answer_result_map.end(); it++){
        map<int, double> prob_answer_given_region; 
        sr_likelihood.insert(make_pair(it->first, prob_answer_given_region)); 
    }

    //this is only if its valid 
    for(it_set = answer_set.begin(); it_set != answer_set.end(); it_set++){
        map<int, double> dist_map; 

        double prob_answer_given_valid = 0; 
        
        //we should look at the 

        for(it = answer_result_map.begin(); it != answer_result_map.end(); it++){
            answerResult res = it->second; 
            
            it_sr_result = sr_likelihood.find(it->first);             

            it_answer = res.answer_likelihood.find(*it_set); 
            
            double prior = 0; 

            it_prior = prior_distribution.find(it->first);
            if(it_prior != prior_distribution.end()){
                prior = it_prior->second; 
            }
            
            if(it_answer != res.answer_likelihood.end()){                
                double prob_answer_and_valid = it_answer->second * prior * (1 - res.prob_invalid);    
                dist_map.insert(make_pair(it->first, it_answer->second * prior));//prob_answer_and_valid));
                //dist_map.insert(make_pair(it->first, it_answer->second * prior * prob_answer_and_valid)); //option 1 is to assume that this tells us about the validity as well 
                prob_answer_given_valid += prob_answer_and_valid;

                sprintf(buf, "[Question Eval] [Eval SR] [Prob] Event : %d (Q : %d) SR : %s Answer : %s, Region : %d Prob Answer : %.2f - Prob Valid : %.2f Prob Region : %.2f => Prob Answer given Valid: %.2f ", 
                        event_id, (int) q_node->id, Utils::getSR(sr).c_str(), Utils::getAnswerString(*it_set).c_str(), 
                        it->first, it_answer->second,  (1 - res.prob_invalid), prior, it_answer->second * prior);//prob_answer_and_valid);
                output_writer->write_to_buffer(buf);

                it_sr_result->second.insert(make_pair(*it_set, it_answer->second * (1 - res.prob_invalid)));
            } 
            else{
                fprintf(stderr, "Error - Not found\n");
            }
        }
        
        //get the entropy for this answer 
        double entropy_answer = get_entropy(dist_map); 

        answer_entropy.insert(make_pair(*it_set, entropy_answer)); 
        double information_gain = orig_entropy - entropy_answer; 

        sprintf(buf, "[Question Eval] [Eval SR] [Info] Event : %d (Q : %d) SR : %s Answer : %s, Prob Answer_Given_valid : %.2f Entropy : %.2f Info Gain : %.2f", 
                event_id, (int) q_node->id, Utils::getSR(sr).c_str(), Utils::getAnswerString(*it_set).c_str(), prob_answer_given_valid, entropy_answer, information_gain); 
        output_writer->write_to_buffer(buf);

        answer_information.insert(make_pair(*it_set, information_gain)); 
        answer_valid.insert(make_pair(*it_set, prob_answer_given_valid)); 
    }
    
    normalize_map(answer_valid); //this is the likelihood of an answer given the question was valid 
    
    double expected_info_gain = 0; 

    map<int, double>::iterator it_info; 
    for(it_answer = answer_valid.begin(); it_answer != answer_valid.end(); it_answer++){
        it_info = answer_information.find(it_answer->first); 
        
        expected_info_gain += it_answer->second * it_info->second; 
    }

    expected_info_gain *= prob_valid; 

    sprintf(buf, "[Question Eval] [Eval SR] [Final] Event : %d (Q : %d) SR : %s Original Entropy : %f Prob Valid : %f -> Expected Information : %f", event_id, (int) q_node->id, 
            Utils::getSR(sr).c_str(), orig_entropy, prob_valid, expected_info_gain);
    output_writer->write_to_buffer(buf);
    
    return expected_info_gain; 
}

double questionEvalResult::get_expected_information_method_3(){
    //double orig_entropy = get_entropy(prior_distribution);
    //calculate the likelihood of getting valid answers 

    //also calculate the entropy of the new regions - if we get yes or no answers 
    map<int, answerResult>::iterator it; 
    map<int, map<int, double> > answer_distributions; //if valid - the distributions if we get a particular answer  
    set<int>::iterator it_set;

    map<int, double>::iterator it_answer; 
    map<int, double>::iterator it_prior; 

    map<int, double> answer_entropy; 
    map<int, double> answer_information; 
    map<int, double> answer_valid; 

    double prob_valid = 0; 
    for(it = answer_result_map.begin(); it != answer_result_map.end(); it++){
        answerResult res = it->second; 
        double prior = 0; 

        it_prior = prior_distribution.find(it->first);
        if(it_prior != prior_distribution.end()){
            prior = it_prior->second; 
        }
        prob_valid += prior * (1 - res.prob_invalid); 
    }

    sr_likelihood.clear(); 

    map<int, map<int, double> >::iterator it_sr_result; 

    for(it = answer_result_map.begin(); it != answer_result_map.end(); it++){
        map<int, double> prob_answer_given_region; 
        sr_likelihood.insert(make_pair(it->first, prob_answer_given_region)); 
    }

    //this is only if its valid 
    for(it_set = answer_set.begin(); it_set != answer_set.end(); it_set++){
        map<int, double> dist_map; 

        double prob_answer_given_valid = 0; 
        
        //we should look at the 

        map<int, double> original_map; 

        for(it = answer_result_map.begin(); it != answer_result_map.end(); it++){
            answerResult res = it->second; 
            
            it_sr_result = sr_likelihood.find(it->first);             

            it_answer = res.answer_likelihood.find(*it_set); 
            
            double prior = 0; 

            it_prior = prior_distribution.find(it->first);
            if(it_prior != prior_distribution.end()){
                prior = it_prior->second; 
            }
            
            if(it_answer != res.answer_likelihood.end()){                
                if(res.prob_invalid < 1.0){
                    original_map.insert(make_pair(it->first, prior)); 
                    double prob_answer_and_valid = it_answer->second * prior * (1 - res.prob_invalid);    
                    dist_map.insert(make_pair(it->first, it_answer->second * prior));//prob_answer_and_valid));
                    //dist_map.insert(make_pair(it->first, it_answer->second * prior * prob_answer_and_valid)); //option 1 is to assume that this tells us about the validity as well 
                    prob_answer_given_valid += prob_answer_and_valid;

                    sprintf(buf, "[Question Eval] [Eval SR] [Prob] [Valid] Event : %d (Q : %d) SR : %s Answer : %s, Region : %d Prob Answer : %.2f - Prob Valid : %.2f Prob Region : %.2f => Prob Answer given Valid: %.2f ", 
                            event_id, (int) q_node->id, Utils::getSR(sr).c_str(), Utils::getAnswerString(*it_set).c_str(), 
                            it->first, it_answer->second,  (1 - res.prob_invalid), prior, it_answer->second * prior);//prob_answer_and_valid);
                    output_writer->write_to_buffer(buf);

                    it_sr_result->second.insert(make_pair(*it_set, it_answer->second * (1 - res.prob_invalid)));
                }
                else{
                    sprintf(buf, "[Question Eval] [Eval SR] [Prob] [Invalid] Event : %d (Q : %d) SR : %s Answer : %s, Region : %d Prob Answer : %.2f - Prob Valid : %.2f Prob Region : %.2f", 
                            event_id, (int) q_node->id, Utils::getSR(sr).c_str(), Utils::getAnswerString(*it_set).c_str(), 
                            it->first, it_answer->second,  (1 - res.prob_invalid), prior); 
                    output_writer->write_to_buffer(buf);
                }
            } 
            else{
                fprintf(stderr, "Error - Not found\n");
            }
        }
        
        //get the entropy for this answer 
        double entropy_answer = get_entropy(dist_map); 
        
        double orig_entropy = get_entropy(original_map); 

        answer_entropy.insert(make_pair(*it_set, entropy_answer)); 
        double information_gain = orig_entropy - entropy_answer; 

        sprintf(buf, "[Question Eval] [Eval SR] [Info] Event : %d (Q : %d) SR : %s Answer : %s, Prob Answer_Given_valid : %.2f Orig Entropy : %.2fEntropy : %.2f Info Gain : %.2f", 
                event_id, (int) q_node->id, Utils::getSR(sr).c_str(), Utils::getAnswerString(*it_set).c_str(), prob_answer_given_valid, orig_entropy, entropy_answer, information_gain); 
        output_writer->write_to_buffer(buf);

        answer_information.insert(make_pair(*it_set, information_gain)); 
        answer_valid.insert(make_pair(*it_set, prob_answer_given_valid)); 
    }
    
    normalize_map(answer_valid); //this is the likelihood of an answer given the question was valid 
    
    double expected_info_gain = 0; 

    map<int, double>::iterator it_info; 
    for(it_answer = answer_valid.begin(); it_answer != answer_valid.end(); it_answer++){
        it_info = answer_information.find(it_answer->first); 
        
        expected_info_gain += it_answer->second * it_info->second; 
    }

    expected_info_gain *= prob_valid; 

    sprintf(buf, "[Question Eval] [Eval SR] [Final] Event : %d (Q : %d) SR : %s Prob Valid : %f -> Expected Information : %f", event_id, (int) q_node->id, 
            Utils::getSR(sr).c_str(), prob_valid, expected_info_gain);
    output_writer->write_to_buffer(buf);
    
    return expected_info_gain; 
}

double questionEvalResult::get_expected_information(){
    return get_expected_information_method_3(); 
}

void questionEvalResult::add_region(int region_id, double prior, double prob_invalid, map<int, double> answer_likelihood_region, string prefix){
    //sr_likelihood.insert(make_pair(region_id, answer_likelihood_region)); 

    answerResult res; 

    res.prob_invalid = prob_invalid; 
    res.prior = prior; 
    res.answer_likelihood = answer_likelihood_region; 
    
    answer_result_map.insert(make_pair(region_id, res));

    map<int, double>::iterator it;
    for(it = answer_likelihood_region.begin(); it != answer_likelihood_region.end(); it++){
        answer_set.insert(it->first);
    }
    
    prior_distribution.insert(make_pair(region_id, prior));
    /*map<int, double>::iterator it;
      map<int, double>::iterator it_answer;
      map<int, map<int,double> >::iterator it_new_answer;
      for(it = answer_likelihood_region.begin(); it != answer_likelihood_region.end(); it++){
      it_answer = answer_likelihood.find(it->first);
      if(it_answer != answer_likelihood.end()){
      it_answer->second += it->second * prior * (1 - prob_invalid); //update the likelihood
      }
      else{
      //add entry to the map 
      answer_likelihood.insert(make_pair(it->first, it->second * prior * (1 - prob_invalid)));
      }

      //update the posterior for each answer 
      it_new_answer = new_distribution.find(it->first);

      if(it_new_answer == new_distribution.end()){
      map<int, double> r_map; 
      r_map.insert(make_pair(region_id, it->second * prior));
      new_distribution.insert(make_pair(it->first, r_map));
      }
      else{
      it_new_answer->second.insert(make_pair(region_id, it->second * prior));
      }
            
      sprintf(buf, "[Question Eval] %s Event : %d (Q : %d) Region : %d (%.2f) SR %s Answer : %s - Prob : %.2f => New prob : %.2f", 
      prefix.c_str(), event_id, (int) q_node->id, region_id, prior, 
      Utils::getSR(sr).c_str(), Utils::getAnswerString(it->first).c_str(), it->second, it->second * prior);                    
      output_writer->write_to_buffer(buf);

      }        
      //update the prior 
      prior_distribution.insert(make_pair(region_id, prior));*/
}

/*
  void questionEvalResult::add_region(int region_id, double prior, double prob_invalid, map<int, double> answer_likelihood_region, string prefix){
  sr_likelihood.insert(make_pair(region_id, answer_likelihood_region)); 

  map<int, double>::iterator it;
  map<int, double>::iterator it_answer;
  map<int, map<int,double> >::iterator it_new_answer;
  for(it = answer_likelihood_region.begin(); it != answer_likelihood_region.end(); it++){
  it_answer = answer_likelihood.find(it->first);
  if(it_answer != answer_likelihood.end()){
  it_answer->second += it->second * prior; //update the likelihood
  }
  else{
  //add entry to the map 
  answer_likelihood.insert(make_pair(it->first, it->second * prior));
  }

  //update the posterior for each answer 
  it_new_answer = new_distribution.find(it->first);

  if(it_new_answer == new_distribution.end()){
  map<int, double> r_map; 
  r_map.insert(make_pair(region_id, it->second * prior));
  new_distribution.insert(make_pair(it->first, r_map));
  }
  else{
  it_new_answer->second.insert(make_pair(region_id, it->second * prior));
  }
            
  sprintf(buf, "[Question Eval] %s Event : %d (Q : %d) Region : %d (%.2f) SR %s Answer : %s - Prob : %.2f => New prob : %.2f", 
  prefix.c_str(), event_id, (int) q_node->id, region_id, prior, 
  Utils::getSR(sr).c_str(), Utils::getAnswerString(it->first).c_str(), it->second, it->second * prior);                    
  output_writer->write_to_buffer(buf);

  }        
  //update the prior 
  prior_distribution.insert(make_pair(region_id, prior));
  }

*/

//normalize 
void questionEvalResult::normalize_map(map<int, double> &p_map) const{
    map<int, double>::iterator it;
    double sum = 0; 
    for(it = p_map.begin(); it != p_map.end(); it++){
        sum += it->second;
    }
    if(sum > 0){
        for(it = p_map.begin(); it != p_map.end(); it++){
            it->second /= sum;
        }
    }
}

bool questionEvalResult::is_normalized(map<int, double> &p_map) const{
    map<int, double>::iterator it;
    double sum = 0; 
    for(it = p_map.begin(); it != p_map.end(); it++){
        sum += it->second;
    }
    if(sum == 1.0){
        return true;
    }
    return false;
}

double questionEvalResult::get_entropy(map<int, double> &p_map) const{
    if(p_map.size() == 0){
        return 0; 
    }
    map<int, double>::iterator it;
    normalize_map(p_map);

    double p_sum = 0; 
    double sum = 0; 
        
    for(it = p_map.begin(); it != p_map.end(); it++){
        if(it->second > 0.0){
            p_sum += it->second;
            sum += -log(it->second) * it->second; 
        }
    }
    return sum; 
}

/*double questionEvalResult::get_expected_information(){
  double orig_entropy = get_entropy(prior_distribution);
  normalize_map(answer_likelihood);
  map<int, double>::iterator it;
  map<int, map<int, double> >::iterator it_new_given_answer;

  double expected_new_entropy = 0; 
  for(it = answer_likelihood.begin(); it != answer_likelihood.end(); it++){
  double p_answer = it->second;
  it_new_given_answer = new_distribution.find(it->first); 
  if(it_new_given_answer == new_distribution.end()){
  fprintf(stderr, "Error - no new distribution found for the answer\n");
  exit(-1); 
  }
  else{
  double new_entropy_given_answer = get_entropy(it_new_given_answer->second);
  expected_new_entropy += new_entropy_given_answer * p_answer; 
  }
  }
  double information = orig_entropy - expected_new_entropy;
  sprintf(buf, "[Question Eval] [Eval SR] Event : %d (Q : %d) SR : %s Original Entropy : %f - New Entropy : %f -> Information : %f", event_id, (int) q_node->id, 
  Utils::getSR(sr).c_str(), orig_entropy, expected_new_entropy, information);
  output_writer->write_to_buffer(buf);
  //fprintf(stdout, "Original Entropy : %f - New Entropy : %f -> Information : %f\n", 
  //orig_entropy, expected_new_entropy, information);
  return information; 
  }*/   


map<int, map<int,double> > questionEvalResult::get_sr_answer_map() const{
    return sr_likelihood;
}
