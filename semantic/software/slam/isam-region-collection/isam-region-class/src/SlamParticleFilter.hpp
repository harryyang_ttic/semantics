#ifndef SLAMPARTICLE_FILER_H_
#define SLAMPARTICLE_FILER_H_

#include "SlamParticle.hpp"
#include <iostream>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

#include <opencv/highgui.h>
#include <getopt.h>

#include <lcm/lcm.h>
#include <bot_core/bot_core.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_lcmgl_client/lcmgl.h>

#include <gsl/gsl_blas.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_sf_bessel.h>
#include <gsl/gsl_integration.h>
#include <math.h>

#include  <classify-semantic/classify_semantic.hpp>
#include <scanmatch/ScanMatcher.hpp>
#include <lcmtypes/sm_rigid_transform_2d_t.h>
#include <bot_frames/bot_frames.h>
#include <lcmtypes/er_lcmtypes.h>

#include <isam/isam.h>
#include <isam/Anchor.h>

#include <occ_map/PixelMap.hpp>
#include <lcmtypes/slam_lcmtypes.h>
#include <lcmtypes/perception_object_detection_list_t.h>

#include "NodeScan.h"
#include <lcmtypes/slam_lcmtypes.h>
#include "SlamGraph.hpp"
#include <er_common/path_util.h>

#define QUESTION_INVALID_AFTER_NODES 5

#include <door_detector_laser/door_detect_laser.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif 

typedef enum { print_node_info, print_particle_info, print_language_info} verbose_level_t;

struct region_mapping_t {
    int particle_id; //id of the unique particle 
    map<int, int> region_map;
};

typedef struct _received_language_t{
    int64_t node_id;
    slam_language_label_t *label;
} received_language_t;    

typedef enum { no_question_sent, question_sent, answer_received} question_state_t;

struct question_info_t {
    //bool outstanding_question;
    question_state_t state; 
    int64_t question_asked_time; 
    int64_t question_asked_node_id;
    int particle_id; 
    int no_questions_asked; 
};

class SlamParticleFilter{
public:
    
    SlamParticleFilter(lcm_t *lcm_, SLUClassifier *slu_classifier_, const ConfigurationParams &config_params, 
                       bool doSemanticClassification_, string log_path, 
                       char *chan, int probMode, int ignoreLanguage, 
                       int resample, int useCopy, int no_particles);

    ~SlamParticleFilter();

    vector<SlamParticle *> get_unique_particles();
    void publish_max_occupancy_map();

    void count_different_particles();
    int publish_outstanding_language_querries();

    int evaluate_outstanding_language_querries();
    int publish_outstanding_language_querries_all_particles();

    void publish_language_collection_list();
    slam_graph_region_particle_list_t *get_slam_region_particle_msg();

    void publish_slam_region_particles();

    void publish_slam_transforms();

    void publish_map_id(int64_t id);

    void clear_dead_edges();

    int renormalize_particles();

    void update_weights();

    int resample_particles(int64_t utime);

    slam_language_question_t* calculate_best_question(SlamParticle *max_particle);

    void publish_slampose_list();
    void send_clear_dialog_msg();

    int process_for_nodes_and_language(int64_t utime, vector<NodeScan *> new_nodes, 
                                       map<int, slam_language_label_t *> new_language_labels, 
                                       vector<complex_language_event_t *> complex_language, 
                                       vector<slam_language_answer_t *> sr_answers, bool finish_slam);

    bool is_valid_id(int64_t id);

    vector<SlamParticle *> slam_particles;
    map<int, SlamParticle *> slam_particles_map;
    map<int, region_mapping_t> particle_to_region_map;
    map<int, set<int> > unique_particle_to_duplicates;  //the mapping from unique particle to duplicates  
    //used for grounding language for the duplicate particles 
    SLUClassifier *slu_classifier; 
    const ConfigurationParams config_params;
    bool doSemanticClassification;
    lcm_t * lcm;

    vector<complex_language_event_t *> outstanding_complex_language; 
    vector<complex_language_event_t *> failed_complex_language; 
    vector<complex_language_event_t *> grounded_language; 

    vector<received_language_t *> grounded_simple_language; 

    slam_graph_weight_list_t *weights;

    int verbose;
    LabelInfo *label_info; 
    string log_path; 
    FloatPixelMap *local_px_map; // Local occupancy map used for measurement likelihood

    vector<NodeScan *> added_slam_nodes;
    LabelInfo *label_info_particles; 
    BotFrames *frames;
    BotParam *param;
    char * chan;
    OutputWriter *output_writer; 

    bot_lcmgl_t * lcmgl_sm_basic;
    bot_lcmgl_t * lcmgl_sm_graph;
    bot_lcmgl_t * lcmgl_sm_prior;
    bot_lcmgl_t * lcmgl_sm_result;
    bot_lcmgl_t * lcmgl_sm_result_low;
    bot_lcmgl_t * lcmgl_loop_closures;
    bot_lcmgl_t * lcmgl_frame_test;
    int next_id;

    set<pair<int, int> > *dist_lc, *lang_lc;
    int verbose_level;

    int64_t last_graph_utime; //update from particle last_node_utime
    int64_t last_added_node_id; 

    question_info_t q_info;
    int probMode;
    int find_ground_truth;
    int ignoreLanguage;

    //vector<ObjectDetection *> object_detections; 
    map<int, ObjectDetection *> object_map; 
    bool finishSlam; //for now this will only force us to scanmatch ther current region 
    GMutex *mutex_slu;
    int waiting_for_slu; 
    int max_prob_particle_id;
    int resample;
    int useCopy;

    int no_particles;
};

#endif
