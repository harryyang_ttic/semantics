#include "SlamGraph.hpp"
#include "Utils.hpp"

using namespace Utils;

//redo the factors 

//the terms are not properly normalized 

//both the appearance to category P(C/A) => we assume that each apperance is equally likely 
//otherwise we can attach a prior

//and the category to label =? P(L/C)

//LabelInfo Class needs to contain the information to create the following factors 

//P(C/A) Prob of category given appearance - [[Done]]
//lets store as maps?? 
//map<<string, string>, double> p_category_given_appearance 
//P(C/O) Prob of category given object - [[Done]]
//map< <string, string>, double> p_category_given_object 
//P(C/\Lambda) Prob of category given \Lambda - And prior over \Lambda ?? - otherwise there will be issues with synonyms - unless we do not connect it?? 
//map<<string, string>, double> p_lambda_given_lambda
//P(A/l_a) Prob of appearance given laser appearance  - [[Done]] 
//map<<string, string>, double> p_appearance_given_laser_appearance
//P(A/i_a) Prob of appearance given image appearance  - [[Done]]
//map<<string, string>, double> p_appearance_given_image_appearance
//P(\Lambda/\lambda, \phi) Prob of \Lambda given \lambda and \phi (heard label and correspondance) 
//map<<string, string>, double> p_Lambda_given_lambda_and_true_phi


LabelInfo::LabelInfo(){

    //in the conditional distributions the first string is the conditioner (the one that is given) 
    verbose = false;
    
    ////////////////////////////////////////////////////////////////////////////////////

    appearance_to_index.insert(make_pair("room", 0));
    appearance_to_index.insert(make_pair("hallway", 1));
    appearance_to_index.insert(make_pair("openarea", 2));

    //fill the prob of appearance given laser appearance 
    //conditioned variable in the front 
    //i.e. laser appearance, appearance
    
    //for now we will use the inverted values 
    
    p_appearance_given_laser.insert(make_pair(make_pair("room", "room"), 0.7));
    p_appearance_given_laser.insert(make_pair(make_pair("room", "openarea"),  0.15));
    p_appearance_given_laser.insert(make_pair(make_pair("room", "hallway"),  0.15));

    p_appearance_given_laser.insert(make_pair(make_pair("openarea", "room"),  0.15));
    p_appearance_given_laser.insert(make_pair(make_pair("openarea", "openarea"), 0.7));
    p_appearance_given_laser.insert(make_pair(make_pair("openarea", "hallway"),  0.15));

    p_appearance_given_laser.insert(make_pair(make_pair("hallway", "room"),  0.15));
    p_appearance_given_laser.insert(make_pair(make_pair("hallway", "openarea"),  0.15));
    p_appearance_given_laser.insert(make_pair(make_pair("hallway", "hallway"),  0.7));

    //fill the prob of appearance given image appearance 
    //for now we will use the inverted values 
    p_appearance_given_image.insert(make_pair(make_pair("room", "room"),  0.784314));
    p_appearance_given_image.insert(make_pair(make_pair("room", "openarea"), 0.045752));
    p_appearance_given_image.insert(make_pair(make_pair("room", "hallway"), 0.169935));

    p_appearance_given_image.insert(make_pair(make_pair("openarea", "room"),  0.012422));
    p_appearance_given_image.insert(make_pair(make_pair("openarea", "openarea"), 0.906832));
    p_appearance_given_image.insert(make_pair(make_pair("openarea", "hallway"),  0.080745));

    p_appearance_given_image.insert(make_pair(make_pair("hallway", "room"),  0.111111));
    p_appearance_given_image.insert(make_pair(make_pair("hallway", "openarea"),  0.066667));
    p_appearance_given_image.insert(make_pair(make_pair("hallway", "hallway"), 0.822222));
    
    //add the categories 
    category_to_index.insert(make_pair("office",0));
    category_to_index.insert(make_pair("lab",1));
    category_to_index.insert(make_pair("conferenceroom",2));
    category_to_index.insert(make_pair("hallway",3));
    category_to_index.insert(make_pair("elevatorlobby",4));
    category_to_index.insert(make_pair("lounge",5));
    category_to_index.insert(make_pair("kitchen",6));

    map<string, int>::iterator it_c;
    for(it_c = category_to_index.begin(); it_c != category_to_index.end(); it_c++){
        index_to_category.insert(make_pair(it_c->second, it_c->first));
    }

    //category_to_index.insert(make_pair("classroom",4));
    //category_to_index.insert(make_pair("elevator",5));
    //category_to_index.insert(make_pair("reception",6));

    //we should read this from a file - xml??
    p_r_appearance_given_node_a.insert(make_pair(make_pair("openarea", "openarea"), 0.9));
    p_r_appearance_given_node_a.insert(make_pair(make_pair("room", "room"), 0.9));
    p_r_appearance_given_node_a.insert(make_pair(make_pair("hallway", "hallway"), 0.9));

    normalizeDistribution(appearance_to_index, appearance_to_index, p_r_appearance_given_node_a);
    
    //put the rest equally - or from data 

    //this is the results calculated from the annotation logs 
    /*
      Appearance : hallway (49)
	Type : hallway -> 0.673469
	Type : kitchen -> 0.183673
	Type : lab -> 0.142857
      Appearance : openarea (41)
	Type : elevatorlobby -> 0.170732
	Type : lab -> 0.512195
	Type : lounge -> 0.317073
      Appearance : room (7)
	Type : conferenceroom -> 0.285714
	Type : office -> 0.714286

     */

    /*p_category_given_appearance.insert(make_pair(make_pair("openarea", "elevatorlobby"), 0.17));
    p_category_given_appearance.insert(make_pair(make_pair("openarea", "lab"), 0.51));
    p_category_given_appearance.insert(make_pair(make_pair("openarea", "lounge"), 0.32));*/

    p_category_given_appearance.insert(make_pair(make_pair("openarea", "elevatorlobby"), 0.2));
    p_category_given_appearance.insert(make_pair(make_pair("openarea", "lab"), 0.35));
    p_category_given_appearance.insert(make_pair(make_pair("openarea", "lounge"), 0.35));
    
    //increased the likelihood of seeing a lab in a room 
    //p_category_given_appearance.insert(make_pair(make_pair("room", "office"), 0.71));
    p_category_given_appearance.insert(make_pair(make_pair("room", "office"), 0.5));
    p_category_given_appearance.insert(make_pair(make_pair("room", "conferenceroom"), 0.29));
    p_category_given_appearance.insert(make_pair(make_pair("room", "lab"), 0.2));
        
    p_category_given_appearance.insert(make_pair(make_pair("hallway", "kitchen"), 0.18));
    p_category_given_appearance.insert(make_pair(make_pair("hallway", "hallway"), 0.67));
    p_category_given_appearance.insert(make_pair(make_pair("hallway", "lab"), 0.15)); 

    //learn this from the annotation data 
    /*p_category_given_appearance.insert(make_pair(make_pair("openarea", "elevatorlobby"), 0.3));
    p_category_given_appearance.insert(make_pair(make_pair("openarea", "lounge"), 0.3));
    p_category_given_appearance.insert(make_pair(make_pair("openarea", "lab"), 0.2));
    
    p_category_given_appearance.insert(make_pair(make_pair("room", "office"), 0.3));
    p_category_given_appearance.insert(make_pair(make_pair("room", "lab"), 0.2));
    p_category_given_appearance.insert(make_pair(make_pair("room", "conferenceroom"), 0.3));
        
    p_category_given_appearance.insert(make_pair(make_pair("hallway", "kitchen"), 0.1));
    p_category_given_appearance.insert(make_pair(make_pair("hallway", "hallway"), 0.5));
    p_category_given_appearance.insert(make_pair(make_pair("hallway", "lab"), 0.2)); */
    //we get hallways in lab spaces as well 

    /*p_category_given_appearance.insert(make_pair(make_pair("openarea", "elevatorlobby"), 0.45));
    p_category_given_appearance.insert(make_pair(make_pair("openarea", "lounge"), 0.45));
    
    p_category_given_appearance.insert(make_pair(make_pair("room", "office"), 0.3));
    p_category_given_appearance.insert(make_pair(make_pair("room", "lab"), 0.3));
    p_category_given_appearance.insert(make_pair(make_pair("room", "conferenceroom"), 0.3));
        
    p_category_given_appearance.insert(make_pair(make_pair("hallway", "kitchen"), 0.2));
    p_category_given_appearance.insert(make_pair(make_pair("hallway", "hallway"), 0.7));*/

    normalizeDistribution(appearance_to_index, category_to_index, p_category_given_appearance);

    //=============== Objects ========================================//   

    vector<pair<string, int> > object_list;
    object_list.push_back(make_pair("monitor",PERCEPTION_OBJECT_DETECTION_T_TYPE_MONITOR));
    object_list.push_back(make_pair("microwave",PERCEPTION_OBJECT_DETECTION_T_TYPE_MICROWAVE));
    object_list.push_back(make_pair("refrigerator",PERCEPTION_OBJECT_DETECTION_T_TYPE_REFRIGERATOR));
    object_list.push_back(make_pair("couch",PERCEPTION_OBJECT_DETECTION_T_TYPE_COUCH));
    object_list.push_back(make_pair("elevatordoor",PERCEPTION_OBJECT_DETECTION_T_TYPE_ELEVATOR_DOOR));    
    object_list.push_back(make_pair("printer",PERCEPTION_OBJECT_DETECTION_T_TYPE_PRINTER));
    //object_list.push_back(make_pair("keyboard",PERCEPTION_OBJECT_DETECTION_T_TYPE_KEYBAORD));
    //object_list.push_back(make_pair("laptop",PERCEPTION_OBJECT_DETECTION_T_TYPE_LAPTOP));
    //object_list.push_back(make_pair("table",PERCEPTION_OBJECT_DETECTION_T_TYPE_TABLE));   
    //object_list.push_back(make_pair("chair",PERCEPTION_OBJECT_DETECTION_T_TYPE_CHAIR));    
    //object_list.push_back(make_pair("cabinet",PERCEPTION_OBJECT_DETECTION_T_TYPE_CABINET));        
    //object_list.push_back(make_pair("sink",PERCEPTION_OBJECT_DETECTION_T_TYPE_SINK));
    //object_list.push_back(make_pair("trashcan",PERCEPTION_OBJECT_DETECTION_T_TYPE_TRASHCAN));
    //object_list.push_back(make_pair("stairs",PERCEPTION_OBJECT_DETECTION_T_TYPE_STAIRS));

    for(int i=0; i < object_list.size(); i++){
        object_id_to_index.insert(make_pair(object_list[i].second, i));
    }

    for(int i=0; i < object_list.size(); i++){
        object_id_to_index.insert(make_pair(i, object_list[i].second));
    }

    for(int i=0; i < object_list.size(); i++){
        object_name_to_index.insert(make_pair(object_list[i].first,i));
    }

    for(int i=0; i < object_list.size(); i++){
        object_index_to_name.insert(make_pair(i, object_list[i].first));
    }

    //P(C/O)
    p_category_given_object.insert(make_pair(make_pair("monitor", "office"), 0.4));
    p_category_given_object.insert(make_pair(make_pair("monitor", "lab"), 0.4));

    p_category_given_object.insert(make_pair(make_pair("printer", "office"), 0.1));
    p_category_given_object.insert(make_pair(make_pair("printer", "hallway"), 0.5));
    p_category_given_object.insert(make_pair(make_pair("printer", "lab"), 0.3));

    p_category_given_object.insert(make_pair(make_pair("microwave", "kitchen"), 0.8));
    p_category_given_object.insert(make_pair(make_pair("microwave", "office"), 0.1));
    
    p_category_given_object.insert(make_pair(make_pair("refrigerator", "kitchen"), 0.7));
    p_category_given_object.insert(make_pair(make_pair("refrigerator", "lab"), 0.2));

    p_category_given_object.insert(make_pair(make_pair("couch", "lounge"), 0.4));
    p_category_given_object.insert(make_pair(make_pair("couch", "lab"), 0.4));
    p_category_given_object.insert(make_pair(make_pair("couch", "office"), 0.1));
    
    p_category_given_object.insert(make_pair(make_pair("elevatordoor", "elevatorlobby"), 0.9));

    fprintf(stderr, "=====================Normalizing Object Category========================\n");
    normalizeDistribution(object_name_to_index, category_to_index, p_category_given_object);

    fprintf(stderr, "\n\n");
    //split the remainder across the other categories 
    
    //=============== Labels =================================================//

    labels_to_index.insert(make_pair("elevatorlobby", 0));
    labels_to_index.insert(make_pair("office", 1));
    labels_to_index.insert(make_pair("lab", 2));
    labels_to_index.insert(make_pair("hallway", 3));
    labels_to_index.insert(make_pair("hall", 4));
    labels_to_index.insert(make_pair("corridor", 5));
    labels_to_index.insert(make_pair("meetingroom", 6));
    labels_to_index.insert(make_pair("conferenceroom", 7));
    labels_to_index.insert(make_pair("kitchen", 8));
    labels_to_index.insert(make_pair("lounge", 9));

    map<string, int>::iterator it_l;
    for(it_l = labels_to_index.begin(); it_l != labels_to_index.end(); it_l++){
        index_to_labels.insert(make_pair(it_l->second, it_l->first));
    }

    //we will use this assuming a uniform category prior - to get the label prior 

    p_label_given_category.insert(make_pair(make_pair("elevatorlobby", "elevatorlobby"), 0.8));

    p_label_given_category.insert(make_pair(make_pair("office", "office"), 0.7));
    p_label_given_category.insert(make_pair(make_pair("office", "lab"), 0.2));

    p_label_given_category.insert(make_pair(make_pair("lab", "lab"), 0.7));
    p_label_given_category.insert(make_pair(make_pair("lab", "office"), 0.2));

    p_label_given_category.insert(make_pair(make_pair("hallway", "hallway"), 0.3));

    p_label_given_category.insert(make_pair(make_pair("hallway", "hall"), 0.3));

    p_label_given_category.insert(make_pair(make_pair("hallway", "corridor"), 0.3));
    
    p_label_given_category.insert(make_pair(make_pair("lounge", "lounge"), 0.9));

    p_label_given_category.insert(make_pair(make_pair("conferenceroom", "conferenceroom"), 0.45));

    p_label_given_category.insert(make_pair(make_pair("conferenceroom", "meetingroom"), 0.45));
    
    p_label_given_category.insert(make_pair(make_pair("kitchen", " kitchen"), 0.9));
      
    normalizeDistribution(category_to_index, labels_to_index, p_label_given_category);

    //category given label is what we need for the factor graph 

    ///The category given label is depricated 

    p_category_given_label.insert(make_pair(make_pair("elevatorlobby", "elevatorlobby"), 0.9));

    p_category_given_label.insert(make_pair(make_pair("office", "office"), 0.7));
    p_category_given_label.insert(make_pair(make_pair("office", "lab"), 0.2));

    p_category_given_label.insert(make_pair(make_pair("lab", "lab"), 0.7));
    p_category_given_label.insert(make_pair(make_pair("lab", "office"), 0.2));

    p_category_given_label.insert(make_pair(make_pair("hallway", "hallway"), 0.9));

    p_category_given_label.insert(make_pair(make_pair("hall", "hallway"), 0.9));

    p_category_given_label.insert(make_pair(make_pair("corridor", "hallway"), 0.9));
    
    p_category_given_label.insert(make_pair(make_pair("lounge", "lounge"), 0.9));

    p_category_given_label.insert(make_pair(make_pair("conferenceroom", "conferenceroom"), 0.9));

    p_category_given_label.insert(make_pair(make_pair("conferenceroom", "meetingroom"), 0.9));
    
    p_category_given_label.insert(make_pair(make_pair("kitchen", " kitchen"), 0.9));

    fprintf(stderr, "=====================Normalizing Label Category========================\n");
    normalizeDistribution(labels_to_index, category_to_index, p_category_given_label);
    fprintf(stderr, "\n\n");

    //prob of label distribution given heard label and phi = 1
    

    /*p_label_given_label_h.insert(make_pair(make_pair("kitchen", "kitchen"), 0.9));

    p_label_given_label_h.insert(make_pair(make_pair("office", "office"), 0.9));

    p_label_given_label_h.insert(make_pair(make_pair("lab", "lab"), 0.9));

    p_label_given_label_h.insert(make_pair(make_pair("elevatorlobby", "elevatorlobby"), 0.9));

    p_label_given_label_h.insert(make_pair(make_pair("hall", "hall"), 0.9));

    p_label_given_label_h.insert(make_pair(make_pair("hallway", "hallway"), 0.9));

    p_label_given_label_h.insert(make_pair(make_pair("corridor", "corridor"), 0.9));

    p_label_given_label_h.insert(make_pair(make_pair("meetingroom", "meetingroom"), 0.9));

    p_label_given_label_h.insert(make_pair(make_pair("conferenceroom", "conferenceroom"), 0.9));

    p_label_given_label_h.insert(make_pair(make_pair("lounge", "lounge"), 0.9));*/

    p_label_given_label_h.insert(make_pair(make_pair("kitchen", "kitchen"), 0.9));

    p_label_given_label_h.insert(make_pair(make_pair("office", "office"), 0.6));
    p_label_given_label_h.insert(make_pair(make_pair("office", "lab"), 0.3));

    p_label_given_label_h.insert(make_pair(make_pair("lab", "office"), 0.3));
    p_label_given_label_h.insert(make_pair(make_pair("lab", "lab"), 0.6));

    p_label_given_label_h.insert(make_pair(make_pair("elevatorlobby", "elevatorlobby"), 0.9));

    p_label_given_label_h.insert(make_pair(make_pair("hall", "hall"), 0.5));
    p_label_given_label_h.insert(make_pair(make_pair("hall", "hallway"), 0.2));
    p_label_given_label_h.insert(make_pair(make_pair("hall", "corridor"), 0.2));

    p_label_given_label_h.insert(make_pair(make_pair("hallway", "hallway"), 0.5));
    p_label_given_label_h.insert(make_pair(make_pair("hallway", "hall"), 0.2));
    p_label_given_label_h.insert(make_pair(make_pair("hallway", "corridor"), 0.2));

    p_label_given_label_h.insert(make_pair(make_pair("corridor", "corridor"), 0.5));
    p_label_given_label_h.insert(make_pair(make_pair("corridor", "hall"), 0.2));
    p_label_given_label_h.insert(make_pair(make_pair("corridor", "hallway"), 0.2));

    p_label_given_label_h.insert(make_pair(make_pair("meetingroom", "meetingroom"), 0.6));
    p_label_given_label_h.insert(make_pair(make_pair("meetingroom", "conferenceroom"), 0.3));

    p_label_given_label_h.insert(make_pair(make_pair("conferenceroom", "meetingroom"), 0.3));
    p_label_given_label_h.insert(make_pair(make_pair("conferenceroom", "conferenceroom"), 0.6));

    p_label_given_label_h.insert(make_pair(make_pair("lounge", "lounge"), 0.9));

    //not sure if we are making this more complicated than needed 
    //lets define a sysnonym set and high weight for synonyms 
    vector<set<string> > synonym_set; 
    
    string hallway_labels[] = {"hallway", "hall", "corridor"};

    set<string> hallway_set(hallway_labels, hallway_labels + sizeof(hallway_labels)/sizeof(hallway_labels[0]));

    synonym_set.push_back(hallway_set);

    string office_labels[] = {"office"};

    set<string> office_set(office_labels, office_labels + sizeof(office_labels)/sizeof(office_labels[0]));
    synonym_set.push_back(office_set);

    string lab_labels[] = {"lab"};

    set<string> lab_set(lab_labels, lab_labels + sizeof(lab_labels)/sizeof(lab_labels[0]));
    
    synonym_set.push_back(lab_set);

    string elevator_labels[] = {"elevatorlobby"};

    set<string> elevator_set(elevator_labels, elevator_labels + sizeof(elevator_labels)/sizeof(elevator_labels[0]));

    synonym_set.push_back(elevator_set);

    string conference_labels[] = {"meetingroom", "conferenceroom"};

    set<string> conference_set(conference_labels, conference_labels + sizeof(conference_labels)/sizeof(conference_labels[0]));
    
    synonym_set.push_back(conference_set);

    string lounge_labels[] = {"lounge"};

    set<string> lounge_set(lounge_labels, lounge_labels + sizeof(lounge_labels)/sizeof(lounge_labels[0]));

    synonym_set.push_back(lounge_set);

    //p_landmark_given_label.insert()
    buildLandmarkToLabel(labels_to_index, synonym_set, p_landmark_given_label);

    normalizeDistribution(labels_to_index, labels_to_index, p_label_given_label_h);
}

double LabelInfo::getProbabilityOfLandmarkGivenLabel(string landmark, string label){
    map<StringPair, double>::iterator it;
    it = p_landmark_given_label.find(make_pair(label, landmark));
    if(it == p_landmark_given_label.end())
        return 0;
    return it->second;
}

void LabelInfo::buildLandmarkToLabel(map<string, int> &labels_to_index, vector<set<string> > &synonym_set, map<StringPair, double> &p_landmark_given_label){
   

    for(int i=0; i < synonym_set.size(); i++){
        set<string> synonym = synonym_set[i];
        if(verbose)
            cout << "Synonym : " << endl;

        set<string>::iterator it;
        set<string>::iterator it_syn;
        //map<string, int> it_labels;
        map<string, int>::iterator it_landmark;

        for(it = synonym.begin() ; it!=synonym.end(); it++){
            cout << "\t" << *it << endl;
            //for(int k=0; k < synonym.size(); k++){ //the landmark 
            //  p_landmark_given_label.insert(make_pair(make_pair(synonym[j], synonym[k]), 0.9));
            //}
            for(it_landmark = labels_to_index.begin(); it_landmark != labels_to_index.end(); it_landmark++){
                //this is a synonym - add high likelihood 
                if(synonym.find(it_landmark->first) != synonym.end()){
                    p_landmark_given_label.insert(make_pair(make_pair(*it, it_landmark->first), 0.9));
                }
                else{
                    p_landmark_given_label.insert(make_pair(make_pair(*it, it_landmark->first), 0.1));
                }
            }
        }
    }
    if(verbose)
        printDistribution(labels_to_index, labels_to_index, p_landmark_given_label);
}

void LabelInfo::printDistribution(map<string, int> &g_var, map<string, int> &var, map<StringPair, double> &dist){
    map<string, int>::iterator it_v;
    map<string, int>::iterator it_g;

    map<StringPair, double>::iterator it_dist;

    for(it_g=g_var.begin(); it_g !=g_var.end(); it_g++){
        fprintf(stderr, "Conditional Var : %s\n", it_g->first.c_str());
        for(it_v = var.begin(); it_v != var.end(); it_v++){
            it_dist = dist.find(make_pair(it_g->first, it_v->first));
            if(it_dist != dist.end()){
                fprintf(stderr, "\t %s => %.3f\n", it_v->first.c_str(), it_dist->second);
            }
            else{
                fprintf(stderr, "\t %s => Error\n", it_v->first.c_str());
            }
        }
    }
}

//g_var is the one on which the distribution is conditioned on 
void LabelInfo::normalizeDistribution(map<string, int> &g_var, map<string, int> &var, map<StringPair, double> &dist, bool v){
    map<string, int>::iterator it_v;
    map<string, int>::iterator it_g;

    map<StringPair, double>::iterator it_dist;

    if(v)
        fprintf(stderr, "=====================Normalizing Label to Label========================\n");

    for(it_g=g_var.begin(); it_g !=g_var.end(); it_g++){
        double sum = 0;
        double scale = 1;
        int v_count = 0;
        for(it_v = var.begin(); it_v != var.end(); it_v++){
            it_dist = dist.find(make_pair(it_g->first, it_v->first));

            if(it_dist != dist.end()){
                sum += it_dist->second;
                v_count++;
            }
        }
        if(sum > 1.0){
            fprintf(stderr, "Error => %s has %f sum => normalizing\n", it_g->first.c_str(), sum);
            scale = (1- 0.9)/(sum); //allocated 10% prob to the other classes 
        }

        double other_v_prob = 0;
        if(v_count < var.size()){
            other_v_prob = (1-sum * scale)/(var.size() - v_count);
        }
        for(it_v = var.begin(); it_v != var.end(); it_v++){
            it_dist = dist.find(make_pair(it_g->first, it_v->first));

            if(it_dist != dist.end()){
                it_dist->second = it_dist->second * scale;
            }
            else{
                dist.insert(make_pair(make_pair(it_g->first, it_v->first), other_v_prob));
            }
        }
    }

    if(v){
        for(it_g=g_var.begin(); it_g !=g_var.end(); it_g++){
            fprintf(stderr, "Conditional Var : %s\n", it_g->first.c_str());
            for(it_v = var.begin(); it_v != var.end(); it_v++){
                it_dist = dist.find(make_pair(it_g->first, it_v->first));
                if(it_dist != dist.end()){
                    fprintf(stderr, "\t %s => %.3f\n", it_v->first.c_str(), it_dist->second);
                }
                else{
                    fprintf(stderr, "\t %s => Error\n", it_v->first.c_str());
                }
            }
        }
        fprintf(stderr, "\n\n");
    }
}

void LabelInfo::print(){
    fprintf(stderr, "Called print\n");

    fprintf(stderr, "Room Types\n");
    map<string, int>::iterator it_a; 
    for(it_a = appearance_to_index.begin(); it_a != appearance_to_index.end(); it_a++){
        fprintf(stderr, "\t%s\n", it_a->first.c_str());
    } 

    fprintf(stderr, "Room Category Types\n");
    map<string, int>::iterator it_c; 
    for(it_c = category_to_index.begin(); it_c != category_to_index.end(); it_c++){
        fprintf(stderr, "\t%s\n", it_c->first.c_str());
    } 

    fprintf(stderr, "Object Types\n");
    map<string, int>::iterator it_o; 
    for(it_o = object_name_to_index.begin(); it_o != object_name_to_index.end(); it_o++){
        fprintf(stderr, "\t%s\n", it_o->first.c_str());
    } 

    fprintf(stderr, "Labels\n");
    map<string, int>::iterator it_l; 
    for(it_l = labels_to_index.begin(); it_l != labels_to_index.end(); it_l++){
        fprintf(stderr, "\t%s\n", it_l->first.c_str());
    }         
}

//converts the string to index 
void LabelInfo::getLabelLcmMessage(slam_label_info_t *msg){
    msg->count = (int) labels_to_index.size();
    msg->id = (int32_t *) calloc (msg->count, sizeof(int32_t));
    msg->names = (char **) calloc (msg->count, sizeof(char *));
    map< string, int>::iterator it;
    int i =0; 
    for(it = labels_to_index.begin(); it != labels_to_index.end(); it++){
        msg->id[i] = it->second;
        msg->names[i] = strdup(it->first.c_str());
        i++;
    } 
}

int LabelInfo::getIndexFromObjectId(int id){
    map<int,int>::iterator it;
    it = object_id_to_index.find(id);
    if(it != object_id_to_index.end()){
        return it->second;
    }
    return -1;
}

void LabelInfo::getApperenceLcmMessage(slam_label_info_t *msg){
    msg->count = (int) appearance_to_index.size();
    msg->id = (int32_t *) calloc (msg->count, sizeof(int32_t));
    msg->names = (char **) calloc (msg->count, sizeof(char *));
    map< string, int>::iterator it;
    int i =0; 
    for(it = appearance_to_index.begin(); it != appearance_to_index.end(); it++){
        msg->id[i] = it->second;
        msg->names[i] = strdup(it->first.c_str());
        i++;
    } 
}

void LabelInfo::getTypeLcmMessage(slam_label_info_t *msg){
    msg->count = (int) category_to_index.size();
    msg->id = (int32_t *) calloc (msg->count, sizeof(int32_t));
    msg->names = (char **) calloc (msg->count, sizeof(char *));
    map< string, int>::iterator it;
    int i =0; 
    for(it = category_to_index.begin(); it != category_to_index.end(); it++){
        msg->id[i] = it->second;
        msg->names[i] = strdup(it->first.c_str());
        i++;
    } 
}

string LabelInfo::getTypeNameFromID(int id){
    map<int, string>::iterator it;
    it = index_to_category.find(id);
    if(it != index_to_category.end())
        return it->second;
    else
        return string("unknown");
}

string LabelInfo::getLabelNameFromID(int id){
    map<int, string>::iterator it;
    it = index_to_labels.find(id);
    if(it != index_to_labels.end())
        return it->second;
    else
        return string("unknown");
}

int LabelInfo::getIndexForLabel(string label){
    map<string, int>::iterator it;
    it = labels_to_index.find(label);
    if(it == labels_to_index.end())
        return -1;
    return it->second;
}

//these two are probably broken 
int LabelInfo::getClassifierClassIdFromIndex(int index){
    /*map<int, int>::iterator it;
    it = index_to_category.find(index);
    if(it == index_to_category.end())
        return -1;
        return it->second;*/
    return index; //not sure why this is here 
}

int LabelInfo::getIndexFromClassifierClassId(int index){
    /*map<int, int>::iterator it;
    it = category_to_index.find(index);
    if(it == category_to_index.end())
        return -1;
        return it->second;*/
    return index;
}

//Get variabels 
SemVar *LabelInfo::getAppearenceVar(int id){
    return new SemVar(id, (int) appearance_to_index.size());
}

SemVar *LabelInfo::getLaserAppearenceVar(int id){
    return new SemVar(id, (int) appearance_to_index.size());
}

SemVar *LabelInfo::getImageAppearenceVar(int id){
    return new SemVar(id, (int) appearance_to_index.size());
}

SemVar *LabelInfo::getRegionTypeVar(int id){
    return new SemVar(id, (int) category_to_index.size());
}

SemVar *LabelInfo::getNodeLabelObsVar(int id){
    return new SemVar(id, (int) labels_to_index.size());
}

SemVar *LabelInfo::getNodePhiVar(int id){
    return new SemVar(id, 2);
}

SemVar *LabelInfo::getRegionLabelVar(int id){
    return new SemVar(id, (int) labels_to_index.size());
}

//should we add this?? this can mess things up 
SemFactor *LabelInfo::getRegionLabelPriorFactor(SemVar *region_label){
    SemFactor *fac = new SemFactor(dai::VarSet(*region_label));
    map<string, int>::iterator it; 
    map<StringPair, double>::iterator it_p; 
    for(int j=0; j < region_label->states(); j++){
        //sum over the categories 
        double sum = 0;
        for(it = category_to_index.begin(); it != category_to_index.end(); it++){
            string label = getLabelNameFromID(j);
            it_p = p_label_given_category.find(make_pair(it->first, label));
            if(it_p != p_label_given_category.end()){
                sum += it_p->second;
            }
        }
        fac->set(j, sum);
    }
    return fac;
}

SemFactor *LabelInfo::getImageConfusionMatrix(SemVar *node_type, SemVar *image_observation){
    vector<SemVar> variables;
    variables.push_back(*node_type);
    variables.push_back(*image_observation);

    sort(variables.begin(), variables.end(), compareID);
    dai::VarSet var_set(variables.begin(), variables.end());
    SemFactor *fac = new SemFactor(var_set);

    int node_factor = 1;
    int obs_factor = 1;

    for(int i=0; i < variables.size(); i++){
        if(variables[i].label() < node_type->label()){
            node_factor *= variables[i].states();
        }
        if(variables[i].label() < image_observation->label()){
             obs_factor *= variables[i].states();
        }
    }

    int max_ind = node_type->states() * image_observation->states(); 

    map<StringPair, double>::iterator it_prob;

    int no_count = 1;

    map<string, int>::iterator it_appearance_ind;
    map<string, int>::iterator it_image_obs_ind;
    for(it_appearance_ind = appearance_to_index.begin(); it_appearance_ind != appearance_to_index.end(); it_appearance_ind++){
        for(it_image_obs_ind = appearance_to_index.begin(); it_image_obs_ind != appearance_to_index.end(); it_image_obs_ind++){
            int appearance_id = it_appearance_ind->second;
            int image_obs_id = it_image_obs_ind->second;

            int ind = node_factor * appearance_id + obs_factor * image_obs_id;

            if(ind > max_ind -1){
                fprintf(stderr, "Error - Index wrong\n");
                exit(-1);
            }

            //find the value in the map 
            it_prob = p_appearance_given_image.find(make_pair(it_image_obs_ind->first, it_appearance_ind->first));
            
            if(it_prob != p_appearance_given_image.end()){
                fac->set(ind, it_prob->second);
            }
            else{
                fprintf(stderr, "Laser appearance missing - exiting\n");
                exit(-1);
            }
        }
    }
    
    //cout << *fac << endl;

    return fac;
}

SemFactor *LabelInfo::getLaserConfusionMatrix(SemVar *node_type, SemVar *laser_observation){
    vector<SemVar> variables;
    variables.push_back(*node_type);
    variables.push_back(*laser_observation);

    sort(variables.begin(), variables.end(), compareID);
    dai::VarSet var_set(variables.begin(), variables.end());
    SemFactor *fac = new SemFactor(var_set);

    int node_factor = 1;
    int obs_factor = 1;

    for(int i=0; i < variables.size(); i++){
        if(variables[i].label() < node_type->label()){
            node_factor *= variables[i].states();
        }
        if(variables[i].label() < laser_observation->label()){
             obs_factor *= variables[i].states();
        }
    }

    //double buffer = 0.05;

    //map<string, map<string, double> >::iterator it_type;
    //map<string, double>::iterator it_obs;
    int max_ind = node_type->states() * laser_observation->states(); 

    map<StringPair, double>::iterator it_prob;

    int no_count = 1;

    map<string, int>::iterator it_appearance_ind;
    map<string, int>::iterator it_laser_obs_ind;
    for(it_appearance_ind = appearance_to_index.begin(); it_appearance_ind != appearance_to_index.end(); it_appearance_ind++){
        for(it_laser_obs_ind = appearance_to_index.begin(); it_laser_obs_ind != appearance_to_index.end(); it_laser_obs_ind++){
            int appearance_id = it_appearance_ind->second;
            int laser_obs_id = it_laser_obs_ind->second;

            int ind = node_factor * appearance_id + obs_factor * laser_obs_id;

            if(ind > max_ind -1){
                fprintf(stderr, "Error - Index wrong\n");
                exit(-1);
            }

            //find the value in the map 
            it_prob = p_appearance_given_laser.find(make_pair(it_laser_obs_ind->first, it_appearance_ind->first));
            
            if(it_prob != p_appearance_given_laser.end()){
                fac->set(ind, it_prob->second);
            }
            else{
                fprintf(stderr, "Laser appearance missing - exiting\n");
                exit(-1);
            }
        }
    }

    //cout << *fac << endl;

    return fac;
}


SemFactor *LabelInfo::getObjectToRegionTypeFactor(SemVar *region_type, SemVar *object_type){
    vector<SemVar> variables;
    variables.push_back(*region_type);
    variables.push_back(*object_type);

    sort(variables.begin(), variables.end(), compareID);
    dai::VarSet var_set(variables.begin(), variables.end());
    SemFactor *fac = new SemFactor(var_set);

    int region_factor = 1;
    int object_factor = 1;

    for(int i=0; i < variables.size(); i++){
        if(variables[i].label() < object_type->label()){
            object_factor *= variables[i].states();
        }
        if(variables[i].label() < region_type->label()){
            region_factor *= variables[i].states();
        }
    }

    map<string, int>::iterator it_object_type;
    map<StringPair, double> ::iterator it_prob;
    map<string, int>::iterator it_cat;

    //iterator through the region types 
    //iterate through the object types 
    //look for conditional probability

    for(it_cat = category_to_index.begin(); it_cat != category_to_index.end(); it_cat++){
        //fprintf(stderr, "Region Type : %s\n", it_cat->first.c_str());
        
        for(it_object_type = object_name_to_index.begin(); it_object_type != object_name_to_index.end(); it_object_type++){
            int object_ind = it_object_type->second;
            int region_ind = it_cat->second;
            
            int ind = object_factor * object_ind + region_factor * region_ind; 
            
            it_prob = p_category_given_object.find(make_pair(it_object_type->first, it_cat->first));

            if(it_prob != p_category_given_object.end()){
                // the region doesnt have any objects - add equal likelihoods 
                fac->set(ind, it_prob->second);
            }
            else{
                fprintf(stderr, "Object to category prob not found %s - %s\n", it_object_type->first.c_str(), it_cat->first.c_str());
                exit(-1);
            }            
        }
    }

    return fac;
}

SemFactor *LabelInfo::getRegionAppearenceToAppearenceFactor(SemVar *region_appearance, SemVar *node_type){ //node type is the appearance model
    vector<SemVar> variables;
    variables.push_back(*region_appearance);
    variables.push_back(*node_type);

    sort(variables.begin(), variables.end(), compareID);
    dai::VarSet var_set(variables.begin(), variables.end());
    SemFactor *fac = new SemFactor(var_set);

    int region_factor = 1;
    int node_factor = 1;

    for(int i=0; i < variables.size(); i++){
        if(variables[i].label() < node_type->label()){
            node_factor *= variables[i].states();
        }
        if(variables[i].label() < region_appearance->label()){
            region_factor *= variables[i].states();
        }
    }

    map<string, int>::iterator it_node_app;
    map<StringPair, double>::iterator it_prob;
    map<string, int>::iterator it_region;


    //double subclass_prob = 0.5;
    //double not_in_subclass_prob = (1 - subclass_prob) /(double) (label_info->classifier_class_to_index.size() - 1);

    for(it_node_app = appearance_to_index.begin(); it_node_app != appearance_to_index.end(); it_node_app++){
        //fprintf(stderr, "Region Type : %s\n", it_node_app->first.c_str());
        for(it_region = appearance_to_index.begin(); it_region != appearance_to_index.end(); it_region++){
            //if the region type is a subtype of the node type - then high value 
            //else low value 
            int node_ind = it_node_app->second;
            int region_ind = it_region->second;

            int ind = node_factor * node_ind + region_factor * region_ind; 

            it_prob = p_r_appearance_given_node_a.find(make_pair(it_node_app->first, it_region->first));

            if(it_prob != p_r_appearance_given_node_a.end()){
                fac->set(ind, it_prob->second);
            }
            else{
                fprintf(stderr, "Error no region to appearance found\n");
                exit(-1);
            }
        }
    }
    
    return fac;
}

SemFactor *LabelInfo::getRegionTypeToLabelFactor(SemVar *region_type, SemVar *region_label){
    vector<SemVar> variables;
    variables.push_back(*region_type);
    variables.push_back(*region_label);

    sort(variables.begin(), variables.end(), compareID);
    dai::VarSet var_set(variables.begin(), variables.end());
    SemFactor *fac = new SemFactor(var_set);

    int type_factor = 1;
    int label_factor = 1;

    for(int i=0; i < variables.size(); i++){
        if(variables[i].label() < region_type->label()){
            type_factor *= variables[i].states();
        }
        if(variables[i].label() < region_label->label()){
            label_factor *= variables[i].states();
        }
    }

    map<StringPair, double >::iterator it_prob;
    map<string, int>::iterator it_type;
    map<string, int>::iterator it_label;

    //fprintf(stderr, "Factors : %d, %d\n", label_factor, type_factor);

    int max_ind = region_type->states() * region_label->states(); 

    int no_count = 1;
    //for(int i=0; i < region_type->states(); i++){
    //  for(int j=0; j < region_label->states(); j++){
    for(it_type = category_to_index.begin(); it_type != category_to_index.end(); it_type++){
        //fprintf(stderr, "[%d] %s\n", it_type->second, it_type->first.c_str());
        for(it_label = labels_to_index.begin(); it_label != labels_to_index.end(); it_label++){
            //get the index for the label and type and put them there 
            int type_ind = it_type->second;
            int label_ind = it_label->second;

            int ind = label_factor * label_ind + type_factor *type_ind; 

            if(ind >= max_ind){
                fprintf(stderr, "Error - wrong index, Ind : %d -> Max Ind : %d : Label Ind : %d Type Ind : %d\n", 
                        ind, max_ind, label_ind, type_ind);
                exit(-1);
            }
            
            it_prob = p_category_given_label.find(make_pair(it_label->first, it_type->first)); //check for the count for the label in the type
            
            if(it_prob != p_label_given_category.end()){
                fac->set(ind, it_prob->second);
            }
            else{
                fprintf(stderr, "No label to category found - error\n");
                exit(-1);
            }
        }
    }

    //cout << *fac << endl;
    return fac;
}

SemFactor *LabelInfo::getRegionTypeToAppearenceFactor(SemVar *region_type, SemVar *region_appearance){
    vector<SemVar> variables;
    variables.push_back(*region_type);
    variables.push_back(*region_appearance);

    sort(variables.begin(), variables.end(), compareID);
    dai::VarSet var_set(variables.begin(), variables.end());
    SemFactor *fac = new SemFactor(var_set);

    int type_factor = 1;
    int appearance_factor = 1;

    for(int i=0; i < variables.size(); i++){
        if(variables[i].label() < region_type->label()){
            type_factor *= variables[i].states();
        }
        if(variables[i].label() < region_appearance->label()){
            appearance_factor *= variables[i].states();
        }
    }

    map<StringPair, double >::iterator it_prob;
    map<string, int>::iterator it_type;
    map<string, int>::iterator it_appearance;

    //fprintf(stderr, "Factors : %d, %d\n", label_factor, type_factor);

    int max_ind = region_type->states() * region_appearance->states(); 

    int no_count = 1;
    //for(int i=0; i < region_type->states(); i++){
    //  for(int j=0; j < region_label->states(); j++){
    for(it_type = category_to_index.begin(); it_type != category_to_index.end(); it_type++){
        //fprintf(stderr, "[%d] %s\n", it_type->second, it_type->first.c_str());
        for(it_appearance = appearance_to_index.begin(); it_appearance != appearance_to_index.end(); it_appearance++){
            //get the index for the label and type and put them there 
            int type_ind = it_type->second;
            int appearance_ind = it_appearance->second;

            int ind = appearance_factor * appearance_ind + type_factor *type_ind; 

            if(ind >= max_ind){
                fprintf(stderr, "Error - wrong index, Ind : %d -> Max Ind : %d : Label Ind : %d Type Ind : %d\n", 
                        ind, max_ind, appearance_ind, type_ind);
                exit(-1);
            }
            
            it_prob = p_category_given_appearance.find(make_pair(it_appearance->first, it_type->first)); //check for the count for the label in the type
            
            if(it_prob != p_category_given_appearance.end()){
                fac->set(ind, it_prob->second);
            }
            else{
                fprintf(stderr, "No label to category found - error\n");
                exit(-1);
            }
        }
    }

    //cout << *fac << endl;
    return fac;
}



SemFactor *LabelInfo::getRegionLabelFactor(SemVar *node_label, SemVar *phi_nd_region, SemVar *region_label){
    
    vector<SemVar> variables;
    variables.push_back(*node_label);
    variables.push_back(*phi_nd_region);
    variables.push_back(*region_label);

    sort(variables.begin(), variables.end(), compareID);
    dai::VarSet var_set(variables.begin(), variables.end());
    SemFactor *fac = new SemFactor(var_set);
    
    int node_factor = 1;
    int region_factor = 1;
    int phi_factor = 1;
    for(int i=0; i < variables.size(); i++){
        //int index = variables[i].label();
        if(variables[i].label() < node_label->label()){
            node_factor *= variables[i].states();
        }
        if(variables[i].label() < region_label->label()){
            region_factor *= variables[i].states();
        }
        if(variables[i].label() < phi_nd_region->label()){
            phi_factor *= variables[i].states();
        }
    }

    double equal_prob = 1/(double) region_label->states();
    int max_ind = phi_nd_region->states() * region_label->states() * node_label->states();

    ///think this is wrong 
    map<string, int>::iterator it_region_label;
    map<string, int>::iterator it_obs_label;

    map<StringPair, double>::iterator it_prob;

    for(int i=0; i < phi_nd_region->states(); i++){
        for(it_region_label = labels_to_index.begin(); it_region_label != labels_to_index.end(); it_region_label++){
            //fprintf(stderr, "[%d] %s\n", it_region_label->second, it_region_label->first.c_str());
            for(it_obs_label = labels_to_index.begin(); it_obs_label != labels_to_index.end(); it_obs_label++){
                //get the index for the label and type and put them there 
                int region_label_ind = it_region_label->second;
                int obs_label_ind = it_obs_label->second;
                
                int ind = phi_factor * i + region_factor * region_label_ind + node_factor * obs_label_ind; 

                if(ind >= max_ind){
                    fprintf(stderr, "Wrong Index\n");
                    exit(-1);
                }

                if(i == 0){
                    fac->set(ind, equal_prob);
                }
                else{
                    it_prob = p_label_given_label_h.find(make_pair(it_obs_label->first, it_region_label->first));
                    if(it_prob != p_label_given_label_h.end()){
                        fac->set(ind, it_prob->second);
                    }
                    else{
                        fprintf(stderr, "Error Label to heard label not found\n");
                        exit(-1);
                    }
                }
            }
        }
    }
    //cout << *fac << endl;
    return fac;
}
