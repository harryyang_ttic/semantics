#include "SlamParticle.hpp"
#include <algorithm>  

#define MAX_SCAN_FOR_BBOX 5.0
#define PIXMAP_RESOLUTION 0.1
#define PIXMAP_HIT_INC 0.5
#define MIN_EDGE_SIMILARITY 0.2 // Minimum similarity for proposed edges
#define PROB_PROPOSE_LANGUAGE_EDGE 0.5 // Probability of proposing language edge
#define LANGUAGE_EDGE_ADD_DIST_THRESHOLD 50.0//100.0 // Probability of proposing language edge
#define NODE_SKIP_DISTANCE 0.9
#define NODE_SKIP_SCANMATCH_DIST 2.0 //if the new node is closer than this threshold to the active nodes - it will not attempt a scanmatch 
#define LANGUAGE_TOP_N_GROUNDINGS 5
#define MIN_LANG_GROUNDING_PROB 0.1
#define BREAK_FROM_LANG_IF_TOP_N 0.15
#define LARGE_SM_TF 2.0 //above this rerun slam
#define SCANMATCH_CONSTRAINT_ADD_PRIOR 0.3
#define PROB_PROPOSE_EDGES_TO_INCOMPLETE_REGIONS 0.4
#define PROB_SM_WITHIN_REGION 0.3
#define MAX_NO_SCANS_TO_SM 10

using namespace Utils;
using namespace ExpUtils;

SlamParticle::SlamParticle(int64_t _utime, int _graph_id, 
                           lcm_t *_lcm,
                           BotTrans _body_to_laser, 
                           LabelInfo *_label_info, 
                           ConfigurationParams _params,
                           string _log_path, 
                           SLUClassifier *_slu_classifier = NULL, 
                           FloatPixelMap *_local_px_map = NULL,                           
                           bot_lcmgl_t *_lcmgl_sm_basic = NULL,
                           bot_lcmgl_t *_lcmgl_sm_graph = NULL,
                           bot_lcmgl_t *_lcmgl_sm_prior = NULL,
                           bot_lcmgl_t *_lcmgl_sm_result = NULL,
                           bot_lcmgl_t *_lcmgl_sm_result_low = NULL,
                           bot_lcmgl_t *_lcmgl_loop_closures = NULL)
{
    param = _params;

    slu_classifier = _slu_classifier;
    
    log_path = _log_path;

    graph_id = _graph_id;  

    output_writer = new OutputWriter(getIDString(), log_path, !param.writeToFile);

    graph = new SlamGraph(_label_info, _utime, output_writer, param.useFactorGraphs, slu_classifier, param.ignoreLMDist, param.ignoreFigureDist, 
                          param.useLaserForSemantic, param.useImageForSemantic);
    sparse_graph = new SlamGraph(_label_info, utime, output_writer, param.useFactorGraphs);
    
    label_info = _label_info;

    //hardcoded loop closing scan matcher params
    double metersPerPixel_lc = 0.02; //translational resolution for the brute force search
    double thetaResolution_lc = 0.01; //angular step size for the brute force search

    //Sachi - try this with resulution 5 
    int useMultires_lc = 5; // low resolution will have resolution metersPerPixel * 2^useMultiRes
    bool useThreads_lc = true;
    
    sm = new ScanMatcher(metersPerPixel_lc, thetaResolution_lc, useMultires_lc, useThreads_lc, false);
    //sm_low_res = _sm_low_res;
    lcmgl_sm_basic = _lcmgl_sm_basic;
    lcmgl_sm_graph = _lcmgl_sm_graph;
    lcmgl_sm_prior = _lcmgl_sm_prior;
    lcmgl_sm_result = _lcmgl_sm_result;
    local_px_map = _local_px_map;
    lcmgl_loop_closures = _lcmgl_loop_closures;
    lcmgl_sm_result_low = _lcmgl_sm_result_low;
    
    last_node_utime = 0;
    weight = 1;
    lcm = _lcm;
    body_to_laser = _body_to_laser;
    laser_to_body = body_to_laser;
    bot_trans_invert(&laser_to_body);

    language_event_count = 0;

    activeNode = NULL;
    tfFromActiveNode = NULL; 
    noiseFromActiveNode = NULL; 

    // Create and seed the random number generator
    const gsl_rng_type * T = gsl_rng_default;
    rng = gsl_rng_alloc (T);
    
    unsigned int seed;
    struct timeval tv;
    FILE *devrandom;
    
    if(1){
        gettimeofday(&tv,0);
        seed = tv.tv_sec + tv.tv_usec;
    }
    else{
        //this seems pretty slow for some reason 
        if ((devrandom = fopen("/dev/random","r")) == NULL) {
            gettimeofday(&tv,0);
            seed = tv.tv_sec + tv.tv_usec;
        } else {
            int status = fread(&seed,sizeof(seed),1,devrandom);
            fclose(devrandom);
        }
    }

    gsl_rng_set (rng, seed);
    active_region = NULL;

    output_writer->write_to_buffer("[Init] Created Particle", false, true, true);
}

SlamParticle::~SlamParticle(){
    if(graph)
        delete graph;
    if(sparse_graph)
        delete sparse_graph;
    
    output_writer->write_to_buffer("[Cleanup] Deleted Particle", false, true, true);
    delete sm; 
}

//do adjustments - such that the same topology exists 
void SlamParticle::adjust(int id, int64_t utime, SlamParticle *part){
    //take the given particle and adjust the graph to match the given particle - this should be less 
   
    //update the constraints 
    //update the region relationships 
    //update the pofz 
    char buf[1024];
    sprintf(buf, "[Resample] Adjusted to match particle : %d -> New ID : %d", part->graph_id, id);
    output_writer->write_to_buffer(buf);
    
    int64_t s_utime = bot_timestamp_now();

    map<int, SlamConstraint *>::iterator c_it;

    //the graph should have the same no of nodes 
    //lets just make sure 

    fprintf(stderr, BLUE "Adjusting Particle %d to match particle %d\n", graph_id, id);
    
    graph_id = id; 
    
    if(param.verbose)
        fprintf(stderr, "Current Graph No Nodes : %d => New Graph : %d\n", (int) graph->slam_node_list.size(), (int) part->graph->slam_node_list.size());
    
    while(graph->region_node_list.size() > 0){
        RegionNode *old_region = graph->region_node_list[0];
        graph->removeRegion(old_region);
    }

    active_region = NULL;

    if(param.verbose)
        fprintf(stderr, "\nAdding new regions\n");
    for(int i=0; i < part->graph->region_node_list.size(); i++){
        RegionNode *p_region = part->graph->region_node_list[i];
        RegionNode *new_region = graph->createNewRegion(p_region->region_id);

        if(p_region == part->active_region){
            active_region = new_region;
        }
        
        if(param.verbose)
            fprintf(stderr, "Adding new region : %d\n", new_region->region_id);

        sprintf(buf, "[Resample] [New Region] %d", p_region->region_id);
        output_writer->write_to_buffer(buf);
    
        for(int k=0; k < p_region->nodes.size(); k++){
            SlamNode *nd = graph->getSlamNodeFromID(p_region->nodes[k]->id);
            if(param.verbose)
                fprintf(stderr, "\tAdding Node : %d\n", nd->id);
            sprintf(buf, "[Resample] [New Node] %d", nd->id);
            output_writer->write_to_buffer(buf);
            new_region->addNode(nd);
        }
        //copy the label distribution 
        delete new_region->labeldist;
        new_region->labeldist = p_region->labeldist->copy();
    }

    //create a new set of constraints that match the old graph

    //update the label distribution 
    //add the nodes and the constraints 
    
    //won't need to do this for now - we will have to reconcile the semantic layer at somepoint 
    /*for(int i=0; i< part->graph->slam_node_list.size(); i++){
        
      SlamNode *ref_node = (SlamNode *) part->graph->slam_node_list.at(i);
        
      //create the nodes 
      SlamNode *adj_node = (SlamNode *) graph->getSlamNodeFromID(ref_node->id);

      //delete the label distribution 
      delete adj_node->labeldist;
      //make a copy of the old one 
      adj_node->labeldist = ref_node->labeldist->copy(); 
      adj_node->prob_used = ref_node->prob_used;
      }*/


    //add the constraints that are present in the part (but not in the current graph) 
    for ( c_it= part->graph->slam_constraints.begin() ; c_it != part->graph->slam_constraints.end(); c_it++ ){
        
        //check if there is a constraint added with the same id - otherwise add it 
        //skip the incremental ones - as these should already be there 
        //SlamConstraint *constraint  = (SlamConstraint *) c_it->second;
        SlamConstraint *edge = (SlamConstraint *) c_it->second;
        if(edge->type == SLAM_GRAPH_EDGE_T_TYPE_SM_INC || edge->type == SLAM_GRAPH_EDGE_T_TYPE_ODOM_INC){
            if(param.verbose)
                fprintf(stderr, "Incremental constraint : %d - %d => already added - skipping : %d\n", 
                        (int) edge->node1->id, (int) edge->node2->id, edge->type);
            continue;
        }
        else {
            //add the constraint 
            if(param.verbose)
                fprintf(stderr, "Adding Loop closure constraint from the old graph : %d - %d\n", 
                        (int) edge->node1->id, (int) edge->node2->id);

            SlamNode *node1 = graph->getSlamNodeFromID(edge->node1->id);
            SlamNode *node2 = graph->getSlamNodeFromID(edge->node2->id);
            
            SlamNode *actualnode1 = graph->getSlamNodeFromID(edge->actualnode1->id);
            SlamNode *actualnode2 = graph->getSlamNodeFromID(edge->actualnode2->id);

            //check if this constraint is already there - this might not be the same constraint also 
            if(graph->hasConstraint(edge->node1->id, edge->node2->id, edge->type)){
                //check if the constraint is the same 
                int ct_id = graph->getConstraintID(edge->node1->id, edge->node2->id, edge->type);
                SlamConstraint *ct = graph->getConstraint(ct_id);
                bool same_ct = ct->checkIfSameConstraint(edge);
                if(!same_ct){
                    //we need to remove the exsiting one before adding the new one 
                    graph->removeConstraint(ct_id);
                    SlamConstraint* new_ct  = graph->addConstraint(node1, node2, actualnode1, actualnode2, edge->transform, 
                                                                   edge->noise, edge->hitpct, edge->type , edge->status);
                    new_ct->pofz = edge->pofz;
                }
                else{
                    continue;
                }
            }
            else{
                SlamConstraint* new_ct  = graph->addConstraint(node1, node2, actualnode1, actualnode2, edge->transform, 
                                                               edge->noise, edge->hitpct, edge->type , edge->status);
                new_ct->pofz = edge->pofz;
            }            
        }
    }

    for ( c_it= graph->slam_constraints.begin() ; c_it != graph->slam_constraints.end(); c_it++ ){
        
        //check if there is a constraint added with the same id - otherwise add it 
        //skip the incremental ones
        SlamConstraint *edge = (SlamConstraint *) c_it->second;
        if(edge->type == SLAM_GRAPH_EDGE_T_TYPE_SM_INC || edge->type == SLAM_GRAPH_EDGE_T_TYPE_ODOM_INC){
            if(param.verbose)
                fprintf(stderr, "Incremental constraint : %d - %d => already added - skipping : %d\n", 
                        (int) edge->node1->id, (int) edge->node2->id, edge->type);
            continue;
        }
        
        else {
            //add the constraint 
            if(param.verbose)
                fprintf(stderr, "Adding Loop closure constraint from the old graph : %d - %d\n", 
                        (int) edge->node1->id, (int) edge->node2->id);

            SlamNode *node1 = graph->getSlamNodeFromID(edge->node1->id);
            SlamNode *node2 = graph->getSlamNodeFromID(edge->node2->id);
            
            if(part->graph->hasConstraint(edge->node1->id, edge->node2->id, edge->type)){
                continue;
            }
            else{
                int id = graph->getConstraintID(edge->node1->id, edge->node2->id, edge->type);
                if(id>=0){
                    if (param.verbose)
                        fprintf(stderr, "Removing constraint : %d\n", id);
                    graph->removeConstraint(id);
                }
            }
        }
    }

    sprintf(buf, "[Resample] Done adding edges");
    output_writer->write_to_buffer(buf);

    map<int, SlamConstraint *>::iterator iter;
    for ( iter = graph->slam_constraints.begin() ; iter != graph->slam_constraints.end(); iter++ ){        
        SlamConstraint *constr = iter->second;
        constr->processed = 1;
    }
    
    graph->updateRegionConnections();
    sprintf(buf, "[Resample] Updated region connections");
    output_writer->write_to_buffer(buf);
    //we need to update the complex language as well - just copy over from the other particle (since the regions are the same)   

    //graph->printRegionConnections();
    graph->status = 0;
    
    //this should run batch optimization
    fprintf(stderr, "Running SLAM on Coppied Graph\n");
    graph->runSlam();    
    sprintf(buf, "[Resample] Slam run");
    output_writer->write_to_buffer(buf);
    //fprintf(stderr, "Done\n");
    
    //need to adjust the complex language events also 
    //i.e. clear the paths we have node - and copy the paths and then 
    //copy the results also 
    //*********** add the adjustments
    //we should update the objects as well 
    graph->updateObjects();    
    sprintf(buf, "[Resample] Objects updated");
    output_writer->write_to_buffer(buf);
    graph->updateRegionMeans();

    fprintf(stderr, "+++++++ Running BP for particle : %d\n", graph_id);

    if(param.useFactorGraphs){
        graph->runBP();
        sprintf(buf, "[Resample] BP run");
        output_writer->write_to_buffer(buf);
    }

    //run this first so that the landmarks are updated - but will the order matter also?? - since some language will 
    //affect other language

    graph->updateComplexLanguage(part->graph->complex_language_events);    
    sprintf(buf, "[Resample] Complex Language Updated");
    output_writer->write_to_buffer(buf);

    if(param.useFactorGraphs){
        graph->runBP();
    }

    int64_t e_utime = bot_timestamp_now();
    
    if(param.verbose)
        fprintf(stderr, "Time taken to Resample : %.3f \n", (e_utime - s_utime) / 1.0e6);

    sprintf(buf, "[Resample] Time taken to Resample : %.3f", (e_utime - s_utime) / 1.0e6);
    output_writer->write_to_buffer(buf, false, true, true);

    fprintf(stderr, RESET_COLOR);
    weight = part->weight;
    
    activeNode = part->activeNode;
    tfFromActiveNode = part->tfFromActiveNode; 
    noiseFromActiveNode = part->noiseFromActiveNode; 
}

vector<SRQuestion> SlamParticle::getQuestionCost(int event_id){
    graph->updateConnectivityGraph();
    vector<SRQuestion> questions = graph->getSRQuestionForLanguageEvent(event_id);
    
    return questions; 
}

// --------------- Adding nodes to slam graph ----------//

int SlamParticle::addNodesAndEdges(vector<NodeScan *> new_nodes, 
                                   map<int, slam_language_label_t *> new_language_labels, 
                                   vector<complex_language_event_t *> complex_language, int probMode,
                                   int find_ground_truth, double *maximum_probability, int ignoreLanguageForEdges, 
                                   AddNodesAndEdgesResult &result_info, map<int, ObjectDetection *> &objects, 
                                   bool finishSlam){

    return addNodesAndEdgesUniversalMethod(new_nodes, new_language_labels, 
                                           complex_language, probMode,
                                           find_ground_truth, maximum_probability, ignoreLanguageForEdges, result_info, objects, finishSlam);
}

//this is a ported implementation of the RSS work 
int SlamParticle::addNodesAndEdgesUniversalMethod(vector<NodeScan *> new_nodes, 
                                             map<int, slam_language_label_t *> new_language_labels, 
                                             vector<complex_language_event_t *> complex_language, int probMode,
                                             int find_ground_truth, double *maximum_probability, 
                                             int ignoreLanguageForEdges, AddNodesAndEdgesResult &result_info, 
                                             map<int, ObjectDetection *> &objects, bool finishSlam)
{
    //just add the node to the graph 
    //then do the spectral clustering stuff 
    //then decide the regions and then do scanmatching    
    
    static char buf[1024];

    sm_tictoc("AddNodes");
    //ParticleOperationResult add_node_results;
    //add_node_results.type = "adding new nodes";
    int64_t s_time = bot_timestamp_now();
    
    sprintf(buf, "[Add Nodes] - No new nodes : %d Lang labels : %d", (int) new_nodes.size(), (int) new_language_labels.size());
    output_writer->write_to_buffer(buf);

    map<int, SlamNode *>::iterator it;

    SlamGraph *basic_graph = graph;
    if(param.useBasicGraph){
        basic_graph = sparse_graph;
    }

    //add each object to an internal map - or maybe create a SlamNode equivalent of NodeScans - for the object 
    

    int created_new_region = 0;
    double time_for_pofz = 0;
    // Steps:
    //  1) Add new nodes to graph (includes bleeding from previous supernode)
    //  2) Add new loop closure edges (includes bleeding between supernodes)
    //  3) Apply simple language:
    //      a) Update Dirichlet for preceding supernode
    //      b) Bleed to neighboring nodes
    //  4) Propose language edges

    int max_node_ind = -1;

    map<int, SlamNode *> lang_updated_nodes;

    int node_added = 0;

    int edge_added = 0;

    //For each node 
    //    Add node to graph 
    //    Check whether to split region 
    //        Sample region segmentation (if fixed method - likelihood function should be 0 for distance below and 1 above)
    //    Sample edges from the current fully formed region to other regions using spatial-semantic prior 
    //    Integrate the edges to the topology and metric representation
    //    Merge regions (if seletected)
    //    Update the semantic layer 
    //    Update the pofz 
   
    double pofz_time = 0;
    double time_lang = 0;
    
    SlamNode *last_added_node = NULL;

    int64_t s_add = bot_timestamp_now();
    for(int i=0; i < new_nodes.size(); i++){
        NodeScan *pose = (NodeScan *) new_nodes.at(i);
        last_node_utime = pose->utime;
        SlamNode *node = NULL;
        if(!pose) {
            fprintf (stdout, "addNodesAndEdges: new_node pose is NULL!\n");
            continue;
        }

        bool new_region_created = false;

        int slam_run = 0;

        RegionNode *new_region = NULL; //this is the newly finished region 
        
        int node_ind = pose->node_id;

        if(param.printGraphCreation)
            fprintf(stderr, "\n\n------------------------------ Checking to add Node : %d\n", node_ind);

        sprintf(buf, "[Add Nodes] Adding new node : %d", node_ind);
        output_writer->write_to_buffer(buf);

        int add_type = 0; 
        
        //we need to infer the pose for this potential node - before we can check the min dist
        PoseToPoseTransform *pose_transform = NULL;

        int type = SLAM_GRAPH_EDGE_T_TYPE_SM_INC;
        if ((param.scanmatchBeforeAdd) && 
            (pose->inc_constraint_sm->hitpct > param.incSMAcceptanceThreshold)) {
            pose_transform = pose->inc_constraint_sm;
        }
        else{
            type = SLAM_GRAPH_EDGE_T_TYPE_ODOM_INC;
            pose_transform = pose->inc_constraint_odom;
        }

        //get the transform from the active node
        Pose2d current_node_to_active_node = *pose_transform->transform; 
        NodeScan *last_nd = pose_transform->node_relative_to;

        if(param.printGraphCreation)
            fprintf(stderr, "Current constraint : %f, %f, %f\n", current_node_to_active_node.x(), current_node_to_active_node.y(), 
                current_node_to_active_node.t());
        
        int region_split = 0;

        RegionNode *current_region = NULL;

        if(activeNode == NULL){
            fprintf(stderr, "First Slam Node - adding\n");
            //add to the first region 
            //we should add this node to the graph by default 
            node = addNodeToNewRegion(pose, pose_transform, NULL);
            distFromLastRegionNode = 0; 

            current_region = node->region_node;

            slam_language_label_t *annotation = getAnnotatedLanguage(new_language_labels, node->id);
            double prob_correspondance = 1.0;
            //add this to the region 
            if(annotation){
                //add this to the slam node 
                if(param.useFactorGraphs){
                    //add this to the slam node 
                    node->addLanguageAnnotation(label_info, prob_correspondance, annotation);
                    node->updateLanguageFactors();
                }
                else{
                    int label_id = current_region->labeldist->getLabelID(string(annotation->update));
                    
                    if(label_id>=0){
                        current_region->labeldist->addDirectObservation(label_id, prob_correspondance, language_event_count);
                    }
                    language_event_count++;
                }
            }            

            graph->runSlam();
            slam_run = 1;

            node->region_node->updateMean();
            activeNode = pose; 
            clearActiveNodeParams();            
            
            last_added_node = node;
        }
        else{
            //active node is the last node added 
            SlamNode *active_node = graph->getSlamNodeFromID(activeNode->node_id);
            Pose2d active_pose = active_node->getPose();
            Pose2d current_node_pose = active_pose.oplus(current_node_to_active_node);
            
            RegionNode *region_to_add = NULL;
            
            //check how far we are from the active node interms of time ?? - this will fail if we scanmatched 
            //to a new node - how about distance??
            
            double dist_from_active = hypot(current_node_to_active_node.x(), 
                                            current_node_to_active_node.y());
          
            distFromLastRegionNode += hypot(current_node_to_active_node.x(), current_node_to_active_node.y());

            current_region = active_node->region_node;

            node = addNodeToRegion(pose, pose_transform, active_node, active_node->region_node);
            graph->runSlam();
            slam_run = 1;
            
            last_added_node = node;

            int64_t s_utime_segment = bot_timestamp_now();
            //new region is the region with the current node (The new current region) 
            //current region is the remainder of the nodes (will not contain the current node if it was segmented) - The newly segmented out region 
            new_region = segmentRegionGeneral(current_region);
            
            int64_t e_utime_segment = bot_timestamp_now();
            sprintf(buf, "[Add Nodes] [Segment] [Performance] Time to segment : %.3f", 
                    (e_utime_segment - s_utime_segment)/ 1.0e6);
            output_writer->write_to_buffer(buf);

            if(new_region){
                sprintf(buf, "[Add Nodes] [Segment] New region created : %d", new_region->region_id);
                output_writer->write_to_buffer(buf);

                new_region_created = true;
                created_new_region = 1;
            }
            if(new_region_created){
                graph->updateRegionConnections();
                graph->updateRegionMeans();
            }
            else{
                node->region_node->updateMean();
            }

            //add direct annotations 
            slam_language_label_t *annotation = getAnnotatedLanguage(new_language_labels, node->id);
            double prob_correspondance = 1.0; 

            bool semantic_added_for_current = false;

            if(annotation){
                sprintf(buf, "[Add Nodes] [Language] Adding language update : %s", annotation->update);
                output_writer->write_to_buffer(buf);
                if(param.useFactorGraphs){
                    //add this to the slam node 
                    node->addLanguageAnnotation(label_info, prob_correspondance, annotation);
                    node->updateLanguageFactors();
                }
                else{
                    int label_id = node->region_node->labeldist->getLabelID(string(annotation->update));
                
                    if(label_id>=0){
                        node->region_node->labeldist->addDirectObservation(label_id, prob_correspondance, language_event_count);
                        semantic_added_for_current=true;
                        language_event_count++;
                    }
                }
            }

            vector<RegionNode *>  regions_to_check_for_semantic;

            graph->updateObjects(objects);
            
            if(!param.useFactorGraphs){
                if(param.bleedLabelsToRegions && (new_region_created)){
                    vector<RegionNode *>  bled_regions = bleedLabelsWithSkip(new_region);
                    for(int r = 0; r < bled_regions.size(); r++){
                        regions_to_check_for_semantic.push_back(bled_regions[r]);
                    }
                }
                if(new_region_created & current_region->labeldist->hasDirectObservation()){// && semantic_added_for_current){
                    //add the current region if there is semantic information there 
                    regions_to_check_for_semantic.push_back(current_region);
                }                
            }
            else{
                if(new_region_created){   
                    graph->runBP();
                }
                else{
                    graph->runBP(node->region_node);
                }
            }

            vector<RegionNode *> new_edges;
            if(new_region_created){
                sm_tictoc("SampleEdges");
                //propose edges 
                int64_t e_time_1 = bot_timestamp_now();
                if(param.proposeSeperateSemanticEdges){
                    vector<RegionNode *>  regions_to_check_empty;
                    new_edges = sampleEdgesFromRegion(current_region, new_region, regions_to_check_empty, ignoreLanguageForEdges);  
                    sprintf(buf, "[Add Nodes] [Sample Edge] [Complete Region] Sampling edges in complete region (dist only)");
                    output_writer->write_to_buffer(buf);
                }
                else{
                    sprintf(buf, "[Add Nodes] [Sample Edge] [Complete Region] Sampling edges in complete region (combined)");
                    output_writer->write_to_buffer(buf);
                    new_edges = sampleEdgesFromRegion(current_region, new_region, regions_to_check_for_semantic, ignoreLanguageForEdges);      
                }
                //we need to propose edges from 
                int64_t e_time_2 = bot_timestamp_now();

                sprintf(buf, "[Add Nodes] [Segment] [Performance] Time to sample edges (s) : %.3f", (e_time_2 - e_time_1) / 1.0e6);
                output_writer->write_to_buffer(buf);
                sm_tictoc("SampleEdges");
                
                for(int ed=0; ed < new_edges.size(); ed++){
                    sprintf(buf, "[Add Nodes] [Sample Region Edges] [New Edge] : %d - %d ", (int) current_region->region_id, (int) new_edges[ed]->region_id);
                    output_writer->write_to_buffer(buf);
                }

                sprintf(buf, "[Add Nodes] [Sample Region Edges] : %d - Time to sample : %.3f", (int) new_edges.size(), (e_time_2 - e_time_1) / 1.0e6);
                output_writer->write_to_buffer(buf);
            }
            else{
                //should we do scanmatch within regions?? - otherwise large regions can have a larger error 
                if(param.sampleEdgesWithCurrentRegion){
                    if(current_region->nodes.size() > 4){
                        double p_sm_incomplete_region = PROB_PROPOSE_EDGES_TO_INCOMPLETE_REGIONS;
                        double u_rand_sm_incomplete_region = gsl_rng_uniform (rng);
                        //do we do this prob 
                        if(u_rand_sm_incomplete_region < p_sm_incomplete_region){
                            new_edges = sampleEdgesFromRegion(current_region, new_region, regions_to_check_for_semantic, ignoreLanguageForEdges, 3);      
                            sprintf(buf, "[Add Nodes] [Sample Edge] [Incomplete Region] Curr Region : %d Sampling edges in incomplete region - region size : %d - Edges : %d", current_region->region_id, (int) current_region->nodes.size(), (int) new_edges.size());
                            output_writer->write_to_buffer(buf);
                            
                        }
                    }
                }
                //we should not do this all the time 
                if(new_edges.size() == 0){
                    if(param.scanmatchWithCurrentRegion){
                        double p_sm_within_region = PROB_SM_WITHIN_REGION;
                        double u_rand_sm_within_region = gsl_rng_uniform (rng);
                        //might require a better heuristic
                        if(u_rand_sm_within_region < p_sm_within_region && current_region->nodes.size() > param.minRegionSizeBeforeScanmatch){
                            if(doScanMatchWithinRegion(current_region, node)){
                                sprintf(buf, "[Add Nodes] [Sample Internal Edges] Added internal edge");
                                output_writer->write_to_buffer(buf);
                            }
                        }                    
                    }
                }
                //current scan to the surrounding region?? 
            }

            //propose edges based on language 
            
            ////////////////Inserting from Old Code - for language edges 
            
            //check what annotation here is 
            if(param.proposeSeperateSemanticEdges && regions_to_check_for_semantic.size() > 0){
                fprintf(stderr, "\n\n\n((((((((((((((( Regions to check for Semantic : %d )))))))))))))))\n\n", (int) regions_to_check_for_semantic.size());
                
                if(new_region){
                    fprintf(stderr, "New region (to Skip) - %d\n", new_region->region_id);
                }
                else{
                    fprintf(stderr, "This should not happen\n");
                }

                for(int l=0; l < regions_to_check_for_semantic.size(); l++){
                    fprintf(stderr, "\tSemantic Region (to check) %d\n", regions_to_check_for_semantic[l]->region_id);
                }
                
                sprintf(buf, "[Add Nodes] [Semantic Region Edges] Sampling edges");
                output_writer->write_to_buffer(buf);
                sampleSemanticEdgesFromRegion(new_region, regions_to_check_for_semantic);
                                
            }      
            
            if(new_edges.size() > 0){
                sm_tictoc("UpdateConnections");
                int64_t e_time_1 = bot_timestamp_now();
                graph->updateRegionConnections();
                int64_t e_time_2 = bot_timestamp_now();
                sprintf(buf, "[Add Nodes] [Updating graph connections] Time taken : %f", (e_time_2 - e_time_1) / 1.0e6);
                output_writer->write_to_buffer(buf);
                sm_tictoc("UpdateConnections");
            }

            if(param.printGraphCreation)
                fprintf(stderr, RED "\n\nActive Region : %d => New node added : %d\n" RESET_COLOR, active_node->region_node->region_id, node->id);

            activeNode = pose; 
            
            if(param.printGraphCreation)
                fprintf(stderr, MAJENTA "\n\nDistance From Region Node : %f\n" RESET_COLOR, distFromLastRegionNode);         

            //should we check previous regions that may have gotten corrected - to add new constraints?? 
            //if(param.checkOldRegions && new_edges.size() > 0){
            if(param.checkOldRegions && new_edges.size() > 0){
                //fprintf(stderr, "Checking past regions\n");
                vector<regionPair> old_constraints = checkOldRegionConstraints(current_region);                                        
            }

            if(new_region_created){
                //lets look for exploration boundaries 
                if(0){
                    findExplorationFrontiers(current_region); //this is the segmented out region - this is perhapse not the best naming - since the current region can mean different things if segmented or not 
                }
                
                //merge region 
                int64_t e_time_1 = bot_timestamp_now();
                int merged = 0;

                if(param.mergeRegions){
                    current_region = mergeRegions(current_region, new_region, &merged);                    
                }

                if(merged){
                    if(param.useFactorGraphs){
                        graph->runBP();
                    }
                }

                int64_t e_time_2 = bot_timestamp_now();

                sprintf(buf, "[Merge Regions] [Performance] Time taken (s): %f", (e_time_2 - e_time_1) / 1.0e6);
                output_writer->write_to_buffer(buf);               
                
            }

            if(param.calculateRegionProb && new_region_created){
                //should we scanmatch these regions??
                //do scanmatch for the region??
                double m_prob = 0;
                //int64_t utime_s = bot_timestamp_now();
                double prob = getProbOfMeasurementLikelihoodSMRegion(current_region, new_region, &m_prob);
                //int64_t utime_e = bot_timestamp_now();
                //pofz_time += (utime_e - utime_s) / 1.0e6;
                weight += prob;
            }            
                        
            clearActiveNodeParams();
        }
        
        bool complex_lang_found = false;
       
        for(int l=0; l < complex_language.size(); l++){
            if(complex_language[l]->node_id == node->id){
                complex_lang_found = true;
                graph->addComplexLanguageEvent(complex_language[l]);
            }
        }
        
        //run optimization - basic part of the graph has changed
        if(slam_run !=1 && (add_type >0 || edge_added || node!= NULL)){
            if(param.useBasicGraph){
                basic_graph->runSlam();
            }
            //graph->runSlam();
        }
        
        //graph->runSlam();

        if(graph->slam_node_list.size() == 1 || node == NULL){
            continue;
        }

        if(param.verbose){
            fprintf(stderr, "\n============================================================\n");
            fprintf(stderr, "Particle : %d - New nodes to add : %d\n", 
                    graph_id, (int) new_nodes.size());
            fprintf(stderr,"Adding basic constraints\n");
        }

        if(param.printGraphCreation)
            fprintf(stderr, YELLOW "No of Nodes : %d\n" RESET_COLOR, (int) graph->slam_node_list.size());

        if(!param.calculateRegionProb && node != NULL){
            double max_prob = 0;

            int64_t s_utime = bot_timestamp_now();
            calculateObservationProbabilityOfNode(node, &max_prob);
            int64_t e_utime = bot_timestamp_now();
            time_for_pofz += (e_utime - s_utime) / 1.0e6;
            //fprintf(stderr, "Doing P(z/G) calculation\n");
        }

        //if there was a change in the regions - we should re-evaluate the topology 
        /*if(region_split && new_segmented_region != NULL){
            graph->updateRegionConnections();   
            }*/           
        
        int64_t bot_timestamp_lang_s = bot_timestamp_now();
        //this needs to happen now - otherwise the label distribution is not correct
        if(new_region_created){
            //we should rebuild the shortest paths from the last completed region to 
            //all other regions 
            //essentially what we want is the path from the last completed region to 
            //all nodes at which we heard a complex language expression 
            //such that we can re-evaluate the language as it applies to this node 
            //also remember to invert the path 
            if(param.findShortestPathEveryTime){
                if(param.useSLULibrary){
                    graph->evaluateAllComplexLanguageGroundings(new_region);
                }
                else{
                    graph->findShortestPathFromAllComplexLanguage(new_region);
                }
            }
            else{
                //this should only be done if we are doing continuous grounding of language 
                //this is not what we want to call if we wish to do one shot language groundings 
                bool finish_grounding = false;
                bool complete_if_valid = false;
                if(param.gMode == GROUND_ONCE){
                    finish_grounding = true;
                }
                if(param.gMode == FINISH_ON_GOOD){
                    complete_if_valid = true;
                }
                //if these were grounded - we should add this to the semantic edge check 
                
                vector<RegionNode *> grounded_regions;
                grounded_regions = graph->findShortestPathFromOutstandingComplexLanguage(new_region, finish_grounding, complete_if_valid);

                if(param.proposeSeperateSemanticEdges){
                    sampleSemanticEdgesFromRegion(active_region, grounded_regions);
                }
            }
        }
        else if(param.checkCurrentRegionForGrounding){
            //check if the region is large enough and when we ran the check for this region the last time - or node id (to prevent every node 
            //after the threshold to be checked 
            //can we bootstrap from the last shortest paths?? - since its unlikely to have changed from the last region to this one?? 
            
            if(current_region->getSize() > param.minRegionSizeBeforeLanguage){ //we need to add skipping when we checked this region n nodes before now
                current_region->updateMean();

                //we do this also for the new approach - to see what the grounding likelihood is for the current region 
                
                graph->findShortestPathFromAllComplexLanguageToCurrentRegion(current_region);

                //maybe check for a different question here 
                //e.g. am I at X? 
                //which will also impact the entropy - if there is a likelihood of the 
                //region being X - then a Yes answer will affect the distribution 
                //with a given expectation 
                
                //this wouldn't lead to the current region getting likelihoods immediately - so prob not the best place to check this 
                //although this will hit at some point 
                graph->checkCurrentRegionForInfoGain(current_region);
            }            
        }

        if(0){
            graph->printParentMap();
        }
        
        int64_t bot_timestamp_lang_e = bot_timestamp_now();
        time_lang += (bot_timestamp_lang_e - bot_timestamp_lang_s) / 1.0e6;        
    }

    //we should only check for questions on the last node 

    //get rid of this function call 
    if(param.askQuestions){
        graph->checkSRQuestionInfoGain(last_added_node, param.considerBotRelativeYesNoQuestions, 
                                       param.considerBotRelativeMCQQuestions, param.considerLandmarkQuestions, lcmgl_sm_graph);
    }

    int64_t e_add = bot_timestamp_now();
    
    sprintf(buf, "[Add Nodes] [Performance] Time to add all nodes : %.3f Time to lang : %.3f", (e_add - s_add) / 1.0e6, time_lang);
    output_writer->write_to_buffer(buf);

    SlamNode *last_graph_node = graph->getSlamNodeFromPosition(graph->slam_node_list.size()-1);
    if(finishSlam && last_graph_node){
        sm_tictoc("FinishSlam");
        //propose edges from the current region 
        sprintf(buf, "[Add Nodes] [Finish Slam] Finish SLAM message heard - proposing edges from the current region to other regions");
        output_writer->write_to_buffer(buf);
     
        
        RegionNode *current_region = last_graph_node->region_node;
        
        vector<RegionNode *> regions_to_check_for_semantic;
        vector<RegionNode *> new_edges;
        new_edges = sampleEdgesFromRegion(current_region, NULL, regions_to_check_for_semantic, ignoreLanguageForEdges);

        if(new_edges.size() > 0){
            graph->updateRegionConnections();
            graph->updateRegionMeans();
        }
        
        int merged = 0;
        current_region = mergeRegions(current_region, NULL, &merged);  

        graph->updateObjects(objects);

        if(param.bleedLabelsToRegions){           
            bleedLabels();
        }
        else{
            sprintf(buf, "[Add Nodes] [Finish Slam] Running BP");
            output_writer->write_to_buffer(buf);
            if(param.useFactorGraphs){
                graph->runBP();
            }
        }

        sm_tictoc("FinishSlam");
    }

    sprintf(buf, "[PofZ] [Performance] Time taken for pofz (s) : %.3f" , time_for_pofz);
    output_writer->write_to_buffer(buf);
    
    output_writer->write_buffer();

    int64_t end_constraint = bot_timestamp_now();
    sm_tictoc("AddNodes");
    return created_new_region;
} 

// -------------- Utils : Adding nodes ----------------- //

SlamNode *SlamParticle::addNodeToNewRegion(NodeScan *pose, PoseToPoseTransform * p2p_constraint, SlamNode *relative_node){
    return addNodeToRegion(pose, p2p_constraint, relative_node, 
                           graph->createNewRegion());
}

SlamNode *SlamParticle::addNodeToRegion(NodeScan *pose, PoseToPoseTransform * p2p_constraint, SlamNode *relative_node, 
                                        RegionNode *region){
    //the transform given here is relative to the given node 
    //if relative node is NULL - this is the first node - then we should add a prior 
    
    //then we should create a new node and then add the node to the new region 
    SlamNode *node = graph->addSlamNode(pose);
    if(param.useBasicGraph){
        sparse_graph->addSlamNode(pose);        
    }
    
    region->addNode(node);
    
    if(graph->slam_node_list.size() == 1){
        if(param.verbose){
            fprintf(stderr, "Adding constraint to the origin\n");
        }
        //this one doesnt matter - set to small for either type
        if(p2p_constraint != NULL){
            //PoseToPoseTransform *p2p_constraint = node->slam_pose->inc_constraint_sm; 
            double cov_hardcode[9] = { .0001, 0, 0, 0, .0001, 0, 0, 0, .0001 };
            Matrix3d cv_hardcode(cov_hardcode);
            Noise cov_hc= SqrtInformation(10000. * eye(3));//Covariance(cv_hardcode);
            graph->addOriginConstraint(node, *p2p_constraint->transform, *p2p_constraint->noise);                
        }
        else{
            fprintf(stderr, "Error - no constraint given for the origin constraint\n");
        }

        if(param.useBasicGraph){
            //for the basic graph 
            SlamNode *b_node = sparse_graph->getSlamNodeFromID(node->id);
                    
            if(b_node == NULL){
                fprintf(stderr, "First node not found - this should not have happened\n");
            }
            else{
                if(param.verbose){
                    fprintf(stderr, "First node found in Basic Slam graph - adding constraint\n"); 
                }
                        
                //this one doesnt matter - set to small for either type
                if(p2p_constraint != NULL){
                    //PoseToPoseTransform *p2p_constraint = b_node->slam_pose->inc_constraint_sm; 
                    double cov_hardcode[9] = { .0001, 0, 0, 0, .0001, 0, 0, 0, .0001 };
                    Matrix3d cv_hardcode(cov_hardcode);
                    Noise cov_hc= SqrtInformation(10000. * eye(3));//Covariance(cv_hardcode);
                    sparse_graph->addOriginConstraint(b_node, *p2p_constraint->transform, *p2p_constraint->noise);  
                }
            }
        }
        node->processed = 1;
    }
    else{
        //add the given constraint
        if(relative_node != NULL){
            graph->addConstraint(node, relative_node, node, relative_node, *p2p_constraint->transform, 
                                 *p2p_constraint->noise, p2p_constraint->hitpct, p2p_constraint->type, 
                                 SLAM_GRAPH_EDGE_T_STATUS_SUCCESS);

            /*Pose2d delta_tf(p2p_constraint->transform->x(), p2p_constraint->transform->y(), p2p_constraint->transform->t() + 2* M_PI);

            graph->addConstraint(node, relative_node, node, relative_node, delta_tf, //*p2p_constraint->transform, 
                                 *p2p_constraint->noise, p2p_constraint->hitpct, p2p_constraint->type, 
                                 SLAM_GRAPH_EDGE_T_STATUS_SUCCESS);*/
        }
        else{
            fprintf(stderr, "Error : Relative Slam node not provided\n");
        }
    }    

    if(param.useBasicGraph){
        sparse_graph->runSlam();        
    }

    return node;
}

SlamNode* SlamParticle::addNodeToSlamGraph(NodeScan *pose){
    fprintf(stderr, "Adding New Node\n");
    SlamNode *node = graph->addSlamNode(pose);
    if(param.useBasicGraph){
        sparse_graph->addSlamNode(pose);        
    }
    graph->updateSupernodeIDs();
    
    //first node added - we need to add a prior for the node 
    if(graph->slam_node_list.size() == 1){
        if(param.verbose){
            fprintf(stderr, "Adding constraint to the origin\n");
        }
        //this one doesnt matter - set to small for either type
        if(node->slam_pose->inc_constraint_sm != NULL){
            PoseToPoseTransform *p2p_constraint = node->slam_pose->inc_constraint_sm; 
            double cov_hardcode[9] = { .0001, 0, 0, 0, .0001, 0, 0, 0, .0001 };
            Matrix3d cv_hardcode(cov_hardcode);
            Noise cov_hc= SqrtInformation(10000. * eye(3));//Covariance(cv_hardcode);
            graph->addOriginConstraint(node, *p2p_constraint->transform, *p2p_constraint->noise);                
        }

        if(param.useBasicGraph){
            //for the basic graph 
            SlamNode *b_node = sparse_graph->getSlamNodeFromID(node->id);
                    
            if(b_node == NULL){
                fprintf(stderr, "First node not found - this should not have happened\n");
            }
            else{
                if(param.verbose){
                    fprintf(stderr, "First node found in Basic Slam graph - adding constraint\n"); 
                }
                        
                //this one doesnt matter - set to small for either type
                if(b_node->slam_pose->inc_constraint_sm != NULL){
                    PoseToPoseTransform *p2p_constraint = b_node->slam_pose->inc_constraint_sm; 
                    double cov_hardcode[9] = { .0001, 0, 0, 0, .0001, 0, 0, 0, .0001 };
                    Matrix3d cv_hardcode(cov_hardcode);
                    Noise cov_hc= SqrtInformation(10000. * eye(3));//Covariance(cv_hardcode);
                    sparse_graph->addOriginConstraint(b_node, *p2p_constraint->transform, *p2p_constraint->noise);  
                }
            }
        }
        node->processed = 1;
        //run slam here 
        /*if(param.useBasicGraph){
          sparse_graph->runSlam();
          }
          graph->runSlam();*/
    }

    return node;
}

int SlamParticle::addIncrementalConstraintToGraph(SlamNode* node1){
    //insert the inc constraint stuff here 
    PoseToPoseTransform *p2p_constraint = NULL;
        
    int32_t type, status;        
    // Use only odometry if requested, otherwise use scanmatching and fall back
    // on odometry if the incremental scan match is bad
    if ((param.scanmatchBeforeAdd) && (node1->slam_pose->inc_constraint_sm->hitpct > param.incSMAcceptanceThreshold)) {
        type = SLAM_GRAPH_EDGE_T_TYPE_SM_INC;
        p2p_constraint = node1->slam_pose->inc_constraint_sm;
    }
    else {
        type =  SLAM_GRAPH_EDGE_T_TYPE_ODOM_INC;
        p2p_constraint = node1->slam_pose->inc_constraint_odom;
        if (param.scanmatchBeforeAdd && param.verbose)
            fprintf (stdout, "--------------- Scan Match Failed. Falling back on Odometry ---------------\n");
    }
    
    if(p2p_constraint == NULL || p2p_constraint->node_relative_to == NULL){
        fprintf (stdout, "p2p_constraint is NULL\n");
        return 0 ;
    }
    
    if(p2p_constraint->node_relative_to->node_id != (p2p_constraint->node_current->node_id -1)){
        if(param.verbose){
            fprintf(stderr, "Not an incremental constraint - skipping for now\n");
        }
        return  0;
    }    

    return addConstraintToGraph(p2p_constraint);    
}

int SlamParticle::addConstraintToGraph(SlamNode* node1, SlamNode *node2, PoseToPoseTransform *constraint){
    fprintf(stderr, "Adding Constraint\n");

    graph->addConstraint(node1, node2, node1, node2, *constraint->transform, *constraint->noise, 
                         constraint->hitpct, constraint->type, SLAM_GRAPH_EDGE_T_STATUS_SUCCESS);
    
    if(param.useBasicGraph){
        //add to the basic graph 
        sparse_graph->addConstraint(sparse_graph->getSlamNodeFromID(node1->id), sparse_graph->getSlamNodeFromID(node2->id), sparse_graph->getSlamNodeFromID(node1->id), sparse_graph->getSlamNodeFromID(node2->id), *constraint->transform, *constraint->noise, 
                                    constraint->hitpct, constraint->type, SLAM_GRAPH_EDGE_T_STATUS_SUCCESS);
    }
    fprintf(stderr, "\tAdding constraint to nodes - %d %d\n", (int) node1->id, (int) node2->id);
    return 1;
}

int SlamParticle::addConstraintToGraph(PoseToPoseTransform *constraint){
    
    fprintf(stderr, "Adding Constraint\n");
    SlamNode* node1 = graph->getSlamNodeFromID(constraint->node_current->node_id);
    SlamNode *node2 = graph->getSlamNodeFromID(constraint->node_relative_to->node_id);
    if(node1 == NULL || node2 == NULL){
        return 0;
    }
    addConstraintToGraph(node1, node2, constraint);
    
    return 1;
}

// --------------- Corresponding related -------------- //

//point overlap stuff - used to PofZ calculations/ region merges etc
PointCorrespondence *SlamParticle::getCachedCorrespondence(int id_1, int id_2){
    map<nodePairKey, PointCorrespondence *>::iterator it;
    it = correspondenceCache.find(make_pair(id_1, id_2));
    if(it == correspondenceCache.end()){
        return NULL;
    }
    return it->second;
}

void SlamParticle::insertCachedCorrespondence(int id_1, int id_2, PointCorrespondence *corr){
    map<nodePairKey, PointCorrespondence *>::iterator it;
    it = correspondenceCache.find(make_pair(id_1, id_2));
    if(it == correspondenceCache.end()){
        correspondenceCache.insert(make_pair(make_pair(id_1, id_2), corr));
    }
    else{
        delete it->second;
        correspondenceCache.erase(it);
        correspondenceCache.insert(make_pair(make_pair(id_1, id_2), corr));
    }
}

double SlamParticle::getUpdatedCorrespondenceScore(SlamNode *nd_i, SlamNode *nd_j){
    //check if we have a cached transform
    BotTrans tf_nd_i_to_nd_j = getTransformFromSlamNodes(nd_i, nd_j);
    PointCorrespondence *corr = getCachedCorrespondence(nd_i->id, nd_j->id);
    int different = false;
    if(corr != NULL){
        Pose2d cache_p = getPoseFromBotTrans(corr->transform);
        Pose2d current_p = getPoseFromBotTrans(tf_nd_i_to_nd_j);
        if(fabs(cache_p.x() - current_p.x()) > 0.01){
            different = true;
        }
        else if(fabs(cache_p.y() - current_p.y()) > 0.01){
            different = true;
        }
        else if(fabs(cache_p.t() - current_p.t()) > bot_to_radians(1.0)){
            different = true;
        }                        
    }
    
    if(corr == NULL || different){
        vector<pointDist> dist_vector;
        int matched = getCorrespondance(tf_nd_i_to_nd_j, nd_j, nd_i, &dist_vector);       

        double score = matched / (double) nd_i->slam_pose->scan->numPoints;
        PointCorrespondence *new_corr = new PointCorrespondence(tf_nd_i_to_nd_j, nd_i->id, nd_j->id, matched, dist_vector);
        insertCachedCorrespondence(nd_i->id, nd_j->id, new_corr);
        return score;
    }
    return corr->matches / (double) nd_i->slam_pose->scan->numPoints;

}

PointCorrespondence *SlamParticle::getUpdatedCorrespondence(SlamNode *nd_i, SlamNode *nd_j){
    //check if we have a cached transform
    BotTrans tf_nd_i_to_nd_j = getTransformFromSlamNodes(nd_i, nd_j);
    PointCorrespondence *corr = getCachedCorrespondence(nd_i->id, nd_j->id);
    int different = false;
    if(corr != NULL){
        Pose2d cache_p = getPoseFromBotTrans(corr->transform);
        Pose2d current_p = getPoseFromBotTrans(tf_nd_i_to_nd_j);
        if(fabs(cache_p.x() - current_p.x()) > 0.01){
            different = true;
        }
        else if(fabs(cache_p.y() - current_p.y()) > 0.01){
            different = true;
        }
        else if(fabs(cache_p.t() - current_p.t()) > bot_to_radians(1.0)){
            different = true;
        }                        
    }
    
    if(corr == NULL || different){
        vector<pointDist> dist_vector;
        int matched = getCorrespondance(tf_nd_i_to_nd_j, nd_j, nd_i, &dist_vector);       

        double score = matched / (double) nd_i->slam_pose->scan->numPoints;
        PointCorrespondence *new_corr = new PointCorrespondence(tf_nd_i_to_nd_j, nd_i->id, nd_j->id, matched, dist_vector);
        insertCachedCorrespondence(nd_i->id, nd_j->id, new_corr);
        return new_corr;
    }
    return corr;
}

//maybe check if the transforms match - just incase 
void SlamParticle::replaceCorrespondenceScore(SlamNode *nd_i, SlamNode *nd_j, int matched){
    //check if we have a cached transform
    BotTrans tf_nd_i_to_nd_j = getTransformFromSlamNodes(nd_i, nd_j);
    
    PointCorrespondence *new_corr = new PointCorrespondence(tf_nd_i_to_nd_j, nd_i->id, nd_j->id, matched);
    insertCachedCorrespondence(nd_i->id, nd_j->id, new_corr);
}

int SlamParticle::getCorrespondance(BotTrans node_match_to_target, SlamNode *target, SlamNode *match, vector<pointDist> *match_list = NULL){

    Scan *match_scan = match->slam_pose->scan;
    double pQuery[3] = {.0},  pMatchInTarget[3];
    ANNpoint qp = annAllocPt(2);
    //pretty large radius bound ??
    double radius_bound = 0.2;//0.05;//0.3; //was 0.05 
    int query_size = 1;
    double epsilon =  0.05;//0.1;
    ANNidxArray  nnIdx = new ANNidx[query_size];
    ANNdistArray dists = new ANNdist[query_size];

    int matched = 0;

    for(unsigned i = 0; i < match_scan->numPoints; i++){
        pQuery[0] = match_scan->points[i].x;
        pQuery[1] = match_scan->points[i].y;

        // Transform the query point into the local frame
        bot_trans_apply_vec(&node_match_to_target, pQuery, pMatchInTarget);

        qp[0] = pMatchInTarget[0];
        qp[1] = pMatchInTarget[1];
        
        //int64_t search_utime = bot_timestamp_now();
        
        target->slam_pose->kd_tree->annkFRSearch (qp,                    // query point
                                                  ANN_POW(radius_bound), // squared radius search bound
                                                  query_size,            // number of near neighbors
                                                  nnIdx,                 // nearest neighbors (returned)
                                                  dists,                 // distance (returned)
                                                  epsilon);
        
        for(int l=0;l< query_size ;l++){
            if(nnIdx[l] < 0){//because we have reached the end of the valid neighbours
                break; 
                //continue;
            }
            if(match_list){                     
                double dist_val = sqrt(dists[l]); 
                int ind = i;
                match_list->push_back(make_pair(i, dist_val));
            }
            //count++;
            matched++;
            break;
        }
    }                                           

    annDeallocPt(qp);
    delete nnIdx;
    delete dists;
    //this is not strictly correct as this is not what we scanmatch for ICP - we scanmatch the entire region 
    //why not use a gridmap ?? 
    return matched;
}

// -------------------- Edge sampling ---------------------- //

//find the best connected region that can be merged with the current region 
RegionNode* SlamParticle::mergeRegions(RegionNode *current_region, RegionNode *new_region, int *merged){
    //vector<RegionNode *> connected_regions){
    //propose merges with connected regions 
    static char buf[1024];
    RegionNode *new_merged_region = current_region;
    vector<RegionNode *> connected_regions = current_region->getConnectedRegions();
    *merged = 0;
    sprintf(buf, "[Merge Regions] Proposed merges");
    output_writer->write_to_buffer(buf);
                
    //maybe we should check which ones are the best to merge and merge all/best or sample them 
    //find the best one to merge with - this current method doesn't do that 
    //higher is better 
    vector<regionDist> merge_score;
    for(int c = 0; c < connected_regions.size(); c++){
        //check the overlap and then see if we can merge these them??
        sprintf(buf, "[Merge Regions] Proposed [%d] - [%d]", current_region->region_id, connected_regions[c]->region_id);
        output_writer->write_to_buffer(buf);

        double merge_ncut = 2;
                        
        RegionNode *region_to_connect = connected_regions[c];

        //skip the active region 
        if(region_to_connect == new_region){
            sprintf(buf, "[Merge Regions] Continuing - new region");
            output_writer->write_to_buffer(buf);        
            continue; 
        }

        if(current_region->isIncrementalEdge(region_to_connect)){
            sprintf(buf, "[Merge Regions] Continuing - incremental region");
            output_writer->write_to_buffer(buf);        
            continue;
        }
        //maybe we should ignore regions that are connected by consecutive nodes?? 
                     
        //or should we merge and cut these regions differently?? 
        //resegmenting this might mess things up - in the model ??

        if(param.mergeRegionsOnOverlap){
            double overlap = getPointOverlap(current_region, region_to_connect);            
            sprintf(buf, "[Merge Regions] [Score] [Point Overlap] Result for merging  %d - %d  => Overlap : %f", 
                    current_region->region_id, region_to_connect->region_id, overlap);
            output_writer->write_to_buffer(buf);  
            merge_score.push_back(make_pair(region_to_connect, overlap));
        }
        else if(param.mergeRegionsOnCloseNodePairs){
            double overlap = current_region->calculateCloseNodes(region_to_connect);//getPointOverlap(current_region, region_to_connect);
            
            sprintf(buf, "[Merge Regions] [Score] [Node Overlap] Close Node Percentage for merging  %d - %d  => Overlap : %f", 
                    current_region->region_id, region_to_connect->region_id, overlap);
            output_writer->write_to_buffer(buf);  

            //if(overlap > 0.7){
            //  new_merged_region = graph->mergeRegions(current_region, region_to_connect);
            //do we have to break here?? - prob need to recalculate the proposals 
            //    break;
            //}   
            merge_score.push_back(make_pair(region_to_connect, overlap));
        }
        else{
            //we need to do this after adding the new edge to the slam graph (i.e. updating the map)
            vector< vector<SlamNode *> >  new_segment_list = checkToMergeRegions(current_region, region_to_connect, &merge_ncut);

            sprintf(buf, "[Merge Regions] [Score] [Merge NCut] %d - %d ==> Merge NCut : %f" RESET_COLOR, current_region->region_id, 
                    region_to_connect->region_id, merge_ncut);
            output_writer->write_to_buffer(buf);  

            merge_score.push_back(make_pair(region_to_connect, merge_ncut));
        }
    }

    vector<RegionNode *> nodes_to_merge;
    
    if(merge_score.size()>0){
        std::sort(merge_score.begin(), merge_score.end(), compareRegionOverlap);
        
        sprintf(buf, "[Merge Regions] Checking to merge based on merge scores");
        output_writer->write_to_buffer(buf);  
        
        for(int i=0; i < merge_score.size(); i++){
            sprintf(buf, "[Merge Regions] [Sorted] Region [%d] : %d -> Score : %f\n", 
                    current_region->region_id, merge_score[0].first->region_id, merge_score[0].second);
            output_writer->write_to_buffer(buf);  
        }

        //only merging one 
        if(!param.mergeMultiple){
            if(param.mergeRegionsOnOverlap){
                if(merge_score[0].second > 0.7){
                    nodes_to_merge.push_back(merge_score[0].first);
                    //new_merged_region = graph->mergeRegions(current_region, merge_score[0].first);
                    *merged = 1;
                }
            }
            else if(param.mergeRegionsOnCloseNodePairs){
                if(merge_score[0].second > 0.5){
                    //new_merged_region = graph->mergeRegions(current_region, merge_score[0].first);
                    nodes_to_merge.push_back(merge_score[0].first);
                    *merged = 1;
                }
            }
            else{
                if(merge_score[0].second > 0.5){
                    //new_merged_region = graph->mergeRegions(current_region, merge_score[0].first);
                    nodes_to_merge.push_back(merge_score[0].first);
                    *merged = 1;
                }
            }
        }
        else{
            for(int i=0; i < merge_score.size(); i++){
                if(param.mergeRegionsOnOverlap){
                    if(merge_score[i].second > 0.7){
                        //new_merged_region = graph->mergeRegions(current_region, merge_score[i].first);
                        nodes_to_merge.push_back(merge_score[i].first);
                        *merged = 1;
                        sprintf(buf, "[Merge Regions] [Merged] [Point Overlap] Region : %d - %d -> Score : %f\n", 
                                current_region->region_id, merge_score[i].first->region_id, merge_score[0].second);
                        output_writer->write_to_buffer(buf);  
                    }
                    else{
                        break;
                    }
                }
                else if(param.mergeRegionsOnCloseNodePairs){
                    if(merge_score[i].second > 0.5){
                        sprintf(buf, "[Merge Regions] [Merged] [Node Overlap] Region : %d - %d -> Score : %f\n", 
                                current_region->region_id, merge_score[i].first->region_id, merge_score[0].second);
                        output_writer->write_to_buffer(buf);  
                        //new_merged_region = graph->mergeRegions(current_region, merge_score[i].first);
                        nodes_to_merge.push_back(merge_score[i].first);
                        *merged = 1;
                    }
                    else{
                        break;
                    }
                }
                else{
                    if(merge_score[0].second > 0.5){
                        sprintf(buf, "[Merge Regions] [Merged] [Merge NCut] Region : %d - %d -> Score : %f\n", 
                                current_region->region_id, merge_score[i].first->region_id, merge_score[0].second);
                        output_writer->write_to_buffer(buf);  
                        //new_merged_region = graph->mergeRegions(current_region, merge_score[i].first);
                        nodes_to_merge.push_back(merge_score[i].first);
                        *merged = 1;
                    }
                    else{
                        break;
                    }
                }
            }
        }
        
        if(nodes_to_merge.size() > 0){
            new_merged_region = current_region;
            for(int i=0; i < nodes_to_merge.size(); i++){
                new_merged_region = graph->mergeRegions(new_merged_region, nodes_to_merge[i]);
            }            
        }
    }

    if(*merged){
        graph->updateRegionConnections();
        graph->updateRegionMeans();
        updateRegionRayTrace(new_merged_region);
    }
    return new_merged_region;
}

//find the best connected region that can be merged with the current region 
vector<regionPair> SlamParticle::checkOldRegionConstraints(RegionNode *current_region){
    vector<regionPair> added_nodes; 
    set<regionPair> added_pairs; 
    //vector<RegionNode *> connected_regions){
    //propose merges with connected regions 
    static char buf[1024];

    double threshold = 0.7;

    vector<regionPairDist> region_pair_score;

    int64_t stime = bot_timestamp_now();
    
    for(int i=0; i < graph->region_node_list.size(); i++){
        RegionNode *r1 = graph->region_node_list[i]; 
        if(r1 == current_region)
            continue;
        for(int j=0; j < graph->region_node_list.size(); j++){
            RegionNode *r2 = graph->region_node_list[j]; 
            if(r2 == current_region)
                continue;

            if(r1 == r2)
                continue;

            if(added_pairs.find(make_pair(r1,r2)) != added_pairs.end()){
                continue;
            }

            if(r1->isConnected(r2)){
                continue;
            }

            double overlap = r1->calculateCloseNodes(r2);
            
            if(overlap >= threshold){
                region_pair_score.push_back(make_pair(make_pair(r1,r2), overlap));
                added_pairs.insert(make_pair(r1,r2));
                added_pairs.insert(make_pair(r2,r1));
            }
        }
    }

    int64_t etime = bot_timestamp_now();
    
    sprintf(buf, "[Check Old Region Edges] [Valid Score Pairs] [Performance] %d Time taken: %.3f", (int) region_pair_score.size(), 
            (etime - stime) / 1.0e6);
    output_writer->write_to_buffer(buf);    
        
    //fprintf(stderr, "Overlap region size : %d - Time to check : %.3f\n", (int) region_pair_score.size(), (etime - stime) / 1.0e6);
    if(region_pair_score.size() > 0){
        sort(region_pair_score.begin(), region_pair_score.end(), compareRegionPairOverlap);
        for(int i=0; i < region_pair_score.size(); i++){
            RegionNode *r1 = region_pair_score[i].first.first;
            RegionNode *r2 = region_pair_score[i].first.second;
            bool large_difference_sm = false;
            int sucess = scanmatchRegions(r1->mean_node, r2->mean_node, large_difference_sm, true);
            
            sprintf(buf, "[Check Old Region Edges] [Edge check] %d -%d - %d", r1->region_id, r2->region_id, sucess);
            output_writer->write_to_buffer(buf);    
            //should we merge these guys?? 

            if(sucess){
                added_nodes.push_back(region_pair_score[i].first);
                //fprintf(stderr, "Found constraint %d - %d\n", r1->region_id, r2->region_id);
            }
        }
    }

    return added_nodes;
}

void SlamParticle::sampleSemanticEdgesFromRegion(RegionNode *new_region, vector<RegionNode *> regions_to_check_for_semantic){
    int no_region_edges_to_sample = 10;
    int semantic_regions_check_max = 3;
    int regions_checked = 0;
    int large_change = 0;

    int total_edges_checked = 0; 

    bool use_simple_if_close = true;

    //space of language edges is high - especially for complex language - where we take the top N (5) 

    //maybe we should sample from the entire space of edges ?? (upto a max - instead of the N per region)


    for(int r = 0; r < regions_to_check_for_semantic.size(); r++){
        RegionNode *to_check_region = regions_to_check_for_semantic[r];
        vector<nodePairDist> semantic_prop_region_merges = proposeOldRegionListFromRegionSemantics(to_check_region, no_region_edges_to_sample);

        fprintf(stderr, "Valid semantic edge proposal size : %d\n", (int) semantic_prop_region_merges.size());

        regions_checked = 0;
        int lang_edge_created = 0;
        //propose region based edges 

        vector<RegionNode *> connected_nodes; 
        vector<nodePairDist> valid_semantic_merges;
        for(int k=0; k < semantic_prop_region_merges.size(); k++){
            double m_dist = semantic_prop_region_merges[k].second;
            //remember to delete this
            SlamNode *c_nd = semantic_prop_region_merges[k].first.first; 
            SlamNode *p_nd = semantic_prop_region_merges[k].first.second;

            RegionNode *r1 = c_nd->region_node; 
            RegionNode *r2 = p_nd->region_node; 

            if(r1->isConnected(r2)){
                fprintf(stderr, "Skipping - Connected\n");
                continue;
            }
            if(r1 == new_region || r2 == new_region){
                fprintf(stderr, "Skipping - New region\n");
                continue;
            }
            valid_semantic_merges.push_back(semantic_prop_region_merges[k]);
            connected_nodes.push_back(r2);
        }

        int64_t s_utime = bot_timestamp_now();
        map<RegionNode*, int> distance_map = graph->getRegionDistance(to_check_region, connected_nodes, 20);
        int64_t e_utime = bot_timestamp_now();
                
        map<RegionNode*, int>::iterator it_dist;

        //for(int k=0; k < semantic_prop_region_merges.size(); k++){
        for(int k=0; k < valid_semantic_merges.size(); k++){
            double m_dist = valid_semantic_merges[k].second;
            //remember to delete this
            SlamNode *c_nd = valid_semantic_merges[k].first.first; 
            SlamNode *p_nd = valid_semantic_merges[k].first.second;

            RegionNode *r1 = c_nd->region_node; 
            RegionNode *r2 = p_nd->region_node; 

            fprintf(stderr, "Checking edge : %d - %d\n", (int) r1->region_id, (int) r2->region_id);

            if(r1->isConnected(r2)){
                fprintf(stderr, "Skipping - Connected\n");
                continue;
            }
            if(r1 == new_region || r2 == new_region){
                fprintf(stderr, "Skipping - New region\n");
                continue;
            }

            if(regions_checked >= semantic_regions_check_max){
                break;
            }
                    
            int large_constraint_change = 0;
            //do a scanmatch - do we do the extensive one??
            fprintf(stderr, "Checking for Language Edge\n");

            int distance = 100; 
            it_dist = distance_map.find(r2);
            
            if(it_dist != distance_map.end()){
                distance = it_dist->second;
            }

            bool close = false; 

            fprintf(stderr, "Topo distance : %d\n", distance);
            
            if(distance < 10){//5){
                close = true;
            }

            int sucess = 0;
            if(use_simple_if_close && close){
                fprintf(stderr, "**** Running simple check\n");
                bool large_tf = false;
                sucess = scanmatchRegions(c_nd, p_nd, large_tf, true, true, true, false);
                //exit(-1);
            }
            else{
                fprintf(stderr, "**** Running extensive check\n");
                sucess = scanmatchRegionsExtensive(c_nd, p_nd, true, &large_constraint_change, true);//scanmatchRegions(c_nd, p_nd, true, true);                     
            }                 

            if(large_constraint_change)
                large_change = 1;
            if(sucess){
                lang_edge_created = 1;
                graph->updateRegionConnections();
                graph->updateRegionMeans();
            }
          
            regions_checked++;
            total_edges_checked++;
        }
                    
        if(lang_edge_created){
            //force the graph to batch optimize
            fprintf(stderr, "Sucessful Language SM Added - Doing batch optimization\n");
            if(large_change)
                graph->status = 0;
            graph->runSlam();
        }

        if(param.printLanguageScanmatch)
            fprintf(stderr, RED "\n\n========= No of edges checked : %d======\n\n\n" RESET_COLOR, regions_checked);
    }

    if(param.printLanguageScanmatch)
        fprintf(stderr, RED "\n\n========= Total No of edges checked : %d======\n\n\n" RESET_COLOR, total_edges_checked);
}

vector<RegionNode *> SlamParticle::sampleEdgesFromRegion(RegionNode *current_region, RegionNode *ignore_region, vector<RegionNode *> regions_to_check_for_semantic, bool ignoreLanguageForEdges, int no_region_edges_to_sample) 
{
    static char buf[1024];
    vector<RegionNode *> added_edges;

    //if we merge we shouldnt use cache - merge can be different to different particles 
    
    if(param.spectralClustering || param.mergeRegions){
        //we propose based on a spatial-semantic prior
               
        graph->updateRegionMeans();

        int64_t s_utime_dist = bot_timestamp_now();
        vector<nodePairDist> prop_region_merges = proposeOldRegionListFromRegion(current_region, no_region_edges_to_sample, param.useSemanticsForDistanceEdge);
        int64_t e_utime_dist = bot_timestamp_now();

        sprintf(buf, "[Sample Edge] [Performance] Time to get regions : %.3f", (e_utime_dist - s_utime_dist) / 1.0e6);
        output_writer->write_to_buffer(buf);
                
        int regions_check_max = 5;
        int regions_checked = 0;
        
        //propose region based edges 
        int64_t s_utime = bot_timestamp_now();

        double time_to_sm = 0;
        bool large_difference = false;

        for(int k=0; k < prop_region_merges.size(); k++){
            double m_dist = prop_region_merges[k].second;
            //remember to delete this
            SlamNode *c_nd = prop_region_merges[k].first.first; 
            SlamNode *p_nd = prop_region_merges[k].first.second;

            RegionNode *r1 = c_nd->region_node; 
            RegionNode *r2 = p_nd->region_node; 
            if(r1->isConnected(r2) || r2 == ignore_region){
                continue;
            }

            bool large_difference_sm = false;
            regions_checked++;
            int64_t s_utime_sm = bot_timestamp_now();
            //look at doing caching scanmatches 
            int sucess = scanmatchRegions(c_nd, p_nd, large_difference_sm, true);
            int64_t e_utime_sm = bot_timestamp_now();

            sprintf(buf, "[Sample Edge] [Checked Edge] Checked between Regions %d - %d - Accepted : %d - Large Diff : %d", 
                    r1->region_id, r2->region_id, sucess, large_difference_sm);
            output_writer->write_to_buffer(buf);

            time_to_sm += (e_utime_sm - s_utime_sm)/ 1.0e6;
            if(sucess){
                added_edges.push_back(r2);
            }

            if(large_difference_sm){
                large_difference = true;
            }

            if(regions_checked >= regions_check_max)
                break;
        }

        if(added_edges.size() > 0){
            if(large_difference){
                graph->status = 0;
                output_writer->write_to_buffer("[Sample Edge] Large Difference in Scanmatch");
            }
            graph->runSlam();
            graph->updateParentMap();
        }

        int64_t e_utime = bot_timestamp_now();       

        double total_time = (e_utime - s_utime) / 1.0e6;
        
        double avg_time = 0; 
        if(regions_checked > 0) 
            avg_time = total_time / regions_checked;
        
        sprintf(buf, "[Sample Edge] [Performance] Time to sample edges : %d -> %.3f / %.3f => Average : %.3f", regions_checked, time_to_sm, total_time, avg_time);
        output_writer->write_to_buffer(buf);

        return added_edges;
    }
    else{
        //add distance edges 
        //current region here is the last fully formed region 
        int64_t s_utime_dist = bot_timestamp_now();
        graph->updateRegionMeans();
        int64_t s_utime_dist_1 = bot_timestamp_now();
        //edge proposals here are not based on semantic similarity - which is what we want - although this is tied to the clustering method for now 
        vector<nodePairDist> dist_prop_region_merges = proposeOldRegionListFromRegion(current_region, no_region_edges_to_sample, param.useSemanticsForDistanceEdge);
        int64_t e_utime_dist = bot_timestamp_now();

        sprintf(buf, "[Sample Edge] [Performance] Time to get regions : %.3f (Update means : %f)", 
                (e_utime_dist - s_utime_dist) / 1.0e6, (s_utime_dist_1 - s_utime_dist) / 1.0e6);
        output_writer->write_to_buffer(buf);
        
        int dist_regions_check_max = 5;
        int regions_checked = 0;
                
        int64_t s_sm_utime = bot_timestamp_now();

        bool large_difference = false;
        
        //propose region based edges 
        for(int k=0; k < dist_prop_region_merges.size(); k++){
            double merge_ncut = 2;
            double m_dist = dist_prop_region_merges[k].second;
            //remember to delete this
            SlamNode *c_nd = dist_prop_region_merges[k].first.first; 
            SlamNode *p_nd = dist_prop_region_merges[k].first.second;

            RegionNode *r1 = c_nd->region_node; 
            RegionNode *r2 = p_nd->region_node; 
            if(r1->isConnected(r2) || r2 == ignore_region){
                continue;
            }
            
            bool large_difference_sm = false;
            
            //do a scanmatch - done with cache and skipping odometry (cache ok since the regions are consistant 
            int sucess = scanmatchRegions(c_nd, p_nd, large_difference_sm, true, true, true, param.ignoreLargeScanmatchChanges);
            if(sucess){
                added_edges.push_back(r2);
            }

            sprintf(buf, "[Sample Edge] [Checked Edge] Checked between Regions %d - %d - Accepted : %d - Large Diff : %d", 
                    r1->region_id, r2->region_id, sucess, large_difference_sm);
            output_writer->write_to_buffer(buf);

            if(large_difference_sm){
                large_difference = true;
            }
            regions_checked++;
            if(regions_checked > dist_regions_check_max){
                break;
            }
        }        

        int64_t e_sm_utime = bot_timestamp_now();
        
        if(added_edges.size() > 0){
            if(large_difference){
                graph->status = 0;
                output_writer->write_to_buffer("[Sample Edge] Large Difference in Scanmatch");
            }
            graph->runSlam();
        }

        int64_t e_slam_utime = bot_timestamp_now();

        sprintf(buf, "[Sample Edge] [Performance] SM Utime : %.4f - Slam Utime : %.4f", (e_sm_utime - s_sm_utime) / 1.0e6, (e_slam_utime - e_sm_utime) /1.0e6);
        output_writer->write_to_buffer(buf);
        
        if(param.printDistanceScanmatch){
            sprintf(buf, "[Sample Edge] No of edges checked : %d", regions_checked);
            output_writer->write_to_buffer(buf);
        }

        if(param.printLanguageScanmatch){
            fprintf(stderr, RED "Regions to check - semantic : %d\n" RESET_COLOR, (int) regions_to_check_for_semantic.size());
            sprintf(buf, "[Sample Edge] Regions to check - semantic : %d", (int) regions_to_check_for_semantic.size());
            output_writer->write_to_buffer(buf);
        }

        if(ignoreLanguageForEdges==1){
            sprintf(buf, "[Sample Edge] Ignoring language");
            output_writer->write_to_buffer(buf);
            fprintf(stderr, "Ignoring language\n");
        }

        if(ignoreLanguageForEdges==0 && regions_to_check_for_semantic.size() > 0){
            int no_region_edges_to_sample = 10;
            int semantic_regions_check_max = 3;
            int regions_checked = 0;
            int large_change = 0;
            for(int r = 0; r < regions_to_check_for_semantic.size(); r++){
                RegionNode *to_check_region = regions_to_check_for_semantic[r];
                vector<nodePairDist> semantic_prop_region_merges = proposeOldRegionListFromRegionSemantics(to_check_region, no_region_edges_to_sample);
                
                regions_checked = 0;
                int lang_edge_created = 0;
                //propose region based edges 
                for(int k=0; k < semantic_prop_region_merges.size(); k++){
                    double merge_ncut = 2;
                    double m_dist = semantic_prop_region_merges[k].second;
                    //remember to delete this
                    SlamNode *c_nd = semantic_prop_region_merges[k].first.first; 
                    SlamNode *p_nd = semantic_prop_region_merges[k].first.second;

                    RegionNode *r1 = c_nd->region_node; 
                    RegionNode *r2 = p_nd->region_node; 
                    if(r1->isConnected(r2) || r1 == ignore_region || r2 == ignore_region){
                        continue;
                    }
                    
                    int large_constraint_change = 0;
                    //do a scanmatch - do we do the extensive one??
                    int sucess = scanmatchRegionsExtensive(c_nd, p_nd, true, &large_constraint_change, true);//scanmatchRegions(c_nd, p_nd, true, true);
                    
                    if(sucess){
                        added_edges.push_back(r2);
                    }

                    if(large_constraint_change)
                        large_change = 1;
                    if(sucess)
                        lang_edge_created = 1;
          
                    regions_checked++;
                    if(regions_checked > semantic_regions_check_max){
                        break;
                    }
                }
                    
                if(lang_edge_created){
                    //force the graph to batch optimize
                    fprintf(stderr, "Sucessful Language SM Added - Doing batch optimization\n");
                    if(large_change)
                        graph->status = 0;
                    graph->runSlam();
                }

                if(param.printLanguageScanmatch){
                    sprintf(buf, "[Sample Edge] No of Semantic edges checked : %d", regions_checked);
                    output_writer->write_to_buffer(buf);
                }
            }
        }
        return added_edges;
    }    
}

vector<nodePairDist> SlamParticle::proposeOldRegionListFromRegion(RegionNode *region, int no_edges_to_sample, bool use_semantics){
    static char buf[1024];
    vector<nodePairDist> sampled_edges;
    //get the min distance to each region and then probabilistically select a region 
    //based on the distance 
    std::vector<nodePairDist> full_list;
    std::vector<nodePairDist> region_dist_list;

    //if we want the distributions for this 
    //get the close pairs of nodes // and then sum up the variances 
    //and then conver to a folded gaussian distribution 
    double m_dist = graph->findDistanceToRegionMeans(region, &full_list, 30);
    //graph->findMeanDistanceToRegions(region, &full_list);

    //
    //graph->findDistanceToRegions(region, &region_dist_list);

    //fprintf(stderr, "No of close regions : %d - Min Dist %f\n", (int) region_dist_list.size(), m_dist);
    if(full_list.size() == 0){
        return sampled_edges;
    }

    //this should be populated with the liklihood of the region being the same based on the dist
    
    std::vector<nodePairDist> region_score; 
    double sum_prob = 0;
    if(param.printDistanceScanmatch && param.verbose){
        fprintf(stderr, CYAN "Current Region : %d\n" RESET_COLOR, region->region_id);
    }
    //maybe we should throw out regions that are too far 
    
    for(int i=0; i< full_list.size(); i++){        
        if(full_list[i].second < param.regionIgnoreDist){
            region_dist_list.push_back(full_list[i]);
        }
    }

    double cov_time = 0;

    int64_t utime_s = bot_timestamp_now();
    for(int i=0; i< region_dist_list.size(); i++){  
        //we can override these nodes if we really need the closest nodes 

        RegionNode *r1 = region_dist_list[i].first.first->region_node;
        RegionNode *r2 = region_dist_list[i].first.second->region_node;
        
        //can give zero - will be trouble if it gave 0 for everything
        //double p_sem = fmax(0.01, getCosineSimilarity(r1->region_label_dist, r2->region_label_dist)); //
        
        //int to_add = check_to_add_basic(cl_nd1, cl_nd2, find_ground_truth, cov);
        //is this as expensive ?? - we call this here 

        //this might be the culprit 
        double d_prob = 0;
        if(1){
            //int64_t utime_s = bot_timestamp_now();
            //this is expensive and its killing us here 
            MatrixXd cov = graph->getCovariances(r1->mean_node, r2->mean_node);
            //int64_t utime_e = bot_timestamp_now();
            //cov_time += (utime_e - utime_s) / 1.0e6;
            d_prob = fmax(0.001, get_prob_to_propose_regions(r1->mean_node, r2->mean_node, cov, 10));
        }
        else{
            d_prob = get_prob_to_propose_regions_no_cov(r1->mean_node, r2->mean_node, 10);
        }

        //exit(-1);
        double prob = d_prob;

        double p_sem  = 0;

        //we should multiply this with the cosine similarity for the regions 
        if(use_semantics){
            p_sem = fmax(0.01, getCosineSimilarity(r1->region_type_dist, r2->region_type_dist));
            prob *= p_sem;
        }

        if(param.printDistanceScanmatch)
            fprintf(stderr, GREEN "Prob of region edge between Regions : %d - %d => C-sim : %.3f, Dist : %.3f ==> Combined : %f\n", 
                    r1->region_id, r2->region_id, p_sem, d_prob, prob);
        
        //fprintf(stderr, GREEN "Prob of region edge between Regions : %d - %d => %.3f\n", r1->region_id, r2->region_id, prob);
        
        //this was the one that was used earlier - which didn't take in to account the distribution of the regions 
        //double prob = get_prob_to_merge(region_dist_list[i].second, 0.3); //1/ (1.0 + 0.8*pow(region_dist_list[i].second,2)); 

        sprintf(buf, "[Sample Edges] [Propose Old Regions] [Prob] Region : %d - Dist : %f => Prob : %.3f", 
                region_dist_list[i].first.second->region_node->region_id, region_dist_list[i].second, prob);
        output_writer->write_to_buffer(buf);  

        region_score.push_back(make_pair(region_dist_list[i].first, prob));
    }
    int64_t utime_e = bot_timestamp_now();

    cov_time = (utime_e - utime_s) / 1.0e6;

    sprintf(buf, "[Sample Edges] [Propose Old Regions] [Performance] Time for prob calculation : %f (No Pairs : %d)", 
            cov_time, (int) region_dist_list.size());
    output_writer->write_to_buffer(buf);  

    param.verbose = 0;

    while(sampled_edges.size() < no_edges_to_sample && region_score.size() > 0){      
        double p_sum = 0;
        std::vector<nodePairDist> region_prob;
        double sum_prob = 0;
        for(int i=0; i< region_score.size(); i++){
            sum_prob +=region_score[i].second;
            region_prob.push_back(region_score[i]);
        }

        for(int i=0; i< region_prob.size(); i++){
            region_prob[i].second /= sum_prob;
        }
        
        // Draw a uniform random number
        double u_rand = gsl_rng_uniform (rng);
        
        double lower = 0;
        
        if(param.printDistanceScanmatch && param.verbose){
            fprintf(stderr, YELLOW "Random No : %f\n" RESET_COLOR, u_rand);
            
            for(int i=0; i< region_prob.size(); i++){
                fprintf(stderr, YELLOW "\tLower : %f - Upper : %f\n" RESET_COLOR, lower, (region_prob[i].second + lower));
                lower += region_prob[i].second;
            }
            lower = 0;
        }

        for(int i=0; i< region_prob.size(); i++){
            if(u_rand > lower && u_rand <= (region_prob[i].second + lower)){
                sampled_edges.push_back(region_dist_list[i]);
                if(param.printDistanceScanmatch && param.verbose){
                    fprintf(stderr, CYAN "[%d] Selected Region : %d => Normalized Prob : %.3f\n" RESET_COLOR, (int) sampled_edges.size(), 
                            region_dist_list[i].first.second->region_node->region_id, region_prob[i].second);
                }
                region_score.erase(region_score.begin() + i);
                region_dist_list.erase(region_dist_list.begin() + i);
                break;
            }
            lower += region_prob[i].second;
        }
    }
    return sampled_edges;
}

vector<nodePairDist> SlamParticle::proposeOldRegionListFromRegionSemantics(RegionNode *region, int no_edges_to_sample){

    vector<nodePairDist> sampled_edges;
    //get the min distance to each region and then probabilistically select a region 
    //based on the distance 
    std::vector<nodePairDist> full_list;
    std::vector<nodePairDist> region_dist_list;

    //if we want the distributions for this 
    //get the close pairs of nodes // and then sum up the variances 
    //and then conver to a folded gaussian distribution 
    double m_dist = graph->findDistanceToRegionMeans(region, &full_list, 200);
    
    //fprintf(stderr, "No of close regions : %d - Min Dist %f\n", (int) region_dist_list.size(), m_dist);
    if(full_list.size() == 0){
        return sampled_edges;
    }

    //this should be populated with the liklihood of the region being the same based on the dist
    
    std::vector<nodePairDist> region_score; 
    double sum_prob = 0;
    if(param.printLanguageScanmatch){
        fprintf(stderr, CYAN "Current Region : %d\n" RESET_COLOR, region->region_id);
    }
        
    for(int i=0; i< full_list.size(); i++){        
        if(full_list[i].second < param.regionLanguageIgnoreDist){
            RegionNode *r1 = full_list[i].first.first->region_node;
            RegionNode *r2 = full_list[i].first.second->region_node;
            
            double min_cosine = r1->labeldist->getBasicCosineSimilarity();

            //this just uses the label distribution 
            if(r1->labeldist->total_obs <=1.0 || r2->labeldist->total_obs <=1.0)
                continue;

            //fprintf(stderr, "Region %d - %d => Distance : %f\n", r1->region_id, r2->region_id, full_list[i].second);

            //we should check if they have been  influenced by different speech events 
            vector<int> l_ids1 = r1->labeldist->labeled_node_ids;
            vector<int> l_ids2 = r2->labeldist->labeled_node_ids;

            int different = 1; 
            for(int j=0; j < l_ids1.size(); j++){
                for(int k=0; k < l_ids2.size(); k++){
                    if(l_ids1[j] == l_ids2[k]){
                        different = 0;
                        break;                        
                    }
                }
            }
                
            //if they were only bled from the same even we should skip this loop closure
            if(different == 0)
                continue;
                
            
            map<int, double> ld1 = r1->labeldist->getProbabilities();
            map<int, double> ld2 = r2->labeldist->getProbabilities();

            //we should skip hallways 
            double c_prob = getLikelihoodOfEdge(r1->labeldist->saliency_map, ld1, ld2);
                
            if(c_prob < 0.15)
                continue;

            region_dist_list.push_back(full_list[i]);
        }
    }

    for(int i=0; i< region_dist_list.size(); i++){        
        //we should also skip these if the two nodes do not have any valid counts 
        //otherwise the hypothesis space will explode 
        RegionNode *r1 = region_dist_list[i].first.first->region_node;
        RegionNode *r2 = region_dist_list[i].first.second->region_node;

        //can give zero - will be trouble if it gave 0 for everything
        map<int, double> ld1 = r1->labeldist->getProbabilities();
        map<int, double> ld2 = r2->labeldist->getProbabilities();
        
        double c_prob = getCosineSimilarity(ld1, ld2);
        
        if(param.printLanguageScanmatch)
            fprintf(stderr, CYAN " Cosine : %f\n", c_prob);
        
        double prob = fmax(0.001, c_prob);
        
        //is this as expensive ?? - we call this here 
        MatrixXd cov = graph->getCovariances(r1->mean_node, r2->mean_node);

        if(param.printLanguageScanmatch)
            fprintf(stderr, GREEN "Prob of region edge between Regions : %d - %d => C-sim : %.3f\n", 
                    r1->region_id, r2->region_id, prob);
        
        if(param.printLanguageScanmatch){
            fprintf(stderr, CYAN "\tRegion : %d - Dist : %f => Prob : %.3f\n" RESET_COLOR , region_dist_list[i].first.second->region_node->region_id, region_dist_list[i].second, prob);
        }
        region_score.push_back(make_pair(region_dist_list[i].first, prob));
    }
    if(param.printLanguageScanmatch){
        fprintf(stderr, "\n\n");
    }

    param.verbose = 0;

    while(sampled_edges.size() < no_edges_to_sample && region_score.size() > 0){      
        double p_sum = 0;
        std::vector<nodePairDist> region_prob;
        double sum_prob = 0;
        for(int i=0; i< region_score.size(); i++){
            sum_prob +=region_score[i].second;
            region_prob.push_back(region_score[i]);
        }

        for(int i=0; i< region_prob.size(); i++){
            region_prob[i].second /= sum_prob;
        }
        
        // Draw a uniform random number
        double u_rand = gsl_rng_uniform (rng);
        
        double lower = 0;
        
        if(param.printLanguageScanmatch && param.verbose){
            fprintf(stderr, YELLOW "Random No : %f\n" RESET_COLOR, u_rand);
            
            for(int i=0; i< region_prob.size(); i++){
                fprintf(stderr, YELLOW "\tLower : %f - Upper : %f\n" RESET_COLOR, lower, (region_prob[i].second + lower));
                lower += region_prob[i].second;
            }
            lower = 0;
        }

        for(int i=0; i< region_prob.size(); i++){
            if(u_rand > lower && u_rand <= (region_prob[i].second + lower)){
                sampled_edges.push_back(region_dist_list[i]);
                if(param.printLanguageScanmatch && param.verbose){
                    fprintf(stderr, CYAN "[%d] Selected Region : %d => Normalized Prob : %.3f\n" RESET_COLOR, (int) sampled_edges.size(), 
                            region_dist_list[i].first.second->region_node->region_id, region_prob[i].second);
                }
                region_score.erase(region_score.begin() + i);
                region_dist_list.erase(region_dist_list.begin() + i);
                break;
            }
            lower += region_prob[i].second;
        }
    }
    return sampled_edges;
}

vector<nodePairDist> SlamParticle::proposeEdgesToOtherSegments(RegionSegment *segment, int no_edges_to_sample, bool use_closest_dist, bool ignore_connected){
    vector<nodePairDist> sampled_edges;
    //get the min distance to each region and then probabilistically select a region 
    //based on the distance 
    std::vector<nodePairDist> full_list;
    std::vector<nodePairDist> region_dist_list;
    double m_dist;
    
    graph->findDistanceToSegments(segment, full_list, 20);

    //fprintf(stderr, "No of close regions : %d - Min Dist %f\n", (int) region_dist_list.size(), m_dist);
    if(full_list.size() == 0){
        return sampled_edges;
    }

    //this should be populated with the liklihood of the region being the same based on the dist
    
    std::vector<nodePairDist> region_score; 
    double sum_prob = 0;
    if(param.verbose){
        fprintf(stderr, CYAN "Current Segment : %d\n" RESET_COLOR, segment->id);
    }
    //maybe we should throw out regions that are too far 
    
    for(int i=0; i< full_list.size(); i++){
        if(ignore_connected){
            RegionNode *r1 = full_list[i].first.first->region_node;
            RegionNode *r2 = full_list[i].first.second->region_node;
            if(r1->isConnected(r2)){
                continue;
            }           
        }
        if(full_list[i].second < param.regionIgnoreDist){
            region_dist_list.push_back(full_list[i]);
        }
    }

    for(int i=0; i< region_dist_list.size(); i++){  
        double prob = get_prob_to_merge(region_dist_list[i].second, 0.3); //1/ (1.0 + 0.8*pow(region_dist_list[i].second,2)); 
        if(param.verbose){
            fprintf(stderr, CYAN "\tSegment : %d - Dist : %f => Prob : %.3f\n" RESET_COLOR , region_dist_list[i].first.second->segment->id, region_dist_list[i].second, prob);
        }
        region_score.push_back(make_pair(region_dist_list[i].first, prob));
    }
    if(param.verbose){
        fprintf(stderr, "\n\n");
    }

    while(sampled_edges.size() < no_edges_to_sample && region_score.size() > 0){      
        double p_sum = 0;
        std::vector<nodePairDist> region_prob;
        double sum_prob = 0;
        for(int i=0; i< region_score.size(); i++){
            sum_prob +=region_score[i].second;
            region_prob.push_back(region_score[i]);
        }

        for(int i=0; i< region_prob.size(); i++){
            region_prob[i].second /= sum_prob;
        }
        
        // Draw a uniform random number
        double u_rand = gsl_rng_uniform (rng);
        
        double lower = 0;
        
        if(param.verbose){
            fprintf(stderr, YELLOW "Random No : %f\n" RESET_COLOR, u_rand);
            
            for(int i=0; i< region_prob.size(); i++){
                fprintf(stderr, YELLOW "\tLower : %f - Upper : %f\n" RESET_COLOR, lower, (region_prob[i].second + lower));
                lower += region_prob[i].second;
            }
            lower = 0;
        }

        for(int i=0; i< region_prob.size(); i++){
            if(u_rand > lower && u_rand <= (region_prob[i].second + lower)){
                sampled_edges.push_back(region_dist_list[i]);
                if(param.verbose){
                    fprintf(stderr, CYAN "[%d] Selected Segment : %d => Normalized Prob : %.3f\n" RESET_COLOR, (int) sampled_edges.size(), 
                            region_dist_list[i].first.second->segment->id, region_prob[i].second);
                }
                region_score.erase(region_score.begin() + i);
                region_dist_list.erase(region_dist_list.begin() + i);
                break;
            }
            lower += region_prob[i].second;
        }
    }
    return sampled_edges;
}

// ----------------------------- PofZ --------------------------------- //

//This does scanmatch for the top N edges and calculates the likelihood - 
double SlamParticle::getProbOfMeasurementLikelihoodSMRegion(RegionNode *region, RegionNode *region_skip, double *max_probability){
    //find the closest region?? and try to scanmatch to that 
    
    double min_prob = 0.05;
    
    //should we remove the connected regions??
    int max_no_edges_to_check = 3;
    int overlap = 0;

    vector<nodePairDist> edges_to_check; 
    
    if(overlap){
        double overlap_score = 0;
        for(int i=0; i < graph->region_node_list.size(); i++){
            RegionNode *c_region = graph->region_node_list[i];

            int nd_id, nd_c_id;
            //if regions are too far then don't consider overlap 
            double dist = region->getClosestDistanceToRegion(c_region, &nd_id, &nd_c_id);
            if(dist > 10.0)
                continue;
            //skip any regions 
            if(region == c_region || c_region == region_skip)
                continue;
            double ol = getPointOverlap(region, c_region);

            edges_to_check.push_back(make_pair(make_pair(graph->getSlamNodeFromID(nd_id), graph->getSlamNodeFromID(nd_c_id)), 1 - ol));
        }
        
        if(overlap_score < 0.3){
            return log(min_prob);
        }
    }
    else{
        double min_dist = 10000000;
        for(int i=0; i < graph->region_node_list.size(); i++){
            RegionNode *c_region = graph->region_node_list[i];
            int nd_id, nd_c_id;
            
            //skip any regions 
            if(region == c_region || c_region == region_skip)
                continue;
            double dist = region->getClosestDistanceToRegion(c_region, &nd_id, &nd_c_id);

            edges_to_check.push_back(make_pair(make_pair(graph->getSlamNodeFromID(nd_id), graph->getSlamNodeFromID(nd_c_id)), dist));     
        }
    }

    if(edges_to_check.size() == 0){
        fprintf(stderr, RED "No Close/Overlapping regions to check\n" RESET_COLOR);
        return log(min_prob);
    }
        
    std::sort(edges_to_check.begin(), edges_to_check.end(), compareNodePairDistance);

    int no_edges_to_check =  fmin(max_no_edges_to_check, edges_to_check.size());

    double c_prob = 1;
    
    for(int i=0; i < no_edges_to_check; i++){
        SlamNode *nd = edges_to_check[i].first.first;
        SlamNode *nd_c = edges_to_check[i].first.second;
        fprintf(stderr, "Node : %d - %d = %f\n", nd->id, nd_c->id, edges_to_check[i].second);
        RegionNode *closest_region = nd_c->region_node;
        
        //do a scanmatch with this region 
        pointlist2d_t *c_plist = getPointsForRegion(nd, 5.0, true);//getPointsForRegion(current_region);
        pointlist2d_t *t_plist = getPointsForRegion(nd_c, 5.0, true);
                    
        BotTrans o_match_to_pf = getBotTransFromPose(nd->getPose());
        BotTrans target_to_pf = getBotTransFromPose(nd_c->getPose());
        BotTrans pf_to_target = target_to_pf;
        bot_trans_invert(&pf_to_target);
                    
        BotTrans o_match_to_target; 
        bot_trans_apply_trans_to(&pf_to_target, &o_match_to_pf, &o_match_to_target);
                    
        double score = 0; 
        int matches = 0;
        BotTrans sm_to_target = o_match_to_target;
                    
        //call the scanmatcher - also get the uncertainty of the scanmatch result 
        double variance[9] = {0};
        int64_t s_utime = bot_timestamp_now();
        scanmatchRegionsMRPTQuick(t_plist, c_plist, sm_to_target, 
                                  &score, &matches, variance);
                    
        int64_t e_utime = bot_timestamp_now();
        fprintf(stderr, CYAN "Time to Scanmatch PofZ : %.3f\n" RESET_COLOR, (e_utime - s_utime) / 1.0e6);
        pointlist2d_free(c_plist);
        pointlist2d_free(t_plist);
    
        Pose2d m_tf = getPoseFromBotTrans(o_match_to_target);
        Pose2d sm_tf = getPoseFromBotTrans(sm_to_target);

        fprintf(stderr, CYAN "[%d] (%d - %d) Map Constraint : %f,%f,%f -> SM constraint : %f,%f,%f\n" RESET_COLOR, graph_id, region->region_id, closest_region->region_id, m_tf.x(), m_tf.y(), m_tf.t(), 
                sm_tf.x(), sm_tf.y(), sm_tf.t());

        fprintf(stderr, CYAN "Covariance : %f,%f,%f\t\t%f,%f,%f\t\t%f,%f,%f\n" RESET_COLOR, 
                variance[0], variance[1], variance[2], 
                variance[3], variance[4], variance[5], 
                variance[6], variance[7], variance[8]);
        fprintf(stderr, CYAN "Region [%d] - [%d] => Score : %f\n" RESET_COLOR, region->region_id, closest_region->region_id, score);
    
        //give higher cov - if the scanmatch failed
        /*if(hit_pct < 0.4){
          cov_sm(0,0) = variance[0];
          cov_sm(0,1) = variance[1];
          cov_sm(1,1) = 0.02;
          cov_sm(2,2) = 0.01;
          }*/

        //calculate the covariance 
        Matrix3d cov_sm = Matrix3d(variance);
    
        double prob = graph->calculateProbability(nd, nd_c, cov_sm, sm_tf);
    
        //fprintf(stderr, CYAN "Probability : %f\n" RESET_COLOR, prob);

        if(prob < min_prob)
            prob = min_prob;

        c_prob *= prob;
    }

    return log(c_prob);
}

//This does scanmatch for the top N edges and calculates the likelihood - 
double SlamParticle::getLikelihoodOfConstraint(SlamNode *nd_1, SlamNode *nd_2, Pose2d transf, Matrix3d cov_sm){
    
    BotTrans o_match_to_pf = getBotTransFromPose(nd_1->getPose());
    BotTrans target_to_pf = getBotTransFromPose(nd_2->getPose());
    BotTrans pf_to_target = target_to_pf;
    bot_trans_invert(&pf_to_target);
                    
    BotTrans o_match_to_target; 
    bot_trans_apply_trans_to(&pf_to_target, &o_match_to_pf, &o_match_to_target);
    
    BotTrans sm_to_target = getBotTransFromPose(transf);
                    
    //not sure if this is what we should use - but lets stick with this for now 
    double prob = graph->calculateProbability(nd_1, nd_2, cov_sm, transf);
    
    //fprintf(stderr, CYAN "Probability : %f\n" RESET_COLOR, prob);

    return prob; 
}


//calculates the likelihood for each new node 
void SlamParticle::calculateObservationProbabilityOfNode(SlamNode *node, double *maximum_probability){
    //for each node in the region (or maybe the nodes that were not already processed??
    //maybe the region and then scanmatch is not using the same information as the vanilar laser points 
    //log likelihood
    
    //should we skip the entire region??
    //double pofz = getProbOfMeasurementSM(node);        
    double pofz = getProbOfMeasurementLikelihoodNode(node, maximum_probability);        
    //fprintf(stderr, "Log likelihood - %f  => Prob : %f\n", log(pofz), pofz);

    weight += log(pofz); 

    //this will have implications if we resample - since the weights of the particles are not synchronized - i.e. some of them might not be calculated yet
}

/* 
   Used to do the actual likelihood calculation 
*/

double SlamParticle::getProbOfMeasurementLikelihoodNode(SlamNode *node, double *max_probability){
    
    // Settings that determine behavior
    // The distance threshold used for kNN
    double radius_bound = 0.1;//0.25; // Distance threshold for NN (types (i) and (iii))
    double point_match_threshold = 0.1; // Distance threshold for valid matches (type (i))
    double prob_2 = 0.05; // Probability associated with type 2 
    double prob_3 = 0.05; // Probability associated with type 3

    //querry the kd tree - find the closest point 
    Pose2d tocheck_value = node->getPose();
    Scan *tocheck_scan = node->slam_pose->scan;
    double dist = 0;
    double pos[2], next_pos[2];
    double start[2];

    // Node->Slam_Pose->Scan laser scans are decimated and in the body frame
    // We will generate the local occupancy grid in the Query Body frame
    BotTrans localToQueryBody;
    BotTrans queryBodyToLocal = getBotTransFromPose(tocheck_value);
    localToQueryBody = queryBodyToLocal;
    bot_trans_invert (&localToQueryBody);

    node->resetProbability();

    BotTrans matchBodyToLocal;
    BotTrans matchBodyToQueryBody;
    BotTrans queryBodyToMatchBody;

    double rpyMatchBody[3] = {0.0, 0.0, 0.0};

    int query_size = 1;
    double epsilon =  0.01;//0.1;
   
    ANNidxArray  nnIdx = new ANNidx[query_size];
    ANNdistArray dists = new ANNdist[query_size];

    ANNpoint qp = annAllocPt(2);

    int64_t s_utime = bot_timestamp_now();

    int64_t time_to_search = 0;

    int count = 0;
    // Find the nodes (and scans) near the local search area
    double pQuery[3] = {.0};
    double pLaser[3];
    double pQueryInMatched[3];

    int valid_trees = 0;
    int kd_searches = 0;
    int first = 1;

    double dist_threshold = 10.0;
    int max_no_nodes = 5;
    vector< pair<int,int> > matched_nodeid_pointidx;

    //we can prob cache this search - when we do the correspondance - depending on how we choose to do it 

    vector<SlamNode *> valid_nodes;
    //find the valid nodes to search - and cache the transform 

    int use_correspondance = 1; //this might be expensive 

    if(use_correspondance){
        double skip_threshold = 0.95;
        vector<nodeDist> valid_node_dist;
        for(int i=0; i < graph->region_node_list.size(); i++){
            RegionNode *region = graph->region_node_list[i];
            //if(region == node->region_node)
            //  continue;

            for(int j=0; j < region->nodes.size(); j++){           
                SlamNode *nd = region->nodes[j];
                if(nd == node)
                    continue;
                //or should we just use the correspondance - since we have that already??
                double dist = getDistanceBetweenNodes(node, nd);
                if(dist < dist_threshold){
                    Pose2d value = nd->getPose();
                    nd->valid = 1;
            
                    ScanTransform T;
                    T.x = value.x();
                    T.y = value.y();
                    T.theta = value.t();

                    // Determine the transform from Match Body ---> Query Body
            
                    // Set the BotTrans from Match BODY to LOCAL
                    matchBodyToLocal.trans_vec[0] = value.x();
                    matchBodyToLocal.trans_vec[1] = value.y();
                    matchBodyToLocal.trans_vec[2] = 0.0;
                    rpyMatchBody[2] = value.t();
                    bot_roll_pitch_yaw_to_quat (rpyMatchBody, matchBodyToLocal.rot_quat);
            
                    // ... and now Match Body ---> Query Body
                    bot_trans_apply_trans_to (&localToQueryBody, &matchBodyToLocal, &matchBodyToQueryBody);
                    nd->queryBodyToMatchBody = matchBodyToQueryBody;
                    bot_trans_invert(&nd->queryBodyToMatchBody);

                    //inverted - because of the sorting function 
                    double score = 1 - getUpdatedCorrespondenceScore(node, nd);
                    if(score >= skip_threshold)
                        continue;
                    valid_node_dist.push_back(make_pair(nd, score));
                }
            }
        }
        std::sort(valid_node_dist.begin(), valid_node_dist.end(),compareDistance);
        for(int i=0; i < valid_node_dist.size(); i++){
            //fprintf(stderr, "\tNode : %d - %f\n", valid_node_dist[i].first->id, valid_node_dist[i].second);
            if(i >=max_no_nodes){
                break;
            }
            valid_nodes.push_back(valid_node_dist[i].first);
        }
    }
    else{
        vector<nodeDist> valid_node_dist;
        for(int i=0; i < graph->region_node_list.size(); i++){
            RegionNode *region = graph->region_node_list[i];
            //if(region == node->region_node)
            //  continue;

            for(int j=0; j < region->nodes.size(); j++){           
                SlamNode *nd = region->nodes[j];
                if(nd == node)
                    continue;
                //or should we just use the correspondance - since we have that already??
                double dist = getDistanceBetweenNodes(node, nd);
                if(dist < dist_threshold){
                    Pose2d value = nd->getPose();
                    nd->valid = 1;
            
                    ScanTransform T;
                    T.x = value.x();
                    T.y = value.y();
                    T.theta = value.t();

                    // Determine the transform from Match Body ---> Query Body
            
                    // Set the BotTrans from Match BODY to LOCAL
                    matchBodyToLocal.trans_vec[0] = value.x();
                    matchBodyToLocal.trans_vec[1] = value.y();
                    matchBodyToLocal.trans_vec[2] = 0.0;
                    rpyMatchBody[2] = value.t();
                    bot_roll_pitch_yaw_to_quat (rpyMatchBody, matchBodyToLocal.rot_quat);
            
                    // ... and now Match Body ---> Query Body
                    bot_trans_apply_trans_to (&localToQueryBody, &matchBodyToLocal, &matchBodyToQueryBody);
                    nd->queryBodyToMatchBody = matchBodyToQueryBody;
                    bot_trans_invert(&nd->queryBodyToMatchBody);
                    valid_node_dist.push_back(make_pair(nd, dist));
                }
            }
        }
        std::sort(valid_node_dist.begin(), valid_node_dist.end(),compareDistance);
        for(int i=0; i < valid_node_dist.size(); i++){
            //fprintf(stderr, "\tNode : %d - %f\n", valid_node_dist[i].first->id, valid_node_dist[i].second);
            if(i >=max_no_nodes){
                break;
            }
            valid_nodes.push_back(valid_node_dist[i].first);
        }
    }
    //vaid no of kd trees 

    //should only check with a maximum no of trees
    valid_trees = valid_nodes.size();
    
    if(valid_nodes.size() == 0){
        fprintf(stderr, "No observations to match against : Giving prob of 1\n");
        return 1;
    }

    double cum_prob = 0;
    double avg_prob = 0;
    double max_prob = 0;  

    int no_of_matches = 0;
    int no_points_matched = 0;
    // For each point in query scan, find the closest point among the scans of each nearby node (valid_nodes)
    for(unsigned i = 0; i < tocheck_scan->numPoints; i++){
        int my_no_matches = 0;
        int point_type = 0; // Either 1, 2, or 3
        
        pQuery[0] = tocheck_scan->points[i].x;
        pQuery[1] = tocheck_scan->points[i].y;
               
        double pQueryLocal[3];
        double pMatchedLocal[3];

        // Transform the query point into the local frame
        bot_trans_apply_vec(&queryBodyToLocal, pQuery, pQueryLocal);

        //fprintf(stderr, "Querry Point in Local frame : %f,%f\n", pQueryLocal[0], pQueryLocal[1]);

        double min_dist = 1000; 
        int close_ind  = -1;
        double pMatched[3] = {0.}; 

        int match_id = -1;
        int match_idx = -1;
        // loop through the neighbor nodes to find the nearest scan point
        int found_match = 0;
        
        for(int k = 0; k < valid_nodes.size(); k++){
            SlamNode *nd = valid_nodes.at(k); 
            
            kd_searches++;

            // Transform the point from the query pose frame to the match frame
            bot_trans_apply_vec ( &nd->queryBodyToMatchBody, pQuery, pQueryInMatched);
                
            qp[0] = pQueryInMatched[0];
            qp[1] = pQueryInMatched[1];
            
            int64_t search_utime = bot_timestamp_now();

            nd->slam_pose->kd_tree->annkFRSearch (qp,                    // query point
                                                  ANN_POW(radius_bound), // squared radius search bound
                                                  query_size,            // number of near neighbors
                                                  nnIdx,                 // nearest neighbors (returned)
                                                  dists,                 // distance (returned)
                                                  epsilon);
            
            int64_t search_end_utime = bot_timestamp_now();
            time_to_search += (search_end_utime - search_utime);
            int found = 0;

            
            // Point is type 2 if there are no NN
            if (nnIdx[0] < 0)
                point_type = 2;
            else
                point_type = 3; // Assume outlier for now
            
            // Loop through the set of nearest scan points
            for(int l=0;l< query_size ;l++){
                if(nnIdx[l] < 0){//because we have reached the end of the valid neighbours
                    break; 
                    //continue;
                }
                
                double dist_val = sqrt(dists[l]); 
                
                // Doensn't pass the test to be type 1
                if (dist_val > point_match_threshold)
                    continue;

                found_match = 1;
                //count++;
                found = 1;
                point_type = 1;
                no_of_matches ++;
                my_no_matches ++;
                //dists[l] = sqrt(dists[l]); // MRW: Commented out
                //cout << i << " " << nnIdx[l] << " " << dists[l] << "\n";

                //nnIdx[l];
                
                vector< pair<int,int> >::iterator pit;
                pair <int, int> test_id_idx(nd->id, nnIdx[(l)]);
                pit = find ( matched_nodeid_pointidx.begin(),  matched_nodeid_pointidx.end(), test_id_idx);

                if(min_dist > dist_val && pit ==  matched_nodeid_pointidx.end()){
                    min_dist = dist_val;
                    ANNpointArray kdtree_points = nd->slam_pose->kd_tree->thePoints();
                    pMatched[0] = (kdtree_points[nnIdx[(l)]][(0)]);
                    pMatched[1] = (kdtree_points[nnIdx[(l)]][(1)]);
                    pMatched[2] = 0.0;
                    close_ind = k;
                    
                    match_id = nd->id;
                    match_idx = nnIdx[(l)];
                }               
            }
        }
        if(found_match){
            no_points_matched++;
        }

        //fprintf(stderr, "Distance - recal : %f\n", hypot(pMatched[0] - pQueryInMatched[0], pMatched[1] - pQueryInMatched[1]));
        //fprintf(stderr, "Min Tree : %d\n", close_ind);
        
        double prob;

        // Did we find a match?
        // Point type 1
        if(close_ind >=0 && min_dist < radius_bound){
            pair <int, int> id_idx(match_id, match_idx);
            matched_nodeid_pointidx.push_back (id_idx);

            SlamNode *nd = valid_nodes.at(close_ind);

            Pose2d match = nd->getPose();

            // Determine the transform from Match Body ---> Query Body
            
            // Set the BotTrans from Match BODY to LOCAL
            matchBodyToLocal.trans_vec[0] = match.x();
            matchBodyToLocal.trans_vec[1] = match.y();
            matchBodyToLocal.trans_vec[2] = 0.0;
            rpyMatchBody[2] = match.t();
            bot_roll_pitch_yaw_to_quat (rpyMatchBody, matchBodyToLocal.rot_quat);
            bot_trans_apply_vec (&matchBodyToLocal, pMatched, pMatchedLocal);

            if(0 && param.drawScanmatch){
                bot_lcmgl_point_size (lcmgl_sm_graph, 3);
                bot_lcmgl_begin (lcmgl_sm_graph, GL_POINTS);
                bot_lcmgl_color3f (lcmgl_sm_graph, 0.0, 1.0, 0.0);
                bot_lcmgl_vertex3f (lcmgl_sm_graph, pQueryLocal[0], pQueryLocal[1], 0);
                bot_lcmgl_color3f (lcmgl_sm_graph, 1.0, 0.7, 0.0);
                bot_lcmgl_vertex3f (lcmgl_sm_graph, pMatchedLocal[0], pMatchedLocal[1], 0);
                bot_lcmgl_end (lcmgl_sm_graph);
                
                bot_lcmgl_color3f (lcmgl_sm_graph, 1.0, 0.0, 0.0);
                bot_lcmgl_line_width (lcmgl_sm_graph, 3);
                bot_lcmgl_begin (lcmgl_sm_graph, GL_LINES);
                bot_lcmgl_vertex3f (lcmgl_sm_graph, pQueryLocal[0], pQueryLocal[1], 0);
                bot_lcmgl_vertex3f (lcmgl_sm_graph, pMatchedLocal[0], pMatchedLocal[1], 0);
                bot_lcmgl_end (lcmgl_sm_graph);
            }
            
            //fprintf(stderr, "Matched Point in Local frame : %f,%f\n", pMatchedLocal[0], pMatchedLocal[1]);
            int64_t s_t = bot_timestamp_now();    
            //get the covariance of the scan point based on the uncertainity of the 
            MatrixXd cov_querry_in_particle_frame = getObservationCovarianceInParticleFrame(pQuery, node);
            int64_t e_t = bot_timestamp_now();
            //fprintf(stderr, "Dist : %f\n", hypot(pMatchedLocal[0] - pQueryLocal[0] , pMatchedLocal[1] - pQueryLocal[1]));
            
            MatrixXd cov_match_in_particle_frame = getObservationCovarianceInParticleFrame(pMatched, nd);
            //looks reasonable enough 
            //cout << endl << "Match Cov : " << endl << cov_match_in_particle_frame << endl; 
            prob = getObservationProbability(cov_match_in_particle_frame, cov_querry_in_particle_frame, 
                                             pMatchedLocal, pQueryLocal);

            // This probability represents the number of matches as a fraction of the maximum possible (one per matched node)
            // we add 1 to each to deal with the case of scans from new areas (see below)
            //double prob = (my_no_matches+1)/(valid_nodes.size() + 1);

            //this is the fixed model - wont take in to account the cov of the nodes 
            //double prob =  getObservationProbabilityFixed(pMatchedLocal, pQueryLocal);

            //fprintf(stderr, "Found => Min dist : %f Prob : %f\n", min_dist, prob);
            
            if(prob > 1.0)
                prob = 1.0;
            
            if(prob < 0.05){
                prob = 0.05;//prob_2 + 0.05;
            }
            
            /*if(prob < 0.1){
                //fprintf(stderr, "Prob too low\n");
                prob = 0.1;//prob_2 + 0.05;
                }*/

            //if(prob < 0.5) 
            //    prob = 0.5; 

            //if (prob < 0.1)
            //    prob = 0.1;

            //double full_prob = prob; // 0.6 * prob + 0.4 * 0.2
            
            //fprintf(stderr, "Found => Min dist : %f Prob : %f\n", min_dist, prob);

            if(max_prob < prob)
                max_prob = prob;

            //saved in the slam node - to be used when updating 
            //last_nd->prob_of_scan[i] = full_prob;
            //fprintf (stdout,"\t\t my_no_matches = %d, valid_nodes.size() = %d\n", my_no_matches, valid_nodes.size());
            /*avg_prob += prob; //0.6 * prob + 0.4 * 0.2; 
              cum_prob += log(prob); //log(0.6 * prob + 0.4 * 0.2);// prob);*/
            //avg_prob += full_prob; //0.6 * prob + 0.4 * 0.2; 
            //cum_prob += log(full_prob);//0.6 * prob + 0.4 * 0.2);
            //fprintf(stderr, "\tProb : %f\n", prob);
            count++;

        }
        else{

            if (point_type == 3) 
                prob = prob_3;
            else
                prob = prob_2;

            //fprintf(stderr, "Not found => Min dist : %f Prob : %f\n", min_dist, 0.4 * 0.8);
            //avg_prob += 0.5;//0.4 * 0.8; 
            //cum_prob += log(0.5);//0.4 * 0.8);
            //avg_prob += 1/(valid_nodes.size()+1);
            //cum_prob += log(1/(valid_nodes.size()+1));
            //last_nd->prob_of_scan[i] =  0.5;//0.4 * 0.8;//0; 
            //last_nd->prob_of_scan[i] =  1/(valid_nodes.size()+1);//0.4 * 0.8;//0; 
        }

        avg_prob += prob;
        cum_prob += log (prob);
        node->prob_of_scan[i] = prob;

    }

    //fprintf(stderr, YELLOW "\t\t [%d] Slam Graph No of Unique : %d No of matches (multiple) : %d\n" RESET_COLOR, graph_id, no_points_matched, no_of_matches);
    
    node->max_scan_prob = max_prob; 
    *max_probability = max_prob;
    avg_prob /= tocheck_scan->numPoints;//count, 
        
    if(param.verbose)
        fprintf(stderr, YELLOW "Count : %d Max Prob : %f Avg : %f Cumalative Prob : (log) %f => %f \n" RESET_COLOR, 
                count, max_prob, avg_prob, 
                cum_prob, exp(cum_prob));
 
    annDeallocPt(qp);
    delete [] nnIdx;
    delete [] dists; 
    int64_t e_utime = bot_timestamp_now();
    if(param.verbose){
        fprintf(stderr, "\t Total : %d => Matched Count : %d\n", tocheck_scan->numPoints, count);
        fprintf(stderr, "\t Valid Trees : %d => Total Searches : %d Time per search : %f\n", valid_trees, kd_searches, (time_to_search) / 1.0e6 / (double) kd_searches);
        fprintf(stderr, "Time to serach kd tree : %f\n", (time_to_search) / 1.0e6);
    }
    
    bot_lcmgl_switch_buffer (lcmgl_sm_graph);
    cum_prob -= log(max_prob) * count; 
    //return exp(cum_prob);
    return avg_prob; //exp(cum_prob); //avg_prob / count; //exp(cum_prob); //count / (double) tocheck_scan->numPoints;
}

// ------------------ PofZ : Utils --------------- //
//this is the individual observation prob calculation done right now - this should be changed 
MatrixXd SlamParticle::getObservationCovarianceInParticleFrame(double pBody[3], SlamNode *nd){

    int64_t s_time = bot_timestamp_now();
    Pose2d tocheck_value = nd->getPose();
    double r_var = pow(0.3, 2);
    double t_var = pow(0.2, 2);

    double x_laser_offset = laser_to_body.trans_vec[0];
    double y_laser_offset = laser_to_body.trans_vec[1];
    double rpy_laser_offset[3];
    bot_quat_to_roll_pitch_yaw (laser_to_body.rot_quat, rpy_laser_offset);
    double theta_laser_offset = rpy_laser_offset[2];
    double c_l = cos(theta_laser_offset);
    double s_l = sin(theta_laser_offset);

    double pLaser[3];
    //calculate the theta in the laser frame         
    bot_trans_apply_vec (&body_to_laser, pBody, pLaser);

    double r = hypot(pLaser[0] , pLaser[1]);
    double theta = atan2(pLaser[1], pLaser[0]);
    double c = cos(theta);
    double s = sin(theta);
    // Covariance of (x,y) in laser frame 
    MatrixXd cov_laser(2,2); 
    
    //this assumes a variance on both r and theta 
    Matrix2d H0  = Matrix2d::Zero();
    H0(0,0) = c_l;
    H0(1,0) = s_l;
    H0(0,1) = -r * s_l;
    H0(1,1) = r * c_l;
 
    MatrixXd cov_l(2,2);
    cov_l.setZero();
    cov_l(0,0) = r_var;
    cov_l(1,1) = t_var;
    
    cov_laser = H0 * cov_l * H0.transpose();

    //up to here its correct 

    // we will use a fixed model 
    

    //this is the cov if you assume no uncertainity in the theta 
    
    /*cov_laser(0,0) = cov_laser(1,1) = c * c * r_var;
      cov_laser(0,1) = cov_laser(1,0) = s * s * r_var;*/
    

    //jacobian for going from laser (x,y) to body (x,y) 
    MatrixXd H1(2,2); 
    H1(0,0) = H1(1,1) = c_l;
    H1(0,1) = - s_l;
    H1(1,0) = s_l;

    //cov of the query laser in body frame
    MatrixXd cov_body_querry = H1 * cov_laser * H1.transpose();

    //this might need to have the theta uncertainity also 
        
    //cout << cov_body_querry; 
    //3x3 cov matrix for the querry node 
    MatrixXd cov_querry_body_in_local = graph->getCovariance(nd);

    //build the full covariance matrix 
    MatrixXd cov_querry_frame_and_point(5,5);

    //fill this matrix 
    cov_querry_frame_and_point.setZero(5,5);
    for(int r_id = 0; r_id < 3; r_id++){
        for(int c_id = 0; c_id < 3; c_id++){
            cov_querry_frame_and_point(r_id,c_id) = cov_querry_body_in_local(r_id,c_id);
        }
    }

    for(int r_id = 0; r_id < 2; r_id++){
        for(int c_id = 0; c_id < 2; c_id++){
            cov_querry_frame_and_point(r_id + 3,c_id +3) = cov_body_querry(r_id,c_id);
        }
    }

    double c_querry_frame = cos(tocheck_value.t());
    double s_querry_frame = sin(tocheck_value.t());
    MatrixXd H2(2,5);
    H2.setZero(2,5);
    H2(0,0) = 1;
    H2(0,2) = -s_querry_frame *pBody[0] - c_querry_frame * pBody[1];
    H2(0,3) = c_querry_frame;
    H2(0,4) = -s_querry_frame;
    H2(1,1) = 1;
    H2(1,2) = c_querry_frame *pBody[0] - s_querry_frame * pBody[1];
    H2(1,3) = s_querry_frame;
    H2(1,4) = c_querry_frame;
        
    //ok - i think the jacobian is correct 
    MatrixXd cov_querry_in_particle_frame = H2 * cov_querry_frame_and_point * H2.transpose();

    int64_t e_time = bot_timestamp_now();

    //cout << endl << cov_querry_in_particle_frame << endl;

    return cov_querry_in_particle_frame;
}

double SlamParticle::getObservationProbability(MatrixXd cov_match_in_particle_frame, MatrixXd cov_querry_in_particle_frame, 
                                               double pMatch[2], double pQuery[2]){
    //these points need to be in the particle frame - not their body frame 

    //will this have the same issue as the edge creation stuff???
    //in which case - we should evaluvate the two transforms - not the distances 
    
    //i.e. (x_match-x_querry) , (y_match - y_querry)

    MatrixXd H(1,4);

    double d = hypot(pMatch[0] - pQuery[0], pMatch[1] - pQuery[1]);
   
    if(d==0)
        d = 0.0001;

    H(0,0) = (pMatch[0] - pQuery[0])/ d;
    H(0,1) = (pMatch[1] - pQuery[1])/d;
    H(0,2) = -(pMatch[0] - pQuery[0])/d;
    H(0,3) = -(pMatch[1] - pQuery[1])/d;

    if(0){
        fprintf(stderr, "Diff : %f,%f Match : %f, %f => Querry : %f,%f\n", 
                pMatch[0] - pQuery[0], 
                pMatch[1] - pQuery[1], 
                pMatch[0], pMatch[1], 
                pQuery[0], pQuery[1]);
    }

    double mean[2] = {pMatch[0] - pQuery[0], pMatch[1] - pQuery[1]};

    //build the full covariance 
    MatrixXd point_cov(4,4);
    point_cov.setZero(4,4);
    
    for(int i=0; i < 2; i++){
        for(int j=0; j < 2; j++){
            point_cov(i,j) = cov_match_in_particle_frame(i,j);
        }
    }

    for(int i=0; i < 2; i++){
        for(int j=0; j < 2; j++){
            point_cov(i+2,j+2) = cov_querry_in_particle_frame(i,j);
        }
    }

    //maybe this covariance is way too high??
    MatrixXd cov = H * point_cov * H.transpose();
        
    //cout << "Mean : " << d << endl << "Stdev : " << pow(cov(0,0),0.5) << endl; 

    double full_prob = get_observation_probability(d, pow(cov(0,0),0.5));//get_observation_probability(d, pow(cov(0,0),0.5));

    //double full_prob = get_observation_probability(0.4, 0.4);

    //cout << "Full Prob : " << full_prob << endl;

    return full_prob; //1;//full_prob;
}

// ------------------- Segmentation ------------------------//

//we need a function that handles segmentations 
RegionNode* SlamParticle::segmentRegionGeneral(RegionNode *current_region){
    static char buf[1024];
    
    RegionNode *new_region  = NULL;
    
    SlamNode *last_added_node = current_region->getLastNode();

    bool door_transition = last_added_node->slam_pose->transition_node;
    //general method that decides to segment the current region or not 
    
    int minRegionSizeToSegment = 3; 
    if(door_transition && current_region->getSize() < minRegionSizeToSegment){
        return new_region;
    }

    if(param.spectralClustering){
        if(door_transition){
            sprintf(buf, "[Segment Region] Segmenting based on door detection");
            output_writer->write_to_buffer(buf);  
        
            current_region->removeNode(last_added_node);
            RegionNode *split_region = graph->createNewRegion();
            graph->addNodeToRegion(last_added_node, split_region);
            new_region = split_region;
            if(param.clearDeadEdges)
                clearDummyConstraints();
            current_region->updateMean();
            updateRegionRayTrace(current_region);

            //we might want to segment this out 
        }
        else{
            //use spectral clustering to split the regions 
            double n_cut = 2;
            double u_rand_split = gsl_rng_uniform (rng);

            //smaller N_Cut means better seperation between the two segments - i.e. higher likelihood of segmentation 
            vector< vector<SlamNode *> > segments = segmentRegion(current_region, &n_cut);
            double prob_to_segment = get_prob_to_segment_from_ncut(n_cut);

            sprintf(buf, "[Segment Region] [Sample Spectral] N Cut value : %f", n_cut);
            output_writer->write_to_buffer(buf);  

            //keep track of which the current segment is (even if the likelihood is too low) 
            if(segments.size() > 1){
                current_segment.clear(); 
                
                for(int k=0; k < segments.size(); k++){
                    vector<SlamNode *> seg = segments[k];
                    for(int l=0; l < seg.size(); l++){
                        SlamNode *nd = seg[l];                            
                        //current_region->removeNode(nd);
                        if(nd == last_added_node){
                            for(int k=0; k < seg.size(); k++){
                                current_segment.insert(nd);
                            }
                            break;
                        }
                    }
                }
            }
                        
            if(segments.size() > 1 && (n_cut < param.segmentSplitThreshold && u_rand_split < prob_to_segment)){

                //make sure that both segments have at least the minimum members 
                bool size_check_valid = true;
                
                for(int k=0; k < segments.size(); k++){
                    if(segments[k].size() < param.minRegionSizeToSegment){
                        size_check_valid = false;
                        sprintf(buf, "[Segment Region] [Sample Spectral] Min segment size check failed");
                        output_writer->write_to_buffer(buf);  
                        break;
                    }
                }

                if(size_check_valid){
                    //split these nodes to new regions 
                    //remove the new region nodes from the current nodes 
                
                    //make sure that the new regions are created and the current region is updated properly
                    //always put the newly created node into the new region 
                    if(segments.size() > 2){
                        sprintf(buf, "[Segment Region] [Sample Spectral] [Error] Received more segments than expected : %d", (int) segments.size());
                        output_writer->write_to_buffer(buf);  
                    }
                
                    sprintf(buf, "[Segment Region] [Sample Spectral] Segmenting Regions");
                    output_writer->write_to_buffer(buf);  

                    int new_current_region_id = 0;
                    for(int k=0; k < segments.size(); k++){
                        vector<SlamNode *> seg = segments[k];
                        for(int l=0; l < seg.size(); l++){
                            SlamNode *nd = seg[l];                            
                            //current_region->removeNode(nd);
                            if(nd == last_added_node){
                                new_current_region_id = k;
                                break;
                            }
                        }
                    }
                
                    vector<SlamNode *> to_remove_seg = segments[new_current_region_id];
                    for(int l=0; l < to_remove_seg.size(); l++){
                        SlamNode *nd = to_remove_seg[l];                            
                        current_region->removeNode(nd);
                    }

                    //move the nodes to a new region 
                    RegionNode *split_region = graph->createNewRegion();
                    for(int l=0; l < to_remove_seg.size(); l++){
                        SlamNode *nd = to_remove_seg[l];
                        graph->addNodeToRegion(nd, split_region);
                    }
                    current_region->updateMean();
                    //need to update the region mean 
                    updateRegionRayTrace(current_region);
                    new_region = split_region;

                    if(param.clearDeadEdges)
                        clearDummyConstraints();
                }
            }
        }
    }
    else{
        if(param.useSpectralFixed){
            if(door_transition){

                sprintf(buf, "[Segment Region] [Fixed Spectral] Segmenting based on door detection");
                output_writer->write_to_buffer(buf);

                current_region->removeNode(last_added_node);
                RegionNode *split_region = graph->createNewRegion();
                graph->addNodeToRegion(last_added_node, split_region);
                new_region = split_region;
                if(param.clearDeadEdges)
                    clearDummyConstraints();
                updateRegionRayTrace(current_region);
            }
            else{
                //use spectral clustering to split the regions 
                double n_cut = 2;
                double u_rand_split = gsl_rng_uniform (rng);

                //smaller N_Cut means better seperation between the two segments - i.e. higher likelihood of segmentation 
                vector< vector<SlamNode *> > segments = segmentRegion(current_region, &n_cut);
                double prob_to_segment = get_prob_to_segment_from_ncut(n_cut);

                sprintf(buf, "[Segment Region] [Fixed Spectral] N Cut value : %f", n_cut);
                output_writer->write_to_buffer(buf);  

                //keep track of which the current segment is (even if the likelihood is too low) 
                if(segments.size() > 1){
                    current_segment.clear(); 
                
                    for(int k=0; k < segments.size(); k++){
                        vector<SlamNode *> seg = segments[k];
                        for(int l=0; l < seg.size(); l++){
                            SlamNode *nd = seg[l];                            
                            //current_region->removeNode(nd);
                            if(nd == last_added_node){
                                for(int k=0; k < seg.size(); k++){
                                    current_segment.insert(nd);
                                }
                                break;
                            }
                        }
                    }
                }
                            
                if(segments.size() > 1 && (n_cut < param.segmentSplitThreshold)){// && u_rand_split < prob_to_segment)){
                    
                    sprintf(buf, "[Segment Region] [Fixed Spectral] Segmenting region");
                    output_writer->write_to_buffer(buf);  
                    
                    bool size_check_valid = true;
                
                    for(int k=0; k < segments.size(); k++){
                        if(segments[k].size() < param.minRegionSizeToSegment){
                            size_check_valid = false;
                            sprintf(buf, "[Segment Region] [Fixed Spectral] Min segment size check failed (fixed spectral)");
                            output_writer->write_to_buffer(buf);  
                            break;                            
                        }
                    }

                    if(size_check_valid){
                        //split these nodes to new regions 
                        //remove the new region nodes from the current nodes 
                
                        //make sure that the new regions are created and the current region is updated properly
                        //always put the newly created node into the new region 
                        if(segments.size() > 2){
                            sprintf(buf, "[Segment Region] [Fixed Spectral] [Error] Received more segments than expected : %d",
                                    (int) segments.size());
                            output_writer->write_to_buffer(buf);  
                        }
                
                        int new_current_region_id = 0;
                        for(int k=0; k < segments.size(); k++){
                            vector<SlamNode *> seg = segments[k];
                            for(int l=0; l < seg.size(); l++){
                                SlamNode *nd = seg[l];                            
                                //current_region->removeNode(nd);
                                if(nd == last_added_node){
                                    new_current_region_id = k;
                                    break;
                                }
                            }
                        }
                
                        vector<SlamNode *> to_remove_seg = segments[new_current_region_id];
                        for(int l=0; l < to_remove_seg.size(); l++){
                            SlamNode *nd = to_remove_seg[l];                            
                            current_region->removeNode(nd);
                        }

                        //move the nodes to a new region 
                        RegionNode *split_region = graph->createNewRegion();
                        for(int l=0; l < to_remove_seg.size(); l++){
                            SlamNode *nd = to_remove_seg[l];
                            graph->addNodeToRegion(nd, split_region);
                        }
                        current_region->updateMean();
                        updateRegionRayTrace(current_region);
                        new_region = split_region;

                        if(param.clearDeadEdges)
                            clearDummyConstraints();
                    }
                }
            }            
        }
        else{
            if(distFromLastRegionNode > param.fixedRegionDistanceThreshold){
                distFromLastRegionNode = 0;
                //get the last node added to this region - and remove it from the list 
                sprintf(buf, "[Segment Region] Segmenting region (fixed distance)");
                output_writer->write_to_buffer(buf);  

                current_region->removeNode(last_added_node);
                RegionNode *split_region = graph->createNewRegion();
                graph->addNodeToRegion(last_added_node, split_region);
                new_region = split_region;
                if(param.clearDeadEdges)
                    clearDummyConstraints();
                updateRegionRayTrace(current_region);
            }
        }
    }
    return new_region;
}

vector< vector<SlamNode *> > SlamParticle::segmentRegion(RegionNode *region, double *n_cut){
    //calculate the overlaps 
    vector<vector<SlamNode *> > result_list; 
    similarity_t sim;

    sim.size = region->nodes.size();
    
    sim.node_id = (int *) calloc(sim.size, sizeof(int));

    sim.mat = (double **) calloc(sim.size, sizeof(double*));
    
    for(int i=0; i < sim.size; i++){
        sim.mat[i] = (double *) calloc(sim.size, sizeof(double));
    }
    int64_t ts = bot_timestamp_now();
    for(int i=0; i < region->nodes.size(); i++){
        SlamNode *nd_i = region->nodes[i];
        sim.node_id[i] = nd_i->id;
        for(int j=0; j < region->nodes.size(); j++){
            SlamNode *nd_j = region->nodes[j];
            if(nd_i == nd_j){
                sim.mat[i][j] = 1.0;
            }
            else{
                //check if we have a cached transform
                BotTrans tf_nd_i_to_nd_j = getTransformFromSlamNodes(nd_i, nd_j);
                PointCorrespondence *corr = getCachedCorrespondence(nd_i->id, nd_j->id);
                int different = false;
                if(corr != NULL){
                    Pose2d cache_p = getPoseFromBotTrans(corr->transform);
                    Pose2d current_p = getPoseFromBotTrans(tf_nd_i_to_nd_j);
                    if(fabs(cache_p.x() - current_p.x()) > 0.01){
                        different = true;
                    }
                    else if(fabs(cache_p.y() - current_p.y()) > 0.01){
                        different = true;
                    }
                    else if(fabs(cache_p.t() - current_p.t()) > bot_to_radians(1.0)){
                        different = true;
                    }                        
                }

                if(corr == NULL || different){
                    int matched = getCorrespondance(tf_nd_i_to_nd_j, nd_j, nd_i);
                    //fprintf(stderr, "\t[%d] - [%d] Matched : %d => %d & %d\n", nd_i->id, nd_j->id, 
                    //      matched, nd_i->slam_pose->scan->numPoints, 
                    //      nd_j->slam_pose->scan->numPoints);

                    sim.mat[i][j] = matched / (double) nd_i->slam_pose->scan->numPoints;
                    PointCorrespondence *new_corr = new PointCorrespondence(tf_nd_i_to_nd_j, nd_i->id, nd_j->id, matched);
                    //what happens if we already have one? - we should delete the old one??
                    insertCachedCorrespondence(nd_i->id, nd_j->id, new_corr);
                }
                else{
                    //use the cached result
                    //fprintf(stderr, RED "\tUsing cache : %d-%d\n" RESET_COLOR, nd_i->id, nd_j->id);
                    sim.mat[i][j] = corr->matches / (double) nd_i->slam_pose->scan->numPoints;
                }
            }
        }
    }
    int64_t te1 = bot_timestamp_now();
    for(int i=0; i< sim.size ;i++){
        for(int j=0; j< sim.size ; j++){
            if(i<j)
                continue;
            //sim_matrix
            double comb_score = (sim.mat[i][j] + sim.mat[j][i])/2;
            sim.mat[i][j] = comb_score;
            sim.mat[j][i] = comb_score;
        }
    }

    double n_cut_threshold = 0.5;
    int recursive = 0;
    int no_segments = 0;
    MatrixXd result = doSpectralClustering(sim, recursive, n_cut_threshold, &no_segments, n_cut);
    int64_t te2 = bot_timestamp_now();
    
    if(param.verbose)
        fprintf(stderr, "No of segments : %d\n", no_segments);

    for(int i=0; i < no_segments; i++){
        vector<SlamNode *> segment;
        for(int j=0; j < sim.size; j++){
            if(result(j) == i){
                segment.push_back(graph->getSlamNodeFromID(sim.node_id[j]));
            }
        }        
        result_list.push_back(segment);
    }

    if(param.verbose){
        for(int i=0; i < no_segments; i++){
            vector<SlamNode *> segment = result_list[i];
            fprintf(stderr, "Segment %d (Nodes):\n", i);
            for(int j=0; j < segment.size(); j++){
                fprintf(stderr, "\t%d", segment[j]->id);
            }
            fprintf(stderr, "\n");
        }
        for(int i=0; i < sim.size; i++){
            fprintf(stderr, "[%d] - %d\n", sim.node_id[i], (int) result(i));
        }
    }

    
    //need to put these in to an organized data structure and return it
    free(sim.node_id);
    for(int i=0; i < sim.size; i++){
        free(sim.mat[i]);
    }
    free(sim.mat);
    //cout << result << endl;
    return result_list;
}

//we can ignore the region segments if we need to - or use them ?? 
vector<vector<SlamNode *> > SlamParticle::checkToMergeRegions(RegionNode *region1, RegionNode *region2, double *n_cut){
    //calculate the overlaps 
    vector<vector<SlamNode *> > result_list; 
    int64_t ts = bot_timestamp_now();

    //just put all the nodes to a single vector 
    vector<SlamNode *> nd_list;
    for(int i=0; i < region1->nodes.size(); i++){
        nd_list.push_back(region1->nodes[i]);
    }
    for(int i=0; i < region2->nodes.size(); i++){
        nd_list.push_back(region2->nodes[i]);
    }

    similarity_t sim;
    
    sim.size = nd_list.size();
    
    sim.node_id = (int *) calloc(sim.size, sizeof(int));

    sim.mat = (double **) calloc(sim.size, sizeof(double*));
    
    for(int i=0; i < sim.size; i++){
        sim.mat[i] = (double *) calloc(sim.size, sizeof(double));
    }
    
    for(int i=0; i < nd_list.size(); i++){
        SlamNode *nd_i = nd_list[i];
        sim.node_id[i] = nd_i->id;
        for(int j=0; j < nd_list.size(); j++){
            SlamNode *nd_j = nd_list[j];
            if(nd_i == nd_j){
                sim.mat[i][j] = 1.0;
            }
            else{
                //check if we have a cached transform
                BotTrans tf_nd_i_to_nd_j = getTransformFromSlamNodes(nd_i, nd_j);
                PointCorrespondence *corr = getCachedCorrespondence(nd_i->id, nd_j->id);
                int different = false;
                if(corr != NULL){
                    Pose2d cache_p = getPoseFromBotTrans(corr->transform);
                    Pose2d current_p = getPoseFromBotTrans(tf_nd_i_to_nd_j);
                    if(fabs(cache_p.x() - current_p.x()) > 0.01){
                        different = true;
                    }
                    else if(fabs(cache_p.y() - current_p.y()) > 0.01){
                        different = true;
                    }
                    else if(fabs(cache_p.t() - current_p.t()) > bot_to_radians(1.0)){
                        different = true;
                    }                        
                }

                if(corr == NULL || different){
                    if(param.verbose && different){
                        fprintf(stderr, RED "Recalculating correspondance : %d - %d\n" RESET_COLOR, nd_i->id, nd_j->id);
                    }
                    //maybe cache this in a map and then clear it every time a segmentation happens?? 
                    
                    //we could cache the transforms and reuse them - from one time step to the other (and maybe across particles
                    //they should not change - especially if do not add LC's within regions 
                    int matched = getCorrespondance(tf_nd_i_to_nd_j, nd_j, nd_i);
                    //fprintf(stderr, "\t[%d] - [%d] Matched : %d => %d & %d\n", nd_i->id, nd_j->id, 
                    //      matched, nd_i->slam_pose->scan->numPoints, 
                    //      nd_j->slam_pose->scan->numPoints);

                    sim.mat[i][j] = matched / (double) nd_i->slam_pose->scan->numPoints;
                    PointCorrespondence *new_corr = new PointCorrespondence(tf_nd_i_to_nd_j, nd_i->id, nd_j->id, matched);
                    //what happens if we already have one? - we should delete the old one??
                    insertCachedCorrespondence(nd_i->id, nd_j->id, new_corr);
                }
                else{
                    //use the cached result
                    //fprintf(stderr, RED "\tUsing cache : %d-%d\n" RESET_COLOR, nd_i->id, nd_j->id);
                    sim.mat[i][j] = corr->matches / (double) nd_i->slam_pose->scan->numPoints;
                }
            }
        }
    }
    int64_t te1 = bot_timestamp_now();
    for(int i=0; i< sim.size ;i++){
        for(int j=0; j< sim.size ; j++){
            if(i<j)
                continue;
            //sim_matrix
            double comb_score = (sim.mat[i][j] + sim.mat[j][i])/2;
            sim.mat[i][j] = comb_score;
            sim.mat[j][i] = comb_score;
        }
    }

    double n_cut_threshold = 0.5;
    int recursive = 0;
    int no_segments = 0;
    MatrixXd result = doSpectralClustering(sim, recursive, n_cut_threshold, &no_segments, n_cut);
    int64_t te2 = bot_timestamp_now();
    /*fprintf(stderr, BLUE "Time to Find Corr : %f - Time to Cut : %f\n" RESET_COLOR, 
      (te1-ts)/ 1.0e6, (te2-te1)/ 1.0e6);*/
    
    if(param.verbose)
        fprintf(stderr, "No of segments : %d\n", no_segments);
    /*if(no_segments == 1){
      vector<SlamNode *> segment;
      for(int i=0; i < sim.size; i++){
      segment.push_back(graph->getSlamNodeFromID(sim.node_id[i]));
      }
      result_list.push_back(segment);
      }
      else{*/
    for(int i=0; i < no_segments; i++){
        vector<SlamNode *> segment;
        for(int j=0; j < sim.size; j++){
            if(result(j) == i){
                segment.push_back(graph->getSlamNodeFromID(sim.node_id[j]));
            }
        }        
        result_list.push_back(segment);
    }

    if(param.verbose){
        for(int i=0; i < no_segments; i++){
            vector<SlamNode *> segment = result_list[i];
            fprintf(stderr, "Segment %d (Nodes):\n", i);
            for(int j=0; j < segment.size(); j++){
                fprintf(stderr, "\t%d", segment[j]->id);
            }
            fprintf(stderr, "\n");
        }
        for(int i=0; i < sim.size; i++){
            fprintf(stderr, "[%d] - %d\n", sim.node_id[i], (int) result(i));
        }
    }

    
    //need to put these in to an organized data structure and return it
    free(sim.node_id);
    for(int i=0; i < sim.size; i++){
        free(sim.mat[i]);
    }
    free(sim.mat);
    //cout << result << endl;
    return result_list;
}

vector< vector<SlamNode *> > SlamParticle::segmentNodes(vector<SlamNode *> nodes, double *n_cut){
    //calculate the overlaps 
    vector<vector<SlamNode *> > result_list; 
    similarity_t sim;

    sim.size = nodes.size();
    
    sim.node_id = (int *) calloc(sim.size, sizeof(int));

    sim.mat = (double **) calloc(sim.size, sizeof(double*));
    
    for(int i=0; i < sim.size; i++){
        sim.mat[i] = (double *) calloc(sim.size, sizeof(double));
    }

    int64_t ts = bot_timestamp_now();
    for(int i=0; i < nodes.size(); i++){
        SlamNode *nd_i = nodes[i];
        sim.node_id[i] = nd_i->id;
        for(int j=0; j < nodes.size(); j++){
            SlamNode *nd_j = nodes[j];
            if(nd_i == nd_j){
                sim.mat[i][j] = 1.0;
            }
            else{
                //check if we have a cached transform
                BotTrans tf_nd_i_to_nd_j = getTransformFromSlamNodes(nd_i, nd_j);
                PointCorrespondence *corr = getCachedCorrespondence(nd_i->id, nd_j->id);
                int different = false;
                if(corr != NULL){
                    Pose2d cache_p = getPoseFromBotTrans(corr->transform);
                    Pose2d current_p = getPoseFromBotTrans(tf_nd_i_to_nd_j);
                    if(fabs(cache_p.x() - current_p.x()) > 0.01){
                        different = true;
                    }
                    else if(fabs(cache_p.y() - current_p.y()) > 0.01){
                        different = true;
                    }
                    else if(fabs(cache_p.t() - current_p.t()) > bot_to_radians(1.0)){
                        different = true;
                    }                        
                }

                if(corr == NULL || different){
                    vector<pointDist> dist_vector;
                    int matched = getCorrespondance(tf_nd_i_to_nd_j, nd_j, nd_i, &dist_vector);
                    //fprintf(stderr, "\t[%d] - [%d] Matched : %d => %d & %d\n", nd_i->id, nd_j->id, 
                    //      matched, nd_i->slam_pose->scan->numPoints, 
                    //      nd_j->slam_pose->scan->numPoints);

                    sim.mat[i][j] = matched / (double) nd_i->slam_pose->scan->numPoints;
                    PointCorrespondence *new_corr = new PointCorrespondence(tf_nd_i_to_nd_j, nd_i->id, nd_j->id, matched, dist_vector);
                    //what happens if we already have one? - we should delete the old one??
                    insertCachedCorrespondence(nd_i->id, nd_j->id, new_corr);
                }
                else{
                    //use the cached result
                    //fprintf(stderr, RED "\tUsing cache : %d-%d\n" RESET_COLOR, nd_i->id, nd_j->id);
                    sim.mat[i][j] = corr->matches / (double) nd_i->slam_pose->scan->numPoints;
                }
            }
        }
    }
    int64_t te1 = bot_timestamp_now();
    for(int i=0; i< sim.size ;i++){
        for(int j=0; j< sim.size ; j++){
            if(i<j)
                continue;
            //sim_matrix
            double comb_score = (sim.mat[i][j] + sim.mat[j][i])/2;
            sim.mat[i][j] = comb_score;
            sim.mat[j][i] = comb_score;
        }
    }

    double n_cut_threshold = 0.5;
    int recursive = 0;
    int no_segments = 0;
    MatrixXd result = doSpectralClustering(sim, recursive, n_cut_threshold, &no_segments, n_cut);
    int64_t te2 = bot_timestamp_now();
    
    if(param.verbose)
        fprintf(stderr, "No of segments : %d\n", no_segments);

    for(int i=0; i < no_segments; i++){
        vector<SlamNode *> segment;
        for(int j=0; j < sim.size; j++){
            if(result(j) == i){
                segment.push_back(graph->getSlamNodeFromID(sim.node_id[j]));
            }
        }        
        result_list.push_back(segment);
    }

    if(param.verbose){
        for(int i=0; i < no_segments; i++){
            vector<SlamNode *> segment = result_list[i];
            fprintf(stderr, "Segment %d (Nodes):\n", i);
            for(int j=0; j < segment.size(); j++){
                fprintf(stderr, "\t%d", segment[j]->id);
            }
            fprintf(stderr, "\n");
        }
        for(int i=0; i < sim.size; i++){
            fprintf(stderr, "[%d] - %d\n", sim.node_id[i], (int) result(i));
        }
    }

    //need to put these in to an organized data structure and return it
    free(sim.node_id);
    for(int i=0; i < sim.size; i++){
        free(sim.mat[i]);
    }
    free(sim.mat);

    int check_overlap = 0;
    if(check_overlap && no_segments > 1){
        //we should check if there is significant overlap of the two regions - would the segmentation not take this into account??
        int small_segment_id = 0;
        int small_segment_size = 1000;
        for(int i=0; i < no_segments; i++){
            vector<SlamNode *> segment = result_list[i];
            if(segment.size() < small_segment_size){
                small_segment_id = i;
                small_segment_size = segment.size();
            }
        }

        vector<SlamNode *> small_segment; 
        vector<SlamNode *> large_segment;

        if(small_segment_id == 0){
            small_segment = result_list[0];
            large_segment = result_list[1];
        }
        else{        
            small_segment = result_list[1];
            large_segment = result_list[0];
        }

        int total_points = 0;
        int total_matches = 0;
        for(int i=0; i < small_segment.size(); i++){
            SlamNode *nd_i = small_segment[i];
            map<int, double> point_matches;
            for(int j=0; j < large_segment.size(); j++){
                SlamNode *nd_j = large_segment[j];
                PointCorrespondence *corr = getCachedCorrespondence(nd_i->id, nd_j->id);
                for(int k = 0; k < corr->match_list.size(); k++){
                    //int id = corr->match_list[k].first;
                    //double dist = corr->match_list[k].second;
                    point_matches.insert(corr->match_list[k]);
                }
            }
            total_points += nd_i->slam_pose->scan->numPoints;
            total_matches += point_matches.size();
            //fprintf(stderr, CYAN "Matches For Node %d => %d / %d\n" RESET_COLOR, nd_i->id, (int) point_matches.size(), nd_i->slam_pose->scan->numPoints);
        }

        //fprintf(stderr, CYAN "Total matches : %d / %d\n" RESET_COLOR, total_matches, total_points);
    }
        
   
    //cout << result << endl;
    return result_list;
}

vector<regionDist> SlamParticle::getOverlapWithRegions(RegionNode *current_region, vector<SlamNode *> nodes, double max_dist=10){
    vector<regionDist> result;
    std::vector<nodePairDist> region_dist_list;    
    double m_dist = graph->findDistanceToRegions(current_region, &region_dist_list);
    std::sort(region_dist_list.begin(), region_dist_list.end(), compareNodePairDistance);
    for(int i=0; i< region_dist_list.size(); i++){
        if(region_dist_list[i].second > max_dist)
            continue;
        
        RegionNode *close_region = region_dist_list[i].first.second->region_node;

        double overlap = getPointOverlap(nodes, close_region->nodes);
        fprintf(stderr, RED "Overlap With region %d -> %f\n" RESET_COLOR , close_region->region_id, overlap);
        result.push_back(make_pair(close_region,overlap));
    }
    return result;
}

double SlamParticle::getPointOverlap(RegionNode *r_i, RegionNode *r_j){
    return getPointOverlap(r_i->nodes, r_j->nodes);
}

double SlamParticle::getPointOverlap(vector<SlamNode *> list_i, vector<SlamNode *> list_j){
    vector<SlamNode *> small_segment = list_i;
    vector<SlamNode *> large_segment = list_j;
    if(list_i.size() > list_j.size()){
        small_segment = list_j;
        large_segment = list_i; 
    }
    
    int total_points = 0;
    int total_matches = 0;
    for(int i=0; i < small_segment.size(); i++){
        SlamNode *nd_i = small_segment[i];
        map<int, double> point_matches;
        for(int j=0; j < large_segment.size(); j++){
            SlamNode *nd_j = large_segment[j];
            PointCorrespondence *corr = getUpdatedCorrespondence(nd_i, nd_j);//getCachedCorrespondence(nd_i->id, nd_j->id);
            for(int k = 0; k < corr->match_list.size(); k++){
                point_matches.insert(corr->match_list[k]);
            }
        }
        total_points += nd_i->slam_pose->scan->numPoints;
        total_matches += point_matches.size();
        //fprintf(stderr, CYAN "Matches For Node %d => %d / %d\n" RESET_COLOR, nd_i->id, (int) point_matches.size(), nd_i->slam_pose->scan->numPoints);
    }
    //fprintf(stderr, CYAN "Total matches : %d / %d\n" RESET_COLOR, total_matches, total_points);
    return total_matches / (double) total_points;
}

// ------------------------------------- Scanmatch Utils ----------------------------------- //

/*
  Scanmatch current node to nodes within the current region - only done based on option 
 */

int SlamParticle::doScanMatchWithinRegion(RegionNode *region, SlamNode *node){
    vector<SlamNode *> node_list; 
    int close_node_dist = 3; 
    SlamNode *close_node = NULL; 
    double min_dist = 1000;

    //we should check if we already have a close node that already has a scanmatch

    /*for(int i= region->nodes.size() - 1; i >=0; i--){
        if()
        }*/

    Pose2d pose = node->getPose();
    ScanTransform res;
    res.x = pose.x();
    res.y = pose.y();
    res.theta = pose.t();

    vector<nodeDist> node_dist_list; 

    for(int i=0; i < region->nodes.size(); i++){
        SlamNode *nd  = region->nodes[i];
        if(fabs(nd->id - node->id) < close_node_dist){
            continue;
        }
        double dist = getDistanceBetweenNodes(nd, node);
        if(dist > 5.0)
            continue;
        
        if(dist < min_dist){
            min_dist = dist;
            close_node = nd;
        }
        node_dist_list.push_back(make_pair(nd, dist));
    }

    sort(node_dist_list.begin(), node_dist_list.end(), compareDistance);

    for(int i=0; i < node_dist_list.size(); i++){
        if(i >= MAX_NO_SCANS_TO_SM){
            break;
        }
        node_list.push_back(node_dist_list[i].first);
    }    

    if(node_list.size() < 2)
        return 0; 

    int added_scans = addNodesToScanMatcher(sm, node_list);
    
    if(!added_scans){
        //fprintf(stderr, "We didn't get scans added\n");
        sm->clearScans(false);    
        return 0; 
    }

    NodeScan *pose_to_match = node->slam_pose;
    
    pose_to_match->scan->applyTransform(res);

    ScanTransform lc_r = sm->gridMatch(pose_to_match->scan->points, pose_to_match->scan->numPoints,
                                       &pose_to_match->scan->T, 1.0, 1.0, M_PI / 12.0);

    double hitpct = lc_r.hits / (double) pose_to_match->scan->numPoints;

    //fprintf(stderr, "Scan match Hit Rate - Small search : %f\n", hitpct);

    if(hitpct > 0.8){
        //we should do a double check to make sure that the constraint isn't too far off - since this is within region also 

        Pose2d target_pose = close_node->getPose();
        
        Pose2d sm_pose(lc_r.x, lc_r.y, lc_r.theta);
    
        BotTrans sm_to_particle_frame = getBotTransFromPose(sm_pose);

        BotTrans target_to_particle_frame = getBotTransFromPose(target_pose);

        BotTrans particle_frame_to_sm = sm_to_particle_frame;

        bot_trans_invert(&particle_frame_to_sm);

        //transform that scanmatch gives us 
        BotTrans target_to_sm;
        bot_trans_apply_trans_to(&particle_frame_to_sm, &target_to_particle_frame, &target_to_sm);

        double rpy_transform[3];
        bot_quat_to_roll_pitch_yaw(target_to_sm.rot_quat, rpy_transform);

        Pose2d transf = Pose2d(target_to_sm.trans_vec[0], target_to_sm.trans_vec[1], rpy_transform[2]);

        double Rcov[9];
        sm_rotateCov2D(lc_r.sigma, lc_r.theta, Rcov);
        Matrix3d cv_sm(Rcov);

        Noise cov_hc;// = NULL; 

        if(param.useSMCov){
            cov_hc = Covariance(cv_sm);
        }
        else{
            double cov_hardcode[9] = { .002, 0, 0, 0, .002, 0, 0, 0, .001 };
            Matrix3d cv_hc(cov_hardcode);
            cov_hc = Covariance(cv_hc);
        }        

        graph->addConstraint(close_node, node, close_node, node,
                             transf, cov_hc, 
                             //maybe give it a new name??
                             hitpct, SLAM_GRAPH_EDGE_T_TYPE_SM_LC, 
                             SLAM_GRAPH_EDGE_T_STATUS_SUCCESS);

        sm->clearScans(false);   
        return 1;
    }
        
    sm->clearScans(false);    
    return 0;
}

/*
  Does extensive scanmatch - only used in the RSS/IJRR implementation - where we sample language edges seperately
 */
//do the extensive scanmatch - only to be used for language edges - where a large bad prior can exist
int SlamParticle::scanmatchRegionsExtensive(SlamNode *c_nd, SlamNode *p_nd, bool add_dummy_regions, int *large_change, bool skip_odometry){
    RegionNode *current_region = c_nd->region_node; 
    RegionNode *closest_region = p_nd->region_node; 
                    
    if(1){ //param.printLanguageScanmatch){
        fprintf(stderr, "[%d] ++++ Proposed Region : %d -> Current Region : %d\n", graph_id, closest_region->region_id, current_region->region_id);
        fprintf(stderr, "Doing Extensive scanmatch for regions\n");
    }

    *large_change = 0;
    int skip_region = 0;

    if(skip_odometry){
        //this is mainly for outdoor data that can cause wrong scanmatches 
        //check if the regions are made up of scanmatch constraints - or odometry 
        int odom_r1, sm_r1, odom_r2, sm_r2;
        double odom_ratio_r1 = current_region->getOdometryRatio(&odom_r1, &sm_r1);
        double odom_ratio_r2 = closest_region->getOdometryRatio(&odom_r2, &sm_r2);
        
        if(odom_ratio_r1 > 0.2 || odom_ratio_r2 > 0.2){
            skip_region = 1;
            return 0;
        }
    }
        
    map<int, PoseToPoseTransform *>::iterator it;
    it = c_nd->slam_pose->sm_lang_constraints.find(p_nd->id);
    if(param.printLanguageScanmatch)
        fprintf(stderr, "Checking cache for exisiting constraints\n");
    if(it != c_nd->slam_pose->sm_lang_constraints.end()){
        if(param.printLanguageScanmatch)
            fprintf(stderr, "Found cached constraint for %d - %d\n", (int) c_nd->id, (int) p_nd->id);
        //use the value 
        PoseToPoseTransform *matched_tf = it->second; 
        
        
        if(matched_tf->accepted == 1){
            if(param.printLanguageScanmatch)
                fprintf(stderr, "\tConstraint matched - adding\n");

            Pose2d o_transf = c_nd->getPose().ominus(p_nd->getPose());
            Pose2d sm_transf = *matched_tf->transform;
            //if(fabs(sm_transf.x() - o_transf.x()) > 5.0 || fabs(sm_transf.y() - o_transf.y()) > 5.0 || fabs(sm_transf.t() - o_transf.t()) > bot_to_radians(45)){
            if(fabs(sm_transf.x() - o_transf.x()) > 10.0 || fabs(sm_transf.y() - o_transf.y()) > 10.0){
                *large_change = 1;
            }
            SlamConstraint *sl_ct = graph->addConstraint(c_nd, p_nd, c_nd, p_nd, *matched_tf->transform, *matched_tf->noise, matched_tf->hitpct, SLAM_GRAPH_EDGE_T_TYPE_LANGUAGE_FULL, SLAM_GRAPH_EDGE_T_STATUS_SUCCESS);
            //continue;
            return 1;
        }
        else{
            if(param.printLanguageScanmatch){
                if(param.verbose)
                    fprintf(stderr, "\tConstraint had failed - skipping\n");
            }
            if(add_dummy_regions)
                graph->addDummyConstraint(current_region->mean_node, closest_region->mean_node, current_region->mean_node, closest_region->mean_node, 
                                          *matched_tf->transform, *matched_tf->noise, matched_tf->hitpct, 
                                          SLAM_GRAPH_EDGE_T_TYPE_LANGUAGE_FULL, SLAM_GRAPH_EDGE_T_STATUS_FAILED);
            return 0;
        }
    }
    if(param.printLanguageScanmatch){
        fprintf(stderr, "Constraint not found\n");
    }

    pointlist2d_t *c_plist = getPointsForRegion(c_nd, 4.0, true);
    pointlist2d_t *t_plist = getPointsForRegion(p_nd, 4.0, true);
                    
    BotTrans o_match_to_pf = getBotTransFromPose(c_nd->getPose());
    BotTrans target_to_pf = getBotTransFromPose(p_nd->getPose());
    BotTrans pf_to_target = target_to_pf;
    bot_trans_invert(&pf_to_target);
                    
    BotTrans o_match_to_target; 
    bot_trans_apply_trans_to(&pf_to_target, &o_match_to_pf, &o_match_to_target);
                    
    double score = 0; 
    int matches = 0;
    BotTrans sm_to_target = o_match_to_target;
                    
    //call the scanmatcher - also get the uncertainty of the scanmatch result 
    double f_score = 0; 
    int matched_points = 0; 
    double max_corr_dist = .2; 
    
    pointlist2d_t *result = NULL;

    int converged = 0; 
    double hit_rate = 0;
    int align_scans = 0;

    int pause = 0;
    int draw_failed = 0;

    double variance[9] = {0};

    result = scanmatchRegionsExtensiveMRPT(t_plist, //target, 
                                           c_plist,//match, 
                                           target_to_pf, 
                                           o_match_to_pf, 
                                           &sm_to_target, 
                                           &score,
                                           //&f_score,
                                           variance,
                                           align_scans, 
                                           param.langSMAcceptanceThreshold,
                                           0.7,
                                           pause, 
                                           draw_failed, 
                                           lcmgl_sm_graph, 
                                           lcmgl_sm_prior, 
                                           lcmgl_sm_result);
                       
    pointlist2d_free(c_plist);
    pointlist2d_free(result);
    pointlist2d_free(t_plist);

    double cov_high[9] = { .02, 0, 0, 0, .02, 0, 0, 0, .01 };
    Matrix3d cv_high(cov_high);
    Covariance cov_hc(cv_high);

    NodeScan *pose_curr = c_nd->slam_pose;  //actually in the function this is called the match 
    NodeScan *pose_to_match = p_nd->slam_pose; //and this is called the target 

    Pose2d value_1 = c_nd->getPose();
    Pose2d value_2 = p_nd->getPose();
    
    Pose2d delta = value_1.ominus(value_2);
    
    double ref_dist = hypot(delta.x(), delta.y());
    double ref_angle = delta.t();

    int accepted = 0;
    if(score > param.langSMAcceptanceThreshold){
        accepted = 1;
    }

    Pose2d target_sm = getPoseFromBotTrans(sm_to_target);

    //we dont need to be selective about the transform for this method (as the prior doesnt influence the result) 
    PoseToPoseTransform *constraint_tf= new PoseToPoseTransform(&target_sm,
                                                                pose_curr, 
                                                                pose_to_match, 
                                                                SLAM_GRAPH_EDGE_T_TYPE_LANGUAGE_FULL,
                                                                &cov_hc, 
                                                                cv_high, 
                                                                score, 
                                                                ref_dist, 
                                                                ref_angle, accepted);

    Pose2d o_transf = c_nd->getPose().ominus(p_nd->getPose());
    Pose2d sm_transf = target_sm;
    //if(fabs(sm_transf.x() - o_transf.x()) > 5.0 || fabs(sm_transf.y() - o_transf.y()) > 5.0 || fabs(sm_transf.t() - o_transf.t()) > bot_to_radians(45)){
    if(fabs(sm_transf.x() - o_transf.x()) > 10.0 || fabs(sm_transf.y() - o_transf.y()) > 10.0){
        *large_change = 1;
    }
    
    BotTrans sm_to_target_inv = sm_to_target;
    bot_trans_invert(&sm_to_target_inv);

    Pose2d target_sm_inv  = getPoseFromBotTrans(sm_to_target_inv);

    //we dont need to be selective about the transform for this method (as the prior doesnt influence the result) 
    PoseToPoseTransform *constraint_tf_inv = new PoseToPoseTransform(&target_sm_inv,
                                                                     pose_to_match, 
                                                                     pose_curr, 
                                                                     SLAM_GRAPH_EDGE_T_TYPE_LANGUAGE_FULL,
                                                                     &cov_hc, 
                                                                     cv_high, 
                                                                     score, 
                                                                     ref_dist, 
                                                                     ref_angle, accepted);
           
               
    c_nd->slam_pose->sm_lang_constraints.insert(make_pair(p_nd->id, constraint_tf));
    //adding the inverse 
    p_nd->slam_pose->sm_lang_constraints.insert(make_pair(c_nd->id, constraint_tf_inv));

    if(param.printLanguageScanmatch){
        fprintf(stderr, "Done insert\n");
        fprintf(stderr, "Region [%d] - [%d] => Score : %f\n", current_region->region_id, closest_region->region_id, score);
    }

    // Right now edges are accepted deterministically 
    // Might make it probabilistic at some point 
                    
    if(score > param.langSMAcceptanceThreshold){//sm_acceptance_threshold){
        if(param.printLanguageScanmatch)
            fprintf(stderr, YELLOW "Accepting scanmatch = Adding constraint\n" RESET_COLOR);
                        
        //a dummy cov - this should be filled by SM 
        double cov_dummy[9] = { .02, 0, 0, 0, .02, 0, 0, 0, .01 };

        Matrix3d cv_temp;
        if(0 && param.useScanmatcherUncertainity){
            cv_temp = Matrix3d(variance);//cov_dummy);
        }
        else{
            cv_temp = Matrix3d(cov_dummy);
        }
        
        Covariance cov_temp(cv_temp);
                        
        Pose2d transf = getPoseFromBotTrans(sm_to_target);
                        
        Pose2d o_transf = getPoseFromBotTrans(o_match_to_target);
                        
        if(param.printLanguageScanmatch){
            fprintf(stderr, "Original Transform : %f,%f - %f\n", o_transf.x(), o_transf.y(), o_transf.t());
            fprintf(stderr, "Transform : %f,%f - %f\n", transf.x(), transf.y(), transf.t());
        }
            
        graph->addConstraint(c_nd, p_nd, c_nd, p_nd,
                             transf, cov_temp, 
                             score, SLAM_GRAPH_EDGE_T_TYPE_SM_LC, 
                             SLAM_GRAPH_EDGE_T_STATUS_SUCCESS);
                                
        //update the graph here - because we want to recalculate the scores - for merging the segments 
        return 1;
    }
    else{

        if(add_dummy_regions){
            //add a dead edge here 
            //we should add new catagories of edges - ones proposed based on distance 
            //ones proposed based on the topology 
            //ones based on semantics 
            //ones based on language 
            double cov_dummy[9] = { .02, 0, 0, 0, .02, 0, 0, 0, .01 };
            Matrix3d cv_temp;
            cv_temp = Matrix3d(cov_dummy);
            Covariance cov_temp(cv_temp);

            Pose2d transf = getPoseFromBotTrans(sm_to_target);
            //add a dead edge 
            if(param.printLanguageScanmatch)
                fprintf(stderr, RED "Scanmatch score : %f\n" RESET_COLOR, score); 
        
            graph->addDummyConstraint(current_region->mean_node, closest_region->mean_node, current_region->mean_node, closest_region->mean_node, 
                                      transf, cov_temp, 
                                      score, SLAM_GRAPH_EDGE_T_TYPE_SM_LC,
                                      SLAM_GRAPH_EDGE_T_STATUS_FAILED);
        }
    }
    return 0;
} 

//large_diff - if there is a large difference between the scanmatch constraint and the current constraint 
int SlamParticle::scanmatchRegions(SlamNode *c_nd, SlamNode *p_nd, bool &large_diff, bool add_dummy_regions, bool skip_odometry, bool use_cache, bool ignore_large_diff){
    RegionNode *current_region = c_nd->region_node; 
    RegionNode *closest_region = p_nd->region_node; 
    large_diff = false;
    int64_t utime_start = bot_timestamp_now();
    
    static char buf[1024];

    sprintf(buf, "[Scanmatch Region] Proposed Region : %d -> Current Region : %d", closest_region->region_id, current_region->region_id);
    output_writer->write_to_buffer(buf);

    if(param.printDistanceScanmatch){
        fprintf(stderr, "++++ Proposed Region : %d -> Current Region : %d\n", closest_region->region_id, current_region->region_id);
        fprintf(stderr, "Doing scanmatch for regions\n");
    }

    int skip_region = 0;

    if(skip_odometry){
        //this is mainly for outdoor data that can cause wrong scanmatches 
        //check if the regions are made up of scanmatch constraints - or odometry 
        int odom_r1, sm_r1, odom_r2, sm_r2;
        double odom_ratio_r1 = current_region->getOdometryRatio(&odom_r1, &sm_r1);
        double odom_ratio_r2 = closest_region->getOdometryRatio(&odom_r2, &sm_r2);
        
        if(odom_ratio_r1 > 0.1 || odom_ratio_r2 > 0.1){
            skip_region = 1;
            return 0;
        }
    }

    int node_id_1 = c_nd->id;
    int node_id_2 = p_nd->id;

    Pose2d value_1 = c_nd->getPose();
    Pose2d value_2 = p_nd->getPose();

    Pose2d delta = value_1.ominus(value_2);

    double ref_dist = hypot(delta.x(), delta.y());
    double ref_angle = delta.t();

    BotTrans o_match_to_pf = getBotTransFromPose(c_nd->getPose());
    BotTrans target_to_pf = getBotTransFromPose(p_nd->getPose());
    BotTrans pf_to_target = target_to_pf;
    bot_trans_invert(&pf_to_target);
                    
    BotTrans o_match_to_target; 
    bot_trans_apply_trans_to(&pf_to_target, &o_match_to_pf, &o_match_to_target);

    vector<PoseToPoseTransform *> *v = NULL;
    
    if(use_cache){
        int64_t utime_cache_s = bot_timestamp_now();
        
        //use the same constraint        
        //if the constraints are more than 0.8 meters 
        double distance_threshold = 0.8;
        double angle_threshold = M_PI / 16.0;

        map<int, vector<PoseToPoseTransform *> *>::iterator c_it;

        c_it = c_nd->slam_pose->sm_full_constraints.find(node_id_2);
        
        if(c_it != c_nd->slam_pose->sm_full_constraints.end()){
            v = c_it->second;
            //this function will now be different 
            //search through the vector; 
            PoseToPoseTransform *constraint_tf = NULL;

            for(uint v_pos = 0; v_pos < v->size(); v_pos++) {
                PoseToPoseTransform *curr_tf = v->at(v_pos);
                if(param.noScanTransformCheck){
                    constraint_tf = curr_tf;
                    //if this is the case - this vector will be of size 1 - so just pick the first element 
                }
                else if(fabs(curr_tf->distance_difference - ref_dist) < distance_threshold &&
                        fabs(curr_tf->angle_difference - ref_angle) < angle_threshold){
                    constraint_tf = curr_tf;
                }
            }
            
            if(constraint_tf != NULL){
                sprintf(buf, "[Scanmatch Region] Found constraint in cache [%d]-[%d] - %f : Accepted : %d", 
                        (int) node_id_1, (int) node_id_2, constraint_tf->hitpct, constraint_tf->accepted);
                output_writer->write_to_buffer(buf);

                if(param.printDistanceScanmatch)
                    fprintf(stderr, "++++++++ [%d]-[%d] Found the constraint in the cache : %f\n", 
                            (int) node_id_1, (int) node_id_2, constraint_tf->hitpct);
                    
                int64_t utime_cache_e = bot_timestamp_now();

                fprintf(stderr, GREEN "\t\t+++++ Time to search cache : %f\n" RESET_COLOR, (utime_cache_e - utime_cache_s) / 1.0e6);
                //add constraint

                if(constraint_tf->accepted == 1){
                    
                    
                    Pose2d transf = *constraint_tf->transform;
                    
                    Pose2d o_transf = getPoseFromBotTrans(o_match_to_target);
                    
                    double delta_d = hypot(o_transf.x() - transf.x(), o_transf.y() - transf.y());

                    //check the likelihood of the edge 

                    Matrix3d obs_cov = constraint_tf->cov_mat;

                    double prob_sm = getLikelihoodOfConstraint(c_nd, p_nd, transf, obs_cov);
                    
                    sprintf(buf, "[Scanmatch Region] Constraint likelihood : %f", prob_sm);
                    output_writer->write_to_buffer(buf);

                    if(prob_sm > SCANMATCH_CONSTRAINT_ADD_PRIOR){
                        sprintf(buf, "[Scanmatch Region] Adding constraint");
                        output_writer->write_to_buffer(buf);
                        SlamConstraint *sl_ct = graph->addConstraint(c_nd, p_nd, c_nd, p_nd, 
                                                                     *constraint_tf->transform, *constraint_tf->noise, 
                                                                     constraint_tf->hitpct, SLAM_GRAPH_EDGE_T_TYPE_SM_LC, 
                                                                     SLAM_GRAPH_EDGE_T_STATUS_SUCCESS);    
                    
                        if(delta_d > LARGE_SM_TF){
                            large_diff = true;
                        }
                        //
                        int64_t utime_end = bot_timestamp_now();
                        sprintf(buf, "[Scanmatch Region] [Performance] Total Time to add from cache : %f", (utime_end - utime_start) / 1.0e6);
                        output_writer->write_to_buffer(buf);
                        return 1;
                    }
                    else{
                        sprintf(buf, "[Scanmatch Region] Rejecting constraint (low likelihood on prior)");
                        output_writer->write_to_buffer(buf);
                        return 0; 
                    }                    
                }       
                else{
                    //the scanmatch failed - lets not do anything 
                    sprintf(buf, "[Scanmatch Region] Rejecting constraint (too low sm likelihood)");
                    output_writer->write_to_buffer(buf);

                    if(add_dummy_regions){
                        graph->addDummyConstraint(current_region->mean_node, closest_region->mean_node, 
                                                  current_region->mean_node, closest_region->mean_node, 
                                                  *constraint_tf->transform, *constraint_tf->noise, 
                                                  constraint_tf->hitpct,
                                                  SLAM_GRAPH_EDGE_T_TYPE_SM_LC,
                                                  SLAM_GRAPH_EDGE_T_STATUS_FAILED);     
                        int64_t utime_end = bot_timestamp_now();
                        fprintf(stderr, "Total Time to add from cache : %f\n", (utime_end - utime_start) / 1.0e6);
                    } //failed SM's weren't getting accepted 
                    return 0;
                }
            }            
        }              
    }    

    int64_t utime_end = bot_timestamp_now();
    
    
    pointlist2d_t *c_plist = getPointsForRegion(c_nd, 5.0, true);//getPointsForRegion(current_region);
    pointlist2d_t *t_plist = getPointsForRegion(p_nd, 5.0, true);
                    
    /*BotTrans o_match_to_pf = getBotTransFromPose(c_nd->getPose());
    BotTrans target_to_pf = getBotTransFromPose(p_nd->getPose());
    BotTrans pf_to_target = target_to_pf;
    bot_trans_invert(&pf_to_target);
                    
    BotTrans o_match_to_target; 
    bot_trans_apply_trans_to(&pf_to_target, &o_match_to_pf, &o_match_to_target);*/
                    
    double score = 0; 
    int matches = 0;
    BotTrans sm_to_target = o_match_to_target;
                    
    //call the scanmatcher - also get the uncertainty of the scanmatch result 
    double variance[9] = {0};
    int64_t s_utime_sm = bot_timestamp_now();
    scanmatchRegionsMRPT(t_plist, c_plist, sm_to_target, 
                         &score, &matches, variance);
    int64_t e_utime_sm = bot_timestamp_now();

    
    sprintf(buf, "[Scanmatch Region] [Performance] Score : %f Time for actual SM : %f", score, (e_utime_sm - s_utime_sm)/ 1.0e6);    
    output_writer->write_to_buffer(buf);
                    
    pointlist2d_free(c_plist);
    pointlist2d_free(t_plist);
     
    sprintf(buf, "[Scanmatch Region] Region [%d] - [%d] => Score : %f", current_region->region_id, closest_region->region_id, score);
    output_writer->write_to_buffer(buf);

    // Right now edges are accepted deterministically 
    // Might make it probabilistic at some point 
        
    //a dummy cov - this should be filled by SM 
    double cov_dummy[9] = { .02, 0, 0, 0, .02, 0, 0, 0, .01 };

    Matrix3d cv_temp;
    if(param.useScanmatcherUncertainity){
        cv_temp = Matrix3d(variance);
    }
    else{
        cv_temp = Matrix3d(cov_dummy);
    }
        
    Covariance cov_temp(cv_temp);

    Noise *cov_hc = NULL;
    if(use_cache){
        cov_hc = new Covariance(cv_temp);
    }

    Pose2d transf = getPoseFromBotTrans(sm_to_target);
    
    Pose2d o_transf = getPoseFromBotTrans(o_match_to_target);
    
    double delta_d = hypot(o_transf.x() - transf.x(), o_transf.y() - transf.y());

    //remove this hack 
    //this was a very bad hack - as this would depend on the orientation as well as the transform - as this is based on the 
    //coor frame 
    if(ignore_large_diff){//param.ignoreLargeScanmatchChanges){
        //for now lets 

        if( delta_d > 5.0){
            score = 0.1;
        }
    }

    //should we clean this up a bit using our scanmatcher?? 
    if(param.cleanupMRPTResult && score > param.smAcceptanceThreshold){
        int64_t s_utime = bot_timestamp_now();
        vector<SlamNode *> nodes_to_add;                

        Pose2d c_p = p_nd->getPose();
        for(int k=0; k < p_nd->region_node->nodes.size(); k++){
            SlamNode *nd = p_nd->region_node->nodes[k];
            Pose2d delta = c_p.ominus(nd->getPose());
            double t_dist = hypot(delta.x(), delta.y());
            if(t_dist > 5.0){
                continue;
            }
            nodes_to_add.push_back(nd);
        }

        int added_scans = addNodesToScanMatcher(sm, nodes_to_add);
    
        if(!added_scans){
            sm->clearScans(false);    
        }
        else{
            Scan* p_scan = getScansForRegion(c_nd, 5.0, true);
    
            //put this scan at the SM transform and see what happens 
            BotTrans sm_to_pf; 
            bot_trans_apply_trans_to(&target_to_pf, &sm_to_target, &sm_to_pf);

            Pose2d sm_transf = getPoseFromBotTrans(sm_to_pf);

            ScanTransform res;
            res.x = sm_transf.x();
            res.y = sm_transf.y();
            res.theta = sm_transf.t();
    
            p_scan->applyTransform(res);

            ScanTransform lc_r = sm->gridMatch(p_scan->points, p_scan->numPoints,
                                               &p_scan->T, .5, .5, M_PI / 12.0);

            double hitpct = lc_r.hits / (double) p_scan->numPoints;

            sprintf(buf, "[Scanmatch Region] [SM Cleanup] Scan match Hit Rate - Small search : %f -> Original Score : %f", hitpct, score);
            output_writer->write_to_buffer(buf);

            //cout << "MRPT Trans : " << sm_transf << endl; 
            
            Pose2d new_sm_to_pf(lc_r.x, lc_r.y, lc_r.theta);
            BotTrans tf_new_sm_to_pf = getBotTransFromPose(new_sm_to_pf);
            BotTrans tf_new_sm_to_target; 
            bot_trans_apply_trans_to(&pf_to_target, &tf_new_sm_to_pf, &tf_new_sm_to_target);

            //fprintf(stderr, "Scanmatch TF : %f,%f, %f\n", lc_r.x, lc_r.y, lc_r.theta);

            //cout << "Old SM To Target " << transf << endl;

            sprintf(buf, "[Scanmatch Region] [SM Cleanup] Old SM Traf : %f,%f,%f", transf.x(), transf.y(), transf.t());
            output_writer->write_to_buffer(buf);
                        
            if(hitpct > 0.9 * score){
                transf = getPoseFromBotTrans(tf_new_sm_to_target);
                
                sprintf(buf, "[Scanmatch Region] [SM Cleanup] Updated SM Traf : %f,%f,%f", transf.x(), transf.y(), transf.t());
                output_writer->write_to_buffer(buf);
            }
            else{
                sprintf(buf, "[Scanmatch Region] [SM Cleanup] Not enough improvement");
                output_writer->write_to_buffer(buf);
            }

            sm->clearScans(false);    
            delete p_scan;
        }
        int64_t e_utime = bot_timestamp_now();
        fprintf(stderr, "Delta Utime : %f\n", (e_utime - s_utime) / 1.0e6);
    }


    NodeScan *pose_curr = c_nd->slam_pose; 
    NodeScan *pose_to_match = p_nd->slam_pose; 
            
    if(score > param.smAcceptanceThreshold || (score > param.smCloseAcceptanceThreshold && delta_d < 1.0)){
        sprintf(buf, "[Scanmatch Region] Original Transform : %f,%f - %f", o_transf.x(), o_transf.y(), o_transf.t());
        output_writer->write_to_buffer(buf);
        
        sprintf(buf, "[Scanmatch Region] SM Transform : %f,%f - %f", transf.x(), transf.y(), transf.t());
        output_writer->write_to_buffer(buf);
                        
        double prob_sm = getLikelihoodOfConstraint(c_nd, p_nd, transf, cv_temp);

        sprintf(buf, "[Scanmatch Region] [Prob] %d - %d Score : %f Prob: %f", current_region->region_id, closest_region->region_id, score, prob_sm);
        output_writer->write_to_buffer(buf);

        sprintf(buf, "[Scanmatch Region] Likelihood of constraint : %f", prob_sm); 
        output_writer->write_to_buffer(buf);
        
        output_writer->write_buffer();

        if(0){
            fprintf(stderr, "Original Transform : %f,%f - %f\n", o_transf.x(), o_transf.y(), o_transf.t());
            fprintf(stderr, "Transform : %f,%f - %f\n", transf.x(), transf.y(), transf.t());
        }

        int edge_added = 0;

        if(prob_sm > SCANMATCH_CONSTRAINT_ADD_PRIOR){
            sprintf(buf, "[Scanmatch Region] Adding constraint");
            output_writer->write_to_buffer(buf);
            if(delta_d > LARGE_SM_TF){
                large_diff = true;
            }
                        
            graph->addConstraint(c_nd, p_nd, c_nd, p_nd, 
                                 transf, cov_temp, 
                                 score, SLAM_GRAPH_EDGE_T_TYPE_SM_LC, 
                                 SLAM_GRAPH_EDGE_T_STATUS_SUCCESS);

            edge_added = 1;
        }
        else{
            sprintf(buf, "[Scanmatch Region] Rejecting constraint (Low likelihood prior)");
            output_writer->write_to_buffer(buf);
        }
        

        //this should ideally be done in the slam graph 
        //lets add this to the constraint cache this 
        if(use_cache){           
            
            PoseToPoseTransform *constraint_tf = new PoseToPoseTransform(new Pose2d(transf), pose_curr, pose_to_match, 
                                                                         SLAM_GRAPH_EDGE_T_TYPE_SM_LC, cov_hc, cv_temp, 
                                                                         score, ref_dist, ref_angle, 1);
            if(v != NULL){
                //add this to the vector
                v->push_back(constraint_tf);
            }
            else{
                vector<PoseToPoseTransform *> *vec = new vector<PoseToPoseTransform *>();
                vec->push_back(constraint_tf);
                pose_curr->sm_full_constraints.insert ( pair<int, vector<PoseToPoseTransform *> *>(node_id_2, vec));
            }
        }
                             
        return edge_added;
    }
    else{
        sprintf(buf, "[Scanmatch Region] Rejecting constraint (too low sm likelihood)");
        output_writer->write_to_buffer(buf);
        if(use_cache){
            PoseToPoseTransform *constraint_tf = new PoseToPoseTransform(new Pose2d(transf), pose_curr, pose_to_match, 
                                                                         SLAM_GRAPH_EDGE_T_TYPE_SM_LC, 
                                                                         cov_hc, cv_temp, 
                                                                         score, ref_dist, ref_angle, 0);
            if(v != NULL){
                //add this to the vector
                v->push_back(constraint_tf);
            }
            else{
                vector<PoseToPoseTransform *> *vec = new vector<PoseToPoseTransform *>();
                vec->push_back(constraint_tf);
                pose_curr->sm_full_constraints.insert ( pair<int, vector<PoseToPoseTransform *> *>(node_id_2, vec));
            }
        }

        if(add_dummy_regions){
            double cov_dummy[9] = { .02, 0, 0, 0, .02, 0, 0, 0, .01 };
            Matrix3d cv_temp;
            cv_temp = Matrix3d(cov_dummy);
            Covariance cov_temp(cv_temp);

            Pose2d transf = getPoseFromBotTrans(sm_to_target);
            //add a dead edge 
            if(param.printDistanceScanmatch){
                fprintf(stderr, RED "Scanmatch score : %f\n" RESET_COLOR, score); 
            }
        
            graph->addDummyConstraint(current_region->mean_node, closest_region->mean_node, current_region->mean_node, closest_region->mean_node, 
                                      transf, cov_temp, 
                                      score, SLAM_GRAPH_EDGE_T_TYPE_SM_LC,
                                      SLAM_GRAPH_EDGE_T_STATUS_FAILED);
        }
    }
    return 0;
} 

// ---------------------------- Semantic Info (Dirichlet dist) ---------------------------------- //

vector<RegionNode *> SlamParticle::bleedLabels(vector<regionPair> connected_regions){
    //bleed the labels
    //for regions to regions 
    //check if connected 
    //check if bled 
    //bleed if not bled

    vector<RegionNode *> bled_regions; 

    for(int i=0; i < connected_regions.size(); i++){
        RegionNode *region1 = connected_regions[i].first;
        RegionNode *region2 = connected_regions[i].second;

        //bleed both ways 
        LabelDistribution *ld1 = region1->labeldist;
        LabelDistribution *ld2 = region2->labeldist;
        
        if(ld1->bleedLabels(ld2)){
            bled_regions.push_back(region1);
        }
        if(ld2->bleedLabels(ld1)){
            bled_regions.push_back(region2);
        }
        //fprintf(stderr, RED "Bleeding Regions : %d - %d\n" RESET_COLOR, region1->region_id, region2->region_id);
    }    
    return bled_regions;
}
    
vector<RegionNode *> SlamParticle::bleedLabels(){
    graph->updateRegionConnections();
    vector<regionPair> connected_regions = graph->getConnectedRegionPairs();
    //fprintf(stderr, RED "\n\n========================Bleeding Labels=======================\n"RESET_COLOR);
    //fprintf(stderr, RED "No of connected regions : %d\n" RESET_COLOR, connected_regions.size());
    return bleedLabels(connected_regions);
}


vector<RegionNode *> SlamParticle::bleedLabelsWithSkip(RegionNode *skip_region){
    graph->updateRegionConnections();
    vector<regionPair> connected_regions = graph->getConnectedRegionPairs(skip_region);
    //fprintf(stderr, RED "\n\n========================Bleeding Labels=======================\n"RESET_COLOR);
    //fprintf(stderr, RED "No of connected regions : %d\n" RESET_COLOR, connected_regions.size());
    return bleedLabels(connected_regions);
}

vector<RegionNode *> SlamParticle::bleedLabels(RegionNode *region){
    graph->updateRegionConnections();

    vector<RegionNode*> connected_regions = graph->getConnectedRegions(region);
    vector<regionPair> connected_region_pairs;
    for(int i = 0; i < connected_regions.size(); i++){
        connected_region_pairs.push_back(make_pair(region, connected_regions[i]));
    }
    return bleedLabels(connected_region_pairs);
    //bleed the labels
    //for regions to regions 
    //check if connected 
    //check if bled 
    //bleed if not bled 
}

// ---------------------- Exploration (in development) -------------------- //

int SlamParticle::findExplorationFrontiers(RegionNode *region){
    return 0;
    SlamNode *mean_node = region->mean_node;
    Pose2d tocheck_value = mean_node->getPose();

    double xy0[2] = {-10.0, -10.0};
    double xy1[2] = {10.0, 10.0};
    double res = 0.5;
    FloatPixelMap *px_map = new FloatPixelMap(xy0, xy1, res, 0.5);//, true, true);

    //allocate data 
    //px_map->
    // Clear the copy of the local occupancy grid
    //memset (px_map->data, 0, px_map->num_cells*sizeof(float));

    double dist =0;
    double pos[2], next_pos[2];
    double start[2];


    // Node->Slam_Pose->Scan laser scans are decimated and in the body frame
    // We will generate the local occupancy grid in the Query Body frame
    BotTrans queryBodyToSelf = getBotTransFromPose(Pose2d(0,0,0));
    
    BotTrans queryBodyToLocal;
    
    BotTrans localToQueryBody;
    queryBodyToLocal.trans_vec[0] = tocheck_value.x();
    queryBodyToLocal.trans_vec[1] = tocheck_value.y();
    queryBodyToLocal.trans_vec[2] = 0.0;
    double rpyQueryBody[3] = {0.0, 0.0, tocheck_value.t()};
    bot_roll_pitch_yaw_to_quat (rpyQueryBody, queryBodyToLocal.rot_quat);

    localToQueryBody = queryBodyToLocal;
    bot_trans_invert (&localToQueryBody);

    BotTrans matchBodyToLocal;
    //BotTrans matchLaserToLocal;
    BotTrans matchBodyToQueryBody;

    double rpyMatchBody[3] = {0.0, 0.0, 0.0};
    //this breaks down - if we use the rear laser as all the points are jumbled together 
    double laserStartMatchBody[3] = {0,0,0};//{0.4460, -0.0101, 0.1};
    double laserStartQueryBody[3];
    double pMatchBody[3] =  {0.0, 0.0, 0.0};
    double pMatchBodyNext[3] = {0.0, 0.0, 0.0};
    double pQueryBody[3];
    double pQueryBodyNext[3];

    // Find the nodes (and scans) near the local search area
    for(int k = 0; k < region->nodes.size(); k++){
        SlamNode *nd = region->nodes[k];
        Pose2d value = nd->getPose();

        //loop through the points and add
        ScanTransform T;
        T.x = value.x();
        T.y = value.y();
        T.theta = value.t();
        
        Pose2d match = nd->getPose();
        
        // Determine the transform from Match Body ---> Query Body
        
        // Set the BotTrans from Match BODY to LOCAL
        matchBodyToLocal.trans_vec[0] = match.x();
        matchBodyToLocal.trans_vec[1] = match.y();
        matchBodyToLocal.trans_vec[2] = 0.0;
        rpyMatchBody[2] = match.t();
        bot_roll_pitch_yaw_to_quat (rpyMatchBody, matchBodyToLocal.rot_quat);

        // ... and now Match Body ---> Query Body
        bot_trans_apply_trans_to (&localToQueryBody, &matchBodyToLocal, &matchBodyToQueryBody);
        
        Scan *scan = nd->slam_pose->scan;
        
        float clamp[2] = {0,1.0} ;
        float ray_trace_clamp[2] = {-11.0, 6};
        
        // Transform Match Laser (0,0,0) ---> Query Laser frame
        bot_trans_apply_vec ( &matchBodyToQueryBody, laserStartMatchBody, laserStartQueryBody);
        start[0] = laserStartQueryBody[0];
        start[1] = laserStartQueryBody[1];
        
        for (unsigned i = 0; i < scan->numPoints; i++) {
            if(hypot(scan->points[i].x, scan->points[i].y) > MAX_SCAN_FOR_BBOX)
                continue;
            pMatchBody[0] = scan->points[i].x;
            pMatchBody[1] = scan->points[i].y;
            
            bot_trans_apply_vec ( &matchBodyToQueryBody, pMatchBody, pQueryBody);
            pos[0] = pQueryBody[0];
            pos[1] = pQueryBody[1];
            
            px_map->rayTrace(start, pos, -1, 2, ray_trace_clamp);
        }            
    }

    if(1){
        const occ_map_pixel_map_t *map = px_map->get_pixel_map_t(bot_timestamp_now());
        occ_map_pixel_map_t_publish(lcm, "PIXEL_MAP", map);
    }
   

    double c_pos[2] = {0,0};
    FloatPixelMap *utility_map = getFrontierMap(px_map, c_pos);

    if(utility_map && 1){
        const occ_map_pixel_map_t *map = utility_map->get_pixel_map_t(bot_timestamp_now());
        occ_map_pixel_map_t_publish(lcm, "FRONTIER_MAP", map);
    }
    
    delete utility_map;
    delete px_map;
}

// ----------------------------- Question asking -----------------------// 

int SlamParticle::updateAnswer(int q_id, string answer){
    return graph->updateAnswer(q_id, answer);
}

void SlamParticle::clearQuestion(){
    return graph->clearQuestion();
}

slam_language_question_t* SlamParticle::getQuestion(){
    return graph->getOutstandingQuestion(graph_id);
}

// ---------------------------- Language Integration ----------------- //

int SlamParticle::addComplexLanguageUpdates(slam_slu_result_t *slu_msg){
    int valid_groundings = 0;
    map<int, SlamNode *> lang_updated_nodes;
    //should have only one label - but just to be safe 
    for(int i = 0; i < slu_msg->no_labels; i++){
        slam_node_label_list_t l_result = slu_msg->labels[i];
        //I think we only get sent updates for a specific label 
        fprintf(stderr, "Received updates for label : %d\n", l_result.label);
        
        //sort this and pick the top N (the nodes are unique - they are already marginalized for the rest of the groundings 
        
        vector<nodeDist> node_grounding;

        for(int j = 0; j < l_result.num; j++){
            //****** this prob value is wrong for now - need to fix it later 
            SlamNode *node = (SlamNode *) graph->getSlamNodeFromID(l_result.nodes[j].id);
            node_grounding.push_back(make_pair(node, l_result.nodes[j].prob));
        }
        
        //this short gives the smallest first 
        std::sort(node_grounding.begin(), node_grounding.end(), compareDistance);
        std::reverse(node_grounding.begin(), node_grounding.end());

        if(param.useFactorGraphs){
            for(int j=0; j < node_grounding.size(); j++){
                SlamNode *node = node_grounding[j].first; 
                double grnd_prob = node_grounding[j].second; 
                fprintf(stderr, "Region %d [%d] - Prob : %f\n", (int) node->region_node->region_id, (int) node->id, grnd_prob);
                if(j >= LANGUAGE_TOP_N_GROUNDINGS){
                    if(grnd_prob < BREAK_FROM_LANG_IF_TOP_N){
                        fprintf(stderr, "Below add threshold - for non top N groundings\n");
                        break;
                    }
                }
                if(grnd_prob >= MIN_LANG_GROUNDING_PROB){
                    node->addLanguageAnnotation(label_info, grnd_prob, l_result.label); //l_result.nodes[j].prob, l_result.label);
                    graph->runBP(node->region_node);
                    valid_groundings = 1;
                }
                else{
                    fprintf(stderr, "Below Threshold - stopping\n");
                    break;
                }
                
            }
        }
        else{
            for(int j=0; j < node_grounding.size(); j++){
                SlamNode *node = node_grounding[j].first; 
                //int label_id = node->region_node->labeldist->getLabelID(string(annotation->update));
                double grnd_prob = node_grounding[j].second; 
                fprintf(stderr, "Region %d [%d] - Prob : %f\n", (int) node->region_node->region_id, (int) node->id, grnd_prob);
                //its ok for us to add to the region in this mode - since regions are persistant 
                 if(j >= LANGUAGE_TOP_N_GROUNDINGS){
                    if(grnd_prob < BREAK_FROM_LANG_IF_TOP_N){
                        fprintf(stderr, "Below add threshold - for non top N groundings\n");
                        break;
                    }
                }

                if(grnd_prob >= MIN_LANG_GROUNDING_PROB){
                    RegionNode *region = node->region_node;
                    //this language event count can mess up with complex language which might be grounded multiple times 
                    region->labeldist->addObservation(l_result.label, grnd_prob, language_event_count);
                    valid_groundings = 1;
                }
                else{
                    fprintf(stderr, "Below Threshold - stopping\n");
                    break;
                }
            }
        }
    }

    language_event_count++;

    //lets call the language edge creation 
    SlamGraph *basic_graph = graph;
    if(param.useBasicGraph){
        basic_graph = sparse_graph;
    }

    //we should perhaps propose edges from these updated nodes as well 
    
    return valid_groundings;
    
    //call the propose edge function 
}

int SlamParticle::addComplexLanguageUpdates(slam_complex_language_result_t *slu_msg){
    //send over to the graph to update each complex language event  
    bool finish_if_good_grounding = false;
    bool finish_always = false;
    
    if(param.gMode == FINISH_ON_GOOD){
        finish_if_good_grounding = true;
    }
    
    if(param.gMode == GROUND_ONCE){
        finish_always = true;
    }

    fprintf(stderr, "Grounding Mode : %d\n", param.gMode);
    
    vector<RegionNode *> grounded_regions; 
    int status = graph->addComplexLanguageUpdates(slu_msg, grounded_regions, !param.alwaysGround, finish_always);    

    //call add language edges for regions that got language updates 
    if(param.proposeSeperateSemanticEdges){
        fprintf(stderr, "Sampling Language Edges\n");
        sampleSemanticEdgesFromRegion(active_region, grounded_regions);
    }

    if(param.useFactorGraphs){
        graph->runBP();
    }

    if(hasOutstandingLanguage()){
        fprintf(stderr, "\n\n+++++ Has outstanding language +++++ \n\n");
    }

    return status;        
}

bool SlamParticle::hasOutstandingLanguage(){
    if(graph->getOutstandingLanguageCount() > 0)
        return true;
    return false;
}

bool SlamParticle::hasLanguagePaths(){
    if(graph->getLanguagePathCount() > 0)
        return true;
    return false;
}

// --------------------- Utils -------------------------------------- //

void SlamParticle::clearDummyConstraints(){
    graph->clearDummyConstraints();
}

//do comparisons between particles - to find if the particles are the same or not 
bool SlamParticle::compareToParticle(SlamParticle *part){
    for(int i=0; i < graph->slam_node_list.size(); i++){
        SlamNode *node = graph->slam_node_list[i];

        SlamNode *p_node = part->graph->getSlamNodeFromID(node->id);
        
        if(p_node == NULL)
            return false;

        Pose2d p1 = node->getPose();
        Pose2d p2 = p_node->getPose();

        Pose2d delta = p1.ominus(p2);

        if(delta.x() > 0.05 || delta.y() > 0.05 || delta.t() > bot_to_degrees(2)){
            return false;
        }
    }
    return true;
}

//region 
bool SlamParticle::compareToParticle(SlamParticle *part, map<int, int> &region_map){
    region_map.clear();
    map<int, int>::iterator it;

    for(int i=0; i < graph->slam_node_list.size(); i++){
        SlamNode *node = graph->slam_node_list[i];

        SlamNode *p_node = part->graph->getSlamNodeFromID(node->id);
        
        int region_id = node->region_node->region_id;
        int region_id_1 = p_node->region_node->region_id;

        it = region_map.find(region_id);
        
        if(it == region_map.end()){
            region_map.insert(make_pair(region_id, region_id_1));
        }
        else{
            if(it->second != region_id_1){
                fprintf(stderr, "Region segmentation is different - particles not the same\n"); //this should not happen in the 
                return false;
            }
        }

        if(p_node == NULL)
            return false;

        Pose2d p1 = node->getPose();
        Pose2d p2 = p_node->getPose();

        Pose2d delta = p1.ominus(p2);

        if(delta.x() > 0.05 || delta.y() > 0.05 || delta.t() > bot_to_degrees(2)){
            return false;
        }

        //maybe do an edge check also 
        //and the region label/semantic distributions 
        //and outstanding language 
    }
    return true;
}

//indexes here are positions 
pointlist2d_t * SlamParticle::getPointsForRegion(SlamNode *node, double radius, bool in_node_frame=true){
    if(param.useBasicGraph){    
        //get these based on the basic graph - and then transform them 
        double pBody[3] = { 0, 0, 0 };
        double pLocal[3];

        RegionNode *region = node->region_node;
        Pose2d pose = node->getPose();

        int no_points = 0;

        BotTrans c_node_to_pf = getBotTransFromPose(node->getPose());
        BotTrans pf_to_c_node = c_node_to_pf;
        bot_trans_invert(&pf_to_c_node);

        for(int i=0; i < region->nodes.size(); i++){
            SlamNode *nd = region->nodes[i];
            Pose2d delta = pose.ominus(nd->getPose());
            double t_dist = hypot(delta.x(), delta.y());
            if(t_dist > radius){
                continue;
            }
            NodeScan *pose_for_points = nd->slam_pose;
            no_points += pose_for_points->scan->numPoints;         
        }

        pointlist2d_t *region_pl = pointlist2d_new(no_points);

        int c = 0;    

        for(int i=0; i < region->nodes.size(); i++){
            SlamNode *nd = region->nodes[i];
            Pose2d delta = pose.ominus(nd->getPose());
            double t_dist = hypot(delta.x(), delta.y());
            if(t_dist > radius){
                continue;
            }
            NodeScan *pose_for_points = nd->slam_pose;
        
            Pose2d value = nd->getPose();

            BotTrans nd_curr_to_pf = getBotTransFromPose(value);
            BotTrans nd_curr_to_c_node; 
            bot_trans_apply_trans_to(&pf_to_c_node, &nd_curr_to_pf, &nd_curr_to_c_node);

            for (unsigned i = 0; i < pose_for_points->scan->numPoints; i++) {
                pBody[0] = pose_for_points->scan->points[i].x;
                pBody[1] = pose_for_points->scan->points[i].y;
                if(in_node_frame)
                    bot_trans_apply_vec(&nd_curr_to_c_node, pBody, pLocal);
                else
                    bot_trans_apply_vec(&nd_curr_to_pf, pBody, pLocal);
                region_pl->points[c].x = pLocal[0];
                region_pl->points[c].y = pLocal[1];
                c++; 
            }
        }
        return region_pl;
    }
    else{
        double pBody[3] = { 0, 0, 0 };
        double pLocal[3];

        RegionNode *region = node->region_node;
        Pose2d pose = node->getPose();

        int no_points = 0;

        BotTrans c_node_to_pf = getBotTransFromPose(node->getPose());
        BotTrans pf_to_c_node = c_node_to_pf;
        bot_trans_invert(&pf_to_c_node);

        for(int i=0; i < region->nodes.size(); i++){
            SlamNode *nd = region->nodes[i];
            Pose2d delta = pose.ominus(nd->getPose());
            double t_dist = hypot(delta.x(), delta.y());
            if(t_dist > radius){
                continue;
            }
            NodeScan *pose_for_points = nd->slam_pose;
            no_points += pose_for_points->scan->numPoints;         
        }

        pointlist2d_t *region_pl = pointlist2d_new(no_points);

        int c = 0;    

        for(int i=0; i < region->nodes.size(); i++){
            SlamNode *nd = region->nodes[i];
            Pose2d delta = pose.ominus(nd->getPose());
            double t_dist = hypot(delta.x(), delta.y());
            if(t_dist > radius){
                continue;
            }
            NodeScan *pose_for_points = nd->slam_pose;
        
            Pose2d value = nd->getPose();

            BotTrans nd_curr_to_pf = getBotTransFromPose(value);
            BotTrans nd_curr_to_c_node; 
            bot_trans_apply_trans_to(&pf_to_c_node, &nd_curr_to_pf, &nd_curr_to_c_node);

            for (unsigned i = 0; i < pose_for_points->scan->numPoints; i++) {
                pBody[0] = pose_for_points->scan->points[i].x;
                pBody[1] = pose_for_points->scan->points[i].y;
                if(in_node_frame)
                    bot_trans_apply_vec(&nd_curr_to_c_node, pBody, pLocal);
                else
                    bot_trans_apply_vec(&nd_curr_to_pf, pBody, pLocal);
                region_pl->points[c].x = pLocal[0];
                region_pl->points[c].y = pLocal[1];
                c++; 
            }
        }
        return region_pl;
    }    
}

//indexes here are positions 
Scan* SlamParticle::getScansForRegion(SlamNode *node, double radius, bool in_node_frame=true){
    
    double pBody[3] = { 0, 0, 0 };
    double pLocal[3];
    
    RegionNode *region = node->region_node;
    Pose2d pose = node->getPose();

    int no_points = 0;

    BotTrans c_node_to_pf = getBotTransFromPose(node->getPose());
    BotTrans pf_to_c_node = c_node_to_pf;
    bot_trans_invert(&pf_to_c_node);


    vector<SlamNode *> nodes_to_add; 
    for(int i=0; i < region->nodes.size(); i++){
        SlamNode *nd = region->nodes[i];
        Pose2d delta = pose.ominus(nd->getPose());
        double t_dist = hypot(delta.x(), delta.y());
        if(t_dist > radius){
            continue;
        }
        nodes_to_add.push_back(nd);
        NodeScan *pose_for_points = nd->slam_pose;
        no_points += pose_for_points->scan->numPoints;         
    }

    smPoint *points = (smPoint *) calloc(no_points, sizeof(smPoint));

    int c = 0;    

    for(int i=0; i < nodes_to_add.size(); i++){
        SlamNode *nd = nodes_to_add[i];

        NodeScan *pose_for_points = nd->slam_pose;
        
        Pose2d value = nd->getPose();

        BotTrans nd_curr_to_pf = getBotTransFromPose(value);
        BotTrans nd_curr_to_c_node; 
        bot_trans_apply_trans_to(&pf_to_c_node, &nd_curr_to_pf, &nd_curr_to_c_node);

        for (unsigned i = 0; i < pose_for_points->scan->numPoints; i++) {
            pBody[0] = pose_for_points->scan->points[i].x;
            pBody[1] = pose_for_points->scan->points[i].y;
            if(in_node_frame)
                bot_trans_apply_vec(&nd_curr_to_c_node, pBody, pLocal);
            else
                bot_trans_apply_vec(&nd_curr_to_pf, pBody, pLocal);

            points[c].x = pLocal[0];
            points[c].y = pLocal[1];

            c++; 
        }
    }
    
    ScanTransform T;
    memset(&T, 0, sizeof(T));

    Scan *scan = new Scan(no_points, points, T, SM_HOKUYO_UTM, node->slam_pose->utime, true);

    return scan;
}


//indexes here are positions 
pointlist2d_t * SlamParticle::getPointsForSegment(SlamNode *node, double radius, bool in_node_frame=true){
    double pBody[3] = { 0, 0, 0 };
    double pLocal[3];

    RegionSegment *segment = node->segment;
    Pose2d pose = node->getPose();

    int no_points = 0;

    BotTrans c_node_to_pf = getBotTransFromPose(node->getPose());
    BotTrans pf_to_c_node = c_node_to_pf;
    bot_trans_invert(&pf_to_c_node);

    for(int i=0; i < segment->nodes.size(); i++){
        SlamNode *nd = segment->nodes[i];
        Pose2d delta = pose.ominus(nd->getPose());
        double t_dist = hypot(delta.x(), delta.y());
        if(t_dist > radius){
            continue;
        }
        NodeScan *pose_for_points = nd->slam_pose;
        no_points += pose_for_points->scan->numPoints;         
    }

    pointlist2d_t *region_pl = pointlist2d_new(no_points);

    int c = 0;    

    for(int i=0; i < segment->nodes.size(); i++){
        SlamNode *nd = segment->nodes[i];
        Pose2d delta = pose.ominus(nd->getPose());
        double t_dist = hypot(delta.x(), delta.y());
        if(t_dist > radius){
            continue;
        }
        NodeScan *pose_for_points = nd->slam_pose;
        
        Pose2d value = nd->getPose();

        BotTrans nd_curr_to_pf = getBotTransFromPose(value);
        BotTrans nd_curr_to_c_node; 
        bot_trans_apply_trans_to(&pf_to_c_node, &nd_curr_to_pf, &nd_curr_to_c_node);

        for (unsigned i = 0; i < pose_for_points->scan->numPoints; i++) {
            pBody[0] = pose_for_points->scan->points[i].x;
            pBody[1] = pose_for_points->scan->points[i].y;
            if(in_node_frame)
                bot_trans_apply_vec(&nd_curr_to_c_node, pBody, pLocal);
            else
                bot_trans_apply_vec(&nd_curr_to_pf, pBody, pLocal);
            region_pl->points[c].x = pLocal[0];
            region_pl->points[c].y = pLocal[1];
            c++; 
        }
    }

    return region_pl;
}

//indexes here are positions 
pointlist2d_t * SlamParticle::getPointsForRegion(RegionNode *region){
    double pBody[3] = { 0, 0, 0 };
    double pLocal[3];

    int no_points = 0;

    for(int i=0; i < region->nodes.size(); i++){
        SlamNode *nd = region->nodes[i];
        NodeScan *pose_for_points = nd->slam_pose;
        no_points += pose_for_points->scan->numPoints; 
    }

    pointlist2d_t *region_pl = pointlist2d_new(no_points);

    int c = 0;    

    for(int i=0; i < region->nodes.size(); i++){
        SlamNode *nd = region->nodes[i];
        NodeScan *pose_for_points = nd->slam_pose;

        Pose2d value = nd->getPose();

        BotTrans nd_curr_to_pf = getBotTransFromPose(value);

        for (unsigned i = 0; i < pose_for_points->scan->numPoints; i++) {
            pBody[0] = pose_for_points->scan->points[i].x;
            pBody[1] = pose_for_points->scan->points[i].y;
            bot_trans_apply_vec(&nd_curr_to_pf, pBody, pLocal);
            region_pl->points[c].x = pLocal[0];
            region_pl->points[c].y = pLocal[1];
            c++; 
        }
    }

    return region_pl;
}

int SlamParticle::addNodesToScanMatcher(ScanMatcher *_sm, vector<SlamNode *> nodes){
    int added = 0;

    int count = 0;

    for(size_t i=0; i < nodes.size(); i++){
        SlamNode *nd = nodes[i];
        count++;
        NodeScan *pose_for_points = nd->slam_pose;

        Pose2d value_basic = nd->getPose();

        ScanTransform T;
        T.x = value_basic.x();
        T.y = value_basic.y();
        T.theta = value_basic.t();

        T.score = pose_for_points->scan->T.score;
        T.hits = pose_for_points->scan->T.hits;

        for (int k=0; k<9; k++)
            T.sigma[k] = pose_for_points->scan->T.sigma[k];
        
        pose_for_points->scan->applyTransform(T);
   
        _sm->addScan(pose_for_points->scan, false);
        added = 1;
    }

    if(added == 0)
        return 0;

    //tell the scanmatcher to build the map 
    _sm->addScan(NULL, true);     
    return added;
}

pointlist2d_t *SlamParticle::getRegionBoundingbox(RegionNode *region){
    //fprintf(stderr, "Creating region BBox for region : %d\n", region->region_id);

    SlamNode *mean_node = region->mean_node;
    Pose2d tocheck_value = mean_node->getPose();

    double dist =0;
    double pos[2], next_pos[2];
    double start[2];


    // Node->Slam_Pose->Scan laser scans are decimated and in the body frame
    // We will generate the local occupancy grid in the Query Body frame
    BotTrans queryBodyToSelf = getBotTransFromPose(Pose2d(0,0,0));
    
    BotTrans queryBodyToLocal;
    
    BotTrans localToQueryBody;
    queryBodyToLocal.trans_vec[0] = tocheck_value.x();
    queryBodyToLocal.trans_vec[1] = tocheck_value.y();
    queryBodyToLocal.trans_vec[2] = 0.0;
    double rpyQueryBody[3] = {0.0, 0.0, tocheck_value.t()};
    bot_roll_pitch_yaw_to_quat (rpyQueryBody, queryBodyToLocal.rot_quat);

    localToQueryBody = queryBodyToLocal;
    bot_trans_invert (&localToQueryBody);

    BotTrans matchBodyToLocal;
    //BotTrans matchLaserToLocal;
    BotTrans matchBodyToQueryBody;

    double rpyMatchBody[3] = {0.0, 0.0, 0.0};
    //this breaks down - if we use the rear laser as all the points are jumbled together 
    double laserStartMatchBody[3] = {0,0,0};//{0.4460, -0.0101, 0.1};
    double laserStartQueryBody[3];
    double pMatchBody[3] =  {0.0, 0.0, 0.0};
    double pMatchBodyNext[3] = {0.0, 0.0, 0.0};
    double pQueryBody[3];
    double pQueryBodyNext[3];

    // Find the nodes (and scans) near the local search area
    //for each node - get circle surrounding the node (out to mabye 3m ?) 
    //add this to the point list 
    //then find the convex hull of it 

    double max_scan_distance = 2.0;
    
    vector<point2d_t> scan_points;
    
    //the other option is to grow the region and then find the boundary (bounding box) 
    
    for(int k = 0; k < region->nodes.size(); k++){
        SlamNode *nd = region->nodes[k];
        Pose2d value = nd->getPose();

        //loop through the points and add
        ScanTransform T;
        T.x = value.x();
        T.y = value.y();
        T.theta = value.t();
        
        Pose2d match = nd->getPose();
        
        // Determine the transform from Match Body ---> Query Body
        
        // Set the BotTrans from Match BODY to LOCAL
        matchBodyToLocal.trans_vec[0] = match.x();
        matchBodyToLocal.trans_vec[1] = match.y();
        matchBodyToLocal.trans_vec[2] = 0.0;
        rpyMatchBody[2] = match.t();
        bot_roll_pitch_yaw_to_quat (rpyMatchBody, matchBodyToLocal.rot_quat);

        // ... and now Match Body ---> Query Body
        bot_trans_apply_trans_to (&localToQueryBody, &matchBodyToLocal, &matchBodyToQueryBody);
        
        Scan *scan = nd->slam_pose->scan;
        
        float clamp[2] = {0,1.0} ;
        float ray_trace_clamp[2] = {-11.0, 6};
        
        // Transform Match Laser (0,0,0) ---> Query Laser frame
        bot_trans_apply_vec ( &matchBodyToQueryBody, laserStartMatchBody, laserStartQueryBody);
        start[0] = laserStartQueryBody[0];
        start[1] = laserStartQueryBody[1];
        
        for (unsigned i = 0; i < scan->numPoints; i++) {
            point2d_t pt;
            
            if(hypot(scan->points[i].x, scan->points[i].y) > max_scan_distance){
                double t = atan2(scan->points[i].y, scan->points[i].x);
                pMatchBody[0] = max_scan_distance*cos(t);
                pMatchBody[1] = max_scan_distance*sin(t);
            }
            else{            
                pMatchBody[0] = scan->points[i].x;
                pMatchBody[1] = scan->points[i].y;
            }

            //bot_trans_apply_vec ( &matchBodyToQueryBody, pMatchBody, pQueryBody);
            bot_trans_apply_vec ( &matchBodyToLocal, pMatchBody, pQueryBody);
            pos[0] = pQueryBody[0];
            pos[1] = pQueryBody[1];
            
            pt.x = pos[0];
            pt.y = pos[1];
            scan_points.push_back(pt);
            //local_px_map->rayTrace(start, pos, -1, 2, ray_trace_clamp);
        }            
    }

    pointlist2d_t *scans = pointlist2d_new((int) scan_points.size());
    for(int i=0; i < scan_points.size(); i++){
        scans->points[i].x = scan_points[i].x;
        scans->points[i].y = scan_points[i].y;
        //fprintf(stderr, "%f,%f\n", scan_points[i].x, scan_points[i].y);
    }
    //fprintf(stderr, "Size : %d\n", (int) scan_points.size());

    //    BotTrans bodyToLocal;
    
    //return scans;
    pointlist2d_t *convex_scans = convexhull_graham_scan_2d(scans);
    pointlist2d_free(scans);
    return convex_scans;//scans;
}

pointlist2d_t *SlamParticle::getRegionRayTrace(RegionNode *region){
    SlamNode *mean_node = region->mean_node;
    Pose2d tocheck_value = mean_node->getPose();

    // Clear the copy of the local occupancy grid
    memset (local_px_map->data, 0, local_px_map->num_cells*sizeof(float));

    double dist =0;
    double pos[2], next_pos[2];
    double start[2];


    // Node->Slam_Pose->Scan laser scans are decimated and in the body frame
    // We will generate the local occupancy grid in the Query Body frame
    BotTrans queryBodyToSelf = getBotTransFromPose(Pose2d(0,0,0));
    
    BotTrans queryBodyToLocal;
    
    BotTrans localToQueryBody;
    queryBodyToLocal.trans_vec[0] = tocheck_value.x();
    queryBodyToLocal.trans_vec[1] = tocheck_value.y();
    queryBodyToLocal.trans_vec[2] = 0.0;
    double rpyQueryBody[3] = {0.0, 0.0, tocheck_value.t()};
    bot_roll_pitch_yaw_to_quat (rpyQueryBody, queryBodyToLocal.rot_quat);

    localToQueryBody = queryBodyToLocal;
    bot_trans_invert (&localToQueryBody);

    BotTrans matchBodyToLocal;
    //BotTrans matchLaserToLocal;
    BotTrans matchBodyToQueryBody;

    double rpyMatchBody[3] = {0.0, 0.0, 0.0};
    //this breaks down - if we use the rear laser as all the points are jumbled together 
    double laserStartMatchBody[3] = {0,0,0};//{0.4460, -0.0101, 0.1};
    double laserStartQueryBody[3];
    double pMatchBody[3] =  {0.0, 0.0, 0.0};
    double pMatchBodyNext[3] = {0.0, 0.0, 0.0};
    double pQueryBody[3];
    double pQueryBodyNext[3];

    // Find the nodes (and scans) near the local search area
    for(int k = 0; k < region->nodes.size(); k++){
        SlamNode *nd = region->nodes[k];
        Pose2d value = nd->getPose();

        //loop through the points and add
        ScanTransform T;
        T.x = value.x();
        T.y = value.y();
        T.theta = value.t();
        
        Pose2d match = nd->getPose();
        
        // Determine the transform from Match Body ---> Query Body
        
        // Set the BotTrans from Match BODY to LOCAL
        matchBodyToLocal.trans_vec[0] = match.x();
        matchBodyToLocal.trans_vec[1] = match.y();
        matchBodyToLocal.trans_vec[2] = 0.0;
        rpyMatchBody[2] = match.t();
        bot_roll_pitch_yaw_to_quat (rpyMatchBody, matchBodyToLocal.rot_quat);

        // ... and now Match Body ---> Query Body
        bot_trans_apply_trans_to (&localToQueryBody, &matchBodyToLocal, &matchBodyToQueryBody);
        
        Scan *scan = nd->slam_pose->scan;
        
        float clamp[2] = {0,1.0} ;
        float ray_trace_clamp[2] = {-11.0, 6};
        
        // Transform Match Laser (0,0,0) ---> Query Laser frame
        bot_trans_apply_vec ( &matchBodyToQueryBody, laserStartMatchBody, laserStartQueryBody);
        start[0] = laserStartQueryBody[0];
        start[1] = laserStartQueryBody[1];
                   
        for (unsigned i = 0; i < scan->numPoints; i++) {
            if(hypot(scan->points[i].x, scan->points[i].y) > MAX_SCAN_FOR_BBOX){
                double t = atan2(scan->points[i].y, scan->points[i].x);
                pMatchBody[0] = MAX_SCAN_FOR_BBOX*cos(t);
                pMatchBody[1] = MAX_SCAN_FOR_BBOX*sin(t);
            
                bot_trans_apply_vec ( &matchBodyToQueryBody, pMatchBody, pQueryBody);
                pos[0] = pQueryBody[0];
                pos[1] = pQueryBody[1];
            
                local_px_map->rayTrace(start, pos, -1, 0, ray_trace_clamp);
            }
            else{
                pMatchBody[0] = scan->points[i].x;
                pMatchBody[1] = scan->points[i].y;
            
                bot_trans_apply_vec ( &matchBodyToQueryBody, pMatchBody, pQueryBody);
                pos[0] = pQueryBody[0];
                pos[1] = pQueryBody[1];
            
                local_px_map->rayTrace(start, pos, -1, 2, ray_trace_clamp);
            }
        }     
    }

    if(1){
        const occ_map_pixel_map_t *map = local_px_map->get_pixel_map_t(bot_timestamp_now());
        occ_map_pixel_map_t_publish(lcm, "PIXEL_MAP", map);
    }

    //LaserSim2D(const occ_map::FloatPixelMap * map, int nranges, float rad0, float radstep, float max_range);
    LaserSim2D laserSim(local_px_map, 1441, -M_PI, 0.004363323, 16.0);

    const bot_core_planar_lidar_t *sim_laser = laserSim.simulate(0, 0, 0, bot_timestamp_now());
    double spatialDecimationThresh = 0.2;
    int beam_skip = 2;
    double maxRange = 15.0;//15.0;
    float validBeamAngles[2] = {-M_PI, M_PI};
    int no_points = 0;
    smPoint *sm_points = get_scan_points_from_laser(sim_laser, beam_skip, spatialDecimationThresh, maxRange, 
                                                    validBeamAngles, 
                                                    &no_points, &queryBodyToLocal);
    pointlist2d_t *scans = pointlist2d_new(no_points);
    
    for(int i=0; i < no_points; i++){
        scans->points[i].x = sm_points[i].x;
        scans->points[i].y = sm_points[i].y;            
    }

    delete sm_points;

    if(1){
        bot_core_planar_lidar_t_publish(lcm,"SIM_SLAM_LASER", sim_laser);
    }

    pointlist2d_t *convex_scans = convexhull_graham_scan_2d(scans);
    pointlist2d_free(scans);
    return convex_scans;//scans;
}

void SlamParticle::updateRegionRayTrace(RegionNode *region){
    SlamNode *mean_node = region->mean_node;
    Pose2d tocheck_value = mean_node->getPose();

    if(param.polygonMode == 3){
        // Clear the copy of the local occupancy grid
        memset (local_px_map->data, 0, local_px_map->num_cells*sizeof(float));

        double dist =0;
        double pos[2], next_pos[2];
        double start[2];


        // Node->Slam_Pose->Scan laser scans are decimated and in the body frame
        // We will generate the local occupancy grid in the Query Body frame
        BotTrans queryBodyToSelf = getBotTransFromPose(Pose2d(0,0,0));
    
        BotTrans queryBodyToLocal;
    
        BotTrans localToQueryBody;
        queryBodyToLocal.trans_vec[0] = tocheck_value.x();
        queryBodyToLocal.trans_vec[1] = tocheck_value.y();
        queryBodyToLocal.trans_vec[2] = 0.0;
        double rpyQueryBody[3] = {0.0, 0.0, tocheck_value.t()};
        bot_roll_pitch_yaw_to_quat (rpyQueryBody, queryBodyToLocal.rot_quat);

        localToQueryBody = queryBodyToLocal;
        bot_trans_invert (&localToQueryBody);

        BotTrans matchBodyToLocal;
        //BotTrans matchLaserToLocal;
        BotTrans matchBodyToQueryBody;

        double rpyMatchBody[3] = {0.0, 0.0, 0.0};
        //this breaks down - if we use the rear laser as all the points are jumbled together 
        double laserStartMatchBody[3] = {0,0,0};//{0.4460, -0.0101, 0.1};
        double laserStartQueryBody[3];
        double pMatchBody[3] =  {0.0, 0.0, 0.0};
        double pMatchBodyNext[3] = {0.0, 0.0, 0.0};
        double pQueryBody[3];
        double pQueryBodyNext[3];

        // Find the nodes (and scans) near the local search area
        for(int k = 0; k < region->nodes.size(); k++){
            SlamNode *nd = region->nodes[k];
            Pose2d value = nd->getPose();

            //loop through the points and add
            ScanTransform T;
            T.x = value.x();
            T.y = value.y();
            T.theta = value.t();
        
            Pose2d match = nd->getPose();
        
            // Determine the transform from Match Body ---> Query Body
        
            // Set the BotTrans from Match BODY to LOCAL
            matchBodyToLocal.trans_vec[0] = match.x();
            matchBodyToLocal.trans_vec[1] = match.y();
            matchBodyToLocal.trans_vec[2] = 0.0;
            rpyMatchBody[2] = match.t();
            bot_roll_pitch_yaw_to_quat (rpyMatchBody, matchBodyToLocal.rot_quat);

            // ... and now Match Body ---> Query Body
            bot_trans_apply_trans_to (&localToQueryBody, &matchBodyToLocal, &matchBodyToQueryBody);
        
            Scan *scan = nd->slam_pose->scan;
        
            float clamp[2] = {0,1.0} ;
            float ray_trace_clamp[2] = {-11.0, 6};
        
            // Transform Match Laser (0,0,0) ---> Query Laser frame
            bot_trans_apply_vec ( &matchBodyToQueryBody, laserStartMatchBody, laserStartQueryBody);
            start[0] = laserStartQueryBody[0];
            start[1] = laserStartQueryBody[1];
        
            for (unsigned i = 0; i < scan->numPoints; i++) {
                if(hypot(scan->points[i].x, scan->points[i].y) > MAX_SCAN_FOR_BBOX){
                    double t = atan2(scan->points[i].y, scan->points[i].x);
                    pMatchBody[0] = MAX_SCAN_FOR_BBOX*cos(t);
                    pMatchBody[1] = MAX_SCAN_FOR_BBOX*sin(t);
            
                    bot_trans_apply_vec ( &matchBodyToQueryBody, pMatchBody, pQueryBody);
                    pos[0] = pQueryBody[0];
                    pos[1] = pQueryBody[1];
            
                    local_px_map->rayTrace(start, pos, -1, 0, ray_trace_clamp);
                }
                else{
                    pMatchBody[0] = scan->points[i].x;
                    pMatchBody[1] = scan->points[i].y;
            
                    bot_trans_apply_vec ( &matchBodyToQueryBody, pMatchBody, pQueryBody);
                    pos[0] = pQueryBody[0];
                    pos[1] = pQueryBody[1];
            
                    local_px_map->rayTrace(start, pos, -1, 2, ray_trace_clamp);
                }
            }     
            /*for (unsigned i = 0; i < scan->numPoints; i++) {
                if(hypot(scan->points[i].x, scan->points[i].y) > MAX_SCAN_FOR_BBOX)
                    continue;

                pMatchBody[0] = scan->points[i].x;
                pMatchBody[1] = scan->points[i].y;
            
                bot_trans_apply_vec ( &matchBodyToQueryBody, pMatchBody, pQueryBody);
                pos[0] = pQueryBody[0];
                pos[1] = pQueryBody[1];
            
                local_px_map->rayTrace(start, pos, -1, 2, ray_trace_clamp);
            }*/          
        }

        double maxRange = 15.0;//15.0;
        //LaserSim2D(const occ_map::FloatPixelMap * map, int nranges, float rad0, float radstep, float max_range);
        //since we decimate this - cant we just extract at a lower resolution ?? 
    
        LaserSim2D laserSim(local_px_map, 1441, -M_PI, 0.004363323, maxRange + 1);

        BotTrans bodyToLocal;
    
        const bot_core_planar_lidar_t *sim_laser = laserSim.simulate(0, 0, 0, bot_timestamp_now());
        double spatialDecimationThresh = 0.2;
        int beam_skip = 2;
    
        float validBeamAngles[2] = {-M_PI, M_PI};
        int no_points = 0;

        smPoint *sm_points = get_scan_points_from_laser(sim_laser, beam_skip, spatialDecimationThresh, maxRange, 
                                                        validBeamAngles, 
                                                        &no_points);
        pointlist2d_t *scans = pointlist2d_new(no_points);
    
        for(int i=0; i < no_points; i++){
            scans->points[i].x = sm_points[i].x;
            scans->points[i].y = sm_points[i].y;            
        }

        region->insertRayTracedPoints(scans);
        pointlist2d_free(scans);

        delete sm_points;
    }
    if(param.polygonMode == 4){
        double dist =0;
        double pos[2], next_pos[2];
        double start[2];

        // Node->Slam_Pose->Scan laser scans are decimated and in the body frame
        // We will generate the local occupancy grid in the Query Body frame
        BotTrans queryBodyToSelf = getBotTransFromPose(Pose2d(0,0,0));
    
        BotTrans queryBodyToLocal;
    
        BotTrans localToQueryBody;
        queryBodyToLocal.trans_vec[0] = tocheck_value.x();
        queryBodyToLocal.trans_vec[1] = tocheck_value.y();
        queryBodyToLocal.trans_vec[2] = 0.0;
        double rpyQueryBody[3] = {0.0, 0.0, tocheck_value.t()};
        bot_roll_pitch_yaw_to_quat (rpyQueryBody, queryBodyToLocal.rot_quat);

        localToQueryBody = queryBodyToLocal;
        bot_trans_invert (&localToQueryBody);

        BotTrans matchBodyToLocal;
        //BotTrans matchLaserToLocal;
        BotTrans matchBodyToQueryBody;

        double rpyMatchBody[3] = {0.0, 0.0, 0.0};
        //this breaks down - if we use the rear laser as all the points are jumbled together 
        double laserStartMatchBody[3] = {0,0,0};//{0.4460, -0.0101, 0.1};
        double laserStartQueryBody[3];
        double pMatchBody[3] =  {0.0, 0.0, 0.0};
        double pMatchBodyNext[3] = {0.0, 0.0, 0.0};
        double pQueryBody[3];
        double pQueryBodyNext[3];

        // Find the nodes (and scans) near the local search area
        //for each node - get circle surrounding the node (out to mabye 3m ?) 
        //add this to the point list 
        //then find the convex hull of it 

        double max_scan_distance = 2.0;
    
        vector<point2d_t> scan_points;
    
        //the other option is to grow the region and then find the boundary (bounding box) 
    
        for(int k = 0; k < region->nodes.size(); k++){
            SlamNode *nd = region->nodes[k];
            Pose2d value = nd->getPose();

            //loop through the points and add
            ScanTransform T;
            T.x = value.x();
            T.y = value.y();
            T.theta = value.t();
        
            Pose2d match = nd->getPose();
        
            // Determine the transform from Match Body ---> Query Body
        
            // Set the BotTrans from Match BODY to LOCAL
            matchBodyToLocal.trans_vec[0] = match.x();
            matchBodyToLocal.trans_vec[1] = match.y();
            matchBodyToLocal.trans_vec[2] = 0.0;
            rpyMatchBody[2] = match.t();
            bot_roll_pitch_yaw_to_quat (rpyMatchBody, matchBodyToLocal.rot_quat);

            // ... and now Match Body ---> Query Body
            bot_trans_apply_trans_to (&localToQueryBody, &matchBodyToLocal, &matchBodyToQueryBody);
        
            Scan *scan = nd->slam_pose->scan;
        
            float clamp[2] = {0,1.0} ;
            float ray_trace_clamp[2] = {-11.0, 6};
        
            // Transform Match Laser (0,0,0) ---> Query Laser frame
            bot_trans_apply_vec ( &matchBodyToQueryBody, laserStartMatchBody, laserStartQueryBody);
            start[0] = laserStartQueryBody[0];
            start[1] = laserStartQueryBody[1];
        
            for (unsigned i = 0; i < scan->numPoints; i++) {
                point2d_t pt;
                if(hypot(scan->points[i].x, scan->points[i].y) > max_scan_distance){
                    double t = atan2(scan->points[i].y, scan->points[i].x);
                    pMatchBody[0] = max_scan_distance*cos(t);
                    pMatchBody[1] = max_scan_distance*sin(t);
                }
                else{
                    pMatchBody[0] = scan->points[i].x;
                    pMatchBody[1] = scan->points[i].y;
                }
                bot_trans_apply_vec ( &matchBodyToQueryBody, pMatchBody, pQueryBody);
                pos[0] = pQueryBody[0];
                pos[1] = pQueryBody[1];
            
                pt.x = pos[0];
                pt.y = pos[1];
                scan_points.push_back(pt);
            }            
        }

        pointlist2d_t *scans = pointlist2d_new((int) scan_points.size());
        for(int i=0; i < scan_points.size(); i++){
            scans->points[i].x = scan_points[i].x;
            scans->points[i].y = scan_points[i].y;
        }

        BotTrans bodyToLocal;
    
        region->insertRayTracedPoints(scans);
        pointlist2d_free(scans);
    }
}

void SlamParticle::publishOccupancyMap () {
    // First determine extent of pixel map
    double minx, miny, maxx, maxy;
    for (int i=0; i < graph->slam_node_list.size(); i++) {
        SlamNode *node = graph->slam_node_list.at(i);

        double nodex = node->getPose().x();
        double nodey = node->getPose().y();

        if (i == 0) {
            minx = nodex;
            maxx = nodex;
            miny = nodey;
            maxy = nodey;
        } else {
            if (nodex < minx)
                minx = nodex;
            else if (nodex > maxx)
                maxx = nodex;

            if (nodey < miny)
                miny = nodey;
            else if (nodey > maxy)
                maxy = nodey;
        }
    }

    double xy0[] = { minx - 15, miny - 15};
    double xy1[] = { maxx + 15, maxy + 15};

    FloatPixelMap *pixmap = new FloatPixelMap (xy0, xy1, PIXMAP_RESOLUTION, 0);
    float ray_trace_clamp[2] = {-11.0, 6};

    // Now, populate the pixel map
    for (int i=0; i < graph->slam_node_list.size(); i++) {
        SlamNode *node = graph->slam_node_list.at(i);

        // skip nodes that have poor incremental scanmatch as indicative of being outside
        if (param.skipOutsideSM && node->slam_pose->inc_constraint_sm->hitpct <= param.incSMAcceptanceThreshold)
            continue;

        Scan *scan = node->slam_pose->scan;

        BotTrans bodyToLocal;
        bodyToLocal.trans_vec[0] = node->getPose().x();
        bodyToLocal.trans_vec[1] = node->getPose().y();
        bodyToLocal.trans_vec[2] = 0;
        double rpy[3] = {0.0, 0.0, node->getPose().t()};
        bot_roll_pitch_yaw_to_quat (rpy, bodyToLocal.rot_quat);
        double pBody[3] = {0, 0, 0};
        double pLocal[3];

        //hmm - this is actually wrong - the laser 0 is in a different location 

        double start[2] = { node->getPose().x(),  node->getPose().y()};

        float clamp_bounds[2] = {0.0, 1.0};

        for (int j=0; j < scan->numPoints; j++) {
            pBody[0] = scan->points[j].x;
            pBody[1] = scan->points[j].y;            pBody[2] = 0.0;
            bot_trans_apply_vec (&bodyToLocal, pBody, pLocal);

            // Update the pixel map by either modifying the cell that the lidar return
            // falls in (via updateValue) or performing ray tracing.
            double pXY[2] = {pLocal[0], pLocal[1]};
            if (pixmap->isInMap (pXY)){
                pixmap->rayTrace(start, pXY, -1, 2, ray_trace_clamp);
                //pixmap->updateValue (pXY, PIXMAP_HIT_INC, clamp_bounds);
            }
        }
    }

    const occ_map_pixel_map_t *pixmap_msg = pixmap->get_pixel_map_t (bot_timestamp_now());
    occ_map_pixel_map_t_publish (lcm, "PIXMAP_MAX_PARTICLE", pixmap_msg);

    delete pixmap;

}


void SlamParticle::printEdges(){
    fprintf(stderr, "Edge matrix\n");
    fprintf(stderr, "N: ");
    for(int i=0; i<graph->slam_nodes.size(); i++)
        fprintf(stderr, "\t%d",i);
    //fprintf(stderr, "\n");
    for(int i=0; i<graph->slam_nodes.size(); i++){
        fprintf(stderr, "\n%d",i);
        for(int j=0; j<graph->slam_nodes.size(); j++){
            bool print = false;
            for(int e=0; e<graph->slam_constraints.size(); e++){
                if(graph->slam_constraints.at(e)->node1->id == graph->slam_nodes.at(i)->id){
                    if(graph->slam_constraints.at(e)->node2->id == graph->slam_nodes.at(j)->id){
                        if(graph->slam_constraints.at(e)->type == SLAM_GRAPH_EDGE_T_TYPE_LANGUAGE)
                            fprintf(stderr, "\tL");
                        else if(graph->slam_constraints.at(e)->type == SLAM_GRAPH_EDGE_T_TYPE_SM_LC)
                            fprintf(stderr, "\tS");
                        else
                            fprintf(stderr, "\tE");
                        print = true;
                    }
                }
            }
            if(!print)
                fprintf(stderr, "\t");
        }
    }
    fprintf(stderr, "\n");
}

void SlamParticle::printParticleInformation(){
    fprintf(stderr, "=============== Particle ID : %d ==============\n", graph_id);
    graph->printComplexLanguageEvents();
    if(param.useFactorGraphs){
        graph->runBP();    
    }
}

Pose2d SlamParticle::getLastPose(){
    SlamNode *current_node = graph->last_added_node;//getSlamNode(graph->slam_node_list.size()-1);
    Pose2d current_pose =  current_node->getPose();
    return current_pose;
}

BotTrans SlamParticle::getBotTransForCurrentNode(){
    bot_core_pose_t pose;
    memset(&pose, 0, sizeof(pose));
    SlamNode *current_node = graph->last_added_node;//getSlamNode(graph->slam_node_list.size()-1);
    Pose2d current_pose(0,0,0);
    if(current_node){
        current_pose =  current_node->getPose();
    }
    pose.pos[0] = current_pose.x();
    pose.pos[1] = current_pose.y();
    double rpy[3] = { 0, 0, current_pose.t() };
    bot_roll_pitch_yaw_to_quat(rpy, pose.orientation);
    pose.utime = current_node->slam_pose->utime;
    
    BotTrans bt_body_to_global;
    bot_trans_set_from_quat_trans (&bt_body_to_global, pose.orientation, pose.pos);

    BotTrans bt_body_to_local = current_node->slam_pose->body_to_local;
    
    BotTrans global_to_local;
    //global_to_local.utime = current_node->slam_pose->utime;//laser_msg->utime;

    BotTrans bt_global_to_body;
    bot_trans_invert (&bt_body_to_global);
    bot_trans_copy (&bt_global_to_body, &bt_body_to_global);

    bot_trans_apply_trans_to (&bt_body_to_local, &bt_global_to_body, &global_to_local);

    //fprintf(stderr, "\n\n\n Transform : %f, %f\n", global_to_local.trans_vec[0] , global_to_local.trans_vec[1]);
    //bot_core_rigid_transform_t_publish (app->lcm, "GLOBAL_TO_LOCAL", &global_to_local);
    return global_to_local;
}

pair<int,int> SlamParticle::getNoOfLoopClosures(){
    map<int, SlamConstraint *>::iterator c_it;
    //add the constraints that are present in the part (but not in the current graph) 
    int dist_lc = 0;
    int lang_lc = 0;
    for ( c_it= graph->slam_constraints.begin() ; c_it != graph->slam_constraints.end(); c_it++ ){
        SlamConstraint *edge = c_it->second;

        if(edge->type == SLAM_GRAPH_EDGE_T_TYPE_SM_LC && edge->status == SLAM_GRAPH_EDGE_T_STATUS_SUCCESS){
            dist_lc++;
        }
        
        if(edge->type == SLAM_GRAPH_EDGE_T_TYPE_LANGUAGE || edge->type == SLAM_GRAPH_EDGE_T_TYPE_LANGUAGE_FULL){
            lang_lc++; 
        }
    }

    return make_pair(dist_lc, lang_lc);
}

void SlamParticle::addLoopClosuresSet(set<pair<int, int> > *dist_lc, set<pair<int, int> > *lang_lc){
    map<int, SlamConstraint *>::iterator c_it;
    //add the constraints that are present in the part (but not in the current graph) 
    for ( c_it= graph->slam_constraints.begin() ; c_it != graph->slam_constraints.end(); c_it++ ){
        SlamConstraint *edge = c_it->second;

        if(edge->type == SLAM_GRAPH_EDGE_T_TYPE_SM_LC && edge->status == SLAM_GRAPH_EDGE_T_STATUS_SUCCESS){
            dist_lc->insert(make_pair(edge->node1->id, edge->node2->id));
        }
        
        if(edge->type == SLAM_GRAPH_EDGE_T_TYPE_LANGUAGE || edge->type == SLAM_GRAPH_EDGE_T_TYPE_LANGUAGE_FULL){
            lang_lc->insert(make_pair(edge->node1->id, edge->node2->id));
        }
    }
}

void SlamParticle::clearActiveNodeParams(){
    if(tfFromActiveNode){
        delete tfFromActiveNode;
        tfFromActiveNode = NULL; 
    }
    if(noiseFromActiveNode){
        delete noiseFromActiveNode;
        noiseFromActiveNode = NULL; 
    }
}

// ------------------ Utils : Messages ---------------- //

/*void SlamParticle::getLCMRegionMessageFromGraph(slam_graph_region_particle_t *particle){
    particle->id = graph_id;
    particle->no_nodes = graph->slam_node_list.size();

    int current_split = 0; 
    if(current_segment.size()> 0){
        current_split = 1;
    }

    particle->no_regions = graph->region_node_list.size() + current_split;
    particle->region_list = (slam_graph_region_t *) calloc(particle->no_regions, sizeof(slam_graph_region_t));
    if(param.polygonMode == 1){
    //this is for nodes 
        graph->updateBoundingBoxes();
    }
    
    //graph->calculateRegionDistance();
    int no_edges = 0;

    if(param.verbose){
        fprintf(stderr, "Particle ID: %d\n", graph_id);
    }

    particle->no_close_node_pairs = graph->no_close_node_pairs;
    particle->no_same_region_close_pairs = graph->no_same_region_close_pairs;
    particle->no_failed_close_node_pairs = graph->no_failed_close_node_pairs;
    particle->max_dist_of_close_node_pairs = graph->max_dist_of_close_node_pairs;
    particle->average_distance_of_close_node_pairs = graph->average_distance_of_close_node_pairs;

    for(int i=0; i < (particle->no_regions - current_split); i++){
        //maybe the region id's should be renormalized at some point 
        RegionNode *region = graph->region_node_list[i];
        if(param.polygonMode == 1){
            region->updateBoundingBox();
        }
        else{
            region->updateRayTracedPoints();
            //exit(-1);
        }
        particle->region_list[i].id = region->region_id;//i;
        particle->region_list[i].mean_xy[0] = region->mean.x();
        particle->region_list[i].mean_xy[1] = region->mean.y();

        no_edges += region->edges.size();

        
        //lets find the mean and then the node closest to the mean - and that will be the representative node 
        double mean_xy[2] = {0};

        if(param.useFactorGraphs){
            particle->region_list[i].region_type_dist.count = region->region_type_dist.size();
            map<int, double>::iterator it;
            int k = 0;
            particle->region_list[i].region_type_dist.classes = (slam_probability_element_t *) calloc(particle->region_list[i].region_type_dist.count , sizeof(slam_probability_element_t));
            for(it=region->region_type_dist.begin(); it !=region->region_type_dist.end(); it++){
                particle->region_list[i].region_type_dist.classes[k].type = it->first;
                particle->region_list[i].region_type_dist.classes[k].probability = it->second;
                k++;
            }

            particle->region_list[i].region_label_dist.count = region->region_label_dist.size();

            k = 0;
            particle->region_list[i].region_label_dist.classes = (slam_probability_element_t *) calloc(particle->region_list[i].region_label_dist.count , sizeof(slam_probability_element_t));
            //fprintf(stderr, BLUE "Region ID : %d\n" RESET_COLOR, region->region_id);
            for(it=region->region_label_dist.begin(); it !=region->region_label_dist.end(); it++){
                particle->region_list[i].region_label_dist.classes[k].type = it->first;
                particle->region_list[i].region_label_dist.classes[k].probability = it->second;
                //fprintf(stderr, BLUE "\tRegion Label : [%d] -> %.3f\n" RESET_COLOR, (int) it->first, (double) it->second);
                k++;
            }

            //distributions without language - for comparison 

            particle->region_list[i].region_type_dist_no_lang.count = region->region_type_dist_no_lang.size();
            k = 0;
            particle->region_list[i].region_type_dist_no_lang.classes = (slam_probability_element_t *) calloc(particle->region_list[i].region_type_dist_no_lang.count , sizeof(slam_probability_element_t));
            for(it=region->region_type_dist_no_lang.begin(); it !=region->region_type_dist_no_lang.end(); it++){
                particle->region_list[i].region_type_dist_no_lang.classes[k].type = it->first;
                particle->region_list[i].region_type_dist_no_lang.classes[k].probability = it->second;
                k++;
            }

            particle->region_list[i].region_label_dist_no_lang.count = region->region_label_dist_no_lang.size();

            k = 0;
            particle->region_list[i].region_label_dist_no_lang.classes = (slam_probability_element_t *) calloc(particle->region_list[i].region_label_dist_no_lang.count , sizeof(slam_probability_element_t));
            //fprintf(stderr, BLUE "Region ID : %d\n" RESET_COLOR, region->region_id);
            for(it=region->region_label_dist_no_lang.begin(); it !=region->region_label_dist_no_lang.end(); it++){
                particle->region_list[i].region_label_dist_no_lang.classes[k].type = it->first;
                particle->region_list[i].region_label_dist_no_lang.classes[k].probability = it->second;
                //fprintf(stderr, BLUE "\tRegion Label : [%d] -> %.3f\n" RESET_COLOR, (int) it->first, (double) it->second);
                k++;
            }
        }
        else{
            //set ther region type dist to be null 
            particle->region_list[i].region_type_dist.count = 0;
            particle->region_list[i].region_type_dist.classes = NULL;

            //build the label dist from the dirichlet 
            map<int, double> label_dist = region->labeldist->getProbabilities();
            particle->region_list[i].region_label_dist.count = label_dist.size();

            map<int, double>::iterator it;
            int k = 0;
            particle->region_list[i].region_label_dist.classes = (slam_probability_element_t *) calloc(particle->region_list[i].region_label_dist.count , sizeof(slam_probability_element_t));
            //fprintf(stderr, BLUE "Region ID : %d\n" RESET_COLOR, region->region_id);
            for(it=label_dist.begin(); it !=label_dist.end(); it++){
                particle->region_list[i].region_label_dist.classes[k].type = it->first;
                particle->region_list[i].region_label_dist.classes[k].probability = it->second;
                //fprintf(stderr, BLUE "\tRegion Label : [%d] -> %.3f\n" RESET_COLOR, (int) it->first, (double) it->second);
                k++;
            }            
        }

        int nd_count = 0; 
        for(int j=0; j < region->nodes.size(); j++){
            SlamNode *node = region->nodes[j];
            
            if(current_segment.find(node) == current_segment.end()){
                nd_count++;
            }
        }

        particle->region_list[i].count = nd_count; //region->nodes.size();
        particle->region_list[i].nodes = (slam_graph_node_t *) calloc(particle->region_list[i].count , sizeof(slam_graph_node_t));

        int k = 0; 
        for(int j=0; j < region->nodes.size(); j++){
            SlamNode *node = region->nodes[j];
            if(current_segment.find(node) == current_segment.end()){
                slam_graph_node_t *nd = &particle->region_list[i].nodes[k];
                node->updateNodeMsg(nd, param.polygonMode);      
                k++;
                if(node == region->mean_node){
                    particle->region_list[i].center_ind = nd->id;                    
                }
            }
        }
        
        particle->no_objects = (int) graph->object_map.size();
        particle->objects = (slam_object_t *) calloc(particle->no_objects, sizeof(slam_object_t));
        map<int, SlamObject *>::iterator it_o;
        int i=0;
        for(it_o = graph->object_map.begin(); it_o != graph->object_map.end(); it_o++, i++){
            SlamObject *obj = it_o->second;
            slam_object_t *m_obj = &particle->objects[i];
            m_obj->id = obj->id;
            m_obj->type = obj->type;
            if(obj->assigned_node){
                m_obj->node_id = obj->assigned_node->id;
            }
            else{
                m_obj->node_id = -1;
            }
            
            m_obj->pos[0] = obj->pose.x();
            m_obj->pos[1] = obj->pose.y();
            m_obj->pos[2] = 0;
        }
    }

    if(current_split){
        int i = particle->no_regions - 1; 
        //add last region 
        particle->region_list[i].count = (int) current_segment.size(); //region->nodes.size();
        particle->region_list[i].nodes = (slam_graph_node_t *) calloc(particle->region_list[i].count , sizeof(slam_graph_node_t));
        
        particle->region_list[i].id = 10000;//i;
        
        particle->region_list[i].region_type_dist.count = 0;
        particle->region_list[i].region_type_dist.classes = NULL;

        particle->region_list[i].region_label_dist.count = 0;
        particle->region_list[i].region_label_dist.classes = NULL;

        particle->region_list[i].region_type_dist_no_lang.count = 0;
        particle->region_list[i].region_type_dist_no_lang.classes = NULL;

        particle->region_list[i].region_label_dist_no_lang.count = 0;
        particle->region_list[i].region_label_dist_no_lang.classes = NULL;
       
        double mean_xy[2] = {0};
        bool first = true;
        set<SlamNode *>::iterator it_nd;
        int j=0;
        for(it_nd = current_segment.begin(); it_nd != current_segment.end(); it_nd++, j++){
            SlamNode *node = *it_nd;
            
            slam_graph_node_t *nd = &particle->region_list[i].nodes[j];
            node->updateNodeMsg(nd, param.polygonMode);      
            if(first){
                particle->region_list[i].center_ind = nd->id;
                particle->region_list[i].mean_xy[0] = node->getPose().x();
                particle->region_list[i].mean_xy[1] = node->getPose().y();
            }
        }        

        particle->no_objects = 0;
        particle->objects = NULL;
    }

    particle->no_region_edges = no_edges; //graph->region_edge_list.size();
    particle->region_edges = (slam_graph_region_edge_t *) calloc(particle->no_region_edges, sizeof(slam_graph_region_edge_t));

    int k = 0;
    map<int, RegionEdge*>::iterator it;
    for(int i=0; i < particle->no_regions; i++){
        RegionNode *region = graph->region_node_list[i];
        for(it= region->edges.begin(); it!= region->edges.end();it++){
            particle->region_edges[k].edge_id = k;
            particle->region_edges[k].region_1_id = it->second->node1->region_id;
            particle->region_edges[k].region_2_id = it->second->node2->region_id;
            particle->region_edges[k].type = it->second->type;
            k++;
        }
    }
    
    particle->no_edges = graph->slam_constraints.size();
    particle->edge_list = (slam_graph_edge_t *) calloc(particle->no_edges, sizeof(slam_graph_edge_t));

    map<int, SlamConstraint *>::iterator c_it;
    int j= 0;
    for ( c_it= graph->slam_constraints.begin() ; c_it != graph->slam_constraints.end(); c_it++, j++ ){        
        SlamConstraint *ct = c_it->second;
        
        particle->edge_list[j].id = ct->id;
        particle->edge_list[j].node_id_1 = ct->node1->position; 
        particle->edge_list[j].node_id_2 = ct->node2->position; 
        particle->edge_list[j].actual_scanned_node_id_1 = ct->actualnode1->position;
        particle->edge_list[j].actual_scanned_node_id_2 = ct->actualnode2->position;
        particle->edge_list[j].scanmatch_hit = ct->hitpct;
        particle->edge_list[j].type = ct->type;
        particle->edge_list[j].status = ct->status;
    }

    particle->no_rejected_edges = graph->failed_slam_constraints.size();
    particle->rejected_edge_list = (slam_graph_edge_t *) calloc(particle->no_rejected_edges, sizeof(slam_graph_edge_t));

    j= 0;
    for ( c_it= graph->failed_slam_constraints.begin() ; c_it != graph->failed_slam_constraints.end(); c_it++, j++ ){        
        SlamConstraint *ct = c_it->second;
        
        particle->rejected_edge_list[j].id = ct->id;
        particle->rejected_edge_list[j].node_id_1 = ct->node1->position; 
        particle->rejected_edge_list[j].node_id_2 = ct->node2->position; 
        particle->rejected_edge_list[j].actual_scanned_node_id_1 = ct->actualnode1->position;
        particle->rejected_edge_list[j].actual_scanned_node_id_2 = ct->actualnode2->position;
        particle->rejected_edge_list[j].scanmatch_hit = ct->hitpct;
        //fprintf(stderr, YELLOW "Edge hit pct : %f\n", ct->hitpct);
        particle->rejected_edge_list[j].type = ct->type;
        particle->rejected_edge_list[j].status = ct->status;
    }

    particle->segments.no_segments = (int) graph->region_segment_list.size();
        
    particle->segments.list = (slam_graph_segment_t *) calloc(particle->segments.no_segments, sizeof(slam_graph_segment_t));

    for(int i= 0; i < graph->region_segment_list.size(); i++){
        particle->segments.list[i].id = graph->region_segment_list[i]->id;
        particle->segments.list[i].no_nodes = (int) graph->region_segment_list[i]->nodes.size();
        particle->segments.list[i].node_ids = (int64_t *) calloc(particle->segments.list[i].no_nodes, sizeof(int64_t));
        for(int j=0; j < graph->region_segment_list[i]->nodes.size(); j++){
            particle->segments.list[i].node_ids[j] = graph->region_segment_list[i]->nodes[j]->id;
        }
    }
    
    particle->weight = normalized_weight;
    if(param.verbose)
        fprintf(stderr, "Weight : %f\n", normalized_weight);
}*/

void SlamParticle::getLCMRegionMessageFromGraph(slam_graph_region_particle_t *particle){
    particle->id = graph_id;
    particle->no_nodes = graph->slam_node_list.size();

    /*int current_split = 0; 
    if(current_segment.size()> 0){
        current_split = 1;
        }*/

    particle->no_regions = graph->region_node_list.size();// + current_split;
    particle->region_list = (slam_graph_region_t *) calloc(particle->no_regions, sizeof(slam_graph_region_t));
    if(param.polygonMode == 1){
    //this is for nodes 
        graph->updateBoundingBoxes();
    }
    
    //graph->calculateRegionDistance();
    int no_edges = 0;

    if(param.verbose){
        fprintf(stderr, "Particle ID: %d\n", graph_id);
    }

    particle->no_close_node_pairs = graph->no_close_node_pairs;
    particle->no_same_region_close_pairs = graph->no_same_region_close_pairs;
    particle->no_failed_close_node_pairs = graph->no_failed_close_node_pairs;
    particle->max_dist_of_close_node_pairs = graph->max_dist_of_close_node_pairs;
    particle->average_distance_of_close_node_pairs = graph->average_distance_of_close_node_pairs;

    for(int i=0; i < particle->no_regions; i++){
        //maybe the region id's should be renormalized at some point 
        RegionNode *region = graph->region_node_list[i];
        if(param.polygonMode == 1){
            region->updateBoundingBox();
        }
        else{
            region->updateRayTracedPoints();
            //exit(-1);
        }
        particle->region_list[i].id = region->region_id;//i;
        particle->region_list[i].mean_xy[0] = region->mean.x();
        particle->region_list[i].mean_xy[1] = region->mean.y();

        no_edges += region->edges.size();

        particle->region_list[i].count = region->nodes.size();
        particle->region_list[i].nodes = (slam_graph_node_t *) calloc(particle->region_list[i].count , sizeof(slam_graph_node_t));
        
        //lets find the mean and then the node closest to the mean - and that will be the representative node 
        double mean_xy[2] = {0};

        if(param.useFactorGraphs){
            particle->region_list[i].region_type_dist.count = region->region_type_dist.size();
            map<int, double>::iterator it;
            int k = 0;
            particle->region_list[i].region_type_dist.classes = (slam_probability_element_t *) calloc(particle->region_list[i].region_type_dist.count , sizeof(slam_probability_element_t));
            for(it=region->region_type_dist.begin(); it !=region->region_type_dist.end(); it++){
                particle->region_list[i].region_type_dist.classes[k].type = it->first;
                particle->region_list[i].region_type_dist.classes[k].probability = it->second;
                k++;
            }

            particle->region_list[i].region_label_dist.count = region->region_label_dist.size();

            k = 0;
            particle->region_list[i].region_label_dist.classes = (slam_probability_element_t *) calloc(particle->region_list[i].region_label_dist.count , sizeof(slam_probability_element_t));
            //fprintf(stderr, BLUE "Region ID : %d\n" RESET_COLOR, region->region_id);
            for(it=region->region_label_dist.begin(); it !=region->region_label_dist.end(); it++){
                particle->region_list[i].region_label_dist.classes[k].type = it->first;
                particle->region_list[i].region_label_dist.classes[k].probability = it->second;
                //fprintf(stderr, BLUE "\tRegion Label : [%d] -> %.3f\n" RESET_COLOR, (int) it->first, (double) it->second);
                k++;
            }

            //distributions without language - for comparison 

            particle->region_list[i].region_type_dist_no_lang.count = region->region_type_dist_no_lang.size();
            k = 0;
            particle->region_list[i].region_type_dist_no_lang.classes = (slam_probability_element_t *) calloc(particle->region_list[i].region_type_dist_no_lang.count , sizeof(slam_probability_element_t));
            for(it=region->region_type_dist_no_lang.begin(); it !=region->region_type_dist_no_lang.end(); it++){
                particle->region_list[i].region_type_dist_no_lang.classes[k].type = it->first;
                particle->region_list[i].region_type_dist_no_lang.classes[k].probability = it->second;
                k++;
            }

            particle->region_list[i].region_label_dist_no_lang.count = region->region_label_dist_no_lang.size();

            k = 0;
            particle->region_list[i].region_label_dist_no_lang.classes = (slam_probability_element_t *) calloc(particle->region_list[i].region_label_dist_no_lang.count , sizeof(slam_probability_element_t));
            //fprintf(stderr, BLUE "Region ID : %d\n" RESET_COLOR, region->region_id);
            for(it=region->region_label_dist_no_lang.begin(); it !=region->region_label_dist_no_lang.end(); it++){
                particle->region_list[i].region_label_dist_no_lang.classes[k].type = it->first;
                particle->region_list[i].region_label_dist_no_lang.classes[k].probability = it->second;
                //fprintf(stderr, BLUE "\tRegion Label : [%d] -> %.3f\n" RESET_COLOR, (int) it->first, (double) it->second);
                k++;
            }
            //without answers 
            particle->region_list[i].region_type_dist_no_answer_lang.count = region->region_type_dist_no_answers.size();
            k = 0;
            particle->region_list[i].region_type_dist_no_answer_lang.classes = (slam_probability_element_t *) calloc(particle->region_list[i].region_type_dist_no_answer_lang.count , sizeof(slam_probability_element_t));
            for(it=region->region_type_dist_no_answers.begin(); it !=region->region_type_dist_no_answers.end(); it++){
                particle->region_list[i].region_type_dist_no_answer_lang.classes[k].type = it->first;
                particle->region_list[i].region_type_dist_no_answer_lang.classes[k].probability = it->second;
                k++;
            }

            particle->region_list[i].region_label_dist_no_answer_lang.count = region->region_label_dist_no_answers.size();

            k = 0;
            particle->region_list[i].region_label_dist_no_answer_lang.classes = (slam_probability_element_t *) calloc(particle->region_list[i].region_label_dist_no_answer_lang.count , sizeof(slam_probability_element_t));
            //fprintf(stderr, BLUE "Region ID : %d\n" RESET_COLOR, region->region_id);
            for(it=region->region_label_dist_no_answers.begin(); it !=region->region_label_dist_no_answers.end(); it++){
                particle->region_list[i].region_label_dist_no_answer_lang.classes[k].type = it->first;
                particle->region_list[i].region_label_dist_no_answer_lang.classes[k].probability = it->second;
                //fprintf(stderr, BLUE "\tRegion Label : [%d] -> %.3f\n" RESET_COLOR, (int) it->first, (double) it->second);
                k++;
            }
        }
        else{
            //set ther region type dist to be null 
            particle->region_list[i].region_type_dist.count = 0;
            particle->region_list[i].region_type_dist.classes = NULL;

            //build the label dist from the dirichlet 
            map<int, double> label_dist = region->labeldist->getProbabilities();
            particle->region_list[i].region_label_dist.count = label_dist.size();

            map<int, double>::iterator it;
            int k = 0;
            particle->region_list[i].region_label_dist.classes = (slam_probability_element_t *) calloc(particle->region_list[i].region_label_dist.count , sizeof(slam_probability_element_t));
            //fprintf(stderr, BLUE "Region ID : %d\n" RESET_COLOR, region->region_id);
            for(it=label_dist.begin(); it !=label_dist.end(); it++){
                particle->region_list[i].region_label_dist.classes[k].type = it->first;
                particle->region_list[i].region_label_dist.classes[k].probability = it->second;
                //fprintf(stderr, BLUE "\tRegion Label : [%d] -> %.3f\n" RESET_COLOR, (int) it->first, (double) it->second);
                k++;
            }            
        }

        for(int j=0; j < region->nodes.size(); j++){          

            SlamNode *node = region->nodes[j];
            slam_graph_node_t *nd = &particle->region_list[i].nodes[j];
            node->updateNodeMsg(nd, param.polygonMode);            
            if(node == region->mean_node){
                particle->region_list[i].center_ind = nd->id;
            }
        }
        
        particle->no_objects = (int) graph->object_map.size();
        particle->objects = (slam_object_t *) calloc(particle->no_objects, sizeof(slam_object_t));
        map<int, SlamObject *>::iterator it_o;
        int i=0;
        for(it_o = graph->object_map.begin(); it_o != graph->object_map.end(); it_o++, i++){
            SlamObject *obj = it_o->second;
            slam_object_t *m_obj = &particle->objects[i];
            m_obj->id = obj->id;
            m_obj->type = obj->type;
            if(obj->assigned_node){
                m_obj->node_id = obj->assigned_node->id;
            }
            else{
                m_obj->node_id = -1;
            }
            
            m_obj->pos[0] = obj->pose.x();
            m_obj->pos[1] = obj->pose.y();
            m_obj->pos[2] = 0;
        }
    }

    particle->no_region_edges = no_edges; //graph->region_edge_list.size();
    particle->region_edges = (slam_graph_region_edge_t *) calloc(particle->no_region_edges, sizeof(slam_graph_region_edge_t));

    int k = 0;
    map<int, RegionEdge*>::iterator it;
    for(int i=0; i < particle->no_regions; i++){
        RegionNode *region = graph->region_node_list[i];
        for(it= region->edges.begin(); it!= region->edges.end();it++){
            particle->region_edges[k].edge_id = k;
            particle->region_edges[k].region_1_id = it->second->node1->region_id;
            particle->region_edges[k].region_2_id = it->second->node2->region_id;
            particle->region_edges[k].type = it->second->type;
            k++;
        }
    }
    
    particle->no_edges = graph->slam_constraints.size();
    particle->edge_list = (slam_graph_edge_t *) calloc(particle->no_edges, sizeof(slam_graph_edge_t));

    map<int, SlamConstraint *>::iterator c_it;
    int j= 0;
    for ( c_it= graph->slam_constraints.begin() ; c_it != graph->slam_constraints.end(); c_it++, j++ ){        
        SlamConstraint *ct = c_it->second;
        
        particle->edge_list[j].id = ct->id;
        particle->edge_list[j].node_id_1 = ct->node1->position; 
        particle->edge_list[j].node_id_2 = ct->node2->position; 
        particle->edge_list[j].actual_scanned_node_id_1 = ct->actualnode1->position;
        particle->edge_list[j].actual_scanned_node_id_2 = ct->actualnode2->position;
        particle->edge_list[j].scanmatch_hit = ct->hitpct;
        particle->edge_list[j].type = ct->type;
        particle->edge_list[j].status = ct->status;
    }

    particle->no_rejected_edges = graph->failed_slam_constraints.size();
    particle->rejected_edge_list = (slam_graph_edge_t *) calloc(particle->no_rejected_edges, sizeof(slam_graph_edge_t));

    j= 0;
    for ( c_it= graph->failed_slam_constraints.begin() ; c_it != graph->failed_slam_constraints.end(); c_it++, j++ ){        
        SlamConstraint *ct = c_it->second;
        
        particle->rejected_edge_list[j].id = ct->id;
        particle->rejected_edge_list[j].node_id_1 = ct->node1->position; 
        particle->rejected_edge_list[j].node_id_2 = ct->node2->position; 
        particle->rejected_edge_list[j].actual_scanned_node_id_1 = ct->actualnode1->position;
        particle->rejected_edge_list[j].actual_scanned_node_id_2 = ct->actualnode2->position;
        particle->rejected_edge_list[j].scanmatch_hit = ct->hitpct;
        //fprintf(stderr, YELLOW "Edge hit pct : %f\n", ct->hitpct);
        particle->rejected_edge_list[j].type = ct->type;
        particle->rejected_edge_list[j].status = ct->status;
    }

    particle->segments.no_segments = (int) graph->region_segment_list.size();
        
    particle->segments.list = (slam_graph_segment_t *) calloc(particle->segments.no_segments, sizeof(slam_graph_segment_t));

    for(int i= 0; i < graph->region_segment_list.size(); i++){
        particle->segments.list[i].id = graph->region_segment_list[i]->id;
        particle->segments.list[i].no_nodes = (int) graph->region_segment_list[i]->nodes.size();
        particle->segments.list[i].node_ids = (int64_t *) calloc(particle->segments.list[i].no_nodes, sizeof(int64_t));
        for(int j=0; j < graph->region_segment_list[i]->nodes.size(); j++){
            particle->segments.list[i].node_ids[j] = graph->region_segment_list[i]->nodes[j]->id;
        }
    }
    
    particle->weight = normalized_weight;
    if(param.verbose)
        fprintf(stderr, "Weight : %f\n", normalized_weight);
}

void SlamParticle::getLCMRegionMessageFromGraphCorrect(slam_graph_region_particle_t *particle){
    particle->id = graph_id;
    particle->no_nodes = graph->slam_node_list.size();

    /*int current_split = 0; 
    if(current_segment.size()> 0){
        current_split = 1;
        }*/

    particle->no_regions = graph->region_node_list.size();// + current_split;
    particle->region_list = (slam_graph_region_t *) calloc(particle->no_regions, sizeof(slam_graph_region_t));
    if(param.polygonMode == 1){
    //this is for nodes 
        graph->updateBoundingBoxes();
    }
    
    //graph->calculateRegionDistance();
    int no_edges = 0;

    if(param.verbose){
        fprintf(stderr, "Particle ID: %d\n", graph_id);
    }

    particle->no_close_node_pairs = graph->no_close_node_pairs;
    particle->no_same_region_close_pairs = graph->no_same_region_close_pairs;
    particle->no_failed_close_node_pairs = graph->no_failed_close_node_pairs;
    particle->max_dist_of_close_node_pairs = graph->max_dist_of_close_node_pairs;
    particle->average_distance_of_close_node_pairs = graph->average_distance_of_close_node_pairs;

    for(int i=0; i < particle->no_regions; i++){
        //maybe the region id's should be renormalized at some point 
        RegionNode *region = graph->region_node_list[i];
        if(param.polygonMode == 1){
            region->updateBoundingBox();
        }
        else{
            region->updateRayTracedPoints();
            //exit(-1);
        }
        particle->region_list[i].id = region->region_id;//i;
        particle->region_list[i].mean_xy[0] = region->mean.x();
        particle->region_list[i].mean_xy[1] = region->mean.y();

        no_edges += region->edges.size();

        particle->region_list[i].count = region->nodes.size();
        particle->region_list[i].nodes = (slam_graph_node_t *) calloc(particle->region_list[i].count , sizeof(slam_graph_node_t));
        
        //lets find the mean and then the node closest to the mean - and that will be the representative node 
        double mean_xy[2] = {0};

        if(param.useFactorGraphs){
            particle->region_list[i].region_type_dist.count = region->region_type_dist.size();
            map<int, double>::iterator it;
            int k = 0;
            particle->region_list[i].region_type_dist.classes = (slam_probability_element_t *) calloc(particle->region_list[i].region_type_dist.count , sizeof(slam_probability_element_t));
            for(it=region->region_type_dist.begin(); it !=region->region_type_dist.end(); it++){
                particle->region_list[i].region_type_dist.classes[k].type = it->first;
                particle->region_list[i].region_type_dist.classes[k].probability = it->second;
                k++;
            }

            particle->region_list[i].region_label_dist.count = region->region_label_dist.size();

            k = 0;
            particle->region_list[i].region_label_dist.classes = (slam_probability_element_t *) calloc(particle->region_list[i].region_label_dist.count , sizeof(slam_probability_element_t));
            //fprintf(stderr, BLUE "Region ID : %d\n" RESET_COLOR, region->region_id);
            for(it=region->region_label_dist.begin(); it !=region->region_label_dist.end(); it++){
                particle->region_list[i].region_label_dist.classes[k].type = it->first;
                particle->region_list[i].region_label_dist.classes[k].probability = it->second;
                //fprintf(stderr, BLUE "\tRegion Label : [%d] -> %.3f\n" RESET_COLOR, (int) it->first, (double) it->second);
                k++;
            }

            //distributions without language - for comparison 

            particle->region_list[i].region_type_dist_no_lang.count = region->region_type_dist_no_lang.size();
            k = 0;
            particle->region_list[i].region_type_dist_no_lang.classes = (slam_probability_element_t *) calloc(particle->region_list[i].region_type_dist_no_lang.count , sizeof(slam_probability_element_t));
            for(it=region->region_type_dist_no_lang.begin(); it !=region->region_type_dist_no_lang.end(); it++){
                particle->region_list[i].region_type_dist_no_lang.classes[k].type = it->first;
                particle->region_list[i].region_type_dist_no_lang.classes[k].probability = it->second;
                k++;
            }

            particle->region_list[i].region_label_dist_no_lang.count = region->region_label_dist_no_lang.size();

            k = 0;
            particle->region_list[i].region_label_dist_no_lang.classes = (slam_probability_element_t *) calloc(particle->region_list[i].region_label_dist_no_lang.count , sizeof(slam_probability_element_t));
            //fprintf(stderr, BLUE "Region ID : %d\n" RESET_COLOR, region->region_id);
            for(it=region->region_label_dist_no_lang.begin(); it !=region->region_label_dist_no_lang.end(); it++){
                particle->region_list[i].region_label_dist_no_lang.classes[k].type = it->first;
                particle->region_list[i].region_label_dist_no_lang.classes[k].probability = it->second;
                //fprintf(stderr, BLUE "\tRegion Label : [%d] -> %.3f\n" RESET_COLOR, (int) it->first, (double) it->second);
                k++;
            }

            //without answers 
            particle->region_list[i].region_type_dist_no_answer_lang.count = region->region_type_dist_no_answers.size();
            k = 0;
            particle->region_list[i].region_type_dist_no_answer_lang.classes = (slam_probability_element_t *) calloc(particle->region_list[i].region_type_dist_no_answer_lang.count , sizeof(slam_probability_element_t));
            for(it=region->region_type_dist_no_answers.begin(); it !=region->region_type_dist_no_answers.end(); it++){
                particle->region_list[i].region_type_dist_no_answer_lang.classes[k].type = it->first;
                particle->region_list[i].region_type_dist_no_answer_lang.classes[k].probability = it->second;
                k++;
            }

            particle->region_list[i].region_label_dist_no_answer_lang.count = region->region_label_dist_no_answers.size();

            k = 0;
            particle->region_list[i].region_label_dist_no_answer_lang.classes = (slam_probability_element_t *) calloc(particle->region_list[i].region_label_dist_no_answer_lang.count , sizeof(slam_probability_element_t));
            //fprintf(stderr, BLUE "Region ID : %d\n" RESET_COLOR, region->region_id);
            for(it=region->region_label_dist_no_answers.begin(); it !=region->region_label_dist_no_answers.end(); it++){
                particle->region_list[i].region_label_dist_no_answer_lang.classes[k].type = it->first;
                particle->region_list[i].region_label_dist_no_answer_lang.classes[k].probability = it->second;
                //fprintf(stderr, BLUE "\tRegion Label : [%d] -> %.3f\n" RESET_COLOR, (int) it->first, (double) it->second);
                k++;
            }
        }
        else{
            //set ther region type dist to be null 
            particle->region_list[i].region_type_dist.count = 0;
            particle->region_list[i].region_type_dist.classes = NULL;

            particle->region_list[i].region_type_dist_no_answer_lang.count = 0;
            particle->region_list[i].region_type_dist_no_answer_lang.classes = NULL;
            
            particle->region_list[i].region_label_dist_no_answer_lang.count = 0;
            particle->region_list[i].region_label_dist_no_answer_lang.classes = NULL;

            //build the label dist from the dirichlet 
            map<int, double> label_dist = region->labeldist->getProbabilities();
            particle->region_list[i].region_label_dist.count = label_dist.size();

            map<int, double>::iterator it;
            int k = 0;
            particle->region_list[i].region_label_dist.classes = (slam_probability_element_t *) calloc(particle->region_list[i].region_label_dist.count , sizeof(slam_probability_element_t));
            //fprintf(stderr, BLUE "Region ID : %d\n" RESET_COLOR, region->region_id);
            for(it=label_dist.begin(); it !=label_dist.end(); it++){
                particle->region_list[i].region_label_dist.classes[k].type = it->first;
                particle->region_list[i].region_label_dist.classes[k].probability = it->second;
                //fprintf(stderr, BLUE "\tRegion Label : [%d] -> %.3f\n" RESET_COLOR, (int) it->first, (double) it->second);
                k++;
            }            
        }

        for(int j=0; j < region->nodes.size(); j++){          

            SlamNode *node = region->nodes[j];
            slam_graph_node_t *nd = &particle->region_list[i].nodes[j];
            node->updateNodeMsg(nd, param.polygonMode);            
            if(node == region->mean_node){
                particle->region_list[i].center_ind = nd->id;
            }
        }
        
        particle->no_objects = (int) graph->object_map.size();
        particle->objects = (slam_object_t *) calloc(particle->no_objects, sizeof(slam_object_t));
        map<int, SlamObject *>::iterator it_o;
        int i=0;
        for(it_o = graph->object_map.begin(); it_o != graph->object_map.end(); it_o++, i++){
            SlamObject *obj = it_o->second;
            slam_object_t *m_obj = &particle->objects[i];
            m_obj->id = obj->id;
            m_obj->type = obj->type;
            if(obj->assigned_node){
                m_obj->node_id = obj->assigned_node->id;
            }
            else{
                m_obj->node_id = -1;
            }
            
            m_obj->pos[0] = obj->pose.x();
            m_obj->pos[1] = obj->pose.y();
            m_obj->pos[2] = 0;
        }
    }

    particle->no_region_edges = no_edges; //graph->region_edge_list.size();
    particle->region_edges = (slam_graph_region_edge_t *) calloc(particle->no_region_edges, sizeof(slam_graph_region_edge_t));

    int k = 0;
    map<int, RegionEdge*>::iterator it;
    for(int i=0; i < particle->no_regions; i++){
        RegionNode *region = graph->region_node_list[i];
        for(it= region->edges.begin(); it!= region->edges.end();it++){
            particle->region_edges[k].edge_id = k;
            particle->region_edges[k].region_1_id = it->second->node1->region_id;
            particle->region_edges[k].region_2_id = it->second->node2->region_id;
            particle->region_edges[k].type = it->second->type;
            k++;
        }
    }
    
    particle->no_edges = graph->slam_constraints.size();
    particle->edge_list = (slam_graph_edge_t *) calloc(particle->no_edges, sizeof(slam_graph_edge_t));

    map<int, SlamConstraint *>::iterator c_it;
    int j= 0;
    for ( c_it= graph->slam_constraints.begin() ; c_it != graph->slam_constraints.end(); c_it++, j++ ){        
        SlamConstraint *ct = c_it->second;
        
        particle->edge_list[j].id = ct->id;
        particle->edge_list[j].node_id_1 = ct->node1->position; 
        particle->edge_list[j].node_id_2 = ct->node2->position; 
        particle->edge_list[j].actual_scanned_node_id_1 = ct->actualnode1->position;
        particle->edge_list[j].actual_scanned_node_id_2 = ct->actualnode2->position;
        particle->edge_list[j].scanmatch_hit = ct->hitpct;
        particle->edge_list[j].type = ct->type;
        particle->edge_list[j].status = ct->status;
    }

    particle->no_rejected_edges = graph->failed_slam_constraints.size();
    particle->rejected_edge_list = (slam_graph_edge_t *) calloc(particle->no_rejected_edges, sizeof(slam_graph_edge_t));

    j= 0;
    for ( c_it= graph->failed_slam_constraints.begin() ; c_it != graph->failed_slam_constraints.end(); c_it++, j++ ){        
        SlamConstraint *ct = c_it->second;
        
        particle->rejected_edge_list[j].id = ct->id;
        particle->rejected_edge_list[j].node_id_1 = ct->node1->position; 
        particle->rejected_edge_list[j].node_id_2 = ct->node2->position; 
        particle->rejected_edge_list[j].actual_scanned_node_id_1 = ct->actualnode1->position;
        particle->rejected_edge_list[j].actual_scanned_node_id_2 = ct->actualnode2->position;
        particle->rejected_edge_list[j].scanmatch_hit = ct->hitpct;
        //fprintf(stderr, YELLOW "Edge hit pct : %f\n", ct->hitpct);
        particle->rejected_edge_list[j].type = ct->type;
        particle->rejected_edge_list[j].status = ct->status;
    }

    particle->segments.no_segments = (int) graph->region_segment_list.size();
        
    particle->segments.list = (slam_graph_segment_t *) calloc(particle->segments.no_segments, sizeof(slam_graph_segment_t));

    for(int i= 0; i < graph->region_segment_list.size(); i++){
        particle->segments.list[i].id = graph->region_segment_list[i]->id;
        particle->segments.list[i].no_nodes = (int) graph->region_segment_list[i]->nodes.size();
        particle->segments.list[i].node_ids = (int64_t *) calloc(particle->segments.list[i].no_nodes, sizeof(int64_t));
        for(int j=0; j < graph->region_segment_list[i]->nodes.size(); j++){
            particle->segments.list[i].node_ids[j] = graph->region_segment_list[i]->nodes[j]->id;
        }
    }
    
    particle->weight = normalized_weight;
    if(param.verbose)
        fprintf(stderr, "Weight : %f\n", normalized_weight);
}

void SlamParticle::fillLanguageCollection(slam_complex_language_collection_t &msg){
    msg.particle_id = graph_id;
    msg.node_count = 0;
    msg.poses = NULL;

    msg.landmark_count = 0;
    msg.landmarks = NULL;
    graph->fillLanguageCollection(msg);
} 

void SlamParticle::fillOutstandingLanguageCollection(slam_complex_language_collection_t &msg){
    //return fillLanguageCollection(msg);
    //broken
    msg.particle_id = graph_id;
    
    msg.node_count = (int) graph->slam_node_list.size();
    msg.poses = (slam_node_pose_t *) calloc(msg.node_count, sizeof(slam_node_pose_t));

    for(int i=0; i < graph->slam_node_list.size(); i++){
        SlamNode *node = graph->slam_node_list[i];
        slam_node_pose_t *pose = &msg.poses[i]; 
        Pose2d nd_pose = node->getPose();
        pose->id = node->id;
        pose->xy[0] = nd_pose.x();
        pose->xy[1] = nd_pose.y();
        pose->t = nd_pose.t();
    }
    
    //for this fill with info about the region mean node and the region 
    //might want to make this more efficient - only relavent landmarks 
    msg.landmark_count = (int) graph->region_node_list.size();
    msg.landmarks = (slam_language_region_node_t *) calloc(msg.landmark_count, sizeof(slam_language_region_node_t));
    
    map<int, double>::iterator it;

    for(int i=0; i < graph->region_node_list.size(); i++){
        RegionNode *region = graph->region_node_list[i];
        SlamNode *node = region->mean_node;
        slam_language_region_node_t *nd = &msg.landmarks[i];
        nd->id = region->region_id;
        nd->node_id = node->id;
        Pose2d pose = node->getPose();
        nd->xy[0] = pose.x();
        nd->xy[1] = pose.y();
        nd->heading = pose.t();
        
        nd->region_label_dist.count = (int) region->region_label_dist.size();
        nd->region_label_dist.classes = (slam_probability_element_t *) calloc(nd->region_label_dist.count, sizeof(slam_probability_element_t));
        int k =0;
        for(it=region->region_label_dist.begin(); it !=region->region_label_dist.end(); it++){
            nd->region_label_dist.classes[k].type = it->first;
            nd->region_label_dist.classes[k].probability = it->second;
            k++;
        }

        //add the bounding boxes 
        if(param.polygonMode == 0){
            nd->no_points = node->bounding_box_global->npoints;
            nd->x_coords = (double *) calloc(nd->no_points, sizeof(double));
            nd->y_coords = (double *) calloc(nd->no_points, sizeof(double));
            for(int j=0; j<node->bounding_box_global->npoints; j++){
                nd->x_coords[j] = node->bounding_box_global->points[j].x;
                nd->y_coords[j] = node->bounding_box_global->points[j].y;
            }                                       
        }
        else if(param.polygonMode == 1){
            if(region->ptr_global){
                nd->no_points = region->convex_ptr->npoints;
                nd->x_coords = (double *) calloc(nd->no_points, sizeof(double));
                nd->y_coords = (double *) calloc(nd->no_points, sizeof(double));
                for(int j=0; j< region->convex_ptr->npoints; j++){
                    nd->x_coords[j] = region->convex_ptr->points[j].x;
                    nd->y_coords[j] = region->convex_ptr->points[j].y;
                }
            }
            else{
                nd->no_points = node->bounding_box_global->npoints;
                nd->x_coords = (double *) calloc(nd->no_points, sizeof(double));
                nd->y_coords = (double *) calloc(nd->no_points, sizeof(double));
                for(int j=0; j<node->bounding_box_global->npoints; j++){
                    nd->x_coords[j] = node->bounding_box_global->points[j].x;
                    nd->y_coords[j] = node->bounding_box_global->points[j].y;
                }                                       
            }
        }
        else if(param.polygonMode == 2){
            pointlist2d_t *points = NULL;
            if(region->ptr_raytraced_convex_global){
                points = region->ptr_raytraced_convex_global;
                nd->no_points = points->npoints;
                nd->x_coords = (double *) calloc(nd->no_points, sizeof(double));
                nd->y_coords = (double *) calloc(nd->no_points, sizeof(double));
                for(int j=0; j< points->npoints; j++){
                    nd->x_coords[j] = points->points[j].x;
                    nd->y_coords[j] = points->points[j].y;
                }  
                pointlist2d_free(points);
                region->ptr_raytraced_convex_global = NULL;                
            }
            else{
                points = getRegionRayTrace(region);
                
                if(points){
                    nd->no_points = points->npoints;
                    nd->x_coords = (double *) calloc(nd->no_points, sizeof(double));
                    nd->y_coords = (double *) calloc(nd->no_points, sizeof(double));
                    for(int j=0; j< points->npoints; j++){
                        nd->x_coords[j] = points->points[j].x;
                        nd->y_coords[j] = points->points[j].y;
                    }  
                    pointlist2d_free(points);
                }
                else{
                    nd->no_points = 0;
                    nd->x_coords = NULL;
                    nd->y_coords = NULL;
                }
            }
        }
        else if(param.polygonMode == 3){
            pointlist2d_t *points = NULL;

            //draw a cube 
            if(region->ptr_simple_global){
                points = region->ptr_simple_global;
            
                nd->no_points = points->npoints;
                nd->x_coords = (double *) calloc(nd->no_points, sizeof(double));
                nd->y_coords = (double *) calloc(nd->no_points, sizeof(double));
                for(int j=0; j< points->npoints; j++){
                    nd->x_coords[j] = points->points[j].x;
                    nd->y_coords[j] = points->points[j].y;
                }  
            }
            else{
                nd->no_points = 0;
                nd->x_coords = NULL;
                nd->y_coords = NULL;
            }
        }
        else if(param.polygonMode == 4){
            pointlist2d_t *points = NULL;                    
                    
            if(region->ptr_raytraced_convex_global){
                points = region->ptr_raytraced_convex_global;
                //getRegionRayTrace(region);
                nd->no_points = points->npoints;
                nd->x_coords = (double *) calloc(nd->no_points, sizeof(double));
                nd->y_coords = (double *) calloc(nd->no_points, sizeof(double));
                for(int j=0; j< points->npoints; j++){
                    nd->x_coords[j] = points->points[j].x;
                    nd->y_coords[j] = points->points[j].y;
                }  
                //pointlist2d_free(points);
                //region->ptr_raytraced_convex_global = NULL;              
            }
            else{
                points = getRegionBoundingbox(region);

                if(points){
                    nd->no_points = points->npoints;
                    nd->x_coords = (double *) calloc(nd->no_points, sizeof(double));
                    nd->y_coords = (double *) calloc(nd->no_points, sizeof(double));
                    for(int j=0; j< points->npoints; j++){
                        nd->x_coords[j] = points->points[j].x;
                        nd->y_coords[j] = points->points[j].y;
                    }  
                }
                else{
                    nd->no_points = 0;
                    nd->x_coords = NULL;
                    nd->y_coords = NULL;
                }
            }
        }
        //we should try a basic one also - maybe just a box or something??
    }
    
    graph->fillOutstandingLanguageCollection(msg);
} 

string SlamParticle::getIDString(){
    char buf[200]; 
    sprintf(buf, "Particle %d : ", graph_id);
    return string(buf);
}
