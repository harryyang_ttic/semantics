#include "SlamGraph.hpp"

RegionPath::RegionPath(RegionNode *_region, NodePath _path, LabelInfo *info, int figure_id, bool use_factor_graph):ungrounded_landmarks(0){
    region = _region;
    path = _path;
    lang_obs= NULL;
    updated = false; 
    if(use_factor_graph){
        lang_obs = new LanguageObservation(info, region);//NULL;
        lang_obs->setLabelObservation(figure_id);
        lang_obs->updateRegionFactor(info, region);
    }
    //slu_datapoint = NULL;
    prob = 0;
}

RegionPath::RegionPath(const RegionPath &path, bool use_factor_graph){
    fprintf(stderr, "+++++++\n\n\nCreated\n\n\n+++++++++++");    
    exit(-1);
}

RegionPath::~RegionPath(){
    fprintf(stderr, "Removing path to region : %d\n", region->region_id);
    delete lang_obs;
    //delete slu_datapoint;
}

bool RegionPath::updatePath(NodePath _path){
    //fprintf(stderr, "Old Path size : %d - New Path size : %d\n", (int) path.size(), (int) _path.size());

    if(path == _path)
        return false;
    else{
        path = _path;
        ungrounded_landmarks = 0;
        map<RegionNode *, GroundingProb>::iterator it; 
        for(it = landmark_prob.begin(); it!= landmark_prob.end(); it++){
            ungrounded_landmarks++;
            it->second.path_prob = 0;
            it->second.evaluated = false;
        }
        updated = false; 
        return true;
    }
}

bool RegionPath::evaluateLanguageNoLandmark(SLUClassifier *slu_classifier, string sr, OutputWriter *output_writer){
    char buf[1024];
    vector<SLUDataPoint> datapoints; 
    vector<Pose> np_path = path.getPathPoses();
    sprintf(buf, "[Complex Lang] [Evaluate Path] [SREval] Figure Region [%d] Path Length : %d", (int) region->region_id, (int) np_path.size());
    output_writer->write_to_buffer(buf);

    map<RegionNode *, GroundingProb>::iterator it; 
    double lm_z[2] = {0, 2.0};
    //typedef pair<RegionNode *, GroundingProb *> gpPair;

    //vector<gpPair> eval_gp;
    vector<Point> lm_points; 
    SLUDataPoint dp(np_path, lm_points, lm_z);
    datapoints.push_back(dp);
        
    vector<double> results = slu_classifier->get_likelihood(sr, datapoints); 
    
    assert(results.size() == 1);
    
    if(results.size()>0){
        sprintf(buf, "[Complex Lang] [Evaluate Path] [SREval] Probability : %f", results[0]);
        output_writer->write_to_buffer(buf);
        updateProbability(NULL, results[0]);
        return true;
    }
    return false;
}

bool RegionPath::evaluateLanguage(SLUClassifier *slu_classifier, string sr, OutputWriter *output_writer){
    char buf[1024];
    vector<SLUDataPoint> datapoints; 
    vector<Pose> np_path = path.getPathPoses();
    sprintf(buf, "[Complex Lang] [Evaluate Path] [SREval] Figure Region [%d] Path Length : %d", (int) region->region_id, (int) np_path.size());
    output_writer->write_to_buffer(buf);

    map<RegionNode *, GroundingProb>::iterator it; 
    double lm_z[2] = {0, 2.0};
    typedef pair<RegionNode *, GroundingProb *> gpPair;

    vector<gpPair> eval_gp;
    
    for(it = landmark_prob.begin(); it != landmark_prob.end(); it++){
        if(!it->second.evaluated){
            vector<Point> lm_points = it->first->getLandmarkPoints();

            if(lm_points.size() > 0){
                SLUDataPoint dp(np_path, lm_points, lm_z);
                datapoints.push_back(dp);
                eval_gp.push_back(make_pair(it->first, &it->second)); 
            }
            else{
                sprintf(buf, "[Complex Lang] [Evaluate Path] [SREval] [Error] No Landmark Points : %d", it->first->region_id);
                output_writer->write_to_buffer(buf);
            }
        }
        else{
            sprintf(buf, "[Complex Lang] [Evaluate Path] [SREval] [Error] Landmark Evaluated : %d", it->first->region_id);
            output_writer->write_to_buffer(buf);
        }
    }

    if(landmark_prob.size() == 0){
        sprintf(buf, "[Complex Lang] [Evaluate Path] [SREval] [Error] No Landmarks found for region path");
        output_writer->write_to_buffer(buf);
    }

    if(datapoints.size() == 0){
        sprintf(buf, "[Complex Lang] [Evaluate Path] [SREval] [Error] No datapoints");
        output_writer->write_to_buffer(buf);
        return false;
    }

    vector<double> results = slu_classifier->get_likelihood(sr, datapoints); 

    if(results.size()>0){
        for(int i=0; i < eval_gp.size(); i++){
            sprintf(buf, "[Complex Lang] [Evaluate Path] [SREval] Landmark : %d - Prob : %.2f", 
                    (int) eval_gp[i].first->region_id, results[i]);
            output_writer->write_to_buffer(buf);

            eval_gp[i].second->path_prob = results[i];
            eval_gp[i].second->evaluated = true;
            ungrounded_landmarks--;
        }
        if(ungrounded_landmarks==0){
            updated = true;
        }
        marginalizeLandmarks();
        if(lang_obs){
            lang_obs->setPhi(prob);
        }
        return true;
    }

    sprintf(buf, "[Complex Lang] [Evaluate Path] [SREval] [Error] No results");
    output_writer->write_to_buffer(buf);
    return false;
}

int RegionPath::addFigureAnswer(int q_id, double _prob){
    //sr_answers.push_back(_prob);
    map<int, double>::iterator it; 

    int erased = 0;
    it = sr_answers.find(q_id); 
    if(it != sr_answers.end()){
        sr_answers.erase(it);        
        erased = 1;
    }
    sr_answers.insert(make_pair(q_id, _prob));
    return erased;
}

bool RegionPath::getFigureAnswerLikelihood(int q_id, double &_prob){
    map<int, double>::iterator it; 
    _prob = 0;
    it = sr_answers.find(q_id); 
    if(it != sr_answers.end()){
        _prob = it->second;
        return true;
    }

    return false;
}

map<int, double> RegionPath::getPathProbs(){
    map<int, double> results;
    map<RegionNode *, GroundingProb>::iterator it; 
    for(it = landmark_prob.begin(); it!=landmark_prob.end(); it++){
        if(it->second.evaluated){
            results.insert(make_pair(it->first->region_id, it->second.path_prob));
        }
    }
    return results;
}

bool RegionPath::isUpdated(){
    if(updated){
        return true;
    }
    return false; 
        //if(ungrounded_landmarks)
        //return false;
        //return true;
}

void RegionPath::updateProbability(RegionNode *l_region, double _prob){
    if(l_region == NULL && landmark_prob.size()==0){
        prob = _prob;
        if(lang_obs){
            lang_obs->setPhi(prob);
        }
    }
    else{
        map<RegionNode *, GroundingProb>::iterator it; 
        it = landmark_prob.find(l_region);
        if(it != landmark_prob.end()){
            it->second.path_prob = _prob;
            it->second.evaluated = true;
            marginalizeLandmarks();
            //might be better off picking the max pair
            //maxLandmarks();
            //set the corresponding variables
            //if we want to normalize - then we should do this in the complex language 
            if(lang_obs){
                lang_obs->setPhi(prob);
            }
            ungrounded_landmarks--;
        }
    }
}

map<int, double> RegionPath::getNormalizedLandmarkDistribution(){
    map<RegionNode *, GroundingProb>::iterator it; 

    map<int, double> result;

    double sum = 0;

    //we should normalize this 
    for(it = landmark_prob.begin(); it != landmark_prob.end(); it++){
        sum += it->second.landmark_prob; 
        result.insert(make_pair(it->first->region_id, it->second.landmark_prob));
    }

    if(sum > 0){
        map<int, double>::iterator it_r; 
        
        for(it_r = result.begin(); it_r != result.end(); it_r++){
            it_r->second = it_r->second / sum;
        }
    }
    return result;
}

void RegionPath::printProbability(){
    fprintf(stderr, "Region : %d - Probability : %f\n", region->region_id, prob);
    double sum = 0; 
    map<RegionNode *, GroundingProb>::iterator it; 
    //for(it = landmark_prob.begin(); it != landmark_prob.end(); it++){
    //  sum += it->second.landmark_prob;
    //}
    //don't normalize out the landmark 
    //if(sum > 0){
    for(it = landmark_prob.begin(); it != landmark_prob.end(); it++){
        fprintf(stderr, "\tLandmark : %d => Path : %f Lm Prob : %f\n", 
                it->first->region_id, it->second.path_prob, 
                it->second.landmark_prob);
    }
    //}
}

double RegionPath::getPathProbability(RegionNode *region){
    map<RegionNode *, GroundingProb>::iterator it; 
    it = landmark_prob.find(region);
    if(it == landmark_prob.end())
        return 0;
    return it->second.path_prob;
}

//this picks the most likely path and considers that pair only 
void RegionPath::maxLandmarks(){
    if(landmark_prob.size() == 0){
        fprintf(stderr, "No landmarks - not marginalizing\n");
        return;
    }
    
    map<RegionNode *, GroundingProb>::iterator it; 
    prob = 0;

    double max = 0;
    for(it = landmark_prob.begin(); it != landmark_prob.end(); it++){
        if(it->second.evaluated){
            double p = it->second.path_prob; // * it->second.landmark_prob;
            if(max < p)
                max = p;
        }
    }
    prob = max;
}

void RegionPath::marginalizeLandmarks(){
    if(landmark_prob.size()==0){
        fprintf(stderr, "No landmarks - not marginalizing\n");
        return;
    }

    map<RegionNode *, GroundingProb>::iterator it; 
    prob = 0;

    //added back the denom - not sure why it was taken out 
    double denom = 0;
    for(it = landmark_prob.begin(); it != landmark_prob.end(); it++){
        if(it->second.evaluated){
            prob += it->second.path_prob * it->second.landmark_prob;
            denom += it->second.landmark_prob;
        }
    }
    if(denom > 0){
        prob /= denom; 
    }
    else{        
        prob = 0;
    }  
}

void RegionPath::print(){
    fprintf(stderr, GREEN "Path to Region %d\n" RESET_COLOR, region->region_id);
    for(int i = 0; i < path.size(); i++){
        fprintf(stderr, GREEN " %d," RESET_COLOR, (int) path[i]->id);
    }
    fprintf(stderr, GREEN "\n" RESET_COLOR);
}

double RegionPath::getInitialProbability(){
    return prob;
}

double RegionPath::getProbability(){
    double result_prob = prob;
    
    map<int, double>::iterator it; 
    for(it = sr_answers.begin(); it != sr_answers.end(); it++){
        result_prob *= it->second;
    }

    return result_prob; 
}

void RegionPath::addLandmark(regionLandmark l_region){
    if(region == l_region.first)
        return;
    
    map<RegionNode *, GroundingProb>::iterator it; 
    it = landmark_prob.find(l_region.first);
    if(it == landmark_prob.end()){
        GroundingProb gp(l_region.second); 
        landmark_prob.insert(make_pair(l_region.first, gp));
        ungrounded_landmarks++;
        updated = false; 
    }
}

void RegionPath::addLandmarkAnswerResult(RegionNode *l_region, int q_id, double a_prob){
    if(region == l_region)
        return;
    
    map<RegionNode *, GroundingProb>::iterator it; 
    it = landmark_prob.find(l_region);
    if(it != landmark_prob.end()){
        it->second.addAnswerLikelihood(q_id, a_prob);
    }
}

void RegionPath::removeLandmark(RegionNode *l_region){
    map<RegionNode *, GroundingProb>::iterator it; 
    it = landmark_prob.find(l_region);
    //remove the landmark from the list 
    if(it != landmark_prob.end()){
        landmark_prob.erase(it);
        ungrounded_landmarks--;
    }
    //prob want to recalculate the grounding likelihood 
}
