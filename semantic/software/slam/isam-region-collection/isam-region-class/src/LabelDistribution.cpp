#include "SlamGraph.hpp"


// The number and labels must match slam_language_label_t.lcm and the topological graph renderer
LabelDistribution::LabelDistribution(){
    total_obs = 0;

    labels.push_back("hallway");
    saliency.push_back (0.1);
    labels.push_back("elevatorlobby");
    saliency.push_back (1);   
    labels.push_back("lobby");
    saliency.push_back (1);
    
    /*labels.push_back("courtyard");
    saliency.push_back (1);
    labels.push_back("gym");
    saliency.push_back (1);
    labels.push_back("amphitheater");
    saliency.push_back (1);
    labels.push_back("cafeteria");
    saliency.push_back (1);
    labels.push_back("entrance");
    saliency.push_back (1); //maybe less for this also */
    
    //these are labels for the indoor datasets 
     
    labels.push_back("office");
    saliency.push_back (1);
    
    labels.push_back("conferenceroom");
    saliency.push_back (1);
    labels.push_back("lab");
    saliency.push_back (1);

    num_labels = labels.size();
    
    for(int i=0; i < labels.size(); i++){
        label_map.insert(make_pair(labels[i], i));
        saliency_map.insert(make_pair(i, saliency[i]));
    }
    //observation_frequency.push_back(1);  Commented out lines are code for with an 'unspecified' label

    prior = 1.0/(num_labels);
    for(int i=0; i<num_labels; i++){ 
        observation_frequency.push_back(prior);
        total_obs+= prior;
    }

    for (int i=0; i<num_labels; i++)
        count.push_back (0);

    last_direct_obs_id = -1;      
}

void LabelDistribution::getLabelLcmMessage(slam_label_info_t *msg){
    msg->count = (int) label_map.size();
    msg->id = (int32_t *) calloc (msg->count, sizeof(int32_t));
    msg->names = (char **) calloc (msg->count, sizeof(char *));
    map< string, int>::iterator it;
    int i =0; 
    for(it = label_map.begin(); it != label_map.end(); it++){
        msg->id[i] = it->second;
        msg->names[i] = strdup(it->first.c_str());
        i++;
    } 
}

int LabelDistribution::getLabelID(string label){
    map<string, int>::iterator it; 
    it = label_map.find(label);
    if(it == label_map.end())
        return -1;
    return it->second;
}


double LabelDistribution::getBasicCosineSimilarity(){
    return num_labels * pow(prior, 2);
}


// Incorporate direct observation by incrementing specific label
void LabelDistribution::addObservation(int l, double increment, int lang_update_id) {
    if(l >=0 && l < observation_frequency.size()){
        total_obs+=increment;
        observation_frequency.at(l) = observation_frequency.at(l) + increment;
        
        count.at(l) = count.at(l) + 1;
        labeled_node_ids.push_back (lang_update_id);
    }
    else{
        fprintf(stderr, "Index outside range - please chack label\n");
    }
}

// Incorporate direct observation by incrementing specific label
void LabelDistribution::addDirectObservation(int l, double increment, int lang_update_id) {
    if(l >=0 && l < observation_frequency.size()){
        total_obs +=increment;
        observation_frequency.at(l) = observation_frequency.at(l) + increment;
    
        count.at(l) = count.at(l) + 1;
        labeled_node_ids.push_back (lang_update_id);
        last_direct_obs_id = lang_update_id;
        //direct_observations.push_back(make_pair(lang_update_id, l));
        direct_observations.insert(make_pair(lang_update_id, l));
    }
    else{
        fprintf(stderr, "Index outside range - please chack label\n");
    }
}

bool LabelDistribution::hasDirectObservation(){
    if(direct_observations.size() > 0)
        return true;
    return false;
}

//this is actually returning the means 
map<int, double> LabelDistribution::getProbabilities(){
    //fprintf(stderr, "Total Observation : %f\n", total_obs);
    double total_count = 0;
    map<int, double> label_dist;
    for(int i =0; i < num_labels; i++){
        double p = observation_frequency[i]/total_obs;
        total_count += p;
        label_dist.insert(make_pair(i, p));
    }
    //fprintf(stderr, "Total Prob : %f\n", total_count);
    return label_dist;
}

int LabelDistribution::bleedLabels (LabelDistribution *ld_src){
    map<int, int>::iterator it_src;
    map<int, int>::iterator it_dest;
    double increment = LABEL_BLEED_INCREMENT;
    int bled = 0; 
    ld_src->direct_observations;
    for(it_src = ld_src->direct_observations.begin(); it_src != ld_src->direct_observations.end(); it_src++){
        it_dest = bled_observations.find(it_src->first);       
        //this hasn't been bled to - so we should bleed this 
        if(it_dest == bled_observations.end()){
            //fprintf(stderr, RED "Bleeding Label : %d\n" RESET_COLOR, it_src->second);
            total_obs += increment;
            observation_frequency.at(it_src->second) = observation_frequency.at(it_src->second) + increment;
            bled_observations.insert(make_pair(it_src->first, it_src->second));
            bled = 1;
            labeled_node_ids.push_back(it_src->first);
        }        
    }
    
    return bled;    
}

LabelDistribution * LabelDistribution::copy(){
    LabelDistribution *ld = new LabelDistribution();
    ld->total_obs = total_obs;

    for(int i=0; i < num_labels; i++)
        ld->observation_frequency.at(i) = observation_frequency.at(i);
    
    for (int i=0; i < num_labels; i++)
        ld->count.at(i) = count.at(i);
    
    for (int i=0; i < labeled_node_ids.size(); i++)
        ld->labeled_node_ids.push_back (labeled_node_ids.at(i));

    map<int, int>::iterator it;
    for(it = bled_observations.begin(); it != bled_observations.end(); it++){
        ld->bled_observations.insert(make_pair(it->first, it->second));
    }
   
    ld->last_direct_obs_id = last_direct_obs_id;

    return ld;
}


bool LabelDistribution::areLabelsSalient () {

    double observation_frequency_total;
    // Find the most observed label
    for (int i=0; i < num_labels; i++) 
        observation_frequency_total += observation_frequency.at(i);

    for (int i=0; i < num_labels; i++) {
        if (observation_frequency.at(i) / observation_frequency_total > saliency.at(i))
            return true;
    }

    return false;
}

    

