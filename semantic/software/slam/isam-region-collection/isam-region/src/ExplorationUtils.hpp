#ifndef EXP_UTILS_H_
#define EXP_UTILS_H_

#include <occ_map/PixelMap.hpp>
#include <geom_utils/geometry.h>
#include <geom_utils/geometry_2i.h>
#include <vector>
#include <queue>
#include <map>

#define OCC_THRESH 0.8
#define UNK_THRESH 0.5

namespace ExpUtils{
    struct frontier {
        point2i_t ipt;
        double weight;
        double dist;
    };

    class FrontierElement {
    public:
        float value;
        int ixy[2];
        FrontierElement(float v, int _ixy[2]) :
            value(v)
        {
            ixy[0] = _ixy[0];
            ixy[1] = _ixy[1];
        }
    };

    class FrontierClusterElement {
    public:
        int cluster;
        int ixy[2];
        FrontierClusterElement(int _ixy[2], int c) :
            cluster(c)
        {
            ixy[0] = _ixy[0];
            ixy[1] = _ixy[1];
        }
        
        bool is_connected(const FrontierClusterElement& fc, int threshold){
            //fprintf(stderr, "Distance : %d, %d\n", (int) fabs(fc.ixy[0] - ixy[0]), (int) fabs(fc.ixy[1] - ixy[1]));
            if(fabs(fc.ixy[0] - ixy[0]) <= threshold && fabs(fc.ixy[1] - ixy[1]) <= threshold)
                return true;
            return false;
        }
    };

    class FrontierCompare {
    public:
        bool operator()(const FrontierElement & f1, const FrontierElement & f2)
        {
            return f1.value > f2.value;
        }
    };

    
    bool test_too_near_other_frontiers(std::vector<frontier> frontiers2i, int ixy[2], int pixel_length);
    int round_i(double X);
    double normalize_theta(double theta);
    double rand_d();
    double rand_d(double min, double max);
    bool is_map_free(double value);
    bool is_map_unexplored(double value);
    bool is_map_obstacle(double value);
    void geometry_project_point(int x, int y, double theta, int *x2, int *y2, occ_map::FloatPixelMap* pixmap);
    void ray_trace(occ_map::FloatPixelMap* pixmap, const point2i_t *a, double theta, double max_range,
                   double *intersection_value, double *intersection_range);
    double compute_weight_of_laser_frontier_point(int ixy[2], occ_map::FloatPixelMap* pixmap);
    occ_map::FloatPixelMap *getFrontierMap(const occ_map::FloatPixelMap * pixmap, const double start[2]);
    occ_map::FloatPixelMap *createUtilityMap(const occ_map::FloatPixelMap * pixmap, const double start[2]);
}
#endif
