#include "SlamGraph.hpp"
#include "Utils.hpp"
using namespace Utils;

//not sure what threshold to use?? 
//or maybe the likelihood of generating invalid increases with dostance 
//
#define VALID_SR_THRESHOLD 20 //20 //15 //for any relation other than near 
#define NEAR_THRESHOLD 15
#define NEAR_VALID_SR_THRESHOLD 50
#define AT_THRESHOLD 5
#define VALID_SR_LIKELIHOOD 0.05 //0.05 //used to be 0.5 
#define ADD_LANDMARK_THRESHOLD 0.35 //was 0.4 

#define SR_HACK

ComplexLanguageEvent::ComplexLanguageEvent(complex_language_event_t *event, OutputWriter *_output_writer, LabelInfo *_info, bool _use_factor_graph):
    outstanding_language(0),event_id(event->language_event_id), output_writer(_output_writer), 
    node_id( event->node_id), info(_info), use_factor_graph(_use_factor_graph){

    landmark = string(event->parse_result->landmark);
    figure = string(event->parse_result->figure);

    figure_id = info->getIndexForLabel(figure);
    utterance = string(event->parse_result->utterance);
    sr = string(event->parse_result->sr);
    complete =false;
    failed_grounding = false;
    number_of_regions_added_since_failure = 0;
    handled_internally = false;
    last_checked_id = -1;
    last_added_id = -1;
    id_at_failed_time = -1;
}

ComplexLanguageEvent::ComplexLanguageEvent(complex_language_event_t *event, OutputWriter *_output_writer, 
                                           SlamNode *node, LabelInfo *_info, bool _use_factor_graph):
    outstanding_language(0), output_writer(_output_writer), 
    event_id(event->language_event_id), info(_info), use_factor_graph(_use_factor_graph)
{
    landmark = string(event->parse_result->landmark);
    figure = string(event->parse_result->figure);
    utterance = string(event->parse_result->utterance);
    sr = string(event->parse_result->sr);
    figure_id = info->getIndexForLabel(figure);
    setNode(node);
    complete = false;
    fprintf(stderr, "Complex Event : %d\n", event_id);
    failed_grounding = false;
    number_of_regions_added_since_failure = 0;
    handled_internally = false;
}

void ComplexLanguageEvent::setNode(SlamNode *node){
    utterence_node = node;
    node_id = node->id;    
}

double ComplexLanguageEvent::getCostOfQuestion(spatialRelation sr, bool q_landmark, double expected_info_gain, SlamNode *q_nd, int last_asked_q_id_global){
    //get the cost of asking this question 
    //it is a function of the considered SR, expected info gain, and the set of questions asked (and answered already). 

    //higher the info gain - lower the cost 
    //higher cost if a question was asked about the same grounding recently (this should go down with time since last question (or nodes traversed since last question)
    //SR can also contribute to the cost - e.g. cognitive burden of the question 
    // as well as the likelihood of receiving an ivalid answer - if there is a high likelihood of receiving invalid as answer the question should cost more

    int last_q_asked_id_local = -1;
    
    if(answered_questions.size() > 0){
        last_q_asked_id_local = answered_questions[answered_questions.size() - 1]->node->id;
    }

    //int nodes_since_last_question
    int nodes_since_last_question = q_nd->id - last_q_asked_id_local; 
    
    int node_threshold = 10.0;

    //this should go down as the time since last question increases (or distance) 
    double node_cost_ratio = 1 - fmin(1.0,  nodes_since_last_question / node_threshold); 

    double node_cost_f = 8.0; 

    double info_gain_f = 5; 

    double cost = node_cost_f * node_cost_ratio - info_gain_f * expected_info_gain;  

    return cost; //
}

vector<SlamNode *> ComplexLanguageEvent::getAnsweredQuestionNodes(){
    vector<SlamNode *> question_nodes;
    for(int i=0; i < answered_questions.size(); i++){
        question_nodes.push_back(answered_questions[i]->node);
    }
    return question_nodes;
}

void ComplexLanguageEvent::printFigureDistribution(string prefix){
    vector<regionProb> figure_results = getFigureDistribution();    
    sprintf(buf, "[Complex Lang] %s Event : %d ================", prefix.c_str(), event_id);
    output_writer->write_to_buffer(buf);
    for(int i=0; i < figure_results.size(); i++){
        sprintf(buf, "[Complex Lang] %s Event : %d Figure %d => Prob : %f", prefix.c_str(), event_id, figure_results[i].first->region_id, figure_results[i].second);
        output_writer->write_to_buffer(buf);
    }
    sprintf(buf, "[Complex Lang] %s Event : %d ================", prefix.c_str(), event_id);
    output_writer->write_to_buffer(buf);
}

//this is the parent map leading up to the figure 
vector<SRQuestion *> ComplexLanguageEvent::getQuestionsToUpdate(){
    vector<SRQuestion *> to_update;
    for(int i=0; i < answered_questions.size(); i++){
        SRQuestion *answered_q = answered_questions[i];
        if(answered_q->hasQuestionPoseChanged()){
            to_update.push_back(answered_q);
        }
    }
    return to_update; 
}

void ComplexLanguageEvent::updateSRQuestionAnswer(SRQuestion *question, string answer){    
    if(question->landmark){
        sprintf(buf, "[Complex Lang] [Answers] [Update] Event : %d Question was asked about the landmark", event_id);
        output_writer->write_to_buffer(buf);
    }
    else{
        sprintf(buf, "[Complex Lang] [Answers] [Update] Event : %d Question was asked about the figure", event_id);
        output_writer->write_to_buffer(buf);
    }

    //need to be trated differently based on whether the question was about the landmark or the figure      

    //use the resulting SR information and the answer to update the likelihoods 
    if(!answer.compare("yes")){
        sprintf(buf, "[Complex Lang] [Answers] [Update] Received Yes answer (Question asked at node %d) : %s\n", question->getNodeID(), getSR(question->sr_result.sr).c_str());
        output_writer->write_to_buffer(buf);
        question->updateAnswer(ANSWER_YES);
    }
    else if(!answer.compare("no")){
        sprintf(buf, "[Complex Lang] [Answers] [Update] Received No answer (Question asked at node %d) : %s\n", question->getNodeID(), getSR(question->sr_result.sr).c_str());
        output_writer->write_to_buffer(buf);
        question->updateAnswer(ANSWER_NO);
    }
    else{
        sprintf(buf, "[Answers] [Update] Received invalid answer");
        output_writer->write_to_buffer(buf);
        question->updateAnswer(ANSWER_INVALID);
        delete question; 
        //return;
    }
    
    //the likelihoods are the region asked about and the likelihood of yes for the SR
    //push this to all the region paths 

    map<int, map<int,double> >::iterator it_answer_map;
    map<int, double>::iterator it_sr;
    map<RegionNode *, RegionPath *>::iterator it; 
    
    if(question->landmark== 0){
        sprintf(buf, "[Complex Lang] [Answers] [Update] Updating Figure likelihoods based on answers : Node paths : %d", (int) node_paths.size());
        output_writer->write_to_buffer(buf);

        //get the answer map for the received answer
        for(it = node_paths.begin(); it != node_paths.end(); it++){            
            int fig_id = it->first->region_id;
            
            it_answer_map = question->sr_result.sr_likelihood.find(fig_id);

            if(it_answer_map != question->sr_result.sr_likelihood.end()){
                map<int, double> sr_for_answer = it_answer_map->second;

                it_sr = sr_for_answer.find(question->answer);
            
                if(it_sr != sr_for_answer.end()){
                    double prob = it_sr->second;
                    
                    //add this to the region path 
                    it->second->addFigureAnswer(question->id, prob);
                                        
                    sprintf(buf, "[Complex Lang] [Answers] [Update] Figure Path : %d - Likelihood of answer : %f", fig_id, prob);
                    output_writer->write_to_buffer(buf);
                }
                else{
                    fprintf(stderr, "Error - answer map not found for answer - size of answer map : %d\n", (int) question->sr_result.sr_likelihood.size());
                }
            }            
            else{
                fprintf(stderr, "Error - answer map not found for region - size of answer map : %d\n", (int) question->sr_result.sr_likelihood.size());
            }
        }
        sprintf(buf, "[Complex Lang] [Answers] [Update] Done updating");
        output_writer->write_to_buffer(buf);
    }
    else{
        sprintf(buf, "[Complex Lang] [Answers] [Update] Updating Landmark likelihoods based on answers");
        output_writer->write_to_buffer(buf);
        //the likelihoods are the region asked about and the likelihood of yes for the SR 
        
        for(it_answer_map = question->sr_result.sr_likelihood.begin(); it_answer_map != question->sr_result.sr_likelihood.end(); it_answer_map++){
            int region_id = it_answer_map->first;
            
            map<int, double> sr_for_answer = it_answer_map->second;
                               
            it_sr = sr_for_answer.find(question->answer);
            
            if(it_sr != sr_for_answer.end()){
                double prob = it_sr->second;
                
                RegionNode *lm_node = getLandmarkNode(region_id);
                
                sprintf(buf, "[Complex Lang] [Answers] [Update] Landmark : %d - Likelihood of answer : %f", lm_node->region_id, prob);
                output_writer->write_to_buffer(buf);

                map<RegionNode *, RegionPath *>::iterator it; 
                for(it = node_paths.begin(); it != node_paths.end(); it++){
                    it->second->addLandmarkAnswerResult(lm_node, question->id, prob);
                }
            }
            else{
                fprintf(stderr, "Error - answer map not found for answer - size of answer map : %d\n", (int) question->sr_result.sr_likelihood.size());
            }
        }     
    }
    
    //push this in to the answered questions 
    answered_questions.push_back(question);
}

void ComplexLanguageEvent::reevaluateAnsweredQuestion(SLUClassifier *slu_classifier, SRQuestion *question, 
                                                      map<SlamNode*, SlamNode *> &parent_map_q){
    map<RegionNode *, RegionPath *>::iterator it; 

    spatialRelation sr = question->sr_result.sr; 
    string spatial_relation_str = getSR(sr);

    bool valid = false;

    if(!question->landmark_region){
        //get the answer map for the received answer
        for(it = node_paths.begin(); it != node_paths.end(); it++){            
            SlamNode *region_node = it->first->mean_node; 
            
            double dist = 0; 
            vector<SlamNode *> path = getPathAndLength(parent_map_q, region_node, &dist, true);         
            
            sprintf(buf, "[Complex Lang] [Question Reeval] [Reeval SR] [Distance Check] Event : %d Distance to region %d - %f Path : %d - %d", event_id, it->first->region_id, dist, (int) path[0]->id, (int) path[path.size() - 1]->id);
            output_writer->write_to_buffer(buf);

            double prob_yes = VALID_SR_LIKELIHOOD;
            double prob_no; 

            double prob_invalid = 1.0 - prob_yes; 
            
            if((sr == NEAR && dist < NEAR_VALID_SR_THRESHOLD) || dist < VALID_SR_THRESHOLD){        
                //if(dist < VALID_SR_THRESHOLD){
                //what if none of the regions are not high-likely at the given SR 
                vector<Pose> np_path = getPosesFromNodes(path); 
            
                double lm_z[2] = {0, 2.0};
                //for the question asking - we don't require landmarks - this is why the points are empty
                vector<Point> lm_points; 
                SLUDataPoint dp(np_path, lm_points, lm_z);
                vector<SLUDataPoint> datapoints;
                datapoints.push_back(dp);
                
                vector<double> results = slu_classifier->get_likelihood(spatial_relation_str, datapoints); 
                
                if(results.size() > 0){
                    prob_yes = results[0]; 
                    prob_invalid = 0.0;
#ifdef SR_HACK
                    Pose2d delta = question->node->getPose().ominus(region_node->getPose());
                    double dist_d = hypot(delta.x(), delta.y());
                    
                    if(NEAR){
                        if(dist_d < NEAR_THRESHOLD){
                            //don't set near high if things are not close topologically 
                            //prob_yes = 0.95;                        
                        }
                        else{
                            prob_yes = 0.05;
                        }
                    }
                    else if(AT){
                        if(dist_d < AT_THRESHOLD){
                            //don't set near high if things are not close topologically 
                            //prob_yes = 0.95;                        
                        }
                        else{
                            prob_yes = 0.05;
                        }
                    }
#endif
                }
                else{
                    //set the p_yes to give us no information 
                    prob_yes = 0.01; 
                }
                
                sprintf(buf, "[Complex Lang] [Question Reeval] [Eval SR] [Distance Check] Event : %d Checking SR %s - Prob : %f Valid : %f", event_id, spatial_relation_str.c_str(), prob_yes, (1 - prob_invalid));
                output_writer->write_to_buffer(buf);
            }
            else{                
                sprintf(buf, "[Complex Lang] [Question Reeval] [Reeval SR] [Distance Check] Event : %d Region too far - assuming not valid SR", event_id);
                output_writer->write_to_buffer(buf);

                prob_yes = 0.01; 
            }

            prob_no = 1 - prob_yes;    

            if(question->answer == ANSWER_YES){
                valid = true;
            }
            
            double prob_of_answer_region = 0;
            if(valid){
                prob_of_answer_region = prob_yes * (1 - prob_invalid);
            }
            else{
                prob_of_answer_region = prob_no * (1 - prob_invalid) + prob_invalid;
            }
            
            it->second->addFigureAnswer(question->id, prob_of_answer_region);
        }
    }
    else{
        for(it = node_paths.begin(); it != node_paths.end(); it++){            
            SlamNode *region_node = it->first->mean_node; 
            
            double dist = 0; 
            vector<SlamNode *> path = getPathAndLength(parent_map_q, region_node, &dist, true);         
            
            sprintf(buf, "[Complex Lang] [Question Reeval] [Reeval SR] [Distance Check] Event : %d Distance to region %d - %f Path : %d - %d", event_id, it->first->region_id, dist, (int) path[0]->id, (int) path[path.size() - 1]->id);
            output_writer->write_to_buffer(buf);

            double prob_yes = VALID_SR_LIKELIHOOD;
            double prob_no; 
            double prob_invalid = 1.0 - VALID_SR_LIKELIHOOD; 

            if((sr == NEAR && dist < NEAR_VALID_SR_THRESHOLD) || dist < VALID_SR_THRESHOLD){        
                //if(dist < VALID_SR_THRESHOLD){
                //what if none of the regions are not high-likely at the given SR 
                vector<Pose> np_path = getPosesFromNodes(path, true); 
            
                double lm_z[2] = {0, 2.0};
                //for the question asking - we don't require landmarks - this is why the points are empty
                vector<Point> lm_points; 
                SLUDataPoint dp(np_path, lm_points, lm_z);
                vector<SLUDataPoint> datapoints;
                datapoints.push_back(dp);
            
                vector<double> results = slu_classifier->get_likelihood(spatial_relation_str, datapoints); 
            
                if(results.size() > 0){
                    prob_yes = results[0]; 
                    prob_invalid = 0.0;
#ifdef SR_HACK
                    //for the landmark questions, we should not calculate this from the robot - but from the landmark
                    //Pose2d delta = question->node->getPose().ominus(region_node->getPose());
                    double dist_d = 0; //hypot(delta.x(), delta.y());
                    if(NEAR){
                        if(dist_d < NEAR_THRESHOLD){
                            //don't set near high if things are not close topologically 
                            //prob_yes = 0.95;                        
                        }
                        else{
                            prob_yes = 0.05;
                        }
                    }
                    else if(AT){
                        if(dist_d < AT_THRESHOLD){
                            //don't set near high if things are not close topologically 
                            //prob_yes = 0.95;                        
                        }
                        else{
                            prob_yes = 0.05;
                        }
                    }
#endif
                }
                else{
                    //set the p_yes to give us no information 
                    prob_yes = 0.01; 
                }
                
                sprintf(buf, "[Complex Lang] [Question Reeval] [Eval SR] [Distance Check] Event : %d Checking SR %s - Prob : %f Valid: %f", event_id, spatial_relation_str.c_str(), prob_yes, (1 - prob_invalid));
                output_writer->write_to_buffer(buf);

            }
            else{
                sprintf(buf, "[Complex Lang] [Question Reeval] [Eval SR] [Distance Check] Event : %d Region too far - assuming not valid SR", event_id);
                output_writer->write_to_buffer(buf);

                prob_yes = 0.01; 
            }

            prob_no = 1 - prob_yes;    

            if(question->answer == ANSWER_YES){
                valid = true;
            }
            
            double prob_of_answer_region = 0;
            if(valid){
                prob_of_answer_region = prob_yes * (1 - prob_invalid);
            }
            else{
                prob_of_answer_region = prob_no * (1 - prob_invalid) + prob_invalid;
            }
            
            it->second->addFigureAnswer(question->id, prob_of_answer_region);            
        }        
    }

    question->updateQuestionPose(); 
}

//this is the parent map leading up to the figure 
void ComplexLanguageEvent::evaluateAnsweredQuestions(SLUClassifier *slu_classifier, RegionNode *figure_node, map<SlamNode*, SlamNode *> &parent_map_fig){
    RegionPath *rp = getRegionPath(figure_node);
    if(rp == NULL){
        sprintf(buf, "[Complex Lang] [Question] [Reeval SR] [Error] Region Path Not found : %d", figure_node->region_id);
        output_writer->write_to_buffer(buf);
        return;
    }
    
    for(int i=0; i < answered_questions.size(); i++){
        SRQuestion *answered_q = answered_questions[i];
        //first evaluate the likelihood of this SR 
        spatialRelation q_sr = answered_q->sr_result.sr;                        
        string spatial_relation_str = getSR(q_sr);

        //question was asked w.r.t. to the robot 
        if(!answered_q->landmark_region){
            if(answered_q->landmark){
                sprintf(buf, "[Complex Lang] [Question] [Reeval SR] Question (%s) was asked regarding landmark - not handled yet", spatial_relation_str.c_str());
                output_writer->write_to_buffer(buf);
                continue;
            }
            //we need shortest paths from the location in which the question was asked - to these region nodes 

            SlamNode *q_node = answered_q->node;
            
            sprintf(buf, "[Complex Lang] [Question] [Reeval SR]  Spatial Relation : %s - Figure Region : %d (nd : %d)- Asked at node %d", spatial_relation_str.c_str(), figure_node->region_id, 
                    (int) figure_node->mean_node->id, (int) q_node->id);
            output_writer->write_to_buffer(buf);
        
            //path from the q_node to the figure node 
            //will have to reverse the path 
            double dist = 100;
            //path should not be reversed
            vector<SlamNode *> path = getPathAndLength(parent_map_fig, q_node, &dist, false);

            sprintf(buf, "[Complex Lang] [Question] [Reeval SR]  Spatial Relation : %s - Figure Region : %d (%d) - Asked at node %d - Path %d - %d (%f)", 
                    spatial_relation_str.c_str(), figure_node->region_id, (int) figure_node->mean_node->id, (int) q_node->id, (int) path[0]->id, (int) path[path.size() - 1]->id, dist);
            output_writer->write_to_buffer(buf);

            double prob_yes = VALID_SR_LIKELIHOOD; 
            double prob_invalid = 1.0 - VALID_SR_LIKELIHOOD; 
            //not sure if this is helping or not 
            if((q_sr == NEAR && dist < NEAR_VALID_SR_THRESHOLD) || dist < VALID_SR_THRESHOLD){        
                //if(dist < VALID_SR_THRESHOLD){
                //what if none of the regions are not high-likely at the given SR 
                vector<Pose> np_path = getPosesFromNodes(path);             
                       
                double lm_z[2] = {0, 2.0};
                //for the question asking - we don't require landmarks - this is why the points are empty
                vector<Point> lm_points; 
                SLUDataPoint dp(np_path, lm_points, lm_z);
                vector<SLUDataPoint> datapoints;
                datapoints.push_back(dp);
            
                vector<double> results = slu_classifier->get_likelihood(spatial_relation_str, datapoints); 
            
                sprintf(buf, "[Complex Lang] [Question] [Eval SR] [Distance Check] Checking SR");
                output_writer->write_to_buffer(buf);

                if(results.size() > 0){
                    prob_yes = results[0]; 
                    prob_invalid = 0.0;
#ifdef SR_HACK
                    Pose2d delta = q_node->getPose().ominus(figure_node->mean_node->getPose());
                    double dist_d = hypot(delta.x(), delta.y());
                    if(NEAR){
                        if(dist_d < NEAR_THRESHOLD){
                            //don't set near high if things are not close topologically 
                            //prob_yes = 0.95;                        
                        }
                        else{
                            prob_yes = 0.05;
                        }
                    }
                    else if(AT){
                        if(dist_d < AT_THRESHOLD){
                            //don't set near high if things are not close topologically 
                            //prob_yes = 0.95;                        
                        }
                        else{
                            prob_yes = 0.05;
                        }
                    }
#endif
                }
            }

            double prob_no = 1 - prob_yes;
            //we need the shortest path from this region to the question locations 
            if(answered_q->answer == ANSWER_YES){
                rp->addFigureAnswer(answered_q->id, prob_yes * (1 - prob_invalid));

                sprintf(buf, "[Complex Lang] [Question] [Reeval SR]  Figure Region : %d - Heard Yes answer -. Likelihood : %f - Updating", figure_node->region_id, prob_yes * (1 - prob_invalid));
                output_writer->write_to_buffer(buf);
            }
            else if(answered_q->answer == ANSWER_NO){
                rp->addFigureAnswer(answered_q->id, prob_no * (1 - prob_invalid));

                sprintf(buf, "[Complex Lang] [Question] [Reeval SR]  Figure Region : %d - Heard No answer -. Likelihood : %f - Updating", figure_node->region_id, prob_no * (1 - prob_invalid));
                output_writer->write_to_buffer(buf);
            
            }
        }
        else{
            //the paths should be extracted going from the landmark (for the question) and the figure 
            //also using the path orientation 
            if(answered_q->landmark){
                sprintf(buf, "[Complex Lang] [Question] [Reeval SR LM] Question (%s) was asked regarding landmark - not handled yet", spatial_relation_str.c_str());
                output_writer->write_to_buffer(buf);
                continue;
            }
            //we need shortest paths from the location in which the question was asked - to these region nodes 

            SlamNode *q_node = answered_q->landmark_region->mean_node; //answered_q->node;
            
            sprintf(buf, "[Complex Lang] [Question] [Reeval SR LM]  Spatial Relation : %s - Figure Region : %d (nd : %d)- Asked at node %d", spatial_relation_str.c_str(), figure_node->region_id, 
                    (int) figure_node->mean_node->id, (int) q_node->id);
            output_writer->write_to_buffer(buf);
        
            //path from the q_node to the figure node 
            //will have to reverse the path 
            double dist = 100;
            //path should not be reversed
            vector<SlamNode *> path = getPathAndLength(parent_map_fig, q_node, &dist, true);

            sprintf(buf, "[Complex Lang] [Question] [Reeval SR LM]  Spatial Relation : %s - Figure Region : %d (%d) - Asked at node %d - Path %d - %d (%f)", 
                    spatial_relation_str.c_str(), figure_node->region_id, (int) figure_node->mean_node->id, (int) q_node->id, (int) path[0]->id, (int) path[path.size() - 1]->id, dist);
            output_writer->write_to_buffer(buf);

            double prob_yes = VALID_SR_LIKELIHOOD; //0.5;
            double prob_invalid = 1.0 - VALID_SR_LIKELIHOOD; 
            //not sure if this is helping or not 
            if((q_sr == NEAR && dist < NEAR_VALID_SR_THRESHOLD) || dist < VALID_SR_THRESHOLD){        
                //if(dist < VALID_SR_THRESHOLD){
                vector<Pose> np_path = getPosesFromNodes(path, true);             
                       
                double lm_z[2] = {0, 2.0};
                //for the question asking - we don't require landmarks - this is why the points are empty
                vector<Point> lm_points; 
                SLUDataPoint dp(np_path, lm_points, lm_z);
                vector<SLUDataPoint> datapoints;
                datapoints.push_back(dp);
            
                vector<double> results = slu_classifier->get_likelihood(spatial_relation_str, datapoints); 
            
                sprintf(buf, "[Complex Lang] [Question] [Eval SR LM] [Distance Check] Checking SR");
                output_writer->write_to_buffer(buf);

                if(results.size() > 0){
                    prob_yes = results[0]; 
                    prob_invalid = 0.0;
#ifdef SR_HACK
                    Pose2d delta = q_node->getPose().ominus(figure_node->mean_node->getPose());
                    double dist_d = hypot(delta.x(), delta.y());
                    if(NEAR){
                        if(dist_d < NEAR_THRESHOLD){
                            //don't set near high if things are not close topologically 
                            //prob_yes = 0.95;                        
                        }
                        else{
                            prob_yes = 0.05;
                        }
                    }
                    else if(AT){
                        if(dist_d < AT_THRESHOLD){
                            //don't set near high if things are not close topologically 
                            //prob_yes = 0.95;                        
                        }
                        else{
                            prob_yes = 0.05;
                        }
                    }
#endif
                }
            }

            double prob_no = 1 - prob_yes;
            //we need the shortest path from this region to the question locations 
            if(answered_q->answer == ANSWER_YES){
                rp->addFigureAnswer(answered_q->id, prob_yes * (1 - prob_invalid));

                sprintf(buf, "[Complex Lang] [Question] [Reeval SR LM]  Figure Region : %d - Heard Yes answer -. Likelihood : %f - Updating", figure_node->region_id, prob_yes * (1 - prob_invalid));
                output_writer->write_to_buffer(buf);
            }
            else if(answered_q->answer == ANSWER_NO){
                rp->addFigureAnswer(answered_q->id, prob_no * (1 - prob_invalid));

                sprintf(buf, "[Complex Lang] [Question] [Reeval SR LM]  Figure Region : %d - Heard No answer -. Likelihood : %f - Updating", figure_node->region_id, prob_no * (1 - prob_invalid));
                output_writer->write_to_buffer(buf);
            
            }
        }
    }

    sprintf(buf, "[Complex Lang] [Evaluate Path] Evaluated SRs - Landmark Size : %d", (int) landmark.size());
    output_writer->write_to_buffer(buf);
}

void ComplexLanguageEvent::updateLastCheckedGrounding(int64_t id){
    last_checked_id = id;
}

void ComplexLanguageEvent::updateFailedGrounding(){
    id_at_failed_time = last_checked_id;
}

int ComplexLanguageEvent::getIDsAddedSinceFailure(){
    return fmax(0, last_checked_id - id_at_failed_time); 
}

int ComplexLanguageEvent::getOutstandingCount(){
    //return outstanding_language;
    int outstanding_count = 0;
    map<RegionNode *, RegionPath*>::iterator it; 

    for(it = node_paths.begin(); it != node_paths.end(); it++){
        //fprintf(stderr, "[%d] Landmarks for region path : %d\n", event_id, (int) it->second->landmark_prob.size());
        if(!it->second->isUpdated())
            outstanding_count++;
    }
    
    fprintf(stderr, "Region Path count : %d / %d\n", outstanding_count, (int) node_paths.size());
    return outstanding_count; 
}

int ComplexLanguageEvent::getNoPaths(){
    return node_paths.size();
}

bool ComplexLanguageEvent::evaluateOutstandingPaths(SLUClassifier *slu_classifier){
    //cout << "Landmark Size : " << landmark.size() << endl;
    
    bool evaluated = false;

    bool no_landmark = false;
    if(landmark.size()==0){
        no_landmark = true;
    }

    vector<RegionPath *> reeval_regions;
    

    if(!no_landmark){
        map<RegionNode *, RegionPath *>::iterator it;
        
        for(it = node_paths.begin(); it != node_paths.end(); it++){
            RegionPath *r_path = it->second;      
            
            if(!r_path->isUpdated()){
                printf(buf, "[Complex Language] [Eval SR] [SLU] Event : %d Outstanding Path : %d", event_id, r_path->region->region_id);
                output_writer->write_to_buffer(buf);
                r_path->evaluateLanguage(slu_classifier, sr, output_writer);
                reeval_regions.push_back(r_path);
                evaluated = true;
            }
        }    
    }
    else{
        map<RegionNode *, RegionPath *>::iterator it;
        //we might have to treat these differently - since there is no valid landmarks for these spatial relations
        for(it = node_paths.begin(); it != node_paths.end(); it++){
            RegionPath *r_path = it->second;      
            if(!r_path->isUpdated()){
                r_path->evaluateLanguageNoLandmark(slu_classifier, sr, output_writer);
                reeval_regions.push_back(r_path);
                evaluated = true;
            }
        }    
    }
    
    return evaluated;
}

vector<regionPathProb> ComplexLanguageEvent::getNormalizePaths(int max_size){
    vector<regionPathProb> result;
    //we can normalize this if we need to 
    map<RegionNode *, RegionPath *>::iterator it;
 
    for(it = node_paths.begin(); it != node_paths.end(); it++){
        double prob = it->second->getProbability();
        output_writer->write_to_buffer(buf);
        result.push_back(make_pair(it->second, prob));
    }    
    
    if(result.size() == 0)
        return result;

    sort(result.begin(), result.end(), compareRegionPathProbs);

    //get top N 
    if(max_size >=0){
        int size = fmin(max_size, result.size());        
        result.resize(size);
    }

    double sum = 0;
    vector<regionPathProb>::iterator it_v;
    for(it_v = result.begin(); it_v != result.end(); it_v++){
        sum += it_v->second;
    }

    sprintf(buf, "[Complex Lang] [Normalized Paths] Event : %d Normalized Figure Paths", event_id);
    output_writer->write_to_buffer(buf);

    for(it_v = result.begin(); it_v != result.end(); it_v++){
        if(sum > 0){
            it_v->second = it_v->second / sum;
        }
        sprintf(buf, "[Complex Lang] [Normalized Paths] Event : %d Figure : %d => Prob : %f", 
                event_id, it_v->first->region->region_id, it_v->second);
        output_writer->write_to_buffer(buf);
    }

    return result;
}

vector<regionPathProb> ComplexLanguageEvent::getNormalizePathsNoAnswers(int max_size){
    vector<regionPathProb> result;
    //we can normalize this if we need to 
    map<RegionNode *, RegionPath *>::iterator it;
 
    for(it = node_paths.begin(); it != node_paths.end(); it++){
        double prob = it->second->getInitialProbability();
        output_writer->write_to_buffer(buf);
        result.push_back(make_pair(it->second, prob));
    }    
    
    if(result.size() == 0)
        return result;

    sort(result.begin(), result.end(), compareRegionPathProbs);

    //get top N 
    if(max_size >=0){
        int size = fmin(max_size, result.size());        
        result.resize(size);
    }

    double sum = 0;
    vector<regionPathProb>::iterator it_v;
    for(it_v = result.begin(); it_v != result.end(); it_v++){
        sum += it_v->second;
    }

    sprintf(buf, "[Complex Lang] [Normalized Paths] Event : %d Normalized Figure Paths", event_id);
    output_writer->write_to_buffer(buf);

    for(it_v = result.begin(); it_v != result.end(); it_v++){
        if(sum > 0){
            it_v->second = it_v->second / sum;
        }
        sprintf(buf, "[Complex Lang] [Normalized Paths] Event : %d Figure : %d => Prob : %f", 
                event_id, it_v->first->region->region_id, it_v->second);
        output_writer->write_to_buffer(buf);
    }

    return result;
}

//double ComplexLanguageEvent::

vector<regionProb> ComplexLanguageEvent::getLandmarkDistribution(){
    vector<regionProb> landmark_prob; 

    sprintf(buf, "[Complex Lang] [Landmark Distribution] No landmarks : %d", (int) valid_landmarks.size());
    output_writer->write_to_buffer(buf);
    
    //margialize the path likelihoods for each landmark 
    map<int, regionLandmark>::iterator it_l; 

    double sum = 0;

    //this calculates the landmark distribution by marginalizing the 
    //groundings for each landmark - which is not the same as likelihoods 
    //we calculate aprior - not sure which one should be used 

    /*
      ToDo - this is probably broken now 
     */    

    for(it_l = valid_landmarks.begin(); it_l != valid_landmarks.end(); it_l++){   
        //for each region path - get the path likelihood 
        RegionNode *lm_nd = it_l->second.first;
        map<RegionNode *, RegionPath *>::iterator it;
        double p_prob = 0;
        for(it = node_paths.begin(); it != node_paths.end(); it++){
            p_prob += it->second->getPathProbability(lm_nd);
        }
        landmark_prob.push_back(make_pair(lm_nd, p_prob));
        sum += p_prob;
    }

    sort(landmark_prob.begin(), landmark_prob.end(), compareRegionProb);


    for(int i=0; i < landmark_prob.size(); i++){    
        //should we normalize 
        if(sum > 0)
            landmark_prob[i].second /= sum;
        
        sprintf(buf, "[Complex Lang] [Landmark Distribution] Landmark : %d - Probability : %f", landmark_prob[i].first->region_id, landmark_prob[i].second);
        output_writer->write_to_buffer(buf);
    }

    return landmark_prob;
}
  
bool ComplexLanguageEvent::addNodePath(SlamNode *node, NodePath path){
    //these are the figure node and the path to the figure from the location of the utterence
    //we should do this for region and not node (since mean nodes can change - and then paths won't get flushed 
    map<RegionNode *, RegionPath *>::iterator it; 

    if(node->region_node == utterence_node->region_node){
        sprintf(buf, "[Complex Lang] [Add Node Path] Event : %d Figure is same region as utterance : %d - skipping", event_id, node->region_node->region_id);
        output_writer->write_to_buffer(buf);
        return false;
    }

    it = node_paths.find(node->region_node);
    
    if(it != node_paths.end()){
        //remove 
        bool new_path = it->second->updatePath(path);
        if(new_path){
            sprintf(buf, "[Complex Lang] [Add Node Path] Event : %d Updated Path to region : %d", event_id, node->region_node->region_id);
            output_writer->write_to_buffer(buf);
        }
        else{
            sprintf(buf, "[Complex Lang] [Add Node Path] Event : %d Not updating Path to region : %d", event_id, node->region_node->region_id);
            output_writer->write_to_buffer(buf);
        }
        return new_path;
        //fprintf(stderr, "Already have path - checking\n");
    }
    else{
        //the region path should contain how likeli the figure node is to be the given label 
        //if we wish to normalize the likelihoods over the figures, then it needs to happen at the 
        //complex language level 
        RegionPath *r_path = new RegionPath(node->region_node, path, info, figure_id, use_factor_graph);
        
        number_of_regions_added_since_failure++;
        
        //add the landmarks for this path 
        map<int, regionLandmark>::iterator it_l; 
        for(it_l = valid_landmarks.begin(); it_l != valid_landmarks.end(); it_l++){   
            r_path->addLandmark(it_l->second);
        }        

        node_paths.insert(make_pair(node->region_node, r_path));
        sprintf(buf, "[Complex Lang] [Add Node Path] Event : %d Adding Path to region : %d", event_id, node->region_node->region_id);
        output_writer->write_to_buffer(buf);
        
        last_added_id = last_checked_id;

        return true;
    }    
}

//shouldn't be getting called anymore 
bool ComplexLanguageEvent::evaluateRegionPaths(bool ground_only_if_high, vector<RegionNode *> &grounded_regions, double ground_threshold, int max_count){
    spatialRelation srType = getSpatialRelationType(sr);

    //fprintf(stderr, "Evaluating the Spatial relation likelihood\n");
    sprintf(buf, "[Complex Lang] [Eval Paths] Evaluating the Spatial relation likelihood");
    output_writer->write_to_buffer(buf);

    sprintf(buf, "[Complex Lang] [Eval Paths] Spatial Relation : %s -> %s", sr.c_str(), getSR(srType).c_str());            
    output_writer->write_to_buffer(buf);
    
    //double ground_threshold = 0.2;

    if(isSRHandled(srType)){
        fprintf(stderr, "\tInternally Handled Spatial Relation\n");
        handled_internally = true;

        if(node_paths.size() == 0){
            failed_grounding = true;
            updateFailedGrounding();
            number_of_regions_added_since_failure = 0;            
            return false; 
        }
        
        map<RegionNode *, RegionPath *>::iterator it;

        it = node_paths.begin();
        assert(it != node_paths.end());

        RegionNode *fig = it->first;

        int landmark_id = -1; 
        int figure_id = -1;

        if(use_factor_graph){
            landmark_id = info->getIndexForLabel(landmark);
            figure_id = info->getIndexForLabel(figure);
        }
        else{
            landmark_id = fig->labeldist->getLabelID(landmark);
            figure_id = fig->labeldist->getLabelID(figure);
        }

        fprintf(stderr, "Landmark : %s -> landmark id : %d\n", landmark.c_str(), landmark_id);

        bool use_robot = true;
        
        if(landmark_id >= 0){
            use_robot = false;
        }

        Pose2d robot_pose = utterence_node->getPose();        

        if(ground_only_if_high){
            double max_prob = 0; 
                        
            if(!use_robot){
                map<RegionPath *, map<RegionNode *, double> > grounding_result; 
                
                fprintf(stderr, "====== Checking SR from landmark\n");

                for(it = node_paths.begin(); it != node_paths.end(); it++){
                    RegionPath *r_path = it->second;
            
                    map<RegionNode *, double> region_path_groundings;

                    double prob = 0;

                    Pose2d fig_pose = it->first->mean_node->getPose();

                    //this has a valid landmark attached - don't complete this unless we have landmarks for this path 
                    if(valid_landmarks.size() == 0){
                        fprintf(stderr, "Valid landmark - but no landmarks found yet - waiting for landmarks\n");
                    }
                    else{
                        fprintf(stderr, "Have valid landmarks - grounding now\n");

                        map<int, regionLandmark>::iterator it_l; 
                        for(it_l = valid_landmarks.begin(); it_l != valid_landmarks.end(); it_l++){ 
                            //would this have valid landmarks??
                            RegionNode *lm = it_l->second.first;
                            prob = getSRLikelihoodGeneral(srType, robot_pose, fig_pose, lm->mean_node->getPose(), use_robot);
                        
                            region_path_groundings.insert(make_pair(lm, prob));
                            if(max_prob < prob){
                                max_prob = prob;
                            }
                            fprintf(stderr, "\tFigure region : %d => Prob of grounding : %f\n", it->first->region_id, prob);              
                        }
                        grounding_result.insert(make_pair(r_path, region_path_groundings)); 
                    }               
                }

                if(max_prob > 0.2){
                    fprintf(stderr, "Grounding high enough - Grounding now : %d\n", (int) grounding_result.size());
                    
                    map<RegionPath *, map<RegionNode *, double> >::iterator it_fig;

                    //sort this and pick the top N 
                    
                    vector<regionProb> figure_grounding;

                    for(it_fig = grounding_result.begin(); it_fig != grounding_result.end(); it_fig++){
                        //it = node_paths.find(it_fig->first);
                        fprintf(stderr, "Grounding Region Path : %d\n", it_fig->first->region->region_id);
                        //if(it == node_paths.end())
                        //   continue;

                        RegionPath *r_path = it_fig->first;                        
                        RegionNode *figure_region = r_path->region;                        
                        
                        if(use_factor_graph){
                            fprintf(stderr, "Using Factor graph\n");

                            map<RegionNode *, double> landmark_grounding = it_fig->second;
                            map<RegionNode *, double>::iterator lm_iter;
                            for(lm_iter = landmark_grounding.begin(); lm_iter != landmark_grounding.end(); lm_iter++){
                                double prob = lm_iter->second;
                                RegionNode *lm = lm_iter->first;
                                r_path->updateProbability(lm, prob);                                
                            }
                        }
                        else{
                            map<int, double> landmark_likelihoods = getLandmarkLikelihoodsForFigure(figure_region);

                            double ground_prob = 0;
                            map<RegionNode *, double> landmark_grounding = it_fig->second;
                            map<RegionNode *, double>::iterator lm_iter;
                            map<int, double>::iterator lm_like_it;

                            fprintf(stderr, "Landmarks : %d\n", (int) landmark_grounding.size());

                            for(lm_iter = landmark_grounding.begin(); lm_iter != landmark_grounding.end(); lm_iter++){
                                double prob = lm_iter->second;
                                RegionNode *lm = lm_iter->first;

                                lm_like_it = landmark_likelihoods.find(lm->region_id);
                                if(lm_like_it == landmark_likelihoods.end())
                                    continue;

                                ground_prob += prob * lm_like_it->second;

                                fprintf(stderr, " -> LM : %d -> %f * %f\n", lm->region_id, prob, lm_like_it->second);
                            }

                            figure_grounding.push_back(make_pair(figure_region, ground_prob)); 
                        }
                    }

                    sort(figure_grounding.begin(), figure_grounding.end(), compareRegionProb);

                    for(int i=0; i < figure_grounding.size(); i++){
                        RegionNode *figure_region = figure_grounding[i].first;
                        double ground_prob = figure_grounding[i].second;
                         
                        if(i > max_count){
                            break;
                        }

                        //we should do the top N here also 
                        if(ground_prob > ground_threshold){
                            figure_region->labeldist->addObservation(figure_id, ground_prob, 1000 + event_id);
                            grounded_regions.push_back(figure_region);
                        }
                        else{
                            break;
                        }

                        fprintf(stderr, "Adding likelihood to region : %d -> %f\n", figure_region->region_id, ground_prob);
                    }
                        
                    complete = true;  
                }
                else{
                    failed_grounding = true;
                    updateFailedGrounding();
                    number_of_regions_added_since_failure = 0;
                }
            }
            else{
                map<RegionNode *, double> grounding_result; 
                vector<regionProb> figure_grounding;

                for(it = node_paths.begin(); it != node_paths.end(); it++){
                    RegionPath *r_path = it->second;
            
                    Pose2d fig_pose = it->first->mean_node->getPose();

                    Pose2d lm_pose(0,0,0);
                    //some of these will not have landmarks 
                    double prob = getSRLikelihoodGeneral(srType, robot_pose, fig_pose, lm_pose, use_robot);

                    grounding_result.insert(make_pair(r_path->region, prob));
                    
                    if(max_prob < prob){
                        max_prob = prob;
                    }
                    
                    fprintf(stderr, "\tFigure region : %d => Prob of grounding : %f\n", it->first->region_id, prob);              
                }
                if(max_prob > 0.2){
                    fprintf(stderr, "Grounding high enough - Grounding now\n");
                    
                    map<RegionNode *, double>::iterator it_fig;

                    for(it_fig = grounding_result.begin(); it_fig != grounding_result.end(); it_fig++){
                        it = node_paths.find(it_fig->first);

                        if(it == node_paths.end())
                            continue;

                        RegionNode *figure_region = it->first;
                        RegionPath *r_path = it->second;                        
                        
                        double prob = it_fig->second;

                        if(use_factor_graph){                            
                            r_path->updateProbability(NULL, prob);
                        }
                        else{
                            if(prob > ground_threshold){
                                figure_grounding.push_back(make_pair(figure_region, prob)); 
                                //figure_region->labeldist->addObservation(figure_id, prob, 1000 + event_id);
                                //grounded_regions.push_back(figure_region);
                            }
                        }                            
                    }                    
                    complete = true;  
                    sort(figure_grounding.begin(), figure_grounding.end(), compareRegionProb);
                    for(int i=0; i < figure_grounding.size(); i++){
                        RegionNode *figure_region = figure_grounding[i].first;
                        double ground_prob = figure_grounding[i].second;
                         
                        if(i > max_count){
                            break;
                        }

                        //we should do the top N here also 
                        if(ground_prob > ground_threshold){
                            figure_region->labeldist->addObservation(figure_id, ground_prob, 1000 + event_id);
                            grounded_regions.push_back(figure_region);
                        }
                        else{
                            break;
                        }
                        
                        fprintf(stderr, "Adding likelihood to region : %d -> %f\n", figure_region->region_id, ground_prob);
                    }
                     
                }
                else{
                    failed_grounding = true;
                    number_of_regions_added_since_failure = 0;
                }
            }
            
        }
        else{
            for(it = node_paths.begin(); it != node_paths.end(); it++){
                RegionPath *r_path = it->second;
            
                double prob = 0;

                Pose2d fig_pose = it->first->mean_node->getPose();

                //this has a valid landmark attached - don't complete this unless we have landmarks for this path 
                if(!use_robot){
                    if(valid_landmarks.size() == 0){
                        fprintf(stderr, "Valid landmark - but no landmarks found yet - waiting for landmarks\n");
                    }
                    else{
                        fprintf(stderr, "Have valid landmarks - grounding now\n");

                        map<int, regionLandmark>::iterator it_l; 
                        for(it_l = valid_landmarks.begin(); it_l != valid_landmarks.end(); it_l++){ 
                            //would this have valid landmarks??
                            RegionNode *lm = it_l->second.first;
                            prob = getSRLikelihoodGeneral(srType, robot_pose, fig_pose, lm->mean_node->getPose(), use_robot);
                        
                            fprintf(stderr, "\tProb : %f\n", prob);
                            r_path->updateProbability(lm, prob);
                            
                            fprintf(stderr, "\tFigure region : %d => Prob of grounding : %f\n", it->first->region_id, prob);              
                        }   
                    }
                }
                else{                
                    Pose2d lm_pose(0,0,0);
                    //some of these will not have landmarks 
                    double prob = getSRLikelihoodGeneral(srType, robot_pose, fig_pose, lm_pose, use_robot);
                    
                    r_path->updateProbability(NULL, prob);
                    fprintf(stderr, "\tFigure region : %d => Prob of grounding : %f\n", it->first->region_id, prob);              
                }
            }
        }
    }
    else{
        fprintf(stderr, "Spatial relation not handled\n");
        if(node_paths.size() == 0){
            return false; 
        }
    }
}

vector<RegionPath *> ComplexLanguageEvent::getRegionPaths(){
    vector<RegionPath *> region_paths; 
    map<RegionNode *, RegionPath *>::iterator it;
    for(it = node_paths.begin(); it != node_paths.end(); it++){
        region_paths.push_back(it->second);
    }
    return region_paths;
}

//the node path, the slam node and the grounding prob for each valid landmark 
void ComplexLanguageEvent::addNewRegionPath(RegionNode *region_node, NodePath path, map<int, double> grounding_prob, double prob){
    RegionPath *r_path = new RegionPath(region_node, path, info, figure_id, use_factor_graph);
    map<int, regionLandmark>::iterator it_l; 
    for(it_l = valid_landmarks.begin(); it_l != valid_landmarks.end(); it_l++){   
        r_path->addLandmark(it_l->second);
    }
    //for(int i=0; i < grounding_prob.size(); i++){
    map<int, double>::iterator it;
    for(it = grounding_prob.begin(); it != grounding_prob.end(); it++){
        it_l = valid_landmarks.find(it->first);
        if(it_l != valid_landmarks.end()){
            r_path->updateProbability(it_l->second.first, it->second);
        }
    }
    //this should copy over the existing prob
    if(valid_landmarks.size() == 0){
        r_path->updateProbability(NULL, prob);
    }
    node_paths.insert(make_pair(region_node, r_path));
}

void ComplexLanguageEvent::removeNodePath(RegionNode *region){    
    map<RegionNode *, RegionPath *>::iterator it; 
    it = node_paths.find(region);    
    if(it != node_paths.end()){
        //remove         
        node_paths.erase(it);
        delete it->second;
    }
}

RegionPath *ComplexLanguageEvent::getRegionPath(RegionNode *region){
    map<RegionNode *, RegionPath *>::iterator it; 
    
    it = node_paths.find(region);
    
    if(it != node_paths.end()){
        return it->second;
    }
    return NULL;
}

void ComplexLanguageEvent::printPath(SlamNode *node){
    map<RegionNode *, RegionPath *>::iterator it; 

    it = node_paths.find(node->region_node);
    
    if(it == node_paths.end()){
        fprintf(stderr, GREEN "Language Event : %d Node %d => No Path found to Node : %d\n" RESET_COLOR, 
                (int) event_id, (int) node_id, (int) node->id);
    }
    else{
        fprintf(stderr, GREEN "Language Event : %d Node %d => Path to Node : %d\n\t" RESET_COLOR, 
                (int) event_id, (int) node_id,(int) node->id);
        RegionPath *path = it->second;
        path->print();
    }
}

void ComplexLanguageEvent::clearNodePaths(){
    //this needs to delete all the contained paths 
    map<RegionNode *, RegionPath *>::iterator it; 

    for(it = node_paths.begin(); it != node_paths.end(); it++){
        delete it->second;
    }
    node_paths.clear();
}

void ComplexLanguageEvent::fillNodePaths(slam_complex_language_path_list_t &msg){
    msg.event_id = event_id;
    msg.utterence = strdup(utterance.c_str());
    msg.landmark = strdup(landmark.c_str());
    msg.figure = strdup(figure.c_str());
    msg.sr = strdup(sr.c_str());
    
    msg.valid_landmark_count = valid_landmarks.size();
    msg.valid_landmarks = (int64_t *) calloc(msg.valid_landmark_count, sizeof(int64_t));

    int i = 0;
    map<int, regionLandmark>::iterator it_l; 
    for(it_l = valid_landmarks.begin(); it_l != valid_landmarks.end(); it_l++, i++){        
        msg.valid_landmarks[i] = it_l->second.first->mean_node->id;
    }

    vector<regionPathProb> normalized_paths = getNormalizePaths(-1);
    
    msg.no_paths = (int) normalized_paths.size();
    msg.path = (slam_node_path_t *) calloc(msg.no_paths, sizeof(slam_node_path_t));
    
    for(int i=0; i < normalized_paths.size(); i++){
        NodePath path = normalized_paths[i].first->path; 
        normalized_paths[i].first->isUpdated()? msg.path[i].updated = 1: msg.path[i].updated =0;
        //should we use the prior?? 
        //cause we use the prior in the entropy calculations 
        msg.path[i].prob = normalized_paths[i].second; 
        msg.path[i].count = (int) path.size();
        msg.path[i].node_id = normalized_paths[i].first->region->mean_node->id;
        msg.path[i].nodes = (int64_t *) calloc(msg.path[i].count, sizeof(int64_t));
        for(int j=0; j < path.size(); j++){
            msg.path[i].nodes[j] = path[j]->id;
        }

        msg.path[i].no_questions = (int) answered_questions.size();
        msg.path[i].orig_prob = normalized_paths[i].first->prob; 
        msg.path[i].prior = normalized_paths[i].first->region->getProbabilityOfLabelPrior(figure);
        if(answered_questions.size() > 0){
            msg.path[i].answer_prob = (slam_probability_element_t *) calloc(msg.path[i].no_questions, sizeof(slam_probability_element_t));
            for(int k=0; k < answered_questions.size(); k++){
                int q_id = answered_questions[k]->id; 
                msg.path[i].answer_prob[k].type = q_id;
                normalized_paths[i].first->getFigureAnswerLikelihood(q_id, msg.path[i].answer_prob[k].probability);
            }
        }

        else{
            msg.path[i].answer_prob = NULL; 
        }

        msg.path[i].no_landmarks = 0;
        msg.path[i].landmark_ids = NULL;
    }

    msg.no_questions = (int) answered_questions.size();
    if(answered_questions.size() > 0){
        msg.questions = (slam_language_question_result_t *) calloc(msg.no_questions, sizeof(slam_language_question_result_t));
         for(int k=0; k < answered_questions.size(); k++){
             int q_id = answered_questions[k]->id; 
             msg.questions[k].id = q_id; 
             msg.questions[k].question = strdup(answered_questions[k]->getQuestionString().c_str()); 
             msg.questions[k].answer = strdup(answered_questions[k]->getAnswerString().c_str());
             msg.questions[k].q_asked_id = answered_questions[k]->node->id; 
             msg.questions[k].landmark_id = answered_questions[k]->getLandmarkRegionID();
             msg.questions[k].landmark = answered_questions[k]->landmark; 
         }
    }
    else{
        msg.questions = NULL;
    }    
}

void ComplexLanguageEvent::printEntropyStats(){
    vector<regionPathProb> normalized_paths = getNormalizePaths(-1);
    
    fprintf(stdout, "-------Language Event : %d - %s ------\n", event_id, utterance.c_str());

    sprintf(buf, "[Complex Lang] [Results] Language Event : %d - %s ------\n", event_id, utterance.c_str());
    output_writer->write_to_buffer(buf);

    vector<double> original_dist;

    vector<double> final_dist;

    double sum_orig = 0;

    double sum_final = 0;

    for(int i=0; i < normalized_paths.size(); i++){
        NodePath path = normalized_paths[i].first->path; 
        double prior = normalized_paths[i].first->region->getProbabilityOfLabelPrior(figure);
        
        //this is without the prior
        double orig_prob_no_prior = normalized_paths[i].first->prob; 
        double orig_prob_with_prior = orig_prob_no_prior * prior;

        double final_prob_no_prior = normalized_paths[i].second; 

        double final_prob_with_prior = final_prob_no_prior * prior;

        sum_orig += orig_prob_with_prior;
        sum_final += final_prob_with_prior;
    }

    if(sum_orig < 1.0e-10){
        sum_orig = 1;
    }

    if(sum_final < 1.0e-10){
        sum_final = 1;
    }


    for(int i=0; i < normalized_paths.size(); i++){
        NodePath path = normalized_paths[i].first->path; 
        double prior = normalized_paths[i].first->region->getProbabilityOfLabelPrior(figure);
        
        //this is without the prior
        double orig_prob_no_prior = normalized_paths[i].first->prob; 
        double orig_prob_with_prior = orig_prob_no_prior * prior / sum_orig;

        double final_prob_no_prior = normalized_paths[i].second; 

        double final_prob_with_prior = final_prob_no_prior * prior / sum_final;

        original_dist.push_back(orig_prob_with_prior);
        final_dist.push_back(final_prob_with_prior);

        fprintf(stdout, "[Complex Lang] [Results] Region : %d - Prior : %f Path Prob (Original) : %.2f - Path Prob (Final) : %.2f"
                " No questions : %d \n", normalized_paths[i].first->region->region_id, 
                prior, orig_prob_with_prior, final_prob_with_prior, (int) answered_questions.size());

        sprintf(buf, "[Complex Lang] [Results] Region : %d - Prior : %f Path Prob (Original) : %.2f - Path Prob (Final) : %.2f"
                " No questions : %d \n", normalized_paths[i].first->region->region_id, 
                prior, orig_prob_with_prior, final_prob_with_prior, (int) answered_questions.size());
        output_writer->write_to_buffer(buf);

        if(answered_questions.size() > 0){
            for(int k=0; k < answered_questions.size(); k++){
                int q_id = answered_questions[k]->id; 
                double answer_prob = 0;
                normalized_paths[i].first->getFigureAnswerLikelihood(q_id, answer_prob);
                //fprintf(stdout, "\t[Complex Lang] [Results] Question %d - Answer Prob : %.2f\n", q_id, answer_prob);
                
                sprintf(buf, "[Complex Lang] [Results] \tQuestion %d - Answer Prob : %.2f\n", q_id, answer_prob);
                output_writer->write_to_buffer(buf);
            }
        }
    }

    double original_entropy = calculateEntropy(original_dist);
    double final_entropy = calculateEntropy(final_dist);

    fprintf(stdout, "\n[Complex Lang] [Results] Original Entropy : %.3f - Final Entropy : %.3f\n", 
            original_entropy, final_entropy);

    sprintf(buf, "[Complex Lang] [Results] Original Entropy : %.3f - Final Entropy : %.3f\n", 
            original_entropy, final_entropy);
    output_writer->write_to_buffer(buf);
    
    fprintf(stdout, "\n");
}

//send only the outstanding paths 
void ComplexLanguageEvent::fillOutstandingNodePaths(slam_complex_language_path_list_t &msg){
    msg.event_id = event_id;
    msg.utterence = strdup(utterance.c_str());
    msg.landmark = strdup(landmark.c_str());
    msg.figure = strdup(figure.c_str());
    msg.sr = strdup(sr.c_str());
    msg.utterence_node = utterence_node->id;
    msg.utterence_region = utterence_node->region_node->region_id;

    map<RegionNode *, RegionPath *>::iterator it; 
    int count = 0;
    for(it = node_paths.begin(); it != node_paths.end(); it++){
        NodePath path = it->second->path; 
        if(!it->second->isUpdated())
            count++;
    }
    
    if(count == 0){
        msg.no_paths = 0;
        msg.path = NULL;
        return;
    }
    
    msg.valid_landmark_count = valid_landmarks.size();
    msg.valid_landmarks = (int64_t *) calloc(msg.valid_landmark_count, sizeof(int64_t));

    int i = 0;
    map<int, regionLandmark>::iterator it_l; 
    for(it_l = valid_landmarks.begin(); it_l != valid_landmarks.end(); it_l++, i++){        
        msg.valid_landmarks[i] = it_l->second.first->mean_node->id;
    }
    
    msg.no_paths = count;
    msg.path = (slam_node_path_t *) calloc(msg.no_paths, sizeof(slam_node_path_t));
        
    i = 0;
    for(it = node_paths.begin(); it != node_paths.end(); it++){
        NodePath path = it->second->path; 
        if(it->second->isUpdated())
            continue;

        it->second->isUpdated()? msg.path[i].updated = 1: msg.path[i].updated =0;
        msg.path[i].prob = it->second->getProbability();
        msg.path[i].count = (int) path.size();
        msg.path[i].node_id = it->first->mean_node->id;
        msg.path[i].nodes = (int64_t *) calloc(msg.path[i].count, sizeof(int64_t));
        for(int j=0; j < path.size(); j++){
            msg.path[i].nodes[j] = path[j]->id;
        }
        map<RegionNode *, GroundingProb>::iterator it_lm; 

        int lm_count = 0;
        for(it_lm = it->second->landmark_prob.begin(); it_lm != it->second->landmark_prob.end(); it_lm++){
            if(!it_lm->second.evaluated)
                lm_count++;
        }

        msg.path[i].no_landmarks = lm_count;
        msg.path[i].landmark_ids = (int64_t *) calloc(msg.path[i].no_landmarks, sizeof(int64_t));
        int k=0;
        for(it_lm = it->second->landmark_prob.begin(); it_lm != it->second->landmark_prob.end(); it_lm++){
            if(!it_lm->second.evaluated){
                msg.path[i].landmark_ids[k] = it_lm->first->mean_node->id;
                k++;
            }
        }

        i++;
    }
}

vector<regionProb> ComplexLanguageEvent::getGroundedRegions(){
    vector<regionProb> result;
    //we can normalize this if we need to 
    map<RegionNode *, RegionPath *>::iterator it; 
    for(it = node_paths.begin(); it != node_paths.end(); it++){
        double prob = it->second->getProbability();
        if(prob > 0){
            result.push_back(make_pair(it->first, prob));
        }        
    }    
    sort(result.begin(), result.end(), compareRegionProb);
    return result;
}

vector<RegionPath *> ComplexLanguageEvent::getGroundedRegionPaths(){
    vector<RegionPath *> result;
    //we can normalize this if we need to 
    map<RegionNode *, RegionPath *>::iterator it; 
    for(it = node_paths.begin(); it != node_paths.end(); it++){
        double prob = it->second->getProbability();
        if(prob > 0){
            result.push_back(it->second);
        }        
    }    
    sort(result.begin(), result.end(), compareRegionPaths);
    return result;
}

map<int, double> ComplexLanguageEvent::getLandmarkLikelihoodsForFigure(RegionNode *fig_region){
     map<RegionNode *, RegionPath *>::iterator it; 
     it = node_paths.find(fig_region);
     map<int, double> figure_landmark_prob; 
     if(it != node_paths.end()){
         fprintf(stderr, "Region found : %d\n", fig_region->region_id);
         return it->second->getNormalizedLandmarkDistribution();
     }
     return figure_landmark_prob; 
}

int ComplexLanguageEvent::updateResult(vector<pair<RegionNode *, slam_complex_language_path_result_t*> > &path_results){
    //search through each path and update the result 
    //the path should have likelihoods for each landmark region ?? (as this is how its sent back) 
    map<int, regionLandmark>::iterator it_l; 
    
    map<RegionNode *, RegionPath *>::iterator it; 
    for(int i=0; i < path_results.size(); i++){
        //reduce the outstanding language count 
        RegionNode *region = path_results[i].first;
        slam_complex_language_path_result_t *p_result = path_results[i].second;
        it = node_paths.find(region);
        if(it != node_paths.end()){
            //update the path likelihoods 
            for(int j=0; j < p_result->no_landmarks; j++){
                //find the landmark region and then updated the probability
                it_l = valid_landmarks.find(p_result->landmark_results[j].landmark_id);
                if(it_l == valid_landmarks.end()){
                    fprintf(stderr, "Region Not found - this should not have happend\n");
                    exit(-1);
                }
                else{
                    RegionNode *landmark_region = it_l->second.first;
                    //this updates the path probability 
                    it->second->updateProbability(landmark_region, p_result->landmark_results[j].path_probability);
                }
            }
        }
    }
    fprintf(stderr, "Oustanding Results : %d\n", outstanding_language);
    if(outstanding_language < 0)
        exit(-1);
}

void ComplexLanguageEvent::clearLandmarks(){
    valid_landmarks.clear();
}

//this gives the distribution over the figures (that also includes the prior likelihood) 
//where as the normalized paths gives the grounding likelihood 
vector<regionProb> ComplexLanguageEvent::getFigureDistribution(){
    map<RegionNode *, RegionPath *>::iterator it; 

    //fprintf(stderr, "\nComplex Langage Event : %d => Utterence : %s\n", event_id, utterance.c_str());

    vector<regionProb> figure_results; 
    
    //do we consider this?? or consider the prior as well?? - with the prior the entropy is much less 

    double sum = 0;
    
    bool use_prior = true;
    
    
    for(it = node_paths.begin(); it != node_paths.end(); it++){
        RegionPath *path = it->second;            

        double probability_grounding = path->getProbability(); 
        double prior_likelihood = path->region->getProbabilityOfLabelPrior(figure);
        
        if(use_prior){
            probability_grounding *= prior_likelihood; 
        }
        
        figure_results.push_back(make_pair(path->region, probability_grounding));
        sum += probability_grounding; 
    }
        
    sort(figure_results.begin(), figure_results.end(), compareRegionProb);
        
    //fprintf(stderr, "\n\n");
    sprintf(buf, "[Complex Lang] [Figure] Figure Distribution");
        output_writer->write_to_buffer(buf);
    for(int i=0; i < figure_results.size(); i++){
        if(sum > 0)
            figure_results[i].second /= sum;

        sprintf(buf, "[Complex Lang] [Figure] Region : %d - %f", figure_results[i].first->region_id, figure_results[i].second);
        output_writer->write_to_buffer(buf);
    }
    return figure_results;
}

int ComplexLanguageEvent::getNoAskedQuestions(){
    return asked_questions.size();
}

void ComplexLanguageEvent::updateAskedQuestions(spatialRelation sr){
    //update the number of asked questions 
    asked_questions.push_back(sr);
}

spatialResult ComplexLanguageEvent::evalSRQuestionsFromLandmark(SLUClassifier *slu_classifier, SlamNode *nd, 
                                                                int *landmark, 
                                                                map<SlamNode *, SlamNode *> &parent_map, bot_lcmgl_t *lcmgl){

    const spatialRelation lst[] = {NEAR}; 
    vector<spatialRelation> sr_list(lst, lst + sizeof(lst)/sizeof(spatialRelation));

    vector<regionProb> figure_results = getFigureDistribution();    

    double figure_entropy = calculateEntropy(figure_results);

    printDistribution(output_writer, figure_results, string("[Complex Language] [Eval SR LM] [Figure Dist]"));
    
    vector<regionProb> landmark_results = getLandmarkDistribution();

    printDistribution(output_writer, landmark_results, string("[Complex Language] [Eval SR LM] [Landmark Dist]"));
       
    //HACK - Sachi //for now we don't consider landmarks - since we need to consider the effect it has on the 
    //figures based on the answer 
    //double landmark_entropy = calculateEntropy(landmark_results);

    double landmark_entropy = 0; 
    
    sprintf(buf, "[Complex Lang] [Question] [Eval SR LM] Event : %d Figure Entropy : %f", event_id, figure_entropy);
    output_writer->write_to_buffer(buf);

    sprintf(buf, "[Complex Lang] [Question] [Eval SR LM] Event : %d Landmark Entropy : %f", event_id, landmark_entropy);
    output_writer->write_to_buffer(buf);

    //we should not pick the highest entropy entity - but the one with the scope to reduce 
    //the entropy (i.e. there might not be a question to ask about the highest 
    //entropy entity 
    
    double original_entropy = 0;
    vector<regionProb> check_regions; 
    if(figure_entropy == 0 && landmark_entropy == 0){
        sprintf(buf, "[Complex Lang] [Question] [Eval SR LM] Event : %d No entropy over figure or landmarks", event_id);
        output_writer->write_to_buffer(buf);
        return spatialResult(INVALID,0);
    }    
    
    if(figure_entropy > landmark_entropy){
        check_regions = figure_results;
        *landmark = 0;
        original_entropy = figure_entropy;
    }
    else{
        //note that if we resolve uncertanity about the landmark - this might reduce the uncertainity over 
        //the figures as well - which is not considered right now - in the yes/no entropy calculations 
        check_regions = landmark_results;
        *landmark = 1;
        original_entropy = landmark_entropy;
    }

    if(check_regions.size() == 0){
        sprintf(buf, "[Complex Lang] [Question] [Eval SR LM] Event : %d Size of regions to check for questions is zero", event_id);
        output_writer->write_to_buffer(buf);

        return spatialResult(INVALID,0);
    }

    sprintf(buf, "[Complex Lang] [Question] [Eval SR LM] Event : %d Current Robot Node : %d", event_id, nd->id);
    output_writer->write_to_buffer(buf);

    //if likelihood is below this threshold - set to 0 - so we don't end up using low likelihood differences 
    double min_yes_threshold = 0.2;
    
    double max_info_gain = 0;
    double min_cost = MAX_COST_QUESTION;
    spatialRelation max_sr = INVALID;
    
    //it might not be advisable to keep this as RegionNode* as this might change by the time the answer is incorporated??
    map<int, map<int, double> > max_sr_likelihood; 

    sprintf(buf, "[Complex Lang] [Question] [Eval SR LM] Event : %d =======================", event_id);
    output_writer->write_to_buffer(buf);

    //we should calculate the entroy for when the answer is yes and no 
    for(int i=0; i < sr_list.size(); i++){        
        
        sprintf(buf, "[Complex Lang] [Question] [Eval SR LM] Event : %d Spatial Relation : %s------------", event_id, getSR(sr_list[i]).c_str());
        output_writer->write_to_buffer(buf);

        map<int, map<int, double> > sr_likelihood;  // region , answer_id, prob_of_answer_given_question_and_grounding

        questionEvalResult question_eval(event_id, sr_list[i], nd, output_writer); 

        string q_sr = getSR(sr_list[i]);     

        for(int j=0; j < check_regions.size(); j++){

            SlamNode *region_node = check_regions[j].first->mean_node;
            assert(region_node);

            //check the distance from the current location to the region 
            
            double dist = 0; 
            vector<SlamNode *> path = getPathAndLength(parent_map, region_node, &dist, true);         

            sprintf(buf, "[Complex Lang] [Question] [Eval SR LM] [Distance Check] Event : %d Distance to region %d - %f Path : %d - %d", event_id, check_regions[j].first->region_id, dist, (int) path[0]->id, (int) path[path.size() - 1]->id);
            output_writer->write_to_buffer(buf);

            map<int, double> answer_map; 
            double prob_yes = VALID_SR_LIKELIHOOD;//0.5; - if its invalid - then the likihood of getting yes answer is low 
            double prob_no = 1- prob_yes;
            
            double prob_invalid = 1 - VALID_SR_LIKELIHOOD; 

            double prior_likelihood = check_regions[j].second;

            if((sr_list[i] == NEAR && dist < NEAR_VALID_SR_THRESHOLD) || dist < VALID_SR_THRESHOLD){        
                //if(dist < VALID_SR_THRESHOLD){
                //what if none of the regions are not high-likely at the given SR 
                vector<Pose> np_path = getPosesFromNodes(path, true); 
            
                double lm_z[2] = {0, 2.0};
                vector<Point> lm_points; 
                //for the question asking - we don't require landmarks - this is why the points are empty
                SLUDataPoint dp(np_path, lm_points, lm_z);
                vector<SLUDataPoint> datapoints;
                datapoints.push_back(dp);
            
                vector<double> results = slu_classifier->get_likelihood(q_sr, datapoints); 

                //draw the poses           
               

                if(results.size() > 0){
                    prob_yes = results[0]; 
                    prob_invalid = 0; 
#ifdef SR_HACK
                    Pose2d delta = nd->getPose().ominus(region_node->getPose());
                    double dist_d = hypot(delta.x(), delta.y());
                    
                    if(NEAR){
                        if(dist_d < NEAR_THRESHOLD){
                            //don't set near high if things are not close topologically 
                            //prob_yes = 0.95;                        
                        }
                        else{
                            prob_yes = 0.05;
                        }
                    }
                    else if(AT){
                        if(dist_d < AT_THRESHOLD){
                            //don't set near high if things are not close topologically 
                            //prob_yes = 0.95;                        
                        }
                        else{
                            prob_yes = 0.05;
                        }
                    }
#endif
                }
                else{
                    //set the p_yes to give us no information 
                    prob_invalid = 1.0; 
                    //we need to fix this 
                    prob_yes = 0.01; 
                }


                sprintf(buf, "[Complex Lang] [Question] [Eval SR LM] [Distance Check] Event : %d Checking SR : %s -> Prob : %.2f", event_id, q_sr.c_str(), prob_yes);
                output_writer->write_to_buffer(buf);
            }
            else{
                sprintf(buf, "[Complex Lang] [Question] [Eval SR LM] [Distance Check] Event : %d Region too far - assuming not valid SR", event_id);
                output_writer->write_to_buffer(buf);

                prob_yes = 0.01; 
                prob_invalid = 1.0; 
            }

            prob_no = 1 - prob_yes;                       

            answer_map.insert(make_pair(ANSWER_YES, prob_yes));
            answer_map.insert(make_pair(ANSWER_NO, prob_no));
            
            question_eval.add_region(check_regions[j].first->region_id, prior_likelihood, prob_invalid, answer_map, string("[Eval SR LM]"));            
        }        
        
        double information_gain = question_eval.get_expected_information();

        //get the cost of asking the question 
        double cost_question = getCostOfQuestion(sr_list[i], *landmark, information_gain, nd, -1);

        sprintf(buf, "[Complex Lang] [Question] [Eval SR LM] Event : %d Information gain : %f Cost: %f", 
                event_id, information_gain, cost_question);
        output_writer->write_to_buffer(buf);

        //we need to keep the SR likelihoods for yes/no answers - atleast for the max question 

        if(min_cost > cost_question){
            max_info_gain = information_gain;
            max_sr = sr_list[i];  
            //does this get destroyed?? 
            max_sr_likelihood = question_eval.get_sr_answer_map();//sr_likelihood;
            min_cost = cost_question; 
        }
    }

    sprintf(buf, "[Complex Lang] [Question] [Eval SR LM] [Best] Event : %d Max SR : %s Information gain : %f Cost: %f\n", 
            event_id, getSR(max_sr).c_str(), max_info_gain, min_cost);
    output_writer->write_to_buffer(buf);

    if(min_cost < MAX_COST_QUESTION){//max_info_gain > info_gain_threshold){
        return spatialResult(max_sr, max_info_gain, min_cost, max_sr_likelihood);
    }                                           
    else{
        return spatialResult(INVALID,0);
    }
}

spatialResult ComplexLanguageEvent::evalSRQuestionsFromCurrentLocation(SLUClassifier *slu_classifier, SlamNode *nd, 
                                                                       int *landmark, 
                                                                       map<SlamNode *, SlamNode *> &parent_map){

    const spatialRelation lst[] = {LEFT_OF, RIGHT_OF, IN_FRONT_OF, BEHIND, NEAR}; //, AT};
    vector<spatialRelation> sr_list(lst, lst + sizeof(lst)/sizeof(spatialRelation));

    vector<regionProb> figure_results = getFigureDistribution();    

    double figure_entropy = calculateEntropy(figure_results);

    printDistribution(output_writer, figure_results, string("[Complex Language] [Eval SR] [Figure Dist]"));
    
    vector<regionProb> landmark_results = getLandmarkDistribution();

    printDistribution(output_writer, landmark_results, string("[Complex Language] [Eval SR] [Landmark Dist]"));
       
    //HACK - Sachi //for now we don't consider landmarks - since we need to consider the effect it has on the 
    //figures based on the answer 
    //double landmark_entropy = calculateEntropy(landmark_results);

    double landmark_entropy = 0; 
    
    sprintf(buf, "[Complex Lang] [Question] [Eval SR] Event : %d Figure Entropy : %f", event_id, figure_entropy);
    output_writer->write_to_buffer(buf);

    sprintf(buf, "[Complex Lang] [Question] [Eval SR] Event : %d Landmark Entropy : %f", event_id, landmark_entropy);
    output_writer->write_to_buffer(buf);

    //we should not pick the highest entropy entity - but the one with the scope to reduce 
    //the entropy (i.e. there might not be a question to ask about the highest 
    //entropy entity 
    
    double original_entropy = 0;
    vector<regionProb> check_regions; 
    if(figure_entropy == 0 && landmark_entropy == 0){
        sprintf(buf, "[Complex Lang] [Question] [Eval SR] Event : %d No entropy over figure or landmarks", event_id);
        output_writer->write_to_buffer(buf);
        return spatialResult(INVALID,0);
    }    
    
    if(figure_entropy > landmark_entropy){
        check_regions = figure_results;
        *landmark = 0;
        original_entropy = figure_entropy;
    }
    else{
        //note that if we resolve uncertanity about the landmark - this might reduce the uncertainity over 
        //the figures as well - which is not considered right now - in the yes/no entropy calculations 
        check_regions = landmark_results;
        *landmark = 1;
        original_entropy = landmark_entropy;
    }

    if(check_regions.size() == 0){
        sprintf(buf, "[Complex Lang] [Question] [Eval SR] Event : %d Size of regions to check for questions is zero", event_id);
        output_writer->write_to_buffer(buf);

        return spatialResult(INVALID,0);
    }

    sprintf(buf, "[Complex Lang] [Question] [Eval SR] Event : %d Current Robot Node : %d", event_id, nd->id);
    output_writer->write_to_buffer(buf);

    //if likelihood is below this threshold - set to 0 - so we don't end up using low likelihood differences 
    double min_yes_threshold = 0.2;
    
    double max_info_gain = 0;
    double min_cost = MAX_COST_QUESTION;
    spatialRelation max_sr = INVALID;
    
    //it might not be advisable to keep this as RegionNode* as this might change by the time the answer is incorporated??
    map<int, map<int, double> > max_sr_likelihood; 

    sprintf(buf, "[Complex Lang] [Question] [Eval SR] Event : %d =======================", event_id);
    output_writer->write_to_buffer(buf);

    //we should calculate the entroy for when the answer is yes and no 
    for(int i=0; i < sr_list.size(); i++){
        
        
        sprintf(buf, "[Complex Lang] [Question] [Eval SR] Event : %d Spatial Relation : %s------------", event_id, getSR(sr_list[i]).c_str());
        output_writer->write_to_buffer(buf);

        map<int, map<int, double> > sr_likelihood;  // region , answer_id, prob_of_answer_given_question_and_grounding

        questionEvalResult question_eval(event_id, sr_list[i], nd, output_writer); 

        string q_sr = getSR(sr_list[i]);     

        for(int j=0; j < check_regions.size(); j++){

            SlamNode *region_node = check_regions[j].first->mean_node;
            assert(region_node);

            //check the distance from the current location to the region 
            
            double dist = 0; 
            vector<SlamNode *> path = getPathAndLength(parent_map, region_node, &dist, true);         

            sprintf(buf, "[Complex Lang] [Question] [Eval SR] [Distance Check] Event : %d Distance to region %d - %f Path : %d - %d", event_id, check_regions[j].first->region_id, dist, (int) path[0]->id, (int) path[path.size() - 1]->id);
            output_writer->write_to_buffer(buf);

            map<int, double> answer_map; 
            double prob_yes = VALID_SR_LIKELIHOOD;//0.5; - if its invalid - then the likihood of getting yes answer is low 
            double prob_no = 1- prob_yes;
            double prob_invalid = 1.0 - VALID_SR_LIKELIHOOD;
            double prior_likelihood = check_regions[j].second;

            if((sr_list[i] == NEAR && dist < NEAR_VALID_SR_THRESHOLD) || dist < VALID_SR_THRESHOLD){        
                //if(dist < VALID_SR_THRESHOLD){
                //what if none of the regions are not high-likely at the given SR 
                vector<Pose> np_path = getPosesFromNodes(path); 
            
                double lm_z[2] = {0, 2.0};
                //for the question asking - we don't require landmarks - this is why the points are empty
                vector<Point> lm_points; 
                SLUDataPoint dp(np_path, lm_points, lm_z);
                vector<SLUDataPoint> datapoints;
                datapoints.push_back(dp);
            
                vector<double> results = slu_classifier->get_likelihood(q_sr, datapoints); 
            
                sprintf(buf, "[Complex Lang] [Question] [Eval SR] [Distance Check] Event : %d Checking SR", event_id);
                output_writer->write_to_buffer(buf);

                if(results.size() > 0){
                    prob_yes = results[0]; 
                    prob_no = 1 - prob_yes;             
                    prob_invalid = 0;
#ifdef SR_HACK
                    Pose2d delta = nd->getPose().ominus(region_node->getPose());
                    double dist_d = hypot(delta.x(), delta.y());
                    
                    if(NEAR){
                        if(dist_d < NEAR_THRESHOLD){
                            //don't set near high if things are not close topologically 
                            //prob_yes = 0.95;                        
                        }
                        else{
                            prob_yes = 0.05;
                        }
                    }
                    else if(AT){
                        if(dist_d < AT_THRESHOLD){
                            //don't set near high if things are not close topologically 
                            //prob_yes = 0.95;                        
                        }
                        else{
                            prob_yes = 0.05;
                        }
                    }                    
#endif
                }
                else{
                    //set the p_yes to give us no information 
                    prob_yes = 0.01; 
                    prob_invalid = 1.0;
                }
            }
            else{
                sprintf(buf, "[Complex Lang] [Question] [Eval SR] [Distance Check] Event : %d Region too far - assuming not valid SR", event_id);
                output_writer->write_to_buffer(buf);
                
                prob_yes = 0.01; 
                prob_invalid = 1.0;
            }

            prob_no = 1 - prob_yes;                       

            answer_map.insert(make_pair(ANSWER_YES, prob_yes));
            answer_map.insert(make_pair(ANSWER_NO, prob_no));
            //answer_map.insert(make_pair(ANSWER_INVALID, prob_invalid));
            
            question_eval.add_region(check_regions[j].first->region_id, prior_likelihood, prob_invalid, answer_map);            
        }        
        
        double information_gain = question_eval.get_expected_information();

        //get the cost of asking the question 
        double cost_question = getCostOfQuestion(sr_list[i], *landmark, information_gain, nd, -1);

        sprintf(buf, "[Complex Lang] [Question] [Eval SR] Event : %d Information gain : %f Cost: %f", 
                event_id, information_gain, cost_question);
        output_writer->write_to_buffer(buf);

        //we need to keep the SR likelihoods for yes/no answers - atleast for the max question 

        if(min_cost > cost_question){
            max_info_gain = information_gain;
            max_sr = sr_list[i];  
            //does this get destroyed?? 
            max_sr_likelihood = question_eval.get_sr_answer_map();//sr_likelihood;
            min_cost = cost_question; 
        }
    }

    sprintf(buf, "[Complex Lang] [Question] [Eval SR] [Best] Event : %d Max SR : %s Information gain : %f Cost: %f\n", 
            event_id, getSR(max_sr).c_str(), max_info_gain, min_cost);
    output_writer->write_to_buffer(buf);

    if(min_cost < MAX_COST_QUESTION){//max_info_gain > info_gain_threshold){
        return spatialResult(max_sr, max_info_gain, min_cost, max_sr_likelihood);
    }                                           
    else{
        return spatialResult(INVALID,0);
    }
}

vector<spatialResult> ComplexLanguageEvent::getQuestionCosts(SLUClassifier *slu_classifier, SlamNode *nd, 
                                                             int *landmark, 
                                                             map<SlamNode *, SlamNode *> &parent_map, 
                                                             const vector<spatialRelation> sr_list){

    vector<spatialResult> questions; 

    vector<regionProb> figure_results = getFigureDistribution();    

    double figure_entropy = calculateEntropy(figure_results);

    printDistribution(output_writer, figure_results, string("[Complex Language] [Eval SR] [Figure Dist]"));
    
    vector<regionProb> landmark_results = getLandmarkDistribution();

    printDistribution(output_writer, landmark_results, string("[Complex Language] [Eval SR] [Landmark Dist]"));
       
    //HACK - Sachi //for now we don't consider landmarks - since we need to consider the effect it has on the 
    //figures based on the answer 
    //double landmark_entropy = calculateEntropy(landmark_results);

    double landmark_entropy = 0; 
    
    sprintf(buf, "[Complex Lang] [Question] [Eval SR] Event : %d Figure Entropy : %f", event_id, figure_entropy);
    output_writer->write_to_buffer(buf);

    sprintf(buf, "[Complex Lang] [Question] [Eval SR] Event : %d Landmark Entropy : %f", event_id, landmark_entropy);
    output_writer->write_to_buffer(buf);

    //we should not pick the highest entropy entity - but the one with the scope to reduce 
    //the entropy (i.e. there might not be a question to ask about the highest 
    //entropy entity 
    
    double original_entropy = 0;
    vector<regionProb> check_regions; 
    if(figure_entropy == 0 && landmark_entropy == 0){
        sprintf(buf, "[Complex Lang] [Question] [Eval SR] Event : %d No entropy over figure or landmarks", event_id);
        output_writer->write_to_buffer(buf);
        return questions;
    }    
    
    if(figure_entropy > landmark_entropy){
        check_regions = figure_results;
        *landmark = 0;
        original_entropy = figure_entropy;
    }
    else{
        //note that if we resolve uncertanity about the landmark - this might reduce the uncertainity over 
        //the figures as well - which is not considered right now - in the yes/no entropy calculations 
        check_regions = landmark_results;
        *landmark = 1;
        original_entropy = landmark_entropy;
    }

    if(check_regions.size() == 0){
        sprintf(buf, "[Complex Lang] [Question] [Eval SR] Event : %d Size of regions to check for questions is zero", event_id);
        output_writer->write_to_buffer(buf);

        return questions; //spatialResult(INVALID,0);
    }

    sprintf(buf, "[Complex Lang] [Question] [Eval SR] Event : %d Current Robot Node : %d", event_id, nd->id);
    output_writer->write_to_buffer(buf);

    //if likelihood is below this threshold - set to 0 - so we don't end up using low likelihood differences 
    double min_yes_threshold = 0.2;
    
    double max_info_gain = 0;
    double min_cost = MAX_COST_QUESTION;
    spatialRelation max_sr = INVALID;
    
    //it might not be advisable to keep this as RegionNode* as this might change by the time the answer is incorporated??
    map<int, map<int, double> > max_sr_likelihood; 

    sprintf(buf, "[Complex Lang] [Question] [Eval SR] Event : %d =======================", event_id);
    output_writer->write_to_buffer(buf);

    //we should calculate the entroy for when the answer is yes and no 
    for(int i=0; i < sr_list.size(); i++){
        
        sprintf(buf, "[Complex Lang] [Question] [Eval SR] Event : %d Spatial Relation : %s------------", event_id, getSR(sr_list[i]).c_str());
        output_writer->write_to_buffer(buf);

        map<int, map<int, double> > sr_likelihood;  // region , answer_id, prob_of_answer_given_question_and_grounding

        questionEvalResult question_eval(event_id, sr_list[i], nd, output_writer); 

        string q_sr = getSR(sr_list[i]);     

        for(int j=0; j < check_regions.size(); j++){

            SlamNode *region_node = check_regions[j].first->mean_node;
            assert(region_node);

            //check the distance from the current location to the region 
            
            double dist = 0; 
            vector<SlamNode *> path = getPathAndLength(parent_map, region_node, &dist, true);         

            sprintf(buf, "[Complex Lang] [Question] [Eval SR] [Distance Check] Event : %d Distance to region %d - %f Path : %d - %d", event_id, check_regions[j].first->region_id, dist, (int) path[0]->id, (int) path[path.size() - 1]->id);
            output_writer->write_to_buffer(buf);

            map<int, double> answer_map; 
            double prob_yes = VALID_SR_LIKELIHOOD;//0.5; - if its invalid - then the likihood of getting yes answer is low 
            double prob_no = 1- prob_yes;
            double prob_invalid = 1.0 - VALID_SR_LIKELIHOOD;
            double prior_likelihood = check_regions[j].second;

            if((sr_list[i] == NEAR && dist < NEAR_VALID_SR_THRESHOLD) || dist < VALID_SR_THRESHOLD){        
                //if(dist < VALID_SR_THRESHOLD){
                //what if none of the regions are not high-likely at the given SR 
                vector<Pose> np_path = getPosesFromNodes(path); 
            
                double lm_z[2] = {0, 2.0};
                //for the question asking - we don't require landmarks - this is why the points are empty
                vector<Point> lm_points; 
                SLUDataPoint dp(np_path, lm_points, lm_z);
                vector<SLUDataPoint> datapoints;
                datapoints.push_back(dp);
            
                vector<double> results = slu_classifier->get_likelihood(q_sr, datapoints); 
            
                sprintf(buf, "[Complex Lang] [Question] [Eval SR] [Distance Check] Event : %d Checking SR", event_id);
                output_writer->write_to_buffer(buf);

                if(results.size() > 0){
                    prob_yes = results[0]; 
                    prob_no = 1 - prob_yes;             
                    prob_invalid = 0;
#ifdef SR_HACK
                    Pose2d delta = nd->getPose().ominus(region_node->getPose());
                    double dist_d = hypot(delta.x(), delta.y());

                    if(NEAR){
                        if(dist_d < NEAR_THRESHOLD){
                            //don't set near high if things are not close topologically 
                            //prob_yes = 0.95;                        
                        }
                        else{
                            prob_yes = 0.05;
                        }
                    }
                    else if(AT){
                        if(dist_d < AT_THRESHOLD){
                            //don't set near high if things are not close topologically 
                            //prob_yes = 0.95;                        
                        }
                        else{
                            prob_yes = 0.05;
                        }
                    }          
#endif
                }
                else{
                    //set the p_yes to give us no information 
                    prob_yes = 0.01; 
                    prob_invalid = 1.0;
                }
            }
            else{
                sprintf(buf, "[Complex Lang] [Question] [Eval SR] [Distance Check] Event : %d Region too far - assuming not valid SR", event_id);
                output_writer->write_to_buffer(buf);
                
                prob_yes = 0.01; 
                prob_invalid = 1.0;
            }

            prob_no = 1 - prob_yes;                       

            answer_map.insert(make_pair(ANSWER_YES, prob_yes));
            answer_map.insert(make_pair(ANSWER_NO, prob_no));
            //answer_map.insert(make_pair(ANSWER_INVALID, prob_invalid));
            
            question_eval.add_region(check_regions[j].first->region_id, prior_likelihood, prob_invalid, answer_map);            
        }        
        
        double information_gain = question_eval.get_expected_information();

        //get the cost of asking the question 
        double cost_question = getCostOfQuestion(sr_list[i], *landmark, information_gain, nd, -1);

        sprintf(buf, "[Complex Lang] [Question] [Eval SR] Event : %d SR : %s Information gain : %f Cost: %f", 
                event_id, getSR(sr_list[i]).c_str(), information_gain, cost_question);
        output_writer->write_to_buffer(buf);

        //we need to keep the SR likelihoods for yes/no answers - atleast for the max question 

        if(min_cost > cost_question){
            max_info_gain = information_gain;
            max_sr = sr_list[i];  
            //does this get destroyed?? 
            max_sr_likelihood = question_eval.get_sr_answer_map();//sr_likelihood;
            min_cost = cost_question; 
        }

        questions.push_back(spatialResult(sr_list[i], information_gain, cost_question, question_eval.get_sr_answer_map()));
    }

    sprintf(buf, "[Complex Lang] [Question] [Eval SR] [Best] Event : %d Max SR : %s Information gain : %f Cost: %f\n", 
            event_id, getSR(max_sr).c_str(), max_info_gain, min_cost);
    output_writer->write_to_buffer(buf);

    return questions;
}

double ComplexLanguageEvent::evalQuestionsAboutRegion(RegionNode *region, double info_gain_threshold, int *landmark){
    //evaluate the Spatial relations from the current location 
    vector<regionProb> figure_results = getFigureDistribution();    

    double figure_entropy = calculateEntropy(figure_results);

    vector<regionProb> landmark_results = getLandmarkDistribution();

    double landmark_entropy = calculateEntropy(landmark_results);
    
    fprintf(stderr, "Figure Entropy : %f\n", figure_entropy);
    fprintf(stderr, "Landmark Entropy : %f\n", landmark_entropy);

    double original_entropy = 0;
    vector<regionProb> check_regions; 
    if(figure_entropy == 0 && landmark_entropy == 0){
        fprintf(stderr, "No entropy over figure or landmarks\n");
        return 0;
    }    
    
    //we should check if the current region is part of the landmark or figure list 
    int region_is_figure = 0;
    int region_is_landmark = 0;

    //check how much the entropy gets reduced - this should be zero if the region isnt a landmark or figure 

    vector<regionProb> figure_new_dist_yes;
    vector<regionProb> figure_new_dist_no;
    double remainder_sum = 0;
    double prob_yes = 0;
    for(int i=0; i < figure_results.size(); i++){
        if(figure_results[i].first == region){
            region_is_figure = 1;
            figure_new_dist_yes.push_back(make_pair(figure_results[i].first, 1.0));
            figure_new_dist_no.push_back(make_pair(figure_results[i].first, 0.0));
            prob_yes = figure_results[i].second; 
        }
        else{
            remainder_sum += figure_results[i].second;
            figure_new_dist_yes.push_back(make_pair(figure_results[i].first, 0.0));
            figure_new_dist_no.push_back(make_pair(figure_results[i].first, figure_results[i].second));
        }        
    }

    double expected_figure_entropy_gain = 0;
    
    if(region_is_figure){
    //normalize the remainder 
        if(remainder_sum > 0){
            for(int i=0; i < figure_new_dist_no.size(); i++){
                figure_new_dist_no[i].second = figure_new_dist_no[i].second / remainder_sum; 
            }
        }

        double figure_new_entropy_yes = calculateEntropy(figure_new_dist_yes);
        double figure_new_entropy_no = calculateEntropy(figure_new_dist_no);
    
        double new_expected_entropy = figure_new_entropy_yes * prob_yes + (1-prob_yes) * figure_new_entropy_no;
        expected_figure_entropy_gain = figure_entropy - new_expected_entropy;
        //this is the expected entropy gain if we ask whether the current region is the figure 
        //and get a yes/no answer 
    }

    vector<regionProb> landmark_new_dist_yes;
    vector<regionProb> landmark_new_dist_no;
    remainder_sum = 0;
    prob_yes = 0;
    for(int i=0; i < landmark_results.size(); i++){
        if(landmark_results[i].first == region){
            region_is_landmark = 1;
            landmark_new_dist_yes.push_back(make_pair(landmark_results[i].first, 1.0));
            landmark_new_dist_no.push_back(make_pair(landmark_results[i].first, 0.0));
            prob_yes = landmark_results[i].second; 
        }
        else{
            remainder_sum += landmark_results[i].second;
            landmark_new_dist_yes.push_back(make_pair(landmark_results[i].first, 0.0));
            landmark_new_dist_no.push_back(make_pair(landmark_results[i].first, landmark_results[i].second));
        }        
    }

    double expected_landmark_entropy_gain = 0;
    
    if(region_is_landmark){
    //normalize the remainder 
        if(remainder_sum > 0){
            for(int i=0; i < landmark_new_dist_no.size(); i++){
                landmark_new_dist_no[i].second = landmark_new_dist_no[i].second / remainder_sum; 
            }
        }

        double landmark_new_entropy_yes = calculateEntropy(landmark_new_dist_yes);
        double landmark_new_entropy_no = calculateEntropy(landmark_new_dist_no);
    
        double new_expected_entropy = landmark_new_entropy_yes * prob_yes + (1-prob_yes) * landmark_new_entropy_no;
        expected_landmark_entropy_gain = landmark_entropy - new_expected_entropy;
        //this is the expected entropy gain if we ask whether the current region is the landmark
        //and get a yes/no answer 
    }
  
    fprintf(stderr, "Expected landmark gain in entropy : %f\n Figure gain in entropy : %f\n", 
            expected_landmark_entropy_gain, expected_figure_entropy_gain);    
    
    if(!region_is_figure && !region_is_landmark){
        fprintf(stderr, "Region is not a landmark of figure - skipping asking question\n");
        return 0;
    }

    if(expected_landmark_entropy_gain > expected_figure_entropy_gain){
        *landmark = 1;
        return expected_landmark_entropy_gain;
    }
    else{
        *landmark = 0;
        return expected_figure_entropy_gain;
    }    
}

void ComplexLanguageEvent::print(bool print_path){
    map<RegionNode *, RegionPath *>::iterator it; 
    if(print_path && node_paths.size() > 0){
        //we should sort this before printing 
        vector<RegionPath *> result_paths;//node_paths.size()); 

        for(it = node_paths.begin(); it != node_paths.end(); it++){
            RegionPath *path = it->second;            
            result_paths.push_back(path);
        }
        
        sort(result_paths.begin(), result_paths.end(), compareRegionPaths);

        //do we want to print the top N?? 
        for(int i=0; i< result_paths.size(); i++){
            result_paths[i]->printProbability();            
        }
    }

    vector<regionProb> figure_results = getFigureDistribution();    

    double figure_entropy = calculateEntropy(figure_results);

    vector<regionProb> landmark_results = getLandmarkDistribution();

    double landmark_entropy = calculateEntropy(landmark_results);
    
    fprintf(stderr, "Figure Entropy : %f\n", figure_entropy);
    fprintf(stderr, "Landmark Entropy : %f\n", landmark_entropy);
}


void ComplexLanguageEvent::updateLandmarks(vector<RegionNode *> landmarks){
    //this can do some pruning on the landmarks as well - since these are just the physically close regions 
    
    map<int, regionLandmark>::iterator it; 
    map<int, regionLandmark>::iterator it_invalid; 
    for(int i=0; i < landmarks.size(); i++){
        it = valid_landmarks.find(landmarks[i]->region_id);
        it_invalid = invalid_landmarks.find(landmarks[i]->region_id);
        if(it == valid_landmarks.end() && it_invalid == invalid_landmarks.end()){
            addLandmark(landmarks[i]);
        }
    }    
}

vector<RegionNode *> ComplexLanguageEvent::getInvalidLandmarks(){
    map<int, regionLandmark>::iterator it;
    vector<RegionNode *> landmarks;
    for(it = invalid_landmarks.begin(); it != invalid_landmarks.end(); it++){
        landmarks.push_back(it->second.first);
    }
    return landmarks;
}

vector<RegionNode *> ComplexLanguageEvent::getValidLandmarks(){
    map<int, regionLandmark>::iterator it;
    vector<RegionNode *> landmarks;
    for(it = valid_landmarks.begin(); it != valid_landmarks.end(); it++){
        landmarks.push_back(it->second.first);
    }
    return landmarks;
}

RegionNode *ComplexLanguageEvent::getLandmarkNode(int id){
    map<int, regionLandmark>::iterator it;
    it = valid_landmarks.find(id);
    
    if(it != valid_landmarks.end()){
        return it->second.first;
    }
    else{
        return NULL;
    }
}

void ComplexLanguageEvent::addLandmark(RegionNode *landmark_node, int type){
    //check if the Region is a valid landmark - otherwise skip 
    //check if the likelihood of being landmark is above some threshold 
    if(type == 0){
        double threshold = ADD_LANDMARK_THRESHOLD;
        if(!use_factor_graph){
            //lets lower this threshold - HACK 
            //threshold = 0.2;
        }

        //update this method to work with synonyms 
        double p_landmark = landmark_node->getProbabilityOfLabel(landmark);

        sprintf(buf, "[Complex Lang] [Add Landmark] Event : %d Landmark %s Region : %d Prob: %f", event_id, landmark.c_str(), landmark_node->region_id, p_landmark);
        output_writer->write_to_buffer(buf);

        if(p_landmark < threshold){
            sprintf(buf, "[Complex Lang] [Add Landmark] Event : %d Skipping for landmark (%d) : prob %.3f ( < %.3f) is too low", event_id, landmark_node->region_id, p_landmark, threshold);
            output_writer->write_to_buffer(buf);
            //we should add this to an invlid landmark pile - so that we don't keep checking them 
            invalid_landmarks.insert(make_pair(landmark_node->region_id, make_pair(landmark_node, p_landmark)));
            return;
        }

        valid_landmarks.insert(make_pair(landmark_node->region_id, make_pair(landmark_node, p_landmark)));
        //add the landmark to the region paths (it should skip its own self
        map<RegionNode *, RegionPath *>::iterator it; 
        for(it = node_paths.begin(); it != node_paths.end(); it++){
            it->second->addLandmark(make_pair(landmark_node, p_landmark));
        }
    }
    else if(type == 1){
        double p_landmark = landmark_node->getProbabilityOfLabel(landmark);
        valid_landmarks.insert(make_pair(landmark_node->region_id, make_pair(landmark_node, p_landmark)));
        sprintf(buf, "[Complex Lang] [Add Landmark] Event : %d Valid landmark : %d - %f", event_id, landmark_node->region_id, p_landmark);
        output_writer->write_to_buffer(buf);
        //add the landmark to the region paths (it should skip its own self
        map<RegionNode *, RegionPath *>::iterator it; 
        for(it = node_paths.begin(); it != node_paths.end(); it++){
            it->second->addLandmark(make_pair(landmark_node, p_landmark));
        }
    }
    else if(type == 2){
        double p_landmark = landmark_node->getProbabilityOfLabel(landmark);
        sprintf(buf, "[Complex Lang] [Add Landmark] Event : %d Invalid landmark : %d - %f", event_id, landmark_node->region_id, p_landmark);
        output_writer->write_to_buffer(buf);
        invalid_landmarks.insert(make_pair(landmark_node->region_id, make_pair(landmark_node, p_landmark)));
    }
}

void ComplexLanguageEvent::removeLandmark(RegionNode *region){
    map<int, regionLandmark>::iterator it; 
    it = valid_landmarks.find(region->region_id);

    //remove the landmark from the event and the paths 
    if(it != valid_landmarks.end()){
        valid_landmarks.erase(it);
        map<RegionNode *, RegionPath *>::iterator it_p; 
        for(it_p = node_paths.begin(); it_p != node_paths.end(); it_p++){
            it_p->second->removeLandmark(region);
        }
    }

    it = invalid_landmarks.find(region->region_id);

    //remove the landmark from the event and the paths 
    if(it != invalid_landmarks.end()){
        invalid_landmarks.erase(it);        
    }
}




