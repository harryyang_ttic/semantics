#include "ExplorationUtils.hpp"

double ExpUtils::rand_d() {
    return double(rand()) / double(RAND_MAX);
}

double ExpUtils::rand_d(double min, double max) {
    return (max - min) * rand_d() - (max - min) / 2.0;
}

bool ExpUtils::is_map_free(double value) {
    if (value < UNK_THRESH && value >= 0.0) {
        return true;
    }
    return false;
}

bool ExpUtils::is_map_unexplored(double value) {
    if (value >= UNK_THRESH && value < OCC_THRESH) {
        return true;
    }
    return false;
}

bool ExpUtils::is_map_obstacle(double value) {
    if (value >= OCC_THRESH) {
        return true;
    }
    return false;
}

void ExpUtils::geometry_project_point(int x, int y, double theta, int *x2, int *y2, occ_map::FloatPixelMap* pixmap) //carmen_map_config_t map_defn)
{
    double delta_x, delta_y, delta_theta;
    double bounding_angle;

    int x_size = pixmap->dimensions[0];
    int y_size = pixmap->dimensions[1];

    theta = normalize_theta(theta);

    if (theta < M_PI / 2 && theta >= -M_PI / 2)
        delta_x = x_size - 1 - x;
    else
        delta_x = -x;
    if (theta >= 0)
        delta_y = y_size - 1 - y;
    else
        delta_y = -y;

    /* The angle to the corner to which theta is closest. */

    bounding_angle = atan2(delta_y, delta_x);

    /* This case if theta is going to run off the top of the bounding box. */

    if (theta >= 0 && ((theta < M_PI / 2 && theta >= bounding_angle) || (theta >= M_PI / 2 && theta < bounding_angle))) {
        *y2 = y_size - 1;
        delta_theta = M_PI / 2 - theta;
        *x2 = round_i(x + tan(delta_theta) * delta_y);
    }

    /* This case if theta is going to run off the right side
       of the bounding box. */

    else if ((theta < M_PI / 2 && theta >= -M_PI / 2) && ((theta >= 0 && theta < bounding_angle) || (theta < 0 && theta
                                                                                                     >= bounding_angle))) {
        *x2 = x_size - 1;
        delta_theta = theta;
        *y2 = round_i(y + tan(delta_theta) * delta_x);
    }

    /* This case if theta is going to run off the bottom of the bounding box. */

    else if (theta < 0 && ((theta >= -M_PI / 2 && theta < bounding_angle) || (theta < -M_PI / 2 && theta
                                                                              >= bounding_angle))) {
        *y2 = 0;
        delta_theta = -M_PI / 2 - theta;
        *x2 = round_i(x + tan(delta_theta) * (delta_y));
    }

    /* This case if theta is going to run off the left side
       of the bounding box. */

    else {
        *x2 = 0;
        if (theta < 0)
            delta_theta = -M_PI - theta;
        else
            delta_theta = M_PI - theta;
        *y2 = round_i(y + tan(delta_theta) * fabs(delta_x));
    }
}

//void ray_trace(occ_map::FloatPixelMap* pixmap, const point2i_t *a, double theta) {
void ExpUtils::ray_trace(occ_map::FloatPixelMap* pixmap, const point2i_t *a, double theta, double max_range,
                         double *intersection_value, double *intersection_range) {

    // get point on boundary of map
    point2i_t b;
    geometry_project_point(a->x, a->y, theta, &b.x, &b.y, pixmap);

    pointlist2i_t *pli = geom_line_seg_covered_points_2i(a, &b);

    double pixel_dist, dist;
    double cell_value;
    int ixy[2];

    *intersection_range = 0.0;
    *intersection_value = 0.0;

    for (int i = 0; i < pli->npoints; i++) {
        ixy[0] = pli->points[i].x;
        ixy[1] = pli->points[i].y;
    
        // check if ray goes off map
        if (ixy[0] < 0 || ixy[0] >= pixmap->dimensions[0] || ixy[1] < 0 || ixy[1] >= pixmap->dimensions[1]) {
            return;
        }

        // track distance from starting location
        pixel_dist = sqrt(pow(a->x - pli->points[i].x, 2) + pow(a->y - pli->points[i].y, 2));
        *intersection_range = pixel_dist * pixmap->metersPerPixel;

        // check cell values
        *intersection_value = pixmap->readValue(ixy);

        if (dist >= max_range) {
            return;
        }

        if (is_map_unexplored(*intersection_value)) {
            return;
        }

        if (is_map_obstacle(*intersection_value)) {
            return;
        }

    }
}

double ExpUtils::compute_weight_of_laser_frontier_point(int ixy[2], occ_map::FloatPixelMap* pixmap) {
    int num_steps = 50;
    double max_range = 10.0;
    double fov = 2 * M_PI;
    double step_size = fov / num_steps;
    //double start_theta = carmen_radians_to_degrees(pt->theta) - fov / 2;
    double start_theta = 0.0;

    double theta;
    double weight = 0;
    //  double MIN_OBSTACLE_DISTANCE_TOLERANCE = 0.5;
    double intersection_value, intersection_range;

    point2i_t a;
    a.x = ixy[0];
    a.y = ixy[1];

    // iterate over each theta and cast ray into map
    for (int i = 0; i < num_steps; i++) {
        theta = normalize_theta(start_theta + i * step_size);
        //cast_laser_ray_into_map(ixy, theta, max_range, pixmap, &intersection_value, &intersection_range);
        ray_trace(pixmap, &a, theta, max_range, &intersection_value, &intersection_range);

        // if cell is free, dweight = 0
        if (is_map_free(intersection_value))
            weight += 0.05;
        // if cell is unexplored, dweight = 1
        if (is_map_unexplored(intersection_value))
            weight += 1;
        // if cell is obstacle, dweight = 1
        if (is_map_obstacle(intersection_value))
            weight += 0.0;
    }

    return weight / double(num_steps); // normalized to 1 max
}

int ExpUtils::round_i(double X) {
    if (X >= 0)
        return (int) (X + 0.5);
    else
        return (int) (X - 0.5);
}

double ExpUtils::normalize_theta(double theta) {
    int multiplier;

    if (theta >= -M_PI && theta < M_PI)
        return theta;

    multiplier = (int) (theta / (2 * M_PI));
    theta = theta - multiplier * 2 * M_PI;
    if (theta >= M_PI)
        theta -= 2 * M_PI;
    if (theta < -M_PI)
        theta += 2 * M_PI;

    return theta;
}

bool ExpUtils::test_too_near_other_frontiers(std::vector<frontier> frontiers2i, int ixy[2], int pixel_length) {
  for (int i = 0; i < frontiers2i.size(); i++) {
    if (abs(frontiers2i[i].ipt.x - ixy[0]) + abs(frontiers2i[i].ipt.y - ixy[0]) < pixel_length) {
      return true;
    }
  }
  return false;
}

occ_map::FloatPixelMap * ExpUtils::getFrontierMap(const occ_map::FloatPixelMap * pixmap, const double start[2])
{
  if (pixmap->readValue(start) > 0) {
    fprintf(stderr, "ERROR: start point is in an obstacle!\n");
    return NULL;
  }

  occ_map::FloatPixelMap * utility = new occ_map::FloatPixelMap(pixmap->xy0, pixmap->xy1, pixmap->metersPerPixel, -1);

  occ_map::FloatPixelMap * frontier_map = new occ_map::FloatPixelMap(pixmap->xy0, pixmap->xy1, pixmap->metersPerPixel, 0);

  int ixy[2] = { 0 };
  utility->worldToTable(start, ixy);
  utility->writeValue(ixy, 0);
  std::priority_queue<FrontierElement, std::vector<FrontierElement>, FrontierCompare> frontier;
  frontier.push(FrontierElement(0, ixy));

  std::vector<FrontierClusterElement *> frontier_elements;
  std::map<int, std::vector<FrontierClusterElement *> > cluster;

  int count = 0;

  int f_count = 0;
  while (!frontier.empty()) {
      count++;

      const FrontierElement f = frontier.top();

      assert(pixmap->readValue(f.ixy) < UNK_THRESH);
      //add all the neighbors //TODO: do we care about more neighbors?
      int ixyn[2] = { 0 };
      //search the neighbours 
      for (int i = -1; i <= 1; i++) {
          ixyn[0] = f.ixy[0] + i;
          for (int j = -1; j <= 1; j++) {
              ixyn[1] = f.ixy[1] + j;
              if (i == 0 && j == 0)
                  continue;
              if (!utility->isInMap(ixyn) || utility->readValue(ixyn) >= 0)
                  continue;
              
              if (pixmap->readValue(ixyn)- UNK_THRESH < -0.001) {
                  float dist = sqrt(i * i + j * j);
                  frontier.push(FrontierElement(f.value + dist, ixyn));
                  // set a high value for the neighbor so that it doesn't get pushed
                  // on to the frontier again
                  utility->writeValue(ixyn, 1e9);                  
              }
              else if (pixmap->readValue(ixyn) > OCC_THRESH) {
                  utility->writeValue(ixyn, INFINITY);
              }
              else {
                  frontier_map->writeValue(ixyn, 1.0);
                  FrontierClusterElement *fc = new FrontierClusterElement(ixyn, f_count++);
                  frontier_elements.push_back(fc);
                  std::vector<FrontierClusterElement *> vec;
                  vec.push_back(fc);
                  cluster.insert(make_pair(fc->cluster, vec));
                  utility->writeValue(ixyn, INFINITY);
              }
          }
      }
     
      utility->writeValue(f.ixy, f.value);
      
      frontier.pop();
      
      if (count % 100000 == 0) {
          printf("dp_entry #%d, frontier size=%jd of %d\n", count, frontier.size(), pixmap->num_cells);
      }
  }

  fprintf(stderr, "Size of frontiers : %d\n", (int) frontier_elements.size());

  
  int threshold = 3;
  
  //doing some simple clustering 
  for(int i=0; i < frontier_elements.size(); i++){
      FrontierClusterElement *fc1 = frontier_elements[i];
      for(int j=0; j < frontier_elements.size(); j++){
          FrontierClusterElement *fc2 = frontier_elements[j];
          if(fc1->cluster == fc2->cluster)
              continue;
                    
          if(fc1->is_connected(*fc2, threshold)){
              //merge clusters
              //fprintf(stderr, "Found connected - Merging\n");
              int new_id = -1, replace_id = -1;
              fc1->cluster < fc2->cluster? new_id = fc1->cluster: new_id = fc2->cluster;
              fc1->cluster < fc2->cluster? replace_id = fc2->cluster: replace_id = fc1->cluster;
                            
              std::map<int, std::vector<FrontierClusterElement *> >::iterator it = cluster.find(replace_id);
              std::map<int, std::vector<FrontierClusterElement *> >::iterator it_new = cluster.find(new_id);
              assert(it_new != cluster.end());
              assert(it_new != it);
              if(it!= cluster.end()){
                  std::vector<FrontierClusterElement *> elements = it->second;
                  for(int k=0; k < elements.size(); k++){
                      elements[k]->cluster = new_id;
                      it_new->second.push_back(elements[k]);
                  }
                  //add to the other vector 
                  cluster.erase(it);
              }              
              //erase the other one 
          }
          //check if they are close - if so add to the same cluster
      }
  }

  fprintf(stderr, "Size of clusters : %d\n", (int) cluster.size());

  std::map<int, std::vector<FrontierClusterElement *> >::iterator it;
  for(it = cluster.begin(); it != cluster.end(); it++){
      fprintf(stderr, "Size of cluster : %d\n", (int) it->second.size());

      for(int i=0; i < it->second.size(); i++){
          FrontierClusterElement *fc1 = it->second[i];
          fprintf(stderr, "\t %d,%d\n", fc1->ixy[0], fc1->ixy[1]);
      }
  }
  
  /*for(int i=0; i < frontier_elements.size(); i++){
      FrontierClusterElement *fc1 = frontier_elements[i];
      fprintf(stderr, "%d - %d\n", i, fc1->cluster);
      }*/

  //cluster these 
  //return utility;
  delete utility;
  return frontier_map;
}

occ_map::FloatPixelMap * ExpUtils::createUtilityMap(const occ_map::FloatPixelMap * pixmap, const double start[2])
{
  if (pixmap->readValue(start) > 0) {
    fprintf(stderr, "ERROR: start point is in an obstacle!\n");
    return NULL;
  }

  occ_map::FloatPixelMap * utility = new occ_map::FloatPixelMap(pixmap->xy0, pixmap->xy1, pixmap->metersPerPixel, -1);

  int ixy[2] = { 0 };
  utility->worldToTable(start, ixy);
  utility->writeValue(ixy, 0);
  std::priority_queue<FrontierElement, std::vector<FrontierElement>, FrontierCompare> frontier;
  frontier.push(FrontierElement(0, ixy));

  int count = 0;
  while (!frontier.empty()) {
      count++;

      const FrontierElement f = frontier.top();

      assert(pixmap->readValue(f.ixy) < UNK_THRESH);
      //add all the neighbors //TODO: do we care about more neighbors?
      int ixyn[2] = { 0 };
      for (int i = -1; i <= 1; i++) {
          ixyn[0] = f.ixy[0] + i;
          for (int j = -1; j <= 1; j++) {
              ixyn[1] = f.ixy[1] + j;
              if (i == 0 && j == 0)
                  continue;
              if (!utility->isInMap(ixyn) || utility->readValue(ixyn) >= 0)
                  continue;
              if (pixmap->readValue(ixyn) < UNK_THRESH) {
                  float dist = sqrt(i * i + j * j);
                  frontier.push(FrontierElement(f.value + dist, ixyn));
                  // set a high value for the neighbor so that it doesn't get pushed
                  // on to the frontier again
                  utility->writeValue(ixyn, 1e9);
              }
              else if (pixmap->readValue(ixyn) > OCC_THRESH) {
                  utility->writeValue(ixyn, INFINITY);
              }
              else {
                  utility->writeValue(ixyn, INFINITY);
              }
          }
      }
      
      utility->writeValue(f.ixy, f.value);
      
      frontier.pop();
      
      if (count % 100000 == 0) {
          printf("dp_entry #%d, frontier size=%jd of %d\n", count, frontier.size(), pixmap->num_cells);
      }
  }

  return utility;
}
