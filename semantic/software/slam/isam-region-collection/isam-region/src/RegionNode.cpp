#include "SlamGraph.hpp"
#include "Utils.hpp" 
using namespace Utils;

RegionNode::RegionNode(int _region_id, bool _use_factor_graph, LabelInfo *info):use_factor_graph(_use_factor_graph){
    region_id = _region_id;
    labeldist = new LabelDistribution();
    mean = Pose2d();
    mean_node = NULL;
    //region ids also start from 0 - this will collide with the node ids - for the factors 
    //maybe we should add a prior to this - since this won't get observations always 
    active_region = false;
    //add type to label factor 
    label_info = NULL;
    region_type_to_label = NULL;
    if(info){
        label_info = info;
        //fill this from the label info
        //int no_types = info->type_to_index.size();
        //int no_labels = info->label_to_index.size();
        //fprintf(stderr, "No of labels : %d - No of Types : %d\n", no_types, no_labels);
        
        region_type = info->getRegionTypeVar(getRegionNodeSemanticID());//new SemVar(getRegionNodeSemanticID(), no_types); 

        region_appearance = info->getAppearenceVar(getRegionNodeAppearenceID());
        //fill the labels from the LabelInfo 
        region_label = info->getRegionLabelVar(getRegionLabelSemanticID());//new SemVar(getRegionLabelSemanticID(), no_labels);
        
        //
        region_type_given_appearance = info->getRegionTypeToAppearenceFactor(region_type, region_appearance);
        //create a region type to region label factor 
        region_type_to_label = info->getRegionTypeToLabelFactor(region_type, region_label);
        //not sure if we should have this 
        label_prior = info->getRegionLabelPriorFactor(region_label);
    }
    else{
        fprintf(stderr, "Error - No LabelInfo found\n");
    }
    ptr_global = NULL;
    convex_ptr = NULL;
    //points for the raytraced 
    ptr_raytraced_local = NULL;
    ptr_raytraced_global = NULL;
    ptr_raytraced_convex = NULL;
    ptr_raytraced_convex_global = NULL;
        
    //this is a simple polygon - a cube 
    int no_points = 4;
    double width = 1.0;
    ptr_simple = pointlist2d_new (no_points);//slam_pose->scan->numPoints);
    ptr_simple->points[0].x = -width;
    ptr_simple->points[0].y = -width;

    ptr_simple->points[1].x = width;
    ptr_simple->points[1].y = -width;

    ptr_simple->points[2].x = width;
    ptr_simple->points[2].y = width;

    ptr_simple->points[3].x = -width;
    ptr_simple->points[3].y = width;

    ptr_simple_global = NULL;
}

RegionNode::RegionNode(int _region_id, SlamNode *node, bool _use_factor_graph, LabelInfo *info):use_factor_graph(_use_factor_graph){
    region_id = _region_id;
    nodes.push_back(node);
    node->updateRegion(this);
    active_region = false;
    labeldist = new LabelDistribution();
    mean = Pose2d();
    mean_node = NULL;
    label_info = NULL;
    if(info){
        label_info = info;
        //fill this from the label info
        region_type = info->getRegionTypeVar(getRegionNodeSemanticID());//new SemVar(getRegionNodeSemanticID(), no_types); 

        region_appearance = info->getAppearenceVar(getRegionNodeAppearenceID());

        region_label = info->getRegionLabelVar(getRegionLabelSemanticID());//new SemVar(getRegionLabelSemanticID(), no_labels);
        
        region_type_given_appearance = info->getRegionTypeToAppearenceFactor(region_type, region_appearance);
        //create a region type to region label factor 
        region_type_to_label = info->getRegionTypeToLabelFactor(region_type, region_label);

        label_prior = info->getRegionLabelPriorFactor(region_label);
    }
    else{
        
    }

    ptr_global = NULL;
    convex_ptr = NULL;

    //points for the raytraced 
    ptr_raytraced_local = NULL;
    ptr_raytraced_global = NULL;
    ptr_raytraced_convex = NULL;
    ptr_raytraced_convex_global = NULL;
    //this is a simple polygon - a cube 
    int no_points = 4;
    ptr_simple = pointlist2d_new (no_points);//slam_pose->scan->numPoints);
    ptr_simple->points[0].x = -2;
    ptr_simple->points[0].y = -2;

    ptr_simple->points[1].x = 2;
    ptr_simple->points[1].y = -2;

    ptr_simple->points[2].x = 2;
    ptr_simple->points[2].y = 2;

    ptr_simple->points[3].x = -2;
    ptr_simple->points[3].y = 2;

    ptr_simple_global = NULL;
}

void RegionNode::setActive(){
    active_region = true;
}

void RegionNode::setFinished(){
    active_region = false;
}

bool RegionNode::getActive(){
    return active_region; 
}

pointlist2d_t *RegionNode::getRegionBoundingbox(){
    Pose2d tocheck_value = mean_node->getPose();

    double dist =0;
    double pos[2], next_pos[2];
    double start[2];


    // Node->Slam_Pose->Scan laser scans are decimated and in the body frame
    // We will generate the local occupancy grid in the Query Body frame
    BotTrans queryBodyToSelf = getBotTransFromPose(Pose2d(0,0,0));
    
    BotTrans queryBodyToLocal;
    
    BotTrans localToQueryBody;
    queryBodyToLocal.trans_vec[0] = tocheck_value.x();
    queryBodyToLocal.trans_vec[1] = tocheck_value.y();
    queryBodyToLocal.trans_vec[2] = 0.0;
    double rpyQueryBody[3] = {0.0, 0.0, tocheck_value.t()};
    bot_roll_pitch_yaw_to_quat (rpyQueryBody, queryBodyToLocal.rot_quat);

    localToQueryBody = queryBodyToLocal;
    bot_trans_invert (&localToQueryBody);

    BotTrans matchBodyToLocal;
    //BotTrans matchLaserToLocal;
    BotTrans matchBodyToQueryBody;

    double rpyMatchBody[3] = {0.0, 0.0, 0.0};
    //this breaks down - if we use the rear laser as all the points are jumbled together 
    double laserStartMatchBody[3] = {0,0,0};//{0.4460, -0.0101, 0.1};
    double laserStartQueryBody[3];
    double pMatchBody[3] =  {0.0, 0.0, 0.0};
    double pMatchBodyNext[3] = {0.0, 0.0, 0.0};
    double pQueryBody[3];
    double pQueryBodyNext[3];

    // Find the nodes (and scans) near the local search area
    //for each node - get circle surrounding the node (out to mabye 3m ?) 
    //add this to the point list 
    //then find the convex hull of it 

    double max_scan_distance = 2.0;
    
    vector<point2d_t> scan_points;
    
    //the other option is to grow the region and then find the boundary (bounding box) 
    
    for(int k = 0; k < nodes.size(); k++){
        SlamNode *nd = nodes[k];
        Pose2d value = nd->getPose();

        //loop through the points and add
        ScanTransform T;
        T.x = value.x();
        T.y = value.y();
        T.theta = value.t();
        
        Pose2d match = nd->getPose();
        
        // Determine the transform from Match Body ---> Query Body
        
        // Set the BotTrans from Match BODY to LOCAL
        matchBodyToLocal.trans_vec[0] = match.x();
        matchBodyToLocal.trans_vec[1] = match.y();
        matchBodyToLocal.trans_vec[2] = 0.0;
        rpyMatchBody[2] = match.t();
        bot_roll_pitch_yaw_to_quat (rpyMatchBody, matchBodyToLocal.rot_quat);

        // ... and now Match Body ---> Query Body
        bot_trans_apply_trans_to (&localToQueryBody, &matchBodyToLocal, &matchBodyToQueryBody);
        
        Scan *scan = nd->slam_pose->scan;
        
        float clamp[2] = {0,1.0} ;
        float ray_trace_clamp[2] = {-11.0, 6};
        
        // Transform Match Laser (0,0,0) ---> Query Laser frame
        bot_trans_apply_vec ( &matchBodyToQueryBody, laserStartMatchBody, laserStartQueryBody);
        start[0] = laserStartQueryBody[0];
        start[1] = laserStartQueryBody[1];
        
        for (unsigned i = 0; i < scan->numPoints; i++) {
            point2d_t pt;
            
            if(hypot(scan->points[i].x, scan->points[i].y) > max_scan_distance){
                double t = atan2(scan->points[i].y, scan->points[i].x);
                pMatchBody[0] = max_scan_distance*cos(t);
                pMatchBody[1] = max_scan_distance*sin(t);
            }
            else{            
                pMatchBody[0] = scan->points[i].x;
                pMatchBody[1] = scan->points[i].y;
            }

            //bot_trans_apply_vec ( &matchBodyToQueryBody, pMatchBody, pQueryBody);
            bot_trans_apply_vec ( &matchBodyToLocal, pMatchBody, pQueryBody);
            pos[0] = pQueryBody[0];
            pos[1] = pQueryBody[1];
            
            pt.x = pos[0];
            pt.y = pos[1];
            scan_points.push_back(pt);
            //local_px_map->rayTrace(start, pos, -1, 2, ray_trace_clamp);
        }            
    }

    pointlist2d_t *scans = pointlist2d_new((int) scan_points.size());
    for(int i=0; i < scan_points.size(); i++){
        scans->points[i].x = scan_points[i].x;
        scans->points[i].y = scan_points[i].y;
        //fprintf(stderr, "%f,%f\n", scan_points[i].x, scan_points[i].y);
    }
    //fprintf(stderr, "Size : %d\n", (int) scan_points.size());

    //    BotTrans bodyToLocal;
    
    //return scans;
    pointlist2d_t *convex_scans = convexhull_graham_scan_2d(scans);
    pointlist2d_free(scans);
    return convex_scans;//scans;
}

void RegionNode::printLabelDistribution(){
    cout << "Label Distribution : " << region_id << endl; 
    map<int, double>::iterator it; 
    for(it = region_label_dist.begin(); it != region_label_dist.end(); it++){
        cout << "\t" << label_info->getLabelNameFromID(it->first) << " : " << it->second << endl;
    }
    cout << endl; 
}

bool RegionNode::getMaxLabelAndLikelihood(string &label, double &prob){
    if(region_label_dist.size() == 0){
        label = string("unknown");
        prob = 0;
        return false;
    }
    prob = 0;
    double max_prob = 0; 
    map<int, double>::iterator it; 
    for(it = region_label_dist.begin(); it != region_label_dist.end(); it++){
        if(it->second > max_prob){
            label = label_info->getLabelNameFromID(it->first);
            max_prob = it->second;
        }
    }
    prob = max_prob;
    return true;
}

double RegionNode::getProbabilityOfLabel(string landmark_label){
    if(use_factor_graph){
        //printLabelDistribution();
        int label_index = label_info->getIndexForLabel(landmark_label);
    
        //we should get the likelihood of generating this label given its current label 
        //or the likelihood of being the landmark given label distribution 
        //this would invlove the prob of being the landmark given label 
        //marginalizing out the label 
    

        //for now lets just return the likelihood for the given label 
    
        //fprintf(stderr, "Region : %d Index of label : %d Distribution of size : %d\n", region_id, label_index, (int) region_label_dist.size());

        map<int, double>::iterator it; 
        it = region_label_dist.find(label_index); 

        if(it == region_label_dist.end())
            return 0; 

        double prob = 0;

        for(it = region_label_dist.begin(); it != region_label_dist.end(); it++){
            string label = label_info->getLabelNameFromID(it->first);
            prob += it->second * label_info->getProbabilityOfLandmarkGivenLabel(label, landmark_label);
        }
        return prob;
    }
    else{
        map<int, double> label_dist = labeldist->getProbabilities();
        int label_index = labeldist->getLabelID(landmark_label);
        map<int, double>::iterator it; 
        it = label_dist.find(label_index); 

        if(it != label_dist.end()){
            return it->second;
        }
        else{
            return 0;
        }
    }
}

//Get probability of label prior (without language) 
double RegionNode::getProbabilityOfLabelPrior(string landmark_label){
    if(use_factor_graph){
        //printLabelDistribution();
        int label_index = label_info->getIndexForLabel(landmark_label);
    
        //we should get the likelihood of generating this label given its current label 
        //or the likelihood of being the landmark given label distribution 
        //this would invlove the prob of being the landmark given label 
        //marginalizing out the label 
    

        //for now lets just return the likelihood for the given label 
    
        //fprintf(stderr, "Region : %d Index of label : %d Distribution of size : %d\n", region_id, label_index, (int) region_label_dist.size());

        map<int, double>::iterator it; 
        it = region_label_dist_no_lang.find(label_index); 

        if(it == region_label_dist_no_lang.end())
            return 0; 

        double prob = 0;

        for(it = region_label_dist_no_lang.begin(); it != region_label_dist_no_lang.end(); it++){
            string label = label_info->getLabelNameFromID(it->first);
            prob += it->second * label_info->getProbabilityOfLandmarkGivenLabel(label, landmark_label);
        }
        return prob;
    }
    else{
        //this is broken 
        map<int, double> label_dist = labeldist->getProbabilities();
        int label_index = labeldist->getLabelID(landmark_label);
        map<int, double>::iterator it; 
        it = label_dist.find(label_index); 

        if(it != label_dist.end()){
            return it->second;
        }
        else{
            return 0;
        }
    }
}

vector<Point> RegionNode::getLandmarkPoints(){
    vector<Point> lm_points; 
    if(!ptr_raytraced_convex_global){
        updateRayTracedPoints();
        if(!ptr_raytraced_convex){
            fprintf(stderr, "No raytraced points\n");
        }
    }
    
    if(ptr_raytraced_convex_global){
        pointlist2d_t *points = ptr_raytraced_convex_global;
        
        double xyz[3] = {0,0,0};
        for(int j=0; j< points->npoints; j++){
            xyz[0] = points->points[j].x;
            xyz[1] = points->points[j].y;
            Point pt(xyz);
            lm_points.push_back(pt);
        }  
    }
    else{
        fprintf(stderr, "No convex raytraced points\n");
    }

    return lm_points; 
}

void RegionNode::addNewObservationFactor(SlamNode *node){

    SemFactor *region_appearance_to_node_type = label_info->getRegionAppearenceToAppearenceFactor(region_appearance, node->node_type);
    prob_type_given_observation.insert(make_pair(make_pair(semantic_id, node->id), region_appearance_to_node_type));
}

void RegionNode::removeObservationFactor(SlamNode *node){
    map<nodePairKey, SemFactor *>::iterator it;
    it = prob_type_given_observation.find(make_pair(semantic_id, node->id));
    if(it != prob_type_given_observation.end()){
        SemFactor *to_remove = it->second;
        prob_type_given_observation.erase(it);
        delete to_remove;
    }
}

//these points are in the local reference frame - around the mean node (lets hope the mean node doesn't change once a region is updated) 
void RegionNode::insertRayTracedPoints(pointlist2d_t *points){    
    if(ptr_raytraced_local != NULL){        
        pointlist2d_free(ptr_raytraced_local);
    }

    if(ptr_raytraced_global != NULL){
        pointlist2d_free(ptr_raytraced_global);
        ptr_raytraced_global = NULL;
    }
    
    if(ptr_raytraced_convex != NULL){        
        pointlist2d_free(ptr_raytraced_convex);
    }
    
    if(ptr_raytraced_convex_global != NULL){        
        pointlist2d_free(ptr_raytraced_convex_global);
        ptr_raytraced_convex_global = NULL;
    }

    ptr_raytraced_local = pointlist2d_new_copy(points);
    
    ptr_raytraced_convex = convexhull_graham_scan_2d(ptr_raytraced_local);
}

//map<int, SlamObject*> observed_objects;
void RegionNode::removeObject(SlamObject *obj){
    map<int, SlamObject*>::iterator it; 
    it = observed_objects.find(obj->id);
    if(it != observed_objects.end()){
        obj->removeAssignedRegion();
        observed_objects.erase(it);
    }
}

int RegionNode::getSize(){
    return nodes.size();
}

void RegionNode::removeAllObjects(){
    map<int, SlamObject*>::iterator it; 
    for(it = observed_objects.begin(); it != observed_objects.end(); it++){
        removeObject(it->second);
    }
}

void RegionNode::addObject(SlamObject *obj){
    map<int, SlamObject*>::iterator it; 
    it = observed_objects.find(obj->id);
    if(it == observed_objects.end()){
        observed_objects.insert(make_pair(obj->id, obj));
    }
    else{
        fprintf(stderr, "This should not happen - object is already in the list\n");
        exit(-1);
        it->second = obj;
    }
}


void RegionNode::updateRayTracedPoints(){
    if(ptr_raytraced_global == NULL && ptr_raytraced_local != NULL && ptr_raytraced_local->npoints){
        ptr_raytraced_global = pointlist2d_new(ptr_raytraced_local->npoints);
    }

    if(ptr_raytraced_convex_global == NULL && ptr_raytraced_convex != NULL && ptr_raytraced_convex->npoints){
        ptr_raytraced_convex_global = pointlist2d_new(ptr_raytraced_convex->npoints);
    }
    
    if(ptr_simple_global == NULL && ptr_simple != NULL && ptr_simple->npoints){
        ptr_simple_global = pointlist2d_new(ptr_simple->npoints);
    }

    BotTrans local_to_global = getBotTransFromPose(mean_node->getPose());

    double localPos[3] = {0};
    double globalPos[3] = {0};
    
    if(ptr_raytraced_local){
        for(int i=0; i < ptr_raytraced_local->npoints; i++){
            localPos[0] = ptr_raytraced_local->points[i].x;
            localPos[1] = ptr_raytraced_local->points[i].y;
            bot_trans_apply_vec ( &local_to_global, localPos, globalPos);
            ptr_raytraced_global->points[i].x = globalPos[0];
            ptr_raytraced_global->points[i].y = globalPos[1];
        }
    }
    if(ptr_raytraced_convex){
        //do the same for convexhull points 
        for(int i=0; i < ptr_raytraced_convex->npoints; i++){
            localPos[0] = ptr_raytraced_convex->points[i].x;
            localPos[1] = ptr_raytraced_convex->points[i].y;
            bot_trans_apply_vec ( &local_to_global, localPos, globalPos);
            ptr_raytraced_convex_global->points[i].x = globalPos[0];
            ptr_raytraced_convex_global->points[i].y = globalPos[1];
        }
        //fprintf(stderr, "\n\n\n Updated Bounding box\n\n\n");
    }
    if(ptr_simple){
        for(int i=0; i < ptr_simple->npoints; i++){
            localPos[0] = ptr_simple->points[i].x;
            localPos[1] = ptr_simple->points[i].y;
            bot_trans_apply_vec ( &local_to_global, localPos, globalPos);
            ptr_simple_global->points[i].x = globalPos[0];
            ptr_simple_global->points[i].y = globalPos[1];
        }
    }
}


void RegionNode::updateBoundingBox(){

    if(ptr_global != NULL){
        pointlist2d_free(ptr_global);
    }

    if(convex_ptr != NULL){
        pointlist2d_free(convex_ptr);
    }

    int total_no_points = 0;
    for(int i=0; i < nodes.size(); i++){
        SlamNode *nd = nodes[i];
        nd->updateScanPoints();
        total_no_points += nd->ptr_global->npoints;
    }
    
    if(total_no_points < 2)
        return;

    ptr_global = pointlist2d_new(total_no_points);

    int k =0;
    for(int i=0; i < nodes.size(); i++){
        SlamNode *nd = nodes[i];
        for(int j =0; j < nd->ptr->npoints; j++){//iterate over smpoints
            ptr_global->points[k].x = nd->ptr_global->points[j].x; 
            ptr_global->points[k].y = nd->ptr_global->points[j].y; 
            k++;
        }
    }
    convex_ptr = convexhull_graham_scan_2d(ptr_global);
}

int RegionNode::getRegionNodeSemanticID(){
    return REGION_FACTOR_ID_INCREMENT + region_id;
}

int RegionNode::getRegionNodeAppearenceID(){
    return REGION_APPEARENCE_ID_INCREMENT + region_id;
}

int RegionNode::getRegionLabelSemanticID(){
    return REGION_LABEL_ID_INCREMENT + region_id;
}

SlamNode* RegionNode::getLastNode(){
    if(nodes.size() > 0)
        return nodes[nodes.size()-1];
    return NULL;
}

int RegionNode::addNode(SlamNode *node){
    nodes.push_back(node);   
    addNewObservationFactor(node);
    node->updateRegion(this);
    return 0;
}

void RegionNode::addSegment(RegionSegment *segment){
    segments.push_back(segment);   
    segment->region = this;
}

//get the ratio of odometry edges vs scanmatch edges 
double RegionNode::getOdometryRatio(int *odom_edge_count, int *sm_edge_count){
    double odom_ratio = 0; 
    int valid_edges = 0;
    *odom_edge_count = 0;
    *sm_edge_count = 0;
    map<int, SlamConstraint *>::iterator it;
    for(int i=0; i < nodes.size() - 1; i++){
        SlamNode *nd = nodes[i];
        SlamNode *nd_n = nd->next_node;

        if(nd_n == NULL){
            fprintf(stderr, "Next node NULL - this should not have happend\n");
            continue;
        }
        it = nd->constraints.find(nd_n->id);
        if(it == nd->constraints.end()){
            //fprintf(stderr, "Constraint not found\n");
            continue;
        }
        
        if(it->second->type == SLAM_GRAPH_EDGE_T_TYPE_ODOM_INC){
            *odom_edge_count = *odom_edge_count +1;
        }
        else if(it->second->type == SLAM_GRAPH_EDGE_T_TYPE_SM_INC){
            *sm_edge_count = *sm_edge_count + 1;
        }
        valid_edges++;
    }
    
    return *odom_edge_count / (double) valid_edges;
}


RegionEdge* RegionNode::addEdge(RegionNode *region2){
    RegionEdge *edge = new RegionEdge(this, region2);
    edges.insert(make_pair(region2->region_id , edge));
    return edge;
}

bool RegionNode::isConnected(RegionNode *region2){
    if(this == region2){
        return true;
    }

    RegionEdge *edge = getEdge(region2);
    if(edge == NULL){
        return false;
    }
    if(edge->edge_list.size() == 0){
        return false;
    }
    return true;
}

RegionEdge* RegionNode::addEdge(RegionNode *region2, SlamConstraint *ct){
    RegionEdge *edge = getEdge(region2);
    
    if(edge == NULL){
        edge = new RegionEdge(this, region2);
        edges.insert(make_pair(region2->region_id , edge));
    }
    edge->addEdge(ct);
    return edge;
}

RegionEdge* RegionNode::getEdge(RegionNode *region2){
    map<int, RegionEdge*>::iterator it;
    it = edges.find(region2->region_id);
    if(it == edges.end()){
        return NULL;
    }
    return it->second;
}

int RegionNode::deleteEdge(RegionNode *region2){
    map<int, RegionEdge*>::iterator it;
    it = edges.find(region2->region_id);
    if(it == edges.end()){
        return 0;
    }
    delete it->second;
    edges.erase(it);
    return 0;
}

double RegionNode::calculateCloseNodes(RegionNode *region){
    //for each node in this region calculate the closest node
    //calculate the ratio of close node pairs to total pairs 

    vector<nodePairDist> close_node_pairs1;
    vector<nodePairDist> close_node_pairs2;

    double close_ratio1 = calculateCloseNodeRatio(nodes, region->nodes, close_node_pairs1);
    double close_ratio2 = calculateCloseNodeRatio(region->nodes, nodes, close_node_pairs2);

    return fmax(close_ratio1,close_ratio2);    
}

int RegionNode::removeNode(SlamNode *node){
    //iterate through the vector and remove the node from it 
    //otherwise regions and slam nodes will have inconsistant assignments 
    for(size_t i=0; i < nodes.size(); i++){
        if(nodes[i]->id== node->id){
            //remove from the list
            //we should set the regions of these nodes as NULL ?? 
            node->region_node = NULL;
            nodes.erase(nodes.begin() + i);
            break;
        }
    } 
    removeObservationFactor(node);
    return 0;
}

bool RegionNode::isIncrementalEdge(RegionNode *region){
    RegionEdge *edge = getEdge(region);

    if(edge->type == REGION_EDGE_INCREMENTAL)
        return true;
    return false;
}

void RegionNode::pruneRegionEdges(bool v){
    if(v)
        fprintf(stderr, CYAN "=== Region : %d ===\n", region_id);
    map<int, RegionEdge *>::iterator it;
    map<int, RegionEdge *> valid_region_edges; 

    for(it = edges.begin(); it!= edges.end(); it++){
        RegionEdge *eg = it->second;
        if(eg->edge_list.size() >0){
            if(v)
                fprintf(stderr, "\t ===> Region %d\n", it->second->node2->region_id);//r_node2_id);
            map<int, SlamConstraint *>::iterator m_it;
            map<int, SlamConstraint *> valid_edges;
            for(m_it = eg->edge_list.begin(); m_it != eg->edge_list.end(); m_it++){
                SlamConstraint *ct = m_it->second;
                if(v)
                    fprintf(stderr, "\t\t %d [%d] - %d [%d]\n", ct->node1->id, ct->node1->region_node->region_id, 
                            ct->node2->id, ct->node2->region_node->region_id);

                int valid = 0;
                if(ct->node1->region_node != NULL && ct->node2->region_node != NULL){
                    if(region_id == ct->node1->region_node->region_id && it->second->node2->region_id == ct->node2->region_node->region_id){
                        valid = 1;
                    }
                    if(region_id == ct->node2->region_node->region_id && it->second->node2->region_id == ct->node1->region_node->region_id){
                        valid = 1;
                    }
                }
                if(valid){
                    if(v)
                        fprintf(stderr, "Edge: Valid\n");
                    valid_edges.insert(make_pair(ct->id, ct));
                }                
                else{
                    if(v)
                        fprintf(stderr, "Edge: Invalid\n");
                }
            }
            eg->edge_list = valid_edges;
        }

        if(eg->edge_list.size() > 0){
            if(v)
                fprintf(stderr, "Edge should be kept\n");            
            valid_region_edges.insert(make_pair(eg->node2->region_id, eg));
        }
        else{
            delete eg;
        }
    }

    edges = valid_region_edges; 
    if(v) 
        fprintf(stderr, "--------------------\n" RESET_COLOR);
}

vector<RegionNode *> RegionNode::getConnectedRegions(){
    vector<RegionNode *> connected_regions; 
    map<int, RegionEdge *>::iterator it;
    for(it = edges.begin(); it!= edges.end(); it++){
        RegionEdge *eg = it->second;
        //fprintf(stderr, RED "Connected Region %d\n" RESET_COLOR, eg->node2->region_id);
        if(eg->edge_list.size() >0){
            connected_regions.push_back(eg->node2);
        }
    }
    return connected_regions; 
}

void RegionNode::printRegionEdges(){
    fprintf(stderr, YELLOW "=== Region : %d ===\n", region_id);
    map<int, RegionEdge *>::iterator it;
    for(it = edges.begin(); it!= edges.end(); it++){
        RegionEdge *eg = it->second;
        if(eg->edge_list.size() >0){
            fprintf(stderr, "\t ===> Region %d\n", it->second->node2->region_id);
            RegionEdge *eg = it->second;
            map<int, SlamConstraint *>::iterator m_it;
            for(m_it = eg->edge_list.begin(); m_it != eg->edge_list.end(); m_it++){
                SlamConstraint *ct = m_it->second;
                fprintf(stderr, "\t\t (%d) %d [%d] - %d [%d]\n", ct->id, ct->node1->id, ct->node1->region_node->region_id, 
                        ct->node2->id, ct->node2->region_node->region_id);
            }
        }
    }
    fprintf(stderr, "--------------------\n" RESET_COLOR);
}

void RegionNode::updateMean(){
    double mean_xyt[3] = {0};
    
    for(int j=0; j < nodes.size(); j++){
        SlamNode *c_node = nodes[j];
        Pose2d pose = c_node->getPose();
        mean_xyt[0] += pose.x();
        mean_xyt[1] += pose.y();
        mean_xyt[2] += pose.t();
    }
    mean_xyt[0] /= nodes.size();
    mean_xyt[1] /= nodes.size();
    mean_xyt[2] /= nodes.size();

    mean.set_x(mean_xyt[0]);
    mean.set_y(mean_xyt[1]);
    mean.set_t(mean_xyt[2]);

    double min_dist;
    mean_node = getClosestNode(mean, &min_dist);    
}

SlamNode *RegionNode::getClosestNode(Pose2d pose, double *min_dist){
    *min_dist = 1000000;
    SlamNode *closest_node = NULL;
    for(int j=0; j < nodes.size(); j++){
        SlamNode *c_node = nodes[j];
        Pose2d delta = c_node->getPose().ominus(pose);
        double dist = hypot(delta.x(), delta.y());

        if(dist < *min_dist){
            *min_dist = dist;
            closest_node = c_node;
        }
    }
    return closest_node;
}

//this does not fill the pointer properly 
double RegionNode::getClosestDistanceToRegion(RegionNode *q_region, SlamNode *c_node, SlamNode *q_node){
    c_node = NULL, q_node = NULL;
    double min_dist = 1000000;
    for(int i=0; i < nodes.size(); i++){
        SlamNode *c_nd = nodes[i];
        //fprintf(stderr, "Node : %d (%d) \n", c_nd->id, c_nd->region_node->region_id);
        for(int j=0; j < q_region->nodes.size(); j++){
            SlamNode *q_nd = q_region->nodes[j];
            
            Pose2d delta = c_nd->getPose().ominus(q_nd->getPose());
            double dist = hypot(delta.x(), delta.y());
            //fprintf(stderr, "\tNode : %d (%d) - %f\n", q_nd->id, q_nd->region_node->region_id, dist);
            if(dist < min_dist){
                min_dist = dist;
                c_node = c_nd;
                q_node = q_nd;
            }
        }
    }
    fprintf(stderr, "%d - %d\n", c_node->id, q_node->id);
    return min_dist;
}

//this does not fill the pointer properly 
double RegionNode::getMeanDistanceToRegion(RegionNode *q_region, vector<pair<SlamNode *, SlamNode*> > &closest_nodes, 
                                           int *c_node_id, int *q_node_id){
    *c_node_id = -1, *q_node_id = -1;
    double avg_dist = 0;
    double overall_min_dist = 10000;
    //for each node in this region - find the closest node in the other region 
    //add this to the sum distance and then average it 
    for(int i=0; i < nodes.size(); i++){
        SlamNode *c_nd = nodes[i];
        double min_dist = 100000;
        SlamNode *closest_node = NULL;
        //fprintf(stderr, "Node : %d (%d) \n", c_nd->id, c_nd->region_node->region_id);
        for(int j=0; j < q_region->nodes.size(); j++){
            SlamNode *q_nd = q_region->nodes[j];
            
            Pose2d delta = c_nd->getPose().ominus(q_nd->getPose());
            double dist = hypot(delta.x(), delta.y());
            //fprintf(stderr, "\tNode : %d (%d) - %f\n", q_nd->id, q_nd->region_node->region_id, dist);
            if(dist < overall_min_dist){
                overall_min_dist = dist;
                *c_node_id = c_nd->id;
                *q_node_id = q_nd->id;
            }
            if(dist < min_dist){
                min_dist = dist;
                closest_node = q_nd;
            }
        }
        closest_nodes.push_back(make_pair(c_nd, closest_node));
        //fprintf(stderr, "\tClosest Pair : %d - %d => %f\n", c_nd->id, closest_node->id, min_dist);
        avg_dist += min_dist;
    }

    avg_dist /= (double) nodes.size();
    //fprintf(stderr, "Closest Nodes %d [%d] - %d [%d]\n", *c_node_id, region_id, *q_node_id, q_region->region_id);
    //fprintf(stderr, "Avg dist : %f\n", avg_dist);
    return avg_dist;
}

//this does not fill the pointer properly 
/*double RegionNode::getMeanDistanceToRegion(RegionNode *q_region, vector<pair<SlamNode *, SlamNode*> > &closest_nodes, 
                                           int *c_node_id, int *q_node_id){
    *c_node_id = -1, *q_node_id = -1;
    double avg_dist = 0;
    double overall_min_dist = 10000;
    //for each node in this region - find the closest node in the other region 
    //add this to the sum distance and then average it 
    SlamNode *c_nd = mean_node;
    SlamNode *q_nd = q_region->mean_node;
    Pose2d delta = c_nd->getPose().ominus(q_nd->getPose());

    double dist = hypot(delta.x(), delta.y());
    *c_node_id = c_nd->id;
    *q_node_id = q_nd->id;
    
    return dist;    
}*/

//this does not fill the pointer properly 
double RegionNode::getMeanDistanceToRegion(RegionNode *q_region, int *c_node_id, int *q_node_id){
    *c_node_id = -1, *q_node_id = -1;
    double avg_dist = 0;
    double overall_min_dist = 10000;
    //for each node in this region - find the closest node in the other region 
    //add this to the sum distance and then average it 
    for(int i=0; i < nodes.size(); i++){
        SlamNode *c_nd = nodes[i];
        double min_dist = 100000;
        SlamNode *closest_node = NULL;
        //fprintf(stderr, "Node : %d (%d) \n", c_nd->id, c_nd->region_node->region_id);
        for(int j=0; j < q_region->nodes.size(); j++){
            SlamNode *q_nd = q_region->nodes[j];
            
            Pose2d delta = c_nd->getPose().ominus(q_nd->getPose());
            double dist = hypot(delta.x(), delta.y());
            //fprintf(stderr, "\tNode : %d (%d) - %f\n", q_nd->id, q_nd->region_node->region_id, dist);
            if(dist < overall_min_dist){
                overall_min_dist = dist;
                *c_node_id = c_nd->id;
                *q_node_id = q_nd->id;
            }
            if(dist < min_dist){
                min_dist = dist;
                closest_node = q_nd;
            }
        }
        
        //fprintf(stderr, "\tClosest Pair : %d - %d => %f\n", c_nd->id, closest_node->id, min_dist);
        avg_dist += min_dist;
    }

    avg_dist /= (double) nodes.size();
    //fprintf(stderr, "Closest Nodes %d [%d] - %d [%d]\n", *c_node_id, region_id, *q_node_id, q_region->region_id);
    //fprintf(stderr, "Avg dist : %f\n", avg_dist);
    return avg_dist;
}

//this does not fill the pointer properly 
double RegionNode::getMeanDistanceFromNode(SlamNode *node, int *c_node_id){
    *c_node_id = -1;
    double avg_dist = 0;
    double min_dist = 10000;
    //for each node in this region - find the closest node in the other region 
    //add this to the sum distance and then average it 
    for(int i=0; i < nodes.size(); i++){
        SlamNode *c_nd = nodes[i];
        Pose2d delta = c_nd->getPose().ominus(node->getPose());
        double dist = hypot(delta.x(), delta.y());
        //fprintf(stderr, "\tNode : %d (%d) - %f\n", q_nd->id, q_nd->region_node->region_id, dist);
        if(dist < min_dist){
            min_dist = dist;
            *c_node_id = c_nd->id;
        }
        //fprintf(stderr, "\tClosest Pair : %d - %d => %f\n", c_nd->id, closest_node->id, min_dist);
        avg_dist += dist;
    }

    avg_dist /= (double) nodes.size();
    //fprintf(stderr, "Closest Nodes %d [%d] - %d [%d]\n", *c_node_id, region_id, *q_node_id, q_region->region_id);
    //fprintf(stderr, "Avg dist : %f\n", avg_dist);
    return avg_dist;
}

double RegionNode::getClosestDistanceToRegion(RegionNode *q_region, int *c_node_id, int *q_node_id){
    *c_node_id = -1, *q_node_id = -1;
    double min_dist = 1000000;
    for(int i=0; i < nodes.size(); i++){
        SlamNode *c_nd = nodes[i];
        //fprintf(stderr, "Node : %d (%d) \n", c_nd->id, c_nd->region_node->region_id);
        for(int j=0; j < q_region->nodes.size(); j++){
            SlamNode *q_nd = q_region->nodes[j];
            
            Pose2d delta = c_nd->getPose().ominus(q_nd->getPose());
            double dist = hypot(delta.x(), delta.y());
            //fprintf(stderr, "\tNode : %d (%d) - %f\n", q_nd->id, q_nd->region_node->region_id, dist);
            if(dist < min_dist){
                min_dist = dist;
                *c_node_id = c_nd->id;
                *q_node_id = q_nd->id;
            }
        }
    }
    //fprintf(stderr, "%d - %d\n", *c_node_id, *q_node_id);
    return min_dist;
}

void RegionNode::printRegionNodes(){
    fprintf(stderr, "Region ID : %d\n", region_id);
    for(size_t i=0; i < nodes.size(); i++){
        fprintf(stderr, "\tNode : %d - %d\n", nodes[i]->id, 
                nodes[i]->region_node->region_id);
    }    
}

//removes the nodes from the current region - same as merging region 
void RegionNode::addCurrentNodesToOtherRegion(RegionNode *region){
    for(size_t i=0; i < nodes.size(); i++){
        region->addNode(nodes[i]);
    }
    removeAllNodesFromRegion();
}

void RegionNode::removeAllNodesFromRegion(){
    for(int i=0; i < nodes.size(); i++){
        SlamNode *node = nodes[i];
        removeObservationFactor(node);
    }
    nodes.clear();
}

RegionNode::~RegionNode(){
    removeAllNodesFromRegion();
    if(ptr_global != NULL){
        pointlist2d_free(ptr_global);
    }

    if(convex_ptr != NULL){
        pointlist2d_free(convex_ptr);
    }
    if(ptr_raytraced_local != NULL){        
        pointlist2d_free(ptr_raytraced_local);
    }

    if(ptr_raytraced_global != NULL){
        pointlist2d_free(ptr_raytraced_global);
    }
    
    if(ptr_raytraced_convex != NULL){        
        pointlist2d_free(ptr_raytraced_convex);
    }
    
    if(ptr_raytraced_convex_global != NULL){        
        pointlist2d_free(ptr_raytraced_convex_global);
    }

    if(ptr_simple != NULL){        
        pointlist2d_free(ptr_simple);
    }

    if(ptr_simple_global != NULL){        
        pointlist2d_free(ptr_simple_global);
    }

    removeAllObjects();
    
    delete region_type;
    delete region_label;
    delete label_prior;
}


