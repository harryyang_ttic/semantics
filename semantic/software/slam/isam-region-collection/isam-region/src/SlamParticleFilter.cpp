#include "SlamParticleFilter.hpp"

//free function to clean up the circular buffer data 
void
circ_free_lidar_data(void *user, void *p) {
    bot_core_planar_lidar_t *np = (bot_core_planar_lidar_t *) p; 
    bot_core_planar_lidar_t_destroy(np);
}

//free function to clean up the circular buffer data 
void
circ_free_image_data(void *user, void *p) {
    bot_core_image_t *np = (bot_core_image_t *) p; 
    bot_core_image_t_destroy(np);
}


SlamParticleFilter::SlamParticleFilter(lcm_t *lcm_, int argc, char *argv[]){
    fprintf(stdout, "SlamParticle Filter created - allocating\n");
        
    prev_odom = (sm_rigid_transform_2d_t *) calloc(1, sizeof(sm_rigid_transform_2d_t)); //set initial prev_odom to zero...
    prev_sm_odom = (sm_rigid_transform_2d_t *) calloc(1, sizeof(sm_rigid_transform_2d_t)); //set initial prev_odom to zero...

    prev_added_odom = (sm_rigid_transform_2d_t *) calloc(1, sizeof(sm_rigid_transform_2d_t)); //set initial prev_odom to zero...
    finishSlam = false;
    finishSlamHeard = false;
    memset(prev_odom, 0, sizeof(sm_rigid_transform_2d_t));
    memset(prev_sm_odom, 0, sizeof(sm_rigid_transform_2d_t));
    particle_id_to_train = -1;
    last_laser_reading_utime = 0;
    complex_language_index = 0;

    requested_map_particle_id = -1;
    chan = NULL;
    rchan = NULL;
    copyNodeScans = false; //this does not work propoerly yet - need to copy the other data structures in NodeScan  
    skipOutsideSM = false;
    useICP = true;
    doSemanticClassification = true;
    weights = NULL;
    probMode = 1;
    verbose = 0;
    verbose_level = print_node_info;
    publish_map = 0;
    map_request = NULL;
    scan_request = NULL;
    noScanTransformCheck = 0;
    useLowResPrior = true;
    door_list = NULL;
    dist_lc = new set<pair<int, int> >();
    lang_lc = new set<pair<int, int> >();
    //making these default
    scanmatchBeforeAdd = 1;
    use_frames = 1;
    odometryConfidence = LINEAR_DIST_TO_ADD;
    useOdom = 1;
    slave_mode = 0;
    no_particles = 2;
    // set to default values
    validBeamAngles[0] = -100;
    validBeamAngles[1] = +100;
    beam_skip = 3;
    spatialDecimationThresh = .2;
    maxRange = 20.0;//29.7; //20.0
    maxUsableRange = 8;
    find_ground_truth = 0;
    useCopy = 0;

    sm_acceptance_threshold = 0.8;
    
    last_slampose_ind = -1;
    last_slampose_for_process_ind = -1;
    last_scan = NULL;

    node_init = NULL;
    //app->basic_graph = NULL;
    useOnlyRegion = true;
    addDummyConstraints = true;
    resample = 1;
    use_doorways = 1;
    keepDeadEdges = 0;
    publish_occupancy_maps = 0;
    useSMCov = 0;
    segmentationMethod = 1;
        
    lcm = lcm_;

    bool alignLaser = true;
    int draw_scanmatch = 0;
    

    Pose2d p1(1, 1, bot_to_radians(45));
    Pose2d p2(2, 2, bot_to_radians(45));
    
    Pose2d delta = p2.ominus(p1); //converted to p1 frame 

    cout << delta << endl;

    ignoreLanguage = 0;
   
    const char *optstring = "t:hruc:dafvmMP:wLboNODM:pn:s::ISRgC:";
    char c;
    struct option long_opts[] = { { "help", no_argument, 0, 'h' },
                                  {  "clustering_mode", required_argument, 0, 'C' },
                                  {  "icp", no_argument, 0, 'I' }, //needed
                                  { "chan", required_argument, 0, 'c' },
                                  { "no_language", required_argument, 0, 'N' },
                                  { "draw_scanmatch", no_argument, 0,'S' },
                                  { "use_doorways", no_argument, 0,'d' },
                                  { "use_fixed_cov", no_argument, 0, 'f'},
                                  { "no_low_res", no_argument, 0, 'L'},
                                  { "bad_mode", no_argument, 0,'b' },
                                  { "draw_map", no_argument, 0, 'm' },
                                  { "skip_outside_region", no_argument, 0, 'O' },
                                  { "prob_mode", required_argument, 0, 'P' },
                                  { "publish_occupancy_maps", required_argument, 0, 'M' },
                                  { "no_resample", required_argument, 0,'R' },
                                  { "strict_scanmatch", no_argument, 0,'T' },
                                  { "dummy_constraints", no_argument, 0, 'D' },    
                                  { "publish_gridmap", no_argument, 0, 'p' },
                                  { "no_particles", required_argument,   0, 'n' },
                                  { "use_odom", required_argument,   0, 'o' },
                                  { "alternate_scanmatch", no_argument, 0, 'a' },
                                  { "scanmatch", optional_argument, 0, 's' },
                                  { "wide_scan", no_argument, 0, 'w' },
                                  { "use_rear_laser", no_argument, 0, 'r' },
                                  { "verbose", no_argument, 0,'v' },
                                  { "scanmatch_threashold", required_argument, 0,'t' },
                                  { "ground_truth", no_argument, 0,'g' },
                                  { 0, 0, 0, 0 } };

    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
        switch (c) {
        case 'N':
            ignoreLanguage = 1;
            break;
        case 'r':
            rchan = strdup("SKIRT_REAR");
            break;
        case 'd':
            doSemanticClassification = false;
            break;
        case 'I':
            useICP = 1;
            break;
        case 'c':
            free(chan);
            chan = strdup(optarg);
            fprintf(stderr,"Main Laser Channel : %s\n", chan);
            break;
        case 'C':
            //useSpectralClustering = false;
            segmentationMethod = atoi(optarg);
            if(segmentationMethod == 0){
                fprintf(stderr, "Segmentation Method : Fixed Distance\n");
            }
            else if(segmentationMethod == 1){
                fprintf(stderr, "Segmentation Method : Sampling Spectral Segmentation\n");
            }
            else if(segmentationMethod == 2){
                fprintf(stderr, "Segmentation Method : Fixed Spectral Clustering\n");
            }
            else if(segmentationMethod == 3){
                fprintf(stderr, "Segmentation Method : Sampling Spectral Clustering - Use for ICRA results\n");
            }
            else{
                fprintf(stderr, "Segmentation Method : Unknown\n");
                exit(-1);
            }
            break;
        case 'f':
            useSMCov = 0;
            break;
        case 'b':
            bad_mode = 1;
            break;
        case 'n':
            no_particles = atoi(optarg);
        case 't':
            mode = 1;
            break;
        case 'o':
            scanmatchBeforeAdd = 0;
            break;
        case 'R':
            resample = 0;
            break;
        case 'L':
            useLowResPrior = false;
            break;
        case 'S':
            draw_scanmatch = 1;
            break;
        case 'O':
            skipOutsideSM = true;
            break;
        case 'w':
            useOnlyRegion = false;
            break;
        case 'm':
            sm_acceptance_threshold = atof(optarg);
            break;
        case 'v':
            verbose = 1;
            break;
        case 'p':
            publish_map = 1;
            break;
        case 's':
            scanmatchBeforeAdd = 1;
            if (optarg != NULL) {
                odometryConfidence = atof(optarg);
            }
            break;
        case 'M':
            publish_occupancy_maps = 1;
            break;
        case 'P':
            probMode = atoi(optarg);
            break;
        case 'D':
            keepDeadEdges = 1;
            break;
        case 'g':
            find_ground_truth = 1;
            no_particles = 1;
            break;
        case 'h':
        default:
            usage(argv[0]);
            break;
        }
    }

    log_path = string("semantic_output.log");
    output_writer = new OutputWriter("[Main Thread] : ", log_path);

    string output("[Main Thread] : Starting SLAM");
    output_writer->write_to_output(output, true);
    output_writer->write_to_buffer("Creating SLAM process", false, true, true);

    classifier = semantic_classifier_create(false);
    param = bot_param_new_from_server(lcm, 1);
    max_prob_particle_id = -1;
    frames = bot_frames_get_global (lcm, param);
    lcmgl_basic = bot_lcmgl_init(lcm, "slam-particle-nodes_basic");
    lcmgl_frame_test = bot_lcmgl_init(lcm, "slam-particle-farmes_test");

    lcmgl_debug = bot_lcmgl_init(lcm, "slam-particle-nodes_debug");
    //for the basic map - transfomed 
    lcmgl_sm_basic = bot_lcmgl_init(lcm, "slam-particle-nodes_sm_basic");
    //for the actual graph - that is being scanmatched for 
    lcmgl_sm_graph = bot_lcmgl_init(lcm, "slam-particle-nodes_sm_graph");
    //original scan position 
    lcmgl_sm_prior = bot_lcmgl_init(lcm, "slam-particle-nodes_sm_prior");
    //scanmatched result
    lcmgl_sm_result = bot_lcmgl_init(lcm, "slam-particle-nodes_sm_result");
    lcmgl_sm_result_low = bot_lcmgl_init(lcm, "slam-particle-nodes_sm_result_low");

    lcmgl_particles = bot_lcmgl_init(lcm, "slam-particle-nodes_particle");
    
    lcmgl_covariance = bot_lcmgl_init(lcm, "slam-particle-nodes_covariance");

    lcmgl_loop_closures = bot_lcmgl_init(lcm, "slam-particle-nodes_loop_closures");
    node_count = 0;
    last_slampose = NULL;
        
    last_added_node_id = -1; 
        
    if (chan == NULL) {
        if (alignLaser)
            chan = strdup("SKIRT_FRONT");
        else
            chan = strdup("LASER");
    }

    if(segmentationMethod == 0){
        setFixedRegionParams();
    }
    else if(segmentationMethod == 1){
        setSpectralRegionParams();
    }
    else if(segmentationMethod == 2){
        setFixedSpectralRegionParams();
    }
    else if(segmentationMethod == 3){
        setSpectralRegionParamsICRA();
    }

    setQuestionSRTypes();

    fprintf(stdout, "Using SLU Library : %d\n", config_params.useSLULibrary);

    static char buf[1024];

    //if (verbose) {
    if (alignLaser){
        //printf("INFO: Listening for robot_laser msgs on %s\n", chan);
        sprintf(buf, "INFO: Listening for robot_laser msgs on %s", chan);
        output_writer->write_to_buffer(buf);
    }
    else{
        sprintf(buf, "INFO: Listening for laser msgs on %s", chan);
        output_writer->write_to_buffer(buf);
        //printf("INFO: Listening for laser msgs on %s\n", chan);
    }
    
    sprintf(buf, "INFO: publish_map: %d", publish_map);
    output_writer->write_to_buffer(buf);
    sprintf(buf, "INFO: Max Range:%lf", maxRange);
    output_writer->write_to_buffer(buf);
    sprintf(buf, "INFO: SpatialDecimationThresh:%lf", spatialDecimationThresh);
    output_writer->write_to_buffer(buf);
    output_writer->write_buffer();
    
    /*printf("INFO: publish_map:%d\n", publish_map);
      printf("INFO: Max Range:%lf\n", maxRange);
      printf("INFO: SpatialDecimationThresh:%lf\n", spatialDecimationThresh);
      printf("INFO: Beam Skip:%d\n", beam_skip);
      printf("INFO: validRange:%f,%f\n", validBeamAngles[0], validBeamAngles[1]);*/
          
    
    
    //fill_label_index_map(app);
    //print_class_ad_labels(app);
    label_info = new LabelInfo();
    label_info_particles = new LabelInfo();
   
    //Load the SLU classifiers 
    const char *data_path = getDataPath();
    char key[200];

    sprintf(key,"natural_language_understanding.model_path"); //
       
    char *slu_path = NULL;
    
    char full_slu_path[2000]; 
    if (0 != bot_param_get_str(param, key, &slu_path)){
        fprintf(stderr, "SLU Path not defined: %s\n", key);
        output_writer->write_to_buffer("[Init] SLU Path not defined", false, true, false);                
        //fprintf(stderr, "Path : %s\n", slu_path);
        slu_classifier = NULL;
    }
    else{
        sprintf(full_slu_path, "%s/%s", data_path, slu_path);
        free(slu_path);
        fprintf(stderr, "SLU Path : %s\n", full_slu_path);
        output_writer->write_to_buffer("[Init] SLU Classifier loaded", false, true, false);                
        slu_classifier = new SLUClassifier(full_slu_path);
        fprintf(stderr, "Loaded SLU classifier\n");
    }    

    //hardcoded loop closing scan matcher params
    double metersPerPixel_lc = 0.02; //translational resolution for the brute force search
    double thetaResolution_lc = 0.01; //angular step size for the brute force search
   
    int useGradientAscentPolish_lc = 1; //use gradient descent to improve estimate after brute force search

    //Sachi - try this with resulution 5 
    int useMultires_lc = 5; // low resolution will have resolution metersPerPixel * 2^useMultiRes
    bool useThreads_lc = true;

    //hardcoded scan matcher params
    double metersPerPixel = 0.01; //translational resolution for the brute force search
    double thetaResolution = 0.01; //angular step size for the brute force search
    
    if(bad_mode){
        metersPerPixel_lc = 0.1;
        thetaResolution_lc = 0.05;
        metersPerPixel = 0.1;
        thetaResolution = 0.1;
    }

    sm_incremental_matching_modes_t matchingMode = SM_GRID_COORD; //use gradient descent to improve estimate after brute force search
    int useMultires = 3; // low resolution will have resolution metersPerPixel * 2^useMultiRes

    double initialSearchRangeXY = .15; //nominal range that will be searched over
    double initialSearchRangeTheta = .1;

    //SHOULD be set greater than the initialSearchRange
    double maxSearchRangeXY = .3; //if a good match isn't found I'll expand and try again up to this size...
    double maxSearchRangeTheta = .2; //if a good match isn't found I'll expand and try again up to this size...

    int maxNumScans = 1;//30;//30; //keep around this many scans in the history
    double addScanHitThresh = .90; //add a new scan to the map when the number of "hits" drops below this

    int useThreads = 1;

    //create the actual scan matcher object
    /*regionslam = new RegionSlam(lcm, metersPerPixel_lc, 
      thetaResolution_lc, useGradientAscentPolish_lc, 
      useMultires_lc,
      draw_scanmatch , useThreads_lc, frames);*/

    //create the incremental scan matcher object
    sm_incremental = new ScanMatcher(metersPerPixel, thetaResolution, useMultires, useThreads, false);
    //set the scan matcher to start at pi/2...
    ScanTransform startPose;
    memset(&startPose, 0, sizeof(startPose));
    sm_incremental->initSuccessiveMatchingParams(maxNumScans, initialSearchRangeXY, maxSearchRangeXY,
                                                 initialSearchRangeTheta, maxSearchRangeTheta, matchingMode, addScanHitThresh, false, .3, &startPose);

    
    sm_loop = new ScanMatcher(metersPerPixel_lc, thetaResolution_lc, useMultires_lc, useThreads_lc, false);

    //create the SLAM scan matcher object
    sm = new ScanMatcher(metersPerPixel, thetaResolution, useMultires, useThreads, false);

    //set the scan matcher to start at pi/2...
    sm->initSuccessiveMatchingParams(maxNumScans, initialSearchRangeXY, maxSearchRangeXY,
                                     initialSearchRangeTheta, maxSearchRangeTheta,//LINEAR_DIST_TO_ADD, LINEAR_DIST_TO_ADD, ANGULAR_DIST_TO_ADD, ANGULAR_DIST_TO_ADD, 
                                     matchingMode, addScanHitThresh, false, .3, &startPose);

    //set the scan matcher to start at pi/2... cuz it looks better
    memset(&sm->currentPose, 0, sizeof(sm->currentPose));
    sm->currentPose.theta = M_PI / 2;
    sm->prevPose = sm->currentPose;
    r_laser_msg = NULL;
    r_utime = 0;

    verbose_laser_level = 0; // 0 - don't write anything except adding node 
    verbose_particle_level = 0; // 0 - only write when particle gets processed
    verbose_node_level = 0;

    //hardcoded loop closing scan matcher params
    double metersPerPixel_low_res = 0.4; //translational resolution for the brute force search
    double thetaResolution_low_res = 0.2; //angular step size for the brute force search
   
    int useGradientAscentPolish_low_res = 0; //use gradient descent to improve estimate after brute force search

    //Sachi - try this with resulution 5 
    int useMultires_low_res = 3; // low resolution will have resolution metersPerPixel * 2^useMultiRes
    bool useThreads_low_res = true;//false;

    //create the SLAM scan matcher object
    sm_lc_low = new ScanMatcher(metersPerPixel_low_res, thetaResolution_low_res, useMultires_low_res, useThreads_low_res, false);

    front_laser_circ = bot_ptr_circular_new (LASER_DATA_CIRC_SIZE,
                                             circ_free_lidar_data, this);
    
    rear_laser_circ = bot_ptr_circular_new (LASER_DATA_CIRC_SIZE,
                                            circ_free_lidar_data, this);

    full_laser_circ = bot_ptr_circular_new (LASER_DATA_CIRC_SIZE,
                                            circ_free_lidar_data, this);

    image_circ = bot_ptr_circular_new (IMAGE_DATA_CIRC_SIZE,
                                       circ_free_image_data, this);
         
    // Allocate the pixel map for the local occupancy grid
    double xy0[2] = {-15.0, -15.0};
    double xy1[2] = {15.0, 15.0};
    double res = 0.1;
    
    //passed to the particles - used for calculating pofz - check if its better to use a local copy
    local_px_map = new FloatPixelMap(xy0, xy1, res, 0, true, true);

    next_id = 0;

    if (sm->isUsingIPP())
        fprintf(stderr, "Using IPP\n");
    else
        fprintf(stderr, "NOT using IPP\n");

    mutex = g_mutex_new();
    mutex_language = g_mutex_new();
    mutex_particles = g_mutex_new();
    mutex_lasers = g_mutex_new();
    mutex_laser_queue = g_mutex_new();
    mutex_objects = g_mutex_new();
    mutex_finish_slam = g_mutex_new();


    still_groovy = 1;
}

SlamParticleFilter::~SlamParticleFilter(){
    if(mutex)
        g_mutex_free (mutex);

    if (mutex_particles)
        g_mutex_free (mutex_particles);

    if (mutex_lasers)
        g_mutex_free (mutex_lasers);

    fprintf(stderr, "Destructor Called\n");
}

void SlamParticleFilter::setQuestionSRTypes(){
    const spatialRelation lst[] = {LEFT_OF, RIGHT_OF, IN_FRONT_OF, BEHIND, NEAR, AT};//, AT};
    vector<spatialRelation> sr_list(lst, lst + sizeof(lst)/sizeof(spatialRelation));
    config_params.question_sr_list = sr_list;
}

//get the params for the RSS/IJRR work
void SlamParticleFilter::setFixedRegionParams(){
    ConfigurationParams &param = config_params;
    //----------- Active ------------//
    //---------- Semantic Information ------------//
    if(doSemanticClassification){
        param.useLaserForSemantic = true;
        param.useImageForSemantic = true;//false;
        param.useFactorGraphs = true;
        param.bleedLabelsToRegions = false;
    }
    else{
        param.useLaserForSemantic = false;
        param.useImageForSemantic = false;//false;
        param.useFactorGraphs = false;
        param.bleedLabelsToRegions = true;
    }

    //--------- Language grounding -------//
    param.useSLULibrary = false;
    param.sendUniqueParticles = true; 
    param.askQuestions = false;
    param.considerBotRelativeYesNoQuestions = false;
    param.considerBotRelativeMCQQuestions = false;
    param.considerLandmarkQuestions = false;
    param.alwaysGround = false; //should be deprecated 
    param.gMode = FINISH_ON_GOOD;
    param.ignoreLMDist = 20.0;
    param.ignoreFigureDist = 40.0;
    param.findShortestPathEveryTime = false;
    param.checkCurrentRegionForGrounding = false; //turn this to true if you want to have the current unformed region checked for grounding
    param.minRegionSizeBeforeLanguage = 4;
    //how the region polygon is extracted
    param.polygonMode = 4; //0 - use the mean node's laser points, 1 - use the region's convex hull, 2 - ray trace from the region and use those laser points

    //---------- Edge Proposals ------------//
    param.ignoreLargeScanmatchChanges = true;
    param.proposeSeperateSemanticEdges = true;
    param.noRegionEdgesToSample = 4; //3
    param.checkOldRegions = false;
    param.sampleEdgesWithCurrentRegion = false;
    param.useSemanticsForDistanceEdge = false;
    //use 80 for the Killian court dataset - for large errors 
    param.regionLanguageIgnoreDist = 80; //was using 20 - not sure which one to use 
    //seems like both of them are used at different places - check which one to keep 
    param.regionIgnoreDist = 10;//20; //was using 20 - not sure which one to use 
    param.maxDistForLC = 10.0;
    param.scanmatchBeforeAdd = scanmatchBeforeAdd;
    
    //---------- Scanmatching ----------------//
    param.noScanTransformCheck = false;
    param.useScanmatcherUncertainity = true; //for LC 
    param.useSMCov = false;
    param.smAcceptanceThreshold = 0.75;//0.75;//0.6 //0.7;//0.8; //0.7 - can create some bad scanmatches 
    param.incSMAcceptanceThreshold = 0.7;//0.7;//0.7;//0.5; 
    param.langSMAcceptanceThreshold = 0.55;
    param.smCloseAcceptanceThreshold = 0.5;
    param.scanmatchWithCurrentRegion = false; 
    param.minRegionSizeBeforeScanmatch = 2;
    param.cleanupMRPTResult = false;

    //---------- Visualization-------------------//
    param.clearDeadEdges = !addDummyConstraints;
    param.drawScanmatch = draw_scanmatch;
    param.addDummyConstraints = true;

    //---------- Verbosity -------------//
    param.printDistanceScanmatch = true;
    param.printLanguageScanmatch = true;
    param.printRegionCreation = false;
    param.printGraphCreation = false;
    param.verbose = false;

    //-------- PofZ method ----//
    param.calculateRegionProb = false;//true;

    //-------- Region Merging ----------//
    param.mergeRegions = false;
    param.mergeRegionsOnOverlap = false;
    param.mergeMultiple = false;
    param.mergeRegionsOnCloseNodePairs = false;
    param.proposeEdgesFromCurrentRegion = false; //set this to true if you wish to propose edges from the current region 
    param.mergeOverlapThreshold = 0.7;
    param.mergeCloseNodesThreshold = 0.7;
    param.mergeSpectralThreshold = 0.5;

    //-------- Segmentation--------------//
    param.segmentSplitThreshold = 0.5;
    param.fixedRegionDistanceThreshold = 6.0;
    //if the following are both false - it uses the fixedDistanceThreshold - should make this an eum type
    param.useSpectralFixed = false;
    param.spectralClustering = false;
    param.minRegionSizeToSegment = 3;

    //---------- Unused ----------//
    param.segmentationMethod = 0;
    param.segmentBasedOnOverlap = true; //otherwise - will always segment - and then rely on the merging to merge regions 
    param.scanmatchOnSegmentEvent = true; //propose edges only when a basic segmentation (which creates a new RegionSegment) happens 
    param.countForBatchOptimize=1;
    param.useICP = useICP; 
    param.skipCloseNodes = true; 

    //----- Deprecated ------ //
    param.scanmatchOnlyRegion = useOnlyRegion;
    param.skipOutsideSM = skipOutsideSM; //skips regions deemed to be outside 
    param.doWideMatchOnFail = false;
    param.useLowResPrior = useLowResPrior;
    param.strictScanmatch = false;
    param.useBasicGraph = false;

}

//get the params for the ICRA work
void SlamParticleFilter::setSpectralRegionParamsICRA(){
    ConfigurationParams &param = config_params;
    //----------- Active ------------//
    //---------- Semantic Information ------------//
    if(doSemanticClassification){
        param.useLaserForSemantic = true;
        param.useImageForSemantic = true;
        param.useFactorGraphs = true;
        param.bleedLabelsToRegions = false;
    }
    else{
        param.useLaserForSemantic = false;
        param.useImageForSemantic = false;//false;
        param.useFactorGraphs = false;
        param.bleedLabelsToRegions = true;
    }

    //how the region polygon is extracted
    param.polygonMode = 4; //0 - use the mean node's laser points, 1 - use the region's convex hull, 2 - ray trace from the region and use those laser points
    
    //--------- Language grounding -------//
    param.useSLULibrary = false;
    param.sendUniqueParticles = false; 
    param.askQuestions = false;
    param.considerBotRelativeYesNoQuestions = false;
    param.considerBotRelativeMCQQuestions = false;
    param.considerLandmarkQuestions = false;
    param.alwaysGround = false;
    param.gMode = GROUND_ONCE;
    param.ignoreLMDist = 20.0;
    param.ignoreFigureDist = 40.0;
    param.findShortestPathEveryTime = false;
    param.checkCurrentRegionForGrounding = false; //turn this to true if you want to have the current unformed region checked for grounding
    param.minRegionSizeBeforeLanguage = 4;

    //---------- Edge Proposals ------------//
    param.ignoreLargeScanmatchChanges = false;
    param.proposeSeperateSemanticEdges = false;
    param.noRegionEdgesToSample = 4; //3
    param.sampleEdgesWithCurrentRegion = false;
    param.checkOldRegions = false;
    param.regionLanguageIgnoreDist = 80;//160; //was using 20 - not sure which one to use 
    //seems like both of them are used at different places - check which one to keep 
    param.regionIgnoreDist = 20; //was using 20 - not sure which one to use 
    param.maxDistForLC = 10.0;
    param.scanmatchBeforeAdd = scanmatchBeforeAdd;
    param.useSemanticsForDistanceEdge = true;
    //---------- Scanmatching ----------------//
    param.noScanTransformCheck = false;
    param.useScanmatcherUncertainity = true; //for LC 
    param.useSMCov = false;
    param.smAcceptanceThreshold = 0.75;//0.75;//0.6 //0.7;//0.8; //0.7 - can create some bad scanmatches 
    param.incSMAcceptanceThreshold = 0.7;//0.7;//0.7;//0.5; 
    param.langSMAcceptanceThreshold = 0.55;
    param.smCloseAcceptanceThreshold = 0.5;
    param.scanmatchWithCurrentRegion = false; 
    param.minRegionSizeBeforeScanmatch = 2;
    param.cleanupMRPTResult = false;

    //---------- Visualization-------------------//
    param.clearDeadEdges = !addDummyConstraints;
    param.drawScanmatch = draw_scanmatch;
    param.addDummyConstraints = true;

    //---------- Verbosity -------------//
    param.printDistanceScanmatch = false;
    param.printLanguageScanmatch = false;
    param.printRegionCreation = false;
    param.printGraphCreation = false;
    param.verbose = false;

    //------ PofZ method -----//
    param.calculateRegionProb = false;

    //-------- Region Merging ----------//
    param.mergeRegions = true;
    param.mergeRegionsOnOverlap = false;
    param.mergeMultiple = false;
    param.mergeOverlapThreshold = 0.7;
    param.mergeCloseNodesThreshold = 0.7;
    param.mergeSpectralThreshold = 0.5;
    param.mergeRegionsOnCloseNodePairs = true;
    param.proposeEdgesFromCurrentRegion = false; //set this to true if you wish to propose edges from the current region 

    //-------- Segmentation--------------//
    param.segmentSplitThreshold = 0.5;
    param.fixedRegionDistanceThreshold = 6.0;
    //if the following are both false - it uses the fixedDistanceThreshold - should make this an eum type
    param.useSpectralFixed = false;
    param.spectralClustering = true;
    param.minRegionSizeToSegment = 3;

    //---------- Unused ----------//
    param.segmentationMethod = 0;
    param.segmentBasedOnOverlap = true; //otherwise - will always segment - and then rely on the merging to merge regions 
    param.scanmatchOnSegmentEvent = true; //propose edges only when a basic segmentation (which creates a new RegionSegment) happens 
    param.countForBatchOptimize=1;
    param.useICP = useICP; 
    param.skipCloseNodes = true; 

    //----- Deprecated ------ //
    param.scanmatchOnlyRegion = useOnlyRegion;
    param.skipOutsideSM = skipOutsideSM; //skips regions deemed to be outside 

    param.doWideMatchOnFail = false;
    param.useLowResPrior = useLowResPrior;
    param.strictScanmatch = false;
    param.useBasicGraph = false;
}

//get the params for the post-icra work - with continuous grounding
void SlamParticleFilter::setSpectralRegionParams(){
    ConfigurationParams &param = config_params;
    //----------- Active ------------//
    //---------- Semantic Information ------------//
    if(doSemanticClassification){
        param.useLaserForSemantic = true;
        param.useImageForSemantic = true;
        param.useFactorGraphs = true;
        param.bleedLabelsToRegions = false;
    }
    else{
        param.useLaserForSemantic = false;
        param.useImageForSemantic = false;//false;
        param.useFactorGraphs = false;
        param.bleedLabelsToRegions = true;
    }

    //how the region polygon is extracted
    param.polygonMode = 4; //0 - use the mean node's laser points, 1 - use the region's convex hull, 2 - ray trace from the region and use those laser points
    
    //--------- Language grounding -------//
    param.useSLULibrary = true;
    param.sendUniqueParticles = false; 
    param.askQuestions = true;
    param.question_type = USE_ALL_PARTICLES; //USE_MAX_PARTICLE; 
    param.considerBotRelativeYesNoQuestions = true;
    param.considerBotRelativeMCQQuestions = false;
    param.considerLandmarkQuestions = true;
    param.alwaysGround = true;
    param.gMode = ALWAYS_GROUND;
    param.ignoreLMDist = 20.0;
    param.ignoreFigureDist = 20.0;//20.0;
    param.findShortestPathEveryTime = true;
    param.checkCurrentRegionForGrounding = false; //turn this to true if you want to have the current unformed region checked for grounding
    param.minRegionSizeBeforeLanguage = 4;
    
    //---------- Edge Proposals ------------//
    param.ignoreLargeScanmatchChanges = false;
    param.proposeSeperateSemanticEdges = false;
    param.noRegionEdgesToSample = 4; //3
    param.sampleEdgesWithCurrentRegion = true;
    param.checkOldRegions = true;
    param.regionLanguageIgnoreDist = 80;//160; //was using 20 - not sure which one to use 
    //seems like both of them are used at different places - check which one to keep 
    param.regionIgnoreDist = 20; //was using 20 - not sure which one to use 
    param.maxDistForLC = 10.0;
    param.scanmatchBeforeAdd = scanmatchBeforeAdd;
    param.useSemanticsForDistanceEdge = true;

    //---------- Scanmatching ----------------//
    param.noScanTransformCheck = false;
    param.useScanmatcherUncertainity = true; //for LC 
    param.useSMCov = false;
    param.smAcceptanceThreshold = 0.6;//0.75;//0.6 //0.7;//0.8; //0.7 - can create some bad scanmatches 
    param.incSMAcceptanceThreshold = 0.4;//0.7;//0.7;//0.7;//0.5; 
    param.langSMAcceptanceThreshold = 0.55;
    param.smCloseAcceptanceThreshold = 0.5;
    param.scanmatchWithCurrentRegion = false;//false; 
    param.minRegionSizeBeforeScanmatch = 2;
    param.cleanupMRPTResult = false;

    //---------- Visualization-------------------//
    param.clearDeadEdges = !addDummyConstraints;
    param.drawScanmatch = draw_scanmatch;
    param.addDummyConstraints = true;

    //---------- Verbosity -------------//
    param.printDistanceScanmatch = false;
    param.printLanguageScanmatch = false;
    param.printRegionCreation = false;
    param.printGraphCreation = false;
    param.verbose = false;

    //------ PofZ method -----//
    param.calculateRegionProb = false;

    //-------- Region Merging ----------//
    param.mergeRegions = true;
    param.mergeRegionsOnOverlap = false;
    param.mergeMultiple = true;
    param.mergeRegionsOnCloseNodePairs = true;
    param.mergeOverlapThreshold = 0.7;
    param.mergeCloseNodesThreshold = 0.7;
    param.mergeSpectralThreshold = 0.5;
    param.proposeEdgesFromCurrentRegion = false; //set this to true if you wish to propose edges from the current region 

    //-------- Segmentation--------------//
    param.segmentSplitThreshold = 0.5;
    param.fixedRegionDistanceThreshold = 6.0;
    //if the following are both false - it uses the fixedDistanceThreshold - should make this an eum type
    //not using the randomizing for this - because of the question asking 
    param.useSpectralFixed = true;
    param.spectralClustering = false;//true;
    param.minRegionSizeToSegment = 3;

    //---------- Unused ----------//
    param.segmentationMethod = 0;
    param.segmentBasedOnOverlap = true; //otherwise - will always segment - and then rely on the merging to merge regions 
    param.scanmatchOnSegmentEvent = true; //propose edges only when a basic segmentation (which creates a new RegionSegment) happens 
    param.countForBatchOptimize=1;
    param.useICP = useICP; 
    param.skipCloseNodes = true; 

    //----- Deprecated ------ //
    param.scanmatchOnlyRegion = useOnlyRegion;
    param.skipOutsideSM = skipOutsideSM; //skips regions deemed to be outside 

    param.doWideMatchOnFail = false;
    param.useLowResPrior = useLowResPrior;
    param.strictScanmatch = false;
    param.useBasicGraph = false;
}

//get the params for the Fixed Spectral work
void SlamParticleFilter::setFixedSpectralRegionParams(){
    ConfigurationParams &param = config_params;
    //----------- Active ------------//
    //---------- Semantic Information ------------//
    if(doSemanticClassification){
        param.useLaserForSemantic = true;
        param.useImageForSemantic = true;
        param.useFactorGraphs = true;
        param.bleedLabelsToRegions = false;
    }
    else{
        param.useLaserForSemantic = false;
        param.useImageForSemantic = false;//false;
        param.useFactorGraphs = false;
        param.bleedLabelsToRegions = true;
    }
    //how the region polygon is extracted
    param.polygonMode = 4; //0 - use the mean node's laser points, 1 - use the region's convex hull, 2 - ray trace from the region and use those laser points
    
    //--------- Language grounding -------//
    param.useSLULibrary = true;
    param.sendUniqueParticles = false; 
    param.askQuestions = true;
    param.question_type = USE_ALL_PARTICLES;//USE_ALL_PARTICLES; //USE_MAX_PARTICLE; 
    param.considerBotRelativeYesNoQuestions = true;
    param.considerBotRelativeMCQQuestions = false;
    param.considerLandmarkQuestions = true;
    param.alwaysGround = false;
    param.gMode = ALWAYS_GROUND;
    param.ignoreLMDist = 20.0;
    param.ignoreFigureDist = 20.0;
    param.findShortestPathEveryTime = true;
    param.checkCurrentRegionForGrounding = true; //turn this to true if you want to have the current unformed region checked for grounding
    param.minRegionSizeBeforeLanguage = 4;

    //---------- Edge Proposals ------------//
    param.ignoreLargeScanmatchChanges = false;
    param.proposeSeperateSemanticEdges = false;
    param.noRegionEdgesToSample = 4; //3
    param.sampleEdgesWithCurrentRegion = true;
    param.checkOldRegions = true;
    param.regionLanguageIgnoreDist = 80;//160; //was using 20 - not sure which one to use 
    //seems like both of them are used at different places - check which one to keep 
    param.regionIgnoreDist = 20; //was using 20 - not sure which one to use 
    param.maxDistForLC = 10.0;
    param.scanmatchBeforeAdd = scanmatchBeforeAdd;
    param.useSemanticsForDistanceEdge = true;
    //---------- Scanmatching ----------------//
    param.noScanTransformCheck = false;
    param.useScanmatcherUncertainity = true; //for LC 
    param.useSMCov = false;
    param.smAcceptanceThreshold = 0.6;//0.75;//0.6 //0.7;//0.8; //0.7 - can create some bad scanmatches 
    param.incSMAcceptanceThreshold = 0.4;//0.7;//0.7;//0.5; 
    param.langSMAcceptanceThreshold = 0.55;
    param.smCloseAcceptanceThreshold = 0.5;
    param.scanmatchWithCurrentRegion = false; 
    param.minRegionSizeBeforeScanmatch = 2;
    param.cleanupMRPTResult = false;

    //---------- Visualization-------------------//
    param.clearDeadEdges = !addDummyConstraints;
    param.drawScanmatch = draw_scanmatch;
    param.addDummyConstraints = true;

    //---------- Verbosity -------------//
    param.printDistanceScanmatch = false;
    param.printLanguageScanmatch = false;
    param.printRegionCreation = false;
    param.printGraphCreation = false;
    param.verbose = false;

    //------ PofZ method -----//
    param.calculateRegionProb = false;

    //-------- Region Merging ----------//
    param.mergeRegions = true;
    param.mergeRegionsOnOverlap = false;
    param.mergeMultiple = true;
    param.mergeRegionsOnCloseNodePairs = true;
    param.resegmentMergedRegion = true;
    param.proposeEdgesFromCurrentRegion = false; //set this to true if you wish to propose edges from the current region 
    param.mergeOverlapThreshold = 0.7;
    param.mergeCloseNodesThreshold = 0.5;
    param.mergeSpectralThreshold = 0.5;

    //-------- Segmentation--------------//
    param.segmentSplitThreshold = 0.5;
    param.fixedRegionDistanceThreshold = 6.0;
    //if the following are both false - it uses the fixedDistanceThreshold - should make this an eum type
    param.useSpectralFixed = true;
    param.spectralClustering = false;
    param.minRegionSizeToSegment = 3;

    //even with the fixed spectral method - because edges are sampled right now - the region merging can change 
    //thus particle regions can be different 

    //---------- Unused ----------//
    param.segmentationMethod = 0;
    param.segmentBasedOnOverlap = true; //otherwise - will always segment - and then rely on the merging to merge regions 
    param.scanmatchOnSegmentEvent = true; //propose edges only when a basic segmentation (which creates a new RegionSegment) happens 
    param.countForBatchOptimize=1;
    param.useICP = useICP; 
    param.skipCloseNodes = true; 

    //----- Deprecated ------ //
    param.scanmatchOnlyRegion = useOnlyRegion;
    param.skipOutsideSM = skipOutsideSM; //skips regions deemed to be outside 
    param.doWideMatchOnFail = false;
    param.useLowResPrior = useLowResPrior;
    param.strictScanmatch = false;
    param.useBasicGraph = false;

}

void SlamParticleFilter::usage(const char *name)
{
    //please update this 
    fprintf(stderr, "usage: %s [options]\n"
            "\n"
            "  -h, --help                      Shows this help text and exits\n"
            "  -a,  --aligned                  Subscribe to aligned_laser (ROBOT_LASER) msgs\n"
            "  -c, --chan <LCM CHANNEL>        Input lcm channel default:\"LASER\" or \"ROBOT_LASER\"\n"
            "  -s, --scanmatch                 Run Incremental scan matcher every time a node gets added to map\n"
            "  -d, --draw                      Show window with scan matches \n"
            "  -p, --publish_pose              publish POSE messages\n"
            "  -v, --verbose                   Be verbose\n"
            "  -m, --mode  \"HOKUYO_UTM\"|\"SICK\" configures low-level options.\n"
            "\n"
            "Low-level laser options:\n"
            "  -A, --mask <min,max>            Mask min max angles in (radians)\n"
            "  -B, --beamskip <n>              Skip every n beams \n"
            "  -D, --decimation <value>        Spatial decimation threshold (meters?)\n"
            "  -F                              Use frames to align laser scans\n"
            "  -R                              Use regions\n"
            "  -S                              Use Slave mode - pose additions will be triggered by external source\n"
            "  -M, --range <range>             Maximum range (meters)\n", name);
}

void SlamParticleFilter::add_full_laser(const bot_core_planar_lidar_t *msg){
    bot_ptr_circular_add (full_laser_circ, bot_core_planar_lidar_t_copy(msg));
}

void SlamParticleFilter::add_image(const bot_core_image_t * msg){
    bot_ptr_circular_add (image_circ, bot_core_image_t_copy(msg));
}

void SlamParticleFilter::add_rear_laser(const bot_core_planar_lidar_t *msg){
    bot_ptr_circular_add (rear_laser_circ, bot_core_planar_lidar_t_copy(msg));
}

void SlamParticleFilter::add_front_laser(const bot_core_planar_lidar_t *msg){
    last_laser_reading_utime = msg->utime;
    if(verbose_laser_level >=2){
        output_writer->write_to_buffer("[LCM] [Laser] Laser handler called", false, true, false);        
    }
    if(slave_mode){
        bot_ptr_circular_add (front_laser_circ,  bot_core_planar_lidar_t_copy(msg));
        //if there is an unhandled node init msg - check if this is it 

        if(node_init != NULL){
            double min_utime = 0.5;//we dont consider if the closest laser is 0.5 s apart
            int close_ind = -1;
            bot_core_planar_lidar_t *flaser = NULL;

            for (int i=0; i<(bot_ptr_circular_size(front_laser_circ)); i++) {
                bot_core_planar_lidar_t *laser = (bot_core_planar_lidar_t *) bot_ptr_circular_index(front_laser_circ, i);
                double time_diff = fabs(laser->utime - msg->utime)  / 1.0e6;

                if(time_diff< min_utime){
                    min_utime = time_diff;
                    close_ind = i;
                }
            }
            
            if(close_ind == -1){
                fprintf(stderr, "No matching laser scan found\n");
                return;
            }
            flaser = (bot_core_planar_lidar_t *) bot_ptr_circular_index(front_laser_circ, close_ind);
            
            process_laser(flaser, msg->utime);

            if(node_init != NULL){
                slam_init_node_t_destroy(node_init);
                node_init = NULL;
            }
        }
    }
    else{
        process_laser( (bot_core_planar_lidar_t *) msg, msg->utime);
    }
    if(verbose_laser_level >=2){
        output_writer->write_to_buffer("[LCM] [Laser] Laser handler finished", false, true, true);        
    }
    output_writer->write_buffer();
    static int64_t utime_prev = msg->utime;
}

//USED 
//process the laser scan - for odometry
void SlamParticleFilter::process_laser(bot_core_planar_lidar_t *flaser, int64_t utime, int force_supernode){
    if(useOdom){
        if(verbose_laser_level >= 2){
            output_writer->write_to_buffer("[LCM] [Laser] Process laser - use odom", false, true, false);               
        }
        sm_rigid_transform_2d_t odom;
        memset(&odom, 0, sizeof(odom));

        double rpy[3];
        
        //this covariance is ignored 
        double cov[9] = { 0 };
        cov[0] = 0.0005;
        cov[1] = 0;
        cov[3] = 0;
        cov[4] = 0.0005; //about 2 cm of std dev 
        cov[8] = 0.0002;
        
        if(!use_frames){
            if(last_deadreakon != NULL){
                //use the odom msg
                memcpy(odom.pos, last_deadreakon->pos, 2 * sizeof(double));
                bot_quat_to_roll_pitch_yaw(last_deadreakon->orientation, rpy);
            }
            else
                return;
        }
        else{ 
            int64_t frame_utime;
            //this can be fine as long as its been updated 
            int status = bot_frames_get_latest_timestamp(frames, "body",
                                                         "local",&frame_utime);

            if(!status || fabs(frame_utime - utime) / 1.0e6 > 10.0){
                fprintf(stderr, "Frames hasn't been updated yet - retruning %f - %f => Gap : %f\n", frame_utime/1.0e6, utime /1.0e6, fabs(frame_utime - utime) / 1.0e6);
                return;
            }

            double body_to_local[12];
            if (!bot_frames_get_trans_mat_3x4_with_utime (frames, "body",
                                                          "local",  flaser->utime,
                                                          body_to_local)) {
                fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
                return;        
            }
            double pose_b[3] = {.0}, pose_l[3];
            bot_vector_affine_transform_3x4_3d (body_to_local, 
                                                pose_b, 
                                                pose_l);
            odom.pos[0] = pose_l[0];
            odom.pos[1] = pose_l[1];

            BotTrans body_to_local_t;

            if (!bot_frames_get_trans_with_utime (frames, "body", "local", flaser->utime, 
                                                  &body_to_local_t)){
                fprintf (stderr, "frame error\n");
                return;
            }
            
            double rpy_b[3] = {0};
            double quat_b[4], quat_l[4];
            bot_roll_pitch_yaw_to_quat (rpy_b, quat_b);
            
            bot_quat_mult (quat_l, body_to_local_t.rot_quat, quat_b);
            bot_quat_to_roll_pitch_yaw (quat_l, rpy);
        }

        odom.theta = rpy[2];
        //cov was published as body frame already... rotate to global for now
        double Rcov[9];
        sm_rotateCov2D(cov, odom.theta, odom.cov);

        static int64_t utime_prev = flaser->utime;

        sm_tictoc("aligned_laser_handler");
        add_to_queue(flaser, &odom, flaser->utime, force_supernode);
        sm_tictoc("aligned_laser_handler");
    }
    else{
        if(verbose_laser_level >= 2){
            output_writer->write_to_buffer("[LCM] [Laser] Process laser - use scanmatch", false, true, false);               
        }
        //if we dont have odom - use scanmatching 
        smPoint * points = (smPoint *) calloc(flaser->nranges, sizeof(smPoint));
        int numValidPoints = sm_projectRangesAndDecimate(beam_skip, spatialDecimationThresh, flaser->ranges,
                                                         flaser->nranges, flaser->rad0, flaser->radstep, points, 
                                                         maxRange, validBeamAngles[0], validBeamAngles[1]);
        if (numValidPoints < 30) {
            fprintf(stderr, "WARNING! NOT ENOUGH VALID POINTS! numValid=%d\n", numValidPoints);
            free (points);
            return;
        }

        double sensor_to_body[12];
        if (!bot_frames_get_trans_mat_3x4 (frames, chan,
                                           "body",
                                           sensor_to_body)) {
            fprintf (stderr, "Error getting bot_frames transformation from %s to body!\n", chan);
            free (points);
            return;
        }

        //transform to body pose
        double pos_s[3] = {0};
        double pos_b[3] = {0};
        for(int i=0; i < numValidPoints; i++){
            pos_s[0] = points[i].x;
            pos_s[1] = points[i].y;
            bot_vector_affine_transform_3x4_3d (sensor_to_body, pos_s , pos_b);
            points[i].x = pos_b[0];
            points[i].y = pos_b[1];
        }
        
        ////////////////////////////////////////////////////////////////////
        //Actually do the matching
        ////////////////////////////////////////////////////////////////////
        //don't have a better estimate than prev, so just set prior to NULL

        ScanTransform r = sm_incremental->matchSuccessive(points, numValidPoints, 
                                                          SM_HOKUYO_UTM, sm_get_utime(), NULL); 

        sm_rigid_transform_2d_t odom;
        memset(&odom, 0, sizeof(odom));
        odom.utime = flaser->utime;
        odom.pos[0] = r.x;
        odom.pos[1] = r.y;
        odom.theta = r.theta;
        memcpy(odom.cov, r.sigma, 9 * sizeof(double));

        //  sm_incremental->drawGUI(points, numValidPoints, r, NULL,"ScanMatcher");
        
        free(points);
        add_to_queue(flaser, &odom, flaser->utime, force_supernode);
    }
    output_writer->write_buffer();
}

//USED
//Only add laser scans to the queue if the scan is over a specific distance from the last one in the queue 
void SlamParticleFilter::add_to_queue(const bot_core_planar_lidar_t * laser_msg, const sm_rigid_transform_2d_t * odom,
                                      int64_t utime, int force_supernode)
{
    static char buf[1024];

    static int first_scan = 1;
    static int first_laser_scan = 1;
    
    //compute the distance between this scan and the last one that got added.
    double dist[2];
    sm_vector_sub_2d(odom->pos, prev_odom->pos, dist);

    double ld = sm_norm(SMPOINT(dist));
    double ad = sm_angle_subtract(odom->theta, prev_odom->theta);
    bool addScan = false;

    if(!slave_mode){
        //this should be taken off when node creation is automatic
        if (fabs(ld) > LINEAR_DIST_TO_ADD) 
            addScan = true;

        if (fabs(ad) > ANGULAR_DIST_TO_ADD) 
            addScan = true;
        
        if(first_scan){
            addScan = true;
            first_scan = 0;
        }       
    }

    //if in slave mode - we add all new requests 
    if(slave_mode && node_init != NULL)
        addScan = true; 

    if(!addScan)
        return;
    
    bot_core_planar_lidar_t *rlaser = get_closest_rear_laser(laser_msg->utime);
    bot_core_planar_lidar_t *full_laser = NULL;
    bot_core_image_t *image = NULL;

    if(doSemanticClassification){
        full_laser = get_closest_full_laser(laser_msg->utime);
        image = get_closest_image(laser_msg->utime);
        
        if(full_laser == NULL){
            if(verbose_node_level >= 0){
                output_writer->write_to_buffer("[Semantic Classification] [Error] No close laser found");
            }
            return;
        }
        if(image == NULL){
            if(verbose_node_level >= 0){
                output_writer->write_to_buffer("[Semantic Classification] [Error] No close image found");
            }
            return;
        }
    }

    //get the odometry delta
    Pose2d curr_pose(odom->pos[0], odom->pos[1], odom->theta);
    Pose2d prev_pose(prev_odom->pos[0], prev_odom->pos[1], prev_odom->theta);
    Pose2d prev_curr_tranf = curr_pose.ominus(prev_pose);

    if (prev_odom != NULL)
        sm_rigid_transform_2d_t_destroy(prev_odom);
    prev_odom = sm_rigid_transform_2d_t_copy(odom);

    laser_odom_t *l_odom = new laser_odom_t();//(laser_odom_t *) calloc(1, sizeof(laser_odom_t));

    l_odom->laser = bot_core_planar_lidar_t_copy(laser_msg);
       
    if(rlaser != NULL){
        l_odom->rlaser = bot_core_planar_lidar_t_copy(rlaser);
    }
    //get the closest full laser and image - which will be used for classification 
    int64_t s_utime = bot_timestamp_now();

    fprintf(stderr, RED);
    if(full_laser != NULL){
        //fprintf(stderr, YELLOW "\n\n+++++++++++++++++ Found Full laser (%f) +++++++++++++++++\n\n" RESET_COLOR, (full_laser->utime - laser_msg->utime)/1.0e6);
        l_odom->full_laser = bot_core_planar_lidar_t_copy(full_laser);
        l_odom->laser_result = get_classification_laser(classifier, l_odom->full_laser);

        //print_classification_result_t(l_odom->laser_result);
        for(int i=0; i < l_odom->laser_result->count; i++){
            semantic_class_t *s_class = &l_odom->laser_result->classes[i];
            int old_class = s_class->type;
            s_class->type = label_info->getIndexFromClassifierClassId(s_class->type);
            //fprintf(stderr, "Class : Old : %d => New : %d\n", old_class,  s_class->type);
            if(verbose_node_level >= 2){
                sprintf(buf, "[Semantic Classification] [Laser] Class : Old : %d => New : %d", old_class,  s_class->type);
                output_writer->write_to_buffer(buf);
            }
        }
        //we should remap this here 
        //
    }
    else{
        l_odom->laser_result = NULL;
    }
    
    if(image != NULL){
        //fprintf(stderr, RED "\n\n+++++++++++++++++ Found Image (%f) +++++++++++++++++\n\n" RESET_COLOR, (image->utime - laser_msg->utime)/1.0e6);
        l_odom->image = bot_core_image_t_copy(image);
        l_odom->image_result = get_classification_image(classifier, l_odom->image);

        //we should remap this here 
        //print_classification_result_t(l_odom->image_result);
        for(int i=0; i < l_odom->image_result->count; i++){
            semantic_class_t *s_class = &l_odom->image_result->classes[i];
            int old_class = s_class->type;
            s_class->type = label_info->getIndexFromClassifierClassId(s_class->type);

            if(verbose_node_level >= 2){
                sprintf(buf, "[Semantic Classification] [Laser] Class : Old : %d => New : %d", old_class,  s_class->type);
                output_writer->write_to_buffer(buf);
            }
            //fprintf(stderr, "Class : Old : %d => New : %d\n", old_class,  s_class->type);
        }
        //
    }
    else{
        l_odom->image_result = NULL;
    }

    int64_t e_utime = bot_timestamp_now();
    
    //fprintf(stderr, RED "\n\n+++++++++++++++++ Classifying semantics (time) : %.4f (s) +++++++++++++++++\n\n" RESET_COLOR, (e_utime - s_utime)/1.0e6);


    l_odom->odom =  sm_rigid_transform_2d_t_copy(odom);//new Pose2d(odom);//(prev_curr_tranf);
    l_odom->force_supernode = force_supernode;

    //lock a mutex
    //fprintf(stderr, YELLOW "Waiting to add laser to Queue\n", RESET_COLOR);
    g_mutex_lock(mutex_laser_queue);
    lasers_to_add.push_back(l_odom);
    g_mutex_unlock(mutex_laser_queue);
    //fprintf(stderr, YELLOW "Added laser to Queue\n", RESET_COLOR);
}
    
////////////////////////////////////////////////////////////////////
//where the laser is put to the circular buffer 
////////////////////////////////////////////////////////////////////
bot_core_planar_lidar_t *SlamParticleFilter::get_closest_rear_laser(int64_t utime){
    double min_utime = 0.5;//we dont consider if the closest laser is 0.5 s apart
    int close_ind = -1;
    bot_core_planar_lidar_t *rlaser = NULL;

    for (int i=0; i<(bot_ptr_circular_size(rear_laser_circ)); i++) {
        bot_core_planar_lidar_t *laser = (bot_core_planar_lidar_t *) bot_ptr_circular_index(rear_laser_circ, i);
        double time_diff = fabs(laser->utime - utime)  / 1.0e6;

        if(time_diff< min_utime){
            min_utime = time_diff;
            close_ind = i;
        }
    }

    if(close_ind == -1){
        if(verbose){
            fprintf(stderr, "No matching rear laser scan found\n");
        }
        return NULL;
    }
  
    rlaser = (bot_core_planar_lidar_t *) bot_ptr_circular_index(rear_laser_circ, close_ind);
    return rlaser;
}

bot_core_planar_lidar_t *SlamParticleFilter::get_closest_full_laser(int64_t utime){
    double min_utime = 0.5;//we dont consider if the closest laser is 0.5 s apart
    int close_ind = -1;
    bot_core_planar_lidar_t *full_laser = NULL;

    for (int i=0; i<(bot_ptr_circular_size(full_laser_circ)); i++) {
        bot_core_planar_lidar_t *laser = (bot_core_planar_lidar_t *) bot_ptr_circular_index(full_laser_circ, i);
        double time_diff = fabs(laser->utime - utime)  / 1.0e6;

        if(time_diff< min_utime){
            min_utime = time_diff;
            close_ind = i;
        }
    }

    if(close_ind == -1){
        if(verbose){
            fprintf(stderr, "No matching full laser scan found\n");
        }
        return NULL;
    }
  
    full_laser = (bot_core_planar_lidar_t *) bot_ptr_circular_index(full_laser_circ, close_ind);
    return full_laser;
}

bot_core_image_t *SlamParticleFilter::get_closest_image(int64_t utime){
    double min_utime = 5.0;//0.5;//we dont consider if the closest laser is 0.5 s apart
    int close_ind = -1;
    bot_core_image_t *image = NULL;

    for (int i=0; i<(bot_ptr_circular_size(image_circ)); i++) {
        bot_core_image_t *image = (bot_core_image_t *) bot_ptr_circular_index(image_circ, i);
        double time_diff = fabs(image->utime - utime)  / 1.0e6;

        if(time_diff< min_utime){
            min_utime = time_diff;
            close_ind = i;
        }
    }

    if(close_ind == -1){
        if(verbose){
            fprintf(stderr, "No matching image found\n");
        }
        return NULL;
    }
  
    image = (bot_core_image_t *) bot_ptr_circular_index(image_circ, close_ind);
    //fprintf(stderr, "Closest Image Diff : %.3f\n", (image->utime - utime)  / 1.0e6);
   
    return image;
}

void SlamParticleFilter::publish_occupancy_map_msg () {

    // Determine most-likely particle id
    int max_particle_idx = 0;
    double weight_max = 0;

    for (int i=0; i < slam_particles.size(); i++) {
        SlamParticle *particle = slam_particles.at(i);

        if (particle->normalized_weight > weight_max) {
            weight_max = particle->normalized_weight;
            max_particle_idx = i;
        }
    }

    SlamParticle *particle = slam_particles.at(max_particle_idx);
    
    particle->publishOccupancyMap();
}

//USED 
int SlamParticleFilter::process_laser_queue(){
    if(verbose_laser_level >= 2)
        output_writer->write_to_buffer("[Laser Queue] Processing laser queue", false, true, false);               
    //go through the queue 
    int to_add = 0;
    //fprintf(stderr, RED "Waiting on Laser Queue\n" RESET_COLOR);
    g_mutex_lock(mutex_laser_queue);    
    if(lasers_to_add.size() > 0){
        //fprintf(stderr, BLUE "+++Laser Queue Size : %d\n" RESET_COLOR, (int) lasers_to_add.size());
        to_add = 1;
    }
    g_mutex_unlock(mutex_laser_queue);
    //fprintf(stderr, RED "Done checking Laser Queue\n" RESET_COLOR);

    bool _finishSlamHeard = false;
    g_mutex_lock(mutex_finish_slam);
    _finishSlamHeard = finishSlamHeard;
    g_mutex_unlock(mutex_finish_slam);

    if(to_add ==0){
        if(_finishSlamHeard){
            g_mutex_lock(mutex_finish_slam);
            finishSlam = true;
            fprintf(stdout, "SLAM Finishing in laser queue\n");
            g_mutex_unlock(mutex_finish_slam);
        }
        return 0;
    }
    
    
    
    int count = 0; 
    //otherwise because of resampling everything gets blocked 
    //maybe this createNodeScan can be broken up - so that another thread 
    //already does the scanmatching - while the particles are being processed
    while(to_add && (_finishSlamHeard || count < MAX_LASERS_TO_COPY_FROM_QUEUE)){
        //fprintf(stderr, BLUE "Waiting on Laser Queue\n", RESET_COLOR);
        g_mutex_lock(mutex_laser_queue);
        //fprintf(stderr, BLUE "Have Lock\n", RESET_COLOR);
        //fprintf(stderr, "Processing Laser in queue\n");
        laser_odom_t *l_odom = lasers_to_add.at(0);

        lasers_to_add.erase(lasers_to_add.begin());

        if(lasers_to_add.size() > 0)
            to_add =1;
        else
            to_add = 0;
        //fprintf(stderr, BLUE "Unlocking Laser Queue\n", RESET_COLOR);
        g_mutex_unlock(mutex_laser_queue);
        count++;
        if(createNodeScan(l_odom)){
            if(verbose_laser_level >= 0){
                output_writer->write_to_buffer("[Laser Queue] Created Node Scan", false, true, false);           
            }
        }
        delete l_odom;
    }

    if(_finishSlamHeard){
        g_mutex_lock(mutex_finish_slam);
        finishSlam = true;
        g_mutex_unlock(mutex_finish_slam);
    }

    if(verbose_laser_level >= 2){
        output_writer->write_to_buffer("[Laser Queue] Done Processing laser queue", false, true, true);           
    }
    output_writer->write_buffer();
    return 1;
}

//this will return a list of unique particles and also update the mapping between the unique particle and 
//the duplicate (region ids) 
vector<SlamParticle *> SlamParticleFilter::get_unique_particles(){
    vector<SlamParticle *> unique_particles;

    particle_to_region_map.clear();
    unique_particle_to_duplicates.clear();

    for(int i=0; i < slam_particles.size(); i++){
        SlamParticle *part1 = slam_particles.at(i);

        //bool found_match = false;
        SlamParticle *match = NULL;
        region_mapping_t r_map;

        for(int j=0; j < unique_particles.size(); j++){
             SlamParticle *part2 = unique_particles.at(j);
             
             //map<int, int> region_map;
             
             bool part_match = part2->compareToParticle(part1, r_map.region_map);

             //we should save the region map - for processing later 
             
             if(part_match){ //part1->compareToParticle(part2)){
                 match = part2;
                 //found_match = true;
                 r_map.particle_id = part2->graph_id;
                 //insert to the match 
                 break;
             }
             else{
                 r_map.region_map.clear();
             }
        }

        if(!match){
            unique_particles.push_back(part1);
            set<int> matches; 
            unique_particle_to_duplicates.insert(make_pair(part1->graph_id, matches));
        }
        else{
            //unique_particle_to_duplicates.in
            map<int, set<int> >::iterator it_matches;
            it_matches = unique_particle_to_duplicates.find(match->graph_id);
            assert(it_matches != unique_particle_to_duplicates.end());
            it_matches->second.insert(part1->graph_id);
            particle_to_region_map.insert(make_pair(part1->graph_id, r_map));
        }
    }
    
    fprintf(stderr, "\n\n============ Number of unique particles : %d\n",(int) unique_particles.size()); 

    return unique_particles;
}

void SlamParticleFilter::publish_slam_progress(){
    slam_performance_t msg;
    msg.utime = bot_timestamp_now();
    msg.last_sensor_utime = last_laser_reading_utime; 
    msg.last_node_utime = last_node_utime; 
    msg.last_node_in_graph_utime = last_graph_utime; 

    slam_performance_t_publish(lcm, "SLAM_PROGRESS", &msg);
}

//USED 
//get the rear laser - compensated to be in the front laser's frame 
smPoint *SlamParticleFilter::get_scan_points_from_laser_compensated(bot_core_planar_lidar_t *laser_msg, char *frame, int *noPoints, int64_t utime){
    smPoint * points = (smPoint *) calloc(laser_msg->nranges, sizeof(smPoint));
    int numValidPoints = sm_projectRangesAndDecimate(beam_skip, spatialDecimationThresh, laser_msg->ranges,
                                                     laser_msg->nranges, laser_msg->rad0, laser_msg->radstep, points, 
                                                     maxRange, 
                                                     //bot_to_radians(-135), bot_to_radians(135)); 
                                                     //validBeamAngles[0], validBeamAngles[1]);
                                                     -M_PI/4, M_PI/4); 

    //fprintf(stderr, MAKE_GREEN "NO of valid points : %d\n" RESET_COLOR, numValidPoints);
    
    if (numValidPoints < 30) {
        //fprintf(stderr, "WARNING! NOT ENOUGH VALID POINTS! numValid=%d\n", numValidPoints);
        free(points);
        *noPoints = 0;
        return NULL;
    }
    else {
        points = (smPoint *) realloc(points, numValidPoints * sizeof(smPoint));//crop memory down to size
        *noPoints = numValidPoints;
    }
    //adding laser offset
    BotTrans sensor_to_local_sensor_time;
    BotTrans local_to_body_comp_time;

    int status_1 = bot_frames_get_trans_with_utime(frames, frame, 
                                    "local",
                                    laser_msg->utime, 
                                    &sensor_to_local_sensor_time);

    int status_2 = bot_frames_get_trans_with_utime(frames, "local", 
                                    "body",
                                    utime, 
                                    &local_to_body_comp_time);

    if(!status_1 || !status_2){
        fprintf (stderr, "Error getting bot_frames transformation from %s to body!\n", chan);
        free (points);
        *noPoints = 0;
        return NULL;
    }
    
    //transform base map to particle's map frame - according to nd2 
    BotTrans sensor_to_body_frame;
    bot_trans_apply_trans_to(&local_to_body_comp_time, &sensor_to_local_sensor_time, &sensor_to_body_frame);
    
    double pos_s[3] = {0}, pos_b[3];
    for(int i=0; i < numValidPoints; i++){
        pos_s[0] = points[i].x;
        pos_s[1] = points[i].y;
        bot_trans_apply_vec(&sensor_to_body_frame, pos_s , pos_b);
        points[i].x = pos_b[0];
        points[i].y = pos_b[1];
    }

    //transform to body pose
    
    return points;
}

//USED - To get points from laser
smPoint *SlamParticleFilter::get_scan_points_from_laser(bot_core_planar_lidar_t *laser_msg, char *frame, int *noPoints){
    smPoint * points = (smPoint *) calloc(laser_msg->nranges, sizeof(smPoint));
    int numValidPoints = sm_projectRangesAndDecimate(beam_skip, spatialDecimationThresh, laser_msg->ranges,
                                                         laser_msg->nranges, laser_msg->rad0, laser_msg->radstep, points, 
                                                         maxRange, validBeamAngles[0], validBeamAngles[1]);
    *noPoints = numValidPoints;
           
    if (numValidPoints < 30) {
        fprintf(stderr, "WARNING! NOT ENOUGH VALID POINTS! numValid=%d\n", numValidPoints);
        free(points);
        *noPoints = 0;
        return NULL;
    }
    else {
        points = (smPoint *) realloc(points, numValidPoints * sizeof(smPoint));//crop memory down to size
    }
    //adding laser offset
    double sensor_to_body[12];
    if (!bot_frames_get_trans_mat_3x4 (frames, frame, 
                                       "body",
                                       sensor_to_body)) {
        fprintf (stderr, "Error getting bot_frames transformation from %s to body!\n", chan);
        free (points);
        *noPoints = 0;
        return NULL;
    }
    //transform to body pose
    double pos_s[3] = {0}, pos_b[3];
    for(int i=0; i < numValidPoints; i++){
        pos_s[0] = points[i].x;
        pos_s[1] = points[i].y;
        bot_vector_affine_transform_3x4_3d (sensor_to_body, pos_s , pos_b);
        points[i].x = pos_b[0];
        points[i].y = pos_b[1];
    }
    return points;
}



//USED
//process the laser observations 
int SlamParticleFilter::createNodeScan(laser_odom_t *l_odom){

    static char buf[1024];

    static int first_scan = 1; 
    static int first_laser_scan = 1;

    //rotate the cov to body frame
    double Rcov[9];
    Matrix3d cv(Rcov);
    
    bot_core_planar_lidar_t * laser_msg = l_odom->laser;
    bot_core_planar_lidar_t * r_laser_msg = l_odom->rlaser;
    int64_t utime = laser_msg->utime;
    int force_supernode = l_odom->force_supernode;
    static double dist_traveled = 0;
    static double angle_turned = 0;
    sm_rigid_transform_2d_t *odom = l_odom->odom;

    //get the odometry delta
    Pose2d curr_pose(odom->pos[0], odom->pos[1], odom->theta);
    Pose2d prev_pose(prev_added_odom->pos[0], prev_added_odom->pos[1], prev_added_odom->theta);
    
    //check if there is a door detected between these two points 
    bool transition_node = false;
    
    if(use_doorways && door_list){
        doorway_t *doorway = NULL;
        double pos_1[2] = {curr_pose.x(), curr_pose.y()};
        double pos_2[2] = {prev_pose.x(), prev_pose.y()};
        doorway = detect_doorway_between_pos(door_list, pos_1, pos_2);
        if(doorway != NULL){
            fprintf(stderr, CYAN "\n\n\n Door Detected \n\n\n" RESET_COLOR);
            if(verbose_node_level >= 0){
                sprintf(buf, "[Create Node] Door detected");
                output_writer->write_to_buffer(buf);
            }
            transition_node = true;
            destroy_door(doorway);
        }    
    }
    
    Pose2d prev_curr_tranf = curr_pose.ominus(prev_pose);

    double dist[2];
    sm_vector_sub_2d(odom->pos, prev_added_odom->pos, dist);

    double ld = sm_norm(SMPOINT(dist));
    double ad = sm_angle_subtract(odom->theta, prev_odom->theta);
    
    if (prev_added_odom != NULL)
        sm_rigid_transform_2d_t_destroy(prev_added_odom);
    prev_added_odom = sm_rigid_transform_2d_t_copy(odom);
    
    dist_traveled += fabs(ld);
    angle_turned += fabs(prev_curr_tranf.t());
    
    if(verbose){
        fprintf(stderr, "Scan is %f meters from last add, adding scan\n", ld);
        fprintf(stderr, "Scan is %f degrees from last add, adding scan\n", prev_curr_tranf.t()* 180.0/M_PI);
    }

    int numValidPoints = 0;
    smPoint * points = get_scan_points_from_laser(laser_msg, chan , &numValidPoints);

    if(points == NULL)
        return 0;
    
    int r_numValidPoints = 0;
    smPoint * r_points = NULL;
    if(r_laser_msg != NULL){
        //fprintf(stderr, MAKE_GREEN "Found rear laser\n" RESET_COLOR);
        r_points =  get_scan_points_from_laser_compensated(r_laser_msg, rchan , &r_numValidPoints, laser_msg->utime);
    }

    ScanTransform T;
    //just use the zero transform... it'll get updated the first time its needed
    memset(&T, 0, sizeof(T));

    Scan * scan;

    int no_of_points = numValidPoints + r_numValidPoints;
    smPoint *full_points =  (smPoint *) calloc(no_of_points, sizeof(smPoint));
    memcpy(full_points, points, sizeof(smPoint) * numValidPoints);
    memcpy(&full_points[numValidPoints], r_points, sizeof(smPoint) * r_numValidPoints);

    //fprintf(stderr, "Total no of Points : %d\n", no_of_points);

    //scan = new Scan(numValidPoints, points, T, SM_HOKUYO_UTM, utime, true);
    scan = new Scan(no_of_points, full_points, T, SM_HOKUYO_UTM, utime, true);
  
    //first node wont have a constraint with another node 
    //add locks here somewhere also - if needed
    
    int node_id = node_count;
    if(slave_mode){
        if (verbose)
            fprintf(stderr, "New node id : %d\n", (int) node_init->id);
        node_id = node_init->id;            
    }
    
    publish_slampose(scan, utime, node_id);
    
    //compute the distance between this scan and the last one that got added.

    double dr = hypot(prev_curr_tranf.x(), prev_curr_tranf.y());
    double alpha1 = atan2(prev_curr_tranf.y(), prev_curr_tranf.x());
    double alpha2 = prev_curr_tranf.t() - alpha1;
    
    //double lambda1 = 0.0001*0.0001, lambda2 = 0.00001, lambda3 = 0.00001, lambda4 = 0.0001*0.0001; 
    //these values were used before and seem too low 
    //double lambda1 = 0.001*0.001, lambda2 = 0.00001, lambda3 = 0.00001, lambda4 = 0.001*0.001; 

    double lambda1 = 0.005*0.005, lambda2 = 0.00005, lambda3 = 0.00005, lambda4 = 0.005*0.005; 
    
    Matrix3d V;
    V << -dr * sin(alpha1), dr, 0, dr * cos(alpha1), dr ,0, 1, 0, 1;

    Matrix3d M;
    
    M << lambda1 * pow(alpha1,2) + lambda2 * pow(dr,2), 0, 0, 
        0, lambda3 * pow(dr,2) + lambda4 * pow(alpha1,2) + lambda4 * pow(alpha2,2), 0, 
        0, 0, (lambda1 * pow(alpha2,2) + lambda2 * pow(dr,2));
    
    Matrix3d motion_cov = V * M * V.transpose();
    
    Noise *cov;
    if(first_laser_scan){
        cov =  new SqrtInformation(10000. * eye(3));
    }
    else{
        cov = new Covariance(motion_cov);
    }

    //cout << motion_cov << endl;

    //double cov_hardcode_odom[9] = { .00002, 0, 0, 0, .00002, 0, 0, 0, .00001 };//{ .0002, 0, 0, 0, .0002, 0, 0, 0, .0001 };
    //Matrix3d cv_hardcode_odom(cov_hardcode_odom);
    //cov = new Covariance(cv_hardcode_odom);
    
    //************* Do the incremental scan match **************//
    
    //do incremental scan matching to refine the odometry estimate
    Pose2d prev_sm_pose(prev_sm_odom->pos[0], prev_sm_odom->pos[1], prev_sm_odom->theta);
    Pose2d curr_sm_pose = prev_sm_pose.oplus(prev_curr_tranf);
    ScanTransform sm_prior;
    memset(&sm_prior, 0, sizeof(sm_prior));
    sm_prior.x = curr_sm_pose.x();
    sm_prior.y = curr_sm_pose.y();
    sm_prior.theta = curr_sm_pose.t();
    sm_prior.score = odometryConfidence; //wide prior
    
    int64_t s_utime = bot_timestamp_now();

    ScanTransform r = sm->matchSuccessive(points, numValidPoints, SM_HOKUYO_UTM, sm_get_utime(), false, &sm_prior);

    double sxx = r.sigma[0];
    double sxy = r.sigma[1];
    double syy = r.sigma[4];
    double stt = r.sigma[8];
    
    /*fprintf(stderr, BLUE "Scanmatch Score : %f Uncertainity :=> sxx=%f,sxy=%f,syy=%f,stt=%f\n" RESET_COLOR, r.hits / (double) numValidPoints, 
      sxx, sxy, syy, stt);*/

    int64_t e_utime = bot_timestamp_now();

    fprintf(stdout, "Time to Scanmatch : %f\n", (e_utime - s_utime) / 1.0e6);

    //we should check how far this is from the odometry - we should reject bad scanmatches that propose significant deviations 


    curr_sm_pose.set(r.x, r.y, r.theta);
    sm_rigid_transform_2d_t sm_odom;
    memset(&sm_odom, 0, sizeof(sm_odom));
    sm_odom.utime = laser_msg->utime;
    sm_odom.pos[0] = r.x;
    sm_odom.pos[1] = r.y;
    sm_odom.theta = r.theta;
    memcpy(sm_odom.cov, r.sigma, 9 * sizeof(double));
    if (prev_sm_odom != NULL)
        sm_rigid_transform_2d_t_destroy(prev_sm_odom);
    prev_sm_odom = sm_rigid_transform_2d_t_copy(&sm_odom);
        
    Pose2d prev_curr_sm_tranf = curr_sm_pose.ominus(prev_sm_pose);

    double dx = prev_curr_sm_tranf.x() - prev_curr_tranf.x();
    double dy = prev_curr_sm_tranf.y() - prev_curr_tranf.y();
    double dt = bot_mod2pi(prev_curr_sm_tranf.t() - prev_curr_tranf.t());

    int sm_error = 0;
    int sm_high_cov = 0;

    double hitpct = r.hits / (double) numValidPoints;

    //this fixes things too well - need to set this back on after the paper 
    if(fabs(dx) > 0.2 || fabs(dy) > 0.2 || fabs(dt) > bot_to_radians(10.0)){
        if(hitpct < 0.5){
            if(verbose_node_level >= 0){
                sprintf(buf, "[Create Node] [SM Error] Node %d Scanmatch %f Scanmatch TF : %f,%f,%f - Odom TF : %f,%f,%f", 
                        node_id, hitpct, prev_curr_sm_tranf.x(), prev_curr_sm_tranf.y(), prev_curr_sm_tranf.t(), 
                        prev_curr_tranf.x(), prev_curr_tranf.y(), prev_curr_tranf.t());
                output_writer->write_to_buffer(buf);
            }
            sm_high_cov = 1;
            sm_error = 1;
        }
        //sm_error = 1;
    }    
    
    double Rcov_sm[9];
    sm_rotateCov2D(r.sigma, -r.theta, Rcov_sm);
    Matrix3d cv_sm(Rcov_sm); 

    //Bianca - Covariance - previous to current constraint 
    Noise *cov_sm;
    if(useSMCov){
        if(first_laser_scan){
            cov_sm =  new SqrtInformation(10000. * eye(3));
        }
        else{
            cov_sm = new Covariance(cv_sm);
        }
    }
    else{
        if(first_laser_scan){
            cov_sm =  new SqrtInformation(10000. * eye(3));
        }
        else{
            //double cov_hardcode[9] = { .00002, 0, 0, 0, .00002, 0, 0, 0, .00001 };
            //used values earlier - { .0002, 0, 0, 0, .0002, 0, 0, 0, .0001 };
            if(sm_high_cov){
                double cov_hardcode[9] = { .002, 0, 0, 0, .002, 0, 0, 0, .001 };
                Matrix3d cv_hardcode(cov_hardcode);
                cov_sm = new Covariance(cv_hardcode);
            }
            else{
                //maybe this needs to be increased
                //double cov_hardcode[9] = { .00005, 0, 0, 0, .00005, 0, 0, 0, .00002 };
                double cov_hardcode[9] = { .0001, 0, 0, 0, .0001, 0, 0, 0, .00005 };
                Matrix3d cv_hardcode(cov_hardcode);
                cov_sm = new Covariance(cv_hardcode);
            }
            
        }
    } 

    bot_lcmgl_t *lcmgl = lcmgl_debug;
    if(0){
        if(last_scan != NULL){
            bot_lcmgl_t *lcmgl = lcmgl_debug;
            bot_lcmgl_point_size(lcmgl, 2);
            bot_lcmgl_begin(lcmgl, GL_POINTS);
            double pBody[3] = { 0, 0, 0 };
            double pLocal[3];
            bot_lcmgl_color3f(lcmgl, 1.0, 0.5, 0);
            for (unsigned i = 0; i < last_scan->numPoints; i++) {        
                pBody[0] = last_scan->points[i].x;
                pBody[1] = last_scan->points[i].y;
                bot_lcmgl_vertex3f(lcmgl, pBody[0], pBody[1], pBody[2]);
            }
            bot_lcmgl_end(lcmgl);
            
            BotTrans bodyToLocal;
            bodyToLocal.trans_vec[0] = prev_curr_sm_tranf.x();
            bodyToLocal.trans_vec[1] = prev_curr_sm_tranf.y();
            bodyToLocal.trans_vec[2] = 0;
            double rpy[3] = { 0, 0, prev_curr_sm_tranf.t()};
            bot_roll_pitch_yaw_to_quat(rpy, bodyToLocal.rot_quat);
            
            bot_lcmgl_point_size(lcmgl, 2);
            bot_lcmgl_begin(lcmgl, GL_POINTS);
            bot_lcmgl_color3f(lcmgl, 0, 0.5, 0.5);
            for (unsigned i = 0; i < scan->numPoints; i++) {        
                pBody[0] = scan->points[i].x;
                pBody[1] = scan->points[i].y;
                bot_trans_apply_vec(&bodyToLocal, pBody, pLocal);
                bot_lcmgl_vertex3f(lcmgl, pLocal[0], pLocal[1], pLocal[2]);                
            }
            bot_lcmgl_end(lcmgl);
            bot_lcmgl_switch_buffer(lcmgl);
        }
        delete last_scan;
    
        last_scan = new Scan(numValidPoints, points, T, SM_HOKUYO_UTM, utime, true);
    }

    // We are now done with points
    free (points);
    free (r_points);
    free (full_points);
    
    //not sure why this is published again
    //publish_slampose(scan, utime, node_id);
               
    if(sm_error){
        hitpct = 0.0;
    } 
    
    if(verbose_node_level >= 0){
        if(hitpct > config_params.incSMAcceptanceThreshold){
            sprintf(buf, "[Create Node] [SM] Node %d Scanmatch %f Scanmatch TF : %f,%f,%f - Odom TF : %f,%f,%f - Delta : %f, %f", node_id, hitpct, 
                    prev_curr_sm_tranf.x(), prev_curr_sm_tranf.y(), prev_curr_sm_tranf.t(), prev_curr_tranf.x(), prev_curr_tranf.y(), prev_curr_tranf.t(), hypot( prev_curr_sm_tranf.x() - prev_curr_tranf.x(), prev_curr_sm_tranf.y() - prev_curr_tranf.y()), prev_curr_sm_tranf.t() - prev_curr_tranf.t());
        }
        else{
            sprintf(buf, "[Create Node] [Odometry] Node %d Scanmatch %f Scanmatch TF : %f,%f,%f - Odom TF : %f,%f,%f - Delta : %f, %f", node_id, hitpct, 
                    prev_curr_sm_tranf.x(), prev_curr_sm_tranf.y(), prev_curr_sm_tranf.t(), prev_curr_tranf.x(), prev_curr_tranf.y(), prev_curr_tranf.t(), hypot( prev_curr_sm_tranf.x() - prev_curr_tranf.x(), prev_curr_sm_tranf.y() - prev_curr_tranf.y()), prev_curr_sm_tranf.t() - prev_curr_tranf.t());
        }
        output_writer->write_to_buffer(buf);
    }
    

    if (verbose)
        fprintf(stderr, "Node added => Node id : %d\n",node_id);

    //get the BotTrans - for local to body 
    BotTrans body_to_local;
    if(!bot_frames_get_trans_with_utime(frames, "body", "local", laser_msg->utime, &body_to_local)){
         if(verbose_node_level >= 0){
             sprintf(buf, "[Create Node] [Error] Error Getting Bot Trans");
             output_writer->write_to_buffer(buf);
         }
         //fprintf(stderr, "Error Getting Bot Trans\n");
    }
    
    if(l_odom->full_laser && l_odom->image){
        /*int64_t s_utime = bot_timestamp_now();
        get_classification(classifier, l_odom->full_laser, l_odom->image);
        int64_t e_utime = bot_timestamp_now();
        fprintf(stderr, RED "\n\n+++++++++++++++++ Classifying semantics (time) : %.4f (s) +++++++++++++++++\n\n" RESET_COLOR, (e_utime - s_utime)/1.0e6);*/
    }
    //add a new Slam node 
    NodeScan * slampose = new NodeScan(utime, node_id, scan, false, curr_pose, l_odom->laser_result, l_odom->image_result);
    
    last_node_utime = utime;

    slampose->transition_node = transition_node;

    if(first_laser_scan){
        slampose->transition_node = true;
        slampose->is_supernode = false;
    }

    first_laser_scan = 0;


    slampose->laser = bot_core_planar_lidar_t_copy(laser_msg);
    slampose->body_to_local = body_to_local;
    node_count++;         
    PoseToPoseTransform *constraint_sm = new PoseToPoseTransform(&prev_curr_sm_tranf, slampose, last_slampose, 
                                                                 SLAM_GRAPH_EDGE_T_TYPE_SM_INC, 
                                                                 cov_sm, cv_sm, hitpct, ld, ad, 1);//cov_hc);
    delete cov_sm;
    
    slampose->inc_constraint_sm = constraint_sm; 

    //odom constraint added 
    PoseToPoseTransform *constraint_odom = new PoseToPoseTransform(&prev_curr_tranf, slampose, last_slampose, 
                                                                   SLAM_GRAPH_EDGE_T_TYPE_ODOM_INC, cov, motion_cov, 1.0, ld, ad,  1);
    delete cov;
    //constraint is added to the current node (transform is respective to - i.e. in the previous node's pov)
    slampose->inc_constraint_odom = constraint_odom; 
    
    //output_writer->write_to_buffer("[Create Node] Created Node Scan", false, true, false);    

    //this can get blocked 
    g_mutex_lock(mutex);
    //fprintf(stderr, RED "Waiting on MUTEX\n" RESET_COLOR); 
    last_slampose = slampose;
    last_slampose_ind = slampose->node_id;
    if(slave_mode){
        vector<NodeScan *>::iterator it = slam_nodes_to_add.begin();
        slam_nodes_to_add.insert(it, slampose);    
        it = all_slam_nodes.begin();
        all_slam_nodes.insert(it, slampose);
    }
    else{
        if (verbose)
            fprintf (stdout, "----------------------adding slam node\n");
        vector<NodeScan *>::iterator it = slam_nodes_to_add.begin();
        slam_nodes_to_add.insert(it, slampose);
        it = all_slam_nodes.begin();
        all_slam_nodes.insert(it, slampose);
    }
    //fprintf(stderr, RED "Done waiting on MUTEX\n" RESET_COLOR); 
    g_mutex_unlock(mutex);

    //destroy the node init - after its handled
    if(node_init != NULL){
        slam_init_node_t_destroy(node_init);
        node_init = NULL;
    }

    return 1;
}


//USED 
/*
 * Publish the laser scan points out - for rendering 
 */



void SlamParticleFilter::publish_slampose(Scan *scan, int64_t utime, int64_t id){
    slam_laser_pose_t msg;
    msg.utime = utime;
    msg.id = id;
    msg.rp[0] = 0;
    msg.rp[1] = 0;
    msg.pl.no = scan->numPoints;
    msg.pl.points = (slam_scan_point_t *) calloc(scan->numPoints, sizeof(slam_scan_point_t));
    for (unsigned i = 0; i < scan->numPoints; i++) {  
        msg.pl.points[i].pos[0] =  scan->points[i].x;
        msg.pl.points[i].pos[1] =  scan->points[i].y;
    }
    slam_laser_pose_t_publish(lcm, "SLAM_POSE_LASER_POINTS", &msg);
    free(msg.pl.points);
}

void SlamParticleFilter::publish_slampose_list(){
    if(scan_request != NULL){
        slam_pixel_map_request_t_destroy(scan_request);
        scan_request = NULL;

        slam_laser_pose_list_t *msg = (slam_laser_pose_list_t  *) calloc(1,sizeof(slam_laser_pose_list_t));
        msg->utime = bot_timestamp_now();
        msg->no_poses = (int) added_slam_nodes.size();
        msg->scans = (slam_laser_pose_t *) calloc(msg->no_poses, sizeof(slam_laser_pose_t));
        for(int i=0; i < added_slam_nodes.size(); i++){
            added_slam_nodes[i]->getSlamposeMessage(msg->scans[i]);
        }

        slam_laser_pose_list_t_publish(lcm, "ISAM_LASER_SCANS", msg);
        slam_laser_pose_list_t_destroy(msg);
    }
}

bool SlamParticleFilter::is_valid_id(int64_t id){
    for (int i=0; i < slam_particles.size(); i++) {
        SlamParticle *particle = slam_particles.at(i);
        if(id == particle->graph_id){
            return true;
        }
    }    
    return false;
}

//USED 
//publishes occupancy map 
void SlamParticleFilter::publish_map_id(int64_t id){
    char buf[1024];
    for (int i=0; i < slam_particles.size(); i++) {
        SlamParticle *particle = slam_particles.at(i);
        if(id == particle->graph_id){
            int64_t s_utime = bot_timestamp_now();
            particle->printParticleInformation();
            particle->publishOccupancyMap();
            int64_t e_utime = bot_timestamp_now();
            
            sprintf(buf,"Publishing Occ-map for particle : %d (%.4f)", (int) id, (e_utime - s_utime)/ 1.0e6);
            output_writer->write_to_buffer(buf);
            
            break;
        }
    }    
}

//USED - Needs re-implementation for updated SLAM particle 
int SlamParticleFilter::process_for_nodes_and_language(int64_t utime, vector<NodeScan *> new_nodes, 
                                   map<int, slam_language_label_t *> new_language_labels, 
                                   vector<complex_language_event_t *> complex_language, 
                                   bool finishSlam_){
    char buf[1024];
    //get the graph 
    //update the slam particles by adding the new nodes and then adding edges 
    BotTrans body_to_laser;
    bot_frames_get_trans(frames, "body", chan, &body_to_laser);

    if(slam_particles.size() == 0){
        for(int i=0; i < no_particles; i++){
            SlamParticle *part = new SlamParticle(utime, next_id, 
                                                  lcm,
                                                  body_to_laser, 
                                                  label_info_particles, 
                                                  config_params, 
                                                  log_path, 
                                                  slu_classifier, 
                                                  local_px_map,
                                                  lcmgl_sm_basic, 
                                                  lcmgl_sm_graph, 
                                                  lcmgl_sm_prior, 
                                                  lcmgl_sm_result, 
                                                  lcmgl_sm_result_low, 
                                                  lcmgl_loop_closures);
            
            next_id++;
            
            slam_particles.push_back(part);
            slam_particles_map.insert(make_pair(part->graph_id, part));
        }
    }

    if(verbose_particle_level >=0){
        sprintf(buf, "[Particles] [Process Particles] [Language] Num of new language labels: %d" RESET_COLOR "\n", (int) new_language_labels.size());
        output_writer->write_to_buffer(buf);
    }

    double max_prob_particle = 0.0;
    
    int new_region_created = 0;
    // add language labels 
    int64_t s_utime = bot_timestamp_now();
    for(int i=0; i < slam_particles.size(); i++){
        double m_prob = 0; 

        SlamParticle *part = slam_particles.at(i);
        AddNodesAndEdgesResult result_info;
        int added_new_region = 0;

        if(verbose_particle_level >=2){
            sprintf(buf, "[Particles] [Process Particles] Particle : %d", part->graph_id);
            output_writer->write_to_buffer(buf);
        }

        added_new_region = part->addNodesAndEdges(new_nodes, new_language_labels, complex_language, probMode, find_ground_truth, &m_prob, ignoreLanguage, result_info, object_map, finishSlam_);

        if(added_new_region)
            new_region_created = 1;

        if(max_prob_particle < m_prob)
            max_prob_particle = m_prob;
        
    }

    int64_t e_utime_1 = bot_timestamp_now();
    if(new_nodes.size() > 0){
        last_graph_utime = new_nodes.at(new_nodes.size() - 1)->utime;
        last_added_node_id =  new_nodes.at(new_nodes.size() - 1)->node_id; 
    }

    if(q_info.timeoutOutstandingQuestion(last_added_node_id, QUESTION_INVALID_AFTER_NODES)){
        sprintf(buf, "[Particles] [Process Particles] [Answers] Question Timeout - inserting invalid answer");
        output_writer->write_to_buffer(buf);
        fprintf(stderr, "Question timeout\n");
        send_clear_dialog_msg();
    }
    
    //dont update the weights for this - this should be done when slu has returned 
    int send_complex_language = 0;
    
    for(int i=0; i < new_nodes.size(); i++){
        NodeScan *nd = new_nodes[i];
        added_slam_nodes.push_back(nd);
    }

    if(outstanding_complex_language.size() > 0){
        sprintf(buf, "[Particles] [Process Particles] [Complex Language] Outstanding Complex Language : %d", (int) outstanding_complex_language.size());
        output_writer->write_to_buffer(buf);
    }
    
    int64_t e_utime = bot_timestamp_now();
    if(verbose_particle_level >=0){
        sprintf(buf, "[Particles] [Process Particles] [Performance] Time to add edges : %.3f / Total Time : %.3f(nodes : %d)", 
                (e_utime_1 - s_utime) / 1.0e6, (e_utime - s_utime) / 1.0e6, (int) new_nodes.size());
        output_writer->write_to_buffer(buf);
    }
        
    if(max_prob_particle == 0)
        max_prob_particle = 0.1;
    
    double max_weight = -1e10;
    SlamParticle *max_particle = NULL;
    for(int i=0; i < slam_particles.size(); i++){
        SlamParticle *part = slam_particles.at(i);
        if(max_weight < part->weight){
            max_weight = part->weight;
            max_prob_particle_id = part->graph_id;
            max_particle = part;
        }
    }    
           
    //use this to reweight/resample
    if(resample){
        resample_particles(utime);
    }
    else{
        renormalize_particles();
        update_weights();
    }

    //we should do this after checking for resampling ?? 
    if(config_params.askQuestions && q_info.state == NO_QUESTION_SENT){
        slam_language_question_t *msg = get_best_question(max_particle);
            
        if(msg){
            if(verbose_particle_level >=0){
                sprintf(buf, "[Particles] [Process Particles] [Answers] Asking question %s", msg->question);
                output_writer->write_to_buffer(buf);
            }
            slam_language_question_t_publish(lcm, "SLAM_SR_QUESTION", msg);
            slam_language_question_t_destroy(msg);
        }
    }

    if(publish_map){
        if(requested_map_particle_id > 0 && is_valid_id(requested_map_particle_id)){
            publish_map_id(requested_map_particle_id);
        }
        else{
            if(max_prob_particle_id >= 0){
                publish_map_id(max_prob_particle_id);
            }
        }
    }
    
    
    return 1;
}

slam_language_question_t* SlamParticleFilter::get_best_question(SlamParticle *max_particle){
    if(config_params.question_type == USE_MAX_PARTICLE && max_particle){
        //this needs to change - such that we follow the same protocol - we need to get rid of this option though 
        return  max_particle->getQuestion();
    }
    else if(config_params.question_type == USE_ALL_PARTICLES){
        //go through each complex language events 
        
        //the node will be the same 
        //event_id, (sr, landmark) 
        
        SRAbstractQuestion *max_question = NULL; 
        for(int j=0; j < grounded_language.size(); j++){
            complex_language_event_t *event = grounded_language[j];
            map<spatialRelation, map<int, SRAbstractQuestion *> > event_questions;
            map<spatialRelation, map<int, SRAbstractQuestion *> >::iterator it_sr;
            for(int i=0; i < slam_particles.size(); i++){
                //for each language - for each particle - for each question type 
                SlamParticle *part = slam_particles.at(i);
                vector<SRQuestion> questions = part->getQuestionCost(event->language_event_id);

                //fprintf(stdout, "Question size : %d\n", (int) questions.size());

                for(int k=0; k < questions.size(); k++){
                    SRAbstractQuestion *a_question = NULL;
                    SRQuestion p_question = questions[k]; 

                    //add the actual question to the abstract question - what do we do if we resample?? 
                    int landmark_id = -1; 
                    if(p_question.landmark_region && p_question.landmark_region->mean_node){
                        landmark_id = p_question.landmark_region->mean_node->id;
                    }
                    
                    it_sr = event_questions.find(p_question.sr_result.sr);
                    if(it_sr != event_questions.end()){
                        map<int, SRAbstractQuestion *>::iterator it_landmark; 
                        it_landmark = it_sr->second.find(landmark_id);
                        if(it_landmark != it_sr->second.end()){
                            a_question = it_landmark->second;
                            a_question->addParticleQuestion(part->graph_id, p_question, exp(part->weight));
                        }
                        else{
                            a_question = new SRAbstractQuestion(event->language_event_id, p_question.sr_result.sr, 
                                                                p_question.node->id, p_question.getQuestionString(), landmark_id);
                            
                            a_question->addParticleQuestion(part->graph_id, p_question, exp(part->weight));
                            it_sr->second.insert(make_pair(landmark_id, a_question));
                        }
                    }
                    else{
                        a_question = new SRAbstractQuestion(event->language_event_id, p_question.sr_result.sr, 
                                                            p_question.node->id, p_question.getQuestionString(), landmark_id);
                        a_question->addParticleQuestion(part->graph_id, p_question, exp(part->weight));
                        map<int, SRAbstractQuestion *> lm_map;
                        lm_map.insert(make_pair(landmark_id, a_question));
                        event_questions.insert(make_pair(p_question.sr_result.sr, lm_map));
                    }
                }
            }
            
            //find the max value question for this event 
            for(it_sr=event_questions.begin(); it_sr != event_questions.end(); it_sr++){
                map<int, SRAbstractQuestion *>::iterator it_l;
                for(it_l = it_sr->second.begin(); it_l != it_sr->second.end(); it_l++){
                    SRAbstractQuestion *temp = it_l->second; 
                    if(!max_question){
                        max_question = temp;
                    }
                    else if(max_question->getAverageCost() > temp->getAverageCost()){
                        delete max_question;
                        max_question = temp;
                    }
                    else{
                        delete temp;
                    }
                }
            }
        }

        double cost_threshold = -1.0;

        if(max_question && max_question->getAverageCost() < cost_threshold){
            //this should set the question ID properly 
            fprintf(stdout, "Valid max question found : %f\n", max_question->getAverageCost());
            q_info.updateAskedQuestion(max_question, last_graph_utime);
            
            //add the questions to the particles 
            map<int, SRQuestion> particle_questions = max_question->getParticleQuestions();
            map<int, SRQuestion>::iterator it_q;
            for(int i=0; i < slam_particles.size(); i++){
                //for each language - for each particle - for each question type 
                SlamParticle *part = slam_particles.at(i);
                
                it_q = particle_questions.find(part->graph_id);

                if(it_q != particle_questions.end()){
                    part->updateAskedQuestion(it_q->second);
                    fprintf(stdout, "Updating question for particle :%d\n", part->graph_id);
                }
                else{
                    fprintf(stderr, "Error - couldn't find question for particle :%d\n", part->graph_id);
                }
            }
            //get the message 
            return max_question->get_question();
        }
        else if(max_question){
            fprintf(stdout, "Max question cost too high : %f\n", max_question->getAverageCost());
        }
    }

    return NULL;
}

void SlamParticleFilter::update_particles_with_answers(){
    
    SRAbstractQuestion *answered_question = q_info.getAnsweredQuestionAndClear();
    
    if(answered_question){        
        fprintf(stdout, "Found answered question\n");
        
        for(int i=0; i < slam_particles.size(); i++){
            SlamParticle *p = slam_particles.at(i);

            int result = p->updateAnswer(answered_question->question_id, answered_question->answer_string);
            fprintf(stdout, "Integrating Answer to question %d - for particle : %d\n", answered_question->question_id, p->graph_id);
        }

    }
}

//USED 
//Renormalizes the particle weights 
int SlamParticleFilter::renormalize_particles(){

    //normalize weights
    if (verbose)
        fprintf(stderr, "Before renormalizing\n");
    double sum_weights = 0.0;
    for(int i=0; i < slam_particles.size(); i++){
        SlamParticle *p = slam_particles.at(i);
        sum_weights += exp(p->weight);
    }
    if(sum_weights == 0){
        fprintf(stderr, "Error! Sum of weights is 0! - assigning equal weights");
        for(int i=0; i < slam_particles.size(); i++){
            SlamParticle *p = slam_particles.at(i);
            p->normalized_weight = log(1.0/(double) slam_particles.size());
            p->weight = p->normalized_weight;
        } 
        return 1; 
    }

    //renormalizing the weights - otherwise things can get too small and mess up 
    
    if (verbose)
        fprintf(stderr, "After renormalizing\n");
    for(int i=0; i < slam_particles.size(); i++){
        SlamParticle *p = slam_particles.at(i);
        p->normalized_weight = p->weight - log(sum_weights);
        p->weight = p->normalized_weight;
    } 
   
    return 1; //succeeded at renormalizing
}

//USED - not sure what this fuction is supposed to do 
void SlamParticleFilter::update_weights(){
    if(weights != NULL){
        slam_graph_weight_list_t_destroy(weights);
    }
    
    weights = (slam_graph_weight_list_t *) calloc(1,sizeof(slam_graph_weight_list_t));
    //we reset the time when we publish along with the particles 
    weights->utime = bot_timestamp_now(); 
    weights->no_particles = slam_particles.size();
    weights->weights_before = (slam_graph_weight_t *) calloc(weights->no_particles, sizeof(slam_graph_weight_t));
    weights->weights_after = (slam_graph_weight_t *) calloc(weights->no_particles, sizeof(slam_graph_weight_t));

    set<pair<int, int> > curr_dist_lc, curr_lang_lc;

     //calculate N_eff
    double sum_sq_weights = 0.0;

    for(int i=0; i < slam_particles.size(); i++){
        SlamParticle *p = slam_particles.at(i);
        sum_sq_weights += pow(exp(p->normalized_weight),2);
        //p->addLoopClosuresSet(&dist_lc, &lang_lc);
        p->addLoopClosuresSet(dist_lc, lang_lc);
        p->addLoopClosuresSet(&curr_dist_lc, &curr_lang_lc);
        pair<int,int> lc_counts = p->getNoOfLoopClosures();
        weights->weights_before[i].id = p->graph_id;
        weights->weights_before[i].no_distance_lc = lc_counts.first;
        weights->weights_before[i].no_lang_lc = lc_counts.second;
        weights->weights_before[i].weight = p->normalized_weight;
        weights->weights_before[i].pofz = 0; //not set for now
    }

    double n_eff = 1.0 / sum_sq_weights;
    double threshold  = slam_particles.size() / 2.0;//slam_particles.size()/2.0; 
    weights->status = SLAM_GRAPH_WEIGHT_LIST_T_STATUS_NO_RESAMPLE;
    weights->n_effective = n_eff;

    for(int i=0; i < slam_particles.size(); i++){
        SlamParticle *p = slam_particles.at(i);
        p->weight = log(1/(double) slam_particles.size());
        weights->weights_after[i].id = p->graph_id;
        pair<int,int> lc_counts = p->getNoOfLoopClosures();
        weights->weights_after[i].no_distance_lc = lc_counts.first;
        weights->weights_after[i].no_lang_lc = lc_counts.second;
        weights->weights_after[i].weight = p->normalized_weight;
        weights->weights_after[i].pofz = 0; //not set for now
    }
}

//USED - This is the function used right now - but is probably broken 
int SlamParticleFilter::resample_particles(int64_t utime){

    //fprintf(stderr, MAKE_GREEN "this will show up green" RESET_COLOR "\n");
    //does this kill the diversity??
    char buf[1024];

    renormalize_particles();

    if(weights != NULL){
        slam_graph_weight_list_t_destroy(weights);
    }
    
    weights = (slam_graph_weight_list_t *) calloc(1,sizeof(slam_graph_weight_list_t));
    //we reset the time when we publish along with the particles 
    weights->utime = bot_timestamp_now(); 
    weights->no_particles = slam_particles.size();
    weights->weights_before = (slam_graph_weight_t *) calloc(weights->no_particles, sizeof(slam_graph_weight_t));
    weights->weights_after = (slam_graph_weight_t *) calloc(weights->no_particles, sizeof(slam_graph_weight_t));


    //calculate N_eff
    double sum_sq_weights = 0.0;
    //lc's in the current particles (ignores ones that were cut out in resampling
    set<pair<int, int> > curr_dist_lc, curr_lang_lc;
    for(int i=0; i < slam_particles.size(); i++){
        SlamParticle *p = slam_particles.at(i);
        sum_sq_weights += pow(exp(p->normalized_weight),2);
        //p->addLoopClosuresSet(&dist_lc, &lang_lc);
        p->addLoopClosuresSet(dist_lc, lang_lc);
        p->addLoopClosuresSet(&curr_dist_lc, &curr_lang_lc);
        pair<int,int> lc_counts = p->getNoOfLoopClosures();
        weights->weights_before[i].id = p->graph_id;
        weights->weights_before[i].no_distance_lc = lc_counts.first;
        weights->weights_before[i].no_lang_lc = lc_counts.second;
        weights->weights_before[i].weight = p->normalized_weight;
        weights->weights_before[i].pofz = 0; //not set for now
    }
    
    weights->no_dist_lc = dist_lc->size();
    weights->no_lang_lc = lang_lc->size();

    weights->curr_no_dist_lc = curr_dist_lc.size();
    weights->curr_no_lang_lc = curr_lang_lc.size();
    //we need to also give a measure of how many lc's there were in total - which we can 
    //only do by checking the lc node pairs 

    double n_eff = 1.0 / sum_sq_weights;
    double threshold  = slam_particles.size()/ 2.0;//slam_particles.size()/2.0; 
    weights->status = SLAM_GRAPH_WEIGHT_LIST_T_STATUS_NO_RESAMPLE;
    weights->n_effective = n_eff;
    //if n_eff value greater than critical value
    if(n_eff <= threshold){
        weights->status = SLAM_GRAPH_WEIGHT_LIST_T_STATUS_RESAMPLE;
        //fprintf(stderr, "\n\n+++++++++++++Resampling+++++++++++++++++++\n");
        if(verbose_particle_level >=0){
            sprintf(buf, "[Particles] [Resampling] N Eff : %f - Threshold : %f", n_eff, threshold);
            output_writer->write_to_buffer(buf);
        }

        //create a new vector
        vector<SlamParticle *> new_slam_particles;
        //sample from particles
        double *weights_d = new double[slam_particles.size()];
        for(int i=0; i<slam_particles.size(); i++){
            if(i==0)
                weights_d[i] = exp(slam_particles.at(i)->normalized_weight);
            else    
                weights_d[i] = weights_d[i-1]+ exp(slam_particles.at(i)->normalized_weight);
        }

        if(verbose_particle_level >=2){
            for(int i=0; i<slam_particles.size(); i++){
                sprintf(buf, "[Particles] [Resampling] Particle id : %d => Cumalative weight : %f",  i, weights_d[i]);
                output_writer->write_to_buffer(buf);
            }
        }
        
        int *par_ids = new int[slam_particles.size()];
        for(int i=0; i<slam_particles.size(); i++)
            par_ids[i] = 0;

        for(int i=0; i<slam_particles.size(); i++){
            double r = rand() / (double) RAND_MAX;
            for(int j=0; j<slam_particles.size(); j++){
                if(j==0){
                    if(r < weights_d[j])
                        par_ids[j]++;
                }
                else{
                    if(r > weights_d[j-1] && r < weights_d[j])
                        par_ids[j]++;
                }                
            }
        }

        vector<SlamParticle *> to_adjust_particles;

        for(int i=0; i<slam_particles.size(); i++){
            int n = par_ids[i];

            if(n ==0){
                to_adjust_particles.push_back(slam_particles.at(i));
                //add to a list to be adjusted 
            }
        }

        for(int i=0; i<slam_particles.size(); i++){
            int n = par_ids[i];
            //fprintf(stderr, "New particles : %d -> %d => weight : %f\n", i, n, slam_particles.at(i)->weight);
            if(verbose_particle_level >=2){
                sprintf(buf, "[Particles] [Resampling] New particles : %d -> %d => weight : %f", i, n, slam_particles.at(i)->weight);
                output_writer->write_to_buffer(buf);
            }
            if(n >0){
                //keep the old particle 
                new_slam_particles.push_back(slam_particles.at(i));
                       
                if(n > 1){              
                    //create copies of the same particle and add to the list
                    map<int, SlamParticle *>::iterator p_it;
                    for(int j=0; j<n-1; j++){
                        //remove from the map 
                        int p_id = to_adjust_particles.at(to_adjust_particles.size()-1)->graph_id;
                        p_it = slam_particles_map.find(p_id);
                        assert(p_it != slam_particles_map.end()); //we should be able to find it 
                        slam_particles_map.erase(p_it);
                        to_adjust_particles.at(to_adjust_particles.size()-1)->adjust(next_id, utime, slam_particles.at(i));
                        //insert back to map 
                        slam_particles_map.insert(make_pair(to_adjust_particles.at(to_adjust_particles.size()-1)->graph_id, to_adjust_particles.at(to_adjust_particles.size()-1))); 
                        to_adjust_particles.pop_back();
                        next_id++;
                    }
                }
            }
        }
        
        for(int i=0; i < slam_particles.size(); i++){
            SlamParticle *p = slam_particles.at(i);
            p->weight = log(1/(double) slam_particles.size());
            weights->weights_after[i].id = p->graph_id;
            pair<int,int> lc_counts = p->getNoOfLoopClosures();
            weights->weights_after[i].no_distance_lc = lc_counts.first;
            weights->weights_after[i].no_lang_lc = lc_counts.second;
            weights->weights_after[i].weight = p->normalized_weight;
            weights->weights_after[i].pofz = 0; //not set for now
        }


        if(verbose_particle_level >=0){
            sprintf(buf, "[Particles] [Resampling] No of samples : %d - weight : %f\n", (int) slam_particles.size(),
                    log(1/(double) slam_particles.size()));
            output_writer->write_to_buffer(buf);
        }

        delete par_ids;
        delete weights_d;

        return 1; //did resample
    }
    
    for(int i=0; i < slam_particles.size(); i++){
        SlamParticle *p = slam_particles.at(i);
        weights->weights_after[i].id = p->graph_id;
        weights->weights_after[i].weight = p->normalized_weight;
        weights->weights_after[i].pofz = 0; //not set for now
    }

    return 0; //didn't resample
}

void SlamParticleFilter::clear_dead_edges(){
    for(int i=0; i < slam_particles.size(); i++){
        SlamParticle *part = slam_particles.at(i);
        part->clearDummyConstraints();
    }
}

void SlamParticleFilter::send_clear_dialog_msg(){
    slam_language_question_t msg; 
    msg.utime = bot_timestamp_now(); 
    msg.id = -1;
    msg.particle_id = -1; 
    msg.question = (char *) "";
    slam_language_question_t_publish(lcm, "SLAM_SR_QUESTION", &msg);
}

//USED 
//returns 0 if no particles or no new message
int SlamParticleFilter::process_graph_particles(){
    static char buf[1024];
    //copy over the remaining nodes - before processing the particles 
    GHashTableIter iter;
    gpointer key, value;
    
    if(verbose_particle_level >= 2){
        output_writer->write_to_buffer("[Particles Thread] Processing graph particles", false, true, false);               
    }
    
    g_mutex_lock(mutex);    
    
    //check if there are any requests for a map 
    if(map_request != NULL){
        requested_map_particle_id = map_request->particle_id;
        publish_map_id(map_request->particle_id);
        slam_pixel_map_request_t_destroy(map_request);
        map_request = NULL;
    }
    
    publish_slampose_list();
    
    //check for any language results we were waiting for 
    int64_t s_utime = bot_timestamp_now();
    vector<NodeScan *> new_nodes;
            
    int added = get_new_slam_nodes(new_nodes);//update_slam_nodes(app);

    static int last_node_added = 0;
  
    if (added > 0){
        check_object_to_closest_node();
        last_node_added = new_nodes[new_nodes.size() - 1]->node_id;
        if(verbose){
            fprintf(stderr, "Processing Slam poses - To Add : %d\n", added);
            fprintf (stdout, "--------- added = %d\n", added);
            fprintf(stderr, "Last node added : %d\n", last_node_added);
        }
    }

    vector<slam_language_label_t *>::iterator itr;

    //we should keep the complex language in an outstanding queue 
       
    map<int, slam_language_label_t*> node_to_label_map;

    vector<slam_language_label_t *> unhandled_language;

    int count = 0;
    for (itr = language_labels_to_add.begin(); itr != language_labels_to_add.end(); itr++) {
        slam_language_label_t *label = (slam_language_label_t *) *itr;    

        int remove = 0;

        for (int j=0; j < new_nodes.size(); j++) {
            NodeScan *node_scan = new_nodes.at(j);
            //fprintf (stdout, "Checking new node with delta t = %.2f\n",
            //((double)(node_scan->utime - label->utime))/1E6);

            if (node_scan->utime >= label->utime) {
                if(label->constraint_type == SLAM_LANGUAGE_LABEL_T_TYPE_SIMPLE){
                    //add this to the NodeScan ??  - there should be only one of these vs complex language labels
                    received_language_t *new_lang = (received_language_t *) calloc(1,sizeof(received_language_t));
                    new_lang->node_id = node_scan->node_id; 
                    new_lang->label = slam_language_label_t_copy(label);
                    grounded_simple_language.push_back(new_lang);
                    node_to_label_map.insert(make_pair(node_scan->node_id, label));
                }
                else{
                    //fprintf(stderr, "+++++ Error - Complex language should not be here +++++\n");
                }
                remove = 1;
                //fprintf (stdout, "Found new node with delta t = %.2f\n",
                //((double)(node_scan->utime - label->utime))/1E6);
                break;
            }
        }

        //fprintf(stderr, "Done - Found : %d\n", remove);
        if (remove == 0)
            unhandled_language.push_back(label);
        count++;
        //fprintf(stderr, "Count : %d\n", count++);
    }

    language_labels_to_add = unhandled_language;

    vector<slam_slu_parse_language_querry_t *> new_complex_language_labels;
    vector<slam_slu_parse_language_querry_t *> unhandled_complex_language;
    vector<slam_slu_parse_language_querry_t *>::iterator itr_c;

    //handle the complex language 
    vector<complex_language_event_t *> new_complex_language;
    int complex_language_found = 0;
    for (itr_c = complex_language_labels_to_add.begin(); itr_c != complex_language_labels_to_add.end(); itr_c++) {
        slam_slu_parse_language_querry_t *label = (slam_slu_parse_language_querry_t *) *itr_c;    

        int remove = 0;
       
        for (int j=0; j < new_nodes.size(); j++) {
            NodeScan *node_scan = new_nodes.at(j);
            if (node_scan->utime >= label->utime) {
                fprintf(stderr, "+++++ Adding complex language +++++\n");
                complex_language_found++;
                //lets not add this to this map 
                complex_language_event_t *complex_lang_pair = (complex_language_event_t *) calloc(1, sizeof(complex_language_event_t));
                complex_lang_pair->node_id = node_scan->node_id;
                complex_lang_pair->parse_result = label;
                complex_lang_pair->language_event_id = complex_language_index;
                if(ignoreLanguage){
                    grounded_language.push_back(complex_lang_pair);
                }
                else{
                    outstanding_complex_language.push_back(complex_lang_pair);
                    //this kind of assumes that there is only one complex language for each node
                    //this will be grounded internally
                    if(config_params.useSLULibrary){
                        grounded_language.push_back(complex_lang_pair);
                    }
                    
                    new_complex_language.push_back(complex_lang_pair);
                }
                complex_language_index++;
            
                remove = 1;
                //fprintf (stdout, "Found new node with delta t = %.2f\n",
                //((double)(node_scan->utime - label->utime))/1E6);
                break;
            }
        }

        //fprintf(stderr, "Done - Found : %d\n", remove);
        if (remove == 0)
            unhandled_complex_language.push_back(label);
    }

    //should we finish SLAM 

    bool finishSlam_ = false;
    g_mutex_lock(mutex_finish_slam);
    finishSlam_ = finishSlam;
    g_mutex_unlock(mutex_finish_slam);


    complex_language_labels_to_add = unhandled_complex_language;
    int64_t e_utime = bot_timestamp_now();
    g_mutex_unlock(mutex);

    if(added){
        sprintf(buf, "[Particles Thread] [Performance] Time to add nodes : %.3f", (e_utime - s_utime) / 1.0e6);
        output_writer->write_to_buffer(buf);
    }

    //update the answers 
    update_particles_with_answers();
    
    if(added == 0 && node_to_label_map.size() == 0 && complex_language_found == 0 && !finishSlam_) {
        return 0; 
    }

    int64_t now = bot_timestamp_now();
    last_node_sent_utime = now;
    if(verbose)
        fprintf(stderr, "----------- First node set ---------- \n");
    
    // if we have any matched complex language - process it differently 
    // This will add nodes, process the simple language and distance based edges and then 
    // query SLU for the complex language 
    // After the SLU result - we will update the graph with the new langauge labels 
    // then propose new edges - and then calculate the measurements
    
    if(verbose_particle_level >= 0)
        output_writer->write_to_buffer("[Particles Thread] Calling process nodes", false, true, false);

    process_for_nodes_and_language(last_node_sent_utime, new_nodes, 
                                   node_to_label_map, new_complex_language, finishSlam_);

    map<int, slam_language_label_t*>::iterator it_label_map;
    for(it_label_map = node_to_label_map.begin(); it_label_map != node_to_label_map.end(); it_label_map++){
        slam_language_label_t_destroy(it_label_map->second);
    }
    
    //destroy the grounded language

    //for drawing the loop closures 
    if(added == 2)
        bot_lcmgl_switch_buffer(lcmgl_loop_closures);
        
    publish_slam_region_particles();
    publish_slam_progress();
    publish_language_collection_list();
    //calculate the transforms ??
    publish_slam_transforms();

    if(verbose_particle_level >= 2)
        output_writer->write_to_buffer("[Particles Thread] Done", false, true, true);
    
    output_writer->write_buffer();

    if(keepDeadEdges ==0){
        clear_dead_edges();
    }
        
    // publish occupancy grid maps
    if (publish_occupancy_maps) {
        double dt = (now - publish_occupancy_maps_last_utime)/1000000.0;
        if (dt > MAP_PUBLISH_PERIOD || finishSlam) {
            //fprintf(stderr, "Publishing Max Map\n");
            publish_occupancy_map_msg ();
            publish_occupancy_maps_last_utime = now;
        }
    }

    if(finishSlam){
        fprintf(stdout, "Finish Slam heard - exiting\n");
        still_groovy = 0;
        output_writer->write_to_buffer("[Particles Thread] Exiting SLAM", false, true, true);
    }

    return 1;
}

//this should be done in an intelligent manner - after querrying if we have any outstanding language 
int SlamParticleFilter::publish_outstanding_language_querries(){
    //check if there is any outstanding language 
    
    bool outstanding = false;

    for(int i=0; i < slam_particles.size(); i++){
        SlamParticle *part = slam_particles.at(i);
        if(part->hasOutstandingLanguage()){
            outstanding = true;
            break;
        }
    }

    if(!outstanding){
        fprintf(stderr, "No outstanding language\n");
        return 0;    
    }

    vector<SlamParticle *> unique_particles = get_unique_particles();

    //we should send this out now and 
    //count_different_particles(app);

    fprintf(stderr, "+++++++++++ Sending Outstanding language querries ++++++++++++\n");

    slam_complex_language_collection_list_t *msg = (slam_complex_language_collection_list_t *) calloc(1, sizeof(slam_complex_language_collection_list_t));
    msg->utime = bot_timestamp_now();
    msg->no_particles = unique_particles.size();

    msg->collection = (slam_complex_language_collection_t *) calloc(msg->no_particles, sizeof(slam_complex_language_collection_t));
    
    for(int i=0; i < unique_particles.size(); i++){
        SlamParticle *part = unique_particles.at(i);
        part->fillOutstandingLanguageCollection(msg->collection[i]);
    }
    slam_complex_language_collection_list_t_publish(lcm, "SLU_COMPLEX_LANGUAGE_QUERRY", msg);
    slam_complex_language_collection_list_t_destroy(msg);
    
    return 1;
}


//this should be done in an intelligent manner - after querrying if we have any outstanding language 
int SlamParticleFilter::evaluate_outstanding_language_querries(){
    if(!slu_classifier)
        return -1;

    //check if there is any outstanding language 
    
    bool outstanding = false;

    for(int i=0; i < slam_particles.size(); i++){
        SlamParticle *part = slam_particles.at(i);
        if(part->hasOutstandingLanguage()){
            outstanding = true;
            break;
        }
    }

    if(!outstanding){
        fprintf(stderr, "No outstanding language\n");
        return 0;    
    }

    //lets just do this internally 



    vector<SlamParticle *> unique_particles = get_unique_particles();
    
    //we should send this out now and 
    //count_different_particles(app);

    fprintf(stderr, "+++++++++++ Sending Outstanding language querries ++++++++++++\n");

    slam_complex_language_collection_list_t *msg = (slam_complex_language_collection_list_t *) calloc(1, sizeof(slam_complex_language_collection_list_t));
    msg->utime = bot_timestamp_now();
    msg->no_particles = unique_particles.size();

    msg->collection = (slam_complex_language_collection_t *) calloc(msg->no_particles, sizeof(slam_complex_language_collection_t));
    
    for(int i=0; i < unique_particles.size(); i++){
        SlamParticle *part = unique_particles.at(i);
        part->fillOutstandingLanguageCollection(msg->collection[i]);
    }
    slam_complex_language_collection_list_t_publish(lcm, "SLU_COMPLEX_LANGUAGE_QUERRY", msg);
    slam_complex_language_collection_list_t_destroy(msg);
    
    return 1;
}

//this should be done in an intelligent manner - after querrying if we have any outstanding language 
int SlamParticleFilter::publish_outstanding_language_querries_all_particles(){
    //check if there is any outstanding language 
    
    //bool outstanding = false;

    vector<SlamParticle *> outstanding_particles;
    //this should actually only send out querries for particles that have querries 
    for(int i=0; i < slam_particles.size(); i++){
        SlamParticle *part = slam_particles.at(i);
        if(part->hasOutstandingLanguage()){
            //outstanding = true;
            //break;
            outstanding_particles.push_back(part);
        }
    }

    if(outstanding_particles.size() == 0){//!outstanding){
        fprintf(stderr, "No outstanding language\n");
        return 0;    
    }

    //we should send this out now and 
    //count_different_particles(app);

    fprintf(stderr, "+++++++++++ Sending Outstanding language querries ++++++++++++\n");

    slam_complex_language_collection_list_t *msg = (slam_complex_language_collection_list_t *) calloc(1, sizeof(slam_complex_language_collection_list_t));
    msg->utime = bot_timestamp_now();
    msg->no_particles = (int) outstanding_particles.size();

    msg->collection = (slam_complex_language_collection_t *) calloc(msg->no_particles, sizeof(slam_complex_language_collection_t));
    
    for(int i=0; i < outstanding_particles.size(); i++){
        SlamParticle *part = outstanding_particles.at(i);
        part->fillOutstandingLanguageCollection(msg->collection[i]);
    }
    slam_complex_language_collection_list_t_publish(lcm, "SLU_COMPLEX_LANGUAGE_QUERRY", msg);
    slam_complex_language_collection_list_t_destroy(msg);
    
    return 1;
}

void SlamParticleFilter::print_language_stats(){
    //do this only for the max particle 

     for(int i=0; i < slam_particles.size(); i++){
         SlamParticle *part = slam_particles.at(i);
         fprintf(stdout, "[Complex Lang] [Results] Entropy Slam Particle : %d\n", part->graph_id);
         part->printEntropyStats();
     }
}

void SlamParticleFilter::publish_language_collection_list(){
    bool has_language = false;

    for(int i=0; i < slam_particles.size(); i++){
        SlamParticle *part = slam_particles.at(i);
        if(part->hasLanguagePaths()){
            has_language = true;
            break;
        }
    }

    if(!has_language)
        return;    

    slam_complex_language_collection_list_t *msg = (slam_complex_language_collection_list_t *) calloc(1, sizeof(slam_complex_language_collection_list_t));
    msg->utime = bot_timestamp_now();
    msg->no_particles = slam_particles.size();
    msg->collection = (slam_complex_language_collection_t *) calloc(msg->no_particles, sizeof(slam_complex_language_collection_t));
    
    for(int i=0; i < slam_particles.size(); i++){
        SlamParticle *part = slam_particles.at(i);
        part->fillLanguageCollection(msg->collection[i]);
    }
    slam_complex_language_collection_list_t_publish(lcm, "COMPLEX_LANGUAGE_PATHS", msg);
    slam_complex_language_collection_list_t_destroy(msg);

    print_language_stats();
}

//USED 
//go through the vector - and pop the old stuff up to the last supernode
//if there are no new nodes - don't update 
int SlamParticleFilter::get_new_slam_nodes(vector<NodeScan *> &new_node_list){
    int max_new_nodes = 5;
    int min_nodes = 3;

    bool has_sn = false;

    if(slam_nodes_to_add.size() == 0)
        return 0;

    int has_supernode = 0;
    int added_new_node = 0;
    //go through the new pose queue - and copy over nodes upto the first supernode 
    while (!slam_nodes_to_add.empty()){
        NodeScan *pose = slam_nodes_to_add.back();
        slam_nodes_to_add.pop_back();
        if (verbose)
            fprintf(stderr, "Slam node id : %d => Super : %d\n", pose->node_id, pose->is_supernode);
        if(pose != NULL){
            new_node_list.push_back(pose);
            added_new_node = 1;
            last_slampose_for_process_ind = pose->node_id; //this is not used - so we should take out at some point 
            if(pose->is_supernode){
                has_supernode = 1;
                break;
            }
        }
        if(new_node_list.size() >= max_new_nodes)
            break;
    }

    //now go through the app transition list - see if there are any nodes that occured after a transition 
    //for(size_t i=0; i < transitions.size(); i++){
    if(transitions.size() > 0){
        std::vector<slam_region_transition_t *>::iterator it = transitions.begin();
    
        while(it != transitions.end()){
            int64_t transition_utime = (*it)->utime;
            int found_node = 0;

            for(size_t j=0; j < new_node_list.size(); j++){
                if(new_node_list.at(j)->utime > transition_utime){
                    if (verbose)
                        fprintf(stderr, "\n\n\n+++++++++++++++++Found transition node - breaking+++++++++++++++++\n");
                    new_node_list.at(j)->transition_node = true;
                    //this is no longer set - so it will take out the entire set of nodes 
                    new_node_list.at(j)->is_supernode = true;
                    found_node = 1;
                    break;
                }            
            }
            if(found_node){ //remove the transition - so that it doesn't label others 
                slam_region_transition_t_destroy(*it);
                transitions.erase(it);
            }
            else{
                ++it;
            }
        }
    }

    if(has_supernode)
        return 2;
    else if(added_new_node)
        return 1;
    else
        return 0;
}

void SlamParticleFilter::check_object_to_closest_node(){
    //if the object was detected before the last NodeScan - then it should be added to the node with the 
    //closest time 

    //otherwise - it should be kept in the outstanding list 
    //search backwards 
    //should we just keep the closest detection with time?? - as this is the one that should have the 
    //least drift??
    
    vector<RawDetection *> remaining_outstanding;

    int break_ind = -1;

    //fprintf(stderr, "No of Outstanding Objects : %d\n", (int) outstanding_object_detections.size());

    //if we want to be efficient 
    //have a map<NodeScan *, RawDetection *> node_close_observation;
    map<ObjectDetection *, map<NodeScan *, RawDetection *> > node_object_observations;
    map<ObjectDetection *, map<NodeScan *, RawDetection *> >::iterator it_object;
    map<NodeScan *, RawDetection *>::iterator it_obs;
    char buf[1024];
    for(int j=0; j < outstanding_object_detections.size(); j++){
        perception_object_detection_t *detection = outstanding_object_detections[j]->observation; 
        NodeScan *matching_node = NULL;
        
        //if the object was detected before the last node - then find the closest node in terms of time 
        //and add the object to that node scan 
        double time_gap = 1000;
        //this is in reverse order - latest first 
        
        if(all_slam_nodes.size() > 0 && all_slam_nodes[0]->utime < detection->utime_last_seen){
            //fprintf(stderr, "Nodes are all older than the current detection - breaking\n");
            break_ind = j;
            break;                
        }

        for(int i= 0; i < all_slam_nodes.size(); i++){
            NodeScan *nd = all_slam_nodes[i];
            
            double deltat = (nd->utime - detection->utime_last_seen)/1.0e6;
            
            if(deltat < 0){ //object was created after this node was created - don't search earlier than this 
                if(fabs(time_gap) > fabs(deltat)){
                    //this is the node we want - set it 
                    matching_node = nd;
                    break;
                }
            }
            else{
                time_gap = deltat;
            }
        }
        
        if(matching_node){
            //fprintf(stderr, "Found matching node : %d => Time gap : %f\n", matching_node->node_id, time_gap);
            //add this to the object node map 
            it_object = node_object_observations.find(outstanding_object_detections[j]->object); 
            
            if(it_object == node_object_observations.end()){
                map<NodeScan *, RawDetection *> nd_obs_map;
                nd_obs_map.insert(make_pair(matching_node, outstanding_object_detections[j]));
                node_object_observations.insert(make_pair(outstanding_object_detections[j]->object, nd_obs_map));

                sprintf(buf, "[Objects] No Entry found for Object : %d - Adding for node : %d",outstanding_object_detections[j]->object->id,  matching_node->node_id);
                output_writer->write_to_buffer(buf);
            }
            else{
                it_obs = it_object->second.find(matching_node);
                if(it_obs == it_object->second.end()){
                    it_object->second.insert(make_pair(matching_node, outstanding_object_detections[j]));
                    sprintf(buf, "[Objects] No Entry found for %d node for Object : %d - Adding", matching_node->node_id, outstanding_object_detections[j]->object->id);
                    output_writer->write_to_buffer(buf);
                }
                else{
                    RawDetection *old_det = it_obs->second; 
                    double delta1 = fabs(old_det->observation->utime_last_seen - matching_node->utime)/1.0e6;
                    double delta2 = fabs(outstanding_object_detections[j]->observation->utime_last_seen - matching_node->utime)/1.0e6;
                    if(delta2 < delta1){
                        //replace 
                        it_obs->second = outstanding_object_detections[j];
                        sprintf(buf, "[Objects] Found closer observation (%f) for %d node for Object : %d - Adding", delta2, matching_node->node_id, outstanding_object_detections[j]->object->id);
                        output_writer->write_to_buffer(buf);
                        delete old_det;
                    }
                    else{
                        delete outstanding_object_detections[j];
                    }                    
                    //found an old match - check if the new one is closer - replace if it is 
                }
            }               

            //outstanding_object_detections[j]->object->updateDetection(outstanding_object_detections[j]->observation, matching_node);
            //maybe we should wait till everything is associated with a node - then we can just put the closest node-object pair 
            //based on time 
            //delete outstanding_object_detections[j];
            //we should add this to the node scan - its up to the node scan to update its current list of objects 
            //if it needs to 
            //if there is an observation from this node already - check which one is closest in time 
            //add if this is the closest 
            
        }
        else{
            remaining_outstanding.push_back(outstanding_object_detections[j]);
        }
    }

    //update the nodes 
    for(it_object = node_object_observations.begin(); it_object != node_object_observations.end(); it_object++){
        ObjectDetection *obj = it_object->first;
        for(it_obs = it_object->second.begin(); it_obs != it_object->second.end(); it_obs++){
            obj->updateDetection(it_obs->second->observation, it_obs->first);
            delete it_obs->second->observation;
        }
    }
        
    if(break_ind >=0){
        for(int i=break_ind; i < outstanding_object_detections.size(); i++){
            remaining_outstanding.push_back(outstanding_object_detections[i]);
        }        
    }

    outstanding_object_detections = remaining_outstanding; 
}

//USED 
//publishes the resulting particles - used by the renderer to render the latest results 
//need to add the set of actions taken during this cycle - to better diagnose 
void SlamParticleFilter::publish_slam_region_particles(){
    slam_graph_region_particle_list_t *msg = get_slam_region_particle_msg();

    slam_graph_region_particle_list_t_publish(lcm, "REGION_PARTICLE_ISAM_RESULT", msg);
    slam_graph_region_particle_list_t_destroy(msg);

    if(weights != NULL){
        slam_graph_weight_list_t_publish(lcm, "PARTICLE_ISAM_WEIGHTS", weights);
    }
}

//USED 
//publish the local to global transforms for each particle 
//the renderer will pick the valid one and publish the transform 
void SlamParticleFilter::publish_slam_transforms(){
    if(added_slam_nodes.size()==0)
        return;    erlcm_rigid_transform_list_t msg;
    msg.utime = bot_timestamp_now();
    msg.num = slam_particles.size();
    msg.list = (erlcm_rigid_transform_t *) calloc(msg.num, sizeof(erlcm_rigid_transform_t));
    bot_lcmgl_t *lcmgl = lcmgl_frame_test;
    bot_lcmgl_point_size(lcmgl, 10);
    bot_lcmgl_color3f(lcmgl, 1.0, 0, 0);
    bot_lcmgl_begin(lcmgl, GL_POINTS);

    for(int i=0; i < slam_particles.size(); i++){
        SlamParticle *part = slam_particles.at(i);

        BotTrans global_to_local = part->getBotTransForCurrentNode();
        msg.list[i].id = part->graph_id;
        bot_core_rigid_transform_t *bt_transform = &msg.list[i].transform;
        memcpy (bt_transform->trans, global_to_local.trans_vec, 3*sizeof(double));
        memcpy (bt_transform->quat, global_to_local.rot_quat, 4*sizeof(double));
        //lets lcmgl it??
        Pose2d last_pose = part->getLastPose();
        double pos[3] = {last_pose.x(),last_pose.y(),0};
        double l_pos[3];
        bot_trans_apply_vec(&global_to_local, pos, l_pos);
        bot_lcmgl_vertex3f(lcmgl, pos[0], pos[1], pos[2]);          
        //bot_lcmgl_vertex3f(lcmgl, l_pos[0], l_pos[1], l_pos[2]);          
    }

    bot_lcmgl_end(lcmgl);
    bot_lcmgl_switch_buffer(lcmgl);
     
    //erlcm_rigid_transform_t_publish(lcm, "SLAM_TRANSFORMS", NULL);
    erlcm_rigid_transform_list_t_publish(lcm, "SLAM_TRANSFORMS", &msg);
    //erlcm_position_t_publish(lcm, "TEST", NULL);
    free(msg.list);
}

//USED
//construct a particle message from the individual particles
slam_graph_region_particle_list_t *SlamParticleFilter::get_slam_region_particle_msg(){
    slam_graph_region_particle_list_t *msg = (slam_graph_region_particle_list_t *) calloc(1, sizeof(slam_graph_region_particle_list_t));
    msg->utime = bot_timestamp_now();
    msg->no_particles = slam_particles.size();
    
    msg->particle_list = (slam_graph_region_particle_t *) calloc(msg->no_particles, sizeof(slam_graph_region_particle_t));
    msg->status = SLAM_GRAPH_REGION_PARTICLE_LIST_T_STATUS_PROCESSED;

    msg->no_failed = (int) failed_complex_language.size();
    msg->annotations_failed = (slam_graph_language_annotation_t *) calloc(msg->no_failed, sizeof(slam_graph_language_annotation_t)); 
    for(int  i=0; i < failed_complex_language.size(); i++){
        msg->annotations_failed[i].node_id = failed_complex_language[i]->node_id; 
        msg->annotations_failed[i].label = strdup(failed_complex_language[i]->parse_result->utterance);
    }

    msg->no_outstanding = (int) outstanding_complex_language.size();
    msg->annotations_outstanding = (slam_graph_language_annotation_t *) calloc(msg->no_outstanding, sizeof(slam_graph_language_annotation_t)); 

    for(int  i=0; i < outstanding_complex_language.size(); i++){
        msg->annotations_outstanding[i].node_id = outstanding_complex_language[i]->node_id; 
        msg->annotations_outstanding[i].label = strdup(outstanding_complex_language[i]->parse_result->utterance);
    }

    msg->no_grounded = (int) grounded_language.size() + (int) grounded_simple_language.size();
    msg->annotations_grounded = (slam_graph_language_annotation_t *) calloc(msg->no_grounded, sizeof(slam_graph_language_annotation_t)); 

    for(int  i=0; i < grounded_language.size(); i++){
        msg->annotations_grounded[i].node_id = grounded_language[i]->node_id; 
        msg->annotations_grounded[i].label = strdup(grounded_language[i]->parse_result->utterance);
    }

    for(int  i=0; i < grounded_simple_language.size(); i++){
        int j = grounded_language.size() + i;
        msg->annotations_grounded[j].node_id = grounded_simple_language[i]->node_id; 
        msg->annotations_grounded[j].label = strdup(grounded_simple_language[i]->label->update);
    }
    
    if(doSemanticClassification){
        label_info->getTypeLcmMessage(&msg->type_info);
        label_info->getApperenceLcmMessage(&msg->appearance_info);
        label_info->getLabelLcmMessage(&msg->label_info);
    }
    else{
        label_info->getTypeLcmMessage(&msg->type_info);
        label_info->getApperenceLcmMessage(&msg->appearance_info);
        LabelDistribution ld;
        ld.getLabelLcmMessage(&msg->label_info);
    }

    for(int i=0; i < msg->no_particles; i++){
        SlamParticle *part = slam_particles.at(i);
        part->getLCMRegionMessageFromGraph(&msg->particle_list[i]);
    }

    //add the node classification stuff to the particle - since its the same for all the particles 
    msg->no_nodes  = (int) added_slam_nodes.size();
    msg->node_classifications = (slam_graph_node_classification_t *) calloc(msg->no_nodes, sizeof(slam_graph_node_classification_t));

    map<int, double>::iterator it;
    for(int i=0; i < added_slam_nodes.size(); i++){
        msg->node_classifications[i].no_laser_classes = 0;
        msg->node_classifications[i].no_image_classes = 0;
        msg->node_classifications[i].laser_classification = NULL;
        msg->node_classifications[i].image_classification = NULL;

        NodeScan *node = added_slam_nodes[i];
        msg->node_classifications[i].node_id = node->node_id;

        if(node->laser_classification){
            msg->node_classifications[i].no_laser_classes = node->laser_classification->count;
            msg->node_classifications[i].laser_classification = (slam_probability_element_t *) 
                calloc(msg->node_classifications[i].no_laser_classes, sizeof(slam_probability_element_t));
            int j=0;
            for(it = node->laser_classification_obs.begin(); it != node->laser_classification_obs.end(); it++, j++){
                msg->node_classifications[i].laser_classification[j].type = it->first;
                msg->node_classifications[i].laser_classification[j].probability = it->second;
            }
        }
                
        if(node->image_classification){
            msg->node_classifications[i].no_image_classes = node->image_classification->count;
            msg->node_classifications[i].image_classification = (slam_probability_element_t *) 
                calloc(msg->node_classifications[i].no_image_classes, sizeof(slam_probability_element_t));
            int j=0;
            for(it = node->image_classification_obs.begin(); it != node->image_classification_obs.end(); it++, j++){
                msg->node_classifications[i].image_classification[j].type = it->first;
                msg->node_classifications[i].image_classification[j].probability = it->second;
            }
        }
    }

    return msg;
}

