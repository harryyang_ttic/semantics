/**
 * @file main.cpp
 * @brief Multiple relative pose graphs with relative constraints.
 * @author Sachithra Hemachandra, Mathew Walter, Bianca Homeburg, Stefanie Tellex
 * @version 
 *
 * Copyright (C) 2009-2012 Massachusetts Institute of Technology.
 *
 */

#include  "SlamParticleFilter.hpp"

using namespace scanmatch;
using namespace std;
using namespace isam;
using namespace Eigen;

//USED
// Publishes occupancy grid maps for the most likely particle
//Done based on interaction from the viewer (so when we get a request for a particular map 
//publish that out

sig_atomic_t still_groovy = 1;

void print_region_to_region_mapping(region_mapping_t &mapping){
    map<int, int>::iterator it; 
    
    for(it = mapping.region_map.begin(); it != mapping.region_map.end(); it++){
        fprintf(stderr, "Region %d => %d\n", it->first, it->second);
    }    
}

void adjust_language_result_from_unique(slam_complex_language_result_t *result, region_mapping_t &mapping){
    //search thrugh the result and make the changes to region ids
    map<int, int>::iterator it;
    for(int i=0; i < result->count; i++){
        slam_complex_language_path_result_list_t *result_list = &result->language[i];
        for(int k=0; k < result_list->no_paths; k++){
            slam_complex_language_path_result_t *path = &result_list->results[k];
            it = mapping.region_map.find(path->region_id);
            if(it != mapping.region_map.end()){
                path->region_id = it->second;
            }
            else{
                fprintf(stderr, "Region mapping not found for region : %d\n", (int) path->region_id);

                //maybe print the found mapping?? 
                exit(-1);
            }
            for(int j=0; j < path->no_landmarks; j++){
                slam_slu_landmark_result_t *lm_result = &path->landmark_results[j];
            
                it = mapping.region_map.find(lm_result->landmark_id);
                if(it != mapping.region_map.end()){
                    fprintf(stderr, "Remapping LM : %d - %d\n", (int) lm_result->landmark_id, (int) it->second); 
                    lm_result->landmark_id = it->second;
                    
                }
                else{
                    fprintf(stderr, "Trying to remap particle : %d => Particle : %d\n", (int) result->particle_id, mapping.particle_id);
                    fprintf(stderr, "Region mapping not found : %d\n", (int) lm_result->landmark_id);
                    print_region_to_region_mapping(mapping);
                    exit(-1);
                }
            }
        }
    }
}

void count_different_particles(SlamParticleFilter *app){
    set<nodePairKey> different_particles;
    set<nodePairKey>::iterator it;
    int count = 0;
    for(int i=0; i < app->slam_particles.size(); i++){
        SlamParticle *part1 = app->slam_particles.at(i);
        for(int j=0; j < app->slam_particles.size(); j++){
            if(i == j)
                continue;
            SlamParticle *part2 = app->slam_particles.at(i);

            if( (different_particles.find(make_pair(part1->graph_id, part2->graph_id))== different_particles.end())
                && (different_particles.find(make_pair(part2->graph_id, part1->graph_id))== different_particles.end())){
                if(!part1->compareToParticle(part2)){
                    different_particles.insert(make_pair(part1->graph_id, part2->graph_id));
                }
            }
        }
    }

    fprintf(stderr, "\n\n============ Number of different particles : %d\n",(int) different_particles.size()); 
}




//USED - Partial functionality
//Helper function to create new NodeScans 
//Currently the NodeScan copy doesn't fully copy its information
//Right now this is used with copyNodeScans function - which is not supported 
void copy_slam_nodes(vector<NodeScan *>nodes, vector<NodeScan *> &node_list){
    for(int i=0; i < nodes.size(); i++){
        NodeScan *nd = nodes[i];
        node_list.push_back(new NodeScan(nd));
    }
}

static void sig_action(int signal, siginfo_t *s, void *user)
{
    //SlamParticleFilter *app = (SlamParticleFilter *) user;
    //app->
    still_groovy = 0;
}

//Used
static void slam_command_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                                 const char * channel __attribute__((unused)), const slam_command_t * msg,
                                 void * user  __attribute__((unused)))
{
    SlamParticleFilter * app = (SlamParticleFilter *) user;
    if(msg->command == SLAM_COMMAND_T_FINISH_SLAM){
        g_mutex_lock(app->mutex_finish_slam);
        app->finishSlamHeard = true;        
        fprintf(stdout, "Finish SLAM Command heard\n");
        g_mutex_unlock(app->mutex_finish_slam);
    }
}

//USED
//helper function
// Compare language_label_t by utime
bool compare_by_utime (slam_language_label_t *first, slam_language_label_t *second)
{
    if (first->utime <= second->utime)
        return true;
    else
        return false;
}

//------------------ LCM Handlers ---------------------------//
static void pixel_map_request_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                                      const char * channel __attribute__((unused)), 
                                      const slam_pixel_map_request_t * msg, void * user  __attribute__((unused))){
    // Currently only handle simple language
    SlamParticleFilter *app = (SlamParticleFilter *) user;
    
    //add this to the state
    g_mutex_lock(app->mutex);
    if(app->map_request != NULL){
        slam_pixel_map_request_t_destroy(app->map_request);
    }
    app->map_request = slam_pixel_map_request_t_copy(msg);
    g_mutex_unlock(app->mutex);
}

static void slam_scan_request_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                                      const char * channel __attribute__((unused)), 
                                      const slam_pixel_map_request_t * msg, void * user  __attribute__((unused))){
    // Currently only handle simple language
    SlamParticleFilter *app = (SlamParticleFilter *) user;
    
    //add this to the state
    g_mutex_lock(app->mutex);
    if(app->scan_request != NULL){
        slam_pixel_map_request_t_destroy(app->scan_request);
    }
    app->scan_request = slam_pixel_map_request_t_copy(msg);
    g_mutex_unlock(app->mutex);
}

static void language_label_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                                   const char * channel __attribute__((unused)), 
                                   const slam_language_label_t * msg, void * user  __attribute__((unused)))
{
    SlamParticleFilter *app = (SlamParticleFilter *) user;
    char buf[1024];
    sprintf(buf, "[LCM] [Language Update] Language Message received (%s)", msg->update);
    app->output_writer->write_to_buffer(buf);

    if(msg->constraint_type == SLAM_LANGUAGE_LABEL_T_TYPE_SIMPLE && msg->label == -1){
        sprintf(buf, "[LCM] [Language Update] [Error] Invalid Label");
        app->output_writer->write_to_buffer(buf);
        return;
    }

    if(msg->constraint_type == SLAM_LANGUAGE_LABEL_T_TYPE_COMPLEX){
        //send this querry over to SLU - to get the parsed output 
        if(!app->config_params.useSLULibrary && app->slu_classifier){
            slam_language_label_t *comp_lang = slam_language_label_t_copy(msg);
            slam_language_label_t_publish(app->lcm, "ISAM_REGION_SLU_LANGUAGE_PARSE", comp_lang);
            slam_language_label_t_destroy(comp_lang);
        }
        else{
            //parse this using the SLUClassifier parser (the same as the hack in SLU) 
            //need to implement a better parser (maybe from NLU) 
            Command cmd = app->slu_classifier->parse_language(string(msg->update));   
            slam_slu_parse_language_querry_t *p_msg = (slam_slu_parse_language_querry_t *) calloc(1, sizeof(slam_slu_parse_language_querry_t));
            p_msg->utime = msg->utime;
            p_msg->utterance = strdup(msg->update);
            p_msg->landmark = strdup(cmd.landmark.c_str());
            p_msg->sr = strdup(cmd.relation.c_str());
            p_msg->figure = strdup(cmd.figure.c_str());
            cout << cmd << endl; 
            g_mutex_lock (app->mutex);
            app->complex_language_labels_to_add.push_back(p_msg);
            g_mutex_unlock (app->mutex);               
        }
    }
    else{
        app->got_label = 1;
        // Add the label to the list and sort by utime
        g_mutex_lock (app->mutex);
        //we only add simple language here 
        app->language_labels_to_add.push_back (slam_language_label_t_copy (msg));
        g_mutex_unlock (app->mutex);   
    }

    return;
}

static void slam_sr_answer_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                         const char * channel __attribute__((unused)), 
                         const slam_language_answer_t * msg,
                         void * user  __attribute__((unused)))
{
    SlamParticleFilter * app = (SlamParticleFilter *) user;

    char buf[1024];
    
    app->q_info.updateAnswer(msg);
    sprintf(buf, "[LCM] [Answer] Valid answer received : %d", (int) msg->id);
    app->output_writer->write_to_buffer(buf);
}

//USED - Deprecated - not sure if we plan to use the door detector 
static void door_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                         const char * channel __attribute__((unused)), 
                         const object_door_list_t * msg,
                         void * user  __attribute__((unused)))
{
    SlamParticleFilter * app = (SlamParticleFilter *) user;
    
    //if we get this - set flag to stop the slam and then do a proper scanmatch (also use its own local region)
    //g_mutex_lock(app->mutex);
    if(app->door_list != NULL)
        object_door_list_t_destroy(app->door_list);
    
    app->door_list = object_door_list_t_copy(msg);
}



//we don't want to collect this list - either match or update 
static void object_list_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                         const char * channel __attribute__((unused)), 
                         const perception_object_detection_list_t * msg,
                         void * user  __attribute__((unused)))
{
    SlamParticleFilter * app = (SlamParticleFilter *) user;
    //we should match to any object we have in the representation already??
    g_mutex_lock(app->mutex_objects);

    char buf[1024];
    sprintf(buf, "[Objects] No of Objects received : %d" RESET_COLOR, msg->count);
    app->output_writer->write_to_buffer(buf);
    app->output_writer->write_buffer();

    for(int i=0; i < msg->count; i++){
        map<int, ObjectDetection *>::iterator it= app->object_map.find(msg->objects[i].id);
        
        ObjectDetection *object = NULL;
        if(it != app->object_map.end()){
            object = it->second;
        }
        else{
            object = new ObjectDetection(msg->objects[i].id, msg->objects[i].type);
            app->object_map.insert(make_pair(msg->objects[i].id, object));
        }
        
        app->outstanding_object_detections.push_back(new RawDetection(object, perception_object_detection_t_copy(&msg->objects[i])));
    }
    //app->new_object_detections.push_back(perception_object_detection_list_t_copy(msg));

    //maybe we should be publishing observations - and they will have the unique id 
    //then it can be matched to the closest 
    //for each object we observe - we should match it to the closest node at which it was observed from 
    
    g_mutex_unlock(app->mutex_objects);
}

static void full_laser_handler(int64_t utime, bot_core_planar_lidar_t * msg,
                            void * user)
{
    SlamParticleFilter * app = (SlamParticleFilter *) user;
    bot_ptr_circular_add (app->full_laser_circ, bot_core_planar_lidar_t_copy(msg));
}

static void image_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), const bot_core_image_t * msg,
                            void * user  __attribute__((unused)))
{
    SlamParticleFilter * app = (SlamParticleFilter *) user;
    app->add_image(msg);
    //bot_ptr_circular_add (app->image_circ, bot_core_image_t_copy(msg));
}

static void r_laser_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), const bot_core_planar_lidar_t * msg,
                            void * user  __attribute__((unused)))
{
    SlamParticleFilter * app = (SlamParticleFilter *) user;
    app->add_rear_laser(msg);
    //bot_ptr_circular_add (app->rear_laser_circ, bot_core_planar_lidar_t_copy(msg));

}

static void laser_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)), const bot_core_planar_lidar_t * msg,
                          void * user  __attribute__((unused)))
{
    SlamParticleFilter * app = (SlamParticleFilter *) user;
    app->add_front_laser(msg);
    // app->last_laser_reading_utime = msg->utime;
    // if(app->verbose_laser_level >=2){
    //     app->output_writer->write_to_buffer("[LCM] [Laser] Laser handler called", false, true, false);        
    // }
    // if(app->slave_mode){
    //     bot_ptr_circular_add (app->front_laser_circ,  bot_core_planar_lidar_t_copy(msg));
    //     //if there is an unhandled node init msg - check if this is it 

    //     if(app->node_init != NULL){
    //         double min_utime = 0.5;//we dont consider if the closest laser is 0.5 s apart
    //         int close_ind = -1;
    //         bot_core_planar_lidar_t *flaser = NULL;

    //         for (int i=0; i<(bot_ptr_circular_size(app->front_laser_circ)); i++) {
    //             bot_core_planar_lidar_t *laser = (bot_core_planar_lidar_t *) bot_ptr_circular_index(app->front_laser_circ, i);
    //             double time_diff = fabs(laser->utime - msg->utime)  / 1.0e6;

    //             if(time_diff< min_utime){
    //                 min_utime = time_diff;
    //                 close_ind = i;
    //             }
    //         }
            
    //         if(close_ind == -1){
    //             fprintf(stderr, "No matching laser scan found\n");
    //             return;
    //         }
    //         flaser = (bot_core_planar_lidar_t *) bot_ptr_circular_index(app->front_laser_circ, close_ind);
            
    //         process_laser(flaser,app, msg->utime);

    //         if(app->node_init != NULL){
    //             slam_init_node_t_destroy(app->node_init);
    //             app->node_init = NULL;
    //         }
    //     }
    // }
    // else{
    //     process_laser( (bot_core_planar_lidar_t *) msg,app, msg->utime);
    // }
    // if(app->verbose_laser_level >=2){
    //     app->output_writer->write_to_buffer("[LCM] [Laser] Laser handler finished", false, true, true);        
    // }
    // app->output_writer->write_buffer();
    // static int64_t utime_prev = msg->utime;
}


gboolean check_updated  (gpointer key, gpointer value, gpointer user_data){
    SlamGraph *graph = (SlamGraph *) value;

    if(graph->updated == 0){
        return TRUE;
    }
    return FALSE;
}


void destroy_slam_graph(gpointer data){
    SlamGraph *graph = (SlamGraph *) data;

    delete graph;
}//addSlamNode



//USED 
//Laser processing thread 
static void *laser_process_thread(void *user)
{
    SlamParticleFilter * app = (SlamParticleFilter *) user;

    printf("isam-particles: laser_process_thread()\n");
    app->output_writer->write_to_buffer("[Init] Laser Thread created", false, true, true);            
    int status = 0;
    while(app->still_groovy && still_groovy){
        //sleep a bit if we have no new graph messages
        status = app->process_laser_queue();
        if(status==0)
            usleep(5000);
        else if(status == 1)
            usleep(500);
    }
}

//particle processing thread
static void *particles_thread(void *user)
{
    SlamParticleFilter * app = (SlamParticleFilter *) user;

    printf("isam-particles: loop_closure_thread()\n");
    app->output_writer->write_to_buffer("[Init] Particles Thread created", false, true, true);            
    int status = 0;
    while(app->still_groovy && still_groovy){
        status = app->process_graph_particles();
        //sleep a bit if we have no new graph messages
        if(status==0)
            usleep(5000);

        if(status==2){
            usleep(50000);
        }
    }
}

int main(int argc, char *argv[])
{
    setlinebuf(stdout);

    lcm_t *lcm = lcm_create(NULL);

    if (!lcm) {
        fprintf(stderr, "ERROR: lcm_create() failed\n");
        return -1;
    }

    SlamParticleFilter *app = new SlamParticleFilter(lcm, argc, argv);
    
    bot_core_planar_lidar_t_subscribe(app->lcm, app->chan, laser_handler, app);

    bot_core_image_t_subscribe(app->lcm, "DRAGONFLY_IMAGE", image_handler, app);

    app->full_laser = full_laser_init(app->lcm, 360, &full_laser_handler, app);

    if(app->rchan != NULL){
        bot_core_planar_lidar_t_subscribe(app->lcm, app->rchan, r_laser_handler, app);
    }

    slam_command_t_subscribe(app->lcm, "SLAM_COMMAND", slam_command_handler, app);
    
    if(app->slave_mode){
        fprintf(stderr,"Slave mode : Subscribing to external trigger\n");
    }
    
    slam_language_label_t_subscribe (app->lcm, "LANGUAGE_LABEL", language_label_handler, app);

    if(app->ignoreLanguage == 1)
        fprintf(stderr, "Ignoring language\n");

    //we will be using a secondary process to publish the slam maps 
    slam_pixel_map_request_t_subscribe (app->lcm, "SLAM_SCAN_REQUEST", slam_scan_request_handler, app);
    
    if(app->use_doorways)
        object_door_list_t_subscribe(app->lcm, "DOOR_LIST", door_handler, app);

    slam_language_answer_t_subscribe(app->lcm, "SLAM_SR_ANSWER", slam_sr_answer_handler, app);
    perception_object_detection_list_t_subscribe(app->lcm, "OBJECT_DETECTION_LIST", object_list_handler, app);

    app->output_writer->write_to_buffer("[Init] Subscribed to LCM messages", false, true, true);               
    
    //reset the viewer 
    slam_status_t r_msg;
    r_msg.utime = bot_timestamp_now();
    r_msg.status = SLAM_STATUS_T_RESET;
    slam_status_t_publish(app->lcm, "SLAM_STATUS", &r_msg);

    //process the particles 
    pthread_create(&app->scanmatch_thread , NULL, particles_thread, app);

    //process the laser queue
    pthread_create(&app->laser_process_thread , NULL, laser_process_thread, app);

    struct sigaction new_action;
    new_action.sa_sigaction = sig_action;
    sigemptyset(&new_action.sa_mask);
    new_action.sa_flags = 0;

    sigaction(SIGINT,  &new_action, NULL);
    sigaction(SIGTERM, &new_action, NULL);
    sigaction(SIGKILL, &new_action, NULL);
    sigaction(SIGHUP,  &new_action, NULL);

    
    /* sit and wait for messages */
    while (app->still_groovy && still_groovy)
        lcm_handle(app->lcm);

    delete app;
    sm_tictoc(NULL);
    return 0;
}
