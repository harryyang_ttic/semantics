#ifndef OUTPUT_WRITER_H
#define OUTPUT_WRITER_H

#include <glib.h>
#include <fstream>
#include <string>
#include <iostream>

using namespace std;

class OutputWriter{
public:
    OutputWriter();
    OutputWriter(string _prefix, string file_name);
    void write_to_output(string &output, bool reset=false);
    void write_to_buffer(string output, bool new_line_begin=false, bool new_line_end=true, bool write=false);
    void write_to_buffer(const char *_output, bool new_line_begin=false, bool new_line_end=true, bool write=false);
    void write_buffer(bool reset=false);
    static GMutex *mutex;
private:
    //std::ofstream out("output.txt");
    string name; 
    string prefix; 
    string output_buffer;
    bool new_line;
    GMutex *o_mutex;
};

#endif
