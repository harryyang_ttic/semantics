#include "SlamGraph.hpp"

SRQuestion::SRQuestion(ComplexLanguageEvent *_event, int _landmark, spatialResult _sr_results, SlamNode *_node, RegionNode *_landmark_region, int _id):
    node(_node), landmark(_landmark), id(_id), landmark_region(_landmark_region){
    event = _event;
    sr_result = _sr_results;//make_pair(INVALID, 0);
    answer = ANSWER_INIT;
    if(landmark_region){
        evaluated_pose = landmark_region->mean_node->getPose();
    }
    else{
        evaluated_pose = node->getPose();
    }
}
    
//guessing this will get called alot - which will make the id's go up - might want a better solution?? 
SRQuestion::SRQuestion(const SRQuestion &question){
    id = question.id;
    node = question.node;
    //id = getNextID();
    event = question.event;
    landmark_region = question.landmark_region;
    landmark = question.landmark;
    sr_result = question.sr_result;
    answer = question.answer; 
}

SRQuestion::~SRQuestion(){
    
}

int SRQuestion::getLandmarkRegionID(){
    if(landmark_region){
        return landmark_region->region_id;
    }
    return -1;
}

void SRQuestion::updateQuestionPose(){
    if(landmark_region){
        evaluated_pose = landmark_region->mean_node->getPose();
    }
    else{
        evaluated_pose = node->getPose();
    }
}

bool SRQuestion::hasQuestionPoseChanged(){
    double dx = 0.1; 
    double dy = 0.1;
    double dt = bot_to_radians(5.0);
    Pose2d new_pose = node->getPose();
    if(landmark_region){
        new_pose = landmark_region->mean_node->getPose();
    }

    if(fabs(new_pose.x() - evaluated_pose.x()) > dx || 
       fabs(new_pose.y() - evaluated_pose.y()) > dy || 
       fabs(new_pose.t() - evaluated_pose.t()) > dt){
        return true;
    }
    return false; 
}

string SRQuestion::fixLabelString(string str){
    if(!str.compare("elevatorlobby")){
        return string("elevator lobby");
    }
    if(!str.compare("conferenceroom")){
        return string("conference room");
    }
    return str;
}

bool SRQuestion::getLandmarkString(string &label_name){
    if(landmark_region){
        double prob = 0; 
        bool found_max_label = landmark_region->getMaxLabelAndLikelihood(label_name, prob);
        return found_max_label;
    }
        
    label_name = "none";
    return false;
}

string SRQuestion::getAnswerString(){
    switch(answer){
    case ANSWER_NO:
        return string("No");
        break;
    case ANSWER_YES:
        return string("Yes");
        break;
    case ANSWER_INVALID:
        return string("Invalid");
        break;
    case ANSWER_INIT:
        return string("Init");
        break;
    case ANSWER_LEFT_OF:
        return string("Left of");
        break;
    case ANSWER_RIGHT_OF:
        return string("Right of");
        break;
    case ANSWER_IN_FRONT_OF:
        return string("In front of");
        break;
    case ANSWER_BEHIND:
        return string("Behind");
        break;
    case ANSWER_NEAR:
        return string("Near");
        break;
    case ANSWER_AT:
        return string("At");
        break;
     case ANSWER_AWAY:
        return string("Away");
        break;
    default:
        return string("Unknown");
        break;
    }
}

string SRQuestion::getQuestionString(){
    string question_label;
    if(landmark){
        question_label = event->landmark;
    }
    else{
        question_label = event->figure;
    }
    //string question_str = createQuestion(question_label, question.sr_result.sr);
               
    if(!landmark_region){            
        string question("Is the ");
        question += fixLabelString(question_label); 
        question += string(" ");
        switch(sr_result.sr){
        case AT:
            return string("Am I at the ") + question_label;
            break;
        case LEFT_OF:
            return question + string("on my left?");
            break;
        case RIGHT_OF:
            return question + string("on my right?");//string("right of");
            break;
        case IN_FRONT_OF:
            return question + string("in front of me?");
            break;
        case BEHIND:
            return question + string("behind me?");
            break;
        case NEAR:
            return question + string("near me?");
            break;
        default:
            return string("unknown");
            break;
        }
    }
    else{
        string lm_string;
        getLandmarkString(lm_string);
            
        string question("Is the ");
        question += fixLabelString(question_label); 
        question += string(" ");

        string sr_string;
            
        switch(sr_result.sr){
        case AT:
            sr_string = string("at the "); 
            break;
        case LEFT_OF:
            sr_string = string("to the left of "); 
            break;
        case RIGHT_OF:
            sr_string = string("to the right of "); 
            break;
        case IN_FRONT_OF:
            sr_string = string("in front of "); 
            break;
        case BEHIND:
            sr_string = string("behind "); 
            break;
        case NEAR:
            sr_string = string("near "); 
            break;
        default:
            sr_string = string("near "); 
            break;
        }
            
        return question + sr_string + string("the ") + fixLabelString(lm_string) + string(" ?");
    }
}

int SRQuestion::getNodeID(){
    if(node){
        return node->id;
    }
    return -1;
}

void SRQuestion::updateAnswer(answer_t _answer){
    answer = _answer;
}

void SRQuestion::setID(int _id){
    id = _id;
}

void SRQuestion::updateEventCount(){
    event->updateAskedQuestions(sr_result.sr);
}

SRAbstractQuestion::SRAbstractQuestion(int event_id_, spatialRelation sr_, int node_id_, string question_string_, int landmark_id_):
    event_id(event_id_), node_id(node_id_), landmark_id(landmark_id_), sr(sr_), question_string(question_string_), 
    weighted_info_gain(0), weighted_cost(0), sum_weights(0), question_id(-1), answer_string("invalid")
{
}

int SRAbstractQuestion::getNodeID(){
    return node_id;
}

void SRAbstractQuestion::setQuestionID(int q_id){
    question_id = q_id;
}

bool SRAbstractQuestion::updateAnswer(const slam_language_answer_t *answer){
    if(question_id == answer->id){
        answer_string = string(answer->answer); 
        fprintf(stdout, "Updating the answer\n");
        return true;
    }
    else{
        fprintf(stdout, "Error - Question ID : %d - Answer ID : %d\n", question_id, (int) answer->id);
        return false;
    }
}

double SRAbstractQuestion::getAverageCost(){
    return weighted_cost / sum_weights; 
}

string SRAbstractQuestion::getQuestionString(){
    return question_string; 
}

slam_language_question_t *SRAbstractQuestion::get_question(){
    slam_language_question_t *msg = (slam_language_question_t *)calloc(1, sizeof(slam_language_question_t));
    msg->id = question_id;//outstanding_question->id;
    msg->particle_id = -1;//p_id;
    msg->utime = bot_timestamp_now();
    string q_string = getQuestionString(); //string("this is a test");//outstanding_question->getQuestionString();
    cout << "Question String : " << q_string << endl;
    msg->question = strdup(q_string.c_str());
    //asked_question = outstanding_question;
    //outstanding_question = NULL;
    return msg; 
}

// void SRAbstractQuestion::addParticleQuestion(int particle_id, double info_gain, double cost, double weight){
//     sum_weights += weight; 
//     weighted_info_gain += info_gain * weight; 
//     weighted_cost += cost * weight;
//     info_result.insert(make_pair(particle_id, InfoResult(info_gain, cost)));
// }


void SRAbstractQuestion::addParticleQuestion(int particle_id, SRQuestion p_question, double weight){
    sum_weights += weight;
    weighted_info_gain +=  p_question.sr_result.info_gain * weight; 
    weighted_cost += p_question.sr_result.cost * weight;
    particle_questions.insert(make_pair(particle_id, p_question));
}

map<int, SRQuestion> SRAbstractQuestion::getParticleQuestions(){
    map<int, SRQuestion>::iterator it;
    for(it = particle_questions.begin(); it != particle_questions.end(); it++){
        it->second.setID(question_id);
    }
    return particle_questions; 
}

//can we just add these questions?? 
