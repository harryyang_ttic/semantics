#ifndef _SLAM_PARTICLE_FILTER_H_
#define _SLAM_PARTICLE_FILTER_H_

#include "SlamParticle.hpp"
#include <iostream>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

#include <opencv/highgui.h>
#include <getopt.h>

#include <lcm/lcm.h>
#include <bot_core/bot_core.h>
//bot param stuff
#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_lcmgl_client/lcmgl.h>

#include <gsl/gsl_blas.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_sf_bessel.h>
#include <gsl/gsl_integration.h>
#include <math.h>

#include  <classify-semantic/classify_semantic.hpp>
#include <scanmatch/ScanMatcher.hpp>
#include <lcmtypes/sm_rigid_transform_2d_t.h>
#include <bot_frames/bot_frames.h>
#include <lcmtypes/er_lcmtypes.h>

#include <isam/isam.h>
#include <isam/Anchor.h>

#include <occ_map/PixelMap.hpp>
#include <lcmtypes/slam_lcmtypes.h>
#include <lcmtypes/perception_object_detection_list_t.h>
//#include "RegionSlam.h"
#include "NodeScan.h"
#include <lcmtypes/slam_lcmtypes.h>
#include "SlamGraph.hpp"
#include <er_common/path_util.h>

#include <door_detector_laser/door_detect_laser.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif 

//add a new node every this many meters
#define LINEAR_DIST_TO_ADD 1.0//1.0//1.0//0.5//1.0
#define ANGULAR_DIST_TO_ADD 1.0//M_PI / 180 * 60
#define MAP_PUBLISH_PERIOD 5.0 //s
#define LASER_DATA_CIRC_SIZE 100
#define IMAGE_DATA_CIRC_SIZE 80
#define MAX_LASERS_TO_COPY_FROM_QUEUE 3
#define QUESTION_INVALID_AFTER_NODES 5

typedef enum { print_node_info, print_particle_info, print_language_info} verbose_level_t;

typedef enum { NO_QUESTION_SENT, QUESTION_SENT, ANSWER_RECEIVED} question_state_t;

class laser_odom_t{
public:
    sm_rigid_transform_2d_t *odom;
    bot_core_planar_lidar_t *laser;
    bot_core_planar_lidar_t *rlaser;
    bot_core_planar_lidar_t *full_laser;
    bot_core_image_t *image;
    
    classification_result_t *laser_result;
    classification_result_t *image_result;

    int force_supernode;
    
    laser_odom_t(): odom(NULL), laser(NULL), rlaser(NULL), full_laser(NULL), image(NULL), laser_result(NULL), image_result(NULL){
                    
    }

    ~laser_odom_t(){
        sm_rigid_transform_2d_t_destroy(odom);
        bot_core_planar_lidar_t_destroy(laser);
        if(rlaser){
            bot_core_planar_lidar_t_destroy(rlaser);
        }
        if(full_laser){
            bot_core_planar_lidar_t_destroy(full_laser);
        }
        if(image){
            bot_core_image_t_destroy(image);
        }
        if(image_result){
            classification_result_t_destroy(image_result);
        }
        if(laser_result){
            classification_result_t_destroy(laser_result);
        }
    }
}; 

struct region_mapping_t {
    int particle_id; //id of the unique particle 
    map<int, int> region_map;
};

typedef struct _received_language_t{
    int64_t node_id;
    slam_language_label_t *label;
} received_language_t;    

class question_info_t {
public:
    //bool outstanding_question;
    question_state_t state; 
    int64_t question_asked_node_id; //WEIRD - removing this variable causes the code to run very SLOW 
    int64_t question_asked_time; 
    
    int no_questions_asked; 

    SRAbstractQuestion  *asked_question; 

    GMutex *mutex; 

    question_info_t(){
        mutex = g_mutex_new();
        asked_question = NULL;
        state = NO_QUESTION_SENT;
        question_asked_time = 0;
        no_questions_asked = 0;
        question_asked_node_id = -1;
    }

    bool timeoutOutstandingQuestion(int current_node_id, int question_invalid_after_nodes){
        g_mutex_lock(mutex);
        if(state == QUESTION_SENT && (current_node_id - question_asked_node_id) > question_invalid_after_nodes){
            fprintf(stdout, "Question Timeout : Q Node id : %d - Current : %d - Timeout after : %d\n", (int) question_asked_node_id, 
                    current_node_id, question_invalid_after_nodes);
            g_mutex_unlock(mutex);
            clearAskedQuestion();            
            return true;
        }
        g_mutex_unlock(mutex);
        return false;
    }

    void updateAskedQuestion(SRAbstractQuestion *new_question, int64_t question_asked_time_){
        g_mutex_lock(mutex);
        if(new_question && state != NO_QUESTION_SENT){
            fprintf(stderr, "Error - asked to update question while not in no question sent state\n");
        }
        
        delete asked_question;
        asked_question = new_question;
        if(asked_question){
            //update the state
            fprintf(stdout, "Updating the asked question\n");
            state = QUESTION_SENT;
            asked_question->setQuestionID(no_questions_asked++); 
            question_asked_node_id = asked_question->getNodeID();
            question_asked_time = question_asked_time_;
        }
        else{
            fprintf(stdout, "Clearing question\n");
            state = NO_QUESTION_SENT;
        }
        g_mutex_unlock(mutex);
    }

    void clearAskedQuestion(){
        updateAskedQuestion(NULL, -1);        
    }
    
    SRAbstractQuestion *getAnsweredQuestionAndClear(){
        //returns NULL if there isn't an answered question 
        SRAbstractQuestion *current_question = NULL;
        g_mutex_lock(mutex);
        if(state == ANSWER_RECEIVED){
            current_question = asked_question;
            asked_question = NULL;
            state = NO_QUESTION_SENT;
        }
        g_mutex_unlock(mutex);
        
        return current_question;
    }
    
    
    bool updateAnswer(const slam_language_answer_t *answer){
        g_mutex_lock(mutex);
        if(state == QUESTION_SENT){
            if(asked_question){
                state = ANSWER_RECEIVED;
                if(!asked_question->updateAnswer(answer)){
                    fprintf(stderr, "Error - update answer returned false\n");
                }
            }
            else{
                fprintf(stderr, "Received answer but missing question\n");
            }        
        }
        else{
            fprintf(stderr, "State was not question sent\n");
        }
        g_mutex_unlock(mutex);
    }
};

class SlamParticleFilter{
public:
    lcm_t * lcm;
    //RegionSlam * regionslam;
    ScanMatcher *sm;
    ScanMatcher *sm_incremental;
    ScanMatcher *sm_loop;
    ScanMatcher *sm_lc_low; //low resolution scanmatcher to use to get a good startoff point

    string log_path; 
    int mode;
    BotFrames *frames;
    BotParam *param;
    bot_lcmgl_t * lcmgl_basic;
    bot_lcmgl_t * lcmgl_frame_test;
    bot_lcmgl_t * lcmgl_debug;
    bot_lcmgl_t * lcmgl_covariance;
    bot_lcmgl_t * lcmgl_particles;
    bot_lcmgl_t * lcmgl_sm_basic;
    bot_lcmgl_t * lcmgl_sm_graph;
    bot_lcmgl_t * lcmgl_sm_prior;
    bot_lcmgl_t * lcmgl_sm_result;
    bot_lcmgl_t * lcmgl_sm_result_low;
    bot_lcmgl_t * lcmgl_loop_closures;
    slam_graph_weight_list_t *weights;
    semantic_classifier_t *classifier;

    SLUClassifier *slu_classifier; 
    
    int requested_map_particle_id; 
    LabelInfo *label_info; 
    LabelInfo *label_info_particles; 
    
    full_laser_state_t *full_laser;
    //arguments for isam-region
    int ignoreLanguage;
    bool doSemanticClassification;
    bool useLowResPrior;
    bool finishSlamHeard; 
    bool finishSlam; //for now this will only force us to scanmatch ther current region 
    int useSMCov; 
    int scanmatchBeforeAdd;
    int probMode;
    int useCopy;
    bool useOnlyRegion;
    bool addDummyConstraints;
    bool useICP;
    bool copyNodeScans;
    int noScanTransformCheck;
    double odometryConfidence;
    slam_pixel_map_request_t *map_request; 
    slam_pixel_map_request_t *scan_request; 
    //bool useSpectralClustering;
    int segmentationMethod; // 0 - Fixed distance; 1 - RSS segmentation method; 2 - Fixed Spectral clustering 
    int publish_map;
    int useOdom; 
    char * chan;
    char * rchan;
    bool skipOutsideSM;
    int draw_scanmatch; 
    int draw_map;
    int keepDeadEdges;
    int publish_occupancy_maps;
    
    int verbose_laser_level;
    int verbose_particle_level;
    int verbose_node_level;

    question_info_t q_info;

    int complex_language_index; 

    vector<slam_language_answer_t *> slam_sr_answers; //prob shouldn't grow more than one 

    vector<complex_language_event_t *> outstanding_complex_language; 

    vector<complex_language_event_t *> failed_complex_language; 

    vector<complex_language_event_t *> grounded_language; 

    vector<received_language_t *> grounded_simple_language; 

    vector<RawDetection *> outstanding_object_detections; 

    vector<ObjectDetection *> object_detections; 
    map<int, ObjectDetection *> object_map; 

    map<int, region_mapping_t> particle_to_region_map;
    map<int, set<int> > unique_particle_to_duplicates;  //the mapping from unique particle to duplicates  
    //used for grounding language for the duplicate particles 
    
    int64_t last_node_utime; 
    int64_t last_graph_utime; //update from particle last_node_utime
    int64_t last_added_node_id; 
    //parameters for the algorithm
    int64_t publish_occupancy_maps_last_utime;
    int64_t last_laser_reading_utime;
    double sm_acceptance_threshold;
    int max_prob_particle_id;

    object_door_list_t *door_list; 
    slam_init_node_t *node_init;
    int beam_skip; //downsample ranges by only taking 1 out of every beam_skip points
    double spatialDecimationThresh; //don't discard a point if its range is more than this many std devs from the mean range (end of hallway)
    double maxRange; //discard beams with reading further than this value
    double maxUsableRange; //only draw map out to this far for MaxRanges...
    float validBeamAngles[2]; //valid part of the field of view of the laser in radians, 0 is the center beam
    Scan * last_scan;
    set<pair<int, int> > *dist_lc, *lang_lc;
    sm_rigid_transform_2d_t * prev_odom; //odometry computed outside module
    sm_rigid_transform_2d_t * prev_added_odom; //odometry computed outside module
    sm_rigid_transform_2d_t * prev_sm_odom; //internally scan matched odometry
    int resample;
    int verbose;
    verbose_level_t verbose_level;
    int slave_mode;
    int use_doorways;
    vector<slam_region_transition_t *> transitions;

    BotPtrCircular   *front_laser_circ;
    BotPtrCircular   *rear_laser_circ;

    BotPtrCircular   *full_laser_circ;
    BotPtrCircular   *image_circ;
    
    //when a new init message is heard - a scan is added to the hash table - and also 
    //a scan match is done with the previous node 
    //the constraint is added to the relavent nodes - for reuse     
    vector<laser_odom_t *> lasers_to_add;
    vector<NodeScan *> slam_nodes_to_add;

    vector<NodeScan *> all_slam_nodes;

    vector<NodeScan *> added_slam_nodes;

    vector<slam_language_edge_t *> language_edges_to_add;
    vector<slam_language_label_t *> language_labels_to_add;

    vector<slam_slu_parse_language_querry_t *> complex_language_labels_to_add;

    vector<perception_object_detection_list_t *> new_object_detections; 
    int got_label;

    vector<SlamParticle *> slam_particles;
    map<int, SlamParticle *> slam_particles_map;
    NodeScan * last_slampose;
    
    //we should only get one?? 
    //this should be deprecated 
    vector<slam_slu_result_list_t *> slu_result; 
    //this is the new one 
    vector<slam_complex_language_result_list_t *> slu_complex_result; 
    int last_slampose_ind;
    int last_slampose_for_process_ind;
    int node_count;
    int bad_mode;

    int particle_id_to_train;

    SlamGraph *basic_graph; //a slam graph that doesn't have loop closure constraints - used for scan matching 

    bot_core_planar_lidar_t * r_laser_msg;
    bot_core_pose_t *last_deadreakon; 
    int64_t r_utime;

    int64_t last_node_sent_utime;

    pthread_t  scanmatch_thread;
    pthread_t  laser_process_thread;

    int no_particles;

    GMutex *mutex;
    GMutex *mutex_language;
    GMutex *mutex_finish_slam;
    GMutex *mutex_particles;
    GMutex *mutex_laser_queue;
    GMutex *mutex_lasers;
    GMutex *mutex_objects;

    int64_t last_graph_processed_time;

    int use_frames; 
    int region;
    
    ConfigurationParams config_params;

    int next_id;
    int find_ground_truth;
    FloatPixelMap *local_px_map; // Local occupancy map used for measurement likelihood

    OutputWriter *output_writer; 

    //signal handling to exit program 
    sig_atomic_t still_groovy;
    
    SlamParticleFilter(lcm_t *lcm_, int argc, char *argv[]);
    ~SlamParticleFilter();


    void setQuestionSRTypes();
    void setFixedRegionParams();
    void setSpectralRegionParamsICRA();
    void setSpectralRegionParams();
    void setFixedSpectralRegionParams();
    void usage(const char *name);
    
    void print_language_stats();
    
    void add_full_laser(const bot_core_planar_lidar_t *msg);
    void add_image(const bot_core_image_t * msg);
    void add_rear_laser(const bot_core_planar_lidar_t *msg);
    void add_front_laser(const bot_core_planar_lidar_t *msg);
    void process_laser(bot_core_planar_lidar_t *flaser, int64_t utime, int force_supernode = 0);
    void add_to_queue(const bot_core_planar_lidar_t * laser_msg, const sm_rigid_transform_2d_t * odom,
                      int64_t utime, int force_supernode = 0);
    bot_core_planar_lidar_t *get_closest_rear_laser(int64_t utime);
    bot_core_planar_lidar_t *get_closest_full_laser(int64_t utime);
    bot_core_image_t *get_closest_image(int64_t utime);

    void publish_occupancy_map_msg();
    vector<SlamParticle *> get_unique_particles();
    void publish_slam_progress();
    int process_laser_queue();
    int createNodeScan(laser_odom_t *l_odom);
    smPoint *get_scan_points_from_laser(bot_core_planar_lidar_t *laser_msg, char *frame, int *noPoints);
    smPoint *get_scan_points_from_laser_compensated(bot_core_planar_lidar_t *laser_msg, char *frame, int *noPoints, int64_t utime);

    void publish_slampose(Scan *scan, int64_t utime, int64_t id);
    void publish_slampose_list();
    bool is_valid_id(int64_t id);
    void publish_map_id(int64_t id);
    int process_for_nodes_and_language(int64_t utime, vector<NodeScan *> new_nodes, 
                                       map<int, slam_language_label_t *> new_language_labels, 
                                       vector<complex_language_event_t *> complex_language, 
                                       bool finishSlam_);

    slam_language_question_t* get_best_question(SlamParticle *max_particle);
    void update_particles_with_answers();
    int renormalize_particles();
    void update_weights();
    int resample_particles(int64_t utime);
    void clear_dead_edges();
    void send_clear_dialog_msg();
    int process_graph_particles();

    int publish_outstanding_language_querries();
    int evaluate_outstanding_language_querries();
    int publish_outstanding_language_querries_all_particles();
    void publish_language_collection_list();
    
    int get_new_slam_nodes(vector<NodeScan *> &new_node_list);
    void check_object_to_closest_node();
    
    void publish_slam_region_particles();
    void publish_slam_transforms();
    slam_graph_region_particle_list_t *get_slam_region_particle_msg();
};

#endif
