/*  This file is part of libDAI - http://www.libdai.org/
 *
 *  Copyright (c) 2006-2011, The libDAI authors. All rights reserved.
 *
 *  Use of this source code is governed by a BSD-style license that can be found in the LICENSE file.
 */


#include <dai/alldai.h>  // Include main libDAI header file
#include <dai/jtree.h>
#include <dai/bp.h>
#include <dai/decmap.h>
#include <dai/factorgraph.h>
#include <iostream>
#include <fstream>

using namespace std;
using namespace dai;

int main() {
    // This example program illustrates how to construct a factorgraph
    // by means of the sprinkler network example discussed at
    // http://www.cs.ubc.ca/~murphyk/Bayes/bnintro.html

    //phi -1 
    Var P1(0,2); // 0  is not grounded  - 1 is grounded 
    Var lambda1(1,3); //0 - Kitchen, 1 - Hallway, 2 - Living room 
    Var Lambda1(5,3); ////0 - Kitchen, 1 - Hallway, 2 - Living room 

    Var P2(3,2);
    Var lambda2(4,3); //0 - Kitchen, 1 - Hallway, 2 - Living room 

    Factor P_P1(P1);  //poor grounding 
    /*P_P1.set(0,0.8);
      P_P1.set(1,.2); */

    P_P1.set(0,0.8);
    P_P1.set(1,0.2); 

    Factor P_P2(P2);  //good grounding 
    P_P2.set(0,0.1);
    P_P2.set(1,0.9); 

    Factor P_Lambda1(Lambda1);
    P_Lambda1.set(0,1.0);
    P_Lambda1.set(1,1.0);
    P_Lambda1.set(2,5.0);

    Factor P_lambda1_obs(lambda1);
    P_lambda1_obs.set(0,1.0);
    P_lambda1_obs.set(1,.0);
    P_lambda1_obs.set(2,.0);

    Factor P_lambda2_obs(lambda2);
    P_lambda2_obs.set(0,.0);
    P_lambda2_obs.set(1,1.0);
    P_lambda2_obs.set(2,.0);

    vector<Var> var_1; 
    var_1.push_back(P1);
    var_1.push_back(lambda1);
    var_1.push_back(Lambda1);
    VarSet w1(var_1.begin(), var_1.end());
    dai::Factor P_P1_lambda1_Lambda1(w1);//VarSet(P1, lambda1, Lambda1));
    
    double equal = 1/ 3.0;
    //indexed based on the id??
    //dai::Factor P_P1_lambda1_Lambda1(VarSet(lambda1, Lambda1));
    P_P1_lambda1_Lambda1.set(0, equal);  //P = 0, lambda = 0, Lambda = 0; 
    P_P1_lambda1_Lambda1.set(1, 1.0);  //P = 1, lambda = 0, Lambda = 0; 
    P_P1_lambda1_Lambda1.set(2, equal);  //P = 0, lambda = 1, Lambda = 0; 
    P_P1_lambda1_Lambda1.set(3, .1);  //P = 1, lambda = 1, Lambda = 0; 
    P_P1_lambda1_Lambda1.set(4, equal);  //P = 0, lambda = 2, Lambda = 0; 
    P_P1_lambda1_Lambda1.set(5, .1);  //P = 1, lambda = 2, Lambda = 0; 
    P_P1_lambda1_Lambda1.set(6, equal);  //P = 0, lambda = 0, Lambda = 0; 
    P_P1_lambda1_Lambda1.set(7, .1);  //P = 1, lambda = 0, Lambda = 1; 
    P_P1_lambda1_Lambda1.set(8, equal);  //P = 0, lambda = 1, Lambda = 1;
    P_P1_lambda1_Lambda1.set(9, 1.0);   //P = 1, lambda = 1, Lambda = 1; 
    P_P1_lambda1_Lambda1.set(10, equal);  //P = 0, lambda = 2, Lambda = 1; 
    P_P1_lambda1_Lambda1.set(11, .1);  //P = 1, lambda = 2, Lambda = 1; 
    P_P1_lambda1_Lambda1.set(12, equal);  //P = 0, lambda = 0, Lambda = 2; 
    P_P1_lambda1_Lambda1.set(13, .1);  //P = 1, lambda = 0, Lambda = 2; 
    P_P1_lambda1_Lambda1.set(14, equal);  //P = 0, lambda = 1, Lambda = 2; 
    P_P1_lambda1_Lambda1.set(15, .1);  //P = 1, lambda = 1, Lambda = 2; 
    P_P1_lambda1_Lambda1.set(16, equal);  //P = 0, lambda = 2, Lambda = 2; 
    P_P1_lambda1_Lambda1.set(17, 1.0);  //P = 1, lambda = 2, Lambda = 2; 
    //*/
    cout << "P1 " << P_P1_lambda1_Lambda1.marginal(P1) << endl;
    cout << "lambda1 " << P_P1_lambda1_Lambda1.marginal(lambda1) << endl;
    cout << "Lambda1 " << P_P1_lambda1_Lambda1.marginal(Lambda1) << endl;
    vector<Var> var_2; 
    var_2.push_back(P2);
    var_2.push_back(lambda2);
    var_2.push_back(Lambda1);
    VarSet w2(var_2.begin(), var_2.end());
    Factor P_P2_lambda2_Lambda1(w2); //VarSet(var_2.begin(), var_2.end()));//P2, lambda2, Lambda1));
    //*/   
    double phi_0 = 0;
    double phi_1 = 1.0;
    
    
    cout << "P2 " << P_P2_lambda2_Lambda1.marginal(P2) << endl;
    cout << "lambda2 " << P_P2_lambda2_Lambda1.marginal(lambda2) << endl;
    cout << "Lambda1 " << P_P2_lambda2_Lambda1.marginal(Lambda1) << endl;

    P_P2_lambda2_Lambda1.set(0, equal);  //P = 0, lambda = 0, Lambda = 0; 
    P_P2_lambda2_Lambda1.set(1, 1.0);  //P = 1, lambda = 0, Lambda = 0; 
    P_P2_lambda2_Lambda1.set(2, equal);  //P = 0, lambda = 1, Lambda = 0; 
    P_P2_lambda2_Lambda1.set(3, .1);  //P = 1, lambda = 1, Lambda = 0; 
    P_P2_lambda2_Lambda1.set(4, equal);  //P = 0, lambda = 2, Lambda = 0; 
    P_P2_lambda2_Lambda1.set(5, .1);  //P = 1, lambda = 2, Lambda = 0; 
    P_P2_lambda2_Lambda1.set(6, equal);  //P = 0, lambda = 0, Lambda = 0; 
    P_P2_lambda2_Lambda1.set(7, .1);  //P = 1, lambda = 0, Lambda = 1; 
    P_P2_lambda2_Lambda1.set(8, equal);  //P = 0, lambda = 1, Lambda = 1;
    P_P2_lambda2_Lambda1.set(9, 1.0);   //P = 1, lambda = 1, Lambda = 1; 
    P_P2_lambda2_Lambda1.set(10, equal);  //P = 0, lambda = 2, Lambda = 1; 
    P_P2_lambda2_Lambda1.set(11, .1);  //P = 1, lambda = 2, Lambda = 1; 
    P_P2_lambda2_Lambda1.set(12, equal);  //P = 0, lambda = 0, Lambda = 2; 
    P_P2_lambda2_Lambda1.set(13, .1);  //P = 1, lambda = 0, Lambda = 2; 
    P_P2_lambda2_Lambda1.set(14, equal);  //P = 0, lambda = 1, Lambda = 2; 
    P_P2_lambda2_Lambda1.set(15, .1);  //P = 1, lambda = 1, Lambda = 2; 
    P_P2_lambda2_Lambda1.set(16, equal);  //P = 0, lambda = 2, Lambda = 2; 
    P_P2_lambda2_Lambda1.set(17, 1.0);  //P = 1, lambda = 2, Lambda = 2; 

    /*
    //Factor P_P2_lambda2_Lambda1(VarSet(lambda2, Lambda1));
    P_P2_lambda2_Lambda1.set(0, .01);  //P = 0, lambda = 0, Lambda = 0; 
    P_P2_lambda2_Lambda1.set(1, .01);  //P = 0, lambda = 0, Lambda = 1; 
    P_P2_lambda2_Lambda1.set(2, .01);  //P = 0, lambda = 0, Lambda = 2; 
    P_P2_lambda2_Lambda1.set(3, .01);  //P = 0, lambda = 1, Lambda = 0; 
    P_P2_lambda2_Lambda1.set(4, .01);  //P = 0, lambda = 1, Lambda = 1; 
    P_P2_lambda2_Lambda1.set(5, .01);  //P = 0, lambda = 1, Lambda = 2; 
    P_P2_lambda2_Lambda1.set(6, .01);  //P = 0, lambda = 2, Lambda = 0; 
    P_P2_lambda2_Lambda1.set(7, .01);  //P = 0, lambda = 2, Lambda = 1; 
    P_P2_lambda2_Lambda1.set(8, .01);  //P = 0, lambda = 2, Lambda = 2; 
    
    P_P2_lambda2_Lambda1.set(9, 1.0);   //P = 1, lambda = 0, Lambda = 0; 
    P_P2_lambda2_Lambda1.set(10, .0);  //P = 1, lambda = 0, Lambda = 1; 
    P_P2_lambda2_Lambda1.set(11, .0);  //P = 1, lambda = 0, Lambda = 2; 
    P_P2_lambda2_Lambda1.set(12, 1.0);  //P = 1, lambda = 1, Lambda = 0; 
    P_P2_lambda2_Lambda1.set(13, .0);  //P = 1, lambda = 1, Lambda = 1; 
    P_P2_lambda2_Lambda1.set(14, .0);  //P = 1, lambda = 1, Lambda = 2; 
    P_P2_lambda2_Lambda1.set(15, 1.0);  //P = 1, lambda = 2, Lambda = 0; 
    P_P2_lambda2_Lambda1.set(16, .0);  //P = 1, lambda = 2, Lambda = 1; 
    P_P2_lambda2_Lambda1.set(17, .0);  //P = 1, lambda = 2, Lambda = 2;
    */
    

    vector<Factor> factors;
    //Prior - this doesn't add much (unlike the dirchlet)
    //factors.push_back(P_Lambda1);
    factors.push_back(P_P1);
    factors.push_back(P_P2);
    factors.push_back(P_lambda1_obs);
    factors.push_back(P_lambda2_obs);
    factors.push_back(P_P1_lambda1_Lambda1);
    factors.push_back(P_P2_lambda2_Lambda1);
    FactorGraph factor_graph( factors );

    // Store the constants in a PropertySet object
    dai::PropertySet opts;

    // Set some constants
    size_t maxiter = 10000;
    dai::Real   tol = 1e-9;
    size_t verb = 1;
    
    opts.set("maxiter", maxiter);  // Maximum number of iterations
    opts.set("tol", tol);          // Tolerance for convergence
    opts.set("verbose",verb);     // Verbosity (amount of output generated)
    
    dai::BP bp(factor_graph, opts("updates",string("SEQRND"))("logdomain",false));
    // Initialize belief propagation algorithm
    bp.init();
    // Run belief propagation algorithm
    bp.run();
    
    cout << P_P2_lambda2_Lambda1 << endl;

    Factor r_fac = bp.belief(Lambda1);
    cout << r_fac << endl;
    cout << bp.belief(P1) << endl;
    cout << bp.belief(lambda1) << endl;

    /*Factor P;
    for( size_t I = 0; I < factor_graph.nrFactors(); I++ )
        P *= factor_graph.factor( I );

    cout << P << endl;
    cout << P.marginal( lambda2 ) << endl;
    */
    /*Var C(0, 2);  // Define binary variable Cloudy (with label 0)
    Var S(1, 2);  // Define binary variable Sprinkler (with label 1)
    Var R(2, 2);  // Define binary variable Rain (with label 2)
    Var W(3, 2);  // Define binary variable Wetgrass (with label 3)

    // Define probability distribution for C
    Factor P_C( C );
    P_C.set(0, 0.5);   // C = 0
    P_C.set(1, 0.5);   // C = 1

    // Define conditional probability of S given C
    Factor P_S_given_C( VarSet( S, C ) );
    P_S_given_C.set(0, 0.5);   // C = 0, S = 0
    P_S_given_C.set(1, 0.9);   // C = 1, S = 0
    P_S_given_C.set(2, 0.5);   // C = 0, S = 1
    P_S_given_C.set(3, 0.1);   // C = 1, S = 1

    // Define conditional probability of R given C
    Factor P_R_given_C( VarSet( R, C ) );
    P_R_given_C.set(0, 0.8);   // C = 0, R = 0
    P_R_given_C.set(1, 0.2);   // C = 1, R = 0
    P_R_given_C.set(2, 0.2);   // C = 0, R = 1
    P_R_given_C.set(3, 0.8);   // C = 1, R = 1

    // Define conditional probability of W given S and R
    Factor P_W_given_S_R( VarSet( S, R ) | W );
    P_W_given_S_R.set(0, 1.0);  // S = 0, R = 0, W = 0
    P_W_given_S_R.set(1, 0.1);  // S = 1, R = 0, W = 0
    P_W_given_S_R.set(2, 0.1);  // S = 0, R = 1, W = 0
    P_W_given_S_R.set(3, 0.01); // S = 1, R = 1, W = 0
    P_W_given_S_R.set(4, 0.0);  // S = 0, R = 0, W = 1
    P_W_given_S_R.set(5, 0.9);  // S = 1, R = 0, W = 1
    P_W_given_S_R.set(6, 0.9);  // S = 0, R = 1, W = 1
    P_W_given_S_R.set(7, 0.99); // S = 1, R = 1, W = 1
    
    // Build factor graph consisting of those four factors
    vector<Factor> SprinklerFactors;
    SprinklerFactors.push_back( P_C );
    SprinklerFactors.push_back( P_R_given_C );
    SprinklerFactors.push_back( P_S_given_C );
    SprinklerFactors.push_back( P_W_given_S_R );
    FactorGraph SprinklerNetwork( SprinklerFactors );

    // Write factorgraph to a file
    SprinklerNetwork.WriteToFile( "sprinkler.fg" );
    cout << "Sprinkler network written to sprinkler.fg" << endl;

    // Output some information about the factorgraph
    cout << SprinklerNetwork.nrVars() << " variables" << endl;
    cout << SprinklerNetwork.nrFactors() << " factors" << endl;

    // Calculate joint probability of all four variables
    Factor P;
    for( size_t I = 0; I < SprinklerNetwork.nrFactors(); I++ )
        P *= SprinklerNetwork.factor( I );
    // P.normalize();  // Not necessary: a Bayesian network is already normalized by definition

    // Calculate some probabilities
    Real denom = P.marginal( W )[1];
    cout << "P(W=1) = " << denom << endl;
    cout << "P(S=1 | W=1) = " << P.marginal( VarSet( S, W ) )[3] / denom << endl;
    cout << "P(R=1 | W=1) = " << P.marginal( VarSet( R, W ) )[3] / denom << endl;
    */
    return 0;
}
