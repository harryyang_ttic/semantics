#include "OutputWriter.hpp"

GMutex* OutputWriter::mutex = g_mutex_new();

OutputWriter::OutputWriter(string _prefix, string file_name):name(file_name), prefix(_prefix){
    o_mutex = g_mutex_new();
    new_line = true;
}

OutputWriter::OutputWriter():name("test.log"), prefix("[Test]"){
    o_mutex = g_mutex_new();
    new_line = true;
}

void OutputWriter::write_to_output(string &output, bool reset){
    g_mutex_lock(OutputWriter::mutex);
    if(reset){
        std::ofstream out(name.c_str(), std::ofstream::out);
        out << output << endl;
        out.close();
    }
    else{
        std::ofstream out(name.c_str(), std::ofstream::app);
        out << output << endl;
        out.close();        
    }
    g_mutex_unlock(OutputWriter::mutex);
}

void OutputWriter::write_buffer(bool reset){
    g_mutex_lock(o_mutex);
    if(output_buffer.size() == 0){
        g_mutex_unlock(o_mutex);
        return;
    }
    g_mutex_lock(OutputWriter::mutex);
    
    if(reset){
        std::ofstream out(name.c_str(), std::ofstream::out);
        out << output_buffer << endl;
        out.close();
    }
    else{
        std::ofstream out(name.c_str(), std::ofstream::app);
        out << output_buffer << endl;
        out.close();        
    }
    output_buffer.clear();

    g_mutex_unlock(OutputWriter::mutex);
    g_mutex_unlock(o_mutex);
}


//might want to have a buffer that gets flushed on call 
void OutputWriter::write_to_buffer(string output, bool new_line_begin, bool new_line_end, bool write){
    g_mutex_lock(o_mutex);
    if(new_line_begin || new_line){    
        if(!new_line)
            output_buffer.append("\n");
        output_buffer.append(prefix);
    }
    
    output_buffer.append(output);
    new_line = false;
    
    if(new_line_end && !write){
        output_buffer.append("\n");            
        new_line = true;
    }
    
    if(write && output_buffer.size() > 0){
        new_line = true;
        write_to_output(output_buffer);
        output_buffer.clear();
    }
    g_mutex_unlock(o_mutex);
}

void OutputWriter::write_to_buffer(const char *_output, bool new_line_begin, bool new_line_end, bool write){
    g_mutex_lock(o_mutex);
    string output(_output);
    if(new_line_begin || new_line){    
        if(!new_line)
            output_buffer.append("\n");
        output_buffer.append(prefix);
    }
    
    output_buffer.append(output);
    new_line = false;
    
    if(new_line_end && !write){
        output_buffer.append("\n");            
        new_line = true;
    }
    
    if(write && output_buffer.size() > 0){
        new_line = true;
        write_to_output(output_buffer);
        output_buffer.clear();
    }
    g_mutex_unlock(o_mutex);
}

