#include <Eigen/Eigenvalues>
#include <unistd.h>
#include <stdio.h>
#include <iostream>

using namespace std;

using namespace Eigen;


typedef struct {
    int size;
    int *node_id;
    double **mat; //size x size matrix
} similarity_t; 

//n_cut theshold recomended 0.7
MatrixXd doSpectralClustering(similarity_t &score, int recursive, double n_cut_threshold, int *no_segments, double *n_cut_result);

