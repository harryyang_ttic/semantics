#include "spectral_cluster.hpp"
using namespace std;

//method based on paper Consistent Observation Grouping for Generating Metric-Topological Maps that Improves Robot Localization

MatrixXd do_spectral_bisection(MatrixXd A, int size, MatrixXd ind, int segment_id, int *segmented, int recursive, double n_cut_threshold, double *n_cut_result){

    *segmented = 0;
    
    //cout << "Here is the similarity matrix, A:" << endl << A << endl << endl;
    
    MatrixXd S = MatrixXd::Ones(size,1);

    MatrixXd D = MatrixXd::Zero(size,size);
    MatrixXd D_s = A*S; 

    for(int i=0; i < size; i++){
        D(i,i) = D_s(i,0);
    }
    
    //Where W is symetric square matrix where the elements are the weights of the arc between nodes 
    //D, where d_ij = Sum_j{W_ij}

    //cout << "Diagonal Mat:" << endl << D << endl << endl;

    MatrixXd D_W = D - A;

    //cout << "Diagonal - W :" << endl << D_W << endl << endl;

    SelfAdjointEigenSolver<MatrixXd> D_sq(D);
    MatrixXd D_sq_inv_val = D_sq.operatorInverseSqrt();

    MatrixXd D_sq_val = D_sq.operatorInverseSqrt();

    //D^{-1/2}*(D-W)*D^{-1/2}y=\lambda*D*y
    EigenSolver<MatrixXd> es( D_sq_inv_val * D_W * D_sq_inv_val);

    //calculate the Ncut associated with the segmentation - to see if there is a valid cut 

    //find the second smallest eigen vector 
    //figure out the second smallest eigen vector
    double min = 10000;
    double second_min = 10000;
    int second_ind = -1;
    int min_ind = -1;

    for(int i=0;i< size; i++){
        complex<double> lambda1 = es.eigenvalues()[i];
        if(min > lambda1.real()){
            min = lambda1.real();
            min_ind = i; 
        }
    }

    for(int i=0;i< size; i++){
        complex<double> lambda1 = es.eigenvalues()[i];
        if(second_min > lambda1.real() && min < lambda1.real()){
            second_min = lambda1.real();
            second_ind = i; 
        }
    }

    //cout << "Min Value Ind : " << min_ind << " Second Min " << second_ind << "Second Val : " << second_min << endl;
    
    //cout << "The matrix of eigenvectors, V, is:" << endl << es.eigenvectors() << endl << endl;
    
    //we have a second smallest eigen vector 
    if(second_ind >=0){
        MatrixXd v = MatrixXd::Zero(size,1);
        for(int i=0;i< size; i++){
            v(i,0) = es.eigenvectors()(i, second_ind).real();
        }

        MatrixXd v_eig = D_sq_inv_val * v;

        double mean = 0;
        for(int i=0;i< size; i++){
            mean += v_eig(i,0);
        }

        mean = mean / (double) size;

        //cout << "Second Smallest Eigen Vector " <<endl << v_eig<< endl;
        complex<double> lambda1 = es.eigenvalues()[second_ind];
        MatrixXd top =  v_eig.transpose() * D_W * v_eig;

        //not sure why bottom is always 1
        MatrixXd bottom = v_eig.transpose() * D * v_eig;
        //cout << "Top : " << top << endl;

        //cout << "Bottom : " << bottom << endl;

        double N_cut = top(0,0) / bottom(0,0);

        *n_cut_result = N_cut;
        //cout << "N Cut " << N_cut << endl; 

        //threshold to decide whether to make a graph cut
        //if above threshold - then not enough difference to split 
        if(N_cut > n_cut_threshold){
            MatrixXd final_result = MatrixXd::Zero(size,1);
            for(int i=0; i< size; i++){
                final_result(i) = segment_id;
            }
            *segmented = 1;
            return final_result;
        }        

        //calculate the mean of the eigen vector 
        int count_1 = 0;
        int count_2 = 0;

        for(int i=0;i< size; i++){
            if(v(i,0) <= mean){
                count_1 += 1;
                //cout << "Node : " << ind(i) << " : -1" << endl;
            }
            else{
                count_2 += 1;
                //cout << "Node : " << ind(i) << " : +1" << endl;
            }
        }
        
        //if not recursive - split once 
        if(recursive == 0){
            MatrixXd final_result = MatrixXd::Zero(size,1);
            
            //the first index should get the lower segment id 
            int lower_ind, upper_ind;
            if(v(0,0) <= mean){
                lower_ind = segment_id;
                upper_ind = segment_id + 1;
            }
            else{
                lower_ind = segment_id +1;
                upper_ind = segment_id;
            }
            
            for(int i=0;i< size; i++){
                if(v(i,0) <= mean){
                    final_result(i) = lower_ind;//segment_id;
                }
                else{
                    final_result(i) = upper_ind;//segment_id +1;
                }
            }

            *segmented = 2;
            return final_result;
        }

        //if recursive - keep splitting till the criteria is met
        MatrixXd ind_1 = MatrixXd::Zero(count_1,1);
        MatrixXd ind_2 = MatrixXd::Zero(count_2,1);

        int index_1 = 0;
        int index_2 = 0;

        for(int i=0;i< size; i++){
            if(v(i,0) <= mean){
                ind_1(index_1) = i;
                index_1 += 1;
            }
            else{
                ind_2(index_2) = i;
                index_2 += 1;
            }
        }

        //cout << "Index 1 " << endl<< ind_1 << endl;
        //cout << "Index 2 " << endl << ind_2 << endl;

        MatrixXd A_1 = MatrixXd::Zero(count_1,count_1);
        MatrixXd A_2 = MatrixXd::Zero(count_2,count_2);
        MatrixXd index_list_1 = MatrixXd::Zero(count_1,1);
        MatrixXd index_list_2 = MatrixXd::Zero(count_2,1);
                
        for(int i=0;i< count_1; i++){
            int i_1 = ind_1(i);
            for(int j=0;j< count_1; j++){
                int j_1 = ind_1(j);
                //cout << i_1 << "," << j_1 << " : " << A(i_1,j_1); 
                A_1(i,j) = A(i_1,j_1);                
            }
            index_list_1(i) = ind(i_1);
        }

        //cout << "A 1 " << endl<< A_1 << endl;
        
        for(int i=0;i< count_2; i++){
            int i_2 = ind_2(i);
            for(int j=0;j< count_2; j++){
                int j_2 = ind_2(j);
                //cout << i_2 << "," << j_2 << " : " << A(i_2,j_2); 
                A_2(i,j) = A(i_2,j_2);
            }
            index_list_2(i) = ind(i_2);
        }

        MatrixXd result_1, result_2;

        //just indicate the split for now 
        int segmented_1 = 0, segmented_2 = 0;

        MatrixXd final_result = MatrixXd::Zero(size,1);

        //try to split the segments further 
        if(count_1 >1){
            fprintf(stderr,"Calling Bisection on the minus set : %d\n", count_1);
            double n_cut_1 = 0;
            result_1 = do_spectral_bisection(A_1, count_1, index_list_1, segment_id, &segmented_1, 1, n_cut_threshold, &n_cut_1);

            cout << "Segmented Result : " << segmented_1 << endl; 
            cout << "result 1 : " << endl << result_1; 
            for(int i=0;i< count_1; i++){
                int i_1 = ind_1(i);
                final_result(i_1) = result_1(i);                
            }
            *segmented += segmented_1;
        }
        else{
            //one element 
            for(int i=0;i< count_1; i++){
                int i_1 = ind_1(i);
                final_result(i_1) = segment_id;                
            }
            *segmented++;
        }

        segment_id += *segmented; 
       
        if(count_2>1){
            fprintf(stderr,"Calling Bisection on the plus set : %d\n", count_2);
            double n_cut_2 = 0;
            result_2 = do_spectral_bisection(A_2, count_2, index_list_2, segment_id, &segmented_2, 1, n_cut_threshold, &n_cut_2);

            for(int i=0;i< count_2; i++){
                int i_2 = ind_2(i);
                final_result(i_2) = result_2(i);                
            }
            *segmented += segmented_2;
        }
        else{
            //one element 
            for(int i=0;i< count_2; i++){
                int i_1 = ind_2(i);
                final_result(i_1) = segment_id + *segmented;                
            }
            *segmented++;
        }
       
        cout << "Done "<< endl; 
        
        return final_result;
    }
}



MatrixXd doSpectralClustering(similarity_t &score, int recursive, double n_cut_threshold, int *no_segments, double *n_cut_result){
    /*for(int i=0; i < score.size; i++){
        fprintf(stderr, "\n");
        for(int j=0; j < score.size; j++){
            fprintf(stderr, "\t%.3f", score.mat[i][j]);
        }
        }*/

    MatrixXd index_list = MatrixXd::Zero(score.size,1);
    MatrixXd A = MatrixXd::Zero(score.size,score.size);

    for(int i=0; i<score.size;i++){
        index_list(i) = score.node_id[i];
        for(int j=0; j< score.size; j++){
            A(i,j) = score.mat[i][j];
        }
    }
    
    MatrixXd result = do_spectral_bisection(A, score.size, index_list, 0, no_segments, recursive, n_cut_threshold, n_cut_result);

    //cout << result << endl;
    //fprintf(stderr, "No of segments : %d\n", *no_segments);
    return result;
}
