// Tool to perform ground truth labeling of regions
//
// To begin an annotation
//  1) Click "Select Annotation File" to specify where to save the annotation
//  2) Click "Begin annotation when you are ready to start"
//  3) Select the annotation type from the drop-down box
//  4) Use the scrollwheel button to select the first and subsequent points that define the polygon
//  5) Click the right mouse button when you are done with a region and the last point will connected with the first
//  6) Continue to label regions
//  7) When you are done, clicking "Save Selection" will save the output

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <glib.h>
#include <string.h>
#include <assert.h>
#include <gdk/gdkkeysyms.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif


#include <bot_core/bot_core.h>
#include <bot_vis/bot_vis.h>
#include <bot_vis/gtk_util.h>
#include <bot_frames/bot_frames.h>
#include <geom_utils/geometry.h>

#define RENDERER_NAME "Ground Truth Regions"

#define PARAM_SELECT_FILE "Select Annotation File"
#define PARAM_CLEAR_CURRENT_REGION "Clear Current Region"
#define PARAM_BEGIN_ANNOTATION "Begin Annotation"
#define PARAM_SAVE_SELECTION "Save Selection"
#define PARAM_NODE_ANNOTATION "Region Type: "
#define PARAM_CLEAR_ALL_REGIONS "Clear All Regions"

#define MAX_NUM_ANNOTATIONS 1000

#define REGION_ERROR_DOMAIN (g_quark_from_string("RendererGroundTruthRegions"))


typedef struct _RegionSegmentation {
    int num_points;
    double *x;
    double *y;
    char *type;
    char *label;
} RegionSegmentation;

typedef struct _RendererRegionSegmentation RendererRegionSegmentation;
struct _RendererRegionSegmentation {
    BotRenderer renderer;
    BotEventHandler ehandler;
    BotViewer         *viewer;
    BotGtkParamWidget *pw;      

    lcm_t *lcm;

    int is_annotating; 
    int in_active_region;
    int active;

    int num_types;
    char ** type_names;
    int *type_nums;
    BotPtrCircular *annotation_list;
    RegionSegmentation *current_active_region;

    int have_data;
    GHashTable *slam_nodes;
    BotPtrCircular   *data_circ;

    GList *annotated_nodes;

    GMutex *mutex;
    
    gchar *annotation_filename;
} ;

typedef struct {
    RendererRegionSegmentation *renderer_region_segmentation;
    int in_region;
    int in_label;
    int in_type;
    int in_vertex;
    int in_num_vertices;
    int in_child_field;
    RegionSegmentation *region;
    int current_vertex_count;
} RegionsParseContext;



RegionSegmentation *region_segmentation_copy (RegionSegmentation *src)
{
    RegionSegmentation *dest =  (RegionSegmentation *) calloc (1, sizeof (RegionSegmentation));
    dest->num_points = src->num_points;
    dest->x = (double *) calloc (1, dest->num_points *sizeof(double));
    dest->y = (double *) calloc (1, dest->num_points *sizeof(double));
    memcpy (dest->x, src->x, src->num_points * sizeof(double));
    memcpy (dest->y, src->y, src->num_points * sizeof(double));
    if (src->type)
        dest->type = strdup (src->type);
    else 
        dest->type = NULL;
    if (src->label)
        dest->label = strdup (src->label);
    else 
        dest->label = NULL;
    return dest;
}


void region_segmentation_destroy(void *user, void *p)
{
    RegionSegmentation *region = (RegionSegmentation *) p;
    free(region->x);
    free(region->y);
    if (region->type)
        free (region->type);
    if (region->label)
        free (region->label);
    free(region);
}


static void
_start_element (GMarkupParseContext *ctx, const char *element_name,
                const char **attribute_names, const char **attribute_values,
                void *user_data, GError **error)
{
    RegionsParseContext *rpc = user_data;

    if (!strcmp (element_name, "region")) {
        if (rpc->region) {
            *error = g_error_new (REGION_ERROR_DOMAIN, 0,
                                  "Unexpected <region> element");
            return;
        }
        
        rpc->region = (RegionSegmentation *) calloc (1, sizeof (RegionSegmentation));
        rpc->in_region = 1;
        rpc->in_child_field = 0;
        rpc->current_vertex_count = 0;
    }
    else if (!strcmp (element_name, "type")) {
        if (!rpc->in_region || !rpc->region || rpc->in_child_field) {
            *error = g_error_new (REGION_ERROR_DOMAIN, 0,
                                  "Unexpected <type> element");
            return;
        }
        rpc->in_type = 1;
        rpc->in_child_field = 1;
    }
    else if (!strcmp (element_name, "label")) {
        if (!rpc->in_region || !rpc->region || rpc->in_child_field) {
            *error = g_error_new (REGION_ERROR_DOMAIN, 0,
                                  "Unexpected <label> element");
            return;
        }
        rpc->in_label = 1;
        rpc->in_child_field = 1;
    }
    else if (!strcmp (element_name, "num_vertices")) {
        if (!rpc->in_region || !rpc->region || rpc->in_child_field) {
            *error = g_error_new (REGION_ERROR_DOMAIN, 0,
                                  "Unexpected <num_vertices> element");
            return;
        }
        rpc->in_num_vertices = 1;
        rpc->in_child_field = 1;
    }
    else if (!strcmp (element_name, "vertex")) {
        if (!rpc->in_region || !rpc->region || rpc->in_child_field) {
            *error = g_error_new (REGION_ERROR_DOMAIN, 0,
                                  "Unexpected <vertex> element");
            return;
        }
        rpc->in_vertex = 1;
        rpc->in_child_field = 1;
    }
}      


static void
_end_element (GMarkupParseContext *ctx, const char *element_name,
              void *user_data, GError **error)
{

    RegionsParseContext *rpc = user_data;
    
    if (!strcmp (element_name, "type")) {
        rpc->in_type = 0;
        rpc->in_child_field = 0;
    }
    else if (!strcmp (element_name, "label")) {
        rpc->in_label = 0;
        rpc->in_child_field = 0;
    }
    else if (!strcmp (element_name, "num_vertices")) {
        rpc->in_num_vertices = 0;
        rpc->in_child_field = 0;
    }
    else if (!strcmp (element_name, "vertex")) {
        rpc->in_vertex = 0;
        rpc->in_child_field = 0;
    }
    else if (!strcmp (element_name, "region")) {
        if (rpc->region) {
            bot_ptr_circular_add (rpc->renderer_region_segmentation->annotation_list, region_segmentation_copy (rpc->region));
            fprintf (stdout, "Adding region of type %s to buffer, which has size %d\n",
                     rpc->region->type, bot_ptr_circular_size (rpc->renderer_region_segmentation->annotation_list));
            region_segmentation_destroy (NULL, rpc->region);
            rpc->region = NULL;
        }

        rpc->in_region = 0;
        rpc->in_child_field = 0;
    }
}      

static void
_text (GMarkupParseContext *ctx, const char *text, gsize text_len, 
       void *user_data, GError **error)
{
    RegionsParseContext *rpc = user_data;
    char buf[text_len + 1];
    memcpy (buf, text, text_len);
    buf[text_len] = 0;
    int args_assigned = 0;
    if (rpc->in_label) {
        char *label = (char *) calloc (1, text_len * sizeof(char));
        args_assigned = sscanf (buf, "%s", label);
        if (args_assigned != 1)
            *error = g_error_new (REGION_ERROR_DOMAIN, 0,
                                  "invalid value [%s] for label", buf);
        else {
            if (rpc->region)
                rpc->region->label = strdup (label);
            else
                *error = g_error_new (REGION_ERROR_DOMAIN, 0,
                                      "unexpected value [%s] for label", buf);
        }
        free (label);
    }
    else if (rpc->in_type) {
        char *type = (char *) calloc (1, text_len * sizeof(char));
        args_assigned = sscanf (buf, "%s", type);
        if (args_assigned != 1)
            *error = g_error_new (REGION_ERROR_DOMAIN, 0,
                                  "invalid value [%s] for type", buf);
        else {
            if (rpc->region)
                rpc->region->type = strdup (type);
            else
                *error = g_error_new (REGION_ERROR_DOMAIN, 0,
                                      "unexpected value [%s] for type", buf);
        }
        free (type);
    }
    else if (rpc->in_vertex) {
        double x, y;
        args_assigned = sscanf (buf, "%lf %lf", &x, &y);
        if (args_assigned != 2)
            *error = g_error_new (REGION_ERROR_DOMAIN, 0,
                                  "invalid value [%s] for vertices", buf);
        else {
            if (rpc->region) {
                
                if (rpc->region->x && rpc->region->y && (rpc->current_vertex_count < rpc->region->num_points)) {
                    rpc->region->x[rpc->current_vertex_count] = x;
                    rpc->region->y[rpc->current_vertex_count] = y;
                    rpc->current_vertex_count++;
                } else
                    *error = g_error_new (REGION_ERROR_DOMAIN, 0,
                                          "unable to add vertex [%s] when the number of vertices for current region is listed as %d and the current count is %d", buf, rpc->region->num_points, rpc->current_vertex_count);
            }
            else
                *error = g_error_new (REGION_ERROR_DOMAIN, 0,
                                      "unexpected value [%s] for vertex", buf);
        }
    }
    else if (rpc->in_num_vertices) {
        int num_vertices;
        args_assigned = sscanf (buf, "%d", &num_vertices);
        if (args_assigned != 1)
            *error = g_error_new (REGION_ERROR_DOMAIN, 0,
                                  "invalid value [%s] for num_vertices", buf);
        else {
            if (rpc->region) {
                rpc->region->num_points = num_vertices;
                rpc->region->x = (double *) calloc (1, num_vertices*sizeof(double));
                rpc->region->y = (double *) calloc (1, num_vertices*sizeof(double));
            }
            else
                *error = g_error_new (REGION_ERROR_DOMAIN, 0,
                                      "unexpected value [%s] for num_vertices", buf);
        }
    }
    
}


static void
_parse_error (GMarkupParseContext *ctx, GError *error, void *user_data)
{
    fprintf (stderr, "parse error?\n");
    fprintf (stderr, "%s\n", error->message);
    fprintf (stderr, "===\n");
}



static GMarkupParser _parser = {
    .start_element = _start_element,
    .end_element = _end_element,
    .text = _text,
    .passthrough = NULL,
    .error = _parse_error
};


void
regions_load_from_str (RendererRegionSegmentation *self, const char *xml_str, GError **error)
{
    RegionsParseContext rpc = {
        .renderer_region_segmentation = self,
        .in_region = 0,
        .in_label = 0,
        .in_type = 0,
        .in_vertex = 0,
        .in_num_vertices = 0,
        .in_child_field = 0,
        .region = NULL,
        .current_vertex_count = 0
    };

    GMarkupParseContext *otx = g_markup_parse_context_new (&_parser,
                                                           0, &rpc, NULL);

    GError *parse_err = NULL;
    g_markup_parse_context_parse (otx, xml_str, strlen (xml_str), &parse_err);

//    int result = parse_err ? -1 : 0;
    if (parse_err) {
        if (error) {
            *error = parse_err;
        } else {
            g_error_free (parse_err);
        }
    }

    g_markup_parse_context_free (otx);  
}


GError *
process_xml_file (RendererRegionSegmentation *self)
{
    char *text = NULL;
    GError *gerr = NULL;

    if (g_file_get_contents (self->annotation_filename, &text, NULL, &gerr)) {
        fprintf (stdout, "Loading ground truth annotations from XML file %s\n", self->annotation_filename);
        regions_load_from_str (self, text, &gerr);
    }
    
    return gerr;
}





static void
renderer_region_segmentation_destroy (BotRenderer *renderer)
{
    if (!renderer)
        return;

    RendererRegionSegmentation *self = (RendererRegionSegmentation *) renderer->user;
    if (!self)
        return;
    
    free (self);
}



static void 
renderer_region_segmentation_draw (BotViewer *viewer, BotRenderer *renderer)
{
    RendererRegionSegmentation *self = (RendererRegionSegmentation*)renderer->user;
    g_assert(self);


    if (self->current_active_region) {
        glColor3f (0, 0, 1);
        glLineWidth (5);
        glBegin (GL_LINE_STRIP);
        for (int i=0; i<self->current_active_region->num_points; i++)
            glVertex3f (self->current_active_region->x[i], self->current_active_region->y[i], 0.0);

        glEnd();
    }

    int num_annotations = bot_ptr_circular_size (self->annotation_list);

    if (num_annotations == 0)
        return;
        
    for (int i=0; i < bot_ptr_circular_size (self->annotation_list); i++) {
        
        RegionSegmentation *region = (RegionSegmentation *) bot_ptr_circular_index (self->annotation_list, i);
        glColor3f (1, 0, 0);
        glLineWidth (5.0);
        glBegin (GL_LINE_LOOP);
        double x_mid = 0;
        double y_mid = 0;
        for (int j=0; j < region->num_points; j++) {
            glVertex3f (region->x[j], region->y[j], 0.0);
            x_mid += region->x[j];
            y_mid += region->y[j];
        }
        glEnd();
        double pos[] = {x_mid/region->num_points, y_mid/region->num_points, 0.1};
        if (region->type)
            bot_gl_draw_text(pos, GLUT_BITMAP_HELVETICA_12, region->type,
                             BOT_GL_DRAW_TEXT_DROP_SHADOW);
    }

        
    int max_prob_id = 0; 
    int min_prob_id = 0;
    int count = 0;
        
    char prob_status[100];
        
    if(count == 0)
        return;
    double scale = 1.0;
        
 
}

static void activate(RendererRegionSegmentation *self, int type)
{
    self->active = type;
    if(type==0){
        self->is_annotating = 0;
        self->in_active_region = 0;
        fprintf(stderr,"Reset.\n");
    }
    if(type==1){
        self->is_annotating = 1;
        self->in_active_region = 0;
        fprintf(stderr,"Ready for first click\n");
    }
    if(type==2){
        self->in_active_region = 1;
        fprintf (stdout, "Actively selecting region\n");
    }
}





static int mouse_press (BotViewer *viewer, BotEventHandler *ehandler,
                        const double ray_start[3], const double ray_dir[3], 
                        const GdkEventButton *event)
{
    RendererRegionSegmentation *self = (RendererRegionSegmentation*) ehandler->user;

    double xy[2];
    int consumed = 0;

    geom_ray_z_plane_intersect_3d(POINT3D(ray_start), POINT3D(ray_dir), 
                                  0, POINT2D(xy));

    if (event->button == 2) {
        // We are creating a new region
        if (self->active == 1) {
            int type_index =  bot_gtk_param_widget_get_enum (self->pw, PARAM_NODE_ANNOTATION);
            if (type_index == 0) {
                fprintf (stderr, "Please select a type first\n");
                return consumed;
            }
            activate (self, 2);
            
            assert (!self->current_active_region);
            self->current_active_region = (RegionSegmentation *) calloc (1, sizeof (RegionSegmentation));

            self->current_active_region->num_points = 1;
            self->current_active_region->x = (double *) calloc (1, sizeof(double));
            self->current_active_region->y = (double *) calloc (1, sizeof(double));
            self->current_active_region->type = strdup (self->type_names[type_index]);
            self->current_active_region->label = NULL;

            self->current_active_region->x[0] = xy[0];
            self->current_active_region->y[0] = xy[1];
        }
        else if (self->active == 2) { // We are adding a point to the currently active region
            assert (self->current_active_region);
            self->current_active_region->num_points += 1;
            self->current_active_region->x = (double *) realloc (self->current_active_region->x,
                                                                 self->current_active_region->num_points * sizeof(double));
            self->current_active_region->x[self->current_active_region->num_points-1] = xy[0];

            self->current_active_region->y = (double *) realloc (self->current_active_region->y,
                                                                 self->current_active_region->num_points * sizeof(double));
            self->current_active_region->y[self->current_active_region->num_points-1] = xy[1];
            

        }
        else if (self->active == 0) {
            activate (self, 0);
            return consumed;
        }
    }
    else if (event->button == 3) { // right-click
        // If we are in an active region, finish it off
        if (self->active == 2) {
            activate (self, 1);
            
            bot_ptr_circular_add (self->annotation_list, region_segmentation_copy (self->current_active_region));
            region_segmentation_destroy (NULL, self->current_active_region);
            self->current_active_region = NULL;

            return consumed;
        }
    }

    bot_viewer_request_redraw(viewer);

    return consumed;
}


static void
on_param_widget_changed (BotGtkParamWidget *pw, const char *name, void *user)
{
    RendererRegionSegmentation *self = user;
    

    if (!strcmp(name, PARAM_SELECT_FILE)) {

        GtkWidget *dialog;
        dialog = gtk_file_chooser_dialog_new("Add particle annotations to file", NULL,
                                             GTK_FILE_CHOOSER_ACTION_SAVE,
                                             GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                             GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
                                             NULL);
        
        if (self->annotation_filename)
            gtk_file_chooser_set_filename (GTK_FILE_CHOOSER(dialog),
                                           self->annotation_filename);
        
        if (gtk_dialog_run (GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT) {
            char *filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
            if (filename != NULL) {
                if (self->annotation_filename)
                    g_free (self->annotation_filename);
                self->annotation_filename = g_strdup (filename);
                
                free (filename);

                GError *gerr = process_xml_file (self);
                if (gerr) 
                    fprintf (stderr, "Error loading ground truth region annotations from XML file %s\n",
                             self->annotation_filename);
            }
        }
        
        gtk_widget_destroy (dialog);
    }

    if (!strcmp(name, PARAM_BEGIN_ANNOTATION)) {
        if (self->active == 0)
            activate (self, 1);
        else
            fprintf (stdout, "Ignoring button press - Already in active mode\n");
    }

    if (!strcmp(name, PARAM_SAVE_SELECTION)){

        if(self->annotation_filename == NULL)
            fprintf (stderr, "Please Select a file\n");
        else{
            FILE *fp = fopen (self->annotation_filename, "w");
            
            fprintf (fp, "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n");
            for (int i=0; i < bot_ptr_circular_size (self->annotation_list); i++) {
                
                RegionSegmentation *region = (RegionSegmentation *) bot_ptr_circular_index (self->annotation_list, i);
                fprintf (fp, "<region>\n");

                if (region->type)
                    fprintf (fp, "    <type>%s</type>\n", region->type);
                else
                    fprintf (fp, "    <type></type>\n");

                fprintf (fp, "    <num_vertices>%d</num_vertices>\n", region->num_points);

                for (int j=0; j < region->num_points; j++) 
                    fprintf (fp, "    <vertex id=\"x y\">%.4f %.4f</vertex>\n", region->x[j], region->y[j]);
                fprintf (fp, "</region>\n\n");
            }
            
            //append the nodes to the annotated list
            fclose (fp);
    
            // Free the annotation list
            //bot_ptr_circular_clear (self->annotation_list);

            //self->is_annotating = 0;
            //self->in_active_region = 0;
        }
    }  

    if (!strcmp(name, PARAM_CLEAR_CURRENT_REGION)) {
        if (self->active == 2) {
            region_segmentation_destroy (NULL, self->current_active_region);
            self->current_active_region = NULL;
            activate (self, 1);
        }
    }

    if (!strcmp(name, PARAM_CLEAR_ALL_REGIONS)) {
        // Clear the current list of annotations
        bot_ptr_circular_clear (self->annotation_list);
        // Clear the current active region if there is one
        if (self->current_active_region) {
            region_segmentation_destroy (NULL, self->current_active_region);
            self->current_active_region = NULL;
        }
        activate (self, 0);
    }

    bot_viewer_request_redraw (self->viewer);
}
      

static void
on_load_preferences (BotViewer *viewer, GKeyFile *keyfile, void *user_data)
{
    RendererRegionSegmentation *self = user_data;
    bot_gtk_param_widget_load_from_key_file (self->pw, keyfile, self->renderer.name);
}

static void
on_save_preferences (BotViewer *viewer, GKeyFile *keyfile, void *user_data)
{
    RendererRegionSegmentation *self = user_data;
    bot_gtk_param_widget_save_to_key_file (self->pw, keyfile, self->renderer.name);
}



static RendererRegionSegmentation *
renderer_region_segmentation_new (BotViewer *viewer, int priority, BotParam * param)
{    
    RendererRegionSegmentation *self = (RendererRegionSegmentation*) calloc (1, sizeof (*self));

    self->viewer = viewer;

    self->annotation_list = bot_ptr_circular_new (MAX_NUM_ANNOTATIONS, region_segmentation_destroy, self);
    self->current_active_region = NULL;
    self->is_annotating = 0;
    self->in_active_region = 0;

    BotRenderer *renderer = &self->renderer;
    renderer->draw = renderer_region_segmentation_draw;
    renderer->destroy = renderer_region_segmentation_destroy;
    renderer->widget = bot_gtk_param_widget_new();
    renderer->name = RENDERER_NAME;
    renderer->user = self;
    renderer->enabled = 1;

    self->lcm = bot_lcm_get_global (NULL);
    if (!self->lcm) {
        fprintf (stderr,"Error: setup_renderer_laser() failed to get global lcm object\n");
        renderer_region_segmentation_destroy (renderer);
        return NULL;
    }

    self->mutex = g_mutex_new ();

    // Hard-coded types
    self->num_types = 8;
    self->type_names = calloc (self->num_types, sizeof (char*));
    self->type_names[0] = "noneselected";
    self->type_names[1] = "elevator";
    self->type_names[2] = "conferenceroom";
    self->type_names[3] = "office";
    self->type_names[4] = "lab";
    self->type_names[5] = "openarea";
    self->type_names[6] = "hallway";
    self->type_names[7] = "classroom"; 
    
    self->type_nums = calloc (1, self->num_types * sizeof(int));
    for (int i = 0; i < self->num_types; i++){
        self->type_nums[i] = i;
    }

    self->pw = BOT_GTK_PARAM_WIDGET (renderer->widget);
    
    gtk_widget_show_all (renderer->widget);
    g_signal_connect (G_OBJECT (self->pw), "changed", 
                      G_CALLBACK (on_param_widget_changed), self);
    g_signal_connect (G_OBJECT (viewer), "load-preferences", 
                      G_CALLBACK (on_load_preferences), self);
    g_signal_connect (G_OBJECT (viewer), "save-preferences",
                      G_CALLBACK (on_save_preferences), self);


                                       
                                     
    // BUTTON - Create new file to write language types
    bot_gtk_param_widget_add_buttons (self->pw, PARAM_SELECT_FILE, NULL);   
                                  
    bot_gtk_param_widget_add_enumv (self->pw, PARAM_NODE_ANNOTATION, BOT_GTK_PARAM_WIDGET_DEFAULTS, 
                                    0, self->num_types, (const char **) self->type_names, self->type_nums);
//bot_gtk_param_widget_add_text_entry(self->pw, PARAM_NODE_ANNOTATION, BOT_GTK_PARAM_WIDGET_ENTRY, "Insert Particle Annotation Here");


    // BUTTON - Begins annotation selections
    bot_gtk_param_widget_add_buttons (self->pw, PARAM_BEGIN_ANNOTATION, NULL);   

    // BUTTON - Saves annotations
    bot_gtk_param_widget_add_buttons (self->pw, PARAM_SAVE_SELECTION, NULL);

    // BUTTON - Clears Current Selection
    bot_gtk_param_widget_add_buttons(self->pw, PARAM_CLEAR_CURRENT_REGION, NULL);

    // BUTTON - Clears all annotations
    bot_gtk_param_widget_add_buttons(self->pw, PARAM_CLEAR_ALL_REGIONS, NULL);


    self->ehandler.name = (char*)RENDERER_NAME;
    self->ehandler.enabled = 1;
    self->ehandler.mouse_press = mouse_press;
    self->ehandler.user = self;
    
    bot_viewer_add_event_handler(viewer, &self->ehandler, priority);
    //tells us when to dump the old buffer
    
    self->active = 0;

    return self;
}

void
setup_renderer_region_segmentation (BotViewer *viewer, int priority, BotParam * param)
{
    RendererRegionSegmentation *self = renderer_region_segmentation_new (viewer, priority, param);
    bot_viewer_add_renderer (viewer, &self->renderer, priority);
}


