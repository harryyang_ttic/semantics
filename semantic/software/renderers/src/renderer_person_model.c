#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

#include <lcm/lcm.h>

#include <bot_core/bot_core.h>
#include <bot_vis/bot_vis.h>
#include <bot_frames/bot_frames.h>
#include <path_util/path_util.h>
#include "er_gl_utils.h"

#include <lcmtypes/er_lcmtypes.h>

#define QUAD_COORD_FRAME "body"

#define QUAD_COORD_FRAME_CORRECTED "body_g"

#define RENDERER_NAME "Person"

#define PARAM_DRAW_TEST_PERSON "Draw Test person"

#define PARAM_X_POS "X Pos"
#define PARAM_Y_POS "Y Pos"
#define PARAM_T_POS "Theta"

#define PARAM_CORRECTED "Draw Corrected"
#define PARAM_BLING "Bling"
#define PARAM_SHOW_SHADOW "Show Shadow"

typedef struct _RendererPerson {
    BotRenderer renderer;
    BotEventHandler ehandler;

    lcm_t *lcm;
    BotParam * param;
    BotFrames * frames;

    BotWavefrontModel *person_model;
    BotViewer *viewer;
    BotGtkParamWidget *pw;

    erlcm_people_pos_msg_t *people_pos_list;

    const char * draw_frame;
    const char * model_param_prefix;

    int display_lists_ready;
    GLuint person_dl;
    GMutex *mutex;
} RendererPerson;


static void
on_people_pos(const lcm_recv_buf_t *buf, const char *channel,
                 const erlcm_people_pos_msg_t *msg, void *user) {
    
  RendererPerson *self = (RendererPerson*)user;

  // g_mutex_lock (self->mutex);
  
  if (self->people_pos_list) 
    erlcm_people_pos_msg_t_destroy (self->people_pos_list);
  self->people_pos_list = erlcm_people_pos_msg_t_copy (msg);
  
  //g_mutex_unlock (self->mutex);
  
  bot_viewer_request_redraw(self->viewer);

}


static void
draw_wavefront_model (RendererPerson * self, double x, double y, double heading)
{
  //printf("drawing model %f %f %f \n",x, y, heading);
    glEnable (GL_BLEND);
    glEnable (GL_RESCALE_NORMAL);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glShadeModel (GL_SMOOTH);
    glEnable (GL_LIGHTING);

    glPushMatrix();
    ///x,y,z
    glTranslated(x, y, 0);
    
    //orientation
    glRotatef(heading, 0, 0, 1);

    glCallList (self->person_dl);

    //undo 
    glPopMatrix();
    //orientation
    //glRotatef(-heading, 0, 0, 1);
     ///x,y,z
    //glTranslated(-x, -y, 0);
    
    
}

static void frames_update_handler(BotFrames *bot_frames, const char *frame, const char * relative_to, int64_t utime,
    void *user)
{
  RendererPerson *self = (RendererPerson *) user;
  if (strcmp(frame, QUAD_COORD_FRAME) == 0)
    bot_viewer_request_redraw(self->viewer);
}

static void on_find_button(GtkWidget *button, RendererPerson *self)
{
  BotViewHandler *vhandler = self->viewer->view_handler;

  double eye[3];
  double lookat[3];
  double up[3];

  vhandler->get_eye_look(vhandler, eye, lookat, up);
  double diff[3];
  bot_vector_subtract_3d(eye, lookat, diff);

  BotTrans pose;
  bot_frames_get_trans(self->frames, QUAD_COORD_FRAME, self->draw_frame, &pose);
  bot_vector_add_3d(pose.trans_vec, diff, eye);

  vhandler->set_look_at(vhandler, eye, pose.trans_vec, up);

  bot_viewer_request_redraw(self->viewer);
}

static void person_free(BotRenderer *super)
{
  RendererPerson *self = (RendererPerson*) super->user;

  if (self->person_model)
    bot_wavefront_model_destroy(self->person_model);
  free(self);
}

static GLuint
compile_display_list (RendererPerson * self, BotWavefrontModel * model)
{
    GLuint dl = glGenLists (1);
    glNewList (dl, GL_COMPILE);
    
    const char * prefix = self->model_param_prefix;
    char key[1024];

    glPushMatrix();

    sprintf(key, "%s.translate", prefix);
    double trans[3];
    if (bot_param_get_double_array(self->param, key, trans, 3) == 3)
      glTranslated(trans[0], trans[1], trans[2]);

    sprintf(key, "%s.scale", prefix);
    double scale;
    if (bot_param_get_double(self->param, key, &scale) == 0)
      glScalef(scale, scale, scale);

    sprintf(key, "%s.rotate_xyz", prefix);
    double rot[3];
    if (bot_param_get_double_array(self->param, key, rot, 3) == 3) {
      glRotatef(rot[2], 0, 0, 1);
      glRotatef(rot[1], 0, 1, 0);
      glRotatef(rot[0], 1, 0, 0);
    }

    glEnable(GL_LIGHTING);
    bot_wavefront_model_gl_draw(model);
    glDisable(GL_LIGHTING);

    glPopMatrix();

    glEndList ();
    return dl;
}


static void person_draw(BotViewer *viewer, BotRenderer *super)
{
  RendererPerson *self = (RendererPerson*) super->user;

  ///////////////////////////////////////////////////////////////////////////
  /* for now, use this to test. take it out when messages are being published*/
  ///////////////////////////////////////////////////////////////////////////
  /*self->people_pos_list = (erlcm_people_pos_msg_t*)calloc(1, sizeof(erlcm_people_pos_msg_t));
  self->people_pos_list->num_people = 1;
  erlcm_person_status_t* person_status_list  = (erlcm_person_status_t*)calloc(1, sizeof(erlcm_person_status_t));
  person_status_list->pos[0] = -1;
  person_status_list->pos[1] = 1.5;
  person_status_list->person_heading = -45;
  self->people_pos_list->people_pos = person_status_list;*/
  ///////////////////////////////////////////////////////////////////////////

  int corrected = bot_gtk_param_widget_get_bool(self->pw, PARAM_CORRECTED);

  int draw_test = bot_gtk_param_widget_get_bool(self->pw, PARAM_DRAW_TEST_PERSON);

  if(corrected){
      if (!bot_frames_have_trans(self->frames, QUAD_COORD_FRAME_CORRECTED, self->draw_frame))
          return;
  }
  else{
      if (!bot_frames_have_trans(self->frames, QUAD_COORD_FRAME, self->draw_frame))
          return;
  }

  int bling = bot_gtk_param_widget_get_bool(self->pw, PARAM_BLING);
  

  if (bling && self->person_model && !self->display_lists_ready) {
    self->person_dl = compile_display_list(self, self->person_model);
    self->display_lists_ready = 1;
  }

  // get the transform to orient the vehicle in drawing coordinates
  BotTrans body_to_local;
  if(corrected){
      bot_frames_get_trans(self->frames, QUAD_COORD_FRAME_CORRECTED, self->draw_frame, &body_to_local);
  }
  else{
      bot_frames_get_trans(self->frames, QUAD_COORD_FRAME, self->draw_frame, &body_to_local);
  }

  if (bling && self->display_lists_ready){
      if(draw_test){
          double body_to_local_m[16], body_to_local_m_opengl[16];

          glEnable(GL_DEPTH_TEST);

          bot_trans_get_mat_4x4(&body_to_local, body_to_local_m);
          bot_matrix_transpose_4x4d(body_to_local_m, body_to_local_m_opengl);// opengl expects column-major matrices
          glPushMatrix();
          glMultMatrixd(body_to_local_m_opengl); // rotate and translate the vehicle
              
          double test_xy[2] = {bot_gtk_param_widget_get_double(self->pw, PARAM_X_POS), bot_gtk_param_widget_get_double(self->pw, PARAM_Y_POS)};

          double test_angle = bot_gtk_param_widget_get_double(self->pw, PARAM_T_POS);//180;

          draw_wavefront_model(self, test_xy[0], test_xy[1], test_angle);
          
          glPopMatrix();
      }
      else if(self->people_pos_list != NULL) { 

          double body_to_local_m[16], body_to_local_m_opengl[16];

          glEnable(GL_DEPTH_TEST);

          bot_trans_get_mat_4x4(&body_to_local, body_to_local_m);
          bot_matrix_transpose_4x4d(body_to_local_m, body_to_local_m_opengl);// opengl expects column-major matrices
          glPushMatrix();
          glMultMatrixd(body_to_local_m_opengl); // rotate and translate the vehicle
    
          // draw each individual
          for (int i = 0; i < self->people_pos_list->num_people; i ++) {
              erlcm_person_status_t person = self->people_pos_list->people_pos[i];     
              draw_wavefront_model(self, person.pos[0], person.pos[1], bot_to_degrees(person.person_heading));
          }

          glPopMatrix();
      }
  }
}

static void on_param_widget_changed(BotGtkParamWidget *pw, const char *name, void *user)
{
  RendererPerson *self = (RendererPerson*) user;
  bot_viewer_request_redraw(self->viewer);
}

static void on_load_preferences(BotViewer *viewer, GKeyFile *keyfile, void *user_data)
{
  RendererPerson *self = (RendererPerson *) user_data;
  bot_gtk_param_widget_load_from_key_file(self->pw, keyfile, RENDERER_NAME);
}

static void on_save_preferences(BotViewer *viewer, GKeyFile *keyfile, void *user_data)
{
  RendererPerson *self = (RendererPerson *) user_data;
  bot_gtk_param_widget_save_to_key_file(self->pw, keyfile, RENDERER_NAME);
}

void add_person_model_renderer_to_viewer(BotViewer *viewer, int render_priority, BotParam * param, BotFrames * frames, lcm_t *_lcm)
{
  RendererPerson *self = (RendererPerson*) calloc(1, sizeof(RendererPerson));

  BotRenderer *renderer = &self->renderer;

  renderer->draw = person_draw;
  renderer->destroy = person_free;

  renderer->widget = gtk_vbox_new(FALSE, 0);
  renderer->name = (char *) RENDERER_NAME;
  renderer->user = self;
  renderer->enabled = 1;

  BotEventHandler *ehandler = &self->ehandler;
  ehandler->name = (char *) RENDERER_NAME;
  ehandler->enabled = 0;
  ehandler->pick_query = NULL;
  ehandler->key_press = NULL;
  ehandler->hover_query = NULL;
  ehandler->mouse_press = NULL;
  ehandler->mouse_release = NULL;
  ehandler->mouse_motion = NULL;
  ehandler->user = self;

  self->viewer = viewer;
  /* attempt to load wavefront model files */
  self->param = param;
  self->frames = frames;
  self->people_pos_list = NULL;

  self->lcm = _lcm;
  //self->followed_person = 0;

  bot_frames_add_update_subscriber(self->frames, frames_update_handler, (void *) self);

  erlcm_people_pos_msg_t_subscribe (self->lcm, "PEOPLE_LIST", on_people_pos, self);

  self->draw_frame = bot_frames_get_root_name(self->frames);

  const char * models_dir = getBasePath();

  char *model_name;
  char model_full_path[256];
  self->model_param_prefix = "models.person";
  char param_key[1024];
  snprintf(param_key, sizeof(param_key), "%s.wavefront_model", self->model_param_prefix);
  
  if (bot_param_get_str(self->param, param_key, &model_name) == 0) {
    snprintf(model_full_path, sizeof(model_full_path), "%s/../../models/%s", models_dir, model_name);
    //fprintf(stderr, "Full path : %s\n" , model_full_path);
    self->person_model = bot_wavefront_model_create(model_full_path);
    double minv[3];
    double maxv[3];
    bot_wavefront_model_get_extrema(self->person_model, minv, maxv);

    double span_x = maxv[0] - minv[0];
    double span_y = maxv[1] - minv[1];
    double span_z = maxv[2] - minv[2];

    double span_max = MAX(span_x, MAX(span_y, span_z));
  }
  else {
    fprintf(stderr, "person model name not found under param %s, drawing with boxy-wheelchair\n", param_key);
  }

  self->pw = BOT_GTK_PARAM_WIDGET(bot_gtk_param_widget_new());
  gtk_box_pack_start(GTK_BOX(renderer->widget), GTK_WIDGET(self->pw), TRUE, TRUE, 0);

  bot_gtk_param_widget_add_booleans(self->pw, BOT_GTK_PARAM_WIDGET_CHECKBOX, PARAM_BLING, 1, PARAM_SHOW_SHADOW, 0, PARAM_CORRECTED, 1, NULL);

  bot_gtk_param_widget_add_double(self->pw, PARAM_X_POS, 
                                 BOT_GTK_PARAM_WIDGET_SLIDER, 0,4.0 , 0.1, 1.0);

  bot_gtk_param_widget_add_double(self->pw, PARAM_Y_POS, 
                                 BOT_GTK_PARAM_WIDGET_SLIDER, 0,4.0 , 0.1, 0.0);
  
  bot_gtk_param_widget_add_double(self->pw, PARAM_T_POS, 
                                 BOT_GTK_PARAM_WIDGET_SLIDER, 0, 360.0 , 1.0, 0.0);

  bot_gtk_param_widget_add_booleans(self->pw, BOT_GTK_PARAM_WIDGET_CHECKBOX, PARAM_DRAW_TEST_PERSON, 0, NULL);

  GtkWidget *find_button = gtk_button_new_with_label("Find");
  gtk_box_pack_start(GTK_BOX(renderer->widget), find_button, FALSE, FALSE, 0);
  g_signal_connect(G_OBJECT(find_button), "clicked", G_CALLBACK(on_find_button), self);

  gtk_widget_show_all(renderer->widget);

  g_signal_connect(G_OBJECT(self->pw), "changed", G_CALLBACK(on_param_widget_changed), self);
  on_param_widget_changed(self->pw, "", self);

  bot_viewer_add_renderer(viewer, &self->renderer, render_priority);
  //    bot_viewer_add_event_handler(viewer, &self->ehandler, render_priority);


  g_signal_connect(G_OBJECT(viewer), "load-preferences", G_CALLBACK(on_load_preferences), self);
  g_signal_connect(G_OBJECT(viewer), "save-preferences", G_CALLBACK(on_save_preferences), self);
}
