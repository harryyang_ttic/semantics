#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <glib.h>
#include <string.h>
#include <assert.h>
#include <gdk/gdkkeysyms.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

#include <bot_vis/bot_vis.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <math.h>

#include <bot_core/bot_core.h>
#include <bot_vis/viewer.h>
#include <bot_vis/gtk_util.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>
#include <bot_core/math_util.h>
#include <geom_utils/geometry.h>

#include "lcmtypes/er_lcmtypes.h" 
#include <lcmtypes/slam_graph_particle_list_t.h>
#include <lcmtypes/slam_graph_edge_t.h>
#include <lcmtypes/slam_language_edge_t.h>
#include <lcmtypes/slam_language_label_t.h>
#include <lcmtypes/slam_graph_particle_t.h>
#include <lcmtypes/slam_particle_request_t.h>
#include <lcmtypes/slam_laser_pose_t.h>
#include <lcmtypes/slam_status_t.h>
#include <lcmtypes/slam_command_t.h>
#include <lcmtypes/slam_pixel_map_request_t.h>
#include <lcmtypes/slam_region_transition_t.h>


#define DTOR M_PI/180
#define RTOD 180/M_PI

#define PARTICLE_HISTORY_SIZE 10

#define MAX_REFERSH_RATE_USEC 30000 // about 1/30 of a second

#define RENDERER_NAME "Advanced Topological Graph"
#define DATA_CIRC_SIZE 10
#define POSE_DATA_CIRC_SIZE 200 
#define PARAM_REQUEST_MAP_SAVE "Save Graph to File"

#define PARAM_ANIMATION_SPEED "Diff Animation Speed (ms)"
#define PARAM_REQUEST_MAP "Request map"
#define PARAM_CLEAR_MAP "Clear Map"
#define PARAM_COLOR_MENU "Color"
#define VALID_MAP_IND "Draw Map"
#define PARAM_DISP_PROB "Display prob."
#define PARAM_DRAW_ALL_GRAPHS "Draw all graphs"
//#define PARAM_DRAW_ONE_GRAPH "Draw one graph"
#define PARAM_DRAW_SIDE_BY_SIDE "Draw side-by-side"
#define PARAM_DRAW_GRAPH_1_ID "Graph ID 1 :"
#define PARAM_DRAW_COV "Draw covariance"
#define PARAM_DRAW_LABEL_PIE_CHART "Draw label pie chart"
#define PARAM_DRAW_MAX_LABEL "Draw Max Label"
#define PARAM_PUBLISH_REGION_TRANSITION "Publish Region Change"
#define PARAM_DRAW_GRAPH_2_ID "Graph ID 2 :"
#define PARAM_DRAW_VAID_GRAPH "Draw Valid Graph"
#define PARAM_DRAW_NODE_ID "Draw Node IDs"
#define PARAM_DRAW_SUBNODES "Draw subnodes"
#define PARAM_DRAW_SUPERNODES "Draw supernodes"
#define PARAM_GRAPHS_IN_GRID "Draw Graphs in Grid"
#define PARAM_REMAP_REGIONS "Remap Region Colors"
#define PARAM_DRAW_LOG "Draw LOG Scale"
#define PARAM_DRAW_ODOM "Highlight Odometry"
#define PARAM_DRAW_DEAD_EDGES "Draw Dead Edges"
#define PARAM_DRAW_MAX_GRAPH "Draw Max Graph"
#define PARAM_COLOR_REGIONS "Color regions"
#define PARAM_DRAW_MAP_POINTS "Draw Map Points"
#define PARAM_EDGE_THICKNESS "Edge Thickness"
#define PARAM_COLOR_MENU "Color"
#define PARAM_DRAW_HEIGHT "Draw height"
#define PARAM_HISTORY_LENGTH "Scan Hist. Len."
#define PARAM_HISTORY_FREQUENCY "Scan Hist. Freq."
#define PARAM_POINT_SIZE "Point Size" 
#define PARAM_DRAW_BLACK_EDGES "Draw Black Edges" 
#define PARAM_POINT_ALPHA "Point Alpha" 
#define PARAM_DRAW_EQUAL_DISTANCE "Equal graph spacing"
#define PARAM_DISTANCE_SCALE "Graph dist scale"
#define PARAM_ADD_CONSTRAINT_LABEL "Add Constraint"
#define PARAM_CONSTRAINT_TYPE "Type: "
#define PARAM_CONFIRM_AND_PUBLISH "Confirm and Publish"
#define ADD_LANG_LABEL "Write language label"
#define LANG_LABEL "Language label: "
#define LANG_CONSTRAINT_TYPE "Type of constraint: "
#define PARAM_CREATE_LANGUAGE_FILE "Start new annotation"
#define PARAM_SAVE_LANGUAGE_FILE "Save annotation file"
#define PARAM_LANGUAGE_UPDATE "Language update text: "
#define PARAM_BOUNDING_BOXES "Bounding boxes"

#define PARAM_COMPARE_PARTICLE_HISTORY_INDEX "Past Particle History"
#define PARAM_COMPARE_PARTICLE_HISTORY "Compare with Particle"

#define GRAPH_ANIMATION_RESOLUTION 10

#define NO_COLORS 8

#define DRAW_EDGES_TO_SUPERNODE "draw edges to supernode (versus closest nodes)"

enum {
    COLOR_Z,
    COLOR_INTENSITY,
    COLOR_NONE,
};

typedef struct _params_t{
    int draw_odom;
    int draw_height;
    int draw_max_map;
    int draw_prob;
    int draw_map_points;
    int draw_all_graphs;
    int draw_dead_edges;
    int edge_thickness;
    int draw_valid_maps;
    int draw_cov;
    int draw_pie_chart;
    int draw_max_label;
    int draw_mini_nodes;
    int draw_supernodes;
    int bounding_boxes;
    int color_regions;
    int remap_regions;
    int draw_diff;
    int log_scale;
    int draw_node_ids;

    int draw_side_by_side;
    int g_id_1;
    int g_id_2;
    int draw_black_edges;
    int distance_scale;
    int equal_dist;
} params_t;




typedef struct _RendererAdvancedTopoGraph RendererAdvancedTopoGraph;
struct _RendererAdvancedTopoGraph {
    BotRenderer renderer;
    BotEventHandler ehandler;
    
    lcm_t *lcm;
    BotParam *param;

    int have_data;
    GHashTable *slam_nodes;
    BotPtrCircular   *data_circ;

    BotViewer         *viewer;
    BotGtkParamWidget *pw;   

    params_t params;

    GMutex *mutex;
    
    double *xy_first;
    double *xy_second;
    int no_particles; 
    int active_particle;
    int node_1;
    int node_2;
    
    int current_ind;
    int active; //1 = add new constraint, 0 = inactive

    //insert to a sorted GList 
    BotPtrCircular *particle_history;

    GList *particle_list;
    slam_graph_particle_t *diff_particle;
    int64_t diff_utime;
    int diff_iter;

    int64_t last_pose_utime;
    gchar *lang_label_filename;
    //FILE *lang_label_fp;
    
    //double prob_bound;
};


inline int remap_ind(int ind, int middle){
    //split them in half - based on even or odd 
    int rem = ind % 2;
    
    int add = 0;
    
    if(middle %2==0)
        add = 1;

    //even 
    if(rem ==0){
        if(ind <= middle)
            return ind;
        else
            return ind - (middle+add);
    }
    else{
        if(ind <= middle)
            return ind+(middle+add);
        else
            return ind;
    }
}

gint compare_prob (gconstpointer a, gconstpointer b){
    slam_graph_particle_t *p1 = (slam_graph_particle_t *)a;
    slam_graph_particle_t *p2 = (slam_graph_particle_t *)b;

    double prob1 = exp(p1->weight);
    double prob2 = exp(p2->weight);
    if(prob1 > prob2)
        return 0;
    return 1;
}

void destroy_particle(gpointer data){
    if(data != NULL){
        slam_graph_particle_t *p = (slam_graph_particle_t *)data;
        slam_graph_particle_t_destroy(p);
    }
}

void draw_graph_particle(RendererAdvancedTopoGraph *self, slam_graph_particle_t *p, int k, double min_prob, 
                         int active_particle, double scale, int num_particles){
    params_t params = self->params;

    char label[1042];
    int j = p->id;
    double prob = p->weight; 
    if(!params.log_scale)
        prob = exp(prob);
    double alpha = 1.0;
    double weight_color = (prob - min_prob) * scale;

    double weight = (prob - min_prob) * scale * params.distance_scale;

    if(params.equal_dist){
        //make the gaps equal
        weight = (num_particles - k) * params.distance_scale;
    }        

    double gap = params.distance_scale; 

    double inc = gap / p->no_nodes;

    glPushAttrib (GL_DEPTH_BUFFER_BIT | GL_POINT_BIT | GL_CURRENT_BIT);
    glEnable (GL_DEPTH_TEST);
    glEnable(GL_BLEND);
        
    glColor3fv(bot_color_util_jet(weight_color));
        
    glEnable( GL_POINT_SMOOTH );
    glEnable( GL_BLEND );
    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    glPointSize( 8.0 );
        
    if(params.draw_pie_chart || self->params.draw_max_label){
        

        /*if(self->params.draw_max_label){
            
            } */           
        GList *colors = NULL;
        
        float c[3] = {1.0, 0, 0}; //red
        colors = g_list_append(colors, c);
        float c2[3] = {1.0,.5,0}; //orange
        colors =g_list_append(colors, c2);
        float c3[3] = {1.0,1.0,0}; //yellow
        colors =g_list_append(colors, c3);
        float c4[3] = {0,.5,0};
        colors =g_list_append(colors, c4);
        float c5[3] = {0,0,1.0};
        colors =g_list_append(colors, c5);
        float c6[3] = {.3,0,.5};
        colors =g_list_append(colors, c6);
        float c7[3] = {1.0,.5,1.0};
        colors =g_list_append(colors, c7);
        float c8[3] = {0.1,1.0,0.5};
        colors =g_list_append(colors, c8);
        
        glDisable(GL_BLEND);
        for(int i=0; i<p->no_nodes; i++){ //for all nodes in the particle...
            slam_graph_node_t node = p->node_list[i];
            slam_label_distribution_t ld = node.labeldist;
                
            double prob = ld.observation_frequency[0];
            int different = 0;
            double max_prob = 0;
            int max_ind = -1;

            if(node.is_supernode == 1){                
                for(int j=0; j<ld.num_labels; j++){
                    if(max_prob < ld.observation_frequency[j]){
                        max_prob = ld.observation_frequency[j];
                        max_ind = j;
                    }
                    if(ld.observation_frequency[j] != prob)
                        different = 1;
                }
            }
                
                
            if(different==1){ //if they're a supernode...
                    
                double c_weight = weight;
                if(params.draw_height){
                    c_weight += inc * i;
                }
                    
                double theta = 0;
                glColor3f(1,0,0);
                double radius = 2.0;
                    
                if(self->params.draw_max_label){    
                    glColor3fv( (g_list_nth_data(colors,(guint)(max_ind%NO_COLORS))));
                    glPointSize( 30.0 );
                    glPushMatrix();
                    glTranslatef (node.xy[0], node.xy[1], c_weight);
                    //glVertex3d( node.xy[0], node.xy[1] , c_weight);
                    bot_gl_draw_disk(1.0);
                    glPopMatrix();
                }
                else{
                    for(int j=0; j<ld.num_labels+1; j++){
                        glBegin(GL_TRIANGLE_FAN);
                        double p = ld.observation_frequency[j] / ld.total_obs * 2 * M_PI;
                        
                        glColor3fv( (g_list_nth_data(colors,(guint)(j%NO_COLORS))));
                        glVertex3d( node.xy[0], node.xy[1] , c_weight);
                        glVertex3d( node.xy[0] + radius*cos(theta), node.xy[1] + radius*sin(theta), c_weight);
                        for(int k=0; k<10; k++){
                            theta += p/10;
                            glVertex3d( node.xy[0] + radius*cos(theta), node.xy[1] + radius*sin(theta), c_weight);
                        }
                        glEnd();
                    }            
                }        
            }
        }
        /*if(self->params.draw_max_label){
            glEnd();
            }*/
    }
                    
    if(params.draw_cov){
        for(int i=0; i < p->no_nodes; i++){
            slam_graph_node_t node = p->node_list[i]; 
                
            double node_cov_xy[] = { node.cov[0], node.cov[1], node.cov[3], node.cov[4] };
                
            gsl_matrix_view cov_xy = 
                gsl_matrix_view_array (node_cov_xy, 2, 2);
                
            gsl_vector *eval = gsl_vector_alloc (2);
            gsl_matrix *evec = gsl_matrix_alloc (2, 2);
                
            gsl_eigen_symmv_workspace *w =
                gsl_eigen_symmv_alloc (2);
                
            gsl_eigen_symmv (&cov_xy.matrix, eval, evec, w);
                
            gsl_eigen_symmv_free (w);
                
            double m_opengl[16] = {evec->data[0], evec->data[1], 0, 0,
                                   evec->data[2], evec->data[3], 0, 0,
                                   0, 0, 1, 0,
                                   0, 0, 0, 1};
                
                
            glPushMatrix();
                
                
            glTranslatef (node.xy[0], node.xy[1], weight);
            glMultMatrixd (m_opengl); 
            glScalef (sqrt(eval->data[0]), sqrt(eval->data[1]), 1);
            gsl_vector_free (eval);
            gsl_matrix_free (evec);
                
            glLineWidth(.8);
            bot_gl_draw_circle(1.0);
            glPopMatrix();
        }                       
    }                    
                    
    if(params.bounding_boxes){
        for(int i=0; i < p->no_nodes; i++){
            slam_graph_node_t node = p->node_list[i]; 
                
            glBegin(GL_LINE_LOOP);
            for(j=0; j< node.no_points; j++){
                glVertex3d( node.x_coords[j], node.y_coords[j] , weight);
            }
            glEnd();
        }                       
    }
        
    glPointSize( 15.0 );
    glBegin( GL_POINTS );
        
    int div = p->no_regions;
    //make this even 
    if(div %2 == 1){
        div++;
    }
    int middle = (div / 2.0) -1;
        
    for(int i=0; i < p->no_nodes; i++){
        slam_graph_node_t node = p->node_list[i]; 

        if(node.is_supernode == 1 && params.draw_supernodes || params.draw_mini_nodes){  
            double c_weight = weight;
            if(params.draw_height){
                c_weight += inc * i;
            }
      
            //skip drawing the node if the pie chart is drawn
            if((params.draw_pie_chart || self->params.draw_max_label) && node.is_supernode == 1){
                slam_label_distribution_t ld = node.labeldist;
                double prob = ld.observation_frequency[0];
                int different = 0;
                for(int j=0; j<ld.num_labels; j++){
                    if(ld.observation_frequency[j] != prob){
                        different = 1;
                        break;
                    }
                }
                if(different)
                    continue;
            }
        
            if(active_particle && (i==(int)self->node_1 || i==(int)self->node_2)){
                glColor3f(1,0,0);
            }

            

            if(params.color_regions){
                double region_weight = 0;
                if(params.remap_regions){
                    int remapped_ind = remap_ind(node.parent_supernode, middle);
                    region_weight = remapped_ind/((double) p->no_regions-1);
                }
                else{
                    region_weight = node.parent_supernode / ((double) p->no_regions-1);
                }                
                glColor3fv(bot_color_util_jet(region_weight));
            }
        
            glVertex3d( node.xy[0], node.xy[1] , c_weight);                
        }
    }
    glEnd();
    glPopAttrib();

    glColor3fv(bot_color_util_jet(weight_color));
        
    if(params.draw_prob || params.draw_node_ids){
        glColor3f(1,1,1);
        for(int i=0; i < p->no_nodes; i++){
            slam_graph_node_t node = p->node_list[i];       
            if(node.is_supernode == 1 && params.draw_supernodes || params.draw_mini_nodes){  
                //draw the prob 
                double c_weight = weight;
                if(params.draw_height){
                    c_weight += inc * i;
                }
                double textpos[3] = {node.xy[0] +0.4, node.xy[1] +0.4, c_weight};
                
                if(params.draw_prob){
                    sprintf(label,"%.3f", node.pofz);
                    bot_gl_draw_text(textpos, GLUT_BITMAP_HELVETICA_12, label,
                                     BOT_GL_DRAW_TEXT_DROP_SHADOW);
                }
                else{
                    sprintf(label,"%d", (int)node.id);
                    bot_gl_draw_text(textpos, GLUT_BITMAP_HELVETICA_12, label,
                                     BOT_GL_DRAW_TEXT_DROP_SHADOW);
                    
                }                
            }
        }
    }
        
    if(params.draw_map_points){
       //|| (params.draw_side_by_side && (k == params.g_id_1 || k== params.g_id_2))){
        glPointSize( 2.0 );
        glBegin( GL_POINTS );
        glColor3fv(bot_color_util_jet(weight_color));
                    
        for(int i=0; i < p->no_nodes; i++){
            slam_graph_node_t node = p->node_list[i];         
                        
            slam_laser_pose_t *laser_pose = (slam_laser_pose_t *) g_hash_table_lookup(self->slam_nodes, &node.node_id);
            if(!laser_pose)
                continue;

            BotTrans bodyToLocal;
            bodyToLocal.trans_vec[0] = node.xy[0];
            bodyToLocal.trans_vec[1] = node.xy[1];
            bodyToLocal.trans_vec[2] = 0;
            double rpy[3] = { 0, 0, node.heading };
            bot_roll_pitch_yaw_to_quat(rpy, bodyToLocal.rot_quat);
            double pBody[3] = { 0, 0, 0 };
            double pLocal[3];

            double c_weight = weight;
            if(params.draw_height){
                c_weight += inc * i;
            }

            for(int k=0; k < laser_pose->pl.no; k++){
                pBody[0] = laser_pose->pl.points[k].pos[0];
                pBody[1] = laser_pose->pl.points[k].pos[1];
                pBody[2] = 0;
                bot_trans_apply_vec(&bodyToLocal, pBody, pLocal);
                            
                glVertex3d( pLocal[0], pLocal[1] , c_weight);
            }
        }
        glEnd();
    }

    //draw the edges 
    glPushAttrib (GL_DEPTH_BUFFER_BIT | GL_POINT_BIT | GL_CURRENT_BIT);
    glEnable (GL_DEPTH_TEST);
    glEnable(GL_BLEND);
        
    for(int i=0; i < p->no_edges; i++){            
        int a;
        int b;
        if(bot_gtk_param_widget_get_bool (self->pw, DRAW_EDGES_TO_SUPERNODE)){
            a = p->edge_list[i].node_id_1;
            b = p->edge_list[i].node_id_2;
        }
        else{              
            a = p->edge_list[i].actual_scanned_node_id_1;
            b = p->edge_list[i].actual_scanned_node_id_2;
        }
            
        double c_weight_1 = weight;
        double c_weight_2 = weight;
        if(params.draw_height){
            c_weight_1 += inc * a;
            c_weight_2 += inc * b;
        }
            
        if(p->edge_list[i].status == SLAM_GRAPH_EDGE_T_STATUS_INITIALIZED){
            //we should prob skip them
            continue;
        }
        else{
            if(p->edge_list[i].status == SLAM_GRAPH_EDGE_T_STATUS_FAILED){ 
                if(!params.draw_dead_edges)
                    continue;
                double width =  params.edge_thickness * p->edge_list[i].scanmatch_hit;
                if(width == 0)
                    width = 0.1;
                glLineWidth(width);
                glColor3fv(bot_color_util_black);                            
            }
            else if(p->edge_list[i].status == SLAM_GRAPH_EDGE_T_STATUS_SUCCESS){
                double width = params.edge_thickness * p->edge_list[i].scanmatch_hit;
                if(width == 0)
                    width = 0.1;
                glLineWidth(width);
                    
                if(!params.draw_black_edges){
                    if(p->edge_list[i].type == SLAM_GRAPH_EDGE_T_TYPE_SM_LC){
                        float loop_color[3] = {1.0, 0, 0};
                        glColor3fv(bot_color_util_jet(1.0));
                    }
                    else if(p->edge_list[i].type == SLAM_GRAPH_EDGE_T_TYPE_LANGUAGE || p->edge_list[i].type == SLAM_GRAPH_EDGE_T_TYPE_LANGUAGE_FULL){
                        float loop_color[3] = {0, 1.0, 0};
                        glColor3fv(loop_color);
                    }
                    else if(p->edge_list[i].type == SLAM_GRAPH_EDGE_T_TYPE_ODOM_INC && params.draw_odom){
                        float loop_color[3] = {1.0, 0.8, 0.34};
                        glColor3fv(loop_color);
                    }
                    else if(p->edge_list[i].type == SLAM_GRAPH_EDGE_T_TYPE_SM_INFERRED){
                        float loop_color[3] = {1.0, 0.3, 1.0};
                        glColor3fv(loop_color);
                    }
                    else if(p->edge_list[i].type == SLAM_GRAPH_EDGE_T_TYPE_SM_NEW_INFO){
                        float loop_color[3] = {0.4, 0.1, 1.0};
                        glColor3fv(loop_color);
                    }                    
                    else{
                        glColor3fv(bot_color_util_jet(weight_color));
                    }
                }
                else{
                    glColor3fv(bot_color_util_black);                  
                }                
            }                        
        }                    
            
        slam_graph_node_t nodea = p->node_list[a];
        slam_graph_node_t nodeb = p->node_list[b];
            
        double alpha = 1.0;
        double scale = 0.05;
            
        glPointSize(4.0f);
        glBegin(GL_LINES);
            
        glVertex3d(nodea.xy[0],
                   nodea.xy[1],
                   c_weight_1);
        glVertex3d(nodeb.xy[0],
                   nodeb.xy[1],
                   c_weight_2);
        glEnd();
    }

    if(params.draw_dead_edges){
        for(int i=0; i < p->no_rejected_edges; i++){
            int a;
            int b;
            if(bot_gtk_param_widget_get_bool (self->pw, DRAW_EDGES_TO_SUPERNODE)) {
                a = p->rejected_edge_list[i].node_id_1;
                b = p->rejected_edge_list[i].node_id_2;
            }
            else{
                a = p->rejected_edge_list[i].actual_scanned_node_id_1;
                b = p->rejected_edge_list[i].actual_scanned_node_id_2;
            }
                
            double c_weight_1 = weight;
            double c_weight_2 = weight;
            if(params.draw_height){
                c_weight_1 += inc * a;
                c_weight_2 += inc * b;
            }
                
            if(p->rejected_edge_list[i].status == SLAM_GRAPH_EDGE_T_STATUS_INITIALIZED){
                //we should prob skip them
                continue;
            }
            else{
                if(p->rejected_edge_list[i].status == SLAM_GRAPH_EDGE_T_STATUS_FAILED){ 
                    double width =  params.edge_thickness * p->edge_list[i].scanmatch_hit;
                    if(width == 0)
                        width = 0.1;
                    glLineWidth(width);
                    glColor3fv(bot_color_util_black);                            
                }                                     
            }                    
                
            //here is the problem 
            slam_graph_node_t nodea = p->node_list[a];
            slam_graph_node_t nodeb = p->node_list[b];
                
            double alpha = 1.0;
            double scale = 0.05;
                
            glPointSize(4.0f);
            glBegin(GL_LINES);
                
            glVertex3d(nodea.xy[0],
                       nodea.xy[1],
                       c_weight_1);
            glVertex3d(nodeb.xy[0],
                       nodeb.xy[1],
                       c_weight_2);
            glEnd();
        }
    }        
    glPopAttrib();
}

slam_graph_particle_t *get_diff_particle(slam_graph_particle_list_t *current, slam_graph_particle_list_t *old, int index, int iter){
    slam_graph_particle_t *new_particle = &current->particle_list[index];
    slam_graph_particle_t *old_particle = NULL;
    
    for(int i=0; i <  old->no_particles; i++){
        if(old->particle_list[index].id == new_particle->id){
            old_particle = &old->particle_list[index];
            break;
        }
    }
    
    if(old_particle == NULL){
        fprintf(stderr, "New particle did not exist\n");
        return NULL;
    }
    slam_graph_particle_t *diff_particle = slam_graph_particle_t_copy(&current->particle_list[index]);
    //find the transition 
    //put the old nodes to a hash table 
    GHashTable *old_nodes = g_hash_table_new(g_int_hash, g_int_equal);
    
    for(int i=0; i< old_particle->no_nodes; i++){ 
        slam_graph_node_t *node = &old_particle->node_list[i];
        //fprintf(stderr, "\tNode : %d => (old : %f,%f)\n", (int) node->id, node->xy[0], node->xy[1]);
        g_hash_table_insert(old_nodes, &(node->id), node);
    }

    for(int i=0; i< diff_particle->no_nodes; i++){ //for all nodes in the particle...
        slam_graph_node_t *node = &diff_particle->node_list[i];
        slam_graph_node_t *old_node = (slam_graph_node_t *) g_hash_table_lookup(old_nodes, &(node->node_id));
        if(old_node == NULL)
            continue;

        double dx = (node->xy[0] -  old_node->xy[0]) / GRAPH_ANIMATION_RESOLUTION;
        double dy = (node->xy[1] -  old_node->xy[1]) / GRAPH_ANIMATION_RESOLUTION;
        node->xy[0] = old_node->xy[0] + dx * iter;
        node->xy[1] = old_node->xy[1] + dy * iter;
    }

    g_hash_table_destroy (old_nodes);    
    return diff_particle;
}

static void on_topo_graph (const lcm_recv_buf_t *rbuf, const char *channel,
                           const slam_graph_particle_list_t *msg, void *user)
{
    RendererAdvancedTopoGraph *self = (RendererAdvancedTopoGraph *)user;
    g_assert(self);

    bot_ptr_circular_add(self->particle_history, slam_graph_particle_list_t_copy(msg));
    //fprintf(stderr, "No of particles : %d\n", bot_ptr_circular_size(self->particle_history));
    if(bot_ptr_circular_size(self->particle_history) > 1){
        bot_gtk_param_widget_set_enabled (self->pw, PARAM_COMPARE_PARTICLE_HISTORY_INDEX, 1);
        int last_history_value = fmin(bot_gtk_param_widget_get_int(self->pw, PARAM_COMPARE_PARTICLE_HISTORY_INDEX) + 1,
                                      bot_ptr_circular_size(self->particle_history)-1);
        
        bot_gtk_param_widget_modify_int(self->pw, PARAM_COMPARE_PARTICLE_HISTORY_INDEX, 1, bot_ptr_circular_size(self->particle_history)-1, 1,last_history_value);
    }
    //destory the list 
    g_list_free_full (self->particle_list, destroy_particle);
    self->particle_list = NULL;
    
    //we should clear this 
    for(int i=0; i <  msg->no_particles; i++){
        slam_graph_particle_t *p = slam_graph_particle_t_copy(&msg->particle_list[i]);
        self->particle_list  = g_list_insert_sorted ( self->particle_list , p, compare_prob);
    }        
    
    int last_valid_ind = bot_gtk_param_widget_get_int(self->pw, VALID_MAP_IND);
    int last_valid_g1 = bot_gtk_param_widget_get_int(self->pw, PARAM_DRAW_GRAPH_1_ID);
    int last_valid_g2 = bot_gtk_param_widget_get_int(self->pw, PARAM_DRAW_GRAPH_2_ID);
    bot_gtk_param_widget_modify_int(self->pw, VALID_MAP_IND, 0, msg->no_particles-1, 1, last_valid_ind);
    bot_gtk_param_widget_modify_int(self->pw, PARAM_DRAW_GRAPH_1_ID, 0, msg->no_particles-1, 1, last_valid_g1);
    bot_gtk_param_widget_modify_int(self->pw, PARAM_DRAW_GRAPH_2_ID, 0, msg->no_particles-1, 1, last_valid_g2);

    self->no_particles = msg->no_particles;
    self->have_data = 1;    

    if(bot_gtk_param_widget_get_bool(self->pw, PARAM_COMPARE_PARTICLE_HISTORY) && bot_ptr_circular_size(self->particle_history) > 1){
        self->diff_utime = bot_timestamp_now();
        self->diff_iter = 0;

        /*for(int i=0; i < GRAPH_ANIMATION_RESOLUTION; i++){
            //create the diff and increment between them 
            slam_graph_particle_list_t *last_list = bot_ptr_circular_index(self->particle_history, 1);
            if(last_list == NULL)
                break;
            slam_graph_particle_list_t *current_list = bot_ptr_circular_index(self->particle_history, 0);
            int valid_ind = bot_gtk_param_widget_get_int(self->pw, VALID_MAP_IND);
            slam_graph_particle_t *diff_particle = get_diff_particle(current_list, last_list, valid_ind, i);
            if(diff_particle != NULL){
                if(self->diff_particle  != NULL){
                  slam_graph_particle_t_destroy(self->diff_particle);
                }
                self->params.draw_diff = 1;
                self->diff_particle = diff_particle;
                bot_viewer_request_redraw (self->viewer);
                fprintf(stderr, "Drawing Diff : %d\n", i);
                int sleep_time = bot_gtk_param_widget_get_int(self->pw, PARAM_ANIMATION_SPEED) * 1000;
                usleep(sleep_time);
            }
            else{
                self->params.draw_diff = 0;
                break;
                
            }
            }*/
    }
    else{
        self->params.draw_diff = 0;
        bot_viewer_request_redraw (self->viewer);
    }
}

static void on_pose (const lcm_recv_buf_t *rbuf, const char *channel,
                     const bot_core_pose_t *msg, void *user)
{
    RendererAdvancedTopoGraph *self = (RendererAdvancedTopoGraph *)user;

    self->last_pose_utime = msg->utime;

    //maybe we should do the diff when we get poses??

    return;
}

static void on_laser_pose (const lcm_recv_buf_t *rbuf, const char *channel,
                           const slam_laser_pose_t *msg, void *user)
{
    RendererAdvancedTopoGraph *self = (RendererAdvancedTopoGraph *)user;
    g_assert(self);

    slam_laser_pose_t *laser_pose = slam_laser_pose_t_copy(msg);
    g_hash_table_insert(self->slam_nodes, &(laser_pose->id), laser_pose);
    bot_viewer_request_redraw (self->viewer);
}

void clear_points(RendererAdvancedTopoGraph *self){
    GHashTableIter iter;
    gpointer key, value;
    g_hash_table_iter_init (&iter, self->slam_nodes);

    fprintf(stderr, "Slam reset - clearing the old pose info\n");
    while (g_hash_table_iter_next (&iter, &key, &value)){
        slam_laser_pose_t *laser_pose = (slam_laser_pose_t *) g_hash_table_lookup(self->slam_nodes, key);
        slam_laser_pose_t_destroy(laser_pose);
    }

    g_hash_table_remove_all(self->slam_nodes);

    //destory the list 
    g_list_free_full (self->particle_list, destroy_particle);
    self->particle_list = NULL;
}

static void on_slam_transforms (const lcm_recv_buf_t *rbuf, const char *channel,
                                const erlcm_rigid_transform_list_t *msg, void *user)
{
    RendererAdvancedTopoGraph *self = (RendererAdvancedTopoGraph *)user;
    g_assert(self);

    int to_match = self->current_ind;
    //find the matching transform and publish it out 
    for(int i=0; i < msg->num; i++){
        if(msg->list[i].id  == to_match){
            bot_core_rigid_transform_t_publish (self->lcm, "GLOBAL_TO_LOCAL", &msg->list[i].transform); 
            return;
        }        
    }
}


static void on_slam_status (const lcm_recv_buf_t *rbuf, const char *channel,
                            const slam_status_t *msg, void *user)
{
    RendererAdvancedTopoGraph *self = (RendererAdvancedTopoGraph *)user;
    g_assert(self);
    clear_points(self);
   
    fprintf(stderr, "Slam reset - clearing the old pose info - Finished\n");    
    bot_viewer_request_redraw (self->viewer);
}


static void
renderer_advanced_topological_graph_destroy (BotRenderer *renderer)
{
    if (!renderer)
        return;

    RendererAdvancedTopoGraph *self = (RendererAdvancedTopoGraph *) renderer->user;
    if (!self)
        return;
    
    free (self);
}



static void 
renderer_advanced_topological_graph_draw (BotViewer *viewer, BotRenderer *renderer)
{
    RendererAdvancedTopoGraph *self = (RendererAdvancedTopoGraph*)renderer->user;
    g_assert(self);

    if(g_list_length (self->particle_list) == 0)
        return;

    int draw_map_ind = 0;
    if(self->active_particle >=0){
        draw_map_ind = self->active_particle;
    }    
        
    int num_particles = g_list_length (self->particle_list);
        
    int max_prob_id = 0; 
    int min_prob_id = 0;
    int count = 0;
        
    char prob_status[100];
    char *full_status = calloc((num_particles+1) *100 , sizeof(char));
        
    slam_graph_particle_t *valid_map = g_list_nth_data (self->particle_list, draw_map_ind);
    sprintf(prob_status, "CP : %d\n", (int) valid_map->id);
    strcat(full_status, prob_status);

    for(guint i=0; i <  g_list_length (self->particle_list); i++){
        slam_graph_particle_t *p = g_list_nth_data (self->particle_list, i);
        slam_graph_particle_t *p_max = g_list_nth_data (self->particle_list, max_prob_id);
        slam_graph_particle_t *p_min = g_list_nth_data (self->particle_list, min_prob_id);
            
        double prob = p->weight;
        double max_prob = p_max->weight;
        double min_prob = p_min->weight;
        if(!self->params.log_scale){
            prob = exp(prob);
            max_prob = exp(max_prob);
            min_prob = exp(min_prob);
        }
        count++;
        sprintf(prob_status, "[%d] : %.3f\n", (int) p->id, prob);
        strcat( full_status, prob_status);
        if(prob > max_prob)
            max_prob_id = i;
        else if(prob < min_prob)
            min_prob_id = i;
    }

    
          
    slam_graph_particle_t *p_max = g_list_nth_data (self->particle_list, max_prob_id);
    slam_graph_particle_t *p_min = g_list_nth_data (self->particle_list, min_prob_id);
        
    double min_prob = p_min->weight; 
    double max_prob = p_max->weight; 
        
    if(!self->params.log_scale){
        max_prob = exp(max_prob);
        min_prob = exp(min_prob);
    }

    if(count == 0)
        return;
    double scale = 1.0;
        
    if(max_prob > min_prob){
        scale = 0.8/ (max_prob - min_prob);
    }

    if(self->params.draw_all_graphs || self->params.draw_max_map){
        self->current_ind = p_max->id;
    }
    if(self->params.draw_valid_maps){
        self->current_ind = valid_map->id;
    }
     
    char label[1042];
    
    if(!self->params.draw_diff){//!bot_gtk_param_widget_get_bool(self->pw, PARAM_COMPARE_PARTICLE_HISTORY)){
        if(self->params.draw_all_graphs){
            for(guint k=0; k <  g_list_length (self->particle_list); k++){
                //draw this particle if it's valid 
                slam_graph_particle_t *p = g_list_nth_data (self->particle_list, k);            
                draw_graph_particle(self, p, k, min_prob, draw_map_ind, scale, num_particles);
            }
        }
        else if(self->params.draw_valid_maps){
            for(guint k=0; k <  g_list_length (self->particle_list); k++){
                slam_graph_particle_t *p = g_list_nth_data (self->particle_list, k); 
                if(valid_map == p){
                    draw_graph_particle(self, p, k, min_prob, draw_map_ind, scale, num_particles); 
                    break;
                }
            }
        }
        else if(self->params.draw_max_map){
            for(guint k=0; k <  g_list_length (self->particle_list); k++){
                slam_graph_particle_t *p = g_list_nth_data (self->particle_list, k); 
                if(p_max == p){
                    draw_graph_particle(self, p, k, min_prob, draw_map_ind, scale, num_particles); 
                    break;
                }
            }
        }
    }
    else{
        //fprintf(stderr, "Redrawing\n");
        if(self->diff_particle){
            //we need to do something funcky to change from the last particle to the next particle 
            draw_graph_particle(self, self->diff_particle, 0, min_prob, draw_map_ind, scale, num_particles); 
        }
    }

    // Render the current robot status
    GLint viewport[4];
    glGetIntegerv (GL_VIEWPORT, viewport);
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluOrtho2D(0, viewport[2], 0, viewport[3]);

    glColor3f(1,1,1);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    double state_xyz[] = {50, 90, 100};
    bot_gl_draw_text(state_xyz, NULL, full_status,
                     BOT_GL_DRAW_TEXT_JUSTIFY_CENTER |
                     BOT_GL_DRAW_TEXT_ANCHOR_VCENTER |
                     BOT_GL_DRAW_TEXT_ANCHOR_HCENTER |
                     BOT_GL_DRAW_TEXT_DROP_SHADOW);
    free(full_status);
        
    glLoadIdentity();

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
}

static void activate(RendererAdvancedTopoGraph *self, int type)
{
    self->active = type;
    if(type==0){
        fprintf(stderr,"Reset.\n");
    }
    if(type==1){
        fprintf(stderr,"Ready for first click\n");
    }
    if(type==2){
        fprintf(stderr,"Ready for second click\n");
    }
    if(type==3){
        fprintf(stderr,"Ready to publish\n");
    }    
}



static void send_constraint (BotViewer *viewer, BotEventHandler *ehandler)
{
    RendererAdvancedTopoGraph *self = (RendererAdvancedTopoGraph*) ehandler->user;
    //fprintf(stderr, "ready to calculate constraint!\n");
   
    fprintf(stderr, "active particle: %d\n", self->active_particle);
    if(self->active_particle == -1 || self->active_particle > g_list_length(self->particle_list)-1)
        return;
      
    slam_graph_particle_t *p = g_list_nth_data (self->particle_list, self->active_particle);
    fprintf(stderr, "--------------------------------------------------------\nParticle index %d (id %d)\n>First node: %d\n>Second node: %d\n",(int)self->active_particle, (int)p->id, self->node_1, self->node_2);
   
    if(self->active != 3)
        return;
   
    slam_language_edge_t msg;
    //msg.particle_id = self->active_particle;  -- this is actually the index
    slam_graph_particle_t *particle = g_list_nth_data (self->particle_list, self->active_particle);
    msg.particle_id = particle->id;
    msg.node_id_1 = self->node_1;
    msg.node_id_2 = self->node_2;
    //double trsfm[3] = {0,0,0};
    //double cv[9] = {1,0,0, 0,1,0, 0,0,100};
    msg.transformation[0] = 0;
    msg.transformation[1] = 0;
    msg.transformation[2] = 0;
    msg.cov[0] = .5;
    msg.cov[1] = 0;
    msg.cov[2] = 0;
   
    msg.cov[3] = 0;
    msg.cov[4] = .5;
    msg.cov[5] = 0;
   
    msg.cov[6] = 0;
    msg.cov[7] = 0;
    msg.cov[8] = 10;
   
    msg.type = (int) bot_gtk_param_widget_get_enum(self->pw, PARAM_CONSTRAINT_TYPE);
    slam_language_edge_t_publish(self->lcm, "LANGUAGE_EDGE", &msg);
}

static void highlight_node(BotViewer *viewer, BotEventHandler *ehandler, int endpoint)
{
    RendererAdvancedTopoGraph *self = (RendererAdvancedTopoGraph*) ehandler->user;
    fprintf(stderr, "Color node %d!\n", endpoint);
   
    double *xy;
   
    if(endpoint==1)
        xy = self->xy_first;
    else xy = self->xy_second;
    double min_dist = HUGE;
    int node = -1;
    //fprintf(stderr, "initialized variables!\n");
    //fprintf(stderr, "active particle: %d\n", self->active_particle);
    if(self->active_particle == -1 || self->active_particle > g_list_length(self->particle_list)-1)
        return;
      
    slam_graph_particle_t *p = g_list_nth_data (self->particle_list, self->active_particle);
   
    //fprintf(stderr, "got particle! %p\n", (void *)p);
   
    //fprintf(stderr, "num nodes: %d\n", (int) p->no_nodes);
   
    for (int i=0; i< p->no_nodes; i++){
        //fprintf(stderr, "in for loop! %d\n", i);
        double *temp = p->node_list[i].xy;
        //fprintf(stderr, "calculated temp vals:, %f, %f\n", temp[0], temp[1]);
        double dist = sqrt(pow(temp[0] - xy[0],2)+pow(temp[1]-xy[1],2));
        //fprintf(stderr, "calculated distances\n");
        if(dist < min_dist){
            min_dist = dist;
            node = i;
        }
    }
    if(endpoint==1)
        self->node_1 = node;
    else self->node_2 = node;
    fprintf(stderr, "--------------------------------------------------------\nParticle index %d (id %d)\n>First node: %d\n>Distance one: %f\n",(int)self->active_particle, (int)p->id, node, min_dist);
}

static int mouse_press (BotViewer *viewer, BotEventHandler *ehandler,
                        const double ray_start[3], const double ray_dir[3], 
                        const GdkEventButton *event)
{
    RendererAdvancedTopoGraph *self = (RendererAdvancedTopoGraph*) ehandler->user;
    //fprintf(stderr, "Mouse pressed\n");

    double xy[2];
    int consumed = 0;

    geom_ray_z_plane_intersect_3d(POINT3D(ray_start), POINT3D(ray_dir), 
                                  0, POINT2D(xy));
    //fprintf(stderr, "done point 2d: %f, %f\n\n", xy[0],xy[1]);

    //memcpy(self->lastxy, xy, 2 * sizeof(double));

    if(self->active == 1){
        activate(self,2);
        self->xy_first = xy;
        highlight_node(self->viewer, &self->ehandler, 1);
    }
    else if(self->active == 2){
        self->xy_second = xy;
        highlight_node(self->viewer, &self->ehandler,2);
        activate(self,3);
        //send_constraint(self->viewer,&self->ehandler);
    }

    bot_viewer_request_redraw(viewer);

    return consumed;
}

void update_params(RendererAdvancedTopoGraph *self, BotGtkParamWidget *pw){
    self->params.draw_odom = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_ODOM);
    self->params.draw_height = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_HEIGHT);
    //fprintf(stderr, "Draw height : %d\n", draw_height);
    self->params.draw_max_map = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_MAX_GRAPH);
    self->params.draw_prob = bot_gtk_param_widget_get_bool (self->pw, PARAM_DISP_PROB);
    self->params.draw_map_points = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_MAP_POINTS);
    self->params.draw_all_graphs = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_ALL_GRAPHS);
    //int draw_one_graph = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_ONE_GRAPH);
    self->params.draw_dead_edges = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_DEAD_EDGES);
    self->params.edge_thickness = bot_gtk_param_widget_get_int (self->pw, PARAM_EDGE_THICKNESS);
    self->params.draw_valid_maps = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_VAID_GRAPH);
    self->params.draw_cov = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_COV);
    //this should be set to draw the pie charts
    self->params.draw_pie_chart = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_LABEL_PIE_CHART);
    self->params.draw_max_label = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_MAX_LABEL);
    self->params.draw_mini_nodes = bot_gtk_param_widget_get_bool(self->pw, PARAM_DRAW_SUBNODES);
    self->params.draw_node_ids = bot_gtk_param_widget_get_bool(self->pw, PARAM_DRAW_NODE_ID);
    //this should be 1 to draw supernodes
    self->params.draw_supernodes = bot_gtk_param_widget_get_bool(self->pw, PARAM_DRAW_SUPERNODES);
    self->params.bounding_boxes = bot_gtk_param_widget_get_bool(self->pw, PARAM_BOUNDING_BOXES);
    self->params.color_regions = bot_gtk_param_widget_get_bool(self->pw, PARAM_COLOR_REGIONS);
    self->params.remap_regions = bot_gtk_param_widget_get_bool(self->pw, PARAM_REMAP_REGIONS);

    self->params.log_scale = bot_gtk_param_widget_get_bool (self->pw, PARAM_DRAW_LOG);

    //maybe turn the others off if this is true??
    self->params.draw_side_by_side = bot_gtk_param_widget_get_bool(self->pw, PARAM_DRAW_SIDE_BY_SIDE); 
    self->params.g_id_1 = bot_gtk_param_widget_get_int(self->pw, PARAM_DRAW_GRAPH_1_ID);
    //maybe turn the others off if this is true??
    self->params.g_id_2 = bot_gtk_param_widget_get_int(self->pw, PARAM_DRAW_GRAPH_2_ID);

    self->params.distance_scale = bot_gtk_param_widget_get_int(self->pw, PARAM_DISTANCE_SCALE);
    self->params.equal_dist = bot_gtk_param_widget_get_bool(self->pw, PARAM_DRAW_EQUAL_DISTANCE);
    self->params.draw_black_edges = bot_gtk_param_widget_get_bool(self->pw, PARAM_DRAW_BLACK_EDGES);

}

static void
on_param_widget_changed (BotGtkParamWidget *pw, const char *name, void *user)
{
    RendererAdvancedTopoGraph *self = user;
    
    update_params(self, pw);

   

    
    
    if(!strcmp(name, PARAM_ADD_CONSTRAINT_LABEL)) {
        //fprintf(stderr,"Button clicked!!\n");
        activate(self, 1);
    }
    if(!strcmp(name, PARAM_CLEAR_MAP)){
        clear_points(self);
    }
    if(!strcmp(name, VALID_MAP_IND)) {
        int raw_value = bot_gtk_param_widget_get_int(pw, VALID_MAP_IND);
        if(raw_value < self->no_particles){
            self->active_particle = raw_value;
            fprintf(stderr,"Changing valid map index: %d\n", raw_value);
        }
        else{
            self->active_particle =  self->no_particles -1;
            //fprintf(stderr, "Outside the max particle no\n");
        }
    }
    if(!strcmp(name, PARAM_CONFIRM_AND_PUBLISH)) {
        fprintf(stderr,"\nConfirm button clicked!\n");
        send_constraint(self->viewer,&self->ehandler);
        activate(self, 0);
        self->node_1 = -1;
        self->node_2 = -1;
    }
    
    if(!strcmp(name, PARAM_PUBLISH_REGION_TRANSITION)){
        fprintf(stderr, "Publishing region transition\n");
        slam_region_transition_t region_msg;
        region_msg.utime = bot_timestamp_now();
        region_msg.prob = 1; 
        region_msg.type = SLAM_REGION_TRANSITION_T_TYPE_VIEWER;
        slam_region_transition_t_publish(self->lcm, "REGION_TRANSITION", &region_msg);
    }

    if(!strcmp(name, PARAM_REQUEST_MAP)){
        int draw_map_ind = bot_gtk_param_widget_get_int(self->pw, VALID_MAP_IND);
        if(draw_map_ind < g_list_length (self->particle_list)){
            slam_graph_particle_t *p = g_list_nth_data (self->particle_list, draw_map_ind);
            if(p == NULL)
                return;
            slam_pixel_map_request_t msg; 
            msg.utime = bot_timestamp_now();
            msg.particle_id = p->id;
            msg.request = SLAM_PIXEL_MAP_REQUEST_T_REQ_PIXEL_MAP;
            slam_pixel_map_request_t_publish(self->lcm, "PIXEL_MAP_REQUEST", &msg);
        }
    }

    if(!strcmp(name, PARAM_REQUEST_MAP_SAVE)){
        int draw_map_ind = bot_gtk_param_widget_get_int(self->pw, VALID_MAP_IND);
        if(draw_map_ind < g_list_length (self->particle_list)){
            slam_graph_particle_t *p = g_list_nth_data (self->particle_list, draw_map_ind);
            if(p == NULL)
                return;
            slam_particle_request_t msg; 
            msg.utime = bot_timestamp_now();
            msg.particle_id = p->id;
            msg.request = SLAM_PARTICLE_REQUEST_T_SAVE_PARTICLE;
            slam_particle_request_t_publish(self->lcm, "PARTICLE_SAVE_REQUEST", &msg);
        }
    }
    
    if(!strcmp(name, ADD_LANG_LABEL)){

        if (self->lang_label_filename) {
            FILE *fp = fopen (self->lang_label_filename, "a");
            fprintf (fp, "%"PRId64",%d,%d,%s\n", self->last_pose_utime,
                     (int32_t) bot_gtk_param_widget_get_enum(pw,LANG_LABEL),
                     (int32_t) bot_gtk_param_widget_get_enum(pw,LANG_CONSTRAINT_TYPE),
                     bot_gtk_param_widget_get_text_entry(pw, PARAM_LANGUAGE_UPDATE));
            fclose (fp);
        } else
            fprintf (stderr, "Error: You have to choose a file to save the language labels to\n");

        //slam_lang_label_t msg;
        //msg.utime = bot_timestamp_now();
        //msg.label = (int) bot_gtk_param_widget_get_enum(pw,LANG_LABEL);
        //msg.constraint_type = (int) bot_gtk_param_widget_get_enum(pw,LANG_CONSTRAINT_TYPE);
        //msg.update = bot_gtk_param_widget_get_text_entry(pw, PARAM_LANGUAGE_UPDATE); 
        //slam_lang_label_t_publish(self->lcm, "LANG_LABEL", &msg);
    }

    if (!strcmp(name, PARAM_CREATE_LANGUAGE_FILE)) {

        GtkWidget *dialog;
        dialog = gtk_file_chooser_dialog_new("Add language labels to file", NULL,
                                             GTK_FILE_CHOOSER_ACTION_SAVE,
                                             GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                             GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
                                             NULL);
    
        if (self->lang_label_filename)
            gtk_file_chooser_set_filename (GTK_FILE_CHOOSER(dialog),
                                           self->lang_label_filename);
        
        if (gtk_dialog_run (GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT) {
            char *filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
            if (filename != NULL) {
                if (self->lang_label_filename)
                    g_free (self->lang_label_filename);
                self->lang_label_filename = g_strdup (filename);
                
                free (filename);
            }
        }
        
        gtk_widget_destroy (dialog);
    }

    bot_viewer_request_redraw (self->viewer);
}

static void
on_load_preferences (BotViewer *viewer, GKeyFile *keyfile, void *user_data)
{
    RendererAdvancedTopoGraph *self = user_data;
    bot_gtk_param_widget_load_from_key_file (self->pw, keyfile, self->renderer.name);
}

static void
on_save_preferences (BotViewer *viewer, GKeyFile *keyfile, void *user_data)
{
    RendererAdvancedTopoGraph *self = user_data;
    bot_gtk_param_widget_save_to_key_file (self->pw, keyfile, self->renderer.name);
}

void particle_destroy(void *user, void *p){
    slam_graph_particle_list_t *part = (slam_graph_particle_list_t *) p;
    slam_graph_particle_list_t_destroy(part);
}

static gboolean
on_timer (RendererAdvancedTopoGraph * self)
{
    //fprintf(stderr, "Called\n");
    int64_t c_utime = bot_timestamp_now();
    int gap_ms = bot_gtk_param_widget_get_int(self->pw, PARAM_ANIMATION_SPEED);
    if((c_utime - self->diff_utime) /1.0e3 < (GRAPH_ANIMATION_RESOLUTION * gap_ms)){
        //fprintf(stderr, "Within diff time : (elapsed ms) : %.1f => (Gap ms) : %.1f  (iter) %d\n", 
        //      (c_utime - self->diff_utime) /1.0e3, (double) (GRAPH_ANIMATION_RESOLUTION * gap_ms),
        //      self->diff_iter);
        //do a diff and increment the iteration 

        if((c_utime - self->diff_utime) /1.0e3 < self->diff_iter  * gap_ms){
            //fprintf(stderr, "Within the same iter\n");
            return TRUE;
        }
        
        if(self->diff_iter < GRAPH_ANIMATION_RESOLUTION){
            fprintf(stderr, "Incrementing Diff : %d\n", self->diff_iter);
            self->diff_iter++;
            slam_graph_particle_list_t *last_list = bot_ptr_circular_index(self->particle_history, 1);
            if(last_list == NULL){
                self->params.draw_diff = 0;
                return TRUE;
            }
            slam_graph_particle_list_t *current_list = bot_ptr_circular_index(self->particle_history, 0);
            int valid_ind = bot_gtk_param_widget_get_int(self->pw, VALID_MAP_IND);
            slam_graph_particle_t *diff_particle = get_diff_particle(current_list, last_list, valid_ind, self->diff_iter);
            if(diff_particle != NULL){
                if(self->diff_particle  != NULL){
                  slam_graph_particle_t_destroy(self->diff_particle);
                }
                self->params.draw_diff = 1;
                self->diff_particle = diff_particle;
                bot_viewer_request_redraw (self->viewer);
                //fprintf(stderr, "Drawing Diff : %d\n", self->diff_iter);
            }
            else{
                fprintf(stderr, "Error getting diff\n");
                self->params.draw_diff = 0;
                return TRUE;
            }
        }
    }
    else{
        self->params.draw_diff = 0;
    }    
    
    return TRUE;
}

static RendererAdvancedTopoGraph *
renderer_advanced_topological_graph_new (BotViewer *viewer, int priority, BotParam * param)
{    
    RendererAdvancedTopoGraph *self = (RendererAdvancedTopoGraph*) calloc (1, sizeof (*self));

    self->viewer = viewer;

    self->particle_history = bot_ptr_circular_new(PARTICLE_HISTORY_SIZE, particle_destroy, self);

    BotRenderer *renderer = &self->renderer;
    renderer->draw = renderer_advanced_topological_graph_draw;
    renderer->destroy = renderer_advanced_topological_graph_destroy;
    renderer->widget = bot_gtk_param_widget_new();
    renderer->name = RENDERER_NAME;
    renderer->user = self;
    renderer->enabled = 1;

    self->lcm = bot_lcm_get_global (NULL);
    if (!self->lcm) {
        fprintf (stderr,"Error: setup_renderer_laser() failed to get global lcm object\n");
        renderer_advanced_topological_graph_destroy (renderer);
        return NULL;
    }

    self->param = param;
    if (!self->param) {
        fprintf (stderr,"Error: setup_renderer_laser() failed to get BotParam instance\n");
        renderer_advanced_topological_graph_destroy (renderer);
        return NULL;
    }

    self->mutex = g_mutex_new ();
     
    self->pw = BOT_GTK_PARAM_WIDGET (renderer->widget);
    self->particle_list = NULL;
    
    //bot_gtk_param_widget_add_double(self->pw, PROB_BOUND, 
    //                                BOT_GTK_PARAM_WIDGET_SLIDER, 0,.5 , .001, .3);
    
    gtk_widget_show_all (renderer->widget);
    g_signal_connect (G_OBJECT (self->pw), "changed", 
                      G_CALLBACK (on_param_widget_changed), self);
    g_signal_connect (G_OBJECT (viewer), "load-preferences", 
                      G_CALLBACK (on_load_preferences), self);
    g_signal_connect (G_OBJECT (viewer), "save-preferences",
                      G_CALLBACK (on_save_preferences), self);


    bot_gtk_param_widget_add_booleans (self->pw, 
                                       0,
                                       DRAW_EDGES_TO_SUPERNODE, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       0,
                                       PARAM_DRAW_ALL_GRAPHS, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       0,
                                       PARAM_GRAPHS_IN_GRID, 0, NULL);



    bot_gtk_param_widget_add_booleans (self->pw, 
                                       0,
                                       PARAM_DRAW_MAP_POINTS, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       0,
                                       PARAM_DISP_PROB, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       0,
                                       PARAM_DRAW_MAX_GRAPH, 0, NULL);

    
    bot_gtk_param_widget_add_booleans (self->pw, 
                                       0,
                                       PARAM_DRAW_VAID_GRAPH, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       0,
                                       PARAM_DRAW_DEAD_EDGES, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       0,
                                       PARAM_DRAW_COV, 0, NULL);
    
    bot_gtk_param_widget_add_booleans (self->pw, 
                                       0,
                                       PARAM_DRAW_LABEL_PIE_CHART, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       0,
                                       PARAM_DRAW_MAX_LABEL, 1, NULL);

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       0,
                                       PARAM_DRAW_NODE_ID, 0, NULL);


    bot_gtk_param_widget_add_booleans (self->pw, 
                                       0,
                                       PARAM_DRAW_SUPERNODES, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       0,
                                       PARAM_DRAW_SUBNODES, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       0,
                                       PARAM_REMAP_REGIONS, 1, NULL);

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       0,
                                       PARAM_COLOR_REGIONS, 1, NULL);    
 

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       0,
                                       PARAM_DRAW_LOG, 0, NULL);
                                       
    bot_gtk_param_widget_add_booleans (self->pw, 
                                       0,
                                       PARAM_BOUNDING_BOXES, 0, NULL);
    
    bot_gtk_param_widget_add_booleans ( self->pw, 0,
                                       PARAM_DRAW_ODOM, 1, NULL);

    bot_gtk_param_widget_add_booleans ( self->pw, 0,
                                       PARAM_DRAW_BLACK_EDGES, 0, NULL);
 

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       0,
                                       PARAM_COMPARE_PARTICLE_HISTORY, 0, NULL);
      
    bot_gtk_param_widget_add_int(self->pw, PARAM_COMPARE_PARTICLE_HISTORY_INDEX, BOT_GTK_PARAM_WIDGET_SLIDER, 1, PARTICLE_HISTORY_SIZE-1, 1, 1);
    bot_gtk_param_widget_add_int(self->pw, PARAM_ANIMATION_SPEED, BOT_GTK_PARAM_WIDGET_SLIDER, 0, 1000, 10, 10);
    bot_gtk_param_widget_set_enabled (self->pw, PARAM_COMPARE_PARTICLE_HISTORY_INDEX, 0);

    bot_gtk_param_widget_add_buttons(self->pw, PARAM_REQUEST_MAP, NULL);

    bot_gtk_param_widget_add_buttons(self->pw, PARAM_CLEAR_MAP, NULL);

    bot_gtk_param_widget_add_buttons(self->pw, PARAM_REQUEST_MAP_SAVE, NULL);

    bot_gtk_param_widget_add_int(self->pw, VALID_MAP_IND, 
                                 BOT_GTK_PARAM_WIDGET_SLIDER, 0,50 , 1, 0);

    bot_gtk_param_widget_add_int(self->pw,PARAM_EDGE_THICKNESS, 
                                 BOT_GTK_PARAM_WIDGET_SLIDER, 0 , 20 , 1, 4);
    
    bot_gtk_param_widget_add_booleans (self->pw, 
                                       0,
                                       PARAM_DRAW_SIDE_BY_SIDE, 0, NULL);
   

    bot_gtk_param_widget_add_int(self->pw, PARAM_DRAW_GRAPH_1_ID, BOT_GTK_PARAM_WIDGET_SLIDER, 0, 50, 1, 0);
    
    bot_gtk_param_widget_add_int(self->pw, PARAM_DRAW_GRAPH_2_ID, BOT_GTK_PARAM_WIDGET_SLIDER, 0, 50, 1, 0);

    bot_gtk_param_widget_add_int(self->pw,PARAM_DISTANCE_SCALE, 
                                 BOT_GTK_PARAM_WIDGET_SLIDER, 0 , 40 , 1, 5);
    
    bot_gtk_param_widget_add_booleans (self->pw, 
                                       0,
                                       PARAM_DRAW_EQUAL_DISTANCE, 0, NULL);

    bot_gtk_param_widget_add_booleans (self->pw, 
                                       0,
                                       PARAM_DRAW_HEIGHT, 0, NULL);

    bot_gtk_param_widget_add_separator(self->pw, "Label Location");
                                       
                                     
    // BUTTON - Create new file to write language labels
    bot_gtk_param_widget_add_buttons (self->pw, PARAM_CREATE_LANGUAGE_FILE, NULL);    
    
    //DROP DOWN MENU - label
    bot_gtk_param_widget_add_enum(self->pw, LANG_LABEL, BOT_GTK_PARAM_WIDGET_MENU, 
                                  0, 
                                  "courtyard",0,
                                  "gym",1,
                                  "hallway",2, 
                                  "amphitheater", 3,
                                  "cafeteria", 4, 
                                  "elevator lobby", 5, 
                                  "entrance", 6, NULL);
                                  
    //DROP DOWN MENU - constraint type
    bot_gtk_param_widget_add_enum(self->pw, LANG_CONSTRAINT_TYPE, BOT_GTK_PARAM_WIDGET_MENU, 
                                  0, 
                                  "At location X",0,
                                  "language update", 1, 
                                  NULL);
                                  
    bot_gtk_param_widget_add_text_entry(self->pw, PARAM_LANGUAGE_UPDATE, BOT_GTK_PARAM_WIDGET_ENTRY, "Insert language update here");


    // BUTTON - Writes the label to the text file
    bot_gtk_param_widget_add_buttons(self->pw, ADD_LANG_LABEL, NULL);

    // BUTTON - Save and close annotation file
    //bot_gtk_param_widget_add_buttons (self->pw, PARAM_SAVE_LANGUAGE_FILE, NULL);
    
    self->slam_nodes = g_hash_table_new(g_int_hash, g_int_equal);

    slam_graph_particle_list_t_subscribe(self->lcm, "PARTICLE_ISAM_RESULT", on_topo_graph, self);

    slam_graph_particle_list_t_subscribe(self->lcm, "PARTICLE_RESULT", on_topo_graph, self);
   
    slam_laser_pose_t_subscribe(self->lcm, "SLAM_POSE_LASER_POINTS", on_laser_pose, self);
       
    bot_core_pose_t_subscribe (self->lcm, "POSE", on_pose, self);

    self->ehandler.name = (char*)RENDERER_NAME;
    self->ehandler.enabled = 1;
    self->ehandler.mouse_press = mouse_press;
    self->ehandler.user = self;
    
    bot_viewer_add_event_handler(viewer, &self->ehandler, priority);
    //tells us when to dump the old buffer
    slam_status_t_subscribe(self->lcm, "SLAM_STATUS", on_slam_status, self);
    erlcm_rigid_transform_list_t_subscribe(self->lcm, "SLAM_TRANSFORMS", on_slam_transforms, self);
    
    g_timeout_add(10, (GSourceFunc) on_timer, self);

    self->active = 0;
    self->active_particle = -1;
    self->no_particles = 0;
    self->node_1 = -1;
    self->node_2 = -1;
    self->diff_particle = NULL;
    self->params.draw_diff = 0;
    self->current_ind = -1;
    self->diff_utime = 0;
    self->diff_iter = 0;

    

    return self;
}

void
setup_renderer_advanced_topological_graph (BotViewer *viewer, int priority, BotParam * param)
{
    RendererAdvancedTopoGraph *self = renderer_advanced_topological_graph_new (viewer, priority, param);
    bot_viewer_add_renderer (viewer, &self->renderer, priority);
}
