add_definitions(-std=gnu99)

include_directories(
    ${GLIB2_INCLUDE_DIRS}
    ${GTK2_INCLUDE_DIRS}
    ${OPENGL_INCLUDE_DIR}
    ${LCMTYPES_INCLUDE_DIRS}
)

# Create a shared library libquad-renderers.so with a single source file
add_library(semantic-renderers SHARED
    er_gl_utils.c
    viewer_aux_data.c
    renderer_grid.c
    renderer_topological_graph.c
    #renderer_region_graph.c
    renderer_robot.c
    renderer_person_model.c
    renderer_annotation.c
    #renderer_graph_annotation.c
    #renderer_language_annotation.c
    renderer_ground_truth_regions.c
)

target_link_libraries (semantic-renderers
    ${GTK2_LDFLAGS}
    ${OPENGL_LIBRARIES}
    ${GLUT_LIBRARIES}
    ${LCMTYPES_LIBS})
    
    
set(REQUIRED_LIBRARIES
    lcm
    bot2-core
    bot2-vis
    bot2-param-client 
    bot2-frames
    lcmtypes_bot2-procman
    lcmtypes_perception_lcmtypes
    lcmtypes_slam-lcmtypes
    opencv
    gsl
    path-util
    )

pods_use_pkg_config_packages(semantic-renderers ${REQUIRED_LIBRARIES})

# make the header public
# install it to include/quad-renderers
pods_install_headers(semantic_renderers.h er_gl_utils.h viewer_aux_data.h DESTINATION semantic_renderers)

pods_use_pkg_config_packages(semantic-renderers lcm bot2-core bot2-vis lcmtypes_er-lcmtypes lcmtypes_slam-lcmtypes)  
# make the library public
pods_install_libraries(semantic-renderers)



# create a pkg-config file for the library, to make it easier for other
# software to use.
pods_install_pkg_config_file(semantic-renderers
    CFLAGS
    LIBS -lsemantic-renderers 
    REQUIRES ${REQUIRED_LIBRARIES}
    VERSION 0.0.1)


