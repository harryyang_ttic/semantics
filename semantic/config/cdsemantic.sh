#! /bin/bash

function cdsemantic() {
  cd $SEMANTIC_PATH/software/$1
}

function cdsemanticconfig() {
  cd $SEMANTIC_PATH/config
}